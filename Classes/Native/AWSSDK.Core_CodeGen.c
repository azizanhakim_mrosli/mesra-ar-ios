﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsArray()
// 0x00000002 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsBoolean()
// 0x00000003 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsDouble()
// 0x00000004 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsInt()
// 0x00000005 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsUInt()
// 0x00000006 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsLong()
// 0x00000007 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsULong()
// 0x00000008 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsObject()
// 0x00000009 System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::get_IsString()
// 0x0000000A System.Boolean ThirdParty.Json.LitJson.IJsonWrapper::GetBoolean()
// 0x0000000B System.Double ThirdParty.Json.LitJson.IJsonWrapper::GetDouble()
// 0x0000000C System.Int32 ThirdParty.Json.LitJson.IJsonWrapper::GetInt()
// 0x0000000D System.UInt32 ThirdParty.Json.LitJson.IJsonWrapper::GetUInt()
// 0x0000000E System.Int64 ThirdParty.Json.LitJson.IJsonWrapper::GetLong()
// 0x0000000F System.UInt64 ThirdParty.Json.LitJson.IJsonWrapper::GetULong()
// 0x00000010 System.String ThirdParty.Json.LitJson.IJsonWrapper::GetString()
// 0x00000011 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetBoolean(System.Boolean)
// 0x00000012 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetDouble(System.Double)
// 0x00000013 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetInt(System.Int32)
// 0x00000014 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetUInt(System.UInt32)
// 0x00000015 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetJsonType(ThirdParty.Json.LitJson.JsonType)
// 0x00000016 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetLong(System.Int64)
// 0x00000017 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetULong(System.UInt64)
// 0x00000018 System.Void ThirdParty.Json.LitJson.IJsonWrapper::SetString(System.String)
// 0x00000019 System.String ThirdParty.Json.LitJson.IJsonWrapper::ToJson()
// 0x0000001A System.Void ThirdParty.Json.LitJson.IJsonWrapper::ToJson(ThirdParty.Json.LitJson.JsonWriter)
// 0x0000001B System.Int32 ThirdParty.Json.LitJson.JsonData::get_Count()
extern void JsonData_get_Count_m5B349CDFCFD373FB85577CB3DFEAD54422E43042 (void);
// 0x0000001C System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsArray()
extern void JsonData_get_IsArray_mB15F4E57CDC2823E471956AD207704304FA9FD9D (void);
// 0x0000001D System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsBoolean()
extern void JsonData_get_IsBoolean_m6E007BC3AF652BD6F196494BFD8C16803E2AF288 (void);
// 0x0000001E System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsDouble()
extern void JsonData_get_IsDouble_m718DDF5CC28F6E72E6D234298A4565E6032D73D8 (void);
// 0x0000001F System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsInt()
extern void JsonData_get_IsInt_mE316BCEFE761DE5CC69038B56009225E9BC73CDF (void);
// 0x00000020 System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsUInt()
extern void JsonData_get_IsUInt_m838B99A056D41CC5660B027657AC320C45750E1A (void);
// 0x00000021 System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsLong()
extern void JsonData_get_IsLong_m209FC856C8C0A209FC149163EA9A923A1CA90ED4 (void);
// 0x00000022 System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsULong()
extern void JsonData_get_IsULong_mA9A1708684D8C1DB8948F7E44251718944656471 (void);
// 0x00000023 System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsObject()
extern void JsonData_get_IsObject_m117659025DFF9D30DC22607E67EEF30B7CDE1EF2 (void);
// 0x00000024 System.Boolean ThirdParty.Json.LitJson.JsonData::get_IsString()
extern void JsonData_get_IsString_mC71B7AE55DE68C51CABA61D97B9617AE44863C39 (void);
// 0x00000025 System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_Count()
extern void JsonData_System_Collections_ICollection_get_Count_m4B402678A0F6309D854747B85EA4F70FA9F153DD (void);
// 0x00000026 System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.get_SyncRoot()
extern void JsonData_System_Collections_ICollection_get_SyncRoot_m29958F93B563136E6B8107B5957D27A60BBABF13 (void);
// 0x00000027 System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_IsReadOnly()
extern void JsonData_System_Collections_IDictionary_get_IsReadOnly_m74E2910DDBA5DCA86E977649B2FC3C76597EA82F (void);
// 0x00000028 System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsArray()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsArray_m157F98DC49902F21B50ADF20D0C4BED8098B4A00 (void);
// 0x00000029 System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsBoolean()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsBoolean_m584B9B491726979D49B14EBA96EFFB6E82844A78 (void);
// 0x0000002A System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsDouble()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsDouble_mEFC058F68112A0F8D5959FDF31355A3E9CE47EBA (void);
// 0x0000002B System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsInt()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsInt_m28110FB4E6676FFE327BF00DF4A6A10FEB287873 (void);
// 0x0000002C System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsLong()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsLong_m1D6A78079FB3C5985F49148DA69CC750FD6FFE10 (void);
// 0x0000002D System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsObject()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsObject_m6A96C3003B244304E86C20DA0B65FE0C5D890516 (void);
// 0x0000002E System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.get_IsString()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsString_mCA097127A6C0D830D951263D295433368207BD3B (void);
// 0x0000002F System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_IsFixedSize()
extern void JsonData_System_Collections_IList_get_IsFixedSize_m093565897EC7BFDD7FB30C8AEE7F816939FF6EE2 (void);
// 0x00000030 System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_IsReadOnly()
extern void JsonData_System_Collections_IList_get_IsReadOnly_mF028CD017F1EDA7DB8F03C4776D1D24A143AC6A1 (void);
// 0x00000031 System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.get_Item(System.Object)
extern void JsonData_System_Collections_IDictionary_get_Item_m607F6D46E5E008D272025B7E4EB6701343433C72 (void);
// 0x00000032 System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern void JsonData_System_Collections_IDictionary_set_Item_mD4EEE468506982400D5B255908C098A5ECC7B19C (void);
// 0x00000033 System.Object ThirdParty.Json.LitJson.JsonData::System.Collections.IList.get_Item(System.Int32)
extern void JsonData_System_Collections_IList_get_Item_m5282DB28977F405139B02A60EF8056E5C701A111 (void);
// 0x00000034 System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void JsonData_System_Collections_IList_set_Item_mC710F8573D068F03EC06DA5875CF72D429ECDFA1 (void);
// 0x00000035 System.Collections.Generic.IEnumerable`1<System.String> ThirdParty.Json.LitJson.JsonData::get_PropertyNames()
extern void JsonData_get_PropertyNames_m09BE0C5184CA1FC5FF1F490CDEC2BEF83549A409 (void);
// 0x00000036 ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::get_Item(System.String)
extern void JsonData_get_Item_m9C7A7EA98ADC9F268DFB852FF55A2E99B842589C (void);
// 0x00000037 System.Void ThirdParty.Json.LitJson.JsonData::set_Item(System.String,ThirdParty.Json.LitJson.JsonData)
extern void JsonData_set_Item_mCE9E32482CB8C6BD05EBC1A3E197CCDF61C97E28 (void);
// 0x00000038 ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::get_Item(System.Int32)
extern void JsonData_get_Item_m45ECDD78142439A435485C0E217C60E6E093D5F8 (void);
// 0x00000039 System.Void ThirdParty.Json.LitJson.JsonData::set_Item(System.Int32,ThirdParty.Json.LitJson.JsonData)
extern void JsonData_set_Item_m54A112315FBD6DB33CACA7B4DA83C0066D65F0CC (void);
// 0x0000003A System.Void ThirdParty.Json.LitJson.JsonData::.ctor()
extern void JsonData__ctor_m9480744DC59E7B5979FCC0DE5B39DFB9D6736275 (void);
// 0x0000003B System.Void ThirdParty.Json.LitJson.JsonData::.ctor(System.Object)
extern void JsonData__ctor_m272D41CC53EFC98B45A6AE6C800B318072B31834 (void);
// 0x0000003C System.Boolean ThirdParty.Json.LitJson.JsonData::op_Explicit(ThirdParty.Json.LitJson.JsonData)
extern void JsonData_op_Explicit_mA62D8090A1F62CC82EF7D8512F7BE5B85210677B (void);
// 0x0000003D System.String ThirdParty.Json.LitJson.JsonData::op_Explicit(ThirdParty.Json.LitJson.JsonData)
extern void JsonData_op_Explicit_m7B9165F06DA967CDFC166F072B19AB6C366E40DC (void);
// 0x0000003E System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void JsonData_System_Collections_ICollection_CopyTo_mBC470FD58CD453B47C3D40F4C1E3926820745DE2 (void);
// 0x0000003F System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Add(System.Object,System.Object)
extern void JsonData_System_Collections_IDictionary_Add_m70FC6F0BB7DC07F37B44832506446A1B38A60762 (void);
// 0x00000040 System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Contains(System.Object)
extern void JsonData_System_Collections_IDictionary_Contains_m760C9188C9DDF16FB97CADE5B36646733E4AC0CD (void);
// 0x00000041 System.Collections.IDictionaryEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.GetEnumerator()
extern void JsonData_System_Collections_IDictionary_GetEnumerator_m243920C141BAF13032866850513A10C750DDCE42 (void);
// 0x00000042 System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IDictionary.Remove(System.Object)
extern void JsonData_System_Collections_IDictionary_Remove_mD13C0D7D9B7B3AF2BAEE42623585ED7CA63AC333 (void);
// 0x00000043 System.Collections.IEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.IEnumerable.GetEnumerator()
extern void JsonData_System_Collections_IEnumerable_GetEnumerator_m5BC9AE38294467EFE94F5CCF294083603D7AC37F (void);
// 0x00000044 System.Boolean ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetBoolean()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetBoolean_mB527C13EC7DCDF31DBCFB98E5E25409C52B43AAC (void);
// 0x00000045 System.Double ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetDouble()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetDouble_mB0DBC1F6649FEC71E5076F61966F28C7FB4A75E5 (void);
// 0x00000046 System.Int32 ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetInt()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetInt_m2818C0E556B1E3ED27D4DC3AFA1676FAE6E50AD4 (void);
// 0x00000047 System.UInt32 ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetUInt()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetUInt_mDA657A0459BE5F64B62FF72380882F4B2FEF30C8 (void);
// 0x00000048 System.Int64 ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetLong()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetLong_mF755BECBBC91B97BE46F130C067FA5A707245456 (void);
// 0x00000049 System.UInt64 ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetULong()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetULong_m4388950E0CF3FF380CE2982504B035702E054D2B (void);
// 0x0000004A System.String ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.GetString()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetString_m45104EDF1155E0AD001A68E8B7D8F6FFF7E75AE6 (void);
// 0x0000004B System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetBoolean(System.Boolean)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetBoolean_m3978CCB92DF739730E6113ECB4428D63201C5A02 (void);
// 0x0000004C System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetDouble(System.Double)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetDouble_m09111419939071696F84C59B078D3662A61F053E (void);
// 0x0000004D System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetInt(System.Int32)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetInt_m8C4573E2273D4ABC8F6438622C00D3879AB24708 (void);
// 0x0000004E System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetUInt(System.UInt32)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetUInt_m46A7674ADE879D3C49143A30F23BE877A541527B (void);
// 0x0000004F System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetLong(System.Int64)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetLong_m9F400D26662373B6F2749C38A50B02ECDE586D74 (void);
// 0x00000050 System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetULong(System.UInt64)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetULong_m1DA4B31B56992FDABB274DE1B9DD4B6E663C43A8 (void);
// 0x00000051 System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.SetString(System.String)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetString_mFB2C16BA27AB3FCB449D291570943E50351C4863 (void);
// 0x00000052 System.String ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.ToJson()
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_ToJson_m7722E814127A81C083284F255AC0901F4BBB99CC (void);
// 0x00000053 System.Void ThirdParty.Json.LitJson.JsonData::ThirdParty.Json.LitJson.IJsonWrapper.ToJson(ThirdParty.Json.LitJson.JsonWriter)
extern void JsonData_ThirdParty_Json_LitJson_IJsonWrapper_ToJson_m72E2521D98EBB0E040C8DD49C1828E25F5A3BB1C (void);
// 0x00000054 System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Add(System.Object)
extern void JsonData_System_Collections_IList_Add_m69528521024C2B26C8F6221EA8B7ED0D8FD75689 (void);
// 0x00000055 System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Clear()
extern void JsonData_System_Collections_IList_Clear_mDE1395499A2AC620E57E51722B115A8DD226F809 (void);
// 0x00000056 System.Boolean ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Contains(System.Object)
extern void JsonData_System_Collections_IList_Contains_mB1EA78A6C254C165BA57DA0C075D92F82C01814D (void);
// 0x00000057 System.Int32 ThirdParty.Json.LitJson.JsonData::System.Collections.IList.IndexOf(System.Object)
extern void JsonData_System_Collections_IList_IndexOf_m7DE1118576B5F070255E48C2526FA67836C74754 (void);
// 0x00000058 System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Insert(System.Int32,System.Object)
extern void JsonData_System_Collections_IList_Insert_m7C3B667A028660BE7F988E0C4E6EE0BD0D319E76 (void);
// 0x00000059 System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.Remove(System.Object)
extern void JsonData_System_Collections_IList_Remove_m439F909368DEBAB46656BAC399047A897DC2275D (void);
// 0x0000005A System.Void ThirdParty.Json.LitJson.JsonData::System.Collections.IList.RemoveAt(System.Int32)
extern void JsonData_System_Collections_IList_RemoveAt_m990800A90EF3544AF48C3F48A9E3788AE6AB1B82 (void);
// 0x0000005B System.Collections.IDictionaryEnumerator ThirdParty.Json.LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern void JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m6234C6A9A4078ECE3E98AD9B18A00387B824288C (void);
// 0x0000005C System.Collections.ICollection ThirdParty.Json.LitJson.JsonData::EnsureCollection()
extern void JsonData_EnsureCollection_mF9B11BD9275C219319FCFC919365B20691A22BCE (void);
// 0x0000005D System.Collections.IDictionary ThirdParty.Json.LitJson.JsonData::EnsureDictionary()
extern void JsonData_EnsureDictionary_m0291DB0FC557B753A82895BED3D476573C223564 (void);
// 0x0000005E System.Collections.IList ThirdParty.Json.LitJson.JsonData::EnsureList()
extern void JsonData_EnsureList_mFCAC833A43D088674F27CBF77CADD823479F5782 (void);
// 0x0000005F ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonData::ToJsonData(System.Object)
extern void JsonData_ToJsonData_mE2B2DAC7889552093592C335DBBA06C2CCD37925 (void);
// 0x00000060 System.Void ThirdParty.Json.LitJson.JsonData::WriteJson(ThirdParty.Json.LitJson.IJsonWrapper,ThirdParty.Json.LitJson.JsonWriter)
extern void JsonData_WriteJson_mF8A773CCBA4C3B33F0D979AECCEA2975C9CF2D8D (void);
// 0x00000061 System.Int32 ThirdParty.Json.LitJson.JsonData::Add(System.Object)
extern void JsonData_Add_m6476490E580AE074857ECB17A069D446EF375BE7 (void);
// 0x00000062 System.Boolean ThirdParty.Json.LitJson.JsonData::Equals(ThirdParty.Json.LitJson.JsonData)
extern void JsonData_Equals_m233AFBBE2C058A3CC450788A5E27149E845FA188 (void);
// 0x00000063 System.Void ThirdParty.Json.LitJson.JsonData::SetJsonType(ThirdParty.Json.LitJson.JsonType)
extern void JsonData_SetJsonType_m1B896834F17F4B3155C12211ED44672B3064D6E8 (void);
// 0x00000064 System.String ThirdParty.Json.LitJson.JsonData::ToJson()
extern void JsonData_ToJson_m33A3B263A33DA951B6E9809668FD9466AFFAA90C (void);
// 0x00000065 System.Void ThirdParty.Json.LitJson.JsonData::ToJson(ThirdParty.Json.LitJson.JsonWriter)
extern void JsonData_ToJson_m88BDAA865DD294DE2D92460B4873BBBCC6C9453D (void);
// 0x00000066 System.String ThirdParty.Json.LitJson.JsonData::ToString()
extern void JsonData_ToString_m8B05D69F04E418FC6E16EB311BA003351006799D (void);
// 0x00000067 System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Current()
extern void OrderedDictionaryEnumerator_get_Current_m967C64BED8A3372739BC5AFD25C99D752DE0D471 (void);
// 0x00000068 System.Collections.DictionaryEntry ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Entry()
extern void OrderedDictionaryEnumerator_get_Entry_mDAEE20FB3EF8AE1534C84F550C4E58BC24AD6ADD (void);
// 0x00000069 System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Key()
extern void OrderedDictionaryEnumerator_get_Key_mC31237F59ACB35F27E5E6447ED23E50413A0D297 (void);
// 0x0000006A System.Object ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::get_Value()
extern void OrderedDictionaryEnumerator_get_Value_m27B7CA61102DA9F947996A2013F9F3FA32B5E12B (void);
// 0x0000006B System.Void ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>)
extern void OrderedDictionaryEnumerator__ctor_mDF82B39A521C648731F59BACD47BC90DFD91B82D (void);
// 0x0000006C System.Boolean ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::MoveNext()
extern void OrderedDictionaryEnumerator_MoveNext_mB19085F74D36FCAA3FA2136D2D468D426D892CBF (void);
// 0x0000006D System.Void ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::Reset()
extern void OrderedDictionaryEnumerator_Reset_m5BD38326A9877C3D2C886E431F6CDD2D6556DC4C (void);
// 0x0000006E System.Void ThirdParty.Json.LitJson.JsonException::.ctor(System.Int32)
extern void JsonException__ctor_m1B6ECFF6D071C36302819E4B46AE304C986C3CE7 (void);
// 0x0000006F System.Void ThirdParty.Json.LitJson.JsonException::.ctor(System.String)
extern void JsonException__ctor_m0A0A59127FA0DCA1A377D3FC9CEB3ED14FB82CAE (void);
// 0x00000070 System.Type ThirdParty.Json.LitJson.ArrayMetadata::get_ElementType()
extern void ArrayMetadata_get_ElementType_mA0F8D763181E8C7897701F98499C20EC872E36E4 (void);
// 0x00000071 System.Void ThirdParty.Json.LitJson.ArrayMetadata::set_ElementType(System.Type)
extern void ArrayMetadata_set_ElementType_m0165398A5F11604DE1D2C59516C3E6C66C92D395 (void);
// 0x00000072 System.Boolean ThirdParty.Json.LitJson.ArrayMetadata::get_IsArray()
extern void ArrayMetadata_get_IsArray_m9D939595CE680B7C8BC4B5EF5665022753A7E879 (void);
// 0x00000073 System.Void ThirdParty.Json.LitJson.ArrayMetadata::set_IsArray(System.Boolean)
extern void ArrayMetadata_set_IsArray_m1CD739BA6B4D869DED37B2CCBBBB2901BC068997 (void);
// 0x00000074 System.Boolean ThirdParty.Json.LitJson.ArrayMetadata::get_IsList()
extern void ArrayMetadata_get_IsList_m73EFF0512BB93D42859059FF0B98E9A4C26AB234 (void);
// 0x00000075 System.Void ThirdParty.Json.LitJson.ArrayMetadata::set_IsList(System.Boolean)
extern void ArrayMetadata_set_IsList_m05E0BFF65E6B91FCB52EF51354B7A492A331C1F9 (void);
// 0x00000076 System.Type ThirdParty.Json.LitJson.ObjectMetadata::get_ElementType()
extern void ObjectMetadata_get_ElementType_m4B736F26EDD2D73390884D9E93ACEF0588C0CEBC (void);
// 0x00000077 System.Void ThirdParty.Json.LitJson.ObjectMetadata::set_ElementType(System.Type)
extern void ObjectMetadata_set_ElementType_m133FC236AEB7BD106D60E0BF1A46EB330D1A8157 (void);
// 0x00000078 System.Boolean ThirdParty.Json.LitJson.ObjectMetadata::get_IsDictionary()
extern void ObjectMetadata_get_IsDictionary_m3D80DEBFB702D2C75364ABB5A9AEA71970F41DD9 (void);
// 0x00000079 System.Void ThirdParty.Json.LitJson.ObjectMetadata::set_IsDictionary(System.Boolean)
extern void ObjectMetadata_set_IsDictionary_m1D62DE45F5999CB46B0BABD7D44DED6A3E2B2DD7 (void);
// 0x0000007A System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.PropertyMetadata> ThirdParty.Json.LitJson.ObjectMetadata::get_Properties()
extern void ObjectMetadata_get_Properties_m9B4BE4987F4D9F01BD098A41645696DDD46C6A20 (void);
// 0x0000007B System.Void ThirdParty.Json.LitJson.ObjectMetadata::set_Properties(System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.PropertyMetadata>)
extern void ObjectMetadata_set_Properties_mF467235BB451CB7F69B6FCFD8344EF01082D14B3 (void);
// 0x0000007C System.Void ThirdParty.Json.LitJson.ExporterFunc::.ctor(System.Object,System.IntPtr)
extern void ExporterFunc__ctor_m262977CFB142C800F94C995354D5876B1B435960 (void);
// 0x0000007D System.Void ThirdParty.Json.LitJson.ExporterFunc::Invoke(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void ExporterFunc_Invoke_m7B91598C9CD54A29CE50C39EF8A2F88269425C7A (void);
// 0x0000007E System.IAsyncResult ThirdParty.Json.LitJson.ExporterFunc::BeginInvoke(System.Object,ThirdParty.Json.LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern void ExporterFunc_BeginInvoke_mD15F2FC001C3A4E0BD51A2117A29674756A4006D (void);
// 0x0000007F System.Void ThirdParty.Json.LitJson.ExporterFunc::EndInvoke(System.IAsyncResult)
extern void ExporterFunc_EndInvoke_mBF01B06C5282AE2CCF9A916779107A50391FBBD2 (void);
// 0x00000080 System.Void ThirdParty.Json.LitJson.ImporterFunc::.ctor(System.Object,System.IntPtr)
extern void ImporterFunc__ctor_m3FFC0BAA62E0AC747210CC0CF45449C0BECA6D36 (void);
// 0x00000081 System.Object ThirdParty.Json.LitJson.ImporterFunc::Invoke(System.Object)
extern void ImporterFunc_Invoke_m11921490DF88C5298FA40E1B7C05D5694979504C (void);
// 0x00000082 System.IAsyncResult ThirdParty.Json.LitJson.ImporterFunc::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ImporterFunc_BeginInvoke_mDF97AE632BF9CFB3569786E87B42231DB392EA34 (void);
// 0x00000083 System.Object ThirdParty.Json.LitJson.ImporterFunc::EndInvoke(System.IAsyncResult)
extern void ImporterFunc_EndInvoke_m7345F8726BF2933CE6892FD790C43DFEAC547EAE (void);
// 0x00000084 System.Void ThirdParty.Json.LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern void WrapperFactory__ctor_mD9AFC7DCBE92B0654B8AC7C89DD8773FB1257050 (void);
// 0x00000085 ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.WrapperFactory::Invoke()
extern void WrapperFactory_Invoke_m6C6184BEFDB603B8F1A5FBBBEDC10F89E21C941E (void);
// 0x00000086 System.IAsyncResult ThirdParty.Json.LitJson.WrapperFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern void WrapperFactory_BeginInvoke_mF33E0466C3C2D83B24E106CD7C637041AFB3B79E (void);
// 0x00000087 ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.WrapperFactory::EndInvoke(System.IAsyncResult)
extern void WrapperFactory_EndInvoke_m0B1EFB30BEDF1341D8B51B0B2C2A3DD29543BB5E (void);
// 0x00000088 System.Void ThirdParty.Json.LitJson.JsonMapper::.cctor()
extern void JsonMapper__cctor_m6DF38B88E837ABEFA4EBE2E6397F2211F82C6014 (void);
// 0x00000089 System.Void ThirdParty.Json.LitJson.JsonMapper::AddArrayMetadata(System.Type)
extern void JsonMapper_AddArrayMetadata_mFC7141AB2B325123BA04E9F505FF1E6034B13BF4 (void);
// 0x0000008A System.Void ThirdParty.Json.LitJson.JsonMapper::AddObjectMetadata(System.Type)
extern void JsonMapper_AddObjectMetadata_m91E003CBE9FA306F04CAA431804009395DAB8406 (void);
// 0x0000008B System.Void ThirdParty.Json.LitJson.JsonMapper::AddTypeProperties(System.Type)
extern void JsonMapper_AddTypeProperties_mDCF0A375DCD37DC89338042D41595D19FE74C1C1 (void);
// 0x0000008C System.Reflection.MethodInfo ThirdParty.Json.LitJson.JsonMapper::GetConvOp(System.Type,System.Type)
extern void JsonMapper_GetConvOp_mB5A6B80BB5F4650A18999FED99A8BBC669584E4F (void);
// 0x0000008D System.Object ThirdParty.Json.LitJson.JsonMapper::ReadValue(System.Type,ThirdParty.Json.LitJson.JsonReader)
extern void JsonMapper_ReadValue_m74BC26C2D550CB99FEA1F31E9FD91E1869045E0A (void);
// 0x0000008E System.Void ThirdParty.Json.LitJson.JsonMapper::ValidateRequiredFields(System.Object,System.Type)
extern void JsonMapper_ValidateRequiredFields_m10A500DEA597284D4450409B3233786DF939B049 (void);
// 0x0000008F ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ReadValue(ThirdParty.Json.LitJson.WrapperFactory,ThirdParty.Json.LitJson.JsonReader)
extern void JsonMapper_ReadValue_m313D23BAAD03233EDC6B5751D458257D9ED9346F (void);
// 0x00000090 System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterBaseExporters()
extern void JsonMapper_RegisterBaseExporters_mA135DEF8904D131E9DE28C116AFF4068AF764796 (void);
// 0x00000091 System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterBaseImporters()
extern void JsonMapper_RegisterBaseImporters_m7283762885A9CBC8FCF73C1409FE0A4B73F73706 (void);
// 0x00000092 System.Void ThirdParty.Json.LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>,System.Type,System.Type,ThirdParty.Json.LitJson.ImporterFunc)
extern void JsonMapper_RegisterImporter_mE3C963E04CF124D1EF70D9AE8655B23673FB990B (void);
// 0x00000093 System.Void ThirdParty.Json.LitJson.JsonMapper::WriteValue(System.Object,ThirdParty.Json.LitJson.JsonWriter,System.Boolean,System.Int32)
extern void JsonMapper_WriteValue_m0C04268F57BD995BC220C1A4AFEFB761AA76216E (void);
// 0x00000094 System.String ThirdParty.Json.LitJson.JsonMapper::ToJson(System.Object)
extern void JsonMapper_ToJson_mEC8DB1E520EC14692F869F5C766AD5E830B9FAEA (void);
// 0x00000095 ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonMapper::ToObject(System.IO.TextReader)
extern void JsonMapper_ToObject_m3F0E12BE022763FFFF80FC0D2B6BF9E5BE669897 (void);
// 0x00000096 ThirdParty.Json.LitJson.JsonData ThirdParty.Json.LitJson.JsonMapper::ToObject(System.String)
extern void JsonMapper_ToObject_mC64EA7544FB2A1312A3A4C3B4C3674A6C5725818 (void);
// 0x00000097 T ThirdParty.Json.LitJson.JsonMapper::ToObject(System.String)
// 0x00000098 ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ToWrapper(ThirdParty.Json.LitJson.WrapperFactory,ThirdParty.Json.LitJson.JsonReader)
extern void JsonMapper_ToWrapper_m96BC581D45EFC12E1BAE33F71A9CA5F545477AC5 (void);
// 0x00000099 ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper::ToWrapper(ThirdParty.Json.LitJson.WrapperFactory,System.String)
extern void JsonMapper_ToWrapper_m097E495DC63458F82C36053A970F9C66100F5DBD (void);
// 0x0000009A System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::.cctor()
extern void U3CU3Ec__cctor_mBC398A41CDF77CED40F737F11A282729CC16B760 (void);
// 0x0000009B System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::.ctor()
extern void U3CU3Ec__ctor_mCB72C11DA63562C2965ECBCF431F3A3855E4C514 (void);
// 0x0000009C System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_0(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_0_m5E65AFAA97DE9886E8685F4CA74F17DE0935824A (void);
// 0x0000009D System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_1(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_1_mA03284DD8D466CF8951FA09E11693B2406649E5F (void);
// 0x0000009E System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_2(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_2_m044D9D87625F9C820A9A641FFD88E36265B6C661 (void);
// 0x0000009F System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_3(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_3_m22DCF509B0371345B34C28811C11FD3EEA51251E (void);
// 0x000000A0 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_4(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_4_m5173AE1B7922832E705F23DD96D7B21716D8DFD8 (void);
// 0x000000A1 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_5(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_5_mA7562BABCCAFCE6961ADAF5A86796F9BFED47362 (void);
// 0x000000A2 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_6(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_6_m279BE783077BEFA61D3C10DA9A70948EA0293A0F (void);
// 0x000000A3 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_7(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_7_m9ACE3DE44C23B4412DCECBF5D8084F932FF47CE7 (void);
// 0x000000A4 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_8(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_8_mA85CC3F8FF379190501B377EB0D9FC56490B5500 (void);
// 0x000000A5 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_9(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_9_m7FDA939669F3EE3B1A7A1081B3ED54489D8DE637 (void);
// 0x000000A6 System.Void ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseExporters>b__25_10(System.Object,ThirdParty.Json.LitJson.JsonWriter)
extern void U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_10_m10D28BCEB66B6A4E20673897EAF82A4C462266AD (void);
// 0x000000A7 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_0(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_0_m1AAFB2154D45ABF1CC9B5B4843F511CB7405BD2D (void);
// 0x000000A8 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_1(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_1_m5A1FC8F094BF3D2F49B06F1EF0E0DE14FB05070F (void);
// 0x000000A9 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_2(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_2_mC4E29C0ACB005735FFF5736A51F9B7ED9793770F (void);
// 0x000000AA System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_3(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_3_m35500DB55A65C188F227633F534BE4455673F827 (void);
// 0x000000AB System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_4(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_4_mDE5A5ECA09D872D5DC395843A92FBC74750DD3A9 (void);
// 0x000000AC System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_5(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_5_mFC370259E48B292AF54B52D8042AFD81BCB78025 (void);
// 0x000000AD System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_6(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_6_m805A79B2C7218A777DA94BD6FF71FEF1EAAB84D8 (void);
// 0x000000AE System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_7(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_7_m3DD36AEEC6F91DFC05D993580AA4E7D243E8940C (void);
// 0x000000AF System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_8(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_8_m28757971BB5CBF7729FD1FCE4F452ACF4AE3A50D (void);
// 0x000000B0 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_9(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_9_mC7A02472079C1F330274BC251138A061F4A866D0 (void);
// 0x000000B1 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_10(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_10_m97BA343722EEC0A7934DDE7ADD1C89A4FD6D5370 (void);
// 0x000000B2 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_11(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_11_mC183338E2E99D595EECCCED5747FF8FBB213A0D8 (void);
// 0x000000B3 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_12(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_12_m7FD0038BFC4FA2A8B791170ACCDD51CCCD5654F7 (void);
// 0x000000B4 System.Object ThirdParty.Json.LitJson.JsonMapper/<>c::<RegisterBaseImporters>b__26_13(System.Object)
extern void U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_13_m2A9E4435AD064C727DA895AC5DB23CE8C6185FEE (void);
// 0x000000B5 ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper/<>c::<ToObject>b__32_0()
extern void U3CU3Ec_U3CToObjectU3Eb__32_0_m086D4879268A7374F7530400F470ECCF09B40328 (void);
// 0x000000B6 ThirdParty.Json.LitJson.IJsonWrapper ThirdParty.Json.LitJson.JsonMapper/<>c::<ToObject>b__33_0()
extern void U3CU3Ec_U3CToObjectU3Eb__33_0_m3A261637AFD7929969FD7768927FAFAA15F47DE5 (void);
// 0x000000B7 System.Boolean ThirdParty.Json.LitJson.JsonPropertyAttribute::get_Required()
extern void JsonPropertyAttribute_get_Required_m13B37FCCC7DA7AA77F335DEFAFBED5BAAA93C670 (void);
// 0x000000B8 ThirdParty.Json.LitJson.JsonToken ThirdParty.Json.LitJson.JsonReader::get_Token()
extern void JsonReader_get_Token_mDC6D348B59ADDD2A98EC43E990DA35128817E694 (void);
// 0x000000B9 System.Object ThirdParty.Json.LitJson.JsonReader::get_Value()
extern void JsonReader_get_Value_m40B584C292D15A740F5305565CF463A9F6FDC08C (void);
// 0x000000BA System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.String)
extern void JsonReader__ctor_m4930899FDB533A49D0727E72ADA8A821A7174A8A (void);
// 0x000000BB System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.IO.TextReader)
extern void JsonReader__ctor_m8975A07F4B42FBECC211DB9D205A980F3864234F (void);
// 0x000000BC System.Void ThirdParty.Json.LitJson.JsonReader::.ctor(System.IO.TextReader,System.Boolean)
extern void JsonReader__ctor_mA92A67018D2730E591D98ED0C3B0FBABCFDCEA91 (void);
// 0x000000BD System.Void ThirdParty.Json.LitJson.JsonReader::ProcessNumber(System.String)
extern void JsonReader_ProcessNumber_mB5B2DC7C8F5B98D63A8C2856FD153E0FF5D78F3D (void);
// 0x000000BE System.Void ThirdParty.Json.LitJson.JsonReader::ProcessSymbol()
extern void JsonReader_ProcessSymbol_m9AABBF0B918EB9DF5E1BEA52EDCCE9DA803B3C17 (void);
// 0x000000BF System.Boolean ThirdParty.Json.LitJson.JsonReader::ReadToken()
extern void JsonReader_ReadToken_mF5095828E4AD13219B5184DAB40DEAB964FEAA47 (void);
// 0x000000C0 System.Void ThirdParty.Json.LitJson.JsonReader::Close()
extern void JsonReader_Close_mA49AE14270AD0CB3A01604F9D86D645923BE26FF (void);
// 0x000000C1 System.Boolean ThirdParty.Json.LitJson.JsonReader::Read()
extern void JsonReader_Read_m3B29CE85487560850A62C72849831E3223BC24E3 (void);
// 0x000000C2 System.Void ThirdParty.Json.LitJson.WriterContext::.ctor()
extern void WriterContext__ctor_mDB04C45F94CFD3378EB5AF77BB8CF07AC0016E42 (void);
// 0x000000C3 System.IO.TextWriter ThirdParty.Json.LitJson.JsonWriter::get_TextWriter()
extern void JsonWriter_get_TextWriter_m3B0CD8D100F5FC45F23EE48C9DE7984221DB4FBE (void);
// 0x000000C4 System.Boolean ThirdParty.Json.LitJson.JsonWriter::get_Validate()
extern void JsonWriter_get_Validate_m39DDCBB4EF53B79558375768981E9F287ACBF983 (void);
// 0x000000C5 System.Void ThirdParty.Json.LitJson.JsonWriter::set_Validate(System.Boolean)
extern void JsonWriter_set_Validate_mABDAB76B0EEDAE8C5A855CC4C406D233513C482D (void);
// 0x000000C6 System.Void ThirdParty.Json.LitJson.JsonWriter::.cctor()
extern void JsonWriter__cctor_m73586093EC9DADDFCB5A43578B9E4D39CC2F8A91 (void);
// 0x000000C7 System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor()
extern void JsonWriter__ctor_mC89F025DB721BAF8C9AF5DC458111401F2037B29 (void);
// 0x000000C8 System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.Text.StringBuilder)
extern void JsonWriter__ctor_mBDA08DAEF878E5223483D0AB61C9F1449EA435D2 (void);
// 0x000000C9 System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.IO.TextWriter)
extern void JsonWriter__ctor_m5ED4B5D93FAD7484093D38FFF40DB5EAD705F0A5 (void);
// 0x000000CA System.Void ThirdParty.Json.LitJson.JsonWriter::DoValidation(ThirdParty.Json.LitJson.Condition)
extern void JsonWriter_DoValidation_mF1EFBCD5D3075E48B693CC47D6B28AAF3BA32891 (void);
// 0x000000CB System.Void ThirdParty.Json.LitJson.JsonWriter::Init()
extern void JsonWriter_Init_m7DEAD93F35483DD8295384E4EC59518C5E76CB49 (void);
// 0x000000CC System.Void ThirdParty.Json.LitJson.JsonWriter::IntToHex(System.Int32,System.Char[])
extern void JsonWriter_IntToHex_m1E3AE7A4927E90F19634899D52B915EE5AC993BE (void);
// 0x000000CD System.Void ThirdParty.Json.LitJson.JsonWriter::Indent()
extern void JsonWriter_Indent_mEE145732A5897943B8D4238673F2FE490BA5D539 (void);
// 0x000000CE System.Void ThirdParty.Json.LitJson.JsonWriter::Put(System.String)
extern void JsonWriter_Put_m50B5F196172094249CB3D0ADB3A42FA6C90EA774 (void);
// 0x000000CF System.Void ThirdParty.Json.LitJson.JsonWriter::PutNewline()
extern void JsonWriter_PutNewline_mBDA8FCFDFF049FEB70A09DD8F553E490336E7AEB (void);
// 0x000000D0 System.Void ThirdParty.Json.LitJson.JsonWriter::PutNewline(System.Boolean)
extern void JsonWriter_PutNewline_m8AA28D5D50DB0E058FE55EDACE4DC1E6A10CDFB4 (void);
// 0x000000D1 System.Void ThirdParty.Json.LitJson.JsonWriter::PutString(System.String)
extern void JsonWriter_PutString_mBB1EB05F0B758BE8A2CBC78C51A3C0FB9577F092 (void);
// 0x000000D2 System.Void ThirdParty.Json.LitJson.JsonWriter::Unindent()
extern void JsonWriter_Unindent_mDEF47B66501CBE3468100F8F7A16BE47FD9FDDD7 (void);
// 0x000000D3 System.String ThirdParty.Json.LitJson.JsonWriter::ToString()
extern void JsonWriter_ToString_mB8AA9F4B7FB6605A5C7C133C02C8FBE753F74315 (void);
// 0x000000D4 System.Void ThirdParty.Json.LitJson.JsonWriter::Reset()
extern void JsonWriter_Reset_m6C1E9090D6DDA4707A1B49E19B1323DEAAD58EFC (void);
// 0x000000D5 System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Boolean)
extern void JsonWriter_Write_m3DD35A09B85541A87CE43AA2A3D0EF2D17DA6D22 (void);
// 0x000000D6 System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Decimal)
extern void JsonWriter_Write_m5D3E11D3563E3BBE14C7F62DE18E902B0CB0A671 (void);
// 0x000000D7 System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Double)
extern void JsonWriter_Write_m53E153F56EE0CAE5F935851DDAE55BBA1D221D93 (void);
// 0x000000D8 System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Int32)
extern void JsonWriter_Write_m3543CE25D30150FFED40FE4B1A0ED96855814B90 (void);
// 0x000000D9 System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.UInt32)
extern void JsonWriter_Write_m8EB022145303FFA0142FEE32FEA3C57DD90009E5 (void);
// 0x000000DA System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.Int64)
extern void JsonWriter_Write_mDB6EE13037A3ED7F214FCDC5EAACF7BE8C3204C9 (void);
// 0x000000DB System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.String)
extern void JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B (void);
// 0x000000DC System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.UInt64)
extern void JsonWriter_Write_mC5058DAFD1A17F59EF9C406C354D29808D3BEA4B (void);
// 0x000000DD System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayEnd()
extern void JsonWriter_WriteArrayEnd_m5B07BFF16123F3B2D07899B10B056F8DAD9758C6 (void);
// 0x000000DE System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayStart()
extern void JsonWriter_WriteArrayStart_mB1FE97CA88553F8E58669D8438BABEBD586F98C9 (void);
// 0x000000DF System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectEnd()
extern void JsonWriter_WriteObjectEnd_m127A9318E08F34226D40487B6D833F540B86A679 (void);
// 0x000000E0 System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectStart()
extern void JsonWriter_WriteObjectStart_m385A8272F17BCB16C9F994381B6BADCE1CFD6A6A (void);
// 0x000000E1 System.Void ThirdParty.Json.LitJson.JsonWriter::WritePropertyName(System.String)
extern void JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F (void);
// 0x000000E2 System.Void ThirdParty.Json.LitJson.FsmContext::.ctor()
extern void FsmContext__ctor_m2CA8508DF1A63875F7E9315B1F60AF3320E20373 (void);
// 0x000000E3 System.Boolean ThirdParty.Json.LitJson.Lexer::get_EndOfInput()
extern void Lexer_get_EndOfInput_mE3D2AC25B3A517770FE20377D01ABF3AE7E34313 (void);
// 0x000000E4 System.Int32 ThirdParty.Json.LitJson.Lexer::get_Token()
extern void Lexer_get_Token_m4A83BBBBD713C8840377E9EF76B0003106559431 (void);
// 0x000000E5 System.String ThirdParty.Json.LitJson.Lexer::get_StringValue()
extern void Lexer_get_StringValue_m7282CF61FB4BF23F3AB564FBB4FF763A84E3449F (void);
// 0x000000E6 System.Void ThirdParty.Json.LitJson.Lexer::.cctor()
extern void Lexer__cctor_m8026CFBC80EC6B79A1963F11C5226E8B85687DD3 (void);
// 0x000000E7 System.Void ThirdParty.Json.LitJson.Lexer::.ctor(System.IO.TextReader)
extern void Lexer__ctor_mDFA89019EB7B6A89FCD65DB608AE70B993D3CF70 (void);
// 0x000000E8 System.Int32 ThirdParty.Json.LitJson.Lexer::HexValue(System.Int32)
extern void Lexer_HexValue_m17E5475BDA450A9BD963F10B54E2F6D58149F8D9 (void);
// 0x000000E9 System.Void ThirdParty.Json.LitJson.Lexer::PopulateFsmTables()
extern void Lexer_PopulateFsmTables_mF32F6DB96A934E032025F1EB171BFA51D68B07FF (void);
// 0x000000EA System.Char ThirdParty.Json.LitJson.Lexer::ProcessEscChar(System.Int32)
extern void Lexer_ProcessEscChar_m801DF0CF016F4FF388B3B28459EFEFCBCB094DE3 (void);
// 0x000000EB System.Boolean ThirdParty.Json.LitJson.Lexer::State1(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State1_m04C4D5F9B2200D52DD57D6623C95163AE749627C (void);
// 0x000000EC System.Boolean ThirdParty.Json.LitJson.Lexer::State2(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State2_mFE6544971C20FB7EC5B3F7A37B009C4509B22528 (void);
// 0x000000ED System.Boolean ThirdParty.Json.LitJson.Lexer::State3(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State3_m956807D4969B408DE982CA63DBBF0B81E104857A (void);
// 0x000000EE System.Boolean ThirdParty.Json.LitJson.Lexer::State4(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State4_m16FEB4679942332962DCE69C481062A249FE8591 (void);
// 0x000000EF System.Boolean ThirdParty.Json.LitJson.Lexer::State5(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State5_m5CA39A23583D3BB06BE62BEDB5D9801086F94FE8 (void);
// 0x000000F0 System.Boolean ThirdParty.Json.LitJson.Lexer::State6(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State6_mC1BB562D862CE582B1367DC5D76BCFFAA59FB104 (void);
// 0x000000F1 System.Boolean ThirdParty.Json.LitJson.Lexer::State7(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State7_m2CDBF5AE90B892C7CED3CA83478D903AFDC5D611 (void);
// 0x000000F2 System.Boolean ThirdParty.Json.LitJson.Lexer::State8(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State8_m665E9647874B34DD74C9F860C158FDE059AF3F25 (void);
// 0x000000F3 System.Boolean ThirdParty.Json.LitJson.Lexer::State9(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State9_mE19F3AF9A5FB0DE45B22315F4F8025ECF1B92545 (void);
// 0x000000F4 System.Boolean ThirdParty.Json.LitJson.Lexer::State10(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State10_m622EB6FA43424C7E25ADFD3C822CBE0178AE9D99 (void);
// 0x000000F5 System.Boolean ThirdParty.Json.LitJson.Lexer::State11(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State11_m2EF6198F5C40F599126B8F13CF57AAFAC720039E (void);
// 0x000000F6 System.Boolean ThirdParty.Json.LitJson.Lexer::State12(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State12_m8DAA8A83D0A7DD86F23D664D8318FED1A8A06423 (void);
// 0x000000F7 System.Boolean ThirdParty.Json.LitJson.Lexer::State13(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State13_m21D465032A22CBEA6F40499F5D02157273ABFC76 (void);
// 0x000000F8 System.Boolean ThirdParty.Json.LitJson.Lexer::State14(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State14_mC9246F0367D80001408BFC0FE4E4851AAC4F45F5 (void);
// 0x000000F9 System.Boolean ThirdParty.Json.LitJson.Lexer::State15(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State15_m6597BF5514B466EE3E0C853E61DC587380425DE0 (void);
// 0x000000FA System.Boolean ThirdParty.Json.LitJson.Lexer::State16(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State16_m8F17B258F0DC016F07436E9C02489CF9ABE7FB82 (void);
// 0x000000FB System.Boolean ThirdParty.Json.LitJson.Lexer::State17(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State17_mDB0CEF1FACC2A4259255F5272C8E9FB14A5748C2 (void);
// 0x000000FC System.Boolean ThirdParty.Json.LitJson.Lexer::State18(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State18_mC9357EBE956FC522FE9B082F7D05CDE8516E08B2 (void);
// 0x000000FD System.Boolean ThirdParty.Json.LitJson.Lexer::State19(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State19_mBA15B867A90E9F20DC84FCA5826674C221F20342 (void);
// 0x000000FE System.Boolean ThirdParty.Json.LitJson.Lexer::State20(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State20_mEEFA027107B256314483E1A90D604B4527E11197 (void);
// 0x000000FF System.Boolean ThirdParty.Json.LitJson.Lexer::State21(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State21_mAEC7C0DD1E280F050F7ADFD32D779D14E795BA3A (void);
// 0x00000100 System.Boolean ThirdParty.Json.LitJson.Lexer::State22(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State22_m7AC6AE2C616DC46C6943C2401EE62ED05B734911 (void);
// 0x00000101 System.Boolean ThirdParty.Json.LitJson.Lexer::State23(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State23_m26C822CA740A53810D10801E269819F58CEA1459 (void);
// 0x00000102 System.Boolean ThirdParty.Json.LitJson.Lexer::State24(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State24_m5D6C4DE5AB75CF790316616E9ED98FF94940303F (void);
// 0x00000103 System.Boolean ThirdParty.Json.LitJson.Lexer::State25(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State25_m1DDE3EC503297F16AB8B78C8549716AFE8C8648B (void);
// 0x00000104 System.Boolean ThirdParty.Json.LitJson.Lexer::State26(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State26_m2521D5E365C21AF1401752A005E42B9929F3C7B4 (void);
// 0x00000105 System.Boolean ThirdParty.Json.LitJson.Lexer::State27(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State27_mC017DD08AF7644DBCF20F9C40C9992BED9FB3F53 (void);
// 0x00000106 System.Boolean ThirdParty.Json.LitJson.Lexer::State28(ThirdParty.Json.LitJson.FsmContext)
extern void Lexer_State28_m508A9BC52B0CB427E5647A4ACDA330A8A886CFC8 (void);
// 0x00000107 System.Boolean ThirdParty.Json.LitJson.Lexer::GetChar()
extern void Lexer_GetChar_mB40CF3CB1C9FB312CE3C731FB0BB7EC12F6A39EB (void);
// 0x00000108 System.Int32 ThirdParty.Json.LitJson.Lexer::NextChar()
extern void Lexer_NextChar_m1CFFA57D4031525FC5CA338D252B50269C31F166 (void);
// 0x00000109 System.Boolean ThirdParty.Json.LitJson.Lexer::NextToken()
extern void Lexer_NextToken_m2E950B886E97AE557217862E2566F81CCD1FC105 (void);
// 0x0000010A System.Void ThirdParty.Json.LitJson.Lexer::UngetChar()
extern void Lexer_UngetChar_m27545FEA0B82E0AF5C2C193613C7EBFAA948CA87 (void);
// 0x0000010B System.Void ThirdParty.Json.LitJson.Lexer/StateHandler::.ctor(System.Object,System.IntPtr)
extern void StateHandler__ctor_m4FCCA7028A029553722A6D5B3FA356FDCD2E3495 (void);
// 0x0000010C System.Boolean ThirdParty.Json.LitJson.Lexer/StateHandler::Invoke(ThirdParty.Json.LitJson.FsmContext)
extern void StateHandler_Invoke_mE436A82A2103774B11AF7B6EDCA8682903C0A8F7 (void);
// 0x0000010D System.IAsyncResult ThirdParty.Json.LitJson.Lexer/StateHandler::BeginInvoke(ThirdParty.Json.LitJson.FsmContext,System.AsyncCallback,System.Object)
extern void StateHandler_BeginInvoke_mA6A4E54F83FA7B6B51E87E6E630659D6D79CF3AF (void);
// 0x0000010E System.Boolean ThirdParty.Json.LitJson.Lexer/StateHandler::EndInvoke(System.IAsyncResult)
extern void StateHandler_EndInvoke_mFBC00C6248F391903E4EA805C18624972A400335 (void);
// 0x0000010F System.Int64 ThirdParty.Ionic.Zlib.CRC32::get_TotalBytesRead()
extern void CRC32_get_TotalBytesRead_mD4256BDDF15109AA5BB3A92C01E7901BA20F4664 (void);
// 0x00000110 System.Int32 ThirdParty.Ionic.Zlib.CRC32::get_Crc32Result()
extern void CRC32_get_Crc32Result_m5C34C5A39CDA6D4D49C0134ECFF8CA5F2864D4F1 (void);
// 0x00000111 System.Void ThirdParty.Ionic.Zlib.CRC32::SlurpBlock(System.Byte[],System.Int32,System.Int32)
extern void CRC32_SlurpBlock_m88E491281C2F632C946323D48D8CB51903524051 (void);
// 0x00000112 System.Void ThirdParty.Ionic.Zlib.CRC32::.cctor()
extern void CRC32__cctor_m1C0A446866F0E8B165B312D54328BB6FBEB5760E (void);
// 0x00000113 System.Void ThirdParty.Ionic.Zlib.CRC32::.ctor()
extern void CRC32__ctor_m76575964AE07E44C7F93F6B6EE4CA3AC060238BD (void);
// 0x00000114 System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::.ctor(System.IO.Stream,System.Int64)
extern void CrcCalculatorStream__ctor_mEB7F08CF49E79830BE2D5D8C1B6208D7B112E047 (void);
// 0x00000115 System.Int32 ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_Crc32()
extern void CrcCalculatorStream_get_Crc32_mDB69F43A67470B477368C4D49639E2E77CE07505 (void);
// 0x00000116 System.Int32 ThirdParty.Ionic.Zlib.CrcCalculatorStream::Read(System.Byte[],System.Int32,System.Int32)
extern void CrcCalculatorStream_Read_mA62379E9EBB57BD3C029952843133372DF8FD9BD (void);
// 0x00000117 System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::Write(System.Byte[],System.Int32,System.Int32)
extern void CrcCalculatorStream_Write_mFA4D550FEC03AF3B378DE863AA571D227E1892F7 (void);
// 0x00000118 System.Boolean ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_CanRead()
extern void CrcCalculatorStream_get_CanRead_mE1F9A4CF15B24EA4282C6CED59A6B6C7EB07C20C (void);
// 0x00000119 System.Boolean ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_CanSeek()
extern void CrcCalculatorStream_get_CanSeek_m2754BD935F8271ADACA978448B6464519A8A1F84 (void);
// 0x0000011A System.Boolean ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_CanWrite()
extern void CrcCalculatorStream_get_CanWrite_m5D2D28FD0EAF7FA8F00C59C534C5C2C98E9B1950 (void);
// 0x0000011B System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::Flush()
extern void CrcCalculatorStream_Flush_m5D8BBBEC92AD0CFCB99BF174B8E0131AACA04823 (void);
// 0x0000011C System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_Length()
extern void CrcCalculatorStream_get_Length_mF4E6BFC137356ABE7D49D85C0FF89A86626A703F (void);
// 0x0000011D System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::get_Position()
extern void CrcCalculatorStream_get_Position_m716EBD855E62DBD12B646088737378372A314538 (void);
// 0x0000011E System.Void ThirdParty.Ionic.Zlib.CrcCalculatorStream::set_Position(System.Int64)
extern void CrcCalculatorStream_set_Position_m355E4C6F95477AA43672FCCB2B4A9A9F11DEF22F (void);
// 0x0000011F System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void CrcCalculatorStream_Seek_mAB455D6A9959E1AB74277DEBF80724B09972FAB4 (void);
// 0x00000120 System.Nullable`1<System.Net.HttpStatusCode> AWSSDK.Runtime.Internal.Util.ExceptionUtils::DetermineHttpStatusCode(System.Exception)
extern void ExceptionUtils_DetermineHttpStatusCode_m832ED3790938A17CC598D1DF1920571165868D59 (void);
// 0x00000121 System.Boolean AWSSDK.Runtime.Internal.Util.ExceptionUtils::IsInnerException(System.Exception)
// 0x00000122 System.Boolean AWSSDK.Runtime.Internal.Util.ExceptionUtils::IsInnerException(System.Exception,T&)
// 0x00000123 System.Nullable`1<System.TimeSpan> Amazon.AWSConfigs::get_ManualClockCorrection()
extern void AWSConfigs_get_ManualClockCorrection_m805F870491B9406F153304933920A3D73DCA954E (void);
// 0x00000124 System.Boolean Amazon.AWSConfigs::get_CorrectForClockSkew()
extern void AWSConfigs_get_CorrectForClockSkew_m7FD93ED74C4CD59924C52A386BCD10636A80643B (void);
// 0x00000125 System.Void Amazon.AWSConfigs::set_ClockOffset(System.TimeSpan)
extern void AWSConfigs_set_ClockOffset_mA16DF92F5F2A10BD24A5D52C01D81ED888A868FB (void);
// 0x00000126 System.String Amazon.AWSConfigs::get_AWSProfileName()
extern void AWSConfigs_get_AWSProfileName_mD4C6B99D9EBF6246CDA20AE59A0557FF249E1DFE (void);
// 0x00000127 System.String Amazon.AWSConfigs::get_AWSProfilesLocation()
extern void AWSConfigs_get_AWSProfilesLocation_mD3E2F6496CC61CD2BB20667B5BB590C9C1420B16 (void);
// 0x00000128 Amazon.LoggingOptions Amazon.AWSConfigs::GetLoggingSetting()
extern void AWSConfigs_GetLoggingSetting_m1148B8C480C000674AD9C070A367F98E54FFBC1E (void);
// 0x00000129 System.String Amazon.AWSConfigs::get_EndpointDefinition()
extern void AWSConfigs_get_EndpointDefinition_mA88054E4EF572298E8185A0C989E057FDC507681 (void);
// 0x0000012A Amazon.Util.LoggingConfig Amazon.AWSConfigs::get_LoggingConfig()
extern void AWSConfigs_get_LoggingConfig_mB1F7486EDC8D8B5740E04672183D7324A7FCA2A5 (void);
// 0x0000012B Amazon.Util.ProxyConfig Amazon.AWSConfigs::get_ProxyConfig()
extern void AWSConfigs_get_ProxyConfig_mC04D7E8D83182605B86B37C968B9E3961424ABBC (void);
// 0x0000012C System.Boolean Amazon.AWSConfigs::get_UseAlternateUserAgentHeader()
extern void AWSConfigs_get_UseAlternateUserAgentHeader_mA2003DCCC278378A98BF9862B4C5293F583F069B (void);
// 0x0000012D Amazon.RegionEndpoint Amazon.AWSConfigs::get_RegionEndpoint()
extern void AWSConfigs_get_RegionEndpoint_m126A32D8D2E72BE9A9B6C9C180ADEA423477D343 (void);
// 0x0000012E Amazon.Util.CSMConfig Amazon.AWSConfigs::get_CSMConfig()
extern void AWSConfigs_get_CSMConfig_mD4499CA01FDE37D7494EC4EE6BC1CF33F33183AB (void);
// 0x0000012F System.Void Amazon.AWSConfigs::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void AWSConfigs_add_PropertyChanged_mCB076570A47E9CECD0F4D4244A2074283792D481 (void);
// 0x00000130 System.Void Amazon.AWSConfigs::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void AWSConfigs_remove_PropertyChanged_m7A1BA5DF145567443804B785912FB1249FFFE903 (void);
// 0x00000131 System.Void Amazon.AWSConfigs::OnPropertyChanged(System.String)
extern void AWSConfigs_OnPropertyChanged_m3A46404400E79BFB2BAEEBD7CBBE1A58DDFA0866 (void);
// 0x00000132 System.Boolean Amazon.AWSConfigs::GetConfigBool(System.String,System.Boolean)
extern void AWSConfigs_GetConfigBool_mE49A29837AA84496ACFB2D01B6199F599E801C96 (void);
// 0x00000133 T Amazon.AWSConfigs::GetConfigEnum(System.String)
// 0x00000134 T Amazon.AWSConfigs::ParseEnum(System.String)
// 0x00000135 System.Boolean Amazon.AWSConfigs::TryParseEnum(System.String,T&)
// 0x00000136 System.DateTime Amazon.AWSConfigs::GetUtcNow()
extern void AWSConfigs_GetUtcNow_m5401E987A9C17AF9A354DF12E49A928F7A6FE820 (void);
// 0x00000137 System.String Amazon.AWSConfigs::GetConfig(System.String)
extern void AWSConfigs_GetConfig_mD3E3F762D75CCC9C2755CF701732A61C41B8F6E2 (void);
// 0x00000138 System.Boolean Amazon.AWSConfigs::XmlSectionExists(System.String)
extern void AWSConfigs_XmlSectionExists_mCEB187ED1044185D797E506A4DF868BFED321FED (void);
// 0x00000139 Amazon.Runtime.HttpClientFactory Amazon.AWSConfigs::get_HttpClientFactory()
extern void AWSConfigs_get_HttpClientFactory_m78229ECA292AF41D8A1D1DC094725778383DF561 (void);
// 0x0000013A System.Diagnostics.TraceListener[] Amazon.AWSConfigs::TraceListeners(System.String)
extern void AWSConfigs_TraceListeners_m0BBE4821EE0A31935246512C35E1BAFA61EA0E5E (void);
// 0x0000013B System.Void Amazon.AWSConfigs::.cctor()
extern void AWSConfigs__cctor_m60C9F1ABF3DEAE0B1738C68E6E0089DDC34F81F5 (void);
// 0x0000013C Amazon.RegionEndpoint Amazon.RegionEndpoint::GetBySystemName(System.String)
extern void RegionEndpoint_GetBySystemName_mA6FECE8FE84FC5767B0CD1412F83F55853A3C23C (void);
// 0x0000013D Amazon.RegionEndpoint Amazon.RegionEndpoint::GetRegionEndpointOverride(Amazon.RegionEndpoint)
extern void RegionEndpoint_GetRegionEndpointOverride_m3894BA13424B9DBD3D819615F73A947F9ACF3D1C (void);
// 0x0000013E Amazon.RegionEndpoint Amazon.RegionEndpoint::GetEndpoint(System.String,System.String)
extern void RegionEndpoint_GetEndpoint_m2C77EE8B62F2971AD60F0E72D4E4AC013051D8B5 (void);
// 0x0000013F Amazon.Internal.IRegionEndpointProvider Amazon.RegionEndpoint::get_RegionEndpointProvider()
extern void RegionEndpoint_get_RegionEndpointProvider_mF4A33FC2FF01C62098F3DA25C093119A3E30A29B (void);
// 0x00000140 System.Void Amazon.RegionEndpoint::.ctor(System.String,System.String)
extern void RegionEndpoint__ctor_mD6BA45FE515A43C06404B9650B094A3C603B0DC2 (void);
// 0x00000141 System.String Amazon.RegionEndpoint::get_SystemName()
extern void RegionEndpoint_get_SystemName_mCA2FF58E84D00596AC3250F797AD2AC3B783FF9D (void);
// 0x00000142 System.Void Amazon.RegionEndpoint::set_SystemName(System.String)
extern void RegionEndpoint_set_SystemName_m98B17B4109A3DFB6C723FC34D060297FA1ECBBB6 (void);
// 0x00000143 System.String Amazon.RegionEndpoint::get_DisplayName()
extern void RegionEndpoint_get_DisplayName_m9F7BF40A023CC420766759C7643189309742A812 (void);
// 0x00000144 System.Void Amazon.RegionEndpoint::set_DisplayName(System.String)
extern void RegionEndpoint_set_DisplayName_m7DDC78786C489CAC929012EF3FA130015572EF42 (void);
// 0x00000145 Amazon.Internal.IRegionEndpoint Amazon.RegionEndpoint::get_InternedRegionEndpoint()
extern void RegionEndpoint_get_InternedRegionEndpoint_m7B7F506B503AE5119F9770B7BEA85D1E57FDD084 (void);
// 0x00000146 Amazon.RegionEndpoint/Endpoint Amazon.RegionEndpoint::GetEndpointForService(System.String,System.Boolean)
extern void RegionEndpoint_GetEndpointForService_mF3283610EE740039B5D0CFFC7CAA408BA2BEF440 (void);
// 0x00000147 System.String Amazon.RegionEndpoint::ToString()
extern void RegionEndpoint_ToString_mF713241471D8370FBC2527AEF4FB73E9296742DC (void);
// 0x00000148 System.Void Amazon.RegionEndpoint::.cctor()
extern void RegionEndpoint__cctor_m1B1FC5C7EDDFF39FDDD15AA4D49E1BB9FEE72C95 (void);
// 0x00000149 System.Void Amazon.RegionEndpoint/Endpoint::.ctor(System.String,System.String,System.String)
extern void Endpoint__ctor_mB3E50CA94CDFA257DA7E2EB77504CA4D59485D3B (void);
// 0x0000014A System.String Amazon.RegionEndpoint/Endpoint::get_Hostname()
extern void Endpoint_get_Hostname_m68CD8A74E6E7DB12FC54D991176DC12D6A0C4925 (void);
// 0x0000014B System.Void Amazon.RegionEndpoint/Endpoint::set_Hostname(System.String)
extern void Endpoint_set_Hostname_m25D4F046B53E36CA01DE38ED67B854336395EB0B (void);
// 0x0000014C System.String Amazon.RegionEndpoint/Endpoint::get_AuthRegion()
extern void Endpoint_get_AuthRegion_m342C6FEF878A06504E6822261A6752BF328879AD (void);
// 0x0000014D System.Void Amazon.RegionEndpoint/Endpoint::set_AuthRegion(System.String)
extern void Endpoint_set_AuthRegion_m5ABD1D4B3686C2C959AB1FCFF0120D66CBDA2554 (void);
// 0x0000014E System.String Amazon.RegionEndpoint/Endpoint::ToString()
extern void Endpoint_ToString_mD538AA4F9B47798139CA9C6B95E7EDD975558077 (void);
// 0x0000014F System.Void Amazon.RegionEndpoint/Endpoint::set_SignatureVersionOverride(System.String)
extern void Endpoint_set_SignatureVersionOverride_m67F109FA152FA50029D59E3019A642326EE6C3A4 (void);
// 0x00000150 System.String Amazon.Internal.IRegionEndpoint::get_DisplayName()
// 0x00000151 Amazon.RegionEndpoint/Endpoint Amazon.Internal.IRegionEndpoint::GetEndpointForService(System.String,System.Boolean)
// 0x00000152 Amazon.Internal.IRegionEndpoint Amazon.Internal.IRegionEndpointProvider::GetRegionEndpoint(System.String)
// 0x00000153 System.Net.IWebProxy Amazon.Internal.RegionEndpointProviderV2::get_Proxy()
extern void RegionEndpointProviderV2_get_Proxy_mF94D500F8D838A0A41E557F8F78E85E1881CBBE7 (void);
// 0x00000154 Amazon.Internal.IRegionEndpoint Amazon.Internal.RegionEndpointProviderV2::GetRegionEndpoint(System.String)
extern void RegionEndpointProviderV2_GetRegionEndpoint_mF4F1BCE89EE0A1BD2E58F13E89CAD647152B0485 (void);
// 0x00000155 System.Void Amazon.Internal.RegionEndpointProviderV2::.ctor()
extern void RegionEndpointProviderV2__ctor_m3CC64AE924D07D3BC151DB3DE54155F33C0F7BE7 (void);
// 0x00000156 Amazon.RegionEndpoint/Endpoint Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetEndpointForService(System.String,System.Boolean)
extern void RegionEndpoint_GetEndpointForService_mA9D111C60C0BA69854A8967D30EBDBFCACDAAF3E (void);
// 0x00000157 ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetEndpointRule(System.String)
extern void RegionEndpoint_GetEndpointRule_mFC69A0B57CA3F3EA12DADAD44B2DED89A5197BA7 (void);
// 0x00000158 Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetEndpoint(System.String,System.String)
extern void RegionEndpoint_GetEndpoint_mABB94426F48F4A256C5B27A378BC8DDAE15F8A31 (void);
// 0x00000159 Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::GetBySystemName(System.String)
extern void RegionEndpoint_GetBySystemName_mB727B55DE528F073D2C346A95CDB594141148783 (void);
// 0x0000015A System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitions()
extern void RegionEndpoint_LoadEndpointDefinitions_m96D9ECAB3D65F2F4173FB8915CBA0B0BB0612DE8 (void);
// 0x0000015B System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitions(System.String)
extern void RegionEndpoint_LoadEndpointDefinitions_m8B7B15E3C44F1A745B56F4FEB1A40C209EE7F3BD (void);
// 0x0000015C System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::ReadEndpointFile(System.IO.Stream)
extern void RegionEndpoint_ReadEndpointFile_m3A6DE81E2CCF4055EE27EEF58201CACD02DC5F5E (void);
// 0x0000015D System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitionsFromEmbeddedResource()
extern void RegionEndpoint_LoadEndpointDefinitionsFromEmbeddedResource_m15B6E2A5A743FF27829AFC34A8899B9297AB71E1 (void);
// 0x0000015E System.Boolean Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::TryLoadEndpointDefinitionsFromAssemblyDir()
extern void RegionEndpoint_TryLoadEndpointDefinitionsFromAssemblyDir_m71D0F68FEEF275478D9C62C92BE6051C83D4929B (void);
// 0x0000015F System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitionFromFilePath(System.String)
extern void RegionEndpoint_LoadEndpointDefinitionFromFilePath_m0F2C0AE8212179A8A2CE5C1234CCC2094C7A325B (void);
// 0x00000160 System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LoadEndpointDefinitionFromWeb(System.String)
extern void RegionEndpoint_LoadEndpointDefinitionFromWeb_m46EB57E8492CA7C57D956077345B067557C6CCB7 (void);
// 0x00000161 System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::.ctor(System.String,System.String)
extern void RegionEndpoint__ctor_mC91B8062C4B6AF72673C2050CB77E1F65313D75A (void);
// 0x00000162 System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::get_SystemName()
extern void RegionEndpoint_get_SystemName_mA927AC85ACE3E746C6BCEE3F85AFA08491345AB5 (void);
// 0x00000163 System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::set_SystemName(System.String)
extern void RegionEndpoint_set_SystemName_mE32DA2C28922213E460BF7B162452D653DA98E6F (void);
// 0x00000164 System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::get_DisplayName()
extern void RegionEndpoint_get_DisplayName_m8CBFE99744D4B800EE260CE6E770C884892ED534 (void);
// 0x00000165 System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::set_DisplayName(System.String)
extern void RegionEndpoint_set_DisplayName_mDDFA9C6B58D6D1331F5739BD546588A6B8E18B16 (void);
// 0x00000166 System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::ToString()
extern void RegionEndpoint_ToString_m01D01303029AF41AF201F350DBED9FBEFCAD89D8 (void);
// 0x00000167 System.Void Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::.cctor()
extern void RegionEndpoint__cctor_m2C373CBC77F6556CEDD011B8624BD2ED389B91D0 (void);
// 0x00000168 System.String Amazon.Internal.RegionEndpointV3::get_RegionName()
extern void RegionEndpointV3_get_RegionName_mB7CEB1A08A15E5A094FB058980751AD56B028974 (void);
// 0x00000169 System.Void Amazon.Internal.RegionEndpointV3::set_RegionName(System.String)
extern void RegionEndpointV3_set_RegionName_mBC114B5247B46D3F873AA93606B19883D91C6EFA (void);
// 0x0000016A System.String Amazon.Internal.RegionEndpointV3::get_DisplayName()
extern void RegionEndpointV3_get_DisplayName_mD3913EEEF925BD31703A43466C887E014E98A673 (void);
// 0x0000016B System.Void Amazon.Internal.RegionEndpointV3::set_DisplayName(System.String)
extern void RegionEndpointV3_set_DisplayName_m81A67F18FA5E4D9529CFD7D6773142BCC5F9F55A (void);
// 0x0000016C System.Void Amazon.Internal.RegionEndpointV3::.ctor(System.String,System.String,ThirdParty.Json.LitJson.JsonData,ThirdParty.Json.LitJson.JsonData)
extern void RegionEndpointV3__ctor_mF380410DF9CEA1966337B78CD5B05784D56937A4 (void);
// 0x0000016D Amazon.RegionEndpoint/Endpoint Amazon.Internal.RegionEndpointV3::GetEndpointForService(System.String,System.Boolean)
extern void RegionEndpointV3_GetEndpointForService_m782EC58A7A5A345E29761CDCEFB82835AF6A87A4 (void);
// 0x0000016E Amazon.RegionEndpoint/Endpoint Amazon.Internal.RegionEndpointV3::CreateUnknownEndpoint(System.String,System.Boolean)
extern void RegionEndpointV3_CreateUnknownEndpoint_m4F444528E9BBBBC118C19C011ECC156E8A060B77 (void);
// 0x0000016F System.Void Amazon.Internal.RegionEndpointV3::ParseAllServices()
extern void RegionEndpointV3_ParseAllServices_mF03BA09DE1E6B21748CD3EC9C185EA5D2D6B90A3 (void);
// 0x00000170 System.Void Amazon.Internal.RegionEndpointV3::AddServiceToMap(ThirdParty.Json.LitJson.JsonData,System.String)
extern void RegionEndpointV3_AddServiceToMap_m1E1A63BBB84733F80C38547FBEF98225E1016386 (void);
// 0x00000171 System.Void Amazon.Internal.RegionEndpointV3::MergeJsonData(ThirdParty.Json.LitJson.JsonData,ThirdParty.Json.LitJson.JsonData)
extern void RegionEndpointV3_MergeJsonData_m465323F7CDFFA8A5A012DED8F1D302A70F0437F0 (void);
// 0x00000172 System.Void Amazon.Internal.RegionEndpointV3::CreateEndpointAndAddToServiceMap(ThirdParty.Json.LitJson.JsonData,System.String,System.String)
extern void RegionEndpointV3_CreateEndpointAndAddToServiceMap_m8BB4C3A21B343D14A732B157A342D8E231BDDD29 (void);
// 0x00000173 System.Void Amazon.Internal.RegionEndpointV3::CreateEndpointAndAddToServiceMap(ThirdParty.Json.LitJson.JsonData,System.String,System.String,System.Boolean)
extern void RegionEndpointV3_CreateEndpointAndAddToServiceMap_m998113EB002B7CB3AD2459786F9356396774C4E9 (void);
// 0x00000174 System.String Amazon.Internal.RegionEndpointV3::DetermineSignatureOverride(ThirdParty.Json.LitJson.JsonData,System.String)
extern void RegionEndpointV3_DetermineSignatureOverride_m810D04421EC536935F10CB456EB9A75EBC6E45BE (void);
// 0x00000175 System.String Amazon.Internal.RegionEndpointV3::DetermineAuthRegion(ThirdParty.Json.LitJson.JsonData)
extern void RegionEndpointV3_DetermineAuthRegion_mF187F53CFE4E35F39719CC36A95E4489211DA105 (void);
// 0x00000176 System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint> Amazon.Internal.RegionEndpointV3/ServiceMap::GetMap(System.Boolean)
extern void ServiceMap_GetMap_mA9C35137A79E922509AAC730C45096A31F598435 (void);
// 0x00000177 System.Boolean Amazon.Internal.RegionEndpointV3/ServiceMap::ContainsKey(System.String)
extern void ServiceMap_ContainsKey_m8993B60BF67FC0699BC197A7C2CFC6A022CAB2A9 (void);
// 0x00000178 System.Void Amazon.Internal.RegionEndpointV3/ServiceMap::Add(System.String,System.Boolean,Amazon.RegionEndpoint/Endpoint)
extern void ServiceMap_Add_m18F509BFF7C82668A2DF54259A4E19E62B07C266 (void);
// 0x00000179 System.Boolean Amazon.Internal.RegionEndpointV3/ServiceMap::TryGetEndpoint(System.String,System.Boolean,Amazon.RegionEndpoint/Endpoint&)
extern void ServiceMap_TryGetEndpoint_m80642BC1FB0618AB1A3F525B6EEEFDA615AF1D7C (void);
// 0x0000017A System.Void Amazon.Internal.RegionEndpointV3/ServiceMap::.ctor()
extern void ServiceMap__ctor_m9342C6CD7674E3A32B962274708B67A091E002E8 (void);
// 0x0000017B System.Void Amazon.Internal.RegionEndpointProviderV3::.ctor()
extern void RegionEndpointProviderV3__ctor_m4E4238A951719DDB1BFA479376C1123D1D0A3518 (void);
// 0x0000017C System.IO.Stream Amazon.Internal.RegionEndpointProviderV3::GetEndpointJsonSourceStream()
extern void RegionEndpointProviderV3_GetEndpointJsonSourceStream_m88FDC46A64728BBE302BEA6245CFBCFF07AA64C1 (void);
// 0x0000017D System.String Amazon.Internal.RegionEndpointProviderV3::GetUnknownRegionDescription(System.String)
extern void RegionEndpointProviderV3_GetUnknownRegionDescription_mF8388D54F8D3865CD4F818972B64568E29DE7113 (void);
// 0x0000017E System.Boolean Amazon.Internal.RegionEndpointProviderV3::IsRegionInPartition(System.String,ThirdParty.Json.LitJson.JsonData,System.String&)
extern void RegionEndpointProviderV3_IsRegionInPartition_m159457AA81674F8C4E9A62C9B2DBA6389727DF30 (void);
// 0x0000017F Amazon.Internal.IRegionEndpoint Amazon.Internal.RegionEndpointProviderV3::GetRegionEndpoint(System.String)
extern void RegionEndpointProviderV3_GetRegionEndpoint_m67099451DA46708CD8331DC88FB8F58F62806BFA (void);
// 0x00000180 Amazon.Internal.IRegionEndpoint Amazon.Internal.RegionEndpointProviderV3::GetNonstandardRegionEndpoint(System.String)
extern void RegionEndpointProviderV3_GetNonstandardRegionEndpoint_m3B5AE2A1910AC3A24B1CEDD786A0422F1D3C21E0 (void);
// 0x00000181 System.Void Amazon.Internal.RegionEndpointProviderV3::.cctor()
extern void RegionEndpointProviderV3__cctor_mE38D9B103AEEED843CE32437FC24792B81F5928F (void);
// 0x00000182 System.String Amazon.Util.ProxyConfig::get_Username()
extern void ProxyConfig_get_Username_m81955ADE534F7677180E2939A6C3D1AF0A2228FC (void);
// 0x00000183 System.String Amazon.Util.ProxyConfig::get_Password()
extern void ProxyConfig_get_Password_mD09CFD5FB9141132944E25757C50D5C421C20E51 (void);
// 0x00000184 System.Void Amazon.Util.ProxyConfig::.ctor()
extern void ProxyConfig__ctor_m8648DADF98050B1FAC492D4B1E454E55FD475488 (void);
// 0x00000185 Amazon.LoggingOptions Amazon.Util.LoggingConfig::get_LogTo()
extern void LoggingConfig_get_LogTo_mA85047D8D22C21160FAB8177971E9C376E84E80C (void);
// 0x00000186 System.Void Amazon.Util.LoggingConfig::set_LogTo(Amazon.LoggingOptions)
extern void LoggingConfig_set_LogTo_mEDF5F4A32C52E71206C36282D6A5FAA00B89C4A9 (void);
// 0x00000187 Amazon.ResponseLoggingOption Amazon.Util.LoggingConfig::get_LogResponses()
extern void LoggingConfig_get_LogResponses_m9F1FA413F3A9AF41C64AC4B219798FA570D18725 (void);
// 0x00000188 System.Void Amazon.Util.LoggingConfig::set_LogResponses(Amazon.ResponseLoggingOption)
extern void LoggingConfig_set_LogResponses_m0E16D7CDB69C7EEE2994914B6D34B24A62C898F3 (void);
// 0x00000189 System.Int32 Amazon.Util.LoggingConfig::get_LogResponsesSizeLimit()
extern void LoggingConfig_get_LogResponsesSizeLimit_mAEF0745D43816A42B5D80B1DE17380FBB14EAF16 (void);
// 0x0000018A System.Void Amazon.Util.LoggingConfig::set_LogResponsesSizeLimit(System.Int32)
extern void LoggingConfig_set_LogResponsesSizeLimit_m7E62A8B361B8EB37305DFFAF37FF0AF6F9D3D0B6 (void);
// 0x0000018B System.Boolean Amazon.Util.LoggingConfig::get_LogMetrics()
extern void LoggingConfig_get_LogMetrics_m47AF794FF34CA33B117B030A865695FD0FB42000 (void);
// 0x0000018C System.Void Amazon.Util.LoggingConfig::set_LogMetrics(System.Boolean)
extern void LoggingConfig_set_LogMetrics_mE4CFC322110C8D56EA52CECB88D69FA8BD560FE4 (void);
// 0x0000018D Amazon.LogMetricsFormatOption Amazon.Util.LoggingConfig::get_LogMetricsFormat()
extern void LoggingConfig_get_LogMetricsFormat_m5C2206F419BF88D2F401258F965407456E4DD30E (void);
// 0x0000018E Amazon.Runtime.IMetricsFormatter Amazon.Util.LoggingConfig::get_LogMetricsCustomFormatter()
extern void LoggingConfig_get_LogMetricsCustomFormatter_mEFF5F77802440902877DF50BC11721A69FFB2E24 (void);
// 0x0000018F System.Void Amazon.Util.LoggingConfig::.ctor()
extern void LoggingConfig__ctor_m124A902B7D0FBEA0E0D9D2D2AB865402B4F1A833 (void);
// 0x00000190 System.Void Amazon.Util.LoggingConfig::.cctor()
extern void LoggingConfig__cctor_m98A3722C696C7B78A44BFAAEDBEB931D0FF13692 (void);
// 0x00000191 System.String Amazon.Util.CSMConfig::get_CSMHost()
extern void CSMConfig_get_CSMHost_mCD6A4223BC46188E253784DD6811D36F1022314B (void);
// 0x00000192 System.Int32 Amazon.Util.CSMConfig::get_CSMPort()
extern void CSMConfig_get_CSMPort_mB95D7D19420DAAC48311A4EC703E1A6F34E0AC44 (void);
// 0x00000193 System.String Amazon.Util.CSMConfig::get_CSMClientId()
extern void CSMConfig_get_CSMClientId_m6994559572B87F47F0B6E55E46367E9F2490E8F7 (void);
// 0x00000194 System.Nullable`1<System.Boolean> Amazon.Util.CSMConfig::get_CSMEnabled()
extern void CSMConfig_get_CSMEnabled_m04F6AA3E68C62699639115DF7BC9FEA121AAB165 (void);
// 0x00000195 System.Void Amazon.Util.CSMConfig::.ctor()
extern void CSMConfig__ctor_m9CF53339FBC3651492BBD00AC79A5CBB5A24ADC1 (void);
// 0x00000196 System.String Amazon.Util.AWSSDKUtils::DetermineValidPathCharacters()
extern void AWSSDKUtils_DetermineValidPathCharacters_m87039C0E22F72198C04358C291E4719009D6000F (void);
// 0x00000197 System.String Amazon.Util.AWSSDKUtils::GetParametersAsString(Amazon.Runtime.Internal.IRequest)
extern void AWSSDKUtils_GetParametersAsString_mA817B78E680075B6FA6DB166BDA251311DCB7DED (void);
// 0x00000198 System.String Amazon.Util.AWSSDKUtils::GetParametersAsString(Amazon.Runtime.Internal.ParameterCollection)
extern void AWSSDKUtils_GetParametersAsString_m7F03BABD27CE1DF12845C29F1155111D69221C01 (void);
// 0x00000199 System.String Amazon.Util.AWSSDKUtils::CanonicalizeResourcePath(System.Uri,System.String,System.Boolean,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Int32)
extern void AWSSDKUtils_CanonicalizeResourcePath_m79BEAB577297145BC0CEA1E69DE47A6BAD9835D7 (void);
// 0x0000019A System.Collections.Generic.IEnumerable`1<System.String> Amazon.Util.AWSSDKUtils::SplitResourcePathIntoSegments(System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AWSSDKUtils_SplitResourcePathIntoSegments_m5B2A02550D2B22A233D7BC810D0FAB5ECF1CB4C1 (void);
// 0x0000019B System.String Amazon.Util.AWSSDKUtils::JoinResourcePathSegments(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void AWSSDKUtils_JoinResourcePathSegments_m824C05DB153DD2AAB927E98AD73564439C9D0C4D (void);
// 0x0000019C System.String Amazon.Util.AWSSDKUtils::ResolveResourcePath(System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AWSSDKUtils_ResolveResourcePath_mE9E964E126AC91AC9F9A96BD4FC5A92A49086936 (void);
// 0x0000019D System.String Amazon.Util.AWSSDKUtils::DetermineRegion(System.String)
extern void AWSSDKUtils_DetermineRegion_mC5F64BA18C180A7FCCDF749045E6891436E16F14 (void);
// 0x0000019E System.String Amazon.Util.AWSSDKUtils::DetermineService(System.String)
extern void AWSSDKUtils_DetermineService_mC6E388DF8284F1110FF45BCB0C4AE2A8687033E3 (void);
// 0x0000019F System.TimeSpan Amazon.Util.AWSSDKUtils::GetTimeSpanInTicks(System.DateTime)
extern void AWSSDKUtils_GetTimeSpanInTicks_m0CF57CB556B76A2ACD6BD29931AB286EE24ADB79 (void);
// 0x000001A0 System.Int64 Amazon.Util.AWSSDKUtils::ConvertDateTimetoMilliseconds(System.DateTime)
extern void AWSSDKUtils_ConvertDateTimetoMilliseconds_m41957A6CF1374E7F16A8BA453460254BF7FAFB74 (void);
// 0x000001A1 System.Int64 Amazon.Util.AWSSDKUtils::ConvertTimeSpanToMilliseconds(System.TimeSpan)
extern void AWSSDKUtils_ConvertTimeSpanToMilliseconds_mC6BC4968AC07422A848EC7F6E45CEB2EE9C9B6EA (void);
// 0x000001A2 System.String Amazon.Util.AWSSDKUtils::ToHex(System.Byte[],System.Boolean)
extern void AWSSDKUtils_ToHex_mCB2E490A5E54E9F0B6A237BCB2D2D6D1D739E1C9 (void);
// 0x000001A3 System.Void Amazon.Util.AWSSDKUtils::InvokeInBackground(System.EventHandler`1<T>,T,System.Object)
// 0x000001A4 Amazon.Runtime.Internal.Util.BackgroundInvoker Amazon.Util.AWSSDKUtils::get_Dispatcher()
extern void AWSSDKUtils_get_Dispatcher_m953348051D1C1BA80934260FA081C240DC367B30 (void);
// 0x000001A5 System.Boolean Amazon.Util.AWSSDKUtils::AreEqual(System.Object[],System.Object[])
extern void AWSSDKUtils_AreEqual_m126C2EF42BF063AA060D24284ECB2FB5AE0385CD (void);
// 0x000001A6 System.Boolean Amazon.Util.AWSSDKUtils::AreEqual(System.Object,System.Object)
extern void AWSSDKUtils_AreEqual_m4B639C0FA027C17F23CC93DB10278BA83016EDB8 (void);
// 0x000001A7 System.Boolean Amazon.Util.AWSSDKUtils::DictionariesAreEqual(System.Collections.Generic.Dictionary`2<K,V>,System.Collections.Generic.Dictionary`2<K,V>)
// 0x000001A8 System.String Amazon.Util.AWSSDKUtils::UrlEncode(System.String,System.Boolean)
extern void AWSSDKUtils_UrlEncode_mFF751215F3D5B019B6FAB326BDE9333A9A0BAECE (void);
// 0x000001A9 System.String Amazon.Util.AWSSDKUtils::UrlEncode(System.Int32,System.String,System.Boolean)
extern void AWSSDKUtils_UrlEncode_mBE8AAF67CADDE09C6C6B8591B8C0899C0F6B023A (void);
// 0x000001AA System.String Amazon.Util.AWSSDKUtils::ProtectEncodedSlashUrlEncode(System.String,System.Boolean)
extern void AWSSDKUtils_ProtectEncodedSlashUrlEncode_m565AB1506E33A4AFAABEC122D4DD3877341FF9B7 (void);
// 0x000001AB System.DateTime Amazon.Util.AWSSDKUtils::get_CorrectedUtcNow()
extern void AWSSDKUtils_get_CorrectedUtcNow_mE4AFBA5A0281699873807E635F0395877AAB9AE7 (void);
// 0x000001AC System.Boolean Amazon.Util.AWSSDKUtils::HasBidiControlCharacters(System.String)
extern void AWSSDKUtils_HasBidiControlCharacters_mD533478215E1E0E98A4D0FE7862FC07C98524430 (void);
// 0x000001AD System.Boolean Amazon.Util.AWSSDKUtils::IsBidiControlChar(System.Char)
extern void AWSSDKUtils_IsBidiControlChar_mF572580CDDC1CC2DE37E5FB2D9E46442DD0C3F9C (void);
// 0x000001AE System.String Amazon.Util.AWSSDKUtils::ExecuteHttpRequest(System.Uri,System.String,System.String,System.TimeSpan,System.Net.IWebProxy,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AWSSDKUtils_ExecuteHttpRequest_m9D231BB7824DE3B76C5F6654220392D6B442DA57 (void);
// 0x000001AF System.Net.Http.HttpClient Amazon.Util.AWSSDKUtils::CreateClient(System.Uri,System.TimeSpan,System.Net.IWebProxy,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AWSSDKUtils_CreateClient_mB840CDD823C8769EF0BC7614ED3B522E81B1081F (void);
// 0x000001B0 System.IO.Stream Amazon.Util.AWSSDKUtils::OpenStream(System.Uri,System.Net.IWebProxy)
extern void AWSSDKUtils_OpenStream_m614439B791BD93BF526647DF2B7B621C1B23229C (void);
// 0x000001B1 System.String Amazon.Util.AWSSDKUtils::CompressSpaces(System.String)
extern void AWSSDKUtils_CompressSpaces_m1E23989796ADE51F9E1FDC9BBB9939EB826F8B5C (void);
// 0x000001B2 System.Void Amazon.Util.AWSSDKUtils::Sleep(System.Int32)
extern void AWSSDKUtils_Sleep_mBB07FE79C9765EB2BAE1859B80E808E8701F144C (void);
// 0x000001B3 System.Void Amazon.Util.AWSSDKUtils::.cctor()
extern void AWSSDKUtils__cctor_m5031AD0FC09F01DD36176D77433E4C23458F6510 (void);
// 0x000001B4 System.Void Amazon.Util.AWSSDKUtils/<>c::.cctor()
extern void U3CU3Ec__cctor_m9F819D44D3134E2B66A864E30A06909D319C826B (void);
// 0x000001B5 System.Void Amazon.Util.AWSSDKUtils/<>c::.ctor()
extern void U3CU3Ec__ctor_m7CF9DBC020D7E5399DCD92ACB1F61A36C13DFDD4 (void);
// 0x000001B6 System.String Amazon.Util.AWSSDKUtils/<>c::<CanonicalizeResourcePath>b__38_0(System.String)
extern void U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__38_0_mE4233E9B88AD81B7F9BC0249E6EA1F0AB2B87FC7 (void);
// 0x000001B7 System.String Amazon.Util.AWSSDKUtils/<>c::<CanonicalizeResourcePath>b__38_1(System.String)
extern void U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__38_1_mE3AE06E0D1E273AB77A8E532E3D36E68FC308725 (void);
// 0x000001B8 System.String Amazon.Util.AWSSDKUtils/<>c::<CanonicalizeResourcePath>b__38_2(System.String)
extern void U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__38_2_m0CE9E2321D125F9115A6EB1DD6E21F4ABA55372E (void);
// 0x000001B9 System.String Amazon.Util.AWSSDKUtils/<>c::<JoinResourcePathSegments>b__40_1(System.String)
extern void U3CU3Ec_U3CJoinResourcePathSegmentsU3Eb__40_1_mB0D9B08B2BC78C52C432D8BC042009E88E159B1B (void);
// 0x000001BA System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mFB5ACDB461169A42C990EB348C2CFD90DD5AD4BA (void);
// 0x000001BB System.String Amazon.Util.AWSSDKUtils/<>c__DisplayClass40_0::<JoinResourcePathSegments>b__0(System.String)
extern void U3CU3Ec__DisplayClass40_0_U3CJoinResourcePathSegmentsU3Eb__0_m48A6C67BA4854BA88F8C9A465E71DC1282454A97 (void);
// 0x000001BC System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass54_0`1::.ctor()
// 0x000001BD System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass54_1`1::.ctor()
// 0x000001BE System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass54_1`1::<InvokeInBackground>b__0()
// 0x000001BF System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass95_0::.ctor()
extern void U3CU3Ec__DisplayClass95_0__ctor_m200B865431333F10F6566235716D5EF111B04E3C (void);
// 0x000001C0 System.Threading.Tasks.Task`1<System.Net.Http.HttpResponseMessage> Amazon.Util.AWSSDKUtils/<>c__DisplayClass95_0::<ExecuteHttpRequest>b__0()
extern void U3CU3Ec__DisplayClass95_0_U3CExecuteHttpRequestU3Eb__0_mAB8FFDDFF6E49F42F10F95AE58FA3594088D2AEB (void);
// 0x000001C1 System.Void Amazon.Util.AWSSDKUtils/<>c__DisplayClass95_1::.ctor()
extern void U3CU3Ec__DisplayClass95_1__ctor_m6FD815F6AEDB2FE622329F40F111E2F60F1D22E9 (void);
// 0x000001C2 System.Threading.Tasks.Task`1<System.String> Amazon.Util.AWSSDKUtils/<>c__DisplayClass95_1::<ExecuteHttpRequest>b__1()
extern void U3CU3Ec__DisplayClass95_1_U3CExecuteHttpRequestU3Eb__1_m52F051D4B9969CBF7BC35CCB3414721B9D456615 (void);
// 0x000001C3 Amazon.Util.ICryptoUtil Amazon.Util.CryptoUtilFactory::get_CryptoInstance()
extern void CryptoUtilFactory_get_CryptoInstance_mBCEF74A8E153C93A0850F86336DCDC1077C7C406 (void);
// 0x000001C4 System.Void Amazon.Util.CryptoUtilFactory::.cctor()
extern void CryptoUtilFactory__cctor_m45D3C87D61547B4F0B2C428227E76C534557FC30 (void);
// 0x000001C5 System.Void Amazon.Util.CryptoUtilFactory/CryptoUtil::.ctor()
extern void CryptoUtil__ctor_m2C3D9C75BA325CC87892736B522CA36874953754 (void);
// 0x000001C6 System.Byte[] Amazon.Util.CryptoUtilFactory/CryptoUtil::ComputeSHA256Hash(System.Byte[])
extern void CryptoUtil_ComputeSHA256Hash_mB7172474091484EC6D47DB1C5988FE3C7BDBE660 (void);
// 0x000001C7 System.Byte[] Amazon.Util.CryptoUtilFactory/CryptoUtil::ComputeSHA256Hash(System.IO.Stream)
extern void CryptoUtil_ComputeSHA256Hash_m02C5B8052902DE688297BED65624EF2F24D73CB5 (void);
// 0x000001C8 System.Byte[] Amazon.Util.CryptoUtilFactory/CryptoUtil::HMACSignBinary(System.Byte[],System.Byte[],Amazon.Runtime.SigningAlgorithm)
extern void CryptoUtil_HMACSignBinary_m62AFD13C51F699D9E02675CB0F3677C253E403EC (void);
// 0x000001C9 System.Security.Cryptography.KeyedHashAlgorithm Amazon.Util.CryptoUtilFactory/CryptoUtil::CreateKeyedHashAlgorithm(Amazon.Runtime.SigningAlgorithm)
extern void CryptoUtil_CreateKeyedHashAlgorithm_m0B00B20BC0433B48F41A261B14CBCAFB5FFDF42C (void);
// 0x000001CA System.Security.Cryptography.HashAlgorithm Amazon.Util.CryptoUtilFactory/CryptoUtil::get_SHA256HashAlgorithmInstance()
extern void CryptoUtil_get_SHA256HashAlgorithmInstance_m94DA4B020684F3CED1292E2EFCEAC4E73EE3D835 (void);
// 0x000001CB System.Byte[] Amazon.Util.ICryptoUtil::ComputeSHA256Hash(System.Byte[])
// 0x000001CC System.Byte[] Amazon.Util.ICryptoUtil::ComputeSHA256Hash(System.IO.Stream)
// 0x000001CD System.Byte[] Amazon.Util.ICryptoUtil::HMACSignBinary(System.Byte[],System.Byte[],Amazon.Runtime.SigningAlgorithm)
// 0x000001CE System.Boolean Amazon.Util.EC2InstanceMetadata::get_IsIMDSEnabled()
extern void EC2InstanceMetadata_get_IsIMDSEnabled_m6D348242E40F583DEB58C5C4D2E9BB56D96AF4F2 (void);
// 0x000001CF System.Net.IWebProxy Amazon.Util.EC2InstanceMetadata::get_Proxy()
extern void EC2InstanceMetadata_get_Proxy_m44E9E5E80195713A75A93A64D56080954ABC18C6 (void);
// 0x000001D0 Amazon.RegionEndpoint Amazon.Util.EC2InstanceMetadata::get_Region()
extern void EC2InstanceMetadata_get_Region_mE329F1E9B1E5A9066997D836362B50AAB9FEEC3F (void);
// 0x000001D1 System.String Amazon.Util.EC2InstanceMetadata::get_IdentityDocument()
extern void EC2InstanceMetadata_get_IdentityDocument_mF6DD9B5AF6AC385A701D1DAE0A9061C6BC51E50B (void);
// 0x000001D2 System.String Amazon.Util.EC2InstanceMetadata::GetData(System.String)
extern void EC2InstanceMetadata_GetData_mBD1DF9EF9367701BB656F7D59B4EDA6A379D59F6 (void);
// 0x000001D3 System.String Amazon.Util.EC2InstanceMetadata::GetData(System.String,System.Int32)
extern void EC2InstanceMetadata_GetData_m85B02C6690D54AD047EDBAE1717A3A0EAB84A043 (void);
// 0x000001D4 System.String Amazon.Util.EC2InstanceMetadata::FetchApiToken(System.Int32)
extern void EC2InstanceMetadata_FetchApiToken_m5C7D58DC4CE3A1EBDD128D4D23B66B0472562B01 (void);
// 0x000001D5 System.Void Amazon.Util.EC2InstanceMetadata::ClearTokenFlag()
extern void EC2InstanceMetadata_ClearTokenFlag_m01EE97679B10CBFDACF163E56DEBEF2F1F15C379 (void);
// 0x000001D6 System.Collections.Generic.List`1<System.String> Amazon.Util.EC2InstanceMetadata::GetItems(System.String,System.Int32,System.Boolean)
extern void EC2InstanceMetadata_GetItems_m5017CF779F1594E60C8E1F963218FEE2BD23B60A (void);
// 0x000001D7 System.Collections.Generic.List`1<System.String> Amazon.Util.EC2InstanceMetadata::GetItems(System.String,System.Int32,System.Boolean,System.String)
extern void EC2InstanceMetadata_GetItems_m5C57CE47D509B9D73240ED7754AE7275ECF519E3 (void);
// 0x000001D8 System.Void Amazon.Util.EC2InstanceMetadata::PauseExponentially(System.Int32)
extern void EC2InstanceMetadata_PauseExponentially_m54E6DE11F9F8EFA15CCAD04461BF2BB2B084B430 (void);
// 0x000001D9 System.Void Amazon.Util.EC2InstanceMetadata::.cctor()
extern void EC2InstanceMetadata__cctor_m07CAB17F1FBB2C8EF424084DE359C6EBF000C7C4 (void);
// 0x000001DA System.Void Amazon.Util.EC2InstanceMetadata/IMDSDisabledException::.ctor()
extern void IMDSDisabledException__ctor_mE7A37EB248C344319641A401E04AF13F9AE8A120 (void);
// 0x000001DB System.String Amazon.Util.Internal.InternalSDKUtils::BuildUserAgentString(System.String)
extern void InternalSDKUtils_BuildUserAgentString_m2C1A262CB88B697FA16980D3485B3DF3BD7C93D4 (void);
// 0x000001DC System.String Amazon.Util.Internal.InternalSDKUtils::GetExecutionEnvironment()
extern void InternalSDKUtils_GetExecutionEnvironment_mF8CB9381B07D8409438D77C017F4235C38AE1968 (void);
// 0x000001DD System.String Amazon.Util.Internal.InternalSDKUtils::GetExecutionEnvironmentUserAgentString()
extern void InternalSDKUtils_GetExecutionEnvironmentUserAgentString_m13F0316278ACBBDDC1D466E95020538455CB0C10 (void);
// 0x000001DE System.Void Amazon.Util.Internal.InternalSDKUtils::.cctor()
extern void InternalSDKUtils__cctor_m755DA5C11DD548AF936FD2CC26A5F613C6565006 (void);
// 0x000001DF Amazon.Util.CSMConfig Amazon.Util.Internal.RootConfig::get_CSMConfig()
extern void RootConfig_get_CSMConfig_mE24357A51A72DC9C705EA286180502BD8715A423 (void);
// 0x000001E0 System.Void Amazon.Util.Internal.RootConfig::set_CSMConfig(Amazon.Util.CSMConfig)
extern void RootConfig_set_CSMConfig_m98B7688DFE7FAD711BFA1555380BB9B3BAE1F278 (void);
// 0x000001E1 Amazon.Util.LoggingConfig Amazon.Util.Internal.RootConfig::get_Logging()
extern void RootConfig_get_Logging_m4625DDD282B9B05DA95A92741359BE067FD2D6E8 (void);
// 0x000001E2 System.Void Amazon.Util.Internal.RootConfig::set_Logging(Amazon.Util.LoggingConfig)
extern void RootConfig_set_Logging_m5E9E2542873931F0E929611F21B9A99BDD56FD9D (void);
// 0x000001E3 Amazon.Util.ProxyConfig Amazon.Util.Internal.RootConfig::get_Proxy()
extern void RootConfig_get_Proxy_m9E0480BE4E106446353942289AF7D2BB178B7162 (void);
// 0x000001E4 System.Void Amazon.Util.Internal.RootConfig::set_Proxy(Amazon.Util.ProxyConfig)
extern void RootConfig_set_Proxy_m1BE8FC28EE2B8C93A2E58C612C8B8212C8B5468F (void);
// 0x000001E5 System.String Amazon.Util.Internal.RootConfig::get_EndpointDefinition()
extern void RootConfig_get_EndpointDefinition_m3204E79A2D482EC5614416625F1BF0D9B4AF1DD9 (void);
// 0x000001E6 System.Void Amazon.Util.Internal.RootConfig::set_EndpointDefinition(System.String)
extern void RootConfig_set_EndpointDefinition_m6535665BFD93C547251D39119AED42E63366337B (void);
// 0x000001E7 System.String Amazon.Util.Internal.RootConfig::get_Region()
extern void RootConfig_get_Region_m284EF44B24F8EBB9672322B91B4134FFCFDF825F (void);
// 0x000001E8 System.Void Amazon.Util.Internal.RootConfig::set_Region(System.String)
extern void RootConfig_set_Region_m453ED10087DC4B2A11408936814FB6142970CFC0 (void);
// 0x000001E9 System.String Amazon.Util.Internal.RootConfig::get_ProfileName()
extern void RootConfig_get_ProfileName_mF622E8B2F448542B28DC0EEDFC59244BAB988205 (void);
// 0x000001EA System.Void Amazon.Util.Internal.RootConfig::set_ProfileName(System.String)
extern void RootConfig_set_ProfileName_m22D6F68E19AFBE2F41CEE81A06CB64C0775614E3 (void);
// 0x000001EB System.String Amazon.Util.Internal.RootConfig::get_ProfilesLocation()
extern void RootConfig_get_ProfilesLocation_mF3FCB10B8A02626FFEBAE7F4A3DE96637840BEEE (void);
// 0x000001EC System.Void Amazon.Util.Internal.RootConfig::set_ProfilesLocation(System.String)
extern void RootConfig_set_ProfilesLocation_m100049E9E25819ED7EB08FE271E9E98639557F50 (void);
// 0x000001ED Amazon.RegionEndpoint Amazon.Util.Internal.RootConfig::get_RegionEndpoint()
extern void RootConfig_get_RegionEndpoint_m2461C25050777A9F2DF13F64E2B61A4140340DCA (void);
// 0x000001EE System.Void Amazon.Util.Internal.RootConfig::set_UseSdkCache(System.Boolean)
extern void RootConfig_set_UseSdkCache_m66A052C98A721A3B7F2CF81F4A812F321A1E0125 (void);
// 0x000001EF System.Boolean Amazon.Util.Internal.RootConfig::get_CorrectForClockSkew()
extern void RootConfig_get_CorrectForClockSkew_m7AFB4F0C317A4C4FD94D3AC49C58E5131099CF49 (void);
// 0x000001F0 System.Void Amazon.Util.Internal.RootConfig::set_CorrectForClockSkew(System.Boolean)
extern void RootConfig_set_CorrectForClockSkew_mFCBFE1909CD693B83D6DFCE3DC3018A0FB89C19B (void);
// 0x000001F1 System.Boolean Amazon.Util.Internal.RootConfig::get_UseAlternateUserAgentHeader()
extern void RootConfig_get_UseAlternateUserAgentHeader_m7F2DA7D47D1FB6C0BD7A6CF6F022500B0292FEDE (void);
// 0x000001F2 System.Void Amazon.Util.Internal.RootConfig::.ctor()
extern void RootConfig__ctor_m14B4194E7323349BA88DE0195C07E52568246C8B (void);
// 0x000001F3 System.Type Amazon.Util.Internal.ITypeInfo::get_BaseType()
// 0x000001F4 System.Reflection.Assembly Amazon.Util.Internal.ITypeInfo::get_Assembly()
// 0x000001F5 System.Type Amazon.Util.Internal.ITypeInfo::GetInterface(System.String)
// 0x000001F6 System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Amazon.Util.Internal.ITypeInfo::GetProperties()
// 0x000001F7 System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Amazon.Util.Internal.ITypeInfo::GetFields()
// 0x000001F8 System.Reflection.FieldInfo Amazon.Util.Internal.ITypeInfo::GetField(System.String)
// 0x000001F9 System.Reflection.MethodInfo Amazon.Util.Internal.ITypeInfo::GetMethod(System.String,Amazon.Util.Internal.ITypeInfo[])
// 0x000001FA System.Reflection.ConstructorInfo Amazon.Util.Internal.ITypeInfo::GetConstructor(Amazon.Util.Internal.ITypeInfo[])
// 0x000001FB System.Reflection.PropertyInfo Amazon.Util.Internal.ITypeInfo::GetProperty(System.String)
// 0x000001FC System.Boolean Amazon.Util.Internal.ITypeInfo::IsAssignableFrom(Amazon.Util.Internal.ITypeInfo)
// 0x000001FD System.Boolean Amazon.Util.Internal.ITypeInfo::get_IsEnum()
// 0x000001FE System.Boolean Amazon.Util.Internal.ITypeInfo::get_IsClass()
// 0x000001FF System.String Amazon.Util.Internal.ITypeInfo::get_FullName()
// 0x00000200 Amazon.Util.Internal.ITypeInfo Amazon.Util.Internal.TypeFactory::GetTypeInfo(System.Type)
extern void TypeFactory_GetTypeInfo_m9755BC00A8A02BCB8A5988D5804DCB8DFFD32E83 (void);
// 0x00000201 System.Void Amazon.Util.Internal.TypeFactory::.cctor()
extern void TypeFactory__cctor_m813F49E6C072849BA06215B382631D454D48D2D0 (void);
// 0x00000202 System.Void Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::.ctor(System.Type)
extern void AbstractTypeInfo__ctor_mE7A188B8B1B0A2CF9ED6DA54F4BFCD542C0073CE (void);
// 0x00000203 System.Type Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_Type()
extern void AbstractTypeInfo_get_Type_m787F427069A926CBD0ACCD3F8D3746CA6E7E2E3F (void);
// 0x00000204 System.Int32 Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetHashCode()
extern void AbstractTypeInfo_GetHashCode_mBB4A52B37BB447B116FE970B2F4382296D49BEA6 (void);
// 0x00000205 System.Boolean Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::Equals(System.Object)
extern void AbstractTypeInfo_Equals_m2AC0799B5983AB58418866DDB031C161DA42A94A (void);
// 0x00000206 System.Type Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_BaseType()
// 0x00000207 System.Reflection.Assembly Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_Assembly()
// 0x00000208 System.Type Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetInterface(System.String)
// 0x00000209 System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetProperties()
// 0x0000020A System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetFields()
// 0x0000020B System.Reflection.FieldInfo Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetField(System.String)
// 0x0000020C System.Reflection.MethodInfo Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetMethod(System.String,Amazon.Util.Internal.ITypeInfo[])
// 0x0000020D System.Reflection.PropertyInfo Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetProperty(System.String)
// 0x0000020E System.Boolean Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::IsAssignableFrom(Amazon.Util.Internal.ITypeInfo)
// 0x0000020F System.Boolean Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_IsClass()
// 0x00000210 System.Boolean Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_IsEnum()
// 0x00000211 System.Reflection.ConstructorInfo Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::GetConstructor(Amazon.Util.Internal.ITypeInfo[])
// 0x00000212 System.String Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::get_FullName()
extern void AbstractTypeInfo_get_FullName_m42F62612E904E29E17470FDA29D70FDB94E75CA9 (void);
// 0x00000213 System.Void Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::.ctor(System.Type)
extern void TypeInfoWrapper__ctor_mF5F625F535BB8FC74A55C7F472C95AE2B1B3FEF4 (void);
// 0x00000214 System.Type Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_BaseType()
extern void TypeInfoWrapper_get_BaseType_m9E8EB5F5C03032DE8ED4818E71A4E1DBDB130D25 (void);
// 0x00000215 System.Type Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetInterface(System.String)
extern void TypeInfoWrapper_GetInterface_m7C80F37C165494E4B2308537C3B88785C08FC290 (void);
// 0x00000216 System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetProperties()
extern void TypeInfoWrapper_GetProperties_mAEED503C2BF7E6C3E72EA356216ABE293D0EE8AB (void);
// 0x00000217 System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetFields()
extern void TypeInfoWrapper_GetFields_m454AC606A26C270131DAE0A423A7B64CCF2AA078 (void);
// 0x00000218 System.Reflection.FieldInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetField(System.String)
extern void TypeInfoWrapper_GetField_mF7E05A1145989AD4AB0268D98CF1C433B8ABA83A (void);
// 0x00000219 System.Boolean Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_IsClass()
extern void TypeInfoWrapper_get_IsClass_m10F5CC76276D71D7510CA672CBBBD5CEF9DAF736 (void);
// 0x0000021A System.Boolean Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_IsEnum()
extern void TypeInfoWrapper_get_IsEnum_m3459DD5B738390A18737A84B129B5C8458A5C3D3 (void);
// 0x0000021B System.Reflection.MethodInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetMethod(System.String,Amazon.Util.Internal.ITypeInfo[])
extern void TypeInfoWrapper_GetMethod_m90037B2EDC2B633D75B04B8DCDEA56A148857481 (void);
// 0x0000021C System.Reflection.PropertyInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetProperty(System.String)
extern void TypeInfoWrapper_GetProperty_mF9FB40A13B7CADBDF926AD45B0B6CF04E0D59D53 (void);
// 0x0000021D System.Boolean Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::IsAssignableFrom(Amazon.Util.Internal.ITypeInfo)
extern void TypeInfoWrapper_IsAssignableFrom_mDC79BF44EFE2EC9465F1D785A2EDC89AFC622E2E (void);
// 0x0000021E System.Reflection.Assembly Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::get_Assembly()
extern void TypeInfoWrapper_get_Assembly_m127B530E969421E28C6C3B00724172978EAE5A92 (void);
// 0x0000021F System.Reflection.ConstructorInfo Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::GetConstructor(Amazon.Util.Internal.ITypeInfo[])
extern void TypeInfoWrapper_GetConstructor_mD5F3FA72B928CA34A8E2477182899CA1F6F826EB (void);
// 0x00000220 System.Void Amazon.Util.Internal.TypeFactory/TypeInfoWrapper::.cctor()
extern void TypeInfoWrapper__cctor_m5A669CE8261177F1CC15E506BD9C1E5823A9C33E (void);
// 0x00000221 System.Void Amazon.Util.Internal.TypeFactory/TypeInfoWrapper/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m89287B63729F734583D871E90819862289227078 (void);
// 0x00000222 System.Boolean Amazon.Util.Internal.TypeFactory/TypeInfoWrapper/<>c__DisplayClass4_0::<GetInterface>b__0(System.Type)
extern void U3CU3Ec__DisplayClass4_0_U3CGetInterfaceU3Eb__0_m4676D80C1D16FB2EA783FCBE727346E82338D900 (void);
// 0x00000223 System.String Amazon.Util.Internal.EnvironmentVariableRetriever::GetEnvironmentVariable(System.String)
extern void EnvironmentVariableRetriever_GetEnvironmentVariable_m1955AFAAA656EDC1B0B399CA8B5AF408AE62CFFE (void);
// 0x00000224 System.Void Amazon.Util.Internal.EnvironmentVariableRetriever::.ctor()
extern void EnvironmentVariableRetriever__ctor_m151A495BAF90E6A87B3EBEE8CE47871DB3ADD11A (void);
// 0x00000225 System.Void Amazon.Util.Internal.EnvironmentVariableSource::.ctor()
extern void EnvironmentVariableSource__ctor_m8DB23ECCEC46CBBEFECE67CBEF609B9FA3EBCE76 (void);
// 0x00000226 System.Void Amazon.Util.Internal.EnvironmentVariableSource::.cctor()
extern void EnvironmentVariableSource__cctor_mDD34CEEA80C34A83CC0E3B99F0D73AEE9ECC1A88 (void);
// 0x00000227 Amazon.Util.Internal.IEnvironmentVariableRetriever Amazon.Util.Internal.EnvironmentVariableSource::get_EnvironmentVariableRetriever()
extern void EnvironmentVariableSource_get_EnvironmentVariableRetriever_mECF810C4761E79E35D61A8B5932F374F49CB788E (void);
// 0x00000228 Amazon.Util.Internal.EnvironmentVariableSource Amazon.Util.Internal.EnvironmentVariableSource::get_Instance()
extern void EnvironmentVariableSource_get_Instance_m68B09747DB905CC1978B01E6206A3A5CF211D89B (void);
// 0x00000229 System.String Amazon.Util.Internal.IEnvironmentVariableRetriever::GetEnvironmentVariable(System.String)
// 0x0000022A System.Void Amazon.Util.Internal.NamedSettingsManager::.ctor(System.String)
extern void NamedSettingsManager__ctor_m7FDB92E35DCD23CB86BD1C90639495B093482867 (void);
// 0x0000022B System.Boolean Amazon.Util.Internal.NamedSettingsManager::TryGetObject(System.String,System.String&,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void NamedSettingsManager_TryGetObject_m41151ADCB7A36AD85988B957160A21A131338D93 (void);
// 0x0000022C System.Boolean Amazon.Util.Internal.SettingsManager::get_IsAvailable()
extern void SettingsManager_get_IsAvailable_m352C1FF550B8C73C310C6806ED55D9FF8917EB02 (void);
// 0x0000022D System.Void Amazon.Util.Internal.SettingsManager::.ctor(System.String)
extern void SettingsManager__ctor_m34E17C8A2CE650DB50F89D5290C269F62D23092E (void);
// 0x0000022E System.String Amazon.Util.Internal.SettingsManager::get_SettingsType()
extern void SettingsManager_get_SettingsType_m30F81BBD2789713AC9887E5025D938E982B2A0E8 (void);
// 0x0000022F System.Void Amazon.Util.Internal.SettingsManager::set_SettingsType(System.String)
extern void SettingsManager_set_SettingsType_m5960C66BF818EC32834889E87F11AAA9CEE406D4 (void);
// 0x00000230 System.Boolean Amazon.Util.Internal.SettingsManager::TryGetObjectByProperty(System.String,System.String,System.String&,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void SettingsManager_TryGetObjectByProperty_m57BA5D26A47C607C8790806DD4FCEDDD67D027FE (void);
// 0x00000231 Amazon.Runtime.Internal.Settings.SettingsCollection Amazon.Util.Internal.SettingsManager::GetSettings()
extern void SettingsManager_GetSettings_m2FEAB55B75E639F20EF33716B754B5D90287AFF2 (void);
// 0x00000232 System.Boolean Amazon.Util.Internal.SettingsManager::TryGetObjectSettings(System.String,System.String,Amazon.Runtime.Internal.Settings.SettingsCollection,Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings&)
extern void SettingsManager_TryGetObjectSettings_m2BFCFA77EF935CD9E502E9F8B0317754FCA1834A (void);
// 0x00000233 System.Void Amazon.Util.Internal.SettingsManager::EnsureAvailable()
extern void SettingsManager_EnsureAvailable_m293524507DB3F799E69E1FCE88F74B89661ED076 (void);
// 0x00000234 System.Void Amazon.Util.Internal.SettingsManager/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mBD111B12B96C5D03D51DBFF7A5850DC89A39E58D (void);
// 0x00000235 System.Boolean Amazon.Util.Internal.SettingsManager/<>c__DisplayClass16_0::<TryGetObjectSettings>b__0(Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings)
extern void U3CU3Ec__DisplayClass16_0_U3CTryGetObjectSettingsU3Eb__0_m328E77FF18B34F07F5C77BDD3A8A35E11DB139F6 (void);
// 0x00000236 System.String Amazon.Util.Internal.PlatformServices.IEnvironmentInfo::get_PlatformUserAgent()
// 0x00000237 System.String Amazon.Util.Internal.PlatformServices.IEnvironmentInfo::get_FrameworkUserAgent()
// 0x00000238 System.Void Amazon.Util.Internal.PlatformServices.ServiceFactory::.ctor()
extern void ServiceFactory__ctor_mA1F85302C39ECFA7930C3551B8BFF1710AE83C6C (void);
// 0x00000239 T Amazon.Util.Internal.PlatformServices.ServiceFactory::GetService()
// 0x0000023A System.Type Amazon.Util.Internal.PlatformServices.ServiceFactory::GetServiceType()
// 0x0000023B System.Void Amazon.Util.Internal.PlatformServices.ServiceFactory::.cctor()
extern void ServiceFactory__cctor_m28A26D12D6756A922657F62C8959E9EA214EFDE1 (void);
// 0x0000023C System.Void Amazon.Runtime.AmazonClientException::.ctor(System.String)
extern void AmazonClientException__ctor_m7B60CAA8E233ABD029FAD1CF1F1D0A425FBB16A4 (void);
// 0x0000023D System.Void Amazon.Runtime.AmazonClientException::.ctor(System.String,System.Exception)
extern void AmazonClientException__ctor_mF58A0DA4D74C0A68D52D1A89BF8C61CFDAEA33C2 (void);
// 0x0000023E System.Void Amazon.Runtime.AmazonServiceClient::set_EndpointDiscoveryResolver(Amazon.Runtime.Internal.EndpointDiscoveryResolverBase)
extern void AmazonServiceClient_set_EndpointDiscoveryResolver_mD8ED6AD49FEAB0E7ADFE0D6F14B1C3624DE8EB21 (void);
// 0x0000023F Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::get_RuntimePipeline()
extern void AmazonServiceClient_get_RuntimePipeline_m666BA08F0997534D6F4D81431D5F34CB684050D5 (void);
// 0x00000240 System.Void Amazon.Runtime.AmazonServiceClient::set_RuntimePipeline(Amazon.Runtime.Internal.RuntimePipeline)
extern void AmazonServiceClient_set_RuntimePipeline_mE11F53789AF484FEFB2A831E18E1FD7CD9F205A8 (void);
// 0x00000241 Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::get_Credentials()
extern void AmazonServiceClient_get_Credentials_m080C193E757F0C6C3C6FACA2F5C7A4EE667CB1C0 (void);
// 0x00000242 System.Void Amazon.Runtime.AmazonServiceClient::set_Credentials(Amazon.Runtime.AWSCredentials)
extern void AmazonServiceClient_set_Credentials_mB189B6BAC7FE34A3FD8B134C703CFB81E2F10FF2 (void);
// 0x00000243 Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::get_Config()
extern void AmazonServiceClient_get_Config_m7896DAF638A60C7D0CD95F2C92F7C95429A00FCF (void);
// 0x00000244 System.Void Amazon.Runtime.AmazonServiceClient::set_Config(Amazon.Runtime.IClientConfig)
extern void AmazonServiceClient_set_Config_mF980BE678237045A0E7CE73D46170D67D8C1A38F (void);
// 0x00000245 Amazon.Runtime.Internal.IServiceMetadata Amazon.Runtime.AmazonServiceClient::get_ServiceMetadata()
extern void AmazonServiceClient_get_ServiceMetadata_mF868195EEB804B404AAE5B09B3B106115487C603 (void);
// 0x00000246 System.Boolean Amazon.Runtime.AmazonServiceClient::get_SupportResponseLogging()
extern void AmazonServiceClient_get_SupportResponseLogging_m22B71A38FC730AA6748E858D14A45AF76DDAB837 (void);
// 0x00000247 System.Void Amazon.Runtime.AmazonServiceClient::add_BeforeRequestEvent(Amazon.Runtime.RequestEventHandler)
extern void AmazonServiceClient_add_BeforeRequestEvent_m818F8CCC42067965654196C87B746679EB5045C4 (void);
// 0x00000248 System.Void Amazon.Runtime.AmazonServiceClient::remove_BeforeRequestEvent(Amazon.Runtime.RequestEventHandler)
extern void AmazonServiceClient_remove_BeforeRequestEvent_m5D0A7FD127A8C3215D18A49C59680AA20766C1C6 (void);
// 0x00000249 System.Void Amazon.Runtime.AmazonServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.Runtime.ClientConfig)
extern void AmazonServiceClient__ctor_mBF0730EE760A3209048451CB7B95CC3F796985AD (void);
// 0x0000024A Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::get_Signer()
extern void AmazonServiceClient_get_Signer_m67573336045A226123F21A0BD07C7248EFF6A481 (void);
// 0x0000024B System.Void Amazon.Runtime.AmazonServiceClient::set_Signer(Amazon.Runtime.Internal.Auth.AbstractAWSSigner)
extern void AmazonServiceClient_set_Signer_m8D3241409ABFDDCF5465DD065A31DB72C162DFFB (void);
// 0x0000024C System.Void Amazon.Runtime.AmazonServiceClient::Initialize()
extern void AmazonServiceClient_Initialize_mFE10731C7B664841D4B5D9BAC00F3FDFC1926F6D (void);
// 0x0000024D System.Threading.Tasks.Task`1<TResponse> Amazon.Runtime.AmazonServiceClient::InvokeAsync(Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.Internal.InvokeOptionsBase,System.Threading.CancellationToken)
// 0x0000024E System.Void Amazon.Runtime.AmazonServiceClient::ProcessPreRequestHandlers(Amazon.Runtime.IExecutionContext)
extern void AmazonServiceClient_ProcessPreRequestHandlers_m646DB7F1C8E63E7840EEF3CF6E45C8F007419E9E (void);
// 0x0000024F System.Void Amazon.Runtime.AmazonServiceClient::ProcessRequestHandlers(Amazon.Runtime.IExecutionContext)
extern void AmazonServiceClient_ProcessRequestHandlers_mD086F4AF4B1B5223FEF59963F9FF9FD1014FF215 (void);
// 0x00000250 System.Void Amazon.Runtime.AmazonServiceClient::ProcessResponseHandlers(Amazon.Runtime.IExecutionContext)
extern void AmazonServiceClient_ProcessResponseHandlers_m2A71998E649EE988F169E9985809C7101AF317C3 (void);
// 0x00000251 System.Void Amazon.Runtime.AmazonServiceClient::ProcessExceptionHandlers(Amazon.Runtime.IExecutionContext,System.Exception)
extern void AmazonServiceClient_ProcessExceptionHandlers_m139EF81F5977833B08979EBE896DDFB9325C3EDC (void);
// 0x00000252 System.Void Amazon.Runtime.AmazonServiceClient::Dispose()
extern void AmazonServiceClient_Dispose_mA8A73A8F49A53E1CA5324B24A55F002E63F4352E (void);
// 0x00000253 System.Void Amazon.Runtime.AmazonServiceClient::Dispose(System.Boolean)
extern void AmazonServiceClient_Dispose_mF1BDBFD1F83C742FA192AFFA3896B74DCEFDC4F7 (void);
// 0x00000254 System.Void Amazon.Runtime.AmazonServiceClient::ThrowIfDisposed()
extern void AmazonServiceClient_ThrowIfDisposed_m0D7418634B3A02D10A974B7E9DBE7888444719BB (void);
// 0x00000255 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::CreateSigner()
// 0x00000256 System.Void Amazon.Runtime.AmazonServiceClient::CustomizeRuntimePipeline(Amazon.Runtime.Internal.RuntimePipeline)
extern void AmazonServiceClient_CustomizeRuntimePipeline_m1A584493513EDC4CB44F671B052FA91D88208A4C (void);
// 0x00000257 System.Void Amazon.Runtime.AmazonServiceClient::BuildRuntimePipeline()
extern void AmazonServiceClient_BuildRuntimePipeline_mBD432FA9C1EF99F19C180AE782B79A1CF263EE62 (void);
// 0x00000258 System.Uri Amazon.Runtime.AmazonServiceClient::ComposeUrl(Amazon.Runtime.Internal.IRequest)
extern void AmazonServiceClient_ComposeUrl_m97D40D5DF7A42FA43B5B40A8D4A4811DC56A571D (void);
// 0x00000259 System.Void Amazon.Runtime.AmazonServiceClient::DontUnescapePathDotsAndSlashes(System.Uri)
extern void AmazonServiceClient_DontUnescapePathDotsAndSlashes_m09466B48076FD4A85C3FF072E3A49E439499D4CB (void);
// 0x0000025A System.Void Amazon.Runtime.AmazonServiceClient::SetupCSMHandler(Amazon.Runtime.IRequestContext)
extern void AmazonServiceClient_SetupCSMHandler_m09B55B160BF4DA53A2B9441DDE79028AF9266E91 (void);
// 0x0000025B System.Void Amazon.Runtime.AmazonServiceClient/<>c::.cctor()
extern void U3CU3Ec__cctor_m3B11D734C1828F5AEAA48062A9A1C1EDB6FD2DEA (void);
// 0x0000025C System.Void Amazon.Runtime.AmazonServiceClient/<>c::.ctor()
extern void U3CU3Ec__ctor_m6C8A395315DE182B761098654CC8ECD8076155A2 (void);
// 0x0000025D System.Boolean Amazon.Runtime.AmazonServiceClient/<>c::<ComposeUrl>b__62_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CComposeUrlU3Eb__62_0_m072C70F108AD3F9040DDC45A21AFE646D5B3D90C (void);
// 0x0000025E System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String)
extern void AmazonServiceException__ctor_m88B20ADAC7190F880CDB7F71F54B595D5A11F666 (void);
// 0x0000025F System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String,System.Exception)
extern void AmazonServiceException__ctor_m42C16BD3F963C32FEB57F93D1719CC90DB627D27 (void);
// 0x00000260 System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String,System.Exception,System.Net.HttpStatusCode)
extern void AmazonServiceException__ctor_m94B75970842642BE6786183DE6B8869BCAFF7ACB (void);
// 0x00000261 System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void AmazonServiceException__ctor_m6A5B1D6DD41F926B3921A96F5259093DDA64E487 (void);
// 0x00000262 System.String Amazon.Runtime.AmazonServiceException::BuildGenericErrorMessage(System.String,System.Net.HttpStatusCode)
extern void AmazonServiceException_BuildGenericErrorMessage_m970CF299316AE7AAAF484943AB0E3ACB207C4ED9 (void);
// 0x00000263 System.String Amazon.Runtime.AmazonServiceException::get_ErrorCode()
extern void AmazonServiceException_get_ErrorCode_m75826A8EC2D14F74E70526232C99F98B7C041D2A (void);
// 0x00000264 System.String Amazon.Runtime.AmazonServiceException::get_RequestId()
extern void AmazonServiceException_get_RequestId_m1D2210EE6BA445475A2808DEF65B7BDAF091EF08 (void);
// 0x00000265 System.Void Amazon.Runtime.AmazonServiceException::set_RequestId(System.String)
extern void AmazonServiceException_set_RequestId_m315D4D8F57B0E7D73F21B7D7E111DF88D2C674F9 (void);
// 0x00000266 System.Net.HttpStatusCode Amazon.Runtime.AmazonServiceException::get_StatusCode()
extern void AmazonServiceException_get_StatusCode_m010197FEFB2C378B5A1468F26C4D25441D2E4E07 (void);
// 0x00000267 Amazon.Runtime.RetryableDetails Amazon.Runtime.AmazonServiceException::get_Retryable()
extern void AmazonServiceException_get_Retryable_mCC294299ADC8776E8326D9D147C7337123B79F36 (void);
// 0x00000268 System.Boolean Amazon.Runtime.RetryableDetails::get_Throttling()
extern void RetryableDetails_get_Throttling_m7170652F32CB321788E87CF4ECE2C480575A1501 (void);
// 0x00000269 System.Void Amazon.Runtime.AmazonUnmarshallingException::.ctor(System.String,System.String,System.Exception,System.Net.HttpStatusCode)
extern void AmazonUnmarshallingException__ctor_m054743941E297950E9BA1A6A347C3F35EDE39DBA (void);
// 0x0000026A System.Void Amazon.Runtime.AmazonUnmarshallingException::.ctor(System.String,System.String,System.String,System.Exception,System.Net.HttpStatusCode)
extern void AmazonUnmarshallingException__ctor_m4F19D2D72BE42684795E11878CD53FD8BF57AFAF (void);
// 0x0000026B System.String Amazon.Runtime.AmazonUnmarshallingException::get_LastKnownLocation()
extern void AmazonUnmarshallingException_get_LastKnownLocation_m125EB29655724CC65286914D205C57131C22B98B (void);
// 0x0000026C System.Void Amazon.Runtime.AmazonUnmarshallingException::set_LastKnownLocation(System.String)
extern void AmazonUnmarshallingException_set_LastKnownLocation_m4EE878DAF0AE3328B94919195FEAEC0DB77BBB5A (void);
// 0x0000026D System.String Amazon.Runtime.AmazonUnmarshallingException::get_ResponseBody()
extern void AmazonUnmarshallingException_get_ResponseBody_m9D8553A581FAF5F4CCDF36335644A495DCAE109C (void);
// 0x0000026E System.Void Amazon.Runtime.AmazonUnmarshallingException::set_ResponseBody(System.String)
extern void AmazonUnmarshallingException_set_ResponseBody_m40F84E8996C657ADA6F77DAFB15E1E6F9F901348 (void);
// 0x0000026F System.String Amazon.Runtime.AmazonUnmarshallingException::get_Message()
extern void AmazonUnmarshallingException_get_Message_m806D2D3910A64060489B8095085E66CB043E6E44 (void);
// 0x00000270 System.Void Amazon.Runtime.AmazonUnmarshallingException::AppendFormat(System.Text.StringBuilder,System.String,System.String)
extern void AmazonUnmarshallingException_AppendFormat_m83E67B465618DB9E802DCA606C45855B7E3B0BDC (void);
// 0x00000271 System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::Amazon.Runtime.Internal.IAmazonWebServiceRequest.get_StreamUploadProgressCallback()
extern void AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_StreamUploadProgressCallback_mE0AF7FC0693F8A85D1FC3869A412CC5A10CAA177 (void);
// 0x00000272 System.Void Amazon.Runtime.AmazonWebServiceRequest::.ctor()
extern void AmazonWebServiceRequest__ctor_m2F70AA8C6602940F5E5A6AEAE90FBED8247D9FB9 (void);
// 0x00000273 System.Void Amazon.Runtime.AmazonWebServiceRequest::FireBeforeRequestEvent(System.Object,Amazon.Runtime.RequestEventArgs)
extern void AmazonWebServiceRequest_FireBeforeRequestEvent_mC72CBA533E2E60AE5FDA10001315979D9444CDBB (void);
// 0x00000274 System.Boolean Amazon.Runtime.AmazonWebServiceRequest::Amazon.Runtime.Internal.IAmazonWebServiceRequest.get_UseSigV4()
extern void AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_UseSigV4_m412C582CD42A6D7E416C3272CDD244B1BEF79AAF (void);
// 0x00000275 System.Boolean Amazon.Runtime.AmazonWebServiceRequest::get_Expect100Continue()
extern void AmazonWebServiceRequest_get_Expect100Continue_mFCD9C05B506D8BE115613A3F46BFD8F02FBFBA07 (void);
// 0x00000276 System.Boolean Amazon.Runtime.AmazonWebServiceRequest::GetExpect100Continue()
extern void AmazonWebServiceRequest_GetExpect100Continue_m8AC2AAAA1DD5A9E1239C8613C46A4AB4186DEE80 (void);
// 0x00000277 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonWebServiceRequest::CreateSigner()
extern void AmazonWebServiceRequest_CreateSigner_m4369E2A18B540789A9482EF5CFCAA815F7021C6C (void);
// 0x00000278 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonWebServiceRequest::GetSigner()
extern void AmazonWebServiceRequest_GetSigner_m979A2525A61990A1A41E18CCF82DA42A9BFE02F8 (void);
// 0x00000279 Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::get_ResponseMetadata()
extern void AmazonWebServiceResponse_get_ResponseMetadata_m1BDDB043FE9F44E076FBED6BF4D08BE2D6ACCA1D (void);
// 0x0000027A System.Void Amazon.Runtime.AmazonWebServiceResponse::set_ResponseMetadata(Amazon.Runtime.ResponseMetadata)
extern void AmazonWebServiceResponse_set_ResponseMetadata_m0929AE71B25154E7425D9D40C2ACA914FE2865FA (void);
// 0x0000027B System.Int64 Amazon.Runtime.AmazonWebServiceResponse::get_ContentLength()
extern void AmazonWebServiceResponse_get_ContentLength_mFCF291D44F972FE27D2BA4C630C9F562A1384104 (void);
// 0x0000027C System.Void Amazon.Runtime.AmazonWebServiceResponse::set_ContentLength(System.Int64)
extern void AmazonWebServiceResponse_set_ContentLength_m4C5E4B38EFCDB99622FDD4DBE4D542AE9BDA3110 (void);
// 0x0000027D System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::get_HttpStatusCode()
extern void AmazonWebServiceResponse_get_HttpStatusCode_mF9A27E46A989A83CF4AC58E052CFB9071B59654B (void);
// 0x0000027E System.Void Amazon.Runtime.AmazonWebServiceResponse::set_HttpStatusCode(System.Net.HttpStatusCode)
extern void AmazonWebServiceResponse_set_HttpStatusCode_mE5792336FEED176D4987EA43118D46DA9542B160 (void);
// 0x0000027F System.Void Amazon.Runtime.AmazonWebServiceResponse::.ctor()
extern void AmazonWebServiceResponse__ctor_mBD52EF4DA0C1AC9176DC586E5BFC88F85F511788 (void);
// 0x00000280 Amazon.RegionEndpoint Amazon.Runtime.AWSRegion::get_Region()
extern void AWSRegion_get_Region_m8917D9F0C32A90ED6803DD6035A7044346D3D3A4 (void);
// 0x00000281 System.Void Amazon.Runtime.AWSRegion::set_Region(Amazon.RegionEndpoint)
extern void AWSRegion_set_Region_mB70ECDCFDD779661E7DE6617C4E81A3A8BE3EFB2 (void);
// 0x00000282 System.Void Amazon.Runtime.AWSRegion::SetRegionFromName(System.String)
extern void AWSRegion_SetRegionFromName_mC03637E049081AF5273F2F0CCCE7ED2A4E05B5CB (void);
// 0x00000283 System.Void Amazon.Runtime.AWSRegion::.ctor()
extern void AWSRegion__ctor_mA158F6E83C9E532A1C54EAF6A38AC064FD7E813F (void);
// 0x00000284 System.Void Amazon.Runtime.AppConfigAWSRegion::.ctor()
extern void AppConfigAWSRegion__ctor_mD6FD98570AF873CB3C6EB78CC015E346AACCF19B (void);
// 0x00000285 System.Void Amazon.Runtime.EnvironmentVariableAWSRegion::.ctor()
extern void EnvironmentVariableAWSRegion__ctor_m7106EB2A57B56C27F9047BD42C3784843A1D8D31 (void);
// 0x00000286 System.Void Amazon.Runtime.InstanceProfileAWSRegion::.ctor()
extern void InstanceProfileAWSRegion__ctor_m3C29510FC9E67DDC5343CD21495748D37D3F390B (void);
// 0x00000287 System.Void Amazon.Runtime.ProfileAWSRegion::.ctor(Amazon.Runtime.CredentialManagement.ICredentialProfileSource)
extern void ProfileAWSRegion__ctor_m6F1FC59819C2630D1C259DD536A57E848B5A39A5 (void);
// 0x00000288 System.Void Amazon.Runtime.ProfileAWSRegion::Setup(Amazon.Runtime.CredentialManagement.ICredentialProfileSource,System.String)
extern void ProfileAWSRegion_Setup_m051F8B424419AE54579F925C6555A6679E771867 (void);
// 0x00000289 System.Void Amazon.Runtime.FallbackRegionFactory::.cctor()
extern void FallbackRegionFactory__cctor_mE5DA73FD84A3B8088E6B988F240E639A8023758E (void);
// 0x0000028A System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator> Amazon.Runtime.FallbackRegionFactory::get_AllGenerators()
extern void FallbackRegionFactory_get_AllGenerators_m91FD3D6C134BB04D7FBB03849689A828C3AC0C44 (void);
// 0x0000028B System.Void Amazon.Runtime.FallbackRegionFactory::set_AllGenerators(System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator>)
extern void FallbackRegionFactory_set_AllGenerators_mD213A68B42DCC5793C7423C4B5C5564E78CAEF46 (void);
// 0x0000028C System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator> Amazon.Runtime.FallbackRegionFactory::get_NonMetadataGenerators()
extern void FallbackRegionFactory_get_NonMetadataGenerators_mF1FA51D6894FF14E2917C483BDA3FE7855C42CE9 (void);
// 0x0000028D System.Void Amazon.Runtime.FallbackRegionFactory::set_NonMetadataGenerators(System.Collections.Generic.List`1<Amazon.Runtime.FallbackRegionFactory/RegionGenerator>)
extern void FallbackRegionFactory_set_NonMetadataGenerators_m515F25404FC75B4E202732360614A438D7EE04B5 (void);
// 0x0000028E System.Void Amazon.Runtime.FallbackRegionFactory::Reset()
extern void FallbackRegionFactory_Reset_m59907C2F261973796CDA95937157C6E3D6879779 (void);
// 0x0000028F Amazon.RegionEndpoint Amazon.Runtime.FallbackRegionFactory::GetRegionEndpoint()
extern void FallbackRegionFactory_GetRegionEndpoint_m6897B70F10A5566BD928929A7E34F54720F1D87A (void);
// 0x00000290 Amazon.RegionEndpoint Amazon.Runtime.FallbackRegionFactory::GetRegionEndpoint(System.Boolean)
extern void FallbackRegionFactory_GetRegionEndpoint_mF07456A644CD99C27BDA7CC2FB89836B61D685AA (void);
// 0x00000291 System.Void Amazon.Runtime.FallbackRegionFactory/RegionGenerator::.ctor(System.Object,System.IntPtr)
extern void RegionGenerator__ctor_m2AD1FB11C62FA4D67DA4B8D4DFC8C705AFA7E374 (void);
// 0x00000292 Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/RegionGenerator::Invoke()
extern void RegionGenerator_Invoke_m6F52D04CCA59028F6EC796025C791238F2DD6565 (void);
// 0x00000293 System.IAsyncResult Amazon.Runtime.FallbackRegionFactory/RegionGenerator::BeginInvoke(System.AsyncCallback,System.Object)
extern void RegionGenerator_BeginInvoke_m6A21CB9D64163AA946BC580459ECA54403CAE92F (void);
// 0x00000294 Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/RegionGenerator::EndInvoke(System.IAsyncResult)
extern void RegionGenerator_EndInvoke_m81752F124FBED2958E07A4639D0FA0CD935C9B62 (void);
// 0x00000295 System.Void Amazon.Runtime.FallbackRegionFactory/<>c::.cctor()
extern void U3CU3Ec__cctor_m60A3B3D1125CC7482344C4E85FFD6E8798955B52 (void);
// 0x00000296 System.Void Amazon.Runtime.FallbackRegionFactory/<>c::.ctor()
extern void U3CU3Ec__ctor_m678EC579177D25204F7D308AEFB815C198A1D4D9 (void);
// 0x00000297 Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_0()
extern void U3CU3Ec_U3CResetU3Eb__12_0_m14EF72EF709DB471C4FDDA57BEBE37A318099885 (void);
// 0x00000298 Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_1()
extern void U3CU3Ec_U3CResetU3Eb__12_1_m8CBE8412EDEC3CEF0AB0B2797714EB06D351ACAC (void);
// 0x00000299 Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_2()
extern void U3CU3Ec_U3CResetU3Eb__12_2_m1814344C7C1B0C3200C0DFA20960A660BB41A3C2 (void);
// 0x0000029A Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_3()
extern void U3CU3Ec_U3CResetU3Eb__12_3_m9FEA735FE4C10176E6C50ECCCCD0F2EDF7DCC258 (void);
// 0x0000029B Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_4()
extern void U3CU3Ec_U3CResetU3Eb__12_4_mE2A855BD524E973FE32FB7D82E8F235F95AD4554 (void);
// 0x0000029C Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_5()
extern void U3CU3Ec_U3CResetU3Eb__12_5_m4CA8D90B9B4E9D35A0AF62F7C81944D4D3BDDDA4 (void);
// 0x0000029D Amazon.Runtime.AWSRegion Amazon.Runtime.FallbackRegionFactory/<>c::<Reset>b__12_6()
extern void U3CU3Ec_U3CResetU3Eb__12_6_m7A8B4547E7C3563A1607FB58B3D38EF6A81DAAC6 (void);
// 0x0000029E System.String Amazon.Runtime.ClientConfig::get_UserAgent()
// 0x0000029F System.Boolean Amazon.Runtime.ClientConfig::get_UseAlternateUserAgentHeader()
extern void ClientConfig_get_UseAlternateUserAgentHeader_mB995968596D86EA4C3F86EE5D0FD0A25C4176571 (void);
// 0x000002A0 Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::get_RegionEndpoint()
extern void ClientConfig_get_RegionEndpoint_m80C9A3EFB58C0A50E2FD72830FBAE0C51D14FEF0 (void);
// 0x000002A1 System.Void Amazon.Runtime.ClientConfig::set_RegionEndpoint(Amazon.RegionEndpoint)
extern void ClientConfig_set_RegionEndpoint_mC0C36203BCE0F2423D15D593A156B100A41A003E (void);
// 0x000002A2 System.String Amazon.Runtime.ClientConfig::get_RegionEndpointServiceName()
// 0x000002A3 System.String Amazon.Runtime.ClientConfig::get_ServiceURL()
extern void ClientConfig_get_ServiceURL_mC15E70DF37E90F12CB9C3691A2C45EE9BFE52ACC (void);
// 0x000002A4 System.Boolean Amazon.Runtime.ClientConfig::get_UseHttp()
extern void ClientConfig_get_UseHttp_m9481604979E4A54608D46F36C05D4B951E28908E (void);
// 0x000002A5 System.String Amazon.Runtime.ClientConfig::DetermineServiceURL()
extern void ClientConfig_DetermineServiceURL_mE544C57CAB2F7FAE0930C59A259F056CBF12A816 (void);
// 0x000002A6 System.String Amazon.Runtime.ClientConfig::GetUrl(Amazon.RegionEndpoint,System.String,System.Boolean,System.Boolean)
extern void ClientConfig_GetUrl_m95B1CE1CAB4C8E2DC41055FDE69B02891C1FD00F (void);
// 0x000002A7 System.String Amazon.Runtime.ClientConfig::get_AuthenticationRegion()
extern void ClientConfig_get_AuthenticationRegion_m9D28EC5CE3E459D7394EB0B34C25497E9D7CCD7D (void);
// 0x000002A8 System.Void Amazon.Runtime.ClientConfig::set_AuthenticationRegion(System.String)
extern void ClientConfig_set_AuthenticationRegion_mE889E4383A9E29BF1DC95BAB61EFC13D38C9B6FF (void);
// 0x000002A9 System.String Amazon.Runtime.ClientConfig::get_AuthenticationServiceName()
extern void ClientConfig_get_AuthenticationServiceName_m2FFEFC4BDFB096831245668D5ED7F37C793FEF29 (void);
// 0x000002AA System.Void Amazon.Runtime.ClientConfig::set_AuthenticationServiceName(System.String)
extern void ClientConfig_set_AuthenticationServiceName_mA6A0E4F14EEA4C9A42061D4C18AEF69785CF83E1 (void);
// 0x000002AB System.Int32 Amazon.Runtime.ClientConfig::get_MaxErrorRetry()
extern void ClientConfig_get_MaxErrorRetry_mE2A7F7B51E478FABE7A21E2FEE2189245FF7E934 (void);
// 0x000002AC System.Boolean Amazon.Runtime.ClientConfig::get_LogResponse()
extern void ClientConfig_get_LogResponse_m5C721823B15B63CB40F1196FD50AFAD3A940BDD6 (void);
// 0x000002AD System.Boolean Amazon.Runtime.ClientConfig::get_ReadEntireResponse()
extern void ClientConfig_get_ReadEntireResponse_mFE290934CE221EE00873AC73C41E2B4982A19BED (void);
// 0x000002AE System.Int32 Amazon.Runtime.ClientConfig::get_BufferSize()
extern void ClientConfig_get_BufferSize_m1BA2B8823FE547F3CF1920DF29A98E17EFC22540 (void);
// 0x000002AF System.Int64 Amazon.Runtime.ClientConfig::get_ProgressUpdateInterval()
extern void ClientConfig_get_ProgressUpdateInterval_mA19752A937BC09D8645395ABD1B77B291CCDAE95 (void);
// 0x000002B0 System.Boolean Amazon.Runtime.ClientConfig::get_ResignRetries()
extern void ClientConfig_get_ResignRetries_m0B9A8B53A897B8AECEA4A3B4A0DCD4A55D92DDBF (void);
// 0x000002B1 System.Boolean Amazon.Runtime.ClientConfig::get_AllowAutoRedirect()
extern void ClientConfig_get_AllowAutoRedirect_m5E00830CA97BFF67E6D8D918C534E8F833D7B929 (void);
// 0x000002B2 System.Boolean Amazon.Runtime.ClientConfig::get_LogMetrics()
extern void ClientConfig_get_LogMetrics_mADAAEEB50CE79C72590C1F2797FD524E1E3885B6 (void);
// 0x000002B3 System.Boolean Amazon.Runtime.ClientConfig::get_DisableLogging()
extern void ClientConfig_get_DisableLogging_mE5AA6E81FC5ADA31E26C9CAF4A0619BF0E18AC39 (void);
// 0x000002B4 System.Net.ICredentials Amazon.Runtime.ClientConfig::get_ProxyCredentials()
extern void ClientConfig_get_ProxyCredentials_m9128B7ECE39CA6B29DC8F530C1C819EDA99AF5E2 (void);
// 0x000002B5 System.Void Amazon.Runtime.ClientConfig::.ctor()
extern void ClientConfig__ctor_mF7458A55808D9FE37F6F49A9E7E1BBE807B3181D (void);
// 0x000002B6 System.Void Amazon.Runtime.ClientConfig::Initialize()
extern void ClientConfig_Initialize_m7F90A7048BD3D20D366E8DD2C1AF6AD66A9328EE (void);
// 0x000002B7 System.Nullable`1<System.TimeSpan> Amazon.Runtime.ClientConfig::get_Timeout()
extern void ClientConfig_get_Timeout_mF14C1C1ABBA09A73A885E48C63B197C5301023B8 (void);
// 0x000002B8 System.Boolean Amazon.Runtime.ClientConfig::get_UseDualstackEndpoint()
extern void ClientConfig_get_UseDualstackEndpoint_m36A52EE3B608E96BC55633EF97248ED52E1B1050 (void);
// 0x000002B9 System.Boolean Amazon.Runtime.ClientConfig::get_ThrottleRetries()
extern void ClientConfig_get_ThrottleRetries_m9222EC99CD4482544EB543104151C7B511F08BB5 (void);
// 0x000002BA System.Void Amazon.Runtime.ClientConfig::Validate()
extern void ClientConfig_Validate_m76BA2C392370797F8FA544AB856D34B82B65F974 (void);
// 0x000002BB System.TimeSpan Amazon.Runtime.ClientConfig::get_ClockOffset()
extern void ClientConfig_get_ClockOffset_mFF326BE6146583A2B98039B7551D314C67B12670 (void);
// 0x000002BC System.Boolean Amazon.Runtime.ClientConfig::get_DisableHostPrefixInjection()
extern void ClientConfig_get_DisableHostPrefixInjection_m47F08E0D0EEEE3B07A82D1B92013944BF2B3796E (void);
// 0x000002BD System.Int32 Amazon.Runtime.ClientConfig::get_EndpointDiscoveryCacheLimit()
extern void ClientConfig_get_EndpointDiscoveryCacheLimit_mF61894B150EA0BA1CA4CD455C0EDD64E486EDBA0 (void);
// 0x000002BE Amazon.Runtime.RequestRetryMode Amazon.Runtime.ClientConfig::get_RetryMode()
extern void ClientConfig_get_RetryMode_m160445D72F69CA1D77CE864155AC67975188747D (void);
// 0x000002BF System.Boolean Amazon.Runtime.ClientConfig::get_FastFailRequests()
extern void ClientConfig_get_FastFailRequests_m2DB2B6D28509361EB03619C7EC8877029EB33316 (void);
// 0x000002C0 System.Boolean Amazon.Runtime.ClientConfig::get_CacheHttpClient()
extern void ClientConfig_get_CacheHttpClient_mBA013FDFF04D44B821A2E8E3D3EE20A95C1A016A (void);
// 0x000002C1 System.Int32 Amazon.Runtime.ClientConfig::get_HttpClientCacheSize()
extern void ClientConfig_get_HttpClientCacheSize_m594A9DEAB0E4B8EE58F51D2C3EF457F9CF8F87F8 (void);
// 0x000002C2 Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::GetDefaultRegionEndpoint()
extern void ClientConfig_GetDefaultRegionEndpoint_mDCC4A0A294BE092CFFB9AE4C5121079C8057A3F7 (void);
// 0x000002C3 System.Net.IWebProxy Amazon.Runtime.ClientConfig::GetWebProxy()
extern void ClientConfig_GetWebProxy_m1007629B8E16CCF84231B620D2FC3959AD75392E (void);
// 0x000002C4 System.Nullable`1<System.Int32> Amazon.Runtime.ClientConfig::get_MaxConnectionsPerServer()
extern void ClientConfig_get_MaxConnectionsPerServer_m62F57F07092DFB2641B38B93452E6CF8837C40A2 (void);
// 0x000002C5 Amazon.Runtime.HttpClientFactory Amazon.Runtime.ClientConfig::get_HttpClientFactory()
extern void ClientConfig_get_HttpClientFactory_mDDFE40B1D7108CEF000727555013D413D998F732 (void);
// 0x000002C6 System.Boolean Amazon.Runtime.ClientConfig::CacheHttpClients(Amazon.Runtime.IClientConfig)
extern void ClientConfig_CacheHttpClients_m8A21AD97C8518644E62BA568300AB96E94872A59 (void);
// 0x000002C7 System.Boolean Amazon.Runtime.ClientConfig::DisposeHttpClients(Amazon.Runtime.IClientConfig)
extern void ClientConfig_DisposeHttpClients_mB2C51F60C2ECA614E0493365479FE236B67912B7 (void);
// 0x000002C8 System.String Amazon.Runtime.ClientConfig::CreateConfigUniqueString(Amazon.Runtime.IClientConfig)
extern void ClientConfig_CreateConfigUniqueString_m8B0E24E11F39E25BBB84B95F3CF16333F45454A7 (void);
// 0x000002C9 System.Boolean Amazon.Runtime.ClientConfig::UseGlobalHttpClientCache(Amazon.Runtime.IClientConfig)
extern void ClientConfig_UseGlobalHttpClientCache_m78D7D6711B74611A01E38E683ED7A5AE0409D12F (void);
// 0x000002CA System.Void Amazon.Runtime.ClientConfig::.cctor()
extern void ClientConfig__cctor_m0CD8DB005C47731A4ACFE8CB5080C32D039DE42D (void);
// 0x000002CB System.Void Amazon.Runtime.ConstantClass::.ctor(System.String)
extern void ConstantClass__ctor_m2E56E06C64185F9EAEC5D3630DF2C35E6E8E2E9B (void);
// 0x000002CC System.String Amazon.Runtime.ConstantClass::get_Value()
extern void ConstantClass_get_Value_mD7D685AF95A226A4BB1990F8EA037319629925D1 (void);
// 0x000002CD System.Void Amazon.Runtime.ConstantClass::set_Value(System.String)
extern void ConstantClass_set_Value_m6780475A4086C5357CA1146CDE36646336E3D958 (void);
// 0x000002CE System.String Amazon.Runtime.ConstantClass::ToString()
extern void ConstantClass_ToString_m59D24CF5E76A91EFB9AB533D0C32CF9C5C61A646 (void);
// 0x000002CF System.String Amazon.Runtime.ConstantClass::op_Implicit(Amazon.Runtime.ConstantClass)
extern void ConstantClass_op_Implicit_m66D411C2C8F30059CBDF13282013A60AD5D95B90 (void);
// 0x000002D0 Amazon.Runtime.ConstantClass Amazon.Runtime.ConstantClass::Intern()
extern void ConstantClass_Intern_m378C71741BD211DD2A756EFDF320C6D603A501E1 (void);
// 0x000002D1 T Amazon.Runtime.ConstantClass::FindValue(System.String)
// 0x000002D2 System.Void Amazon.Runtime.ConstantClass::LoadFields(System.Type)
extern void ConstantClass_LoadFields_mE8E0523E6109959FFC625AEF9C77ADF9BA2ECF56 (void);
// 0x000002D3 System.Int32 Amazon.Runtime.ConstantClass::GetHashCode()
extern void ConstantClass_GetHashCode_m2C6AE6F5DB7515722384F280AF845ABEEA3E8B7F (void);
// 0x000002D4 System.Boolean Amazon.Runtime.ConstantClass::Equals(System.Object)
extern void ConstantClass_Equals_mB56A1342C2CE4DDC7591822F4FDC72083CC92688 (void);
// 0x000002D5 System.Boolean Amazon.Runtime.ConstantClass::Equals(Amazon.Runtime.ConstantClass)
extern void ConstantClass_Equals_m7357D3007D5FC9B70CE66BBA6EB099E769283459 (void);
// 0x000002D6 System.Boolean Amazon.Runtime.ConstantClass::Equals(System.String)
extern void ConstantClass_Equals_m56CFFDEAFF8B9E3F60BD362190D526E44CD68A0B (void);
// 0x000002D7 System.Boolean Amazon.Runtime.ConstantClass::op_Equality(Amazon.Runtime.ConstantClass,Amazon.Runtime.ConstantClass)
extern void ConstantClass_op_Equality_mD7367B7F61F6CAF50AEE822393D7EDD80B62DFFD (void);
// 0x000002D8 System.Boolean Amazon.Runtime.ConstantClass::op_Inequality(Amazon.Runtime.ConstantClass,Amazon.Runtime.ConstantClass)
extern void ConstantClass_op_Inequality_m2D2895C8CC0CD5ECE29B6AE26FD4F95DD7F63D9D (void);
// 0x000002D9 System.Void Amazon.Runtime.ConstantClass::.cctor()
extern void ConstantClass__cctor_mBFA1C77C86AF58DF497985AED6660F46B1D94CC5 (void);
// 0x000002DA System.Nullable`1<System.TimeSpan> Amazon.Runtime.CorrectClockSkew::get_GlobalClockCorrection()
extern void CorrectClockSkew_get_GlobalClockCorrection_m7DCE300816422EB94B5D0CE927EF302B2F793810 (void);
// 0x000002DB System.TimeSpan Amazon.Runtime.CorrectClockSkew::GetClockCorrectionForEndpoint(System.String)
extern void CorrectClockSkew_GetClockCorrectionForEndpoint_m7DDCFBA01F81402EC4D278C6AD5922CCCEBA6244 (void);
// 0x000002DC System.DateTime Amazon.Runtime.CorrectClockSkew::GetCorrectedUtcNowForEndpoint(System.String)
extern void CorrectClockSkew_GetCorrectedUtcNowForEndpoint_m607062892AF9EF76ACB1091EBFE8E7C49B01A857 (void);
// 0x000002DD System.Void Amazon.Runtime.CorrectClockSkew::SetClockCorrectionForEndpoint(System.String,System.TimeSpan)
extern void CorrectClockSkew_SetClockCorrectionForEndpoint_m806ABA5DFAEC7D00B4D5034AEB58A7F69C102CB5 (void);
// 0x000002DE System.Void Amazon.Runtime.CorrectClockSkew::.cctor()
extern void CorrectClockSkew__cctor_m303C0F9372391238BEDAF195FE50EE825304C9F9 (void);
// 0x000002DF Amazon.Runtime.ImmutableCredentials Amazon.Runtime.AnonymousAWSCredentials::GetCredentials()
extern void AnonymousAWSCredentials_GetCredentials_m46F4F14A5E1F3E23D0D6814C803716CF8134A7BC (void);
// 0x000002E0 System.Void Amazon.Runtime.AnonymousAWSCredentials::.ctor()
extern void AnonymousAWSCredentials__ctor_m1FB7877C947EF2EAF5360563F234290E1CE7C9F0 (void);
// 0x000002E1 Amazon.Runtime.ImmutableCredentials Amazon.Runtime.AWSCredentials::GetCredentials()
// 0x000002E2 System.Threading.Tasks.Task`1<Amazon.Runtime.ImmutableCredentials> Amazon.Runtime.AWSCredentials::GetCredentialsAsync()
extern void AWSCredentials_GetCredentialsAsync_mC6A301BB520C02D65E6DA9196AB8214EF58854C1 (void);
// 0x000002E3 System.Void Amazon.Runtime.AWSCredentials::.ctor()
extern void AWSCredentials__ctor_m40219DF0F5E9875EBD0127595453F37060A286A0 (void);
// 0x000002E4 System.String Amazon.Runtime.ImmutableCredentials::get_AccessKey()
extern void ImmutableCredentials_get_AccessKey_m509B5AE44459C85FBEADA67014CF2921BC8893C4 (void);
// 0x000002E5 System.Void Amazon.Runtime.ImmutableCredentials::set_AccessKey(System.String)
extern void ImmutableCredentials_set_AccessKey_m04293A46CAAE7BEAF677E502A611FDB32A449D56 (void);
// 0x000002E6 System.String Amazon.Runtime.ImmutableCredentials::get_SecretKey()
extern void ImmutableCredentials_get_SecretKey_m8277ED2C42DC5F2180CFCC7C142DB6ABAB12BFAF (void);
// 0x000002E7 System.Void Amazon.Runtime.ImmutableCredentials::set_SecretKey(System.String)
extern void ImmutableCredentials_set_SecretKey_m71E51B0FB54A61F43B8C6CCA0B5D78B94BE74F63 (void);
// 0x000002E8 System.String Amazon.Runtime.ImmutableCredentials::get_Token()
extern void ImmutableCredentials_get_Token_m084E4F55E325D68EBBE74EDAEEF101E29D6748E4 (void);
// 0x000002E9 System.Void Amazon.Runtime.ImmutableCredentials::set_Token(System.String)
extern void ImmutableCredentials_set_Token_m85A958A0783A5889E0F7429C0E8D624E0D70A7B9 (void);
// 0x000002EA System.Boolean Amazon.Runtime.ImmutableCredentials::get_UseToken()
extern void ImmutableCredentials_get_UseToken_m562113AA82685945C8742DF8AD0E6B9BFA423EFB (void);
// 0x000002EB System.Void Amazon.Runtime.ImmutableCredentials::.ctor(System.String,System.String,System.String)
extern void ImmutableCredentials__ctor_mA1B9576B57F723C53AC9F591B930CCC964AA0227 (void);
// 0x000002EC System.Void Amazon.Runtime.ImmutableCredentials::.ctor()
extern void ImmutableCredentials__ctor_mBE44F71C8EA5998875DBD0CD76C6BFAA934AB5F3 (void);
// 0x000002ED Amazon.Runtime.ImmutableCredentials Amazon.Runtime.ImmutableCredentials::Copy()
extern void ImmutableCredentials_Copy_mAFF2F626B1B26F1ECB31335E9F3BABA72D043354 (void);
// 0x000002EE System.Int32 Amazon.Runtime.ImmutableCredentials::GetHashCode()
extern void ImmutableCredentials_GetHashCode_mE9EE32697664541F246703BD98E90EDADFFC8A56 (void);
// 0x000002EF System.Boolean Amazon.Runtime.ImmutableCredentials::Equals(System.Object)
extern void ImmutableCredentials_Equals_mAD33704068FBB88234B7F685CC76F768A6D1C68C (void);
// 0x000002F0 System.TimeSpan Amazon.Runtime.RefreshingAWSCredentials::get_PreemptExpiryTime()
extern void RefreshingAWSCredentials_get_PreemptExpiryTime_m26220A24D051767D3A64013B452E5AFE25C46A9C (void);
// 0x000002F1 Amazon.Runtime.ImmutableCredentials Amazon.Runtime.RefreshingAWSCredentials::GetCredentials()
extern void RefreshingAWSCredentials_GetCredentials_m3EB48E5DC2E9522B2704EBF6454A45BA0E190702 (void);
// 0x000002F2 System.Threading.Tasks.Task`1<Amazon.Runtime.ImmutableCredentials> Amazon.Runtime.RefreshingAWSCredentials::GetCredentialsAsync()
extern void RefreshingAWSCredentials_GetCredentialsAsync_mB595D535B6BFD152A8051EAB4C2C297BCEE6B7EA (void);
// 0x000002F3 System.Void Amazon.Runtime.RefreshingAWSCredentials::UpdateToGeneratedCredentials(Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState,System.TimeSpan)
extern void RefreshingAWSCredentials_UpdateToGeneratedCredentials_m479C8757127ED2CF738B664584A40A30868A3DE3 (void);
// 0x000002F4 System.Boolean Amazon.Runtime.RefreshingAWSCredentials::ShouldUpdateState(Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState,System.TimeSpan)
extern void RefreshingAWSCredentials_ShouldUpdateState_mA5FE6091272BEDDECE28169535EBB9B249318BA0 (void);
// 0x000002F5 Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.Runtime.RefreshingAWSCredentials::GenerateNewCredentials()
extern void RefreshingAWSCredentials_GenerateNewCredentials_mC2EB5641B686E03CEE3B731A0B332B0BECF64AD8 (void);
// 0x000002F6 System.Threading.Tasks.Task`1<Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState> Amazon.Runtime.RefreshingAWSCredentials::GenerateNewCredentialsAsync()
extern void RefreshingAWSCredentials_GenerateNewCredentialsAsync_m44B357056ED386C764C27F1458E49F94B90C1FE3 (void);
// 0x000002F7 System.Void Amazon.Runtime.RefreshingAWSCredentials::Dispose(System.Boolean)
extern void RefreshingAWSCredentials_Dispose_mC9D6C17509D5011D21E0C3A9844B7F8E250C84E3 (void);
// 0x000002F8 System.Void Amazon.Runtime.RefreshingAWSCredentials::ClearCredentials()
extern void RefreshingAWSCredentials_ClearCredentials_m4D98EBD5597C8737E00EAC748F8D8C39931761B5 (void);
// 0x000002F9 System.Void Amazon.Runtime.RefreshingAWSCredentials::Dispose()
extern void RefreshingAWSCredentials_Dispose_mBB2058CBA8F9135C1D75A6270ABB6CD7B9F00F82 (void);
// 0x000002FA System.Void Amazon.Runtime.RefreshingAWSCredentials::.ctor()
extern void RefreshingAWSCredentials__ctor_m32CA1E300BA753E32D73A3169771C0F0D13FF3A9 (void);
// 0x000002FB Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.Runtime.RefreshingAWSCredentials::<GenerateNewCredentialsAsync>b__16_0()
extern void RefreshingAWSCredentials_U3CGenerateNewCredentialsAsyncU3Eb__16_0_m0FC03D1BEEF1A6854CDAE4736471A16EDB04A301 (void);
// 0x000002FC Amazon.Runtime.ImmutableCredentials Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::get_Credentials()
extern void CredentialsRefreshState_get_Credentials_m6C85D5BD9D58FEB3DE818A71424499EF455DADB2 (void);
// 0x000002FD System.Void Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::set_Credentials(Amazon.Runtime.ImmutableCredentials)
extern void CredentialsRefreshState_set_Credentials_m107E3DADE4BFD6157B147BE7438BD649F1A2AD01 (void);
// 0x000002FE System.DateTime Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::get_Expiration()
extern void CredentialsRefreshState_get_Expiration_mFA7346BBB61AEDB3A90A8012518B54433EF7B17A (void);
// 0x000002FF System.Void Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::set_Expiration(System.DateTime)
extern void CredentialsRefreshState_set_Expiration_m1D4B0D4D19FEC97F0DD52569E60D4AF367A481D5 (void);
// 0x00000300 System.Void Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::.ctor(Amazon.Runtime.ImmutableCredentials,System.DateTime)
extern void CredentialsRefreshState__ctor_mF42D5DFDF74C5C5B1C3BE07234B86ED1A0E06C7E (void);
// 0x00000301 System.Boolean Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState::IsExpiredWithin(System.TimeSpan)
extern void CredentialsRefreshState_IsExpiredWithin_mD65E84E9F6A1462BA48466191470E97403229499 (void);
// 0x00000302 System.Void Amazon.Runtime.RefreshingAWSCredentials/<GetCredentialsAsync>d__10::MoveNext()
extern void U3CGetCredentialsAsyncU3Ed__10_MoveNext_m005DC9454535AFC3DDDADBBC988058FF8A4875A2 (void);
// 0x00000303 System.Void Amazon.Runtime.RefreshingAWSCredentials/<GetCredentialsAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetCredentialsAsyncU3Ed__10_SetStateMachine_mFA86F065CDB16A8B134F7E86E7E51EFEBE33081E (void);
// 0x00000304 System.Void Amazon.Runtime.ExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern void ExceptionEventHandler__ctor_mAF47BE12EEAE11B92679FC63A064BC1A4C016069 (void);
// 0x00000305 System.Void Amazon.Runtime.ExceptionEventHandler::Invoke(System.Object,Amazon.Runtime.ExceptionEventArgs)
extern void ExceptionEventHandler_Invoke_m52A024CF85D630A1C6496092ED7F1A5A1E2C8877 (void);
// 0x00000306 System.IAsyncResult Amazon.Runtime.ExceptionEventHandler::BeginInvoke(System.Object,Amazon.Runtime.ExceptionEventArgs,System.AsyncCallback,System.Object)
extern void ExceptionEventHandler_BeginInvoke_mCA6D132EA956495AD7668881AB229FF40F14E3CD (void);
// 0x00000307 System.Void Amazon.Runtime.ExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern void ExceptionEventHandler_EndInvoke_mF6610B57B4466108CF26C583451F846477451370 (void);
// 0x00000308 System.Void Amazon.Runtime.ExceptionEventArgs::.ctor()
extern void ExceptionEventArgs__ctor_mEB9C374B8087936FF7AED1392F0E474EA89AD8F9 (void);
// 0x00000309 System.Void Amazon.Runtime.WebServiceExceptionEventArgs::.ctor()
extern void WebServiceExceptionEventArgs__ctor_m222BEA133F885D8DEBF6EA270C480BB86F23E48E (void);
// 0x0000030A System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Headers(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceExceptionEventArgs_set_Headers_m308C72B975C6934CC942A2690D429FC1544C04AA (void);
// 0x0000030B System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Parameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceExceptionEventArgs_set_Parameters_m5E27A0995E6B14D58130418E0563E57BE1327AA1 (void);
// 0x0000030C System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_ServiceName(System.String)
extern void WebServiceExceptionEventArgs_set_ServiceName_mEEA7C88A76BB3147789C6D9B9CAE7B45F5B3F5DB (void);
// 0x0000030D System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Endpoint(System.Uri)
extern void WebServiceExceptionEventArgs_set_Endpoint_m753A6846890C931016AB3E9746D32CC29A3C7499 (void);
// 0x0000030E System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern void WebServiceExceptionEventArgs_set_Request_m975F088853F42DBE16625E28A06C8BC3F6B61631 (void);
// 0x0000030F System.Void Amazon.Runtime.WebServiceExceptionEventArgs::set_Exception(System.Exception)
extern void WebServiceExceptionEventArgs_set_Exception_m670BD3838487CD3BC56C8111A55DBAF0B6AB98BC (void);
// 0x00000310 Amazon.Runtime.WebServiceExceptionEventArgs Amazon.Runtime.WebServiceExceptionEventArgs::Create(System.Exception,Amazon.Runtime.Internal.IRequest)
extern void WebServiceExceptionEventArgs_Create_mF3972242383310195B89A4565C3D6D63474CB317 (void);
// 0x00000311 Amazon.RegionEndpoint Amazon.Runtime.IClientConfig::get_RegionEndpoint()
// 0x00000312 System.String Amazon.Runtime.IClientConfig::get_RegionEndpointServiceName()
// 0x00000313 System.String Amazon.Runtime.IClientConfig::get_ServiceURL()
// 0x00000314 System.Boolean Amazon.Runtime.IClientConfig::get_UseHttp()
// 0x00000315 System.String Amazon.Runtime.IClientConfig::get_AuthenticationRegion()
// 0x00000316 System.String Amazon.Runtime.IClientConfig::get_AuthenticationServiceName()
// 0x00000317 System.String Amazon.Runtime.IClientConfig::get_UserAgent()
// 0x00000318 System.Boolean Amazon.Runtime.IClientConfig::get_LogMetrics()
// 0x00000319 System.Boolean Amazon.Runtime.IClientConfig::get_LogResponse()
// 0x0000031A System.Boolean Amazon.Runtime.IClientConfig::get_ReadEntireResponse()
// 0x0000031B System.Boolean Amazon.Runtime.IClientConfig::get_AllowAutoRedirect()
// 0x0000031C System.Int32 Amazon.Runtime.IClientConfig::get_BufferSize()
// 0x0000031D System.Int32 Amazon.Runtime.IClientConfig::get_MaxErrorRetry()
// 0x0000031E System.Int64 Amazon.Runtime.IClientConfig::get_ProgressUpdateInterval()
// 0x0000031F System.Boolean Amazon.Runtime.IClientConfig::get_ResignRetries()
// 0x00000320 System.Net.ICredentials Amazon.Runtime.IClientConfig::get_ProxyCredentials()
// 0x00000321 System.Nullable`1<System.TimeSpan> Amazon.Runtime.IClientConfig::get_Timeout()
// 0x00000322 System.Boolean Amazon.Runtime.IClientConfig::get_UseDualstackEndpoint()
// 0x00000323 System.Boolean Amazon.Runtime.IClientConfig::get_ThrottleRetries()
// 0x00000324 System.String Amazon.Runtime.IClientConfig::DetermineServiceURL()
// 0x00000325 System.TimeSpan Amazon.Runtime.IClientConfig::get_ClockOffset()
// 0x00000326 System.Boolean Amazon.Runtime.IClientConfig::get_DisableHostPrefixInjection()
// 0x00000327 System.Int32 Amazon.Runtime.IClientConfig::get_EndpointDiscoveryCacheLimit()
// 0x00000328 Amazon.Runtime.RequestRetryMode Amazon.Runtime.IClientConfig::get_RetryMode()
// 0x00000329 System.Boolean Amazon.Runtime.IClientConfig::get_FastFailRequests()
// 0x0000032A System.Nullable`1<System.Int32> Amazon.Runtime.IClientConfig::get_MaxConnectionsPerServer()
// 0x0000032B System.Boolean Amazon.Runtime.IClientConfig::get_CacheHttpClient()
// 0x0000032C System.Int32 Amazon.Runtime.IClientConfig::get_HttpClientCacheSize()
// 0x0000032D System.Net.IWebProxy Amazon.Runtime.IClientConfig::GetWebProxy()
// 0x0000032E Amazon.Runtime.HttpClientFactory Amazon.Runtime.IClientConfig::get_HttpClientFactory()
// 0x0000032F System.Void Amazon.Runtime.ParameterValue::.ctor()
extern void ParameterValue__ctor_m14513C26A3DA99BECD8894F635142019EA2682DE (void);
// 0x00000330 System.String Amazon.Runtime.StringParameterValue::get_Value()
extern void StringParameterValue_get_Value_m6805935D4A23333C6204D77BC47A40B5780B2F01 (void);
// 0x00000331 System.Void Amazon.Runtime.StringParameterValue::set_Value(System.String)
extern void StringParameterValue_set_Value_m2BDDB0D689259FFAD473566188DD655BB972ECD1 (void);
// 0x00000332 System.Void Amazon.Runtime.StringParameterValue::.ctor(System.String)
extern void StringParameterValue__ctor_m07D760E9C8B49EA94E35210577D82634CE99B393 (void);
// 0x00000333 System.Collections.Generic.List`1<System.String> Amazon.Runtime.StringListParameterValue::get_Value()
extern void StringListParameterValue_get_Value_m8E742CAEA3583E1393F735531020B553C977A593 (void);
// 0x00000334 System.Void Amazon.Runtime.StringListParameterValue::set_Value(System.Collections.Generic.List`1<System.String>)
extern void StringListParameterValue_set_Value_mCBB88C38BF207C8182ABC8B9F2B3AEFA3C8E3D16 (void);
// 0x00000335 Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.IRequestContext::get_OriginalRequest()
// 0x00000336 System.String Amazon.Runtime.IRequestContext::get_RequestName()
// 0x00000337 Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.IRequestContext::get_Marshaller()
// 0x00000338 Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.IRequestContext::get_Unmarshaller()
// 0x00000339 Amazon.Runtime.Internal.InvokeOptionsBase Amazon.Runtime.IRequestContext::get_Options()
// 0x0000033A Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.IRequestContext::get_Metrics()
// 0x0000033B Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.IRequestContext::get_Signer()
// 0x0000033C Amazon.Runtime.IClientConfig Amazon.Runtime.IRequestContext::get_ClientConfig()
// 0x0000033D Amazon.Runtime.ImmutableCredentials Amazon.Runtime.IRequestContext::get_ImmutableCredentials()
// 0x0000033E System.Void Amazon.Runtime.IRequestContext::set_ImmutableCredentials(Amazon.Runtime.ImmutableCredentials)
// 0x0000033F Amazon.Runtime.Internal.IRequest Amazon.Runtime.IRequestContext::get_Request()
// 0x00000340 System.Void Amazon.Runtime.IRequestContext::set_Request(Amazon.Runtime.Internal.IRequest)
// 0x00000341 System.Boolean Amazon.Runtime.IRequestContext::get_IsSigned()
// 0x00000342 System.Void Amazon.Runtime.IRequestContext::set_IsSigned(System.Boolean)
// 0x00000343 System.Boolean Amazon.Runtime.IRequestContext::get_IsAsync()
// 0x00000344 System.Int32 Amazon.Runtime.IRequestContext::get_Retries()
// 0x00000345 System.Void Amazon.Runtime.IRequestContext::set_Retries(System.Int32)
// 0x00000346 Amazon.Runtime.Internal.CapacityManager/CapacityType Amazon.Runtime.IRequestContext::get_LastCapacityType()
// 0x00000347 System.Int32 Amazon.Runtime.IRequestContext::get_EndpointDiscoveryRetries()
// 0x00000348 System.Void Amazon.Runtime.IRequestContext::set_EndpointDiscoveryRetries(System.Int32)
// 0x00000349 System.Threading.CancellationToken Amazon.Runtime.IRequestContext::get_CancellationToken()
// 0x0000034A Amazon.Runtime.Internal.MonitoringAPICallAttempt Amazon.Runtime.IRequestContext::get_CSMCallAttempt()
// 0x0000034B System.Void Amazon.Runtime.IRequestContext::set_CSMCallAttempt(Amazon.Runtime.Internal.MonitoringAPICallAttempt)
// 0x0000034C Amazon.Runtime.Internal.MonitoringAPICallEvent Amazon.Runtime.IRequestContext::get_CSMCallEvent()
// 0x0000034D System.Void Amazon.Runtime.IRequestContext::set_CSMCallEvent(Amazon.Runtime.Internal.MonitoringAPICallEvent)
// 0x0000034E Amazon.Runtime.Internal.IServiceMetadata Amazon.Runtime.IRequestContext::get_ServiceMetaData()
// 0x0000034F System.Boolean Amazon.Runtime.IRequestContext::get_CSMEnabled()
// 0x00000350 System.Boolean Amazon.Runtime.IRequestContext::get_IsLastExceptionRetryable()
// 0x00000351 System.Void Amazon.Runtime.IRequestContext::set_IsLastExceptionRetryable(System.Boolean)
// 0x00000352 Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.IResponseContext::get_Response()
// 0x00000353 System.Void Amazon.Runtime.IResponseContext::set_Response(Amazon.Runtime.AmazonWebServiceResponse)
// 0x00000354 Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.IResponseContext::get_HttpResponse()
// 0x00000355 System.Void Amazon.Runtime.IResponseContext::set_HttpResponse(Amazon.Runtime.Internal.Transform.IWebResponseData)
// 0x00000356 Amazon.Runtime.IResponseContext Amazon.Runtime.IExecutionContext::get_ResponseContext()
// 0x00000357 Amazon.Runtime.IRequestContext Amazon.Runtime.IExecutionContext::get_RequestContext()
// 0x00000358 System.Boolean Amazon.Runtime.IExceptionHandler::Handle(Amazon.Runtime.IExecutionContext,System.Exception)
// 0x00000359 Amazon.Runtime.IHttpRequest`1<TRequestContent> Amazon.Runtime.IHttpRequestFactory`1::CreateHttpRequest(System.Uri)
// 0x0000035A System.Void Amazon.Runtime.IHttpRequest`1::set_Method(System.String)
// 0x0000035B System.Void Amazon.Runtime.IHttpRequest`1::ConfigureRequest(Amazon.Runtime.IRequestContext)
// 0x0000035C System.Void Amazon.Runtime.IHttpRequest`1::SetRequestHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
// 0x0000035D System.Void Amazon.Runtime.IHttpRequest`1::WriteToRequestBody(TRequestContent,System.IO.Stream,System.Collections.Generic.IDictionary`2<System.String,System.String>,Amazon.Runtime.IRequestContext)
// 0x0000035E System.Void Amazon.Runtime.IHttpRequest`1::WriteToRequestBody(TRequestContent,System.Byte[],System.Collections.Generic.IDictionary`2<System.String,System.String>)
// 0x0000035F System.IO.Stream Amazon.Runtime.IHttpRequest`1::SetupProgressListeners(System.IO.Stream,System.Int64,System.Object,System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>)
// 0x00000360 System.Threading.Tasks.Task`1<TRequestContent> Amazon.Runtime.IHttpRequest`1::GetRequestContentAsync()
// 0x00000361 System.Threading.Tasks.Task`1<Amazon.Runtime.Internal.Transform.IWebResponseData> Amazon.Runtime.IHttpRequest`1::GetResponseAsync(System.Threading.CancellationToken)
// 0x00000362 System.Net.Http.HttpClient Amazon.Runtime.HttpClientFactory::CreateHttpClient(Amazon.Runtime.IClientConfig)
// 0x00000363 System.Boolean Amazon.Runtime.HttpClientFactory::UseSDKHttpClientCaching(Amazon.Runtime.IClientConfig)
extern void HttpClientFactory_UseSDKHttpClientCaching_m80136D61159EB95ED0E94914232AD6118CAEDC8F (void);
// 0x00000364 System.Boolean Amazon.Runtime.HttpClientFactory::DisposeHttpClientsAfterUse(Amazon.Runtime.IClientConfig)
extern void HttpClientFactory_DisposeHttpClientsAfterUse_m3C9064AF28E15E3AD3B0C270ABA2B99C8BC37891 (void);
// 0x00000365 System.String Amazon.Runtime.HttpClientFactory::GetConfigUniqueString(Amazon.Runtime.IClientConfig)
extern void HttpClientFactory_GetConfigUniqueString_mBF78F30875151039670D53710ABF89E947DD58FE (void);
// 0x00000366 System.Void Amazon.Runtime.HttpRequestMessageFactory::.ctor(Amazon.Runtime.IClientConfig)
extern void HttpRequestMessageFactory__ctor_m1223A8366BA23F0D90CBB87F06DF519B90D7D8BB (void);
// 0x00000367 Amazon.Runtime.IHttpRequest`1<System.Net.Http.HttpContent> Amazon.Runtime.HttpRequestMessageFactory::CreateHttpRequest(System.Uri)
extern void HttpRequestMessageFactory_CreateHttpRequest_m52AE6BB0D65B13AC0815CA07EA7C80DA8452DFEB (void);
// 0x00000368 System.Void Amazon.Runtime.HttpRequestMessageFactory::Dispose()
extern void HttpRequestMessageFactory_Dispose_mD557F2C91E1506AE02B0E655359619CFDF00E97D (void);
// 0x00000369 System.Void Amazon.Runtime.HttpRequestMessageFactory::Dispose(System.Boolean)
extern void HttpRequestMessageFactory_Dispose_mECC3D9D8BF8068D3BD77799CDC4343B7E173BA65 (void);
// 0x0000036A Amazon.Runtime.HttpClientCache Amazon.Runtime.HttpRequestMessageFactory::CreateHttpClientCache(Amazon.Runtime.IClientConfig)
extern void HttpRequestMessageFactory_CreateHttpClientCache_m2025426D9CF87646E681E6487542C8F7CA920A51 (void);
// 0x0000036B System.Net.Http.HttpClient Amazon.Runtime.HttpRequestMessageFactory::CreateHttpClient(Amazon.Runtime.IClientConfig)
extern void HttpRequestMessageFactory_CreateHttpClient_mBFA721C6B0154392AEC7295DFF639D93134D0935 (void);
// 0x0000036C System.Net.Http.HttpClient Amazon.Runtime.HttpRequestMessageFactory::CreateManagedHttpClient(Amazon.Runtime.IClientConfig)
extern void HttpRequestMessageFactory_CreateManagedHttpClient_m35F5C8EC4F9E3E3FF67240DD056FED8AB54DF00A (void);
// 0x0000036D System.Void Amazon.Runtime.HttpRequestMessageFactory::.cctor()
extern void HttpRequestMessageFactory__cctor_mE9C1F676FFF18B77E6C3742F131A1282B6779612 (void);
// 0x0000036E System.Void Amazon.Runtime.HttpClientCache::.ctor(System.Net.Http.HttpClient[])
extern void HttpClientCache__ctor_m1637D80D04CC94B3EC13C477DFADDD8831424352 (void);
// 0x0000036F System.Net.Http.HttpClient Amazon.Runtime.HttpClientCache::GetNextClient()
extern void HttpClientCache_GetNextClient_mB7691BEC8B0306F67E2017F9F7AB348A3CB02AB5 (void);
// 0x00000370 System.Void Amazon.Runtime.HttpClientCache::Dispose()
extern void HttpClientCache_Dispose_m754BC02BFA3AC4114E6895A86757DCD28C2E3FA7 (void);
// 0x00000371 System.Void Amazon.Runtime.HttpClientCache::Dispose(System.Boolean)
extern void HttpClientCache_Dispose_m983BDD4FC51B58EB011FD2770130E54AC9DAD92F (void);
// 0x00000372 System.Void Amazon.Runtime.HttpWebRequestMessage::.ctor(System.Net.Http.HttpClient,System.Uri,Amazon.Runtime.IClientConfig)
extern void HttpWebRequestMessage__ctor_mB0046BCD5EF3615B24AE09ADD9AA9B0C1587EC02 (void);
// 0x00000373 System.Void Amazon.Runtime.HttpWebRequestMessage::set_Method(System.String)
extern void HttpWebRequestMessage_set_Method_m633B297B3153DE58624F375C044C6E684B0F703B (void);
// 0x00000374 System.Void Amazon.Runtime.HttpWebRequestMessage::ConfigureRequest(Amazon.Runtime.IRequestContext)
extern void HttpWebRequestMessage_ConfigureRequest_mB48B9450537567252FB4D7FC499E8C75CBC97B18 (void);
// 0x00000375 System.Void Amazon.Runtime.HttpWebRequestMessage::SetRequestHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void HttpWebRequestMessage_SetRequestHeaders_mF9FFDB919E7E9FAB5FB3703C5CB3D53517870736 (void);
// 0x00000376 System.Threading.Tasks.Task`1<Amazon.Runtime.Internal.Transform.IWebResponseData> Amazon.Runtime.HttpWebRequestMessage::GetResponseAsync(System.Threading.CancellationToken)
extern void HttpWebRequestMessage_GetResponseAsync_m093528F50DB252F5D33A06C9F20250CE885DE094 (void);
// 0x00000377 System.Void Amazon.Runtime.HttpWebRequestMessage::WriteToRequestBody(System.Net.Http.HttpContent,System.IO.Stream,System.Collections.Generic.IDictionary`2<System.String,System.String>,Amazon.Runtime.IRequestContext)
extern void HttpWebRequestMessage_WriteToRequestBody_m9087C73D8AA358E048A45BF86BB45803E4345D9C (void);
// 0x00000378 System.Void Amazon.Runtime.HttpWebRequestMessage::WriteToRequestBody(System.Net.Http.HttpContent,System.Byte[],System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void HttpWebRequestMessage_WriteToRequestBody_mC61F7A3E1A5C26A892511F9D53B9BC70F49B8711 (void);
// 0x00000379 System.Threading.Tasks.Task`1<System.Net.Http.HttpContent> Amazon.Runtime.HttpWebRequestMessage::GetRequestContentAsync()
extern void HttpWebRequestMessage_GetRequestContentAsync_m261C2DAE58ECF3152277EFC83D28B4EF29B70400 (void);
// 0x0000037A System.Void Amazon.Runtime.HttpWebRequestMessage::WriteContentHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void HttpWebRequestMessage_WriteContentHeaders_mCA24144AE7A97EEE86CD74DFC07C4302DCAFDEE6 (void);
// 0x0000037B System.Void Amazon.Runtime.HttpWebRequestMessage::Dispose()
extern void HttpWebRequestMessage_Dispose_mC202EC06AD67D3B90C975E0FF1C54B5643B0CFC7 (void);
// 0x0000037C System.Void Amazon.Runtime.HttpWebRequestMessage::Dispose(System.Boolean)
extern void HttpWebRequestMessage_Dispose_mDCF6D05433B8B6336A0A8E63D6D7EAE8B7920DA1 (void);
// 0x0000037D System.IO.Stream Amazon.Runtime.HttpWebRequestMessage::SetupProgressListeners(System.IO.Stream,System.Int64,System.Object,System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>)
extern void HttpWebRequestMessage_SetupProgressListeners_mEBE72A5CDA0BFF9649760F55AD61A5FB811C529D (void);
// 0x0000037E System.Void Amazon.Runtime.HttpWebRequestMessage::.cctor()
extern void HttpWebRequestMessage__cctor_m1C34BEC61216B03B1DEEC923357D9F70A43967F1 (void);
// 0x0000037F System.Void Amazon.Runtime.HttpWebRequestMessage/<GetResponseAsync>d__20::MoveNext()
extern void U3CGetResponseAsyncU3Ed__20_MoveNext_m929B5DA511CAF4416ADF67FCD460262B60BE78F8 (void);
// 0x00000380 System.Void Amazon.Runtime.HttpWebRequestMessage/<GetResponseAsync>d__20::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetResponseAsyncU3Ed__20_SetStateMachine_m10865D2EFB43097DB415EFB5DB496B817C850ED5 (void);
// 0x00000381 System.Void Amazon.Runtime.IPipelineHandler::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
// 0x00000382 Amazon.Runtime.IPipelineHandler Amazon.Runtime.IPipelineHandler::get_InnerHandler()
// 0x00000383 System.Void Amazon.Runtime.IPipelineHandler::set_InnerHandler(Amazon.Runtime.IPipelineHandler)
// 0x00000384 Amazon.Runtime.IPipelineHandler Amazon.Runtime.IPipelineHandler::get_OuterHandler()
// 0x00000385 System.Void Amazon.Runtime.IPipelineHandler::set_OuterHandler(Amazon.Runtime.IPipelineHandler)
// 0x00000386 System.Threading.Tasks.Task`1<T> Amazon.Runtime.IPipelineHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000387 System.Int32 Amazon.Runtime.RetryPolicy::get_MaxRetries()
extern void RetryPolicy_get_MaxRetries_mA76CE36A06EFC6A3935BDB64429EA5B6C596BB53 (void);
// 0x00000388 System.Void Amazon.Runtime.RetryPolicy::set_MaxRetries(System.Int32)
extern void RetryPolicy_set_MaxRetries_m36D4FB81E32CF4F8465CF6094FA4743C9D37F273 (void);
// 0x00000389 Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.RetryPolicy::get_Logger()
extern void RetryPolicy_get_Logger_mE0AF8B9D9FC0BD25BBBAE14A26ADF916FF5610F2 (void);
// 0x0000038A System.Void Amazon.Runtime.RetryPolicy::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
extern void RetryPolicy_set_Logger_m68A68C566F9C95673F60129AB1BD87D1EEC8E5A4 (void);
// 0x0000038B System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.RetryPolicy::get_ThrottlingErrorCodes()
extern void RetryPolicy_get_ThrottlingErrorCodes_m2CF20C9AD038CD8F4AA224BDB68351B424A3ED6D (void);
// 0x0000038C System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.RetryPolicy::get_TimeoutErrorCodesToRetryOn()
extern void RetryPolicy_get_TimeoutErrorCodesToRetryOn_mD151D14490561845F443491871D89725648AFFDC (void);
// 0x0000038D System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode> Amazon.Runtime.RetryPolicy::get_HttpStatusCodesToRetryOn()
extern void RetryPolicy_get_HttpStatusCodesToRetryOn_mA009A938783A07A9CEB7717806D9967308C5CA5F (void);
// 0x0000038E System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus> Amazon.Runtime.RetryPolicy::get_WebExceptionStatusesToRetryOn()
extern void RetryPolicy_get_WebExceptionStatusesToRetryOn_m329A25793DD3B758615EA509FA6B03F6A120C330 (void);
// 0x0000038F Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.RetryPolicy::get_RetryCapacity()
extern void RetryPolicy_get_RetryCapacity_m63B7EA20A13720F8E9A9C5C82641A54CEC0E97D3 (void);
// 0x00000390 System.Void Amazon.Runtime.RetryPolicy::set_RetryCapacity(Amazon.Runtime.Internal.RetryCapacity)
extern void RetryPolicy_set_RetryCapacity_m088CC1BB9F5588B6D254B74CFA9F6533E214CA7C (void);
// 0x00000391 System.Boolean Amazon.Runtime.RetryPolicy::CanRetry(Amazon.Runtime.IExecutionContext)
// 0x00000392 System.Boolean Amazon.Runtime.RetryPolicy::RetryLimitReached(Amazon.Runtime.IExecutionContext)
// 0x00000393 System.Void Amazon.Runtime.RetryPolicy::NotifySuccess(Amazon.Runtime.IExecutionContext)
extern void RetryPolicy_NotifySuccess_mF1BF382D1529CB7EFEE5EF0111F4B617D8C49C21 (void);
// 0x00000394 System.Boolean Amazon.Runtime.RetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean)
extern void RetryPolicy_OnRetry_m17CB684AB1AD7DA4F642620BCFD24D2B3FE9B1A3 (void);
// 0x00000395 System.Boolean Amazon.Runtime.RetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean,System.Boolean)
extern void RetryPolicy_OnRetry_mFDE79FA3B43B3324625C020D30BBE7649D862869 (void);
// 0x00000396 System.Boolean Amazon.Runtime.RetryPolicy::IsThrottlingError(System.Exception)
extern void RetryPolicy_IsThrottlingError_mEDA45FB14255BDE672AAF48EC30BCED5FF46AD63 (void);
// 0x00000397 System.Boolean Amazon.Runtime.RetryPolicy::IsTransientError(Amazon.Runtime.IExecutionContext,System.Exception)
extern void RetryPolicy_IsTransientError_m46A215231514296D07C97B3647321A4721ACD621 (void);
// 0x00000398 System.Boolean Amazon.Runtime.RetryPolicy::IsServiceTimeoutError(System.Exception)
extern void RetryPolicy_IsServiceTimeoutError_m8737ABD27DB78C97175669CE54FE2E6F8CD5A704 (void);
// 0x00000399 System.Boolean Amazon.Runtime.RetryPolicy::IsClockskew(Amazon.Runtime.IExecutionContext,System.Exception)
extern void RetryPolicy_IsClockskew_m22FFFD42064A5D7FF027E137345EB5ECF5DB05F2 (void);
// 0x0000039A System.Boolean Amazon.Runtime.RetryPolicy::TryParseDateHeader(Amazon.Runtime.AmazonServiceException,System.DateTime&)
extern void RetryPolicy_TryParseDateHeader_mCF27A2B35632C45F76AF04C8B664E6B06E86BA14 (void);
// 0x0000039B System.Boolean Amazon.Runtime.RetryPolicy::TryParseExceptionMessage(Amazon.Runtime.AmazonServiceException,System.DateTime&)
extern void RetryPolicy_TryParseExceptionMessage_m6A2FADB91AA2589F39C24320B19888060DBEFBE3 (void);
// 0x0000039C Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.RetryPolicy::GetWebData(Amazon.Runtime.AmazonServiceException)
extern void RetryPolicy_GetWebData_m185A6932F19DC8B57128FD63AF43A37136064630 (void);
// 0x0000039D System.Threading.Tasks.Task`1<System.Boolean> Amazon.Runtime.RetryPolicy::RetryAsync(Amazon.Runtime.IExecutionContext,System.Exception)
extern void RetryPolicy_RetryAsync_m3D741AF314A36B2C421CD7717A0A4098F6DA88E4 (void);
// 0x0000039E System.Threading.Tasks.Task Amazon.Runtime.RetryPolicy::ObtainSendTokenAsync(Amazon.Runtime.IExecutionContext,System.Exception)
extern void RetryPolicy_ObtainSendTokenAsync_m712BC6BA91B0559324AC38A62D843F5053324F6A (void);
// 0x0000039F System.Threading.Tasks.Task`1<System.Boolean> Amazon.Runtime.RetryPolicy::RetryForExceptionAsync(Amazon.Runtime.IExecutionContext,System.Exception)
// 0x000003A0 System.Threading.Tasks.Task Amazon.Runtime.RetryPolicy::WaitBeforeRetryAsync(Amazon.Runtime.IExecutionContext)
// 0x000003A1 System.Void Amazon.Runtime.RetryPolicy::.ctor()
extern void RetryPolicy__ctor_mB928C9108D858BA8596B5DDA0A0A84A1FCDE807C (void);
// 0x000003A2 System.Void Amazon.Runtime.RetryPolicy::.cctor()
extern void RetryPolicy__cctor_m8A5CCBC746EBDEFF3BF439B125F040B2F9C2B576 (void);
// 0x000003A3 System.Void Amazon.Runtime.RetryPolicy/<RetryAsync>d__57::MoveNext()
extern void U3CRetryAsyncU3Ed__57_MoveNext_m1678AA3BDDFA116D31DCD6FD44884DEEAF509ECF (void);
// 0x000003A4 System.Void Amazon.Runtime.RetryPolicy/<RetryAsync>d__57::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRetryAsyncU3Ed__57_SetStateMachine_mF497DCFE92BB472842C19A106F09007A860FFF71 (void);
// 0x000003A5 System.Void Amazon.Runtime.PreRequestEventArgs::.ctor()
extern void PreRequestEventArgs__ctor_m9E55322D1287AECED08BB8B8F7E5F140F05335A6 (void);
// 0x000003A6 System.Void Amazon.Runtime.PreRequestEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern void PreRequestEventArgs_set_Request_m913512B993407176BDE17EED7CCDB3B8EA466979 (void);
// 0x000003A7 Amazon.Runtime.PreRequestEventArgs Amazon.Runtime.PreRequestEventArgs::Create(Amazon.Runtime.AmazonWebServiceRequest)
extern void PreRequestEventArgs_Create_m29F3F956990463D3CD7B9D3574D781827A9DE50C (void);
// 0x000003A8 System.Void Amazon.Runtime.PreRequestEventHandler::.ctor(System.Object,System.IntPtr)
extern void PreRequestEventHandler__ctor_m0FA3CF4F9E61954A7D51A1C64939234A4564E3C4 (void);
// 0x000003A9 System.Void Amazon.Runtime.PreRequestEventHandler::Invoke(System.Object,Amazon.Runtime.PreRequestEventArgs)
extern void PreRequestEventHandler_Invoke_mE82C1E06AE4144E0A5E30D23389430C5289A8B8C (void);
// 0x000003AA System.IAsyncResult Amazon.Runtime.PreRequestEventHandler::BeginInvoke(System.Object,Amazon.Runtime.PreRequestEventArgs,System.AsyncCallback,System.Object)
extern void PreRequestEventHandler_BeginInvoke_m21CCD614AF4E7102832B2D200DC0CD2EBF659627 (void);
// 0x000003AB System.Void Amazon.Runtime.PreRequestEventHandler::EndInvoke(System.IAsyncResult)
extern void PreRequestEventHandler_EndInvoke_m470A8927759AA6E6A9B89256F8FCF92E494729E6 (void);
// 0x000003AC System.Void Amazon.Runtime.RequestEventArgs::.ctor()
extern void RequestEventArgs__ctor_m606A1C3A35E8CD95ECBC60A763956C7A9ACF341D (void);
// 0x000003AD System.Void Amazon.Runtime.WebServiceRequestEventArgs::.ctor()
extern void WebServiceRequestEventArgs__ctor_m26CDF2DD6CA813BB2C91A5CF5C99E5F8E85BD575 (void);
// 0x000003AE System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::get_Headers()
extern void WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5 (void);
// 0x000003AF System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Headers(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceRequestEventArgs_set_Headers_m4C4474DD9D0B6975CEE99E3ACD66C38585195278 (void);
// 0x000003B0 System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Parameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceRequestEventArgs_set_Parameters_m304A0C4EF2B41D03A1D35C78411F1240BDE41606 (void);
// 0x000003B1 System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_ParameterCollection(Amazon.Runtime.Internal.ParameterCollection)
extern void WebServiceRequestEventArgs_set_ParameterCollection_m63448435DC566936F2FACB1C97C9DAD1963E2F65 (void);
// 0x000003B2 System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_ServiceName(System.String)
extern void WebServiceRequestEventArgs_set_ServiceName_m0A8B8B8C719817D7B4AC4CC873E84672367B7692 (void);
// 0x000003B3 System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Endpoint(System.Uri)
extern void WebServiceRequestEventArgs_set_Endpoint_m719E6225716C3AD740266C4DB15DDA9F11CD59A0 (void);
// 0x000003B4 System.Void Amazon.Runtime.WebServiceRequestEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern void WebServiceRequestEventArgs_set_Request_m2E93015C782370542C385BF39139235D647C81B6 (void);
// 0x000003B5 Amazon.Runtime.WebServiceRequestEventArgs Amazon.Runtime.WebServiceRequestEventArgs::Create(Amazon.Runtime.Internal.IRequest)
extern void WebServiceRequestEventArgs_Create_mF007FE7AFF928E0F60DC0F2693034B98B19C9227 (void);
// 0x000003B6 System.Void Amazon.Runtime.RequestEventHandler::.ctor(System.Object,System.IntPtr)
extern void RequestEventHandler__ctor_m570E54DC4EF47898C92E313326A679EE57D8E3B5 (void);
// 0x000003B7 System.Void Amazon.Runtime.RequestEventHandler::Invoke(System.Object,Amazon.Runtime.RequestEventArgs)
extern void RequestEventHandler_Invoke_m4D81B0B79F54EE87949AA4FA30AEB42227FDF009 (void);
// 0x000003B8 System.IAsyncResult Amazon.Runtime.RequestEventHandler::BeginInvoke(System.Object,Amazon.Runtime.RequestEventArgs,System.AsyncCallback,System.Object)
extern void RequestEventHandler_BeginInvoke_m78E9AFC9969D387315FCA143266D871435A5488D (void);
// 0x000003B9 System.Void Amazon.Runtime.RequestEventHandler::EndInvoke(System.IAsyncResult)
extern void RequestEventHandler_EndInvoke_m64B9F1FD6B812D7FABFB020ABD9EEC7E9E36FA5A (void);
// 0x000003BA System.Boolean Amazon.Runtime.IMetricsTiming::get_IsFinished()
// 0x000003BB System.TimeSpan Amazon.Runtime.IMetricsTiming::get_ElapsedTime()
// 0x000003BC System.String Amazon.Runtime.IMetricsFormatter::FormatMetrics(Amazon.Runtime.IRequestMetrics)
// 0x000003BD System.Void Amazon.Runtime.ResponseEventHandler::.ctor(System.Object,System.IntPtr)
extern void ResponseEventHandler__ctor_mE95AC96FFDAA846B237B061995D4C2699B822101 (void);
// 0x000003BE System.Void Amazon.Runtime.ResponseEventHandler::Invoke(System.Object,Amazon.Runtime.ResponseEventArgs)
extern void ResponseEventHandler_Invoke_m3669BBD4BD6AED2549E0EF32CADA9F1A3C7901C5 (void);
// 0x000003BF System.IAsyncResult Amazon.Runtime.ResponseEventHandler::BeginInvoke(System.Object,Amazon.Runtime.ResponseEventArgs,System.AsyncCallback,System.Object)
extern void ResponseEventHandler_BeginInvoke_m09DBE6ACF8EF50503D7331FEF4278C7E70A6F20D (void);
// 0x000003C0 System.Void Amazon.Runtime.ResponseEventHandler::EndInvoke(System.IAsyncResult)
extern void ResponseEventHandler_EndInvoke_m340EF4EFBB9BF2F42158CE1C843C1C46F116B18D (void);
// 0x000003C1 System.Void Amazon.Runtime.ResponseEventArgs::.ctor()
extern void ResponseEventArgs__ctor_m0D378EF39FE4823A8AFE6E70F7E2EE2D1EFC430C (void);
// 0x000003C2 System.Void Amazon.Runtime.WebServiceResponseEventArgs::.ctor()
extern void WebServiceResponseEventArgs__ctor_m79D0373CC1E803E2561891D2A45C08223B53F0FE (void);
// 0x000003C3 System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_RequestHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceResponseEventArgs_set_RequestHeaders_m0F160670222462F92D8AC6B4E49400F2C5098EAC (void);
// 0x000003C4 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::get_ResponseHeaders()
extern void WebServiceResponseEventArgs_get_ResponseHeaders_mB3B05CF7F3CA81AD0E15756A6F1785D33BE103DA (void);
// 0x000003C5 System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_ResponseHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceResponseEventArgs_set_ResponseHeaders_m437D99FE61C57DC60BFABE2FD4D5FD1FB6675C5F (void);
// 0x000003C6 System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Parameters(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void WebServiceResponseEventArgs_set_Parameters_m35607FF08CF6574B51713FA9C4B042C9C70DDFA3 (void);
// 0x000003C7 System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_ServiceName(System.String)
extern void WebServiceResponseEventArgs_set_ServiceName_m08E80F5EC7828B0B97D34DA6C95E64A34531EA2C (void);
// 0x000003C8 System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Endpoint(System.Uri)
extern void WebServiceResponseEventArgs_set_Endpoint_m77D2AC3E87BB0C1997933A63273252B383496281 (void);
// 0x000003C9 System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Request(Amazon.Runtime.AmazonWebServiceRequest)
extern void WebServiceResponseEventArgs_set_Request_m74545AC00FF2931AC9D2FEF9F6AB7359459E0722 (void);
// 0x000003CA System.Void Amazon.Runtime.WebServiceResponseEventArgs::set_Response(Amazon.Runtime.AmazonWebServiceResponse)
extern void WebServiceResponseEventArgs_set_Response_m6B8ADF83266A32FAB4DD7EF7C8494BF4E807B251 (void);
// 0x000003CB Amazon.Runtime.WebServiceResponseEventArgs Amazon.Runtime.WebServiceResponseEventArgs::Create(Amazon.Runtime.AmazonWebServiceResponse,Amazon.Runtime.Internal.IRequest,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern void WebServiceResponseEventArgs_Create_m1E2408AEEE67540FF63E713DB0F11F512CB3249C (void);
// 0x000003CC System.String Amazon.Runtime.ResponseMetadata::get_RequestId()
extern void ResponseMetadata_get_RequestId_m28C6345C23135E757E664A3ED9773E2A8CA0A5B2 (void);
// 0x000003CD System.Void Amazon.Runtime.ResponseMetadata::set_RequestId(System.String)
extern void ResponseMetadata_set_RequestId_m411505C12EEDA793D530C7A65CD9C1198D5DFFBB (void);
// 0x000003CE System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.ResponseMetadata::get_Metadata()
extern void ResponseMetadata_get_Metadata_m0B17E9487DF8F850A0D9D130F0EBFCD83BD038C1 (void);
// 0x000003CF System.Void Amazon.Runtime.ResponseMetadata::.ctor()
extern void ResponseMetadata__ctor_mAB772A283F3A4C1F14D1F22A8CC837B34CCCB637 (void);
// 0x000003D0 System.Void Amazon.Runtime.StreamTransferProgressArgs::.ctor(System.Int64,System.Int64,System.Int64)
extern void StreamTransferProgressArgs__ctor_m25C105F8AA3D53FE2B5255E3E0E8320DF282A894 (void);
// 0x000003D1 System.Int32 Amazon.Runtime.StreamTransferProgressArgs::get_PercentDone()
extern void StreamTransferProgressArgs_get_PercentDone_mFFF8215C86D6D191473C82E1F4F527FBB542CCD0 (void);
// 0x000003D2 System.String Amazon.Runtime.StreamTransferProgressArgs::ToString()
extern void StreamTransferProgressArgs_ToString_m06B13291A479B52935A0BDE10EA3E2CC1D6A8596 (void);
// 0x000003D3 System.Void Amazon.Runtime.MonitoringListener::.ctor()
extern void MonitoringListener__ctor_m654872655C419AB72A018161969B5FE14DA1D36A (void);
// 0x000003D4 System.Void Amazon.Runtime.MonitoringListener::.cctor()
extern void MonitoringListener__cctor_m825FDCA988FC906C350CF2DA12742053A2BE6C2D (void);
// 0x000003D5 Amazon.Runtime.MonitoringListener Amazon.Runtime.MonitoringListener::get_Instance()
extern void MonitoringListener_get_Instance_m4F926136E26C14D14A24DB1DC11E7DD09709B3D8 (void);
// 0x000003D6 System.Threading.Tasks.Task Amazon.Runtime.MonitoringListener::PostMessagesOverUDPAsync(System.String)
extern void MonitoringListener_PostMessagesOverUDPAsync_mF045DACA39A35CE18A8D5C15D023D0389686A072 (void);
// 0x000003D7 System.Void Amazon.Runtime.MonitoringListener::Dispose()
extern void MonitoringListener_Dispose_m7793DA0C212776DC5499744CBC0A7EFC0134C118 (void);
// 0x000003D8 System.Void Amazon.Runtime.MonitoringListener::Dispose(System.Boolean)
extern void MonitoringListener_Dispose_m3158048A5161DF8048B15EE847BD97A64B78659E (void);
// 0x000003D9 System.Void Amazon.Runtime.MonitoringListener/<PostMessagesOverUDPAsync>d__10::MoveNext()
extern void U3CPostMessagesOverUDPAsyncU3Ed__10_MoveNext_mEDBD4C9D6810E90196AF654DF1B20F41C6AD1B00 (void);
// 0x000003DA System.Void Amazon.Runtime.MonitoringListener/<PostMessagesOverUDPAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostMessagesOverUDPAsyncU3Ed__10_SetStateMachine_mE5C9EEDE31FEEC56BCF88F148813303D2AA51CAC (void);
// 0x000003DB System.String Amazon.Runtime.CredentialManagement.CredentialProfile::get_Name()
extern void CredentialProfile_get_Name_mB713E06DF9022D5FFE37AD44EB9CCF31B82F60D3 (void);
// 0x000003DC System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_Name(System.String)
extern void CredentialProfile_set_Name_m63BC3AD462D0CF3669C11B75A37B3A72BB68FFDF (void);
// 0x000003DD Amazon.Runtime.CredentialManagement.CredentialProfileOptions Amazon.Runtime.CredentialManagement.CredentialProfile::get_Options()
extern void CredentialProfile_get_Options_m4BEDDAF9941BA979CC731717C17A3FA7623749D3 (void);
// 0x000003DE System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_Options(Amazon.Runtime.CredentialManagement.CredentialProfileOptions)
extern void CredentialProfile_set_Options_m7651DA7DA24A064F6178A6E6EC92DCA807641CFB (void);
// 0x000003DF Amazon.RegionEndpoint Amazon.Runtime.CredentialManagement.CredentialProfile::get_Region()
extern void CredentialProfile_get_Region_mE253F69F7886AD25DA66A653E39109186664EC95 (void);
// 0x000003E0 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_Region(Amazon.RegionEndpoint)
extern void CredentialProfile_set_Region_m44E8C475F95D6C64B4AF534CA00C14267C09A795 (void);
// 0x000003E1 System.Nullable`1<System.Guid> Amazon.Runtime.CredentialManagement.CredentialProfile::get_UniqueKey()
extern void CredentialProfile_get_UniqueKey_mB5E4CE2360F0A7A4ED514F50670DF31C51A7BDFB (void);
// 0x000003E2 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_UniqueKey(System.Nullable`1<System.Guid>)
extern void CredentialProfile_set_UniqueKey_m0EA7516DBFD617FF619D864790F48CC9A653B3DA (void);
// 0x000003E3 System.Nullable`1<System.Boolean> Amazon.Runtime.CredentialManagement.CredentialProfile::get_EndpointDiscoveryEnabled()
extern void CredentialProfile_get_EndpointDiscoveryEnabled_m3DD6687CA952054E30AB5E108C586BDC1A2493DC (void);
// 0x000003E4 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_EndpointDiscoveryEnabled(System.Nullable`1<System.Boolean>)
extern void CredentialProfile_set_EndpointDiscoveryEnabled_m266929EB8A3C01BBDB26483D547F0E2113AC7E97 (void);
// 0x000003E5 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_S3UseArnRegion(System.Nullable`1<System.Boolean>)
extern void CredentialProfile_set_S3UseArnRegion_mD66A32EE76676B6E0D9A398A0D6643F4ED720CCC (void);
// 0x000003E6 System.Nullable`1<Amazon.Runtime.StsRegionalEndpointsValue> Amazon.Runtime.CredentialManagement.CredentialProfile::get_StsRegionalEndpoints()
extern void CredentialProfile_get_StsRegionalEndpoints_m629728B051B1A50E72E33D61D6E94DDC7A162A26 (void);
// 0x000003E7 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_StsRegionalEndpoints(System.Nullable`1<Amazon.Runtime.StsRegionalEndpointsValue>)
extern void CredentialProfile_set_StsRegionalEndpoints_m7AD9C668F3A944E2CBD5A358F3A6918E996F9CD7 (void);
// 0x000003E8 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_S3RegionalEndpoint(System.Nullable`1<Amazon.Runtime.S3UsEast1RegionalEndpointValue>)
extern void CredentialProfile_set_S3RegionalEndpoint_m54FD9379579C26441119FEBBCB3B3F0F21B1DC09 (void);
// 0x000003E9 System.Nullable`1<Amazon.Runtime.RequestRetryMode> Amazon.Runtime.CredentialManagement.CredentialProfile::get_RetryMode()
extern void CredentialProfile_get_RetryMode_m73DB6E189368FE3F055AFEAA9F924EBED95D3A08 (void);
// 0x000003EA System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_RetryMode(System.Nullable`1<Amazon.Runtime.RequestRetryMode>)
extern void CredentialProfile_set_RetryMode_m244DD300B11793D33F00593DD421B8598F1050AE (void);
// 0x000003EB System.Nullable`1<System.Int32> Amazon.Runtime.CredentialManagement.CredentialProfile::get_MaxAttempts()
extern void CredentialProfile_get_MaxAttempts_m3AB54AC420F35373634610FAAE1C62EFDEB190B5 (void);
// 0x000003EC System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_MaxAttempts(System.Nullable`1<System.Int32>)
extern void CredentialProfile_set_MaxAttempts_mF8CFD52861CBC44DCA46556C0E43E68F0FF890A7 (void);
// 0x000003ED System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.CredentialManagement.CredentialProfile::get_Properties()
extern void CredentialProfile_get_Properties_mB2D597C31DD0808A0163AF3521862E607EB2AE46 (void);
// 0x000003EE System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_Properties(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void CredentialProfile_set_Properties_mAF13B979A257CAFA4275C5DA2BA58491899B8154 (void);
// 0x000003EF System.Boolean Amazon.Runtime.CredentialManagement.CredentialProfile::get_CanCreateAWSCredentials()
extern void CredentialProfile_get_CanCreateAWSCredentials_m9B0F5A0E9F3F7332BD7BEA6427263B5E273A3D1C (void);
// 0x000003F0 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::set_CredentialProfileStore(Amazon.Runtime.CredentialManagement.ICredentialProfileStore)
extern void CredentialProfile_set_CredentialProfileStore_m8EA2A883DECA4473D43CE84A3DC15EA7EFD49097 (void);
// 0x000003F1 System.Nullable`1<Amazon.Runtime.CredentialManagement.Internal.CredentialProfileType> Amazon.Runtime.CredentialManagement.CredentialProfile::get_ProfileType()
extern void CredentialProfile_get_ProfileType_mFD862D0784CA967C0E34F7610E8D9971047725C5 (void);
// 0x000003F2 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile::.ctor(System.String,Amazon.Runtime.CredentialManagement.CredentialProfileOptions)
extern void CredentialProfile__ctor_m2BD2CB9D2B7742BF59038E8C6B8B8EF70A72CBFD (void);
// 0x000003F3 System.String Amazon.Runtime.CredentialManagement.CredentialProfile::GetPropertiesString()
extern void CredentialProfile_GetPropertiesString_mAB1975A23364E9D6BECC078872F5BD3C90115D2D (void);
// 0x000003F4 System.String Amazon.Runtime.CredentialManagement.CredentialProfile::ToString()
extern void CredentialProfile_ToString_mCED00D55E2C68AFAB61F9BBB53203A87979411BF (void);
// 0x000003F5 System.Boolean Amazon.Runtime.CredentialManagement.CredentialProfile::Equals(System.Object)
extern void CredentialProfile_Equals_m6B6720C477C6B504D19C3275CE79DF788071FFEA (void);
// 0x000003F6 System.Int32 Amazon.Runtime.CredentialManagement.CredentialProfile::GetHashCode()
extern void CredentialProfile_GetHashCode_mE404618614EE3088BFE003CE7A695E6F851ACA48 (void);
// 0x000003F7 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile/<>c::.cctor()
extern void U3CU3Ec__cctor_m0D34C068838518B558D0F73A46006925242F5617 (void);
// 0x000003F8 System.Void Amazon.Runtime.CredentialManagement.CredentialProfile/<>c::.ctor()
extern void U3CU3Ec__ctor_mB856A3F46409ADD8DC9737A1DBC81E7B4F63E1C2 (void);
// 0x000003F9 System.String Amazon.Runtime.CredentialManagement.CredentialProfile/<>c::<GetPropertiesString>b__59_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CGetPropertiesStringU3Eb__59_0_m122F8CEC220CB294971DE637A4C938995AB94573 (void);
// 0x000003FA System.String Amazon.Runtime.CredentialManagement.CredentialProfile/<>c::<GetPropertiesString>b__59_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CGetPropertiesStringU3Eb__59_1_mF517DD46DC06E589AF7F5DD5239B59259EE2BF69 (void);
// 0x000003FB System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_AccessKey()
extern void CredentialProfileOptions_get_AccessKey_m54D2DE99D522F53500F3CD0FD9388812C2AF6BD3 (void);
// 0x000003FC System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_AccessKey(System.String)
extern void CredentialProfileOptions_set_AccessKey_m8F3AD526CCB0CC1DAD563F0364ADEE05A91B85E9 (void);
// 0x000003FD System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_CredentialSource()
extern void CredentialProfileOptions_get_CredentialSource_m1D564B618015AF6BC155329EF0EE4D6DF8C16A52 (void);
// 0x000003FE System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_CredentialSource(System.String)
extern void CredentialProfileOptions_set_CredentialSource_mE1EF94AB93F049279711F301C2DAEF14F149C0A6 (void);
// 0x000003FF System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_EndpointName()
extern void CredentialProfileOptions_get_EndpointName_mA13C59933C46404D3017FCB826EAC2DDBD5E7E8D (void);
// 0x00000400 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_EndpointName(System.String)
extern void CredentialProfileOptions_set_EndpointName_mA51B88631A57B8BD75F330661072C58FB1961D8C (void);
// 0x00000401 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_ExternalID()
extern void CredentialProfileOptions_get_ExternalID_m64A40A52D928228D64CD8C7B10DBF3101E5CF033 (void);
// 0x00000402 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_ExternalID(System.String)
extern void CredentialProfileOptions_set_ExternalID_mCB2664BFB5E632389C706417724A2D52BBBDE95D (void);
// 0x00000403 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_MfaSerial()
extern void CredentialProfileOptions_get_MfaSerial_m9F3C36B9F19852F4EC2BDA5C61CB7700160053C0 (void);
// 0x00000404 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_MfaSerial(System.String)
extern void CredentialProfileOptions_set_MfaSerial_mA43E21320CC090A5D1F2D2EE9C763ACEDBCE5459 (void);
// 0x00000405 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_RoleArn()
extern void CredentialProfileOptions_get_RoleArn_mD33C1E609E6DC33FCBA1F100DB756B3CE18F6CD0 (void);
// 0x00000406 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_RoleArn(System.String)
extern void CredentialProfileOptions_set_RoleArn_m3705C867F98614A1B66A34EDADBBA009690EEE3A (void);
// 0x00000407 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_RoleSessionName()
extern void CredentialProfileOptions_get_RoleSessionName_m59225087CF2D7BB1A79F122FBB9C3D6DF136F5F3 (void);
// 0x00000408 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_RoleSessionName(System.String)
extern void CredentialProfileOptions_set_RoleSessionName_m31748E87FCE77B513CBCB3D9C8BB9B692493819E (void);
// 0x00000409 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_SecretKey()
extern void CredentialProfileOptions_get_SecretKey_mE9227433E5F455B2B3CCAB940DF1EF346D587439 (void);
// 0x0000040A System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_SecretKey(System.String)
extern void CredentialProfileOptions_set_SecretKey_m3D8CC902AD4C18EBE5F63747675EDDD4536AB7AD (void);
// 0x0000040B System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_SourceProfile()
extern void CredentialProfileOptions_get_SourceProfile_mCF1A8615E110EA66E829E79313B1AA7BAD904D36 (void);
// 0x0000040C System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_SourceProfile(System.String)
extern void CredentialProfileOptions_set_SourceProfile_m9E621D9FD6D7AB1FDD6933D6FC46CC1CD02DB6B8 (void);
// 0x0000040D System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_Token()
extern void CredentialProfileOptions_get_Token_m67BF5840EAD9E17CF3B5741B23F446A8D568F1F5 (void);
// 0x0000040E System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_Token(System.String)
extern void CredentialProfileOptions_set_Token_m9FD9FD44198997D786FF87FE35AF41A0D6BB70BB (void);
// 0x0000040F System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_UserIdentity()
extern void CredentialProfileOptions_get_UserIdentity_m4239DE2133B4968497CE6EA73849891F2B1797B7 (void);
// 0x00000410 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_UserIdentity(System.String)
extern void CredentialProfileOptions_set_UserIdentity_m90374CF6A6BB801369B7089063281017876E835C (void);
// 0x00000411 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_CredentialProcess()
extern void CredentialProfileOptions_get_CredentialProcess_m06365B5DE3931629A94EAFE9651915EF32DCE968 (void);
// 0x00000412 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_CredentialProcess(System.String)
extern void CredentialProfileOptions_set_CredentialProcess_mA6FD2353B05476E70C062F1C9B9A3A0E2A493ED6 (void);
// 0x00000413 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::get_WebIdentityTokenFile()
extern void CredentialProfileOptions_get_WebIdentityTokenFile_m816A1C6AB30B2CF7F3D8C564558BFFDF5C357F21 (void);
// 0x00000414 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::set_WebIdentityTokenFile(System.String)
extern void CredentialProfileOptions_set_WebIdentityTokenFile_mA959CF718B74550D6CB663BD0880B8A635F4A742 (void);
// 0x00000415 System.String Amazon.Runtime.CredentialManagement.CredentialProfileOptions::ToString()
extern void CredentialProfileOptions_ToString_mD89BD0FBC6E76C42D96150A4C4053A58098DD9AE (void);
// 0x00000416 System.Boolean Amazon.Runtime.CredentialManagement.CredentialProfileOptions::Equals(System.Object)
extern void CredentialProfileOptions_Equals_m4D0A5D591E178B91B9F1BD39C8818631B6751609 (void);
// 0x00000417 System.Int32 Amazon.Runtime.CredentialManagement.CredentialProfileOptions::GetHashCode()
extern void CredentialProfileOptions_GetHashCode_m1D89072C1958AF5B39849195488EC516B52BD373 (void);
// 0x00000418 System.Void Amazon.Runtime.CredentialManagement.CredentialProfileOptions::.ctor()
extern void CredentialProfileOptions__ctor_m633D1CD30EB8F55A25AEC00BBC9DFAE5ECCEBD61 (void);
// 0x00000419 System.String Amazon.Runtime.CredentialManagement.CredentialProfileStoreChain::get_ProfilesLocation()
extern void CredentialProfileStoreChain_get_ProfilesLocation_mA618D2CDAF7EBE2C16FFD0F2BB91B7449C0AFA90 (void);
// 0x0000041A System.Void Amazon.Runtime.CredentialManagement.CredentialProfileStoreChain::set_ProfilesLocation(System.String)
extern void CredentialProfileStoreChain_set_ProfilesLocation_m58C470C2CCA2358F7B72478DA1FDE835881E8A77 (void);
// 0x0000041B System.Void Amazon.Runtime.CredentialManagement.CredentialProfileStoreChain::.ctor()
extern void CredentialProfileStoreChain__ctor_mBD8151AFF8C1C777D1CCC32CDB18368BE6A6A9D3 (void);
// 0x0000041C System.Void Amazon.Runtime.CredentialManagement.CredentialProfileStoreChain::.ctor(System.String)
extern void CredentialProfileStoreChain__ctor_mE0D8B61630360070022B476E6C5659BBA3907B27 (void);
// 0x0000041D System.Boolean Amazon.Runtime.CredentialManagement.CredentialProfileStoreChain::TryGetProfile(System.String,Amazon.Runtime.CredentialManagement.CredentialProfile&)
extern void CredentialProfileStoreChain_TryGetProfile_m115E1C55847B63C92C759EE337BC02D4436A45FF (void);
// 0x0000041E System.Boolean Amazon.Runtime.CredentialManagement.ICredentialProfileSource::TryGetProfile(System.String,Amazon.Runtime.CredentialManagement.CredentialProfile&)
// 0x0000041F System.Void Amazon.Runtime.CredentialManagement.NetSDKCredentialsFile::.ctor()
extern void NetSDKCredentialsFile__ctor_mF33C3D60477D87371BA807F37F37CADB7DA8CEDA (void);
// 0x00000420 System.Boolean Amazon.Runtime.CredentialManagement.NetSDKCredentialsFile::TryGetProfile(System.String,Amazon.Runtime.CredentialManagement.CredentialProfile&)
extern void NetSDKCredentialsFile_TryGetProfile_mD3634409916E43015EB7FBD9150BA5D9A5BA9E01 (void);
// 0x00000421 System.Void Amazon.Runtime.CredentialManagement.NetSDKCredentialsFile::.cctor()
extern void NetSDKCredentialsFile__cctor_m923F91DFC39AF9945CCD83EF43E6B915D2130AFD (void);
// 0x00000422 System.Void Amazon.Runtime.CredentialManagement.SharedCredentialsFile::.cctor()
extern void SharedCredentialsFile__cctor_mC2C6C46ED894B10F3E45BAD0C6F4025177FE9607 (void);
// 0x00000423 System.String Amazon.Runtime.CredentialManagement.SharedCredentialsFile::get_FilePath()
extern void SharedCredentialsFile_get_FilePath_m13D49F7E2F5200941148407607A118BA8BD85596 (void);
// 0x00000424 System.Void Amazon.Runtime.CredentialManagement.SharedCredentialsFile::set_FilePath(System.String)
extern void SharedCredentialsFile_set_FilePath_m03BAF421690A3CECA50363586E082EDC0F03901F (void);
// 0x00000425 System.Void Amazon.Runtime.CredentialManagement.SharedCredentialsFile::.ctor(System.String)
extern void SharedCredentialsFile__ctor_m51729BC7BA919C2E22830B0C17F50F77E143354F (void);
// 0x00000426 System.Void Amazon.Runtime.CredentialManagement.SharedCredentialsFile::SetUpFilePath(System.String)
extern void SharedCredentialsFile_SetUpFilePath_m354B20B8157F2B077AF63DF90AB98760EB6D030E (void);
// 0x00000427 System.Boolean Amazon.Runtime.CredentialManagement.SharedCredentialsFile::TryGetProfile(System.String,Amazon.Runtime.CredentialManagement.CredentialProfile&)
extern void SharedCredentialsFile_TryGetProfile_m620C076FF3591E6467424039A123D5B023B744CD (void);
// 0x00000428 System.Void Amazon.Runtime.CredentialManagement.SharedCredentialsFile::Refresh()
extern void SharedCredentialsFile_Refresh_m49D2C93D9DC7D6099298D79A3FE68705A1E1AEEB (void);
// 0x00000429 System.Boolean Amazon.Runtime.CredentialManagement.SharedCredentialsFile::TryGetProfile(System.String,System.Boolean,Amazon.Runtime.CredentialManagement.CredentialProfile&)
extern void SharedCredentialsFile_TryGetProfile_m8758A295A2861E2E17D3871C4627FDB1EB1291FA (void);
// 0x0000042A System.Boolean Amazon.Runtime.CredentialManagement.SharedCredentialsFile::TryGetSection(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void SharedCredentialsFile_TryGetSection_mA87F12C2AF2B0407E147579E79433A642BECDE6E (void);
// 0x0000042B System.Boolean Amazon.Runtime.CredentialManagement.SharedCredentialsFile::IsSupportedProfileType(System.Nullable`1<Amazon.Runtime.CredentialManagement.Internal.CredentialProfileType>)
extern void SharedCredentialsFile_IsSupportedProfileType_m88907B3DB9BC1FB6549EA37ED11459FF4968A666 (void);
// 0x0000042C System.Void Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void CredentialProfilePropertyMapping__ctor_m808A38ABA5C086AC8ECF0A3AC08198DCAB37D74A (void);
// 0x0000042D System.Void Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping::ExtractProfileParts(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Collections.Generic.HashSet`1<System.String>,Amazon.Runtime.CredentialManagement.CredentialProfileOptions&,System.Collections.Generic.Dictionary`2<System.String,System.String>&,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void CredentialProfilePropertyMapping_ExtractProfileParts_m71C5B27F8F649607917478878E5F8CAA1D8340E9 (void);
// 0x0000042E System.Void Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping::.cctor()
extern void CredentialProfilePropertyMapping__cctor_m990A6CD68F7B2C506B48D5C07555A2717340F033 (void);
// 0x0000042F System.Void Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping/<>c::.cctor()
extern void U3CU3Ec__cctor_m5E0267D292EB7FA8706BFC30A5174454BC066A23 (void);
// 0x00000430 System.Void Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping/<>c::.ctor()
extern void U3CU3Ec__ctor_m647B7D43D58F9689AABC207A7E908501B40836A8 (void);
// 0x00000431 System.Boolean Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping/<>c::<.ctor>b__4_0(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__4_0_m4B49B3668FA89288376C2E7AD7DE80AF971DDE85 (void);
// 0x00000432 System.String Amazon.Runtime.CredentialManagement.Internal.CredentialProfilePropertyMapping/<>c::<.cctor>b__11_0(System.Reflection.PropertyInfo)
extern void U3CU3Ec_U3C_cctorU3Eb__11_0_m3F8EAD33F994A7572397261805364DCD235FB706 (void);
// 0x00000433 System.Nullable`1<Amazon.Runtime.CredentialManagement.Internal.CredentialProfileType> Amazon.Runtime.CredentialManagement.Internal.CredentialProfileTypeDetector::DetectProfileType(Amazon.Runtime.CredentialManagement.CredentialProfileOptions)
extern void CredentialProfileTypeDetector_DetectProfileType_mC85E8602C55813422B39F16CED3520D928819327 (void);
// 0x00000434 System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.CredentialManagement.Internal.CredentialProfileTypeDetector::GetPropertyNames(Amazon.Runtime.CredentialManagement.CredentialProfileOptions)
extern void CredentialProfileTypeDetector_GetPropertyNames_m52DCA1D68A9EF8E78D89136007DDB5CD59B05ADF (void);
// 0x00000435 System.Void Amazon.Runtime.CredentialManagement.Internal.CredentialProfileTypeDetector::.cctor()
extern void CredentialProfileTypeDetector__cctor_m7172C236AB6C4D0DDC2FD726A1BB77A6CBE003B9 (void);
// 0x00000436 System.Void Amazon.Runtime.Internal.CapacityManager::Dispose()
extern void CapacityManager_Dispose_mADAA054C494613C69974BE1D2CA882EEE12B9C96 (void);
// 0x00000437 System.Void Amazon.Runtime.Internal.CapacityManager::Dispose(System.Boolean)
extern void CapacityManager_Dispose_mC5123115DA474A4C2A7BC5A95B803F7FBD009CF5 (void);
// 0x00000438 System.Void Amazon.Runtime.Internal.CapacityManager::.ctor(System.Int32,System.Int32,System.Int32)
extern void CapacityManager__ctor_m19D7FBA86E81FFFAF49D7AD3259584E9B817CAE1 (void);
// 0x00000439 System.Void Amazon.Runtime.Internal.CapacityManager::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void CapacityManager__ctor_mAF4B10F1BD7879222AE94A7EEE64C26A7B675632 (void);
// 0x0000043A System.Boolean Amazon.Runtime.Internal.CapacityManager::TryAcquireCapacity(Amazon.Runtime.Internal.RetryCapacity,Amazon.Runtime.Internal.CapacityManager/CapacityType)
extern void CapacityManager_TryAcquireCapacity_mDCAF2DA054EA178DFF1BDDA27CBE770C42407E9C (void);
// 0x0000043B System.Void Amazon.Runtime.Internal.CapacityManager::ReleaseCapacity(Amazon.Runtime.Internal.CapacityManager/CapacityType,Amazon.Runtime.Internal.RetryCapacity)
extern void CapacityManager_ReleaseCapacity_m0344B0A0899653A82A83A86AC0C9A12482587AC8 (void);
// 0x0000043C Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.CapacityManager::GetRetryCapacity(System.String)
extern void CapacityManager_GetRetryCapacity_mBA5E2403E32B6F2669903571EFCCB4605157DF96 (void);
// 0x0000043D System.Boolean Amazon.Runtime.Internal.CapacityManager::TryGetRetryCapacity(System.String,Amazon.Runtime.Internal.RetryCapacity&)
extern void CapacityManager_TryGetRetryCapacity_m62667620B36BBE90401E23072AABBA7630FC4600 (void);
// 0x0000043E Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.CapacityManager::AddNewRetryCapacity(System.String)
extern void CapacityManager_AddNewRetryCapacity_mA988ADFCDBAE5F94E0638F20B151D90EB117F20B (void);
// 0x0000043F System.Void Amazon.Runtime.Internal.CapacityManager::ReleaseCapacity(System.Int32,Amazon.Runtime.Internal.RetryCapacity)
extern void CapacityManager_ReleaseCapacity_m61F8EAC09838A80C2B86C13AE75372A881E94C01 (void);
// 0x00000440 System.Void Amazon.Runtime.Internal.CapacityManager::.cctor()
extern void CapacityManager__cctor_m46CDE29DFF069945BA2E1C120FF3AB6C9A66AAC5 (void);
// 0x00000441 System.Int32 Amazon.Runtime.Internal.RetryCapacity::get_AvailableCapacity()
extern void RetryCapacity_get_AvailableCapacity_mCD9C04903EC7590720F4C00841A1A69DF0628611 (void);
// 0x00000442 System.Void Amazon.Runtime.Internal.RetryCapacity::set_AvailableCapacity(System.Int32)
extern void RetryCapacity_set_AvailableCapacity_mF627A8F4B70ACE8DFB8B93C56CB21DDDAD273261 (void);
// 0x00000443 System.Int32 Amazon.Runtime.Internal.RetryCapacity::get_MaxCapacity()
extern void RetryCapacity_get_MaxCapacity_m39DCE957C8A8D844D2906AC47F6056F1BDCE4F37 (void);
// 0x00000444 System.Void Amazon.Runtime.Internal.RetryCapacity::.ctor(System.Int32)
extern void RetryCapacity__ctor_m4C9B75AB24624164768CF6257CECC4FABC0B6076 (void);
// 0x00000445 System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Required(System.Boolean)
extern void AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6 (void);
// 0x00000446 System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_IsMinSet(System.Boolean)
extern void AWSPropertyAttribute_set_IsMinSet_mF2D9D9E5E5A2AFB8E96933B9A7D1F36BBF0ACAB1 (void);
// 0x00000447 System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Min(System.Int64)
extern void AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B (void);
// 0x00000448 System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_IsMaxSet(System.Boolean)
extern void AWSPropertyAttribute_set_IsMaxSet_m098F9CAB1E60CBA824EF649B7AF3F4180F08A0A6 (void);
// 0x00000449 System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Max(System.Int64)
extern void AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880 (void);
// 0x0000044A System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::.ctor()
extern void AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D (void);
// 0x0000044B System.Void Amazon.Runtime.Internal.DefaultRequest::.ctor(Amazon.Runtime.AmazonWebServiceRequest,System.String)
extern void DefaultRequest__ctor_mCC8FA797C2692B14AAF4BB563BB011A03642DAE3 (void);
// 0x0000044C System.String Amazon.Runtime.Internal.DefaultRequest::get_RequestName()
extern void DefaultRequest_get_RequestName_m0960B241AEAF6E581276C15D813458F427D66DA2 (void);
// 0x0000044D System.String Amazon.Runtime.Internal.DefaultRequest::get_HttpMethod()
extern void DefaultRequest_get_HttpMethod_m9FC095E23BC90C23CEBDF723443DD44F5D132402 (void);
// 0x0000044E System.Void Amazon.Runtime.Internal.DefaultRequest::set_HttpMethod(System.String)
extern void DefaultRequest_set_HttpMethod_m773B4B5664377E728264D8D3C57D36250AE15DA4 (void);
// 0x0000044F System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_UseQueryString()
extern void DefaultRequest_get_UseQueryString_m3931BD2C759651F4B96BCA62F51B928DA32E6D8D (void);
// 0x00000450 Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.DefaultRequest::get_OriginalRequest()
extern void DefaultRequest_get_OriginalRequest_m06E93CA8E0C70CDB6D3F008DC7F21ACC85EE9C9A (void);
// 0x00000451 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_Headers()
extern void DefaultRequest_get_Headers_mC1510D36397AEBD9B7CDE4197BD4882A9755962A (void);
// 0x00000452 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_Parameters()
extern void DefaultRequest_get_Parameters_m27D82D2B212D91834CAFFBBE323FEF2038D34AD2 (void);
// 0x00000453 Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.Internal.DefaultRequest::get_ParameterCollection()
extern void DefaultRequest_get_ParameterCollection_m4A30C2F0C3461104F5BFF1C09572ECEE3C91838B (void);
// 0x00000454 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_SubResources()
extern void DefaultRequest_get_SubResources_m6934358912AB0465F91B635DC8E96524B233A575 (void);
// 0x00000455 System.Uri Amazon.Runtime.Internal.DefaultRequest::get_Endpoint()
extern void DefaultRequest_get_Endpoint_m845E94EAE98049DAE107DF4C91D380CDBB77D2B7 (void);
// 0x00000456 System.Void Amazon.Runtime.Internal.DefaultRequest::set_Endpoint(System.Uri)
extern void DefaultRequest_set_Endpoint_mDC75E73C52BD13AB3672E044750EC6CEDD7267A5 (void);
// 0x00000457 System.String Amazon.Runtime.Internal.DefaultRequest::get_ResourcePath()
extern void DefaultRequest_get_ResourcePath_m1D95E3E7D008895DD98BB39945810B4CAF8357B9 (void);
// 0x00000458 System.Void Amazon.Runtime.Internal.DefaultRequest::set_ResourcePath(System.String)
extern void DefaultRequest_set_ResourcePath_m279D4BC36B238F6786FEE6FF1891BAF886BB13B8 (void);
// 0x00000459 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::get_PathResources()
extern void DefaultRequest_get_PathResources_m9786E119DC798D2929393A27311B1818E7332543 (void);
// 0x0000045A System.Int32 Amazon.Runtime.Internal.DefaultRequest::get_MarshallerVersion()
extern void DefaultRequest_get_MarshallerVersion_mCA53CC55FEAC354A864D3E89B51DD15E0FDD12B8 (void);
// 0x0000045B System.Void Amazon.Runtime.Internal.DefaultRequest::set_MarshallerVersion(System.Int32)
extern void DefaultRequest_set_MarshallerVersion_mE7F265B1EB7AF6805EB1EF4BBD338728B42FB14E (void);
// 0x0000045C System.Byte[] Amazon.Runtime.Internal.DefaultRequest::get_Content()
extern void DefaultRequest_get_Content_mA0E5E2A2E2C9D84D90941D463D60476153CFABC5 (void);
// 0x0000045D System.Void Amazon.Runtime.Internal.DefaultRequest::set_Content(System.Byte[])
extern void DefaultRequest_set_Content_mD1D65AB53D468885128597B28113AD91B5124DBE (void);
// 0x0000045E System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_SetContentFromParameters()
extern void DefaultRequest_get_SetContentFromParameters_mB03C1E564A7BB85E7B1840F5EE7918AEBE977309 (void);
// 0x0000045F System.Void Amazon.Runtime.Internal.DefaultRequest::set_SetContentFromParameters(System.Boolean)
extern void DefaultRequest_set_SetContentFromParameters_mD83CB98B12FDC98258DDBE891F195215600A56DD (void);
// 0x00000460 System.IO.Stream Amazon.Runtime.Internal.DefaultRequest::get_ContentStream()
extern void DefaultRequest_get_ContentStream_m0B8B56F28A65CE6C877F9BF38A260A3E63D3B815 (void);
// 0x00000461 System.Int64 Amazon.Runtime.Internal.DefaultRequest::get_OriginalStreamPosition()
extern void DefaultRequest_get_OriginalStreamPosition_m23A07C35FDAEED501ADC8B540525DE5960EDCDEF (void);
// 0x00000462 System.String Amazon.Runtime.Internal.DefaultRequest::ComputeContentStreamHash()
extern void DefaultRequest_ComputeContentStreamHash_m91DCD02670498A8220E323C3A38E5A5DD690365A (void);
// 0x00000463 System.String Amazon.Runtime.Internal.DefaultRequest::get_ServiceName()
extern void DefaultRequest_get_ServiceName_m60713D0D8427B1C943FEC2F1C0CFB3A981337B32 (void);
// 0x00000464 Amazon.RegionEndpoint Amazon.Runtime.Internal.DefaultRequest::get_AlternateEndpoint()
extern void DefaultRequest_get_AlternateEndpoint_mD083189EF0A1562A97FD51676E83D113A4962C76 (void);
// 0x00000465 System.String Amazon.Runtime.Internal.DefaultRequest::get_HostPrefix()
extern void DefaultRequest_get_HostPrefix_m10D9DC3D8FB879A77D66612DABA751217A92A03A (void);
// 0x00000466 System.Void Amazon.Runtime.Internal.DefaultRequest::set_HostPrefix(System.String)
extern void DefaultRequest_set_HostPrefix_m6E4E12A783326EF99A979ECF1992D7C4AD28E1B5 (void);
// 0x00000467 System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_Suppress404Exceptions()
extern void DefaultRequest_get_Suppress404Exceptions_m9EBECC5B37C54D8027C015CE9A8E4C8714E02398 (void);
// 0x00000468 Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.DefaultRequest::get_AWS4SignerResult()
extern void DefaultRequest_get_AWS4SignerResult_m2E075FE2E9038A89E9EB365206B9101FA627EF8C (void);
// 0x00000469 System.Boolean Amazon.Runtime.Internal.DefaultRequest::get_UseChunkEncoding()
extern void DefaultRequest_get_UseChunkEncoding_m5375AF8E3FF31E65C632F3DEAD6E859885ECF329 (void);
// 0x0000046A System.Void Amazon.Runtime.Internal.DefaultRequest::set_UseSigV4(System.Boolean)
extern void DefaultRequest_set_UseSigV4_m4CA850E5CD4FB0F28EF2408B6D443B731AF10C7D (void);
// 0x0000046B System.String Amazon.Runtime.Internal.DefaultRequest::get_AuthenticationRegion()
extern void DefaultRequest_get_AuthenticationRegion_mEC4FD9325E607789DA3E0AB657F4997D1EBDCFFA (void);
// 0x0000046C System.Void Amazon.Runtime.Internal.DefaultRequest::set_AuthenticationRegion(System.String)
extern void DefaultRequest_set_AuthenticationRegion_m7390CFC4C888DA4876576238BF48C2E5884028D4 (void);
// 0x0000046D System.String Amazon.Runtime.Internal.DefaultRequest::get_DeterminedSigningRegion()
extern void DefaultRequest_get_DeterminedSigningRegion_mF6982E156E6F30E8611F4672B86241104D76C8DD (void);
// 0x0000046E System.Void Amazon.Runtime.Internal.DefaultRequest::set_DeterminedSigningRegion(System.String)
extern void DefaultRequest_set_DeterminedSigningRegion_m3DF45EF335C58F20A251970CE54E8E61C842AD38 (void);
// 0x0000046F System.Boolean Amazon.Runtime.Internal.DefaultRequest::IsRequestStreamRewindable()
extern void DefaultRequest_IsRequestStreamRewindable_mEB9DCABC62BBB38147555F5C0875D46837914041 (void);
// 0x00000470 System.Boolean Amazon.Runtime.Internal.DefaultRequest::MayContainRequestBody()
extern void DefaultRequest_MayContainRequestBody_m543A5B89D29195C6F706DBACB0C0E0353FF787A4 (void);
// 0x00000471 System.Boolean Amazon.Runtime.Internal.DefaultRequest::HasRequestBody()
extern void DefaultRequest_HasRequestBody_m3993A6C9EBAFDF99C940667E41C297FC48E78BB7 (void);
// 0x00000472 System.String Amazon.Runtime.Internal.DefaultRequest::GetHeaderValue(System.String)
extern void DefaultRequest_GetHeaderValue_m144A8CD7DF9F45713368E043911C827A185EB621 (void);
// 0x00000473 System.Void Amazon.Runtime.Internal.DefaultRequest/<>c::.cctor()
extern void U3CU3Ec__cctor_m219E434C34F94BA18E02466FCCC89CE09CFDE904 (void);
// 0x00000474 System.Void Amazon.Runtime.Internal.DefaultRequest/<>c::.ctor()
extern void U3CU3Ec__ctor_m781F38E383FE02F45DEC7EDD4994D08F67056453 (void);
// 0x00000475 System.Boolean Amazon.Runtime.Internal.DefaultRequest/<>c::<ComputeContentStreamHash>b__68_0(System.IO.Stream)
extern void U3CU3Ec_U3CComputeContentStreamHashU3Eb__68_0_mA6FF4428A581D9CF6FF0F5C85AE7839DD274C512 (void);
// 0x00000476 System.String Amazon.Runtime.Internal.DiscoveryEndpointBase::get_Address()
extern void DiscoveryEndpointBase_get_Address_m477487CD6AC2144E8292EAF321ED01AC31A8FAA4 (void);
// 0x00000477 System.Collections.Generic.SortedDictionary`2<System.String,System.String> Amazon.Runtime.Internal.EndpointDiscoveryDataBase::get_Identifiers()
extern void EndpointDiscoveryDataBase_get_Identifiers_m811566AB6803F875E4981394FB8B3E7EA5CB06BB (void);
// 0x00000478 System.Void Amazon.Runtime.Internal.EndpointDiscoveryResolverBase::.ctor(Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.Logger)
extern void EndpointDiscoveryResolverBase__ctor_m8DDCBABF364689A3448107D1790F14AF803C0E5D (void);
// 0x00000479 System.Void Amazon.Runtime.Internal.EndpointDiscoveryResolver::.ctor(Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.Logger)
extern void EndpointDiscoveryResolver__ctor_m5C1170C8ECE0E17EA9AF08C2C865685507023750 (void);
// 0x0000047A System.Void Amazon.Runtime.Internal.EndpointOperationContextBase::.ctor(System.String,System.String,Amazon.Runtime.Internal.EndpointDiscoveryDataBase,System.Boolean,System.Uri)
extern void EndpointOperationContextBase__ctor_mBBE00029F9394734536361C9D016A0600878CCD2 (void);
// 0x0000047B System.Void Amazon.Runtime.Internal.EndpointOperationContext::.ctor(System.String,System.String,Amazon.Runtime.Internal.EndpointDiscoveryDataBase,System.Boolean,System.Uri)
extern void EndpointOperationContext__ctor_m04D78DDCE20117D4C1046F505C1F8BA20BC3C5D7 (void);
// 0x0000047C Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::get_Type()
extern void ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06 (void);
// 0x0000047D System.Void Amazon.Runtime.Internal.ErrorResponse::set_Type(Amazon.Runtime.ErrorType)
extern void ErrorResponse_set_Type_mE53555DD966531864E2D76017ACFB4BEBBACBBDC (void);
// 0x0000047E System.String Amazon.Runtime.Internal.ErrorResponse::get_Code()
extern void ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD (void);
// 0x0000047F System.Void Amazon.Runtime.Internal.ErrorResponse::set_Code(System.String)
extern void ErrorResponse_set_Code_mB45D11E4AC2905CC68C3703BD276AA7D7FF352C4 (void);
// 0x00000480 System.String Amazon.Runtime.Internal.ErrorResponse::get_Message()
extern void ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188 (void);
// 0x00000481 System.Void Amazon.Runtime.Internal.ErrorResponse::set_Message(System.String)
extern void ErrorResponse_set_Message_m7C450B5EFD404E19C84070BF859275A7F95C595C (void);
// 0x00000482 System.String Amazon.Runtime.Internal.ErrorResponse::get_RequestId()
extern void ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34 (void);
// 0x00000483 System.Void Amazon.Runtime.Internal.ErrorResponse::set_RequestId(System.String)
extern void ErrorResponse_set_RequestId_m7469C3C6D31632D79BBB4C5A6AB456D311D0FD41 (void);
// 0x00000484 System.Exception Amazon.Runtime.Internal.ErrorResponse::get_InnerException()
extern void ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6 (void);
// 0x00000485 System.Void Amazon.Runtime.Internal.ErrorResponse::set_InnerException(System.Exception)
extern void ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF (void);
// 0x00000486 System.Net.HttpStatusCode Amazon.Runtime.Internal.ErrorResponse::get_StatusCode()
extern void ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461 (void);
// 0x00000487 System.Void Amazon.Runtime.Internal.ErrorResponse::set_StatusCode(System.Net.HttpStatusCode)
extern void ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156 (void);
// 0x00000488 System.Void Amazon.Runtime.Internal.ErrorResponse::.ctor()
extern void ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C (void);
// 0x00000489 System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.Internal.IAmazonWebServiceRequest::get_StreamUploadProgressCallback()
// 0x0000048A System.Boolean Amazon.Runtime.Internal.IAmazonWebServiceRequest::get_UseSigV4()
// 0x0000048B System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.InternalConfiguration::get_EndpointDiscoveryEnabled()
extern void InternalConfiguration_get_EndpointDiscoveryEnabled_mCE81E1B1BC2AD1FA28359A0C9132350EF0A94D07 (void);
// 0x0000048C System.Void Amazon.Runtime.Internal.InternalConfiguration::set_EndpointDiscoveryEnabled(System.Nullable`1<System.Boolean>)
extern void InternalConfiguration_set_EndpointDiscoveryEnabled_mDE3516A6EB931D9DE0ED43832E3CD9D32F4E0AF3 (void);
// 0x0000048D System.Nullable`1<Amazon.Runtime.RequestRetryMode> Amazon.Runtime.Internal.InternalConfiguration::get_RetryMode()
extern void InternalConfiguration_get_RetryMode_m85D04441ACF031F843F40340FE4D2B269735297E (void);
// 0x0000048E System.Void Amazon.Runtime.Internal.InternalConfiguration::set_RetryMode(System.Nullable`1<Amazon.Runtime.RequestRetryMode>)
extern void InternalConfiguration_set_RetryMode_mAC0C563C32C2EC4FB8005DE17A71A95DE47907C4 (void);
// 0x0000048F System.Nullable`1<System.Int32> Amazon.Runtime.Internal.InternalConfiguration::get_MaxAttempts()
extern void InternalConfiguration_get_MaxAttempts_mA1F79F4677829F49F656D48A56F8E012B27BEBF2 (void);
// 0x00000490 System.Void Amazon.Runtime.Internal.InternalConfiguration::set_MaxAttempts(System.Nullable`1<System.Int32>)
extern void InternalConfiguration_set_MaxAttempts_m16CC99498CF834DD7FBF28FF931D323333B44775 (void);
// 0x00000491 System.Void Amazon.Runtime.Internal.InternalConfiguration::.ctor()
extern void InternalConfiguration__ctor_m8BBCE021F0134246225C316719FE673862E07074 (void);
// 0x00000492 System.Void Amazon.Runtime.Internal.EnvironmentVariableInternalConfiguration::.ctor()
extern void EnvironmentVariableInternalConfiguration__ctor_mB1C8F4EBE75AB65779B5E87B856BD76C56220A4D (void);
// 0x00000493 System.Boolean Amazon.Runtime.Internal.EnvironmentVariableInternalConfiguration::TryGetEnvironmentVariable(System.String,System.String&)
extern void EnvironmentVariableInternalConfiguration_TryGetEnvironmentVariable_m22C913542F67A5F58F68F3D343E076BB8D6FBB29 (void);
// 0x00000494 System.Nullable`1<T> Amazon.Runtime.Internal.EnvironmentVariableInternalConfiguration::GetEnvironmentVariable(System.String)
// 0x00000495 System.Void Amazon.Runtime.Internal.ProfileInternalConfiguration::.ctor(Amazon.Runtime.CredentialManagement.ICredentialProfileSource)
extern void ProfileInternalConfiguration__ctor_m9B419E3A67C3B3BF25C275AAD07445086F319BC9 (void);
// 0x00000496 System.Void Amazon.Runtime.Internal.ProfileInternalConfiguration::Setup(Amazon.Runtime.CredentialManagement.ICredentialProfileSource,System.String)
extern void ProfileInternalConfiguration_Setup_m6D79F0188907415C288D3532747498F4FC09EA48 (void);
// 0x00000497 System.Void Amazon.Runtime.Internal.FallbackInternalConfigurationFactory::.cctor()
extern void FallbackInternalConfigurationFactory__cctor_m0D712020F71D5ABCC8878850D16CF3491F5DEE49 (void);
// 0x00000498 System.Void Amazon.Runtime.Internal.FallbackInternalConfigurationFactory::Reset()
extern void FallbackInternalConfigurationFactory_Reset_mA02AAAAD24F8EB9B505EB543D9736D16E261EBA0 (void);
// 0x00000499 System.Nullable`1<T> Amazon.Runtime.Internal.FallbackInternalConfigurationFactory::SeekValue(System.Collections.Generic.List`1<Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/ConfigGenerator>,System.Func`2<Amazon.Runtime.Internal.InternalConfiguration,System.Nullable`1<T>>)
// 0x0000049A System.Nullable`1<Amazon.Runtime.RequestRetryMode> Amazon.Runtime.Internal.FallbackInternalConfigurationFactory::get_RetryMode()
extern void FallbackInternalConfigurationFactory_get_RetryMode_m9C4E3FEA407729E28AC31A35065398CD1FCD63BA (void);
// 0x0000049B System.Nullable`1<System.Int32> Amazon.Runtime.Internal.FallbackInternalConfigurationFactory::get_MaxAttempts()
extern void FallbackInternalConfigurationFactory_get_MaxAttempts_m6D112251D442489B5C15AF13714653A424D3C6F4 (void);
// 0x0000049C System.Void Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/ConfigGenerator::.ctor(System.Object,System.IntPtr)
extern void ConfigGenerator__ctor_mFD796D73FB51679A1F6D77CB8E4047B6AF787E7F (void);
// 0x0000049D Amazon.Runtime.Internal.InternalConfiguration Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/ConfigGenerator::Invoke()
extern void ConfigGenerator_Invoke_mD930904ACB5D86DA058039DE5275440E7230177A (void);
// 0x0000049E System.IAsyncResult Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/ConfigGenerator::BeginInvoke(System.AsyncCallback,System.Object)
extern void ConfigGenerator_BeginInvoke_m9269D9BCAE8AC139F73D7A2DA956F53DDB473686 (void);
// 0x0000049F Amazon.Runtime.Internal.InternalConfiguration Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/ConfigGenerator::EndInvoke(System.IAsyncResult)
extern void ConfigGenerator_EndInvoke_m67D13F7B8C485E8C8FE7979E6BE954D5CD99F695 (void);
// 0x000004A0 System.Void Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF9905E1013D0D1B61A0265846F5385FDC2B9C2DE (void);
// 0x000004A1 Amazon.Runtime.Internal.InternalConfiguration Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c__DisplayClass4_0::<Reset>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CResetU3Eb__3_m2D72D5EE0BD4283D73D54DF76ED41A1242280C8F (void);
// 0x000004A2 Amazon.Runtime.Internal.InternalConfiguration Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c__DisplayClass4_0::<Reset>b__4()
extern void U3CU3Ec__DisplayClass4_0_U3CResetU3Eb__4_m0258D79B4B282ADC7AD4689F54F8BAE1F420763D (void);
// 0x000004A3 System.Void Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c::.cctor()
extern void U3CU3Ec__cctor_m95E37308F75F94A0E6C5682EB2461BF0F654D3ED (void);
// 0x000004A4 System.Void Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c::.ctor()
extern void U3CU3Ec__ctor_m19E4948641CC27025FF54FB466F763C81E45B73E (void);
// 0x000004A5 System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c::<Reset>b__4_0(Amazon.Runtime.Internal.InternalConfiguration)
extern void U3CU3Ec_U3CResetU3Eb__4_0_mF25A1FB57CE6CA69DD9D07ADE16F1B76D46C3B0B (void);
// 0x000004A6 System.Nullable`1<Amazon.Runtime.RequestRetryMode> Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c::<Reset>b__4_1(Amazon.Runtime.Internal.InternalConfiguration)
extern void U3CU3Ec_U3CResetU3Eb__4_1_mCFE46EBC3CFD6AB20267B2F393C6A683E2465AE2 (void);
// 0x000004A7 System.Nullable`1<System.Int32> Amazon.Runtime.Internal.FallbackInternalConfigurationFactory/<>c::<Reset>b__4_2(Amazon.Runtime.Internal.InternalConfiguration)
extern void U3CU3Ec_U3CResetU3Eb__4_2_mD0E22CEDAF497BE380B06736E3BAB23BE9DC1980 (void);
// 0x000004A8 System.Void Amazon.Runtime.Internal.EndpointOperationDelegate::.ctor(System.Object,System.IntPtr)
extern void EndpointOperationDelegate__ctor_m0F9F904B122AF0C0641DB7BB34006EB8402ECC63 (void);
// 0x000004A9 System.Collections.Generic.IEnumerable`1<Amazon.Runtime.Internal.DiscoveryEndpointBase> Amazon.Runtime.Internal.EndpointOperationDelegate::Invoke(Amazon.Runtime.Internal.EndpointOperationContextBase)
extern void EndpointOperationDelegate_Invoke_m8D9F9EB0EAF2AB1BD2C6251B873DB6D498374E78 (void);
// 0x000004AA System.IAsyncResult Amazon.Runtime.Internal.EndpointOperationDelegate::BeginInvoke(Amazon.Runtime.Internal.EndpointOperationContextBase,System.AsyncCallback,System.Object)
extern void EndpointOperationDelegate_BeginInvoke_mD7B5641417D64DBE5C0DDF6AFE641C821CE99E8F (void);
// 0x000004AB System.Collections.Generic.IEnumerable`1<Amazon.Runtime.Internal.DiscoveryEndpointBase> Amazon.Runtime.Internal.EndpointOperationDelegate::EndInvoke(System.IAsyncResult)
extern void EndpointOperationDelegate_EndInvoke_mE3774E72F97D7E9992966F1E366A0C48D30DAD3F (void);
// 0x000004AC System.Void Amazon.Runtime.Internal.InvokeOptionsBase::.ctor()
extern void InvokeOptionsBase__ctor_m61BE9BE2BB377F74B5265537270D1152C509C9F6 (void);
// 0x000004AD Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.InvokeOptionsBase::get_RequestMarshaller()
extern void InvokeOptionsBase_get_RequestMarshaller_mF638ABC860AF48DEDF250B8A0C4B34F6006838D9 (void);
// 0x000004AE System.Void Amazon.Runtime.Internal.InvokeOptionsBase::set_RequestMarshaller(Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>)
extern void InvokeOptionsBase_set_RequestMarshaller_m8F3F59E9E9E5F5BFD51CDA1B87986151F34B2EDA (void);
// 0x000004AF Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.Internal.InvokeOptionsBase::get_ResponseUnmarshaller()
extern void InvokeOptionsBase_get_ResponseUnmarshaller_m9E52A88C379AA1F997073F599691071A64252252 (void);
// 0x000004B0 System.Void Amazon.Runtime.Internal.InvokeOptionsBase::set_ResponseUnmarshaller(Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
extern void InvokeOptionsBase_set_ResponseUnmarshaller_m80053CE2242C5FB45F67617A493D4D15F3074FA7 (void);
// 0x000004B1 Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.EndpointDiscoveryDataBase,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.InvokeOptionsBase::get_EndpointDiscoveryMarshaller()
extern void InvokeOptionsBase_get_EndpointDiscoveryMarshaller_m6DF8A9D7F49C84CC812D39D35BD81B23A8DAAB5C (void);
// 0x000004B2 Amazon.Runtime.Internal.EndpointOperationDelegate Amazon.Runtime.Internal.InvokeOptionsBase::get_EndpointOperation()
extern void InvokeOptionsBase_get_EndpointOperation_m394BC4A6DE1373AE1D269CAF359E05A44C5E12C2 (void);
// 0x000004B3 System.Void Amazon.Runtime.Internal.InvokeOptions::.ctor()
extern void InvokeOptions__ctor_mC8DD478F8404260C05FD2E6FF4D26C944E216AB4 (void);
// 0x000004B4 System.String Amazon.Runtime.Internal.IRequest::get_RequestName()
// 0x000004B5 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers()
// 0x000004B6 System.Boolean Amazon.Runtime.Internal.IRequest::get_UseQueryString()
// 0x000004B7 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Parameters()
// 0x000004B8 Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.Internal.IRequest::get_ParameterCollection()
// 0x000004B9 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_SubResources()
// 0x000004BA System.String Amazon.Runtime.Internal.IRequest::get_HttpMethod()
// 0x000004BB System.Void Amazon.Runtime.Internal.IRequest::set_HttpMethod(System.String)
// 0x000004BC System.Uri Amazon.Runtime.Internal.IRequest::get_Endpoint()
// 0x000004BD System.Void Amazon.Runtime.Internal.IRequest::set_Endpoint(System.Uri)
// 0x000004BE System.String Amazon.Runtime.Internal.IRequest::get_ResourcePath()
// 0x000004BF System.Void Amazon.Runtime.Internal.IRequest::set_ResourcePath(System.String)
// 0x000004C0 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_PathResources()
// 0x000004C1 System.Int32 Amazon.Runtime.Internal.IRequest::get_MarshallerVersion()
// 0x000004C2 System.Void Amazon.Runtime.Internal.IRequest::set_MarshallerVersion(System.Int32)
// 0x000004C3 System.Byte[] Amazon.Runtime.Internal.IRequest::get_Content()
// 0x000004C4 System.Void Amazon.Runtime.Internal.IRequest::set_Content(System.Byte[])
// 0x000004C5 System.String Amazon.Runtime.Internal.IRequest::GetHeaderValue(System.String)
// 0x000004C6 System.Boolean Amazon.Runtime.Internal.IRequest::get_SetContentFromParameters()
// 0x000004C7 System.Void Amazon.Runtime.Internal.IRequest::set_SetContentFromParameters(System.Boolean)
// 0x000004C8 System.IO.Stream Amazon.Runtime.Internal.IRequest::get_ContentStream()
// 0x000004C9 System.Int64 Amazon.Runtime.Internal.IRequest::get_OriginalStreamPosition()
// 0x000004CA System.String Amazon.Runtime.Internal.IRequest::ComputeContentStreamHash()
// 0x000004CB System.String Amazon.Runtime.Internal.IRequest::get_ServiceName()
// 0x000004CC Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.IRequest::get_OriginalRequest()
// 0x000004CD Amazon.RegionEndpoint Amazon.Runtime.Internal.IRequest::get_AlternateEndpoint()
// 0x000004CE System.String Amazon.Runtime.Internal.IRequest::get_HostPrefix()
// 0x000004CF System.Boolean Amazon.Runtime.Internal.IRequest::get_Suppress404Exceptions()
// 0x000004D0 Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.IRequest::get_AWS4SignerResult()
// 0x000004D1 System.Boolean Amazon.Runtime.Internal.IRequest::get_UseChunkEncoding()
// 0x000004D2 System.String Amazon.Runtime.Internal.IRequest::get_AuthenticationRegion()
// 0x000004D3 System.Void Amazon.Runtime.Internal.IRequest::set_AuthenticationRegion(System.String)
// 0x000004D4 System.String Amazon.Runtime.Internal.IRequest::get_DeterminedSigningRegion()
// 0x000004D5 System.Void Amazon.Runtime.Internal.IRequest::set_DeterminedSigningRegion(System.String)
// 0x000004D6 System.Boolean Amazon.Runtime.Internal.IRequest::IsRequestStreamRewindable()
// 0x000004D7 System.Boolean Amazon.Runtime.Internal.IRequest::MayContainRequestBody()
// 0x000004D8 System.Boolean Amazon.Runtime.Internal.IRequest::HasRequestBody()
// 0x000004D9 System.String Amazon.Runtime.Internal.IServiceMetadata::get_ServiceId()
// 0x000004DA System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IServiceMetadata::get_OperationNameMapping()
// 0x000004DB System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::.ctor(Amazon.Runtime.Internal.ParameterCollection)
extern void ParametersDictionaryFacade__ctor_mA46D0CC4803CFA0532227183E0BD911A150CF2DE (void);
// 0x000004DC System.String Amazon.Runtime.Internal.ParametersDictionaryFacade::ParameterValueToString(Amazon.Runtime.ParameterValue)
extern void ParametersDictionaryFacade_ParameterValueToString_mDC76C12BDFC47DF01C1A1678FF49B2C693BC7E33 (void);
// 0x000004DD System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::UpdateParameterValue(Amazon.Runtime.ParameterValue,System.String)
extern void ParametersDictionaryFacade_UpdateParameterValue_mECC3B2412F1BBE125124B05F5FEFC91B39AD93B1 (void);
// 0x000004DE System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::Add(System.String,System.String)
extern void ParametersDictionaryFacade_Add_mF25984CC26DDB348535108D6CFEF7DFCE470386E (void);
// 0x000004DF System.Int32 Amazon.Runtime.Internal.ParametersDictionaryFacade::get_Count()
extern void ParametersDictionaryFacade_get_Count_mB2B9E778B9DB3AB12B887A3F45364B72E1D659BA (void);
// 0x000004E0 System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade::ContainsKey(System.String)
extern void ParametersDictionaryFacade_ContainsKey_m544509E254D2B91361E3DD3C24087A7078A0B40D (void);
// 0x000004E1 System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade::Remove(System.String)
extern void ParametersDictionaryFacade_Remove_m128D7A50B77DADF41ADCFBEC71E3AACD74C2803F (void);
// 0x000004E2 System.String Amazon.Runtime.Internal.ParametersDictionaryFacade::get_Item(System.String)
extern void ParametersDictionaryFacade_get_Item_m39F1E34DE4273C9E21CC28765BB1046006FD19AE (void);
// 0x000004E3 System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::set_Item(System.String,System.String)
extern void ParametersDictionaryFacade_set_Item_m7E018D41A02016019CE13E533181E5B0D9EFEECD (void);
// 0x000004E4 System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.Internal.ParametersDictionaryFacade::get_Keys()
extern void ParametersDictionaryFacade_get_Keys_m2407889BD7B5A6593D0BE8ADE3A20FF1B3093CA1 (void);
// 0x000004E5 System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade::TryGetValue(System.String,System.String&)
extern void ParametersDictionaryFacade_TryGetValue_m42E0E06216BA90722DA938AB9CD473E436B6E248 (void);
// 0x000004E6 System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void ParametersDictionaryFacade_Remove_m085FF61C83511C16A76960801C6265E5982F258D (void);
// 0x000004E7 System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.Internal.ParametersDictionaryFacade::get_Values()
extern void ParametersDictionaryFacade_get_Values_m1C1D6EAE66DE5BE9E80D70BCB9BC02B6527934F0 (void);
// 0x000004E8 System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void ParametersDictionaryFacade_Add_m7343C37917CEC82C6BF41A79A63900FC12948860 (void);
// 0x000004E9 System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void ParametersDictionaryFacade_Contains_m4857E89F63A65BCDBBE30C4F92F57CCA3EFD22E5 (void);
// 0x000004EA System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.String>[],System.Int32)
extern void ParametersDictionaryFacade_CopyTo_m441A00076F7E8EC0182D326EFF8CA7CBF5F2388B (void);
// 0x000004EB System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade::get_IsReadOnly()
extern void ParametersDictionaryFacade_get_IsReadOnly_m87E6F85E3B7EF8BBB1322BC8A709710FCB44DCFC (void);
// 0x000004EC System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Amazon.Runtime.Internal.ParametersDictionaryFacade::GetEnumerator()
extern void ParametersDictionaryFacade_GetEnumerator_m8106C6F93EFBE6BBC5E921ACFBDCD8198F97D8C9 (void);
// 0x000004ED System.Collections.IEnumerator Amazon.Runtime.Internal.ParametersDictionaryFacade::System.Collections.IEnumerable.GetEnumerator()
extern void ParametersDictionaryFacade_System_Collections_IEnumerable_GetEnumerator_m7BA5D4B55EF8E37179729DEF2E16BF0771984AD5 (void);
// 0x000004EE System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade::Clear()
extern void ParametersDictionaryFacade_Clear_mE60CBF431A8588ECA6E988F792F5922D5071B7CF (void);
// 0x000004EF System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__23__ctor_mF2A47390FF6A0D9DF41AEDCF316A05038745DAD3 (void);
// 0x000004F0 System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__23_System_IDisposable_Dispose_mCCB562FCB94036DF98F8DDD82D0CEBC92739E7D0 (void);
// 0x000004F1 System.Boolean Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::MoveNext()
extern void U3CGetEnumeratorU3Ed__23_MoveNext_m3CEFF87DF2AAA6DD792EE636DF5A6B355051494F (void);
// 0x000004F2 System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__23_U3CU3Em__Finally1_mC2F612DA526D2BF4D1F13C2EEBFD9E080903C396 (void);
// 0x000004F3 System.Collections.Generic.KeyValuePair`2<System.String,System.String> Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,System.String>>.get_Current()
extern void U3CGetEnumeratorU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mB770895B34BB9A97BDD81972BAA878115A963E80 (void);
// 0x000004F4 System.Void Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_Reset_m7CD8581DD59E442903A7DEA8F5A7A6019A5FD816 (void);
// 0x000004F5 System.Object Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_get_Current_mE07999BA03923739E386C7B5BD271FC010E714A8 (void);
// 0x000004F6 Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::get_Instance()
extern void RuntimePipelineCustomizerRegistry_get_Instance_m0FCF38817786A1D0F32D0DC785EB0A056AA6F060 (void);
// 0x000004F7 System.Void Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::.ctor()
extern void RuntimePipelineCustomizerRegistry__ctor_mA5D660F0E11641FDBB7FAB5D5C177AA4D868B234 (void);
// 0x000004F8 System.Void Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::ApplyCustomizations(System.Type,Amazon.Runtime.Internal.RuntimePipeline)
extern void RuntimePipelineCustomizerRegistry_ApplyCustomizations_mC32328A9DAFFE74313678DD33441554229F86DF0 (void);
// 0x000004F9 System.Void Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::Dispose()
extern void RuntimePipelineCustomizerRegistry_Dispose_m4C0FA1C5411FFD6D4D648CBF1E4C7C68E7605976 (void);
// 0x000004FA System.Void Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::Dispose(System.Boolean)
extern void RuntimePipelineCustomizerRegistry_Dispose_mE5B8C16C466436BBA2FE5FA389AB8CF709453BC8 (void);
// 0x000004FB System.Void Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::.cctor()
extern void RuntimePipelineCustomizerRegistry__cctor_m90D783B628137581BEDC98C9622B77B619D9DB3A (void);
// 0x000004FC System.String Amazon.Runtime.Internal.IRuntimePipelineCustomizer::get_UniqueName()
// 0x000004FD System.Void Amazon.Runtime.Internal.IRuntimePipelineCustomizer::Customize(System.Type,Amazon.Runtime.Internal.RuntimePipeline)
// 0x000004FE System.String Amazon.Runtime.Internal.ServiceMetadata::get_ServiceId()
extern void ServiceMetadata_get_ServiceId_m42AA96D72D2FD71B2EC4A9B4B33DC1B9A835A4AF (void);
// 0x000004FF System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.ServiceMetadata::get_OperationNameMapping()
extern void ServiceMetadata_get_OperationNameMapping_mAE5C256DC7C08383E6FCF215B18ED0D477D2DAD9 (void);
// 0x00000500 System.Void Amazon.Runtime.Internal.ServiceMetadata::.ctor()
extern void ServiceMetadata__ctor_mA39791F0BDFF6859A7D20D79422A56F7BFDAA1E2 (void);
// 0x00000501 System.Void Amazon.Runtime.Internal.StreamReadTracker::.ctor(System.Object,System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>,System.Int64,System.Int64)
extern void StreamReadTracker__ctor_m71F55F97D28B19A39DFE9ED48E843175CD3169FB (void);
// 0x00000502 System.Void Amazon.Runtime.Internal.StreamReadTracker::ReadProgress(System.Int32)
extern void StreamReadTracker_ReadProgress_m8662F4043206EB20524AC4012B7128E6B1E70983 (void);
// 0x00000503 System.Void Amazon.Runtime.Internal.ParameterCollection::.ctor()
extern void ParameterCollection__ctor_m15DCE498499A9FB590FAA1DFCB2E2B6AFE483A5F (void);
// 0x00000504 System.Void Amazon.Runtime.Internal.ParameterCollection::Add(System.String,System.String)
extern void ParameterCollection_Add_m579741858D341585CE795481B02953B3A7DC52D0 (void);
// 0x00000505 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Amazon.Runtime.Internal.ParameterCollection::GetSortedParametersList()
extern void ParameterCollection_GetSortedParametersList_mC858545EDE1D937A92DC4A2C9841308FE9DF61C8 (void);
// 0x00000506 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Amazon.Runtime.Internal.ParameterCollection::GetParametersEnumerable()
extern void ParameterCollection_GetParametersEnumerable_mFD6A666A3B962D2B53212BAFB5D24132C33A21DC (void);
// 0x00000507 System.Void Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::.ctor(System.Int32)
extern void U3CGetParametersEnumerableU3Ed__4__ctor_mA3660F51DEA5FCCA18250DF86B1E4228CE3004BC (void);
// 0x00000508 System.Void Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::System.IDisposable.Dispose()
extern void U3CGetParametersEnumerableU3Ed__4_System_IDisposable_Dispose_mB5E20B4637F27495FBB2EF920877949FCA12FFD1 (void);
// 0x00000509 System.Boolean Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::MoveNext()
extern void U3CGetParametersEnumerableU3Ed__4_MoveNext_m8C6085C7C60018355B4B6F105C49874D084FD986 (void);
// 0x0000050A System.Void Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>m__Finally1()
extern void U3CGetParametersEnumerableU3Ed__4_U3CU3Em__Finally1_mD2F027189ABA718D43B212181B851FEAC2902CAD (void);
// 0x0000050B System.Void Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>m__Finally2()
extern void U3CGetParametersEnumerableU3Ed__4_U3CU3Em__Finally2_mFFEF852C54083F2401306C74FCEE7F3A27854629 (void);
// 0x0000050C System.Collections.Generic.KeyValuePair`2<System.String,System.String> Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,System.String>>.get_Current()
extern void U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mD78AC053140A1678AA5390E3B84194BA638B732E (void);
// 0x0000050D System.Void Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::System.Collections.IEnumerator.Reset()
extern void U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_Reset_m440496B3B80A3C4CA4E26640D5297498FDABED23 (void);
// 0x0000050E System.Object Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_get_Current_m4B1FA65DB4B72667C4995D7FA54F47701A8EF03C (void);
// 0x0000050F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.String,System.String>>.GetEnumerator()
extern void U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m8556E7C7BFD90DCA9FB7837CB5D1FBC3C2F22D6B (void);
// 0x00000510 System.Collections.IEnumerator Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerable_GetEnumerator_m583C549C1976AF018F67004591B37239E0FCA2EE (void);
// 0x00000511 System.Void Amazon.Runtime.Internal.RequestContext::.ctor(System.Boolean,Amazon.Runtime.Internal.Auth.AbstractAWSSigner)
extern void RequestContext__ctor_mBCA4AA9FD7A8C54147C3496790F1726A18C20838 (void);
// 0x00000512 Amazon.Runtime.Internal.IRequest Amazon.Runtime.Internal.RequestContext::get_Request()
extern void RequestContext_get_Request_mA4471DE841DE086DCA9A7949E3E906BD9E8EA7BF (void);
// 0x00000513 System.Void Amazon.Runtime.Internal.RequestContext::set_Request(Amazon.Runtime.Internal.IRequest)
extern void RequestContext_set_Request_m4D9502541AA3E426E0AD100D820F57097F0F3359 (void);
// 0x00000514 Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.Internal.RequestContext::get_Metrics()
extern void RequestContext_get_Metrics_m8245B5AD45A4136FEA4A41FD0E9AE03BF910137D (void);
// 0x00000515 System.Void Amazon.Runtime.Internal.RequestContext::set_Metrics(Amazon.Runtime.Internal.Util.RequestMetrics)
extern void RequestContext_set_Metrics_mFBEC125FE4162520753F26D54BA2ECEE87D7173F (void);
// 0x00000516 Amazon.Runtime.IClientConfig Amazon.Runtime.Internal.RequestContext::get_ClientConfig()
extern void RequestContext_get_ClientConfig_m00A68FB365505864D204F7D9F0B4B3B0CA440976 (void);
// 0x00000517 System.Void Amazon.Runtime.Internal.RequestContext::set_ClientConfig(Amazon.Runtime.IClientConfig)
extern void RequestContext_set_ClientConfig_mB9CCDA771A29F9C7765C9AF67EAC5A88646874E0 (void);
// 0x00000518 System.Int32 Amazon.Runtime.Internal.RequestContext::get_Retries()
extern void RequestContext_get_Retries_mB59731D9636F968BAA1706BDC5C07626A3FE1219 (void);
// 0x00000519 System.Void Amazon.Runtime.Internal.RequestContext::set_Retries(System.Int32)
extern void RequestContext_set_Retries_mD88CBDC3B30D3A4BAA83FBE3BE13A6A977F378E6 (void);
// 0x0000051A Amazon.Runtime.Internal.CapacityManager/CapacityType Amazon.Runtime.Internal.RequestContext::get_LastCapacityType()
extern void RequestContext_get_LastCapacityType_m0B7619EE743164D5DC511D6A1DD843A467A8C7DF (void);
// 0x0000051B System.Int32 Amazon.Runtime.Internal.RequestContext::get_EndpointDiscoveryRetries()
extern void RequestContext_get_EndpointDiscoveryRetries_m98C8DB853AA051758AC68367C68DF25D71A7E5A2 (void);
// 0x0000051C System.Void Amazon.Runtime.Internal.RequestContext::set_EndpointDiscoveryRetries(System.Int32)
extern void RequestContext_set_EndpointDiscoveryRetries_m76846D7FF5CAA564393ADB168341B125C1C23C79 (void);
// 0x0000051D System.Boolean Amazon.Runtime.Internal.RequestContext::get_IsSigned()
extern void RequestContext_get_IsSigned_mC3D6CB04C958B62F4D4423D216C4B01B8D43089D (void);
// 0x0000051E System.Void Amazon.Runtime.Internal.RequestContext::set_IsSigned(System.Boolean)
extern void RequestContext_set_IsSigned_m92398DD2A210E0A2E98BE81442B7BEAA1F89AF82 (void);
// 0x0000051F System.Boolean Amazon.Runtime.Internal.RequestContext::get_IsAsync()
extern void RequestContext_get_IsAsync_mE367ACF5C06381343028C5A45BDDDFAEF3164912 (void);
// 0x00000520 System.Void Amazon.Runtime.Internal.RequestContext::set_IsAsync(System.Boolean)
extern void RequestContext_set_IsAsync_m9F1AF9842CB659049A616F2A850C6A594490C9AE (void);
// 0x00000521 Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RequestContext::get_OriginalRequest()
extern void RequestContext_get_OriginalRequest_m76BC7D9FCAFA242D20FE9834A4E18D6F921ACF8D (void);
// 0x00000522 System.Void Amazon.Runtime.Internal.RequestContext::set_OriginalRequest(Amazon.Runtime.AmazonWebServiceRequest)
extern void RequestContext_set_OriginalRequest_m5D45287D4130E982EB4EF26BEBEC1ABF28B4DEBC (void);
// 0x00000523 Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.RequestContext::get_Marshaller()
extern void RequestContext_get_Marshaller_mE77303460FA245309D9033950B4594DFCC9D5F2F (void);
// 0x00000524 System.Void Amazon.Runtime.Internal.RequestContext::set_Marshaller(Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>)
extern void RequestContext_set_Marshaller_mBC9EF52693055C503B97AA6E185EEB5FE965CDE3 (void);
// 0x00000525 Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.Internal.RequestContext::get_Unmarshaller()
extern void RequestContext_get_Unmarshaller_m5A458EBF82A9432CF3C2614970F4A40960CC8116 (void);
// 0x00000526 System.Void Amazon.Runtime.Internal.RequestContext::set_Unmarshaller(Amazon.Runtime.Internal.Transform.ResponseUnmarshaller)
extern void RequestContext_set_Unmarshaller_mCE129D11F8052AFDB6CF9165B7E791DAB77BBE4E (void);
// 0x00000527 Amazon.Runtime.Internal.InvokeOptionsBase Amazon.Runtime.Internal.RequestContext::get_Options()
extern void RequestContext_get_Options_m0A2898FF3EABBAB4E978889AF1BB379BA7B7B24C (void);
// 0x00000528 System.Void Amazon.Runtime.Internal.RequestContext::set_Options(Amazon.Runtime.Internal.InvokeOptionsBase)
extern void RequestContext_set_Options_m4F9A2FFAE721D3CAE9C9407B99FACA305FBCCA0A (void);
// 0x00000529 Amazon.Runtime.ImmutableCredentials Amazon.Runtime.Internal.RequestContext::get_ImmutableCredentials()
extern void RequestContext_get_ImmutableCredentials_mC7ABFF395C149FF56563340D9AE520AA4CF82BCB (void);
// 0x0000052A System.Void Amazon.Runtime.Internal.RequestContext::set_ImmutableCredentials(Amazon.Runtime.ImmutableCredentials)
extern void RequestContext_set_ImmutableCredentials_mB8A770A9E8FFDB3EBD95C661A64BE87A4F682AB8 (void);
// 0x0000052B Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.Internal.RequestContext::get_Signer()
extern void RequestContext_get_Signer_m2BAD0D8E49B7E99383817286165C3CFB88AD1836 (void);
// 0x0000052C System.Threading.CancellationToken Amazon.Runtime.Internal.RequestContext::get_CancellationToken()
extern void RequestContext_get_CancellationToken_mAB9910A6A46E6F719840B02B74007BC8F9D14A35 (void);
// 0x0000052D System.Void Amazon.Runtime.Internal.RequestContext::set_CancellationToken(System.Threading.CancellationToken)
extern void RequestContext_set_CancellationToken_m416969291C198882377AB7F302BA0DBC4E953D17 (void);
// 0x0000052E System.String Amazon.Runtime.Internal.RequestContext::get_RequestName()
extern void RequestContext_get_RequestName_m5613C0F6CC6E513DDCC3D8C17162AEB132A2B596 (void);
// 0x0000052F Amazon.Runtime.Internal.MonitoringAPICallAttempt Amazon.Runtime.Internal.RequestContext::get_CSMCallAttempt()
extern void RequestContext_get_CSMCallAttempt_m02134A8CD42466EBD1745D01C390776F393F6177 (void);
// 0x00000530 System.Void Amazon.Runtime.Internal.RequestContext::set_CSMCallAttempt(Amazon.Runtime.Internal.MonitoringAPICallAttempt)
extern void RequestContext_set_CSMCallAttempt_m6A1FF6F8CF854F9298D2910A9D746EECEE87DE8C (void);
// 0x00000531 Amazon.Runtime.Internal.MonitoringAPICallEvent Amazon.Runtime.Internal.RequestContext::get_CSMCallEvent()
extern void RequestContext_get_CSMCallEvent_m241604A1F65FA848A747968080BC8D4BE19D9CAA (void);
// 0x00000532 System.Void Amazon.Runtime.Internal.RequestContext::set_CSMCallEvent(Amazon.Runtime.Internal.MonitoringAPICallEvent)
extern void RequestContext_set_CSMCallEvent_m2A7C298840A43F98784D2447CAC0C7437FC385DD (void);
// 0x00000533 Amazon.Runtime.Internal.IServiceMetadata Amazon.Runtime.Internal.RequestContext::get_ServiceMetaData()
extern void RequestContext_get_ServiceMetaData_m2E19199234CA5F2676479B2CE363ECB0DE386B46 (void);
// 0x00000534 System.Void Amazon.Runtime.Internal.RequestContext::set_ServiceMetaData(Amazon.Runtime.Internal.IServiceMetadata)
extern void RequestContext_set_ServiceMetaData_m088026368D0314986B0E3C377E9BFFF35C627F21 (void);
// 0x00000535 System.Boolean Amazon.Runtime.Internal.RequestContext::get_CSMEnabled()
extern void RequestContext_get_CSMEnabled_mEC4878BD651F342B235ACB9E5B04B1C39EC376BB (void);
// 0x00000536 System.Void Amazon.Runtime.Internal.RequestContext::set_CSMEnabled(System.Boolean)
extern void RequestContext_set_CSMEnabled_m3F7FD5E4D36DD00A8515633DB76CFA1F1CF9AF0E (void);
// 0x00000537 System.Boolean Amazon.Runtime.Internal.RequestContext::get_IsLastExceptionRetryable()
extern void RequestContext_get_IsLastExceptionRetryable_m9CBDEF39DDEB049FBC67FEB11EA2E369078E50DF (void);
// 0x00000538 System.Void Amazon.Runtime.Internal.RequestContext::set_IsLastExceptionRetryable(System.Boolean)
extern void RequestContext_set_IsLastExceptionRetryable_m06ED2876291D5203B96981CC608CE51BA73BADAC (void);
// 0x00000539 Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.ResponseContext::get_Response()
extern void ResponseContext_get_Response_m8B11E0CAD36313831E9E73DE776625B055C6E41B (void);
// 0x0000053A System.Void Amazon.Runtime.Internal.ResponseContext::set_Response(Amazon.Runtime.AmazonWebServiceResponse)
extern void ResponseContext_set_Response_mC3E7EC2795FC2AEDE791C0D6C5DD9CBDC2C229D1 (void);
// 0x0000053B Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.ResponseContext::get_HttpResponse()
extern void ResponseContext_get_HttpResponse_m0DE5A537E04426B2FBA39A54E1A6ED490714B12D (void);
// 0x0000053C System.Void Amazon.Runtime.Internal.ResponseContext::set_HttpResponse(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern void ResponseContext_set_HttpResponse_mCDB00703E92D7AF877659F9C077D0A478DB6DD30 (void);
// 0x0000053D System.Void Amazon.Runtime.Internal.ResponseContext::.ctor()
extern void ResponseContext__ctor_m3941F56DDFCFDF9C1506EDC4EFBA50D73E3F5F2D (void);
// 0x0000053E Amazon.Runtime.IRequestContext Amazon.Runtime.Internal.ExecutionContext::get_RequestContext()
extern void ExecutionContext_get_RequestContext_m65AB9054F3B778D154885B4C08576F7E56D73471 (void);
// 0x0000053F System.Void Amazon.Runtime.Internal.ExecutionContext::set_RequestContext(Amazon.Runtime.IRequestContext)
extern void ExecutionContext_set_RequestContext_mFD3FB7214104B3EF2E0ADA06F9E381EBD986E502 (void);
// 0x00000540 Amazon.Runtime.IResponseContext Amazon.Runtime.Internal.ExecutionContext::get_ResponseContext()
extern void ExecutionContext_get_ResponseContext_mDA86EFB7136DEA63A6C462D56D0C19DBF2F5CB94 (void);
// 0x00000541 System.Void Amazon.Runtime.Internal.ExecutionContext::set_ResponseContext(Amazon.Runtime.IResponseContext)
extern void ExecutionContext_set_ResponseContext_m10DEA16A9002732D37F74B980F2EA9267682EDF5 (void);
// 0x00000542 System.Void Amazon.Runtime.Internal.ExecutionContext::.ctor(Amazon.Runtime.IRequestContext,Amazon.Runtime.IResponseContext)
extern void ExecutionContext__ctor_mAE96DA25DD104F5D74DE18C60F7F4202A32A9E7E (void);
// 0x00000543 System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler> Amazon.Runtime.Internal.ErrorHandler::get_ExceptionHandlers()
extern void ErrorHandler_get_ExceptionHandlers_mB572E61E5C35D98C90A040312353890DC48ED0C4 (void);
// 0x00000544 System.Void Amazon.Runtime.Internal.ErrorHandler::.ctor(Amazon.Runtime.Internal.Util.ILogger)
extern void ErrorHandler__ctor_mC862AC6C257D7E5D46B62CAA34E0A88F2F4F9FF7 (void);
// 0x00000545 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.ErrorHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000546 System.Void Amazon.Runtime.Internal.ErrorHandler::DisposeReponse(Amazon.Runtime.IResponseContext)
extern void ErrorHandler_DisposeReponse_m01AE65AD6A675871B03EC14F70E6D35A732845A6 (void);
// 0x00000547 System.Boolean Amazon.Runtime.Internal.ErrorHandler::ProcessException(Amazon.Runtime.IExecutionContext,System.Exception)
extern void ErrorHandler_ProcessException_mAA5D010039EF1A7F34BF331AB1248F2385EC19AF (void);
// 0x00000548 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.ErrorHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x00000549 System.Void Amazon.Runtime.Internal.ErrorHandler/<InvokeAsync>d__5`1::MoveNext()
// 0x0000054A System.Void Amazon.Runtime.Internal.ErrorHandler/<InvokeAsync>d__5`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x0000054B Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.ExceptionHandler`1::get_Logger()
// 0x0000054C System.Void Amazon.Runtime.Internal.ExceptionHandler`1::.ctor(Amazon.Runtime.Internal.Util.ILogger)
// 0x0000054D System.Boolean Amazon.Runtime.Internal.ExceptionHandler`1::Handle(Amazon.Runtime.IExecutionContext,System.Exception)
// 0x0000054E System.Boolean Amazon.Runtime.Internal.ExceptionHandler`1::HandleException(Amazon.Runtime.IExecutionContext,T)
// 0x0000054F System.Void Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler::.ctor(Amazon.Runtime.Internal.Util.ILogger)
extern void HttpErrorResponseExceptionHandler__ctor_m8ED40C0382C8603F8946448FA917B2AC4C58E096 (void);
// 0x00000550 System.Boolean Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler::HandleException(Amazon.Runtime.IExecutionContext,Amazon.Runtime.Internal.HttpErrorResponseException)
extern void HttpErrorResponseExceptionHandler_HandleException_mA006BA596A01A06A560FD584530B23A5FCFD2F2B (void);
// 0x00000551 System.Boolean Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler::HandleSuppressed404(Amazon.Runtime.IExecutionContext,Amazon.Runtime.Internal.Transform.IWebResponseData)
extern void HttpErrorResponseExceptionHandler_HandleSuppressed404_mCFD5A5BFFF47028CAE3E6B9E787A2177055BAECC (void);
// 0x00000552 System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::get_OnPreInvoke()
extern void CallbackHandler_get_OnPreInvoke_m1A71F567B0855CDC3FF0EB544116E7172AFF2E26 (void);
// 0x00000553 System.Void Amazon.Runtime.Internal.CallbackHandler::set_OnPreInvoke(System.Action`1<Amazon.Runtime.IExecutionContext>)
extern void CallbackHandler_set_OnPreInvoke_m87C3C20DF9BD145545EE87C806FEB7D9E6371A50 (void);
// 0x00000554 System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::get_OnPostInvoke()
extern void CallbackHandler_get_OnPostInvoke_m508D36C9CF19EABD021159EABB46E9BDEB3D8323 (void);
// 0x00000555 System.Void Amazon.Runtime.Internal.CallbackHandler::set_OnPostInvoke(System.Action`1<Amazon.Runtime.IExecutionContext>)
extern void CallbackHandler_set_OnPostInvoke_m2B7939C80173D9408D0D31C441EF9222C946900B (void);
// 0x00000556 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CallbackHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000557 System.Void Amazon.Runtime.Internal.CallbackHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void CallbackHandler_PreInvoke_m674FC13DD0B7C6B8809AE34B5BCDC27736E2B93C (void);
// 0x00000558 System.Void Amazon.Runtime.Internal.CallbackHandler::PostInvoke(Amazon.Runtime.IExecutionContext)
extern void CallbackHandler_PostInvoke_m869DEDC14CE5BA5E6F0673320213D7217838E065 (void);
// 0x00000559 System.Void Amazon.Runtime.Internal.CallbackHandler::RaiseOnPreInvoke(Amazon.Runtime.IExecutionContext)
extern void CallbackHandler_RaiseOnPreInvoke_mD698C514D684A541657FF58A17E9083BCEF18AD6 (void);
// 0x0000055A System.Void Amazon.Runtime.Internal.CallbackHandler::RaiseOnPostInvoke(Amazon.Runtime.IExecutionContext)
extern void CallbackHandler_RaiseOnPostInvoke_mBBD4E7F01897523794EB310B64270E488E1E93F8 (void);
// 0x0000055B System.Void Amazon.Runtime.Internal.CallbackHandler::.ctor()
extern void CallbackHandler__ctor_m9AAB9136A90D8544DD26358345E87F3993947B26 (void);
// 0x0000055C System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CallbackHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x0000055D System.Void Amazon.Runtime.Internal.CallbackHandler/<InvokeAsync>d__9`1::MoveNext()
// 0x0000055E System.Void Amazon.Runtime.Internal.CallbackHandler/<InvokeAsync>d__9`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x0000055F System.Void Amazon.Runtime.Internal.CredentialsRetriever::.ctor(Amazon.Runtime.AWSCredentials)
extern void CredentialsRetriever__ctor_m2A5A0CE664B352B004791B069C6EC78ADEB6B4D9 (void);
// 0x00000560 Amazon.Runtime.AWSCredentials Amazon.Runtime.Internal.CredentialsRetriever::get_Credentials()
extern void CredentialsRetriever_get_Credentials_m536C03A6F14228A70E0FE37416B02E246B0CC698 (void);
// 0x00000561 System.Void Amazon.Runtime.Internal.CredentialsRetriever::set_Credentials(Amazon.Runtime.AWSCredentials)
extern void CredentialsRetriever_set_Credentials_m0443A627032FBFA8D7D571BE129870693CB7A87B (void);
// 0x00000562 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CredentialsRetriever::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000563 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CredentialsRetriever::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x00000564 System.Void Amazon.Runtime.Internal.CredentialsRetriever/<InvokeAsync>d__7`1::MoveNext()
// 0x00000565 System.Void Amazon.Runtime.Internal.CredentialsRetriever/<InvokeAsync>d__7`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000566 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.EndpointDiscoveryHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000567 System.Void Amazon.Runtime.Internal.EndpointDiscoveryHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void EndpointDiscoveryHandler_PreInvoke_mF159347BB4140DB025FB3BAAC9DEE45E091F8025 (void);
// 0x00000568 System.Void Amazon.Runtime.Internal.EndpointDiscoveryHandler::EvictCacheKeyForRequest(Amazon.Runtime.IRequestContext,System.Uri)
extern void EndpointDiscoveryHandler_EvictCacheKeyForRequest_mB1790C70A241D1D9357891E007E4C8694328A189 (void);
// 0x00000569 System.Void Amazon.Runtime.Internal.EndpointDiscoveryHandler::DiscoverEndpoints(Amazon.Runtime.IRequestContext,System.Boolean)
extern void EndpointDiscoveryHandler_DiscoverEndpoints_m15B9D071C481BC29CF3EC9E42D7BDC7C57DDFC55 (void);
// 0x0000056A System.Collections.Generic.IEnumerable`1<Amazon.Runtime.Internal.DiscoveryEndpointBase> Amazon.Runtime.Internal.EndpointDiscoveryHandler::ProcessEndpointDiscovery(Amazon.Runtime.IRequestContext,System.Boolean,System.Uri)
extern void EndpointDiscoveryHandler_ProcessEndpointDiscovery_mA0893AA63A1C08D020D0CC33D4B2BFE57DF1372A (void);
// 0x0000056B System.String Amazon.Runtime.Internal.EndpointDiscoveryHandler::OperationNameFromRequestName(System.String)
extern void EndpointDiscoveryHandler_OperationNameFromRequestName_m76E9A312258F42D810A862FFCC6DEBA869DC80EA (void);
// 0x0000056C System.Boolean Amazon.Runtime.Internal.EndpointDiscoveryHandler::IsInvalidEndpointException(System.Exception)
extern void EndpointDiscoveryHandler_IsInvalidEndpointException_mEE55FD9E2BC849F0ECCB0342E73D7BA29102D669 (void);
// 0x0000056D System.Void Amazon.Runtime.Internal.EndpointDiscoveryHandler::.ctor()
extern void EndpointDiscoveryHandler__ctor_m87958934ECE67D4C7AB3FD672411552D6E5AF9FD (void);
// 0x0000056E System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.EndpointDiscoveryHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x0000056F System.Void Amazon.Runtime.Internal.EndpointDiscoveryHandler/<InvokeAsync>d__2`1::MoveNext()
// 0x00000570 System.Void Amazon.Runtime.Internal.EndpointDiscoveryHandler/<InvokeAsync>d__2`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000571 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.EndpointResolver::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000572 System.Void Amazon.Runtime.Internal.EndpointResolver::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void EndpointResolver_PreInvoke_m41ACBA2E127B354C416E986AC393190E0F673C8D (void);
// 0x00000573 System.Uri Amazon.Runtime.Internal.EndpointResolver::DetermineEndpoint(Amazon.Runtime.IRequestContext)
extern void EndpointResolver_DetermineEndpoint_m0A2617AF5BD3BDF6E697786A613573BFD23982B6 (void);
// 0x00000574 System.Uri Amazon.Runtime.Internal.EndpointResolver::DetermineEndpoint(Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.IRequest)
extern void EndpointResolver_DetermineEndpoint_m1DD4CBB04D09805F4CB4D8BEF3AD361EB06442FE (void);
// 0x00000575 System.Uri Amazon.Runtime.Internal.EndpointResolver::InjectHostPrefix(Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.IRequest,System.Uri)
extern void EndpointResolver_InjectHostPrefix_m9866706C753ACB0424D043717A252EF5890B1C54 (void);
// 0x00000576 System.Void Amazon.Runtime.Internal.EndpointResolver::.ctor()
extern void EndpointResolver__ctor_m7718BD253418587E6040FFF8DAE2D212FBDAB6B1 (void);
// 0x00000577 System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception> Amazon.Runtime.Internal.ErrorCallbackHandler::get_OnError()
extern void ErrorCallbackHandler_get_OnError_m128B66FA238E4109FE664413CA8D53BCE0A3721E (void);
// 0x00000578 System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::set_OnError(System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception>)
extern void ErrorCallbackHandler_set_OnError_m9B39DBF861DDF1523501DE9C1C7FBC96885AA0A0 (void);
// 0x00000579 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.ErrorCallbackHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x0000057A System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::HandleException(Amazon.Runtime.IExecutionContext,System.Exception)
extern void ErrorCallbackHandler_HandleException_m679AF3F2F1BCD187799DCC57695F126502E520D0 (void);
// 0x0000057B System.Void Amazon.Runtime.Internal.ErrorCallbackHandler::.ctor()
extern void ErrorCallbackHandler__ctor_m678D0A72D39FA10575760B858E4DBA1D8BEF3BBF (void);
// 0x0000057C System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.ErrorCallbackHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x0000057D System.Void Amazon.Runtime.Internal.ErrorCallbackHandler/<InvokeAsync>d__5`1::MoveNext()
// 0x0000057E System.Void Amazon.Runtime.Internal.ErrorCallbackHandler/<InvokeAsync>d__5`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x0000057F System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.Marshaller::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000580 System.Void Amazon.Runtime.Internal.Marshaller::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void Marshaller_PreInvoke_m087615AE4831D4F3E6D7B442287B29EA8DBD46F0 (void);
// 0x00000581 System.Void Amazon.Runtime.Internal.Marshaller::.ctor()
extern void Marshaller__ctor_m9292D6F9F48C23C321693194EC5E014408875ECD (void);
// 0x00000582 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.MetricsHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000583 System.Void Amazon.Runtime.Internal.MetricsHandler::.ctor()
extern void MetricsHandler__ctor_mD2DE7F55D1036EF0513C0CF4FEE70A7CD57B1518 (void);
// 0x00000584 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.MetricsHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x00000585 System.Void Amazon.Runtime.Internal.MetricsHandler/<InvokeAsync>d__1`1::MoveNext()
// 0x00000586 System.Void Amazon.Runtime.Internal.MetricsHandler/<InvokeAsync>d__1`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000587 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.Signer::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x00000588 System.Void Amazon.Runtime.Internal.Signer::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void Signer_PreInvoke_m04CACF5F3C3366422C7E8FAF3358E4B291F46185 (void);
// 0x00000589 System.Boolean Amazon.Runtime.Internal.Signer::ShouldSign(Amazon.Runtime.IRequestContext)
extern void Signer_ShouldSign_m5FF13C3682C0CA20BB62ADCF1DD6C50CDF0BFD56 (void);
// 0x0000058A System.Void Amazon.Runtime.Internal.Signer::SignRequest(Amazon.Runtime.IRequestContext)
extern void Signer_SignRequest_m38A9FDE9169C258C16EF26C36754782565DE1CAA (void);
// 0x0000058B System.Void Amazon.Runtime.Internal.Signer::.ctor()
extern void Signer__ctor_m84FD1835BB4E6649ABFAE9CE18794CC21AD3E882 (void);
// 0x0000058C System.Void Amazon.Runtime.Internal.Unmarshaller::.ctor(System.Boolean)
extern void Unmarshaller__ctor_m0EA12C5E751B722603AB282C4FAA2ED13AC8038C (void);
// 0x0000058D System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.Unmarshaller::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x0000058E System.Threading.Tasks.Task Amazon.Runtime.Internal.Unmarshaller::UnmarshallAsync(Amazon.Runtime.IExecutionContext)
extern void Unmarshaller_UnmarshallAsync_m9EFFEA0695F1F4D6EB0388FF9465AA6EB981F4A2 (void);
// 0x0000058F Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Unmarshaller::UnmarshallResponse(Amazon.Runtime.Internal.Transform.UnmarshallerContext,Amazon.Runtime.IRequestContext)
extern void Unmarshaller_UnmarshallResponse_mA90C284C5760AD8C61F4FB0D1ABD1F6FB166483B (void);
// 0x00000590 System.Boolean Amazon.Runtime.Internal.Unmarshaller::ShouldLogResponseBody(System.Boolean,Amazon.Runtime.IRequestContext)
extern void Unmarshaller_ShouldLogResponseBody_mB176DCD73A65EDB8AF17A436BF99538BA14C190D (void);
// 0x00000591 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.Unmarshaller::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x00000592 System.Void Amazon.Runtime.Internal.Unmarshaller/<InvokeAsync>d__3`1::MoveNext()
// 0x00000593 System.Void Amazon.Runtime.Internal.Unmarshaller/<InvokeAsync>d__3`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000594 System.Void Amazon.Runtime.Internal.Unmarshaller/<UnmarshallAsync>d__5::MoveNext()
extern void U3CUnmarshallAsyncU3Ed__5_MoveNext_mEFF02A4DC2C96E2D42CB3555EEB8D738EDBC2090 (void);
// 0x00000595 System.Void Amazon.Runtime.Internal.Unmarshaller/<UnmarshallAsync>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUnmarshallAsyncU3Ed__5_SetStateMachine_m92CF4477AB1C3D3C38CED8DE399CC58D1ED0F3F4 (void);
// 0x00000596 Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.HttpErrorResponseException::get_Response()
extern void HttpErrorResponseException_get_Response_mF300FD82AF9B72A5FAC48955B57D1A6C16570391 (void);
// 0x00000597 System.Void Amazon.Runtime.Internal.HttpErrorResponseException::set_Response(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern void HttpErrorResponseException_set_Response_m0EFA97C19C16304F4A116E23F4405DC3012630D9 (void);
// 0x00000598 System.Void Amazon.Runtime.Internal.HttpErrorResponseException::.ctor(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern void HttpErrorResponseException__ctor_m9BB2D9A7978D65343179B44DEB08D6F953E87377 (void);
// 0x00000599 System.Object Amazon.Runtime.Internal.HttpHandler`1::get_CallbackSender()
// 0x0000059A System.Void Amazon.Runtime.Internal.HttpHandler`1::set_CallbackSender(System.Object)
// 0x0000059B System.Void Amazon.Runtime.Internal.HttpHandler`1::.ctor(Amazon.Runtime.IHttpRequestFactory`1<TRequestContent>,System.Object)
// 0x0000059C System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.HttpHandler`1::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x0000059D System.Threading.Tasks.Task Amazon.Runtime.Internal.HttpHandler`1::CompleteFailedRequest(Amazon.Runtime.IExecutionContext,Amazon.Runtime.IHttpRequest`1<TRequestContent>)
// 0x0000059E System.Void Amazon.Runtime.Internal.HttpHandler`1::SetMetrics(Amazon.Runtime.IRequestContext)
// 0x0000059F System.Void Amazon.Runtime.Internal.HttpHandler`1::WriteContentToRequestBody(TRequestContent,Amazon.Runtime.IHttpRequest`1<TRequestContent>,Amazon.Runtime.IRequestContext)
// 0x000005A0 Amazon.Runtime.IHttpRequest`1<TRequestContent> Amazon.Runtime.Internal.HttpHandler`1::CreateWebRequest(Amazon.Runtime.IRequestContext)
// 0x000005A1 System.Void Amazon.Runtime.Internal.HttpHandler`1::Dispose()
// 0x000005A2 System.Void Amazon.Runtime.Internal.HttpHandler`1::Dispose(System.Boolean)
// 0x000005A3 System.IO.Stream Amazon.Runtime.Internal.HttpHandler`1::GetInputStream(Amazon.Runtime.IRequestContext,System.IO.Stream,Amazon.Runtime.Internal.IRequest)
// 0x000005A4 System.Void Amazon.Runtime.Internal.HttpHandler`1/<InvokeAsync>d__9`1::MoveNext()
// 0x000005A5 System.Void Amazon.Runtime.Internal.HttpHandler`1/<InvokeAsync>d__9`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000005A6 System.Void Amazon.Runtime.Internal.HttpHandler`1/<CompleteFailedRequest>d__10::MoveNext()
// 0x000005A7 System.Void Amazon.Runtime.Internal.HttpHandler`1/<CompleteFailedRequest>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000005A8 Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.PipelineHandler::get_Logger()
extern void PipelineHandler_get_Logger_m079669ADD1EC8F4E440F64144BA3AF4EE7958403 (void);
// 0x000005A9 System.Void Amazon.Runtime.Internal.PipelineHandler::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
extern void PipelineHandler_set_Logger_m0C70A5AFC9FA063FB8A3E49FB0E9DF35BD663BE5 (void);
// 0x000005AA Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::get_InnerHandler()
extern void PipelineHandler_get_InnerHandler_mC14FC8E11103E7D2DD80C869934B005486069356 (void);
// 0x000005AB System.Void Amazon.Runtime.Internal.PipelineHandler::set_InnerHandler(Amazon.Runtime.IPipelineHandler)
extern void PipelineHandler_set_InnerHandler_m2CA2C10774C68B7556C3BC7CC3FD7FAE1E0D6677 (void);
// 0x000005AC Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::get_OuterHandler()
extern void PipelineHandler_get_OuterHandler_m3C8B44F35F670546B89E7CEC831456D952E00814 (void);
// 0x000005AD System.Void Amazon.Runtime.Internal.PipelineHandler::set_OuterHandler(Amazon.Runtime.IPipelineHandler)
extern void PipelineHandler_set_OuterHandler_mDD4DE1D53C5122E7AB75AA43F93C38967F105C74 (void);
// 0x000005AE System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.PipelineHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x000005AF System.Void Amazon.Runtime.Internal.PipelineHandler::LogMetrics(Amazon.Runtime.IExecutionContext)
extern void PipelineHandler_LogMetrics_mD28DC7BB77DE6F69FAF1C5F29EB4C1DEC74CCCFE (void);
// 0x000005B0 System.Void Amazon.Runtime.Internal.PipelineHandler::.ctor()
extern void PipelineHandler__ctor_m24C7252C3D0420B488D8F8E92C13EAAE84F83247 (void);
// 0x000005B1 Amazon.Runtime.Internal.TokenBucket Amazon.Runtime.Internal.AdaptiveRetryPolicy::get_TokenBucket()
extern void AdaptiveRetryPolicy_get_TokenBucket_m262D3EE41D0717E08267DA1261D9F9BAC003C85B (void);
// 0x000005B2 System.Void Amazon.Runtime.Internal.AdaptiveRetryPolicy::.ctor(Amazon.Runtime.IClientConfig)
extern void AdaptiveRetryPolicy__ctor_m61A2CEB18BDC539B358A992C12D59A8D700ED6DA (void);
// 0x000005B3 System.Boolean Amazon.Runtime.Internal.AdaptiveRetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean,System.Boolean)
extern void AdaptiveRetryPolicy_OnRetry_m02A696DB08710AD44AA0A1FAB54BADA6DD681DB3 (void);
// 0x000005B4 System.Void Amazon.Runtime.Internal.AdaptiveRetryPolicy::NotifySuccess(Amazon.Runtime.IExecutionContext)
extern void AdaptiveRetryPolicy_NotifySuccess_m978D3764F2E4C720C1ADBF2A911B16A0026F0BA9 (void);
// 0x000005B5 System.Threading.Tasks.Task`1<System.Boolean> Amazon.Runtime.Internal.AdaptiveRetryPolicy::RetryForExceptionAsync(Amazon.Runtime.IExecutionContext,System.Exception)
extern void AdaptiveRetryPolicy_RetryForExceptionAsync_m31E28DE033B5BAEE29B2FE06811AAC2FD8270531 (void);
// 0x000005B6 System.Threading.Tasks.Task Amazon.Runtime.Internal.AdaptiveRetryPolicy::WaitBeforeRetryAsync(Amazon.Runtime.IExecutionContext)
extern void AdaptiveRetryPolicy_WaitBeforeRetryAsync_m9096E0D15A927CE5C22651E4ECD46294031ED3B3 (void);
// 0x000005B7 System.Threading.Tasks.Task Amazon.Runtime.Internal.AdaptiveRetryPolicy::ObtainSendTokenAsync(Amazon.Runtime.IExecutionContext,System.Exception)
extern void AdaptiveRetryPolicy_ObtainSendTokenAsync_m5C3B0456589FE9949204CF28185B0F6EA6C55F95 (void);
// 0x000005B8 System.Void Amazon.Runtime.Internal.AdaptiveRetryPolicy/<ObtainSendTokenAsync>d__11::MoveNext()
extern void U3CObtainSendTokenAsyncU3Ed__11_MoveNext_mF2D1AE364315836D27E81DFC6A89EC6AE0A7E305 (void);
// 0x000005B9 System.Void Amazon.Runtime.Internal.AdaptiveRetryPolicy/<ObtainSendTokenAsync>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CObtainSendTokenAsyncU3Ed__11_SetStateMachine_m5B189B23DDAAE678549DE1625AD265E0EBE6E43D (void);
// 0x000005BA System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::get_MaxBackoffInMilliseconds()
extern void DefaultRetryPolicy_get_MaxBackoffInMilliseconds_mAD8744FAF8A1B9D70F9D864F64C1A5B5FE28AE17 (void);
// 0x000005BB System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::.ctor(Amazon.Runtime.IClientConfig)
extern void DefaultRetryPolicy__ctor_mC9EFB0166A32D407F246DC47E4EA555774598C88 (void);
// 0x000005BC System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::CanRetry(Amazon.Runtime.IExecutionContext)
extern void DefaultRetryPolicy_CanRetry_m41659AFC3EFBAF722ED7FBD32CE77229945F31E4 (void);
// 0x000005BD System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean)
extern void DefaultRetryPolicy_OnRetry_mCD53101E707719478326F0417F0264FAAB443475 (void);
// 0x000005BE System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean,System.Boolean)
extern void DefaultRetryPolicy_OnRetry_m3AE0B5F55BABB08A4F0D64D698607B8BE34111CF (void);
// 0x000005BF System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::NotifySuccess(Amazon.Runtime.IExecutionContext)
extern void DefaultRetryPolicy_NotifySuccess_m197BBAE717AA2F3D9793F83E5CFE15C8B2D250A0 (void);
// 0x000005C0 System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::RetryForExceptionSync(System.Exception,Amazon.Runtime.IExecutionContext)
extern void DefaultRetryPolicy_RetryForExceptionSync_m6EE260E7E9903A9EFD7578AB702C6115C9BBBA0F (void);
// 0x000005C1 System.Boolean Amazon.Runtime.Internal.DefaultRetryPolicy::RetryLimitReached(Amazon.Runtime.IExecutionContext)
extern void DefaultRetryPolicy_RetryLimitReached_m260DE3904A49C2415B5FE5926A1C112D6CDE7E0E (void);
// 0x000005C2 System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::CalculateRetryDelay(System.Int32,System.Int32)
extern void DefaultRetryPolicy_CalculateRetryDelay_m56D66364664BF00D921A1A0C836EBB399B2EA964 (void);
// 0x000005C3 System.Threading.Tasks.Task`1<System.Boolean> Amazon.Runtime.Internal.DefaultRetryPolicy::RetryForExceptionAsync(Amazon.Runtime.IExecutionContext,System.Exception)
extern void DefaultRetryPolicy_RetryForExceptionAsync_mCE233F4A7A6B93366E6BC5AED47474C9B01DCEEC (void);
// 0x000005C4 System.Threading.Tasks.Task Amazon.Runtime.Internal.DefaultRetryPolicy::WaitBeforeRetryAsync(Amazon.Runtime.IExecutionContext)
extern void DefaultRetryPolicy_WaitBeforeRetryAsync_m7270337ADF17CF597542FAA913461B6775C813AE (void);
// 0x000005C5 System.Void Amazon.Runtime.Internal.DefaultRetryPolicy::.cctor()
extern void DefaultRetryPolicy__cctor_m0090C0706D4897FFD2A6D2E75349BF78351C042F (void);
// 0x000005C6 Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RetryHandler::get_Logger()
extern void RetryHandler_get_Logger_mFFC5E13E0606E42A8C0309E7A721636A9E696AA4 (void);
// 0x000005C7 System.Void Amazon.Runtime.Internal.RetryHandler::set_Logger(Amazon.Runtime.Internal.Util.ILogger)
extern void RetryHandler_set_Logger_m5A1DDC0D6EED44C07B33A544B53CB478A4579DE3 (void);
// 0x000005C8 Amazon.Runtime.RetryPolicy Amazon.Runtime.Internal.RetryHandler::get_RetryPolicy()
extern void RetryHandler_get_RetryPolicy_mDC30A963F84526B46BCB050863888F12D4F572BC (void);
// 0x000005C9 System.Void Amazon.Runtime.Internal.RetryHandler::set_RetryPolicy(Amazon.Runtime.RetryPolicy)
extern void RetryHandler_set_RetryPolicy_m63A981A434D1D7B794D90E8768339391FA299A0B (void);
// 0x000005CA System.Void Amazon.Runtime.Internal.RetryHandler::.ctor(Amazon.Runtime.RetryPolicy)
extern void RetryHandler__ctor_m8F983301F21F601924F126906C335A1DC8F830CC (void);
// 0x000005CB System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.RetryHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x000005CC System.Void Amazon.Runtime.Internal.RetryHandler::PrepareForRetry(Amazon.Runtime.IRequestContext)
extern void RetryHandler_PrepareForRetry_mF87FF727AC2AF75EED2596DC3077ED47496BA4E0 (void);
// 0x000005CD System.Void Amazon.Runtime.Internal.RetryHandler::LogForRetry(Amazon.Runtime.IRequestContext,System.Exception)
extern void RetryHandler_LogForRetry_m13921D86E65D150383352886EDE7E69665FB8C2F (void);
// 0x000005CE System.Void Amazon.Runtime.Internal.RetryHandler::LogForError(Amazon.Runtime.IRequestContext,System.Exception)
extern void RetryHandler_LogForError_m241E5B1F92D0C01591E666A7778E6E06BB849DC0 (void);
// 0x000005CF System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.RetryHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x000005D0 System.Void Amazon.Runtime.Internal.RetryHandler/<InvokeAsync>d__10`1::MoveNext()
// 0x000005D1 System.Void Amazon.Runtime.Internal.RetryHandler/<InvokeAsync>d__10`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000005D2 Amazon.Runtime.Internal.CapacityManager Amazon.Runtime.Internal.StandardRetryPolicy::get_CapacityManagerInstance()
extern void StandardRetryPolicy_get_CapacityManagerInstance_m5B53C8DF35CF8F30F2651706E2F2DC69289BD0F7 (void);
// 0x000005D3 System.Int32 Amazon.Runtime.Internal.StandardRetryPolicy::get_MaxBackoffInMilliseconds()
extern void StandardRetryPolicy_get_MaxBackoffInMilliseconds_m6662682822FF79F5655C62F39E86FEF381F21A04 (void);
// 0x000005D4 System.Void Amazon.Runtime.Internal.StandardRetryPolicy::.ctor(Amazon.Runtime.IClientConfig)
extern void StandardRetryPolicy__ctor_m721B68567D0DDB0ED43A32844F5B8DB800CC2447 (void);
// 0x000005D5 System.Boolean Amazon.Runtime.Internal.StandardRetryPolicy::CanRetry(Amazon.Runtime.IExecutionContext)
extern void StandardRetryPolicy_CanRetry_m929825BBE67DB18C7E23784E39F541E1B1690EA6 (void);
// 0x000005D6 System.Boolean Amazon.Runtime.Internal.StandardRetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean)
extern void StandardRetryPolicy_OnRetry_m0A7813681FC6C3BCD3A01E77E82ED83FB7E48526 (void);
// 0x000005D7 System.Boolean Amazon.Runtime.Internal.StandardRetryPolicy::OnRetry(Amazon.Runtime.IExecutionContext,System.Boolean,System.Boolean)
extern void StandardRetryPolicy_OnRetry_m2320CEB7443DC67FEB2CE34E33F890B65DA8CA55 (void);
// 0x000005D8 System.Void Amazon.Runtime.Internal.StandardRetryPolicy::NotifySuccess(Amazon.Runtime.IExecutionContext)
extern void StandardRetryPolicy_NotifySuccess_m887B0689ADC1B5BD82C29374FF140FA78827D911 (void);
// 0x000005D9 System.Boolean Amazon.Runtime.Internal.StandardRetryPolicy::RetryForExceptionSync(System.Exception,Amazon.Runtime.IExecutionContext)
extern void StandardRetryPolicy_RetryForExceptionSync_mE06331665738BE50218F5B7BAE760D1D10B08219 (void);
// 0x000005DA System.Boolean Amazon.Runtime.Internal.StandardRetryPolicy::RetryLimitReached(Amazon.Runtime.IExecutionContext)
extern void StandardRetryPolicy_RetryLimitReached_m09FFDB9EACC1D08D73244D6A23EA859F9AFFF97D (void);
// 0x000005DB System.Int32 Amazon.Runtime.Internal.StandardRetryPolicy::CalculateRetryDelay(System.Int32,System.Int32)
extern void StandardRetryPolicy_CalculateRetryDelay_m7038A8C14A2EEB2A71DDC4A415D8F79DB7EC22FC (void);
// 0x000005DC System.Threading.Tasks.Task`1<System.Boolean> Amazon.Runtime.Internal.StandardRetryPolicy::RetryForExceptionAsync(Amazon.Runtime.IExecutionContext,System.Exception)
extern void StandardRetryPolicy_RetryForExceptionAsync_m1431EEF8D53BFDF75600CCE841CD203A97A2961D (void);
// 0x000005DD System.Threading.Tasks.Task Amazon.Runtime.Internal.StandardRetryPolicy::WaitBeforeRetryAsync(Amazon.Runtime.IExecutionContext)
extern void StandardRetryPolicy_WaitBeforeRetryAsync_m1ADC5FEEE6EDDEC092FA7ED5B8FBBE3194CDE5F0 (void);
// 0x000005DE System.Void Amazon.Runtime.Internal.StandardRetryPolicy::.cctor()
extern void StandardRetryPolicy__cctor_m508B256B8D8B1FFEE6D73EEEEDDF9B16481443CB (void);
// 0x000005DF Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.RuntimePipeline::get_Handler()
extern void RuntimePipeline_get_Handler_mE947032E5BC33E6EA93489B97D340A61EF05107D (void);
// 0x000005E0 System.Void Amazon.Runtime.Internal.RuntimePipeline::.ctor(System.Collections.Generic.IList`1<Amazon.Runtime.IPipelineHandler>,Amazon.Runtime.Internal.Util.ILogger)
extern void RuntimePipeline__ctor_mDBDA5003B800B66FAC2A1FE65C60445E482F3026 (void);
// 0x000005E1 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.RuntimePipeline::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x000005E2 System.Void Amazon.Runtime.Internal.RuntimePipeline::AddHandler(Amazon.Runtime.IPipelineHandler)
extern void RuntimePipeline_AddHandler_m36440BF38C30A9159493A058DD60781372712CAF (void);
// 0x000005E3 System.Void Amazon.Runtime.Internal.RuntimePipeline::AddHandlerBefore(Amazon.Runtime.IPipelineHandler)
// 0x000005E4 System.Void Amazon.Runtime.Internal.RuntimePipeline::ReplaceHandler(Amazon.Runtime.IPipelineHandler)
// 0x000005E5 System.Void Amazon.Runtime.Internal.RuntimePipeline::InsertHandler(Amazon.Runtime.IPipelineHandler,Amazon.Runtime.IPipelineHandler)
extern void RuntimePipeline_InsertHandler_mD9AADCB0037E4111D4B35114450439CDEE2223F1 (void);
// 0x000005E6 Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.RuntimePipeline::GetInnermostHandler(Amazon.Runtime.IPipelineHandler)
extern void RuntimePipeline_GetInnermostHandler_mCE9998B71F55C07800349CCCAF43D06528484440 (void);
// 0x000005E7 System.Void Amazon.Runtime.Internal.RuntimePipeline::SetHandlerProperties(Amazon.Runtime.IPipelineHandler)
extern void RuntimePipeline_SetHandlerProperties_m4C3424ECB28F84ED10EA7DB0799BD796C273F6A5 (void);
// 0x000005E8 System.Void Amazon.Runtime.Internal.RuntimePipeline::Dispose()
extern void RuntimePipeline_Dispose_mFB7683DAB71E95B4786C29A81BDAF197904BD033 (void);
// 0x000005E9 System.Void Amazon.Runtime.Internal.RuntimePipeline::Dispose(System.Boolean)
extern void RuntimePipeline_Dispose_m768257612CF16E565E799B20071CC38CBAD3070C (void);
// 0x000005EA System.Void Amazon.Runtime.Internal.RuntimePipeline::ThrowIfDisposed()
extern void RuntimePipeline_ThrowIfDisposed_mA71613F23F4A21C635C9959E8C3E5200E98C3AEF (void);
// 0x000005EB System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CSMCallAttemptHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x000005EC System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler::CSMCallAttemptMetricsCapture(Amazon.Runtime.IRequestContext,Amazon.Runtime.IResponseContext)
extern void CSMCallAttemptHandler_CSMCallAttemptMetricsCapture_mEE265B739A42FBD97E5929AA0BF6402C9A55D816 (void);
// 0x000005ED System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void CSMCallAttemptHandler_PreInvoke_mAB527B2E25555F4EA2B5DF709CEF8665FD4DCB9E (void);
// 0x000005EE System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler::CaptureSDKExceptionMessage(Amazon.Runtime.Internal.MonitoringAPICallAttempt,System.Exception)
extern void CSMCallAttemptHandler_CaptureSDKExceptionMessage_mCBEE9D9246EF9D0D90B3DA0089247773CF5F5EA3 (void);
// 0x000005EF System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler::CaptureAmazonException(Amazon.Runtime.Internal.MonitoringAPICallAttempt,Amazon.Runtime.AmazonServiceException)
extern void CSMCallAttemptHandler_CaptureAmazonException_mF6692471E19EA3E775018DBA525CDC9BA8B3466A (void);
// 0x000005F0 System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler::.ctor()
extern void CSMCallAttemptHandler__ctor_m6CF7C324C210DD41EF9F7CDEB0B2F1552203CBE3 (void);
// 0x000005F1 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CSMCallAttemptHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x000005F2 System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler/<InvokeAsync>d__1`1::MoveNext()
// 0x000005F3 System.Void Amazon.Runtime.Internal.CSMCallAttemptHandler/<InvokeAsync>d__1`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000005F4 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CSMCallEventHandler::InvokeAsync(Amazon.Runtime.IExecutionContext)
// 0x000005F5 System.Void Amazon.Runtime.Internal.CSMCallEventHandler::CSMCallEventMetricsCapture(Amazon.Runtime.IExecutionContext)
extern void CSMCallEventHandler_CSMCallEventMetricsCapture_m125407A8E7A4635F48FB04E0AF1A44E796DA9A6C (void);
// 0x000005F6 System.Void Amazon.Runtime.Internal.CSMCallEventHandler::CaptureCSMCallEventExceptionData(Amazon.Runtime.IRequestContext,System.Exception)
extern void CSMCallEventHandler_CaptureCSMCallEventExceptionData_m6A6446AF068C18EAFA2DEF7B9F1B8FC6701901E9 (void);
// 0x000005F7 System.Void Amazon.Runtime.Internal.CSMCallEventHandler::PreInvoke(Amazon.Runtime.IExecutionContext)
extern void CSMCallEventHandler_PreInvoke_mF09B7A794A6A5790D934DEE53FC9F4317D83CB8A (void);
// 0x000005F8 System.Void Amazon.Runtime.Internal.CSMCallEventHandler::.ctor()
extern void CSMCallEventHandler__ctor_mBA8342C811C44D44250DADCAA429B7F5C8028D5C (void);
// 0x000005F9 System.Threading.Tasks.Task`1<T> Amazon.Runtime.Internal.CSMCallEventHandler::<>n__0(Amazon.Runtime.IExecutionContext)
// 0x000005FA System.Void Amazon.Runtime.Internal.CSMCallEventHandler/<InvokeAsync>d__2`1::MoveNext()
// 0x000005FB System.Void Amazon.Runtime.Internal.CSMCallEventHandler/<InvokeAsync>d__2`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000005FC System.Nullable`1<System.Double> Amazon.Runtime.Internal.TokenBucket::get_FillRate()
extern void TokenBucket_get_FillRate_mA0A7ED0D0D33364D5A61E2BBBC7E6C3F664727F7 (void);
// 0x000005FD System.Void Amazon.Runtime.Internal.TokenBucket::set_FillRate(System.Nullable`1<System.Double>)
extern void TokenBucket_set_FillRate_m72489ECECCF952D97E9A6C0D9B7E28CE87DEDC12 (void);
// 0x000005FE System.Nullable`1<System.Double> Amazon.Runtime.Internal.TokenBucket::get_MaxCapacity()
extern void TokenBucket_get_MaxCapacity_mE1472754E58556B1E734686A99F455499093F774 (void);
// 0x000005FF System.Void Amazon.Runtime.Internal.TokenBucket::set_MaxCapacity(System.Nullable`1<System.Double>)
extern void TokenBucket_set_MaxCapacity_mC9EE0F436FA9DA0B77AB46F8DDACE7EE35C8942F (void);
// 0x00000600 System.Double Amazon.Runtime.Internal.TokenBucket::get_CurrentCapacity()
extern void TokenBucket_get_CurrentCapacity_mA421C72A7EED744D51FF1C8042EFB16E090F2C6F (void);
// 0x00000601 System.Void Amazon.Runtime.Internal.TokenBucket::set_CurrentCapacity(System.Double)
extern void TokenBucket_set_CurrentCapacity_mCE6F1E8099C66C0A073A45B760E7E74A8E0EA8AB (void);
// 0x00000602 System.Nullable`1<System.Double> Amazon.Runtime.Internal.TokenBucket::get_LastTimestamp()
extern void TokenBucket_get_LastTimestamp_m0FE13E882A81F71410D028982FB12C3A2FCD9158 (void);
// 0x00000603 System.Void Amazon.Runtime.Internal.TokenBucket::set_LastTimestamp(System.Nullable`1<System.Double>)
extern void TokenBucket_set_LastTimestamp_mD6473615DB7CC4E992DE2A09264467D9E62B7B12 (void);
// 0x00000604 System.Double Amazon.Runtime.Internal.TokenBucket::get_MeasuredTxRate()
extern void TokenBucket_get_MeasuredTxRate_m1A629CE8A0D664B7A488ECA0893CBA3E5B3344CE (void);
// 0x00000605 System.Void Amazon.Runtime.Internal.TokenBucket::set_MeasuredTxRate(System.Double)
extern void TokenBucket_set_MeasuredTxRate_mFB5DE4F1071C968447A87E109189008485EE0EAD (void);
// 0x00000606 System.Double Amazon.Runtime.Internal.TokenBucket::get_LastTxRateBucket()
extern void TokenBucket_get_LastTxRateBucket_m02293BF2983E261302A22A14DF81AAF421ED0779 (void);
// 0x00000607 System.Void Amazon.Runtime.Internal.TokenBucket::set_LastTxRateBucket(System.Double)
extern void TokenBucket_set_LastTxRateBucket_m218A404F7B552EB50D6C40D0140C222BF1DB83E1 (void);
// 0x00000608 System.Int64 Amazon.Runtime.Internal.TokenBucket::get_RequestCount()
extern void TokenBucket_get_RequestCount_m05B019782DEB0D4D21A960F659A6E1B3B45DEBA7 (void);
// 0x00000609 System.Void Amazon.Runtime.Internal.TokenBucket::set_RequestCount(System.Int64)
extern void TokenBucket_set_RequestCount_mB65440290DFA7CA01D193C6815F774E4E70E5CD6 (void);
// 0x0000060A System.Double Amazon.Runtime.Internal.TokenBucket::get_LastMaxRate()
extern void TokenBucket_get_LastMaxRate_m18DF834E4844CEC6844F4DEABBF8B6AE3A1CB658 (void);
// 0x0000060B System.Void Amazon.Runtime.Internal.TokenBucket::set_LastMaxRate(System.Double)
extern void TokenBucket_set_LastMaxRate_m6F3B8E232E1213DC18CD217A37A2F345B2E0D8A9 (void);
// 0x0000060C System.Double Amazon.Runtime.Internal.TokenBucket::get_LastThrottleTime()
extern void TokenBucket_get_LastThrottleTime_mD3267451748BD6074DEFB8ECA779D9E630D10765 (void);
// 0x0000060D System.Void Amazon.Runtime.Internal.TokenBucket::set_LastThrottleTime(System.Double)
extern void TokenBucket_set_LastThrottleTime_m88EAAC3991614FD2C871E79FBF147CDB303B4CBC (void);
// 0x0000060E System.Double Amazon.Runtime.Internal.TokenBucket::get_TimeWindow()
extern void TokenBucket_get_TimeWindow_m7916F196613D45C135BD7A1E263326F5B4993F91 (void);
// 0x0000060F System.Void Amazon.Runtime.Internal.TokenBucket::set_TimeWindow(System.Double)
extern void TokenBucket_set_TimeWindow_mDB11F5A40AEEE3B6F25ACA9AB5768DC7263819F5 (void);
// 0x00000610 System.Boolean Amazon.Runtime.Internal.TokenBucket::get_Enabled()
extern void TokenBucket_get_Enabled_m776C13DE5371FB8CBCB2E21F2999FEE99B03F5FB (void);
// 0x00000611 System.Void Amazon.Runtime.Internal.TokenBucket::set_Enabled(System.Boolean)
extern void TokenBucket_set_Enabled_m095AB1B9C82FFA6598F942926581D6D1AF7820AF (void);
// 0x00000612 System.Void Amazon.Runtime.Internal.TokenBucket::.ctor()
extern void TokenBucket__ctor_mC73F1B30FE63F3D64924C2B3988B5EF92038E18E (void);
// 0x00000613 System.Void Amazon.Runtime.Internal.TokenBucket::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double)
extern void TokenBucket__ctor_m27AAA8A68D72B56A02262DBC4BA55EE07AB1E203 (void);
// 0x00000614 System.Threading.Tasks.Task`1<System.Boolean> Amazon.Runtime.Internal.TokenBucket::TryAcquireTokenAsync(System.Double,System.Boolean,System.Threading.CancellationToken)
extern void TokenBucket_TryAcquireTokenAsync_mC4D9588455662EC0320D548DA857AF578A5A7C3F (void);
// 0x00000615 System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.TokenBucket::SetupAcquireToken(System.Double)
extern void TokenBucket_SetupAcquireToken_m6BFB05D4BA6560A90074B22FB32B2EF8F8AAB196 (void);
// 0x00000616 System.Int32 Amazon.Runtime.Internal.TokenBucket::ObtainCapacity(System.Double)
extern void TokenBucket_ObtainCapacity_m7800AF5B7DD26EA404F6A29906CCE05338039E75 (void);
// 0x00000617 System.Void Amazon.Runtime.Internal.TokenBucket::UpdateClientSendingRate(System.Boolean)
extern void TokenBucket_UpdateClientSendingRate_m9F6F700B112780CBA435167BFB914EA1C03826D2 (void);
// 0x00000618 System.Void Amazon.Runtime.Internal.TokenBucket::TokenBucketRefill()
extern void TokenBucket_TokenBucketRefill_m89A8A2FEC0B0E44DE526D0310974D7F9B484C11F (void);
// 0x00000619 System.Void Amazon.Runtime.Internal.TokenBucket::TokenBucketUpdateRate(System.Double)
extern void TokenBucket_TokenBucketUpdateRate_m0B7C52D2C1F1C589019073109093EA04623524BB (void);
// 0x0000061A System.Void Amazon.Runtime.Internal.TokenBucket::UpdateMeasuredRate()
extern void TokenBucket_UpdateMeasuredRate_m085E3CE72BFF2DDA61EAFA35051CD5B7A98158AE (void);
// 0x0000061B System.Void Amazon.Runtime.Internal.TokenBucket::CalculateTimeWindow()
extern void TokenBucket_CalculateTimeWindow_m2FB4A15344D62CB72AC672AE0EEE5BF25664FD15 (void);
// 0x0000061C System.Double Amazon.Runtime.Internal.TokenBucket::CUBICSuccess(System.Double)
extern void TokenBucket_CUBICSuccess_mDB554F541B22FE9746354AC13424AADA9591F794 (void);
// 0x0000061D System.Double Amazon.Runtime.Internal.TokenBucket::CUBICThrottle(System.Double)
extern void TokenBucket_CUBICThrottle_m60EEB958CD59B5D8854F91D37A3EA3C26709F9F0 (void);
// 0x0000061E System.Int32 Amazon.Runtime.Internal.TokenBucket::CalculateWait(System.Double,System.Double,System.Double)
extern void TokenBucket_CalculateWait_mF67858CACC98BD4DA9EAE3A9F14A3432343BA250 (void);
// 0x0000061F System.Threading.Tasks.Task Amazon.Runtime.Internal.TokenBucket::WaitForTokenAsync(System.Int32,System.Threading.CancellationToken)
extern void TokenBucket_WaitForTokenAsync_m928EB18E092E08F143F8EBDC694B5E9C3CB91CD5 (void);
// 0x00000620 System.Double Amazon.Runtime.Internal.TokenBucket::GetTimestamp()
extern void TokenBucket_GetTimestamp_m398D3868281BF899BB9E53B04CAAD0CB36F5C745 (void);
// 0x00000621 System.Double Amazon.Runtime.Internal.TokenBucket::GetTimeInSeconds()
extern void TokenBucket_GetTimeInSeconds_mCEC49103AA3DA6213A365B4D92286D2B02031CAB (void);
// 0x00000622 System.Void Amazon.Runtime.Internal.TokenBucket::.cctor()
extern void TokenBucket__cctor_mEAA861DF00A661A389BC5A1006E784D6FB20B39B (void);
// 0x00000623 System.Void Amazon.Runtime.Internal.TokenBucket/<TryAcquireTokenAsync>d__55::MoveNext()
extern void U3CTryAcquireTokenAsyncU3Ed__55_MoveNext_m2F7C74047347318A1AB0C15C16CDF097C9E0E7EC (void);
// 0x00000624 System.Void Amazon.Runtime.Internal.TokenBucket/<TryAcquireTokenAsync>d__55::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTryAcquireTokenAsyncU3Ed__55_SetStateMachine_mC8FB6D650565057DAF0C7310E6CFC027923C0B62 (void);
// 0x00000625 System.Void Amazon.Runtime.Internal.TokenBucket/<WaitForTokenAsync>d__67::MoveNext()
extern void U3CWaitForTokenAsyncU3Ed__67_MoveNext_mDBAC6DA4007E72A458F4CD637B22715939DC38E3 (void);
// 0x00000626 System.Void Amazon.Runtime.Internal.TokenBucket/<WaitForTokenAsync>d__67::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWaitForTokenAsyncU3Ed__67_SetStateMachine_mCA1B76DFBDD3ED10552418255052F57DA8067186 (void);
// 0x00000627 System.String Amazon.Runtime.Internal.CSMConfiguration::get_Host()
extern void CSMConfiguration_get_Host_mB62FF9EA748848CC74E83DB1F44655C0BD243E92 (void);
// 0x00000628 System.Void Amazon.Runtime.Internal.CSMConfiguration::set_Host(System.String)
extern void CSMConfiguration_set_Host_m9B1428EB43FB9DC145988A0AE89AF5A0285BF471 (void);
// 0x00000629 System.Int32 Amazon.Runtime.Internal.CSMConfiguration::get_Port()
extern void CSMConfiguration_get_Port_m1E23C410430AA481A44D5B334219AA366F8C1059 (void);
// 0x0000062A System.Void Amazon.Runtime.Internal.CSMConfiguration::set_Port(System.Int32)
extern void CSMConfiguration_set_Port_m51AFCCA8066C2479050A5D4395B625A824D47475 (void);
// 0x0000062B System.Boolean Amazon.Runtime.Internal.CSMConfiguration::get_Enabled()
extern void CSMConfiguration_get_Enabled_m81AD5D5A20D6A39D2F8A2620EBC7B89AD6731E32 (void);
// 0x0000062C System.Void Amazon.Runtime.Internal.CSMConfiguration::set_Enabled(System.Boolean)
extern void CSMConfiguration_set_Enabled_mD6BC7CD6553DC314AAB1B598F5F260335F8427AB (void);
// 0x0000062D System.String Amazon.Runtime.Internal.CSMConfiguration::get_ClientId()
extern void CSMConfiguration_get_ClientId_m4CEA76CCE2A19DF9F341B90C1001B38755A436A6 (void);
// 0x0000062E System.Void Amazon.Runtime.Internal.CSMConfiguration::set_ClientId(System.String)
extern void CSMConfiguration_set_ClientId_m5A2E035C2DBA99F74DA66203FCE3F5303BB83E3E (void);
// 0x0000062F System.Void Amazon.Runtime.Internal.CSMConfiguration::.ctor()
extern void CSMConfiguration__ctor_m2347F69B8A36555D9C22792B38210AF059B247C3 (void);
// 0x00000630 System.Collections.Generic.List`1<Amazon.Runtime.Internal.CSMFallbackConfigChain/ConfigurationSource> Amazon.Runtime.Internal.CSMFallbackConfigChain::get_AllGenerators()
extern void CSMFallbackConfigChain_get_AllGenerators_mB2A3806671D707BA6166978686F6B5BA9FAF31F4 (void);
// 0x00000631 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::set_AllGenerators(System.Collections.Generic.List`1<Amazon.Runtime.Internal.CSMFallbackConfigChain/ConfigurationSource>)
extern void CSMFallbackConfigChain_set_AllGenerators_mFF5CE6CA4C99BD98545E405EA4FB105E41EB54A4 (void);
// 0x00000632 System.Boolean Amazon.Runtime.Internal.CSMFallbackConfigChain::get_IsConfigSet()
extern void CSMFallbackConfigChain_get_IsConfigSet_m31F9CCE5FC84C9C093A4DCD333B9E34A3769EB11 (void);
// 0x00000633 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::set_IsConfigSet(System.Boolean)
extern void CSMFallbackConfigChain_set_IsConfigSet_mA9BEC2B921058B43BED14457DF2A83CD720AB1CF (void);
// 0x00000634 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::set_ConfigSource(System.String)
extern void CSMFallbackConfigChain_set_ConfigSource_mB2E85549468F1E0237B189424F548A851D043F68 (void);
// 0x00000635 Amazon.Runtime.Internal.CSMConfiguration Amazon.Runtime.Internal.CSMFallbackConfigChain::get_CSMConfiguration()
extern void CSMFallbackConfigChain_get_CSMConfiguration_m529E6F75EA90D53EA97DF81FCC1F8AC8E8225766 (void);
// 0x00000636 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::set_CSMConfiguration(Amazon.Runtime.Internal.CSMConfiguration)
extern void CSMFallbackConfigChain_set_CSMConfiguration_mB622A47543892199ABEF2C8F79E586D01B3CD5CC (void);
// 0x00000637 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::.ctor()
extern void CSMFallbackConfigChain__ctor_mE475D0EA772F6219E4281D56778E764237ACB8E6 (void);
// 0x00000638 Amazon.Runtime.Internal.CSMConfiguration Amazon.Runtime.Internal.CSMFallbackConfigChain::GetCSMConfig()
extern void CSMFallbackConfigChain_GetCSMConfig_m428FB35FC6AD6C018DA2A82D1A7434F14DF28C72 (void);
// 0x00000639 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::.cctor()
extern void CSMFallbackConfigChain__cctor_m7119AD292023B64759A50E62C3F3080414B4FE6E (void);
// 0x0000063A System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::<.ctor>b__19_0()
extern void CSMFallbackConfigChain_U3C_ctorU3Eb__19_0_m2AA81B437DA8CF71B653F813766CAD9A12B224BE (void);
// 0x0000063B System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::<.ctor>b__19_1()
extern void CSMFallbackConfigChain_U3C_ctorU3Eb__19_1_mFC07E82E37B78AFB08278848459549CC77CD5658 (void);
// 0x0000063C System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain::<.ctor>b__19_2()
extern void CSMFallbackConfigChain_U3C_ctorU3Eb__19_2_mCEB6395641F132B61A623792AC2A39C8F41D102F (void);
// 0x0000063D System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain/ConfigurationSource::.ctor(System.Object,System.IntPtr)
extern void ConfigurationSource__ctor_m45246B3630682C48A8AD509A61BFF40D12603123 (void);
// 0x0000063E System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain/ConfigurationSource::Invoke()
extern void ConfigurationSource_Invoke_mBAC172A6559EAB45BD378F7042F0035D9EB1F21E (void);
// 0x0000063F System.IAsyncResult Amazon.Runtime.Internal.CSMFallbackConfigChain/ConfigurationSource::BeginInvoke(System.AsyncCallback,System.Object)
extern void ConfigurationSource_BeginInvoke_m41CE3867E3DC26D6EDD76350928ABABB1E4AE6AE (void);
// 0x00000640 System.Void Amazon.Runtime.Internal.CSMFallbackConfigChain/ConfigurationSource::EndInvoke(System.IAsyncResult)
extern void ConfigurationSource_EndInvoke_mC824F9C1E861939E1781ABE58E84B67818D17328 (void);
// 0x00000641 System.String Amazon.Runtime.Internal.ProfileCSMConfigs::get_ProfileName()
extern void ProfileCSMConfigs_get_ProfileName_mBDAFA68A6EB56A0C3F5779C8D3DCC4F9336F0519 (void);
// 0x00000642 System.Void Amazon.Runtime.Internal.ProfileCSMConfigs::set_ProfileName(System.String)
extern void ProfileCSMConfigs_set_ProfileName_m4AF99B4E75DB4445951F8F25191C5BF1E69090DE (void);
// 0x00000643 System.Void Amazon.Runtime.Internal.ProfileCSMConfigs::.ctor(Amazon.Runtime.CredentialManagement.ICredentialProfileSource,Amazon.Runtime.Internal.CSMFallbackConfigChain)
extern void ProfileCSMConfigs__ctor_mA2EF7F56AC946C979BBD995A1024623AC220F845 (void);
// 0x00000644 System.Void Amazon.Runtime.Internal.ProfileCSMConfigs::Setup(Amazon.Runtime.Internal.CSMFallbackConfigChain,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void ProfileCSMConfigs_Setup_mD5D6F3521858634B4237CA5B5F572046C285E538 (void);
// 0x00000645 Amazon.Util.Internal.IEnvironmentVariableRetriever Amazon.Runtime.Internal.EnvironmentVariableCSMConfigs::get_environmentRetriever()
extern void EnvironmentVariableCSMConfigs_get_environmentRetriever_m0DB2581E62898D47F6616E31AA36CAF0146E028E (void);
// 0x00000646 System.Void Amazon.Runtime.Internal.EnvironmentVariableCSMConfigs::.ctor(Amazon.Runtime.Internal.CSMFallbackConfigChain)
extern void EnvironmentVariableCSMConfigs__ctor_m98203DB9D48990DEEA63375C7C4D2990832F1E73 (void);
// 0x00000647 System.Void Amazon.Runtime.Internal.EnvironmentVariableCSMConfigs::SetupConfiguration(Amazon.Runtime.Internal.CSMFallbackConfigChain)
extern void EnvironmentVariableCSMConfigs_SetupConfiguration_m595BFAB9E6A8FE2CE7A72791CB756A95B11BBA43 (void);
// 0x00000648 System.Void Amazon.Runtime.Internal.AppConfigCSMConfigs::.ctor(Amazon.Runtime.Internal.CSMFallbackConfigChain)
extern void AppConfigCSMConfigs__ctor_mBE7D5A9DE2E53008B29690269C8158D8CBF1F897 (void);
// 0x00000649 System.Threading.Tasks.Task Amazon.Runtime.Internal.CSMUtilities::SerializetoJsonAndPostOverUDPAsync(Amazon.Runtime.Internal.MonitoringAPICall)
extern void CSMUtilities_SerializetoJsonAndPostOverUDPAsync_m1BC4CBD268D9855B6A6404405B6AD0248929E579 (void);
// 0x0000064A System.String Amazon.Runtime.Internal.CSMUtilities::GetApiNameFromRequest(System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.String)
extern void CSMUtilities_GetApiNameFromRequest_m03C1B1BFB9280589BDD4B1CF91B1AE39B68C6D2A (void);
// 0x0000064B System.Boolean Amazon.Runtime.Internal.CSMUtilities::CreateUDPMessage(Amazon.Runtime.Internal.MonitoringAPICallAttempt,System.String&)
extern void CSMUtilities_CreateUDPMessage_mF3A8E785B874AE941978B9DF9E0E08AE39EE5F1B (void);
// 0x0000064C System.Boolean Amazon.Runtime.Internal.CSMUtilities::CreateUDPMessage(Amazon.Runtime.Internal.MonitoringAPICallEvent,System.String&)
extern void CSMUtilities_CreateUDPMessage_mB74C96B974CA490C38F97C3A6862F73BC32ED801 (void);
// 0x0000064D ThirdParty.Json.LitJson.JsonWriter Amazon.Runtime.Internal.CSMUtilities::CreateUDPMessage(Amazon.Runtime.Internal.MonitoringAPICall,ThirdParty.Json.LitJson.JsonWriter)
extern void CSMUtilities_CreateUDPMessage_m6374AA68C6BBC48F82C5097D7F0A08CBDE08E9A6 (void);
// 0x0000064E Amazon.Runtime.Internal.CSMConfiguration Amazon.Runtime.Internal.DeterminedCSMConfiguration::get_CSMConfiguration()
extern void DeterminedCSMConfiguration_get_CSMConfiguration_mAB96F0DC4AE5F8F1DB7727A094F2BA66BD933382 (void);
// 0x0000064F System.Void Amazon.Runtime.Internal.DeterminedCSMConfiguration::set_CSMConfiguration(Amazon.Runtime.Internal.CSMConfiguration)
extern void DeterminedCSMConfiguration_set_CSMConfiguration_m5D22C46AFCFD26C97C8F6E662F9D0475C4F9D8EF (void);
// 0x00000650 System.Void Amazon.Runtime.Internal.DeterminedCSMConfiguration::.ctor()
extern void DeterminedCSMConfiguration__ctor_m3CD1B062031C383204A047CFF07F079FA4254FB2 (void);
// 0x00000651 System.Void Amazon.Runtime.Internal.DeterminedCSMConfiguration::.cctor()
extern void DeterminedCSMConfiguration__cctor_mE94297B48C485F1C51640E716E3A3BB1B14EBD55 (void);
// 0x00000652 Amazon.Runtime.Internal.DeterminedCSMConfiguration Amazon.Runtime.Internal.DeterminedCSMConfiguration::get_Instance()
extern void DeterminedCSMConfiguration_get_Instance_m6487A4331D0A2930011CD6FF2DF23204D9DF00C9 (void);
// 0x00000653 System.Void Amazon.Runtime.Internal.MonitoringAPICall::.ctor(Amazon.Runtime.IRequestContext)
extern void MonitoringAPICall__ctor_m378C9985448873908D3C6C0E1B0510739212DFA8 (void);
// 0x00000654 System.Void Amazon.Runtime.Internal.MonitoringAPICall::.ctor()
extern void MonitoringAPICall__ctor_mA947B2F3508E810AD7928BD39564199E662CDDEA (void);
// 0x00000655 System.String Amazon.Runtime.Internal.MonitoringAPICall::get_Api()
extern void MonitoringAPICall_get_Api_m0421D8D4DA5663899D39BD23D46C40EBBA6F3483 (void);
// 0x00000656 System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_Api(System.String)
extern void MonitoringAPICall_set_Api_mC4C55733606CFBF05B305CB46D6E8B4B005CEE30 (void);
// 0x00000657 System.String Amazon.Runtime.Internal.MonitoringAPICall::get_Service()
extern void MonitoringAPICall_get_Service_m3B91F2CD211A542CBB96C9136BA39202067353A0 (void);
// 0x00000658 System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_Service(System.String)
extern void MonitoringAPICall_set_Service_m144B8CFF0ECB8CC57B168F4069A84BD6AA82920D (void);
// 0x00000659 System.String Amazon.Runtime.Internal.MonitoringAPICall::get_ClientId()
extern void MonitoringAPICall_get_ClientId_m2A044772ADA95AC4480BC9951B69BF92044042B5 (void);
// 0x0000065A System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_ClientId(System.String)
extern void MonitoringAPICall_set_ClientId_m6C80B675D0CB589DCFCB208BCC6E9F3CA12183D4 (void);
// 0x0000065B System.Int64 Amazon.Runtime.Internal.MonitoringAPICall::get_Timestamp()
extern void MonitoringAPICall_get_Timestamp_m1A5EDDC9894C0FB1AEB4F00CCD8870B1A6604936 (void);
// 0x0000065C System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_Timestamp(System.Int64)
extern void MonitoringAPICall_set_Timestamp_m03E081ACED1B45E8652C1DEFC3AE78220C6B2E22 (void);
// 0x0000065D System.String Amazon.Runtime.Internal.MonitoringAPICall::get_Type()
extern void MonitoringAPICall_get_Type_m838B0741BBF668D7B367C1C3BAF1309D7D4071ED (void);
// 0x0000065E System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_Type(System.String)
extern void MonitoringAPICall_set_Type_m9AEF4F1CBD04509B7926C45ADC98C15936DA4AB3 (void);
// 0x0000065F System.Int32 Amazon.Runtime.Internal.MonitoringAPICall::get_Version()
extern void MonitoringAPICall_get_Version_m1350F113A9E10FDB65F58362C721175A6F91B4AE (void);
// 0x00000660 System.String Amazon.Runtime.Internal.MonitoringAPICall::get_Region()
extern void MonitoringAPICall_get_Region_m5CD08A43220F126CB25B2AB57CA2D3D774FD41A5 (void);
// 0x00000661 System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_Region(System.String)
extern void MonitoringAPICall_set_Region_m3D084EFEDF90D8B8449E28A372C780B999ED0E0B (void);
// 0x00000662 System.String Amazon.Runtime.Internal.MonitoringAPICall::get_UserAgent()
extern void MonitoringAPICall_get_UserAgent_mDCF593DAA9DF1950F9788E0DF4D42CEB1E627C02 (void);
// 0x00000663 System.Void Amazon.Runtime.Internal.MonitoringAPICall::set_UserAgent(System.String)
extern void MonitoringAPICall_set_UserAgent_m1F837089B138AD498DF2950E1F390D0D6C0D64E8 (void);
// 0x00000664 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::.ctor(Amazon.Runtime.IRequestContext)
extern void MonitoringAPICallAttempt__ctor_m4E326C45F9881D516F1D74B1CC72A4B7E66FBB7B (void);
// 0x00000665 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_Fqdn()
extern void MonitoringAPICallAttempt_get_Fqdn_mC1C7B451BFB6364D78DB00E5070E111FAB35D0CF (void);
// 0x00000666 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_Fqdn(System.String)
extern void MonitoringAPICallAttempt_set_Fqdn_m01BE278F2D17A95AB94C37F7FC59A4E9FD64313F (void);
// 0x00000667 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_SessionToken()
extern void MonitoringAPICallAttempt_get_SessionToken_mFDA438C3142E23E75C4B0132490CF1F519C1FD80 (void);
// 0x00000668 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_SessionToken(System.String)
extern void MonitoringAPICallAttempt_set_SessionToken_m945A8D8440A19BCDD6576467B44894A81BFF9EA4 (void);
// 0x00000669 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_AccessKey()
extern void MonitoringAPICallAttempt_get_AccessKey_m96D3FB844360E8D74D7FBEAA11076D33EB01C515 (void);
// 0x0000066A System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_AccessKey(System.String)
extern void MonitoringAPICallAttempt_set_AccessKey_m4A6EA9A4C5E28F7CF04F7B02EE0113183AD8F164 (void);
// 0x0000066B System.Nullable`1<System.Int32> Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_HttpStatusCode()
extern void MonitoringAPICallAttempt_get_HttpStatusCode_mD24FDF30CD12CD3F88848993681D41F93781C502 (void);
// 0x0000066C System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_HttpStatusCode(System.Nullable`1<System.Int32>)
extern void MonitoringAPICallAttempt_set_HttpStatusCode_m987F92618CAD35F5BC8668334BBF5EAC3B0FBBDE (void);
// 0x0000066D System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_SdkExceptionMessage()
extern void MonitoringAPICallAttempt_get_SdkExceptionMessage_m630E9AEDE21F51C10E55EFEC2C6C3230D0EF8871 (void);
// 0x0000066E System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_SdkExceptionMessage(System.String)
extern void MonitoringAPICallAttempt_set_SdkExceptionMessage_mBE3B617CA3F000E2B607171A82C93150132B395C (void);
// 0x0000066F System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_SdkException()
extern void MonitoringAPICallAttempt_get_SdkException_m094D5FBD1FA02A53608668E5891F650005D7F0D0 (void);
// 0x00000670 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_SdkException(System.String)
extern void MonitoringAPICallAttempt_set_SdkException_m96DA837D58F7B9E33292F7DAB80C6D616FE7246E (void);
// 0x00000671 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_AWSException()
extern void MonitoringAPICallAttempt_get_AWSException_m621BF4D5C58B7ABB9FEFCE8EF74F78C85428FE5E (void);
// 0x00000672 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_AWSException(System.String)
extern void MonitoringAPICallAttempt_set_AWSException_m8A014D4863F83034A00C45402AA37DF090DEFB24 (void);
// 0x00000673 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_AWSExceptionMessage()
extern void MonitoringAPICallAttempt_get_AWSExceptionMessage_m1C9CA1F161FE48A9E07E0B75CA28C5B1B784599F (void);
// 0x00000674 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_AWSExceptionMessage(System.String)
extern void MonitoringAPICallAttempt_set_AWSExceptionMessage_m7112DBAA36E0128BC4BE3D4522EDF9062D57ABCA (void);
// 0x00000675 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_XAmznRequestId()
extern void MonitoringAPICallAttempt_get_XAmznRequestId_m45F79A7B23338AEE1F00F484DFFFAAF6A6EAE3E4 (void);
// 0x00000676 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_XAmznRequestId(System.String)
extern void MonitoringAPICallAttempt_set_XAmznRequestId_mD2E1584FB7FFE82F4CF4830E1489132D01AE94F3 (void);
// 0x00000677 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_XAmzRequestId()
extern void MonitoringAPICallAttempt_get_XAmzRequestId_mD13076615AEADA948BEDB6B9E9BED3C1992AA04F (void);
// 0x00000678 System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_XAmzRequestId(System.String)
extern void MonitoringAPICallAttempt_set_XAmzRequestId_m641F0ABC8509CBC655349F822A3568D3B11B995E (void);
// 0x00000679 System.String Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_XAmzId2()
extern void MonitoringAPICallAttempt_get_XAmzId2_m63ECE3EA15B22180CEDB408A79A7F1B4BC76F37B (void);
// 0x0000067A System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_XAmzId2(System.String)
extern void MonitoringAPICallAttempt_set_XAmzId2_m5CA9D84BDD1AEAD6F04509F43D3A0D8FA0E356D6 (void);
// 0x0000067B System.Int64 Amazon.Runtime.Internal.MonitoringAPICallAttempt::get_AttemptLatency()
extern void MonitoringAPICallAttempt_get_AttemptLatency_m16BA30F9971385B8E01E41FE78961DFBFF81E6C1 (void);
// 0x0000067C System.Void Amazon.Runtime.Internal.MonitoringAPICallAttempt::set_AttemptLatency(System.Int64)
extern void MonitoringAPICallAttempt_set_AttemptLatency_m0CAEAAC28246217A1C9964A1779C3B58F5D6D926 (void);
// 0x0000067D System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::.ctor(Amazon.Runtime.IRequestContext)
extern void MonitoringAPICallEvent__ctor_m14D2BFF02B4B89557736106A001E2E97321B1E3F (void);
// 0x0000067E System.Int32 Amazon.Runtime.Internal.MonitoringAPICallEvent::get_AttemptCount()
extern void MonitoringAPICallEvent_get_AttemptCount_m9FD13509E959B8D2845F6B343977E8C868076AAD (void);
// 0x0000067F System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_AttemptCount(System.Int32)
extern void MonitoringAPICallEvent_set_AttemptCount_mBD8F813864ADCEF4408000597EF31D39219B723C (void);
// 0x00000680 System.Int64 Amazon.Runtime.Internal.MonitoringAPICallEvent::get_Latency()
extern void MonitoringAPICallEvent_get_Latency_m03DA86A6D65A9293B19F3C3A6BE18EEDD91E8344 (void);
// 0x00000681 System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_Latency(System.Int64)
extern void MonitoringAPICallEvent_set_Latency_mC5CD2AE1112B756AE6BF5E7DC5B8B0516533FED6 (void);
// 0x00000682 System.Boolean Amazon.Runtime.Internal.MonitoringAPICallEvent::get_IsLastExceptionRetryable()
extern void MonitoringAPICallEvent_get_IsLastExceptionRetryable_m02D2DB01CD71002D7BD5F7E7B6370B338BD5781F (void);
// 0x00000683 System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_IsLastExceptionRetryable(System.Boolean)
extern void MonitoringAPICallEvent_set_IsLastExceptionRetryable_mAA7FD50220A6E6D50F202F17AFF8E6224D3F348B (void);
// 0x00000684 System.String Amazon.Runtime.Internal.MonitoringAPICallEvent::get_FinalSdkExceptionMessage()
extern void MonitoringAPICallEvent_get_FinalSdkExceptionMessage_mB883D7A30AEFE8D0C95F7ACBC14FF17F3D3E8D58 (void);
// 0x00000685 System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_FinalSdkExceptionMessage(System.String)
extern void MonitoringAPICallEvent_set_FinalSdkExceptionMessage_mCE4CEBB9D3195B4FCA1C97F06FDE129BF4E038D7 (void);
// 0x00000686 System.String Amazon.Runtime.Internal.MonitoringAPICallEvent::get_FinalSdkException()
extern void MonitoringAPICallEvent_get_FinalSdkException_m4C0783F4BFD0AECB34A62A8BD9482D985B4692F1 (void);
// 0x00000687 System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_FinalSdkException(System.String)
extern void MonitoringAPICallEvent_set_FinalSdkException_m844468E3AC8FCEB66D06429B93F3CC1DBD348A95 (void);
// 0x00000688 System.String Amazon.Runtime.Internal.MonitoringAPICallEvent::get_FinalAWSException()
extern void MonitoringAPICallEvent_get_FinalAWSException_m915DD2A5206AEB58EF4B4C97BCC865DBBAD5AED4 (void);
// 0x00000689 System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_FinalAWSException(System.String)
extern void MonitoringAPICallEvent_set_FinalAWSException_mAE895618DF6F8E6795E9BA79F7494C613C037BDF (void);
// 0x0000068A System.String Amazon.Runtime.Internal.MonitoringAPICallEvent::get_FinalAWSExceptionMessage()
extern void MonitoringAPICallEvent_get_FinalAWSExceptionMessage_m57BF5E0DB609F04A09946FBC697EB8A73866CA08 (void);
// 0x0000068B System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_FinalAWSExceptionMessage(System.String)
extern void MonitoringAPICallEvent_set_FinalAWSExceptionMessage_m5F69148E6FA2848E9476EDC98F4C7F66F23B26D6 (void);
// 0x0000068C System.Nullable`1<System.Int32> Amazon.Runtime.Internal.MonitoringAPICallEvent::get_FinalHttpStatusCode()
extern void MonitoringAPICallEvent_get_FinalHttpStatusCode_m52DF47D94212EBE72D665682FBFBE07B726989D8 (void);
// 0x0000068D System.Void Amazon.Runtime.Internal.MonitoringAPICallEvent::set_FinalHttpStatusCode(System.Nullable`1<System.Int32>)
extern void MonitoringAPICallEvent_set_FinalHttpStatusCode_m1765EFAE3F3608A5C828F78249B1674D7829EF65 (void);
// 0x0000068E System.Collections.Generic.List`1<System.Byte> Amazon.Runtime.Internal.Util.CachingWrapperStream::get_AllReadBytes()
extern void CachingWrapperStream_get_AllReadBytes_m47F8162EE81AF5D64CC11404AC5F016C410A571A (void);
// 0x0000068F System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::set_AllReadBytes(System.Collections.Generic.List`1<System.Byte>)
extern void CachingWrapperStream_set_AllReadBytes_mF9E1FE6C9EAE04CB72D94F64A12E80D148792449 (void);
// 0x00000690 System.Collections.Generic.List`1<System.Byte> Amazon.Runtime.Internal.Util.CachingWrapperStream::get_LoggableReadBytes()
extern void CachingWrapperStream_get_LoggableReadBytes_m7FFB64D94C17B8D09F174F9E6AFC3EB878D6DE2F (void);
// 0x00000691 System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::.ctor(System.IO.Stream,System.Nullable`1<System.Int32>)
extern void CachingWrapperStream__ctor_mAD1FC9B4C218F2A742DC707500C8B458E44BBAD6 (void);
// 0x00000692 System.Int32 Amazon.Runtime.Internal.Util.CachingWrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern void CachingWrapperStream_Read_m7683473877085AC49C7B1E2E6BE92E6D177B84A6 (void);
// 0x00000693 System.Threading.Tasks.Task`1<System.Int32> Amazon.Runtime.Internal.Util.CachingWrapperStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void CachingWrapperStream_ReadAsync_mCCF843346B0BF947D6ED985A9D134B554BE74A95 (void);
// 0x00000694 System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::UpdateCacheAfterReading(System.Byte[],System.Int32,System.Int32)
extern void CachingWrapperStream_UpdateCacheAfterReading_m5685E68AEF5EB23AD6B0BCBF6A7B160281E239C2 (void);
// 0x00000695 System.Boolean Amazon.Runtime.Internal.Util.CachingWrapperStream::get_CanSeek()
extern void CachingWrapperStream_get_CanSeek_m0357E132F878E076172CF55A0D78BEACA035B966 (void);
// 0x00000696 System.Int64 Amazon.Runtime.Internal.Util.CachingWrapperStream::get_Position()
extern void CachingWrapperStream_get_Position_m4EB3A0F3E288BFFDF4120611D41632982C7C3604 (void);
// 0x00000697 System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream::set_Position(System.Int64)
extern void CachingWrapperStream_set_Position_mD595FEBC84AEEDB104F004409D838C63B62DA606 (void);
// 0x00000698 System.Int64 Amazon.Runtime.Internal.Util.CachingWrapperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void CachingWrapperStream_Seek_mF4D8D86DA75EEEA6123233236709A6F920E8A20D (void);
// 0x00000699 System.Threading.Tasks.Task`1<System.Int32> Amazon.Runtime.Internal.Util.CachingWrapperStream::<>n__0(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void CachingWrapperStream_U3CU3En__0_m732E60ABD6DCDDE8A24C7630D1DDB960A789B57D (void);
// 0x0000069A System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream/<ReadAsync>d__10::MoveNext()
extern void U3CReadAsyncU3Ed__10_MoveNext_m4EF4BA3B7A01EDDE2FAA1C6D06393F3CDAABF912 (void);
// 0x0000069B System.Void Amazon.Runtime.Internal.Util.CachingWrapperStream/<ReadAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CReadAsyncU3Ed__10_SetStateMachine_mFD24E47F2568DF602D75FEB1AD250C09AF1402F3 (void);
// 0x0000069C System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::.ctor(System.IO.Stream,System.Int32,Amazon.Runtime.Internal.Auth.AWS4SigningResult)
extern void ChunkedUploadWrapperStream__ctor_m1BDA47C949DB38586059DAE2D7C27594755FB234 (void);
// 0x0000069D System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ChunkedUploadWrapperStream_Read_mFA4DA6548E819FE540E89D8EA3D81E6F70677042 (void);
// 0x0000069E System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::AdjustBufferAfterReading(System.Byte[],System.Int32,System.Int32,System.Int32)
extern void ChunkedUploadWrapperStream_AdjustBufferAfterReading_mC77899CBD26862A0DEBB85284C575677B83971BD (void);
// 0x0000069F System.Threading.Tasks.Task`1<System.Int32> Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void ChunkedUploadWrapperStream_ReadAsync_m6A47C1B5537F49092C6FC4F3BEF907635D170E27 (void);
// 0x000006A0 System.Threading.Tasks.Task`1<System.Int32> Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::FillInputBufferAsync(System.Threading.CancellationToken)
extern void ChunkedUploadWrapperStream_FillInputBufferAsync_mE7F390F23DC7FB28A04E09403ADB3089687A2DB8 (void);
// 0x000006A1 Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_HeaderSigningResult()
extern void ChunkedUploadWrapperStream_get_HeaderSigningResult_m65E448AFAA7096A20053DAAB9072B2B487A1F1C8 (void);
// 0x000006A2 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::set_HeaderSigningResult(Amazon.Runtime.Internal.Auth.AWS4SigningResult)
extern void ChunkedUploadWrapperStream_set_HeaderSigningResult_m83F964155AFC9D296239E73EF94E1C3B8113B65A (void);
// 0x000006A3 System.String Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_PreviousChunkSignature()
extern void ChunkedUploadWrapperStream_get_PreviousChunkSignature_m92C109BB64841B864EDDBC37BB622F1A5CE4AF12 (void);
// 0x000006A4 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::set_PreviousChunkSignature(System.String)
extern void ChunkedUploadWrapperStream_set_PreviousChunkSignature_m352F26BF5C023E4CC80654ABE32CBA91012467A0 (void);
// 0x000006A5 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::ConstructOutputBufferChunk(System.Int32)
extern void ChunkedUploadWrapperStream_ConstructOutputBufferChunk_m79FF0ED9F1C4F3A3AEB0FA31911DA1591B2CAEE7 (void);
// 0x000006A6 System.Int64 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_Length()
extern void ChunkedUploadWrapperStream_get_Length_mE7D8FFCD8E3AE17D40A22C2381E32E85B2777C4E (void);
// 0x000006A7 System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_CanSeek()
extern void ChunkedUploadWrapperStream_get_CanSeek_mC190ECAD7D4DB3DA30348A9661C28CC15AFAF884 (void);
// 0x000006A8 System.Int64 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::ComputeChunkedContentLength(System.Int64)
extern void ChunkedUploadWrapperStream_ComputeChunkedContentLength_m8B4AF0121C8CD785ADBE826E7609209422AD6670 (void);
// 0x000006A9 System.Int64 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::CalculateChunkHeaderLength(System.Int64)
extern void ChunkedUploadWrapperStream_CalculateChunkHeaderLength_m76FC0EB716A1D558283CD3C174C8EA9BD890E310 (void);
// 0x000006AA System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::FillInputBuffer()
extern void ChunkedUploadWrapperStream_FillInputBuffer_m4BE56644BF84AC4F059298C69414EFA837355C4D (void);
// 0x000006AB System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::get_HasLength()
extern void ChunkedUploadWrapperStream_get_HasLength_m8672AAACA1194C82A706BA5D14CB36AFC1B0993D (void);
// 0x000006AC System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::.cctor()
extern void ChunkedUploadWrapperStream__cctor_mAF3558049BF700DCB3E38083865F0FB1816ECF83 (void);
// 0x000006AD System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<>c::.cctor()
extern void U3CU3Ec__cctor_m9A188949252BDA0A532BE23319623E576EB03379 (void);
// 0x000006AE System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<>c::.ctor()
extern void U3CU3Ec__ctor_mA0066995D40AD9ABDC941DEC139DBF7AD622737A (void);
// 0x000006AF System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<>c::<.ctor>b__14_0(System.IO.Stream)
extern void U3CU3Ec_U3C_ctorU3Eb__14_0_mB73A460CA0E6EAF24674AD10169F55E96ECE283C (void);
// 0x000006B0 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<ReadAsync>d__17::MoveNext()
extern void U3CReadAsyncU3Ed__17_MoveNext_mE30629A8299DF67D7AA06C25BD9F50149D44259E (void);
// 0x000006B1 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<ReadAsync>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CReadAsyncU3Ed__17_SetStateMachine_mAEBAEDD593301A03FA75541227C724FE80BE9FEB (void);
// 0x000006B2 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<FillInputBufferAsync>d__18::MoveNext()
extern void U3CFillInputBufferAsyncU3Ed__18_MoveNext_m8DD329E339F709CACD5CE1C40669C0663D0F7DF0 (void);
// 0x000006B3 System.Void Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/<FillInputBufferAsync>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFillInputBufferAsyncU3Ed__18_SetStateMachine_mD3F93DDF86B76F1C0F6546C305314C798C2255EC (void);
// 0x000006B4 System.Void Amazon.Runtime.Internal.Util.AlwaysSendList`1::.ctor()
// 0x000006B5 System.Void Amazon.Runtime.Internal.Util.AlwaysSendDictionary`2::.ctor()
// 0x000006B6 System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::set_IsRunning(System.Boolean)
// 0x000006B7 System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::.ctor(System.Action`1<T>)
// 0x000006B8 System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::Finalize()
// 0x000006B9 System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::Dispose(System.Boolean)
// 0x000006BA System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::Dispose()
// 0x000006BB System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::Dispatch(T)
// 0x000006BC System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::Stop()
// 0x000006BD System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::Run()
// 0x000006BE System.Void Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::HandleInvoked()
// 0x000006BF System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker::.ctor()
extern void BackgroundInvoker__ctor_mFF8522C5BA8772D6664618D3AE810FF665B9E85C (void);
// 0x000006C0 System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::.cctor()
extern void U3CU3Ec__cctor_mACA4DF93CAF6BBDE9F02BADBE15DB6C751FA651E (void);
// 0x000006C1 System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::.ctor()
extern void U3CU3Ec__ctor_m61E6BDB336902817A9D75F7B75226CDD9D1FF736 (void);
// 0x000006C2 System.Void Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::<.ctor>b__0_0(System.Action)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_m6FFA8B5369BFF78610EA050DDD601BB5E2388F65 (void);
// 0x000006C3 System.Void Amazon.Runtime.Internal.Util.EventStream::add_OnRead(Amazon.Runtime.Internal.Util.EventStream/ReadProgress)
extern void EventStream_add_OnRead_m0DE98B157EC382ADADEF709A3151FC5EEAF22B64 (void);
// 0x000006C4 System.Void Amazon.Runtime.Internal.Util.EventStream::remove_OnRead(Amazon.Runtime.Internal.Util.EventStream/ReadProgress)
extern void EventStream_remove_OnRead_mEAA2C6160B35C2510258E99BBBD8FFA3E4C2934F (void);
// 0x000006C5 System.Void Amazon.Runtime.Internal.Util.EventStream::.ctor(System.IO.Stream,System.Boolean)
extern void EventStream__ctor_mB410CB9A85D534E4ACDA49445802BFFC7A6BD2C8 (void);
// 0x000006C6 System.Void Amazon.Runtime.Internal.Util.EventStream::Dispose(System.Boolean)
extern void EventStream_Dispose_m8555ACC56D3503B628903F9530BAB4F81F9688B4 (void);
// 0x000006C7 System.Boolean Amazon.Runtime.Internal.Util.EventStream::get_CanRead()
extern void EventStream_get_CanRead_m11FC188A2A0EE00F58F72D6CC9EF8F7BDCAC6AF0 (void);
// 0x000006C8 System.Boolean Amazon.Runtime.Internal.Util.EventStream::get_CanSeek()
extern void EventStream_get_CanSeek_m8B4F82A12935EC18C98F490A8A74C5BB9CF27C19 (void);
// 0x000006C9 System.Boolean Amazon.Runtime.Internal.Util.EventStream::get_CanWrite()
extern void EventStream_get_CanWrite_m2F933EA124DB953D5A65CB5769E606011D6352E0 (void);
// 0x000006CA System.Int64 Amazon.Runtime.Internal.Util.EventStream::get_Length()
extern void EventStream_get_Length_m6B3FF4607A107DCEBAF210E0EC15F5497923F8D2 (void);
// 0x000006CB System.Int64 Amazon.Runtime.Internal.Util.EventStream::get_Position()
extern void EventStream_get_Position_m5776C1F89D1FEE3A26A7F6FA7511F4DC42808173 (void);
// 0x000006CC System.Void Amazon.Runtime.Internal.Util.EventStream::set_Position(System.Int64)
extern void EventStream_set_Position_m684B422FC6DC5789C6CBE008B0E115AEE40C4BEF (void);
// 0x000006CD System.Int32 Amazon.Runtime.Internal.Util.EventStream::get_ReadTimeout()
extern void EventStream_get_ReadTimeout_mF64F0D1A1DEEEC5711F4FE83B802AE7FBBCF5896 (void);
// 0x000006CE System.Void Amazon.Runtime.Internal.Util.EventStream::set_ReadTimeout(System.Int32)
extern void EventStream_set_ReadTimeout_m9671CF92959B3672B5CA4C5D0578B6C6C41146E6 (void);
// 0x000006CF System.Int32 Amazon.Runtime.Internal.Util.EventStream::get_WriteTimeout()
extern void EventStream_get_WriteTimeout_m5ACAEA1B96ED57C6C613A431DBF5B8BF143C62FA (void);
// 0x000006D0 System.Void Amazon.Runtime.Internal.Util.EventStream::Flush()
extern void EventStream_Flush_m7646F0C57543D11A0EC8152BDC57FDD60193043E (void);
// 0x000006D1 System.Int32 Amazon.Runtime.Internal.Util.EventStream::Read(System.Byte[],System.Int32,System.Int32)
extern void EventStream_Read_mC60BA92A9CA5CBDF1689B61610D0FFDDCBC01010 (void);
// 0x000006D2 System.Int64 Amazon.Runtime.Internal.Util.EventStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void EventStream_Seek_mCE12477EBD558597058515CDB4EA0F6BF40B0137 (void);
// 0x000006D3 System.Void Amazon.Runtime.Internal.Util.EventStream::Write(System.Byte[],System.Int32,System.Int32)
extern void EventStream_Write_mE388A7EFC620210C772072AC5C7520C7CDE3DDA7 (void);
// 0x000006D4 System.Void Amazon.Runtime.Internal.Util.EventStream::WriteByte(System.Byte)
extern void EventStream_WriteByte_m5319C32F067EBAC9DA767CA40CC8AE9949B2BB5B (void);
// 0x000006D5 System.Threading.Tasks.Task`1<System.Int32> Amazon.Runtime.Internal.Util.EventStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void EventStream_ReadAsync_mA6ECBA89A86A4B35D07528B73E5E2C5181996E37 (void);
// 0x000006D6 System.Threading.Tasks.Task Amazon.Runtime.Internal.Util.EventStream::WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void EventStream_WriteAsync_mB399FEA07A6AFF74FF1409E5BD0A3454B5E703FB (void);
// 0x000006D7 System.Void Amazon.Runtime.Internal.Util.EventStream/ReadProgress::.ctor(System.Object,System.IntPtr)
extern void ReadProgress__ctor_m03F90935CA2AD55754107277D6C16952D1B3DA48 (void);
// 0x000006D8 System.Void Amazon.Runtime.Internal.Util.EventStream/ReadProgress::Invoke(System.Int32)
extern void ReadProgress_Invoke_m8BEC70D8C832F5DD3F56B56CDE4A0078AB977CB6 (void);
// 0x000006D9 System.IAsyncResult Amazon.Runtime.Internal.Util.EventStream/ReadProgress::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void ReadProgress_BeginInvoke_m2932837D197A4B1987F5DF94436C7501F645BCA5 (void);
// 0x000006DA System.Void Amazon.Runtime.Internal.Util.EventStream/ReadProgress::EndInvoke(System.IAsyncResult)
extern void ReadProgress_EndInvoke_m434FD006A39EE4EEA06B98A845A53B0DC643A816 (void);
// 0x000006DB System.Void Amazon.Runtime.Internal.Util.EventStream/<ReadAsync>d__33::MoveNext()
extern void U3CReadAsyncU3Ed__33_MoveNext_mBC419EA13A9997E3E0E65F36F59433A8936F32B7 (void);
// 0x000006DC System.Void Amazon.Runtime.Internal.Util.EventStream/<ReadAsync>d__33::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CReadAsyncU3Ed__33_SetStateMachine_m93DC06F8C05B23E12F7E832FF063A3D652DDA55E (void);
// 0x000006DD System.Int64 Amazon.Runtime.Internal.Util.Extensions::GetElapsedDateTimeTicks(System.Diagnostics.Stopwatch)
extern void Extensions_GetElapsedDateTimeTicks_mFC9EB180A1895BD3CFA90ADDC1BA0E8EBB37A3B0 (void);
// 0x000006DE System.Boolean Amazon.Runtime.Internal.Util.Extensions::HasRequestData(Amazon.Runtime.Internal.IRequest)
extern void Extensions_HasRequestData_m89B15A85A93DE8851680363F7237A1BAF07F6700 (void);
// 0x000006DF System.Void Amazon.Runtime.Internal.Util.Extensions::.cctor()
extern void Extensions__cctor_m517EE48A13FF83498C691420BEDBE77B80E4803C (void);
// 0x000006E0 System.Boolean Amazon.Runtime.Internal.Util.GuidUtils::TryParseNullableGuid(System.String,System.Nullable`1<System.Guid>&)
extern void GuidUtils_TryParseNullableGuid_m1DC08B14DB69D1FDA63C794B038A3DB531046C5C (void);
// 0x000006E1 System.Boolean Amazon.Runtime.Internal.Util.GuidUtils::TryParseGuid(System.String,System.Guid&)
extern void GuidUtils_TryParseGuid_mF4789C834B8D756A58E2F3EDE6CBFD0D7A225117 (void);
// 0x000006E2 System.Int32 Amazon.Runtime.Internal.Util.Hashing::Hash(System.Object[])
extern void Hashing_Hash_m70FC42FA944667BD6E608BD6040E3695B3B0F1E0 (void);
// 0x000006E3 System.Int32 Amazon.Runtime.Internal.Util.Hashing::CombineHashes(System.Int32[])
extern void Hashing_CombineHashes_m4098B2EDCAA2187D45472404BB15B32364853310 (void);
// 0x000006E4 System.Int32 Amazon.Runtime.Internal.Util.Hashing::CombineHashesInternal(System.Int32,System.Int32)
extern void Hashing_CombineHashesInternal_m08307CB2102F2B02F59A64DFCEC9F6DA3C3C0448 (void);
// 0x000006E5 System.Void Amazon.Runtime.Internal.Util.Hashing/<>c::.cctor()
extern void U3CU3Ec__cctor_m70D8CEBCDA251FD223D213CEEBF2D7BE1F281696 (void);
// 0x000006E6 System.Void Amazon.Runtime.Internal.Util.Hashing/<>c::.ctor()
extern void U3CU3Ec__ctor_mD9C252C8649C45D02E4DB986B10EBC974FC4AE48 (void);
// 0x000006E7 System.Int32 Amazon.Runtime.Internal.Util.Hashing/<>c::<Hash>b__0_0(System.Object)
extern void U3CU3Ec_U3CHashU3Eb__0_0_m7182EC3EFB00276A44E1D067B636972423269A53 (void);
// 0x000006E8 Amazon.Runtime.Internal.Util.IHashingWrapper Amazon.Runtime.Internal.Util.HashStream::get_Algorithm()
extern void HashStream_get_Algorithm_m77777CC969D213FAB4ABCBBA87FF03AB65FB9647 (void);
// 0x000006E9 System.Void Amazon.Runtime.Internal.Util.HashStream::set_CurrentPosition(System.Int64)
extern void HashStream_set_CurrentPosition_mAC27EEE93E512BAB4450D2ED614850362DAF3F9A (void);
// 0x000006EA System.Void Amazon.Runtime.Internal.Util.HashStream::set_CalculatedHash(System.Byte[])
extern void HashStream_set_CalculatedHash_m78F1745C53826D49A4888F317B65495C587FBD72 (void);
// 0x000006EB System.Void Amazon.Runtime.Internal.Util.HashStream::Reset()
extern void HashStream_Reset_m0A7A55F7AD350FC5A3E6DDEA7CE456B5F9183AE8 (void);
// 0x000006EC System.Void Amazon.Runtime.Internal.Util.IHashingWrapper::Clear()
// 0x000006ED System.Void Amazon.Runtime.Internal.Util.ILogger::InfoFormat(System.String,System.Object[])
// 0x000006EE System.Void Amazon.Runtime.Internal.Util.ILogger::Debug(System.Exception,System.String,System.Object[])
// 0x000006EF System.Void Amazon.Runtime.Internal.Util.ILogger::DebugFormat(System.String,System.Object[])
// 0x000006F0 System.Void Amazon.Runtime.Internal.Util.ILogger::Error(System.Exception,System.String,System.Object[])
// 0x000006F1 System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::.ctor(System.Type)
extern void InternalConsoleLogger__ctor_m82F71F6995A5D7518CEEF49C3F416F34B7566528 (void);
// 0x000006F2 System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::Error(System.Exception,System.String,System.Object[])
extern void InternalConsoleLogger_Error_m8923FC249477CC668869717EBB99806926E41235 (void);
// 0x000006F3 System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::Debug(System.Exception,System.String,System.Object[])
extern void InternalConsoleLogger_Debug_m8E6ABC0E7BF0C1A2A801E722EE546EDEBF694431 (void);
// 0x000006F4 System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::DebugFormat(System.String,System.Object[])
extern void InternalConsoleLogger_DebugFormat_mF95C2856A41926ADB444CCC2C51E5B093D253C29 (void);
// 0x000006F5 System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::InfoFormat(System.String,System.Object[])
extern void InternalConsoleLogger_InfoFormat_mFECE6C230B64353D25358155EEB0088B48A3F867 (void);
// 0x000006F6 System.Void Amazon.Runtime.Internal.Util.InternalConsoleLogger::Log(Amazon.Runtime.Internal.Util.InternalConsoleLogger/LogLevel,System.String,System.Exception)
extern void InternalConsoleLogger_Log_mC56668F7CA67A8BE35FF8D02BF9D673CB996BEF0 (void);
// 0x000006F7 System.Void Amazon.Runtime.Internal.Util.Logger::.ctor()
extern void Logger__ctor_m226C438F69FA7897F419F12BF02433E1F30A7CC9 (void);
// 0x000006F8 System.Void Amazon.Runtime.Internal.Util.Logger::.ctor(System.Type)
extern void Logger__ctor_m7C70EF7BF01939E4B459459527940F414CD92B21 (void);
// 0x000006F9 System.Void Amazon.Runtime.Internal.Util.Logger::ConfigsChanged(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern void Logger_ConfigsChanged_mB9CD24DFA4900D25B4EF825EA157F2D7EADD7CDD (void);
// 0x000006FA System.Void Amazon.Runtime.Internal.Util.Logger::ConfigureLoggers()
extern void Logger_ConfigureLoggers_m83A77BF4E5B3E832802B745B28F60A92571C9830 (void);
// 0x000006FB Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.Logger::GetLogger(System.Type)
extern void Logger_GetLogger_m18C612C6586A3F343F5CC865041C82A257CDFF1A (void);
// 0x000006FC Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.Logger::get_EmptyLogger()
extern void Logger_get_EmptyLogger_m5CC262E9F11F49DA598A22FC8A05CEF26C3ED9C5 (void);
// 0x000006FD System.Void Amazon.Runtime.Internal.Util.Logger::Error(System.Exception,System.String,System.Object[])
extern void Logger_Error_mD839C4FC38C8C01EF7836C7632549115BEA394BA (void);
// 0x000006FE System.Void Amazon.Runtime.Internal.Util.Logger::Debug(System.Exception,System.String,System.Object[])
extern void Logger_Debug_mD756AEAD6298C36D3C76F94FC478D15E3FE965B1 (void);
// 0x000006FF System.Void Amazon.Runtime.Internal.Util.Logger::DebugFormat(System.String,System.Object[])
extern void Logger_DebugFormat_m525735E1788943E3DE772B42C9A64BAF1C32307B (void);
// 0x00000700 System.Void Amazon.Runtime.Internal.Util.Logger::InfoFormat(System.String,System.Object[])
extern void Logger_InfoFormat_m834B8C45A15D6C18D10E8DA641C376B1A96B7AE0 (void);
// 0x00000701 System.Void Amazon.Runtime.Internal.Util.Logger::.cctor()
extern void Logger__cctor_mDF4AC77A926156E5E8F1C4BDECCF54B5B4CA722F (void);
// 0x00000702 System.Type Amazon.Runtime.Internal.Util.InternalLogger::get_DeclaringType()
extern void InternalLogger_get_DeclaringType_mF8C58DF13E2E9F52A6C1C28533698C7FC23549A3 (void);
// 0x00000703 System.Void Amazon.Runtime.Internal.Util.InternalLogger::set_DeclaringType(System.Type)
extern void InternalLogger_set_DeclaringType_mB5E079C1381CB75406DC0A2B0C3F679FE6892E50 (void);
// 0x00000704 System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsEnabled()
extern void InternalLogger_get_IsEnabled_mC05569861EF1FD59E173C1CC3D538015554CE830 (void);
// 0x00000705 System.Void Amazon.Runtime.Internal.Util.InternalLogger::set_IsEnabled(System.Boolean)
extern void InternalLogger_set_IsEnabled_m60444B552DE4B9925465942D0E3155D5839B5316 (void);
// 0x00000706 System.Void Amazon.Runtime.Internal.Util.InternalLogger::.ctor(System.Type)
extern void InternalLogger__ctor_m9334AF9E260F1A402BFA66EA348462830BF63655 (void);
// 0x00000707 System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsErrorEnabled()
extern void InternalLogger_get_IsErrorEnabled_m507CFD61D6D6477E52DE59EAC7D86392EFFE1FDA (void);
// 0x00000708 System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsDebugEnabled()
extern void InternalLogger_get_IsDebugEnabled_m3967843696A1A6750202CB9B43A4245E605477AD (void);
// 0x00000709 System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::get_IsInfoEnabled()
extern void InternalLogger_get_IsInfoEnabled_m3534FB989D4DA4C6AB76BDDBF6D5924F45184FD7 (void);
// 0x0000070A System.Void Amazon.Runtime.Internal.Util.InternalLogger::Error(System.Exception,System.String,System.Object[])
// 0x0000070B System.Void Amazon.Runtime.Internal.Util.InternalLogger::Debug(System.Exception,System.String,System.Object[])
// 0x0000070C System.Void Amazon.Runtime.Internal.Util.InternalLogger::DebugFormat(System.String,System.Object[])
// 0x0000070D System.Void Amazon.Runtime.Internal.Util.InternalLogger::InfoFormat(System.String,System.Object[])
// 0x0000070E System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::loadStatics()
extern void InternalLog4netLogger_loadStatics_m1DB77FE774C935E2A2C298E39C30EF7E866B3822 (void);
// 0x0000070F System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::.ctor(System.Type)
extern void InternalLog4netLogger__ctor_mF9BBAAEC7E937622CE2761FD5714B1D7CA768E61 (void);
// 0x00000710 System.Boolean Amazon.Runtime.Internal.Util.InternalLog4netLogger::get_IsErrorEnabled()
extern void InternalLog4netLogger_get_IsErrorEnabled_m399891FE9A026ADF91B015BC5D7C4AF122AB2D68 (void);
// 0x00000711 System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::Error(System.Exception,System.String,System.Object[])
extern void InternalLog4netLogger_Error_m8C3018BBD99EA7D20D9D71034E5424DB9C0DAA57 (void);
// 0x00000712 System.Boolean Amazon.Runtime.Internal.Util.InternalLog4netLogger::get_IsDebugEnabled()
extern void InternalLog4netLogger_get_IsDebugEnabled_m1E304BB61279F29D4AAEEDF9C3DA33C8BE2C1314 (void);
// 0x00000713 System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::Debug(System.Exception,System.String,System.Object[])
extern void InternalLog4netLogger_Debug_mFDBE196995454C86FCC9F1D7F760A2B6D9DB56F3 (void);
// 0x00000714 System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::DebugFormat(System.String,System.Object[])
extern void InternalLog4netLogger_DebugFormat_m7D57E2EC78D5CF1F90534253B2CA280E18BD92EF (void);
// 0x00000715 System.Boolean Amazon.Runtime.Internal.Util.InternalLog4netLogger::get_IsInfoEnabled()
extern void InternalLog4netLogger_get_IsInfoEnabled_mA018A5BF4E6B43006F06EBB7BEE246B861292235 (void);
// 0x00000716 System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::InfoFormat(System.String,System.Object[])
extern void InternalLog4netLogger_InfoFormat_m4C7FB0FBD2A2E2E5553A5F3242BFCE901ED1E5FC (void);
// 0x00000717 System.Void Amazon.Runtime.Internal.Util.InternalLog4netLogger::.cctor()
extern void InternalLog4netLogger__cctor_mC290C95CFD5F684D5FCEE19DFF399B74C2566B02 (void);
// 0x00000718 System.Object[] Amazon.Runtime.Internal.Util.LogMessage::get_Args()
extern void LogMessage_get_Args_m64559F784FBCEF8CA2E60E453E9E2D284D4924CE (void);
// 0x00000719 System.Void Amazon.Runtime.Internal.Util.LogMessage::set_Args(System.Object[])
extern void LogMessage_set_Args_m57166E78E7484C714F1CF706EE6ADED27C3430A2 (void);
// 0x0000071A System.IFormatProvider Amazon.Runtime.Internal.Util.LogMessage::get_Provider()
extern void LogMessage_get_Provider_m3A902C604D423FBCA0E6BAAEF89002A0D443B8BC (void);
// 0x0000071B System.Void Amazon.Runtime.Internal.Util.LogMessage::set_Provider(System.IFormatProvider)
extern void LogMessage_set_Provider_m44E3AF9A95855B86FEC00453D9B1BEEF4AEB12B8 (void);
// 0x0000071C System.String Amazon.Runtime.Internal.Util.LogMessage::get_Format()
extern void LogMessage_get_Format_m60859CBB4FAE49145535300C94EE5B55598D2DF4 (void);
// 0x0000071D System.Void Amazon.Runtime.Internal.Util.LogMessage::set_Format(System.String)
extern void LogMessage_set_Format_m72DEAFAF7695EEB50DA0A52565923C01F4068ABD (void);
// 0x0000071E System.Void Amazon.Runtime.Internal.Util.LogMessage::.ctor(System.IFormatProvider,System.String,System.Object[])
extern void LogMessage__ctor_mC9E82515541AEA85C7ACFE35081A77EC6E94657F (void);
// 0x0000071F System.String Amazon.Runtime.Internal.Util.LogMessage::ToString()
extern void LogMessage_ToString_m1312E43025D02AB8AED0262CABBF6EE5D1936A45 (void);
// 0x00000720 System.Void Amazon.Runtime.Internal.Util.LruCache`2::set_MaxEntries(System.Int32)
// 0x00000721 System.Void Amazon.Runtime.Internal.Util.LruCache`2::.ctor(System.Int32)
// 0x00000722 System.Void Amazon.Runtime.Internal.Util.LruList`2::.ctor()
// 0x00000723 System.Int64 Amazon.Runtime.Internal.Util.RequestMetrics::get_CurrentTime()
extern void RequestMetrics_get_CurrentTime_mED969D865585AEC8F03C5F700593D6B0C2DF4243 (void);
// 0x00000724 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::LogError_Locked(Amazon.Runtime.Metric,System.String,System.Object[])
extern void RequestMetrics_LogError_Locked_m233C41DA154865B536D979BC9FDC3C748576CF84 (void);
// 0x00000725 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::Log(System.Text.StringBuilder,Amazon.Runtime.Metric,System.Object)
extern void RequestMetrics_Log_mABEC2813301E3904E01BA6FC7D68798BC6B0E62C (void);
// 0x00000726 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::Log(System.Text.StringBuilder,Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>)
extern void RequestMetrics_Log_mEB8D48F09DE24292EF7A74F636EA985091FE0623 (void);
// 0x00000727 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::LogHelper(System.Text.StringBuilder,Amazon.Runtime.Metric,System.Object[])
extern void RequestMetrics_LogHelper_m06E3E60039285253B932C73671888A65B1C89D4C (void);
// 0x00000728 System.String Amazon.Runtime.Internal.Util.RequestMetrics::ObjectToString(System.Object)
extern void RequestMetrics_ObjectToString_mFF490B51A276548C1AFF965D6A55F614FE66A639 (void);
// 0x00000729 System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>> Amazon.Runtime.Internal.Util.RequestMetrics::get_Properties()
extern void RequestMetrics_get_Properties_m84BC22CAA8541BABE6FE73999D148C1ED76DBBF0 (void);
// 0x0000072A System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_Properties(System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>)
extern void RequestMetrics_set_Properties_m3C55799F13A923439566FF92885EB32CE2EC716F (void);
// 0x0000072B System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>> Amazon.Runtime.Internal.Util.RequestMetrics::get_Timings()
extern void RequestMetrics_get_Timings_m16064050B633CBE0E591C154A132ABF4B0A28E26 (void);
// 0x0000072C System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_Timings(System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>)
extern void RequestMetrics_set_Timings_mEF536D7BA4D9F52DA8869B9937C519F356B9DF67 (void);
// 0x0000072D System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64> Amazon.Runtime.Internal.Util.RequestMetrics::get_Counters()
extern void RequestMetrics_get_Counters_m8A0BDC41BE680C0E804B86D01693558CFE0E5894 (void);
// 0x0000072E System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_Counters(System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>)
extern void RequestMetrics_set_Counters_m04B53A30F43131DDBD3978F730C6ECD02257749D (void);
// 0x0000072F System.Boolean Amazon.Runtime.Internal.Util.RequestMetrics::get_IsEnabled()
extern void RequestMetrics_get_IsEnabled_m1C66114D0BC5B5FADE7FA6A59F0DE19776471245 (void);
// 0x00000730 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::set_IsEnabled(System.Boolean)
extern void RequestMetrics_set_IsEnabled_m8F6AECA196BF20FB649C8AB8EA0A0BF3E7186E1E (void);
// 0x00000731 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::.ctor()
extern void RequestMetrics__ctor_m4DEA6AEFE678C87D18813DDF4E125663E8C26E6F (void);
// 0x00000732 Amazon.Runtime.Internal.Util.TimingEvent Amazon.Runtime.Internal.Util.RequestMetrics::StartEvent(Amazon.Runtime.Metric)
extern void RequestMetrics_StartEvent_m271F4D4E110290B50567218EA4D1AB9500786831 (void);
// 0x00000733 Amazon.Runtime.Internal.Util.Timing Amazon.Runtime.Internal.Util.RequestMetrics::StopEvent(Amazon.Runtime.Metric)
extern void RequestMetrics_StopEvent_m65411582B8651E4E69AB199C5AF1D27952F468BF (void);
// 0x00000734 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::AddProperty(Amazon.Runtime.Metric,System.Object)
extern void RequestMetrics_AddProperty_m54036FB59C030059BD4A888ABC19C8699B9944E9 (void);
// 0x00000735 System.Void Amazon.Runtime.Internal.Util.RequestMetrics::SetCounter(Amazon.Runtime.Metric,System.Int64)
extern void RequestMetrics_SetCounter_mA6AB2F144BA0CDD66B940EA8EECB787EC9676392 (void);
// 0x00000736 System.String Amazon.Runtime.Internal.Util.RequestMetrics::GetErrors()
extern void RequestMetrics_GetErrors_m183C7743E86E8238695C0F749F9A50EFDF1A02ED (void);
// 0x00000737 System.String Amazon.Runtime.Internal.Util.RequestMetrics::ToString()
extern void RequestMetrics_ToString_m5EB67C37EA7A40C4C3DAA372D25617E7E5C2F6F6 (void);
// 0x00000738 System.String Amazon.Runtime.Internal.Util.RequestMetrics::ToJSON()
extern void RequestMetrics_ToJSON_m4B74D16EEAE412F5675C245ABF52FC3257B8EE3D (void);
// 0x00000739 System.Void Amazon.Runtime.Internal.Util.RequestMetrics/<>c::.cctor()
extern void U3CU3Ec__cctor_m45DBEC4240710FBF9DDD897040EB3A133982B7FA (void);
// 0x0000073A System.Void Amazon.Runtime.Internal.Util.RequestMetrics/<>c::.ctor()
extern void U3CU3Ec__ctor_m64582B1B1D489F27A10EC6F7E68D4B133376A0BE (void);
// 0x0000073B System.String Amazon.Runtime.Internal.Util.RequestMetrics/<>c::<GetErrors>b__33_0(Amazon.Runtime.Metric)
extern void U3CU3Ec_U3CGetErrorsU3Eb__33_0_mDA926F44664C61BD942C1B6E69E63DF9A9A6658F (void);
// 0x0000073C System.Void Amazon.Runtime.Internal.Util.Timing::.ctor()
extern void Timing__ctor_mDDB669A2B54B25B230401E85029CB7001E55DED6 (void);
// 0x0000073D System.Void Amazon.Runtime.Internal.Util.Timing::.ctor(System.Int64)
extern void Timing__ctor_mA2C4F0FBEDB3880C141663843F1869F870B1C37E (void);
// 0x0000073E System.Void Amazon.Runtime.Internal.Util.Timing::Stop(System.Int64)
extern void Timing_Stop_mD83878F0DD74A58043E146232FE859ADAD135517 (void);
// 0x0000073F System.Boolean Amazon.Runtime.Internal.Util.Timing::get_IsFinished()
extern void Timing_get_IsFinished_mEB880282891D5893958B9981CDFF58ECF2BB304E (void);
// 0x00000740 System.Void Amazon.Runtime.Internal.Util.Timing::set_IsFinished(System.Boolean)
extern void Timing_set_IsFinished_mDB14D3D301BF0DAAFC799C58EE20E6838623A380 (void);
// 0x00000741 System.Int64 Amazon.Runtime.Internal.Util.Timing::get_ElapsedTicks()
extern void Timing_get_ElapsedTicks_mEA31A7D897E6641EE65E4B691686803BFAEBC0C2 (void);
// 0x00000742 System.TimeSpan Amazon.Runtime.Internal.Util.Timing::get_ElapsedTime()
extern void Timing_get_ElapsedTime_m26E1E7D21D9F519C1F2937DFCAEE11F24F1F6B19 (void);
// 0x00000743 System.Void Amazon.Runtime.Internal.Util.TimingEvent::.ctor(Amazon.Runtime.Internal.Util.RequestMetrics,Amazon.Runtime.Metric)
extern void TimingEvent__ctor_m5B0807C9E7E967910255FE1AAB69EC0160A6EDA3 (void);
// 0x00000744 System.Void Amazon.Runtime.Internal.Util.TimingEvent::Dispose(System.Boolean)
extern void TimingEvent_Dispose_m6E6298FDA146F8F908330A192613522208093CF8 (void);
// 0x00000745 System.Void Amazon.Runtime.Internal.Util.TimingEvent::Dispose()
extern void TimingEvent_Dispose_mE1ED663542AB69392A35BE754137AC2BE80458E5 (void);
// 0x00000746 System.Void Amazon.Runtime.Internal.Util.TimingEvent::Finalize()
extern void TimingEvent_Finalize_mDC7FEF9C68BACAD9C3F1057A21E1AFBCFA1BEE6F (void);
// 0x00000747 Amazon.Runtime.Metric Amazon.Runtime.Internal.Util.MetricError::get_Metric()
extern void MetricError_get_Metric_m0EFDCF995545075980F8C80E5597CEB829AADF6C (void);
// 0x00000748 System.Void Amazon.Runtime.Internal.Util.MetricError::set_Metric(Amazon.Runtime.Metric)
extern void MetricError_set_Metric_m520EB610297922EE7EAB30E41E3C0F6E6EA091FA (void);
// 0x00000749 System.String Amazon.Runtime.Internal.Util.MetricError::get_Message()
extern void MetricError_get_Message_m0CB9DC2D53A93DA95B07DD455CDC0FB60CD2C05F (void);
// 0x0000074A System.Void Amazon.Runtime.Internal.Util.MetricError::set_Message(System.String)
extern void MetricError_set_Message_m0541CC94D5A9606C37C1485C32A0C39133A5FE3E (void);
// 0x0000074B System.Exception Amazon.Runtime.Internal.Util.MetricError::get_Exception()
extern void MetricError_get_Exception_m6FC385BBE21A845875D34A44D1DC6B265419B139 (void);
// 0x0000074C System.Void Amazon.Runtime.Internal.Util.MetricError::set_Exception(System.Exception)
extern void MetricError_set_Exception_mB492556F5CDD058FF0A84D4FEEF2EDD032A6FF55 (void);
// 0x0000074D System.DateTime Amazon.Runtime.Internal.Util.MetricError::get_Time()
extern void MetricError_get_Time_m287005246E7A6BED191E05ADB098B6BE5856F39C (void);
// 0x0000074E System.Void Amazon.Runtime.Internal.Util.MetricError::set_Time(System.DateTime)
extern void MetricError_set_Time_m0FD2F80D3A72BC1D80EBA419A23833BF268747C4 (void);
// 0x0000074F System.Void Amazon.Runtime.Internal.Util.MetricError::.ctor(Amazon.Runtime.Metric,System.String,System.Object[])
extern void MetricError__ctor_mEF8FCBFD3DD508E52D6BE9798AC603FDBEF7F58F (void);
// 0x00000750 System.Void Amazon.Runtime.Internal.Util.MetricError::.ctor(Amazon.Runtime.Metric,System.Exception,System.String,System.Object[])
extern void MetricError__ctor_m55C1C6FEF81AF627136BB5489BC1BA441246FE36 (void);
// 0x00000751 System.Void Amazon.Runtime.Internal.Util.NonDisposingWrapperStream::.ctor(System.IO.Stream)
extern void NonDisposingWrapperStream__ctor_mC2D061B812BAB078AD604D63D575E1A764584744 (void);
// 0x00000752 System.Void Amazon.Runtime.Internal.Util.NonDisposingWrapperStream::Dispose(System.Boolean)
extern void NonDisposingWrapperStream_Dispose_m58313ED55C0895044623DCBB212B952F015A4336 (void);
// 0x00000753 System.Boolean Amazon.Runtime.Internal.Util.S3Uri::IsS3Uri(System.Uri)
extern void S3Uri_IsS3Uri_m1B7427D0B8B49DB624CC96478D6CEDC4C69972BF (void);
// 0x00000754 System.Void Amazon.Runtime.Internal.Util.S3Uri::.cctor()
extern void S3Uri__cctor_m1A48D34B9D68760EF17A34A4F95B19026FD34550 (void);
// 0x00000755 System.String Amazon.Runtime.Internal.Util.StringUtils::FromString(System.String)
extern void StringUtils_FromString_m3EEFED8018E3DD19A48794362230C078FDE9CE35 (void);
// 0x00000756 System.String Amazon.Runtime.Internal.Util.StringUtils::FromMemoryStream(System.IO.MemoryStream)
extern void StringUtils_FromMemoryStream_m8390D1D01AD7AC326C5B09E6E0D3116A28930BED (void);
// 0x00000757 System.String Amazon.Runtime.Internal.Util.StringUtils::FromInt(System.Int32)
extern void StringUtils_FromInt_m140C1228F73D061A604B3BC22F26E0E00A1B6A29 (void);
// 0x00000758 System.Void Amazon.Runtime.Internal.Util.StringUtils::.cctor()
extern void StringUtils__cctor_mB156AA15CF8BBF3FC07E3CF4A067FF5AD91A0FC1 (void);
// 0x00000759 System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::get_BaseStream()
extern void WrapperStream_get_BaseStream_mFEB26EFB3BC758284D7CF3F725BD9E865E3D086A (void);
// 0x0000075A System.Void Amazon.Runtime.Internal.Util.WrapperStream::set_BaseStream(System.IO.Stream)
extern void WrapperStream_set_BaseStream_mD8B7FFBEA1AFC67B0B69D0AD97F28771F6767DFE (void);
// 0x0000075B System.Void Amazon.Runtime.Internal.Util.WrapperStream::.ctor(System.IO.Stream)
extern void WrapperStream__ctor_m5AE4B4945227374B6961AC423E7541D9C3A0AD42 (void);
// 0x0000075C System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::GetNonWrapperBaseStream()
extern void WrapperStream_GetNonWrapperBaseStream_mABA5E7B57D9FE5F69CCA894501506ECFFA9B04D8 (void);
// 0x0000075D System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::GetSeekableBaseStream()
extern void WrapperStream_GetSeekableBaseStream_m121E5CED8444CBDD0CA27B3C0BE2DE38A7C59A71 (void);
// 0x0000075E System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::GetNonWrapperBaseStream(System.IO.Stream)
extern void WrapperStream_GetNonWrapperBaseStream_m21B77CDBE5EFF4E60BD37604577432F6707AC517 (void);
// 0x0000075F System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::SearchWrappedStream(System.Func`2<System.IO.Stream,System.Boolean>)
extern void WrapperStream_SearchWrappedStream_mCC8BFAD19D8887D77FA547A3D27806405F31F97B (void);
// 0x00000760 System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::SearchWrappedStream(System.IO.Stream,System.Func`2<System.IO.Stream,System.Boolean>)
extern void WrapperStream_SearchWrappedStream_m96D13AE5A10E6ED516C40CE1D20BA44BF1C81427 (void);
// 0x00000761 System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_CanRead()
extern void WrapperStream_get_CanRead_m4BA5A19D73B3F1373B817671BF3CCCCF7D800457 (void);
// 0x00000762 System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_CanSeek()
extern void WrapperStream_get_CanSeek_m8404E6AE766A2C332BC11E9BE2C33F7C470DA25E (void);
// 0x00000763 System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_CanWrite()
extern void WrapperStream_get_CanWrite_m060705526E11DF9C4C9DD9C756DD139698FF9E46 (void);
// 0x00000764 System.Void Amazon.Runtime.Internal.Util.WrapperStream::Dispose(System.Boolean)
extern void WrapperStream_Dispose_mBDD2064ECF5945930F75CD0DCD3FF81ED704EB93 (void);
// 0x00000765 System.Int64 Amazon.Runtime.Internal.Util.WrapperStream::get_Length()
extern void WrapperStream_get_Length_mEEA0CE603ED4708F7388999013DCE7F9E3FB3BC0 (void);
// 0x00000766 System.Int64 Amazon.Runtime.Internal.Util.WrapperStream::get_Position()
extern void WrapperStream_get_Position_mC2B75A713474CDA16D2423263B1A460FEC7D7DC2 (void);
// 0x00000767 System.Void Amazon.Runtime.Internal.Util.WrapperStream::set_Position(System.Int64)
extern void WrapperStream_set_Position_mC2B673FD3657BD19C39169533D3C71B390296F3C (void);
// 0x00000768 System.Int32 Amazon.Runtime.Internal.Util.WrapperStream::get_ReadTimeout()
extern void WrapperStream_get_ReadTimeout_mE467214DD610D4F1324C7E11F60530DF251515F3 (void);
// 0x00000769 System.Void Amazon.Runtime.Internal.Util.WrapperStream::set_ReadTimeout(System.Int32)
extern void WrapperStream_set_ReadTimeout_mD92FA6636A63C7A04424399FF54552C3D75FB61D (void);
// 0x0000076A System.Int32 Amazon.Runtime.Internal.Util.WrapperStream::get_WriteTimeout()
extern void WrapperStream_get_WriteTimeout_m5DF0633A0F0040A9B8B339F4731FF8E02343EC2E (void);
// 0x0000076B System.Void Amazon.Runtime.Internal.Util.WrapperStream::Flush()
extern void WrapperStream_Flush_mCC0BD37433C8ECF9D34D4EF30D71B67762DFF291 (void);
// 0x0000076C System.Int32 Amazon.Runtime.Internal.Util.WrapperStream::Read(System.Byte[],System.Int32,System.Int32)
extern void WrapperStream_Read_mE1D853429ED4D36C770345B447186FF75F63C34B (void);
// 0x0000076D System.Int64 Amazon.Runtime.Internal.Util.WrapperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void WrapperStream_Seek_m7A7639B6DF8F8054454EBC6F1BC46A7FDF1E6573 (void);
// 0x0000076E System.Void Amazon.Runtime.Internal.Util.WrapperStream::Write(System.Byte[],System.Int32,System.Int32)
extern void WrapperStream_Write_mC2C2CAD74002505C6EED49613E5B9EA411B9C0A6 (void);
// 0x0000076F System.Threading.Tasks.Task`1<System.Int32> Amazon.Runtime.Internal.Util.WrapperStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void WrapperStream_ReadAsync_m475537AB27E220B8402E1DC2EDE77ED909C5D770 (void);
// 0x00000770 System.Threading.Tasks.Task Amazon.Runtime.Internal.Util.WrapperStream::WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void WrapperStream_WriteAsync_m58FC94640CFDB97CBF47133B7DBDA83C548B03B5 (void);
// 0x00000771 System.Boolean Amazon.Runtime.Internal.Util.WrapperStream::get_HasLength()
extern void WrapperStream_get_HasLength_m2E69B14EFD028357E090139D0E88B6ABD0700FE3 (void);
// 0x00000772 System.Void Amazon.Runtime.Internal.Util.IniFile::.ctor(System.String)
extern void IniFile__ctor_m4AF5DC976681C2EE54EC182057AD26F9DDE4C42E (void);
// 0x00000773 System.String Amazon.Runtime.Internal.Util.IniFile::get_FilePath()
extern void IniFile_get_FilePath_m4038C59B30A6304F21A8C1B6FB0B73760144E30D (void);
// 0x00000774 System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Util.IniFile::get_Lines()
extern void IniFile_get_Lines_m75B529190BCB6672BB957E2E76588F6F0A886333 (void);
// 0x00000775 System.Boolean Amazon.Runtime.Internal.Util.IniFile::TryGetSection(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void IniFile_TryGetSection_mD24708E03892B3238BC90C5FA692E86A833DFDF4 (void);
// 0x00000776 System.Boolean Amazon.Runtime.Internal.Util.IniFile::TryGetSection(System.Text.RegularExpressions.Regex,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void IniFile_TryGetSection_mA28DECB79F6BCC697192AE71709A72A88FAA3720 (void);
// 0x00000777 System.Boolean Amazon.Runtime.Internal.Util.IniFile::TryGetSection(System.Text.RegularExpressions.Regex,System.String&,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void IniFile_TryGetSection_m803CA08A6E71E7EE10AF86E32AEE7BBABA337E30 (void);
// 0x00000778 System.String Amazon.Runtime.Internal.Util.IniFile::ToString()
extern void IniFile_ToString_m93380C8F0FEF816398A63D4515DBE7863A8F4992 (void);
// 0x00000779 System.Boolean Amazon.Runtime.Internal.Util.IniFile::IsDuplicateProperty(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String,System.String,System.Int32)
extern void IniFile_IsDuplicateProperty_mE6CC8DBA9637BBFC36E8BEF2F77E3B1D956645AE (void);
// 0x0000077A System.Void Amazon.Runtime.Internal.Util.IniFile::Validate()
extern void IniFile_Validate_m92B0DAA6CAF532017A3693B08B11021F835F75D9 (void);
// 0x0000077B System.Boolean Amazon.Runtime.Internal.Util.IniFile::TrySeekSection(System.Text.RegularExpressions.Regex,System.Int32&,System.String&)
extern void IniFile_TrySeekSection_mF214B91AE9C98ACBE8B7A712B1A41DD0E6EB3BEB (void);
// 0x0000077C System.Boolean Amazon.Runtime.Internal.Util.IniFile::TrySeekSection(System.String,System.Int32&)
extern void IniFile_TrySeekSection_mE0986EC719D4349E369F6CC7972A1D45AEA8871E (void);
// 0x0000077D System.Boolean Amazon.Runtime.Internal.Util.IniFile::SeekSection(System.Int32&,System.String&)
extern void IniFile_SeekSection_m6BF76DBC2F525496EA150167FBA2DBEB8DF8EC1D (void);
// 0x0000077E System.Boolean Amazon.Runtime.Internal.Util.IniFile::SeekProperty(System.Int32&,System.String&,System.String&)
extern void IniFile_SeekProperty_m843DDD5C093D9A833FD3BAB9F35ECFD7C7ED3CE4 (void);
// 0x0000077F System.String Amazon.Runtime.Internal.Util.IniFile::GetErrorMessage(System.Int32)
extern void IniFile_GetErrorMessage_mDAE8BE1DB235DD89E3D0CC9CB9AB71211295ECED (void);
// 0x00000780 System.Boolean Amazon.Runtime.Internal.Util.IniFile::IsCommentOrBlank(System.String)
extern void IniFile_IsCommentOrBlank_m50DA2EF2AD2FDF4153AE73AC4989A2BF71BA87FF (void);
// 0x00000781 System.Boolean Amazon.Runtime.Internal.Util.IniFile::IsSection(System.String)
extern void IniFile_IsSection_m4B68220FE6192BAB6C75FB703C8460ECA1B287F5 (void);
// 0x00000782 System.Boolean Amazon.Runtime.Internal.Util.IniFile::TryParseSection(System.String,System.String&)
extern void IniFile_TryParseSection_mBE7518C7101C96397498B3AA33CE7BE92961DA5B (void);
// 0x00000783 System.Boolean Amazon.Runtime.Internal.Util.IniFile::IsProperty(System.String)
extern void IniFile_IsProperty_m19BA77923961BDAEF1246DA439D003E115460063 (void);
// 0x00000784 System.Boolean Amazon.Runtime.Internal.Util.IniFile::TryParseProperty(System.String,System.String&,System.String&)
extern void IniFile_TryParseProperty_m21F0281B31BE33199CA1F7C0EF35608177CB6C30 (void);
// 0x00000785 System.String Amazon.Runtime.Internal.Util.IniFile::GetLineMessage(System.Int32)
extern void IniFile_GetLineMessage_m7DABC42217FC51118B815C31089E365A49DDE451 (void);
// 0x00000786 System.Void Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::.ctor(System.Type)
extern void InternalSystemDiagnosticsLogger__ctor_m0AD0B53E18F5FDD5D3EA59F3C24D623DD5A193F5 (void);
// 0x00000787 System.Void Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::Error(System.Exception,System.String,System.Object[])
extern void InternalSystemDiagnosticsLogger_Error_m43F37752F40DDDA5E12BB8215DA6AA108827B464 (void);
// 0x00000788 System.Void Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::Debug(System.Exception,System.String,System.Object[])
extern void InternalSystemDiagnosticsLogger_Debug_mA2001BC068D54399EA529AD2F7B6394C3F7806C2 (void);
// 0x00000789 System.Void Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::DebugFormat(System.String,System.Object[])
extern void InternalSystemDiagnosticsLogger_DebugFormat_m6F0D41299168CCA93DC6CD0388F93395618D7DF9 (void);
// 0x0000078A System.Void Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::InfoFormat(System.String,System.Object[])
extern void InternalSystemDiagnosticsLogger_InfoFormat_m9023523528D24439C250B64566BE630C5CD7D407 (void);
// 0x0000078B System.Boolean Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::get_IsDebugEnabled()
extern void InternalSystemDiagnosticsLogger_get_IsDebugEnabled_mA7389402E736AFCEEB438699CA8B51F703442A27 (void);
// 0x0000078C System.Boolean Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::get_IsErrorEnabled()
extern void InternalSystemDiagnosticsLogger_get_IsErrorEnabled_mE71EF1CA9833C8B263D977CF30E4A4A250399C36 (void);
// 0x0000078D System.Boolean Amazon.Runtime.Internal.Util.InternalSystemDiagnosticsLogger::get_IsInfoEnabled()
extern void InternalSystemDiagnosticsLogger_get_IsInfoEnabled_m48A0089397E2EA90A82DC0F7C0536842DA4A81B3 (void);
// 0x0000078E System.Diagnostics.TraceSource Amazon.Runtime.Internal.Util.TraceSourceUtil::GetTraceSource(System.Type)
extern void TraceSourceUtil_GetTraceSource_mF42F323087F386FF58B055429B46C47BC99A9E81 (void);
// 0x0000078F System.Diagnostics.TraceSource Amazon.Runtime.Internal.Util.TraceSourceUtil::GetTraceSource(System.Type,System.Diagnostics.SourceLevels)
extern void TraceSourceUtil_GetTraceSource_m365FBA77060CBA44B48F3E5C810594445C1A7188 (void);
// 0x00000790 System.Diagnostics.TraceSource Amazon.Runtime.Internal.Util.TraceSourceUtil::GetTraceSourceWithListeners(System.String,System.Diagnostics.SourceLevels)
extern void TraceSourceUtil_GetTraceSourceWithListeners_mACAD6DF0BBD75747624EFD5083FF00AAD04240AC (void);
// 0x00000791 System.String Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::get_OriginalContents()
extern void OptimisticLockedTextFile_get_OriginalContents_m7ED6E4DB738F208C66A18E8BE114AFFE62296852 (void);
// 0x00000792 System.Void Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::set_OriginalContents(System.String)
extern void OptimisticLockedTextFile_set_OriginalContents_mBDD535792A93B7CA5602DAEADE392190C5D51AF3 (void);
// 0x00000793 System.String Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::get_FilePath()
extern void OptimisticLockedTextFile_get_FilePath_m8B21A1CC348771C89308D256C7A25B7AE7884491 (void);
// 0x00000794 System.Void Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::set_FilePath(System.String)
extern void OptimisticLockedTextFile_set_FilePath_mFD1F1F01F35075BE40E6CC242C9E85DA541E4E34 (void);
// 0x00000795 System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::get_Lines()
extern void OptimisticLockedTextFile_get_Lines_m6DF3631DE46A8EB2B3C5B0F96F89DB2D711BDBA9 (void);
// 0x00000796 System.Void Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::set_Lines(System.Collections.Generic.List`1<System.String>)
extern void OptimisticLockedTextFile_set_Lines_mCD018C2303B399C5A81BC7A2D90DAA0233CD0254 (void);
// 0x00000797 System.Void Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::.ctor(System.String)
extern void OptimisticLockedTextFile__ctor_m7E505437A9A232CCE77C44886E70AE1BBDEC14EB (void);
// 0x00000798 System.String Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::ToString()
extern void OptimisticLockedTextFile_ToString_m1E3AC2B920FDFDE57FF0A7E09B1EAFE2725993F4 (void);
// 0x00000799 System.Void Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::Read()
extern void OptimisticLockedTextFile_Read_mE16D7D604A86DDF4DC79120521453445C3335181 (void);
// 0x0000079A System.Boolean Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::HasEnding(System.String)
extern void OptimisticLockedTextFile_HasEnding_m9D234A7CE52DB8E707595A2CA27A0872656E9977 (void);
// 0x0000079B System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Util.OptimisticLockedTextFile::ReadLinesWithEndings(System.String)
extern void OptimisticLockedTextFile_ReadLinesWithEndings_mAF25338F1A89284E14268C8BE3EC8E3F5ECF3D33 (void);
// 0x0000079C System.Boolean Amazon.Runtime.Internal.Util.ProfileIniFile::get_ProfileMarkerRequired()
extern void ProfileIniFile_get_ProfileMarkerRequired_mBF818D67B6A16168B253C60CD65BD3CB4A6E29E4 (void);
// 0x0000079D System.Void Amazon.Runtime.Internal.Util.ProfileIniFile::set_ProfileMarkerRequired(System.Boolean)
extern void ProfileIniFile_set_ProfileMarkerRequired_m78330C35440F6AAAA16C6B6115F7611E640FDCC9 (void);
// 0x0000079E System.Void Amazon.Runtime.Internal.Util.ProfileIniFile::.ctor(System.String,System.Boolean)
extern void ProfileIniFile__ctor_mEE9EFEF3E6393590F2CA0281FD1D30DB20FE7ECE (void);
// 0x0000079F System.Boolean Amazon.Runtime.Internal.Util.ProfileIniFile::TryGetSection(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>&)
extern void ProfileIniFile_TryGetSection_mF3EF38C926678C32CB05A3E3922DDB87598C4412 (void);
// 0x000007A0 T Amazon.Runtime.Internal.Util.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task`1<T>>)
// 0x000007A1 System.Exception Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::get_ObjectException()
extern void ExclusiveSynchronizationContext_get_ObjectException_m7D008D6F4B95533A1B9994CF182147BCA532C3A5 (void);
// 0x000007A2 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::set_ObjectException(System.Exception)
extern void ExclusiveSynchronizationContext_set_ObjectException_m6C1CB681D339DED2069010AA0513E31E155D20DE (void);
// 0x000007A3 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Send_mA557C607D37719A5AE0B66D1BF41D65A5E6A9B53 (void);
// 0x000007A4 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Post_m4D7D07238647EDFFFF8AF1E1FE905C00CE9E5E18 (void);
// 0x000007A5 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::EndMessageLoop()
extern void ExclusiveSynchronizationContext_EndMessageLoop_m8C1F5C9019C02B635B7B5F1B00C427773722D8D0 (void);
// 0x000007A6 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::BeginMessageLoop()
extern void ExclusiveSynchronizationContext_BeginMessageLoop_m6B34068C382E5605750CE5F34B5438A24C0A19F0 (void);
// 0x000007A7 System.Threading.SynchronizationContext Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::CreateCopy()
extern void ExclusiveSynchronizationContext_CreateCopy_m61AFBD0AA23F755BED7D900EAAE45AA5583F6735 (void);
// 0x000007A8 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::.ctor()
extern void ExclusiveSynchronizationContext__ctor_m74EE592107B49B31BAE200E05C0505D6C1224B89 (void);
// 0x000007A9 System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/ExclusiveSynchronizationContext::<EndMessageLoop>b__9_0(System.Object)
extern void ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m7BEA15F25CAD1B33751ADD0797A19CE192B808A6 (void);
// 0x000007AA System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/<>c__DisplayClass1_0`1::.ctor()
// 0x000007AB System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/<>c__DisplayClass1_0`1::<RunSync>b__0(System.Object)
// 0x000007AC System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::MoveNext()
// 0x000007AD System.Void Amazon.Runtime.Internal.Util.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000007AE Amazon.Runtime.Internal.ErrorResponse Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void ErrorResponseUnmarshaller_Unmarshall_m6A267750A35488F8A8F190AFA22EB15453900188 (void);
// 0x000007AF System.Void Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::PopulateErrorResponseFromXmlIfPossible(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void ErrorResponseUnmarshaller_PopulateErrorResponseFromXmlIfPossible_m64358A89E2969B7473F174843197DB3D2AFB9D4C (void);
// 0x000007B0 System.Boolean Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::TryReadContext(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void ErrorResponseUnmarshaller_TryReadContext_m25AD4E6349CC178AFD83FE4DC4008CCC64AF9DA4 (void);
// 0x000007B1 Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::GetInstance()
extern void ErrorResponseUnmarshaller_GetInstance_m3D989CD75ED16E77AC1E51F105B6AD3B2CAC2B3F (void);
// 0x000007B2 System.Void Amazon.Runtime.Internal.Transform.ErrorResponseUnmarshaller::.ctor()
extern void ErrorResponseUnmarshaller__ctor_m1C9FC3E1045E26E48B6295F57A4CC3B0E6D20BCC (void);
// 0x000007B3 T Amazon.Runtime.Internal.Transform.IMarshaller`2::Marshall(R)
// 0x000007B4 System.Void Amazon.Runtime.Internal.Transform.MarshallerContext::set_Request(Amazon.Runtime.Internal.IRequest)
extern void MarshallerContext_set_Request_m8F802D68F09FC7F70F1BA36E3B4FEA893706D834 (void);
// 0x000007B5 System.Void Amazon.Runtime.Internal.Transform.MarshallerContext::.ctor(Amazon.Runtime.Internal.IRequest)
extern void MarshallerContext__ctor_mEF233921C78C3C3F2A417E0AD8BCE26B777338A3 (void);
// 0x000007B6 ThirdParty.Json.LitJson.JsonWriter Amazon.Runtime.Internal.Transform.JsonMarshallerContext::get_Writer()
extern void JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B (void);
// 0x000007B7 System.Void Amazon.Runtime.Internal.Transform.JsonMarshallerContext::set_Writer(ThirdParty.Json.LitJson.JsonWriter)
extern void JsonMarshallerContext_set_Writer_m324F3C5EBDFBD98ED3E6B96565B40DCE1B066955 (void);
// 0x000007B8 System.Void Amazon.Runtime.Internal.Transform.JsonMarshallerContext::.ctor(Amazon.Runtime.Internal.IRequest,ThirdParty.Json.LitJson.JsonWriter)
extern void JsonMarshallerContext__ctor_m72EC1A523D8129B418D93DAA8CB9D73FF5DC8E76 (void);
// 0x000007B9 T Amazon.Runtime.Internal.Transform.IUnmarshaller`2::Unmarshall(R)
// 0x000007BA System.Int64 Amazon.Runtime.Internal.Transform.IWebResponseData::get_ContentLength()
// 0x000007BB System.String Amazon.Runtime.Internal.Transform.IWebResponseData::get_ContentType()
// 0x000007BC System.Net.HttpStatusCode Amazon.Runtime.Internal.Transform.IWebResponseData::get_StatusCode()
// 0x000007BD System.String[] Amazon.Runtime.Internal.Transform.IWebResponseData::GetHeaderNames()
// 0x000007BE System.Boolean Amazon.Runtime.Internal.Transform.IWebResponseData::IsHeaderPresent(System.String)
// 0x000007BF System.String Amazon.Runtime.Internal.Transform.IWebResponseData::GetHeaderValue(System.String)
// 0x000007C0 Amazon.Runtime.Internal.Transform.IHttpResponseBody Amazon.Runtime.Internal.Transform.IWebResponseData::get_ResponseBody()
// 0x000007C1 System.IO.Stream Amazon.Runtime.Internal.Transform.IHttpResponseBody::OpenResponse()
// 0x000007C2 System.Threading.Tasks.Task`1<System.IO.Stream> Amazon.Runtime.Internal.Transform.IHttpResponseBody::OpenResponseAsync()
// 0x000007C3 Amazon.Runtime.Internal.ErrorResponse Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void JsonErrorResponseUnmarshaller_Unmarshall_m057826DF6640BD156507A76F4BDA8CEB1C43EE9B (void);
// 0x000007C4 System.Void Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::GetValuesFromJsonIfPossible(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.String&,System.String&,System.String&)
extern void JsonErrorResponseUnmarshaller_GetValuesFromJsonIfPossible_mC7A98D5BEA9FE4539CB22926E5C6ADA335856E03 (void);
// 0x000007C5 System.Boolean Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::TryReadContext(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void JsonErrorResponseUnmarshaller_TryReadContext_mCF2332A25F261074C57105A8D85AC824685424AA (void);
// 0x000007C6 Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::GetInstance()
extern void JsonErrorResponseUnmarshaller_GetInstance_mF38659F6157C8EDF253734672FFD34FCD188AAA3 (void);
// 0x000007C7 System.Void Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::.ctor()
extern void JsonErrorResponseUnmarshaller__ctor_m358A02D75D1D2357A7ED2E548FE2DC10E850C3D0 (void);
// 0x000007C8 System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::.ctor(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern void JsonUnmarshallerContext__ctor_mF3B6DEDDF1A70D283EFD799EDAAE8A952AC87D7D (void);
// 0x000007C9 System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_IsStartOfDocument()
extern void JsonUnmarshallerContext_get_IsStartOfDocument_mEDB4C32061E285C35D6723BA16220A3C16677730 (void);
// 0x000007CA System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_IsEndElement()
extern void JsonUnmarshallerContext_get_IsEndElement_mD1C62D2E1B529CE910B6F891BE3E7A4F58E49556 (void);
// 0x000007CB System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_IsStartElement()
extern void JsonUnmarshallerContext_get_IsStartElement_m9A7721EC6688782AEC29B052B1581917EB4A0A8F (void);
// 0x000007CC System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentDepth()
extern void JsonUnmarshallerContext_get_CurrentDepth_mB2770FC3F3BE219BB79B508A49CCEA9AC65FCE84 (void);
// 0x000007CD System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentPath()
extern void JsonUnmarshallerContext_get_CurrentPath_m474A141F0CA1400939219B0C1C774C04E3CDAB4C (void);
// 0x000007CE System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Read()
extern void JsonUnmarshallerContext_Read_m4C00443ED8484C982F4D00E5BB31674C2AB9E6F1 (void);
// 0x000007CF System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Peek(ThirdParty.Json.LitJson.JsonToken)
extern void JsonUnmarshallerContext_Peek_m0E18DC4029DEBF901D313C4798197DC307636127 (void);
// 0x000007D0 System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::ReadText()
extern void JsonUnmarshallerContext_ReadText_mEE7D6100B8DC120E615F4DEC67CB3617D01A3C65 (void);
// 0x000007D1 ThirdParty.Json.LitJson.JsonToken Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentTokenType()
extern void JsonUnmarshallerContext_get_CurrentTokenType_mA60F9E7148B1EF1BCD664D3AB068A81910999E19 (void);
// 0x000007D2 System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Peek()
extern void JsonUnmarshallerContext_Peek_mBE31DFF89AA0D1B1E0F71C17D049D5193C9F8197 (void);
// 0x000007D3 System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::StreamPeek()
extern void JsonUnmarshallerContext_StreamPeek_m415E13D43AFECBCA04B8CEC10EAAF0BD381070E7 (void);
// 0x000007D4 System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::UpdateContext()
extern void JsonUnmarshallerContext_UpdateContext_m02339EFE5531AEC9DA39397C2A837E8A8CF3D5CA (void);
// 0x000007D5 System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::Dispose(System.Boolean)
extern void JsonUnmarshallerContext_Dispose_m6E749BBC11043E6BA1C6EBE5F1A4678745E02170 (void);
// 0x000007D6 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegmentType Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment::get_SegmentType()
extern void PathSegment_get_SegmentType_m65B82F8564E2206BCE241B78F79E678AA696D575 (void);
// 0x000007D7 System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment::set_SegmentType(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegmentType)
extern void PathSegment_set_SegmentType_m40F0CBA4B9A4C76E4256AD3195C964DD88906D2A (void);
// 0x000007D8 System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment::get_Value()
extern void PathSegment_get_Value_m35D398000A8EA78DE5FAF47E7BC0BF1C44EDBDD2 (void);
// 0x000007D9 System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment::set_Value(System.String)
extern void PathSegment_set_Value_m909F2ED45DF5DCABEA9D81BC2761C3D8267F5101 (void);
// 0x000007DA System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::get_CurrentDepth()
extern void JsonPathStack_get_CurrentDepth_mBDC7BB722BC988DF7E63D066A5395AADBA1517CA (void);
// 0x000007DB System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::get_CurrentPath()
extern void JsonPathStack_get_CurrentPath_m9CC17AB933CEE8BCF6C9A71018A83792DCBE68D0 (void);
// 0x000007DC System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::Push(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment)
extern void JsonPathStack_Push_mD8705841894D15A02C67353B2126D215646F15B6 (void);
// 0x000007DD Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::Pop()
extern void JsonPathStack_Pop_m2532BF29F37DED88A341CDDE5037C0B05655CA8B (void);
// 0x000007DE Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/PathSegment Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::Peek()
extern void JsonPathStack_Peek_mA13FF5D7EFA8AB0D80C0FA4ED2AEF2D182B2ACB7 (void);
// 0x000007DF System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::get_Count()
extern void JsonPathStack_get_Count_m15E84330105BA5A6A0CA7C786FF31D7F81A0E374 (void);
// 0x000007E0 System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::.ctor()
extern void JsonPathStack__ctor_mDF39374E807EA8054C68231642C94F7DA66514C6 (void);
// 0x000007E1 Amazon.Runtime.Internal.Transform.UnmarshallerContext Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::CreateContext(Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean,System.IO.Stream,Amazon.Runtime.Internal.Util.RequestMetrics,System.Boolean)
extern void ResponseUnmarshaller_CreateContext_mA2DE575FA22FEF2C23FED0F4C2241D2FB53081D6 (void);
// 0x000007E2 System.Boolean Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::get_HasStreamingProperty()
extern void ResponseUnmarshaller_get_HasStreamingProperty_m2BCF48C61431F1CE4EAFB156A3A3109EB9EE2F21 (void);
// 0x000007E3 Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.UnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void ResponseUnmarshaller_UnmarshallException_mDA4231C57076783038BA09C1D9A8E92107CB9673 (void);
// 0x000007E4 Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::UnmarshallResponse(Amazon.Runtime.Internal.Transform.UnmarshallerContext)
extern void ResponseUnmarshaller_UnmarshallResponse_m5461512CEB99C5E41B14A958382B28653D8EC32F (void);
// 0x000007E5 Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.UnmarshallerContext)
// 0x000007E6 Amazon.Runtime.Internal.Transform.UnmarshallerContext Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::ConstructUnmarshallerContext(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
// 0x000007E7 System.Boolean Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::ShouldReadEntireResponse(Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern void ResponseUnmarshaller_ShouldReadEntireResponse_m535B3768F38132F77853A04AFB87CF00AC7E088F (void);
// 0x000007E8 System.Void Amazon.Runtime.Internal.Transform.ResponseUnmarshaller::.ctor()
extern void ResponseUnmarshaller__ctor_mCB109484E65959D2535B81596886962662A944E9 (void);
// 0x000007E9 Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.UnmarshallerContext)
extern void XmlResponseUnmarshaller_Unmarshall_m537436332683B6101DF5F6D2D07FF28E234726FA (void);
// 0x000007EA Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.UnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void XmlResponseUnmarshaller_UnmarshallException_m61E020B3B119BD1D5AC124ED11FC70C841063BC0 (void);
// 0x000007EB Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
// 0x000007EC Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
// 0x000007ED Amazon.Runtime.Internal.Transform.UnmarshallerContext Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller::ConstructUnmarshallerContext(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern void XmlResponseUnmarshaller_ConstructUnmarshallerContext_mBB63C0330008B79B8AE24A2BB14DFE007AC35515 (void);
// 0x000007EE System.Void Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller::.ctor()
extern void XmlResponseUnmarshaller__ctor_m975DBC6E2BA1462E4CCD19A9329C837BE0F45B38 (void);
// 0x000007EF Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.UnmarshallerContext)
extern void JsonResponseUnmarshaller_Unmarshall_m1DB481EA4F4497B6174B143260329ABAD3A9EF36 (void);
// 0x000007F0 Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.UnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void JsonResponseUnmarshaller_UnmarshallException_m70AD13D81D31B67CF5D9994964E078069A3B256C (void);
// 0x000007F1 Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
// 0x000007F2 Amazon.Runtime.AmazonServiceException Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
// 0x000007F3 Amazon.Runtime.Internal.Transform.UnmarshallerContext Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::ConstructUnmarshallerContext(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern void JsonResponseUnmarshaller_ConstructUnmarshallerContext_m97DA0A34FB557F3EF055FD3D7667CB12329583B6 (void);
// 0x000007F4 System.Boolean Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::ShouldReadEntireResponse(Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern void JsonResponseUnmarshaller_ShouldReadEntireResponse_m2233FE2C5943B8214213DCA5859029A3E76200CE (void);
// 0x000007F5 System.Void Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::.ctor()
extern void JsonResponseUnmarshaller__ctor_m7A471A9D41EBFE83FEA37BAAD40BCF2D485F9ADE (void);
// 0x000007F6 T Amazon.Runtime.Internal.Transform.SimpleTypeUnmarshaller`1::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
// 0x000007F7 T Amazon.Runtime.Internal.Transform.SimpleTypeUnmarshaller`1::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
// 0x000007F8 System.Void Amazon.Runtime.Internal.Transform.IntUnmarshaller::.ctor()
extern void IntUnmarshaller__ctor_m2A97B34D2BD8CDDC53DD69EDC693779C15B23C27 (void);
// 0x000007F9 Amazon.Runtime.Internal.Transform.IntUnmarshaller Amazon.Runtime.Internal.Transform.IntUnmarshaller::get_Instance()
extern void IntUnmarshaller_get_Instance_m746BA04951EF77262464E3E09FDC12BFDAA4E321 (void);
// 0x000007FA System.Int32 Amazon.Runtime.Internal.Transform.IntUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void IntUnmarshaller_Unmarshall_m5F85391FEA87FBA3EC21163FB10692428215B0CF (void);
// 0x000007FB System.Int32 Amazon.Runtime.Internal.Transform.IntUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void IntUnmarshaller_Unmarshall_m6DC0F5DE60C245CE4314BB8D8A7671441EDC46E7 (void);
// 0x000007FC System.Void Amazon.Runtime.Internal.Transform.IntUnmarshaller::.cctor()
extern void IntUnmarshaller__cctor_m1B92338A3397BA99FCF703A99589BB8819ACA178 (void);
// 0x000007FD System.Void Amazon.Runtime.Internal.Transform.BoolUnmarshaller::.ctor()
extern void BoolUnmarshaller__ctor_mC0F123A1C614CE53BBCCE05504F759D96FB1CCD7 (void);
// 0x000007FE Amazon.Runtime.Internal.Transform.BoolUnmarshaller Amazon.Runtime.Internal.Transform.BoolUnmarshaller::get_Instance()
extern void BoolUnmarshaller_get_Instance_mF9B455017CCF1BCF02671BEA6968CB71D3C95F29 (void);
// 0x000007FF System.Boolean Amazon.Runtime.Internal.Transform.BoolUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void BoolUnmarshaller_Unmarshall_m40EEAE1622C39195AAE166F0609AE96360A5E88C (void);
// 0x00000800 System.Boolean Amazon.Runtime.Internal.Transform.BoolUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void BoolUnmarshaller_Unmarshall_m320688D5889AEDF067214028DD4EEC8755CF6A35 (void);
// 0x00000801 System.Void Amazon.Runtime.Internal.Transform.BoolUnmarshaller::.cctor()
extern void BoolUnmarshaller__cctor_m70231FE702B88CBCEE2334A4BB7CA3CA71EC187A (void);
// 0x00000802 System.Void Amazon.Runtime.Internal.Transform.StringUnmarshaller::.ctor()
extern void StringUnmarshaller__ctor_m4FF9B4A68C8061C9B9F26B4DF665A1A11457E25E (void);
// 0x00000803 Amazon.Runtime.Internal.Transform.StringUnmarshaller Amazon.Runtime.Internal.Transform.StringUnmarshaller::get_Instance()
extern void StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436 (void);
// 0x00000804 Amazon.Runtime.Internal.Transform.StringUnmarshaller Amazon.Runtime.Internal.Transform.StringUnmarshaller::GetInstance()
extern void StringUnmarshaller_GetInstance_mCEB0A724B45D56FD3A6F22823237C506B296406F (void);
// 0x00000805 System.String Amazon.Runtime.Internal.Transform.StringUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void StringUnmarshaller_Unmarshall_m37C8340144CE14394EA2171204879EB540ABAF3C (void);
// 0x00000806 System.String Amazon.Runtime.Internal.Transform.StringUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415 (void);
// 0x00000807 System.Void Amazon.Runtime.Internal.Transform.StringUnmarshaller::.cctor()
extern void StringUnmarshaller__cctor_mCD18E97CCFB622332CBFED0E124DA772315DC10D (void);
// 0x00000808 System.Void Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::.ctor()
extern void DateTimeUnmarshaller__ctor_m3621628E6EDB2450DB7C9A5411B2E3E8EA6FEF2A (void);
// 0x00000809 Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::get_Instance()
extern void DateTimeUnmarshaller_get_Instance_m6576E37458B18A1C1BA0E122716548A4F8410D01 (void);
// 0x0000080A System.DateTime Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void DateTimeUnmarshaller_Unmarshall_mD43B95D52A49BCF94AD1422F4552ACCED54E0849 (void);
// 0x0000080B System.DateTime Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void DateTimeUnmarshaller_Unmarshall_m9542A67765A9E84CB5DF4A7AEA56060F683D7BE1 (void);
// 0x0000080C System.Nullable`1<System.DateTime> Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::UnmarshallInternal(System.String,System.Boolean)
extern void DateTimeUnmarshaller_UnmarshallInternal_m3382B1041067FA8807203A2F2ADD47B16AD39189 (void);
// 0x0000080D System.Void Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::.cctor()
extern void DateTimeUnmarshaller__cctor_m1593E2C0738834E455503B4DF532789DD8C56BCA (void);
// 0x0000080E System.Void Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::.ctor()
extern void ResponseMetadataUnmarshaller__ctor_mDB588D2F3B766EC43F2E5DC782FCC2F180DA3D36 (void);
// 0x0000080F Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::get_Instance()
extern void ResponseMetadataUnmarshaller_get_Instance_mFC0B6EFC6832968A56181C722E74CB66E4F9CD02 (void);
// 0x00000810 Amazon.Runtime.ResponseMetadata Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void ResponseMetadataUnmarshaller_Unmarshall_mC6E97054299B8F693E7DBBEAC59D2E984BE76C0A (void);
// 0x00000811 Amazon.Runtime.ResponseMetadata Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ResponseMetadataUnmarshaller_Unmarshall_m375F4E691DCDDD0EC7F4F9C04317217EECBB9FB9 (void);
// 0x00000812 System.Void Amazon.Runtime.Internal.Transform.ResponseMetadataUnmarshaller::.cctor()
extern void ResponseMetadataUnmarshaller__cctor_mF3BE41A5B7CDB7AEB4E9EC93FA10285A1F48E2C3 (void);
// 0x00000813 System.Void Amazon.Runtime.Internal.Transform.KeyValueUnmarshaller`4::.ctor(KUnmarshaller,VUnmarshaller)
// 0x00000814 System.Collections.Generic.KeyValuePair`2<K,V> Amazon.Runtime.Internal.Transform.KeyValueUnmarshaller`4::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
// 0x00000815 System.Collections.Generic.KeyValuePair`2<K,V> Amazon.Runtime.Internal.Transform.KeyValueUnmarshaller`4::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
// 0x00000816 System.Void Amazon.Runtime.Internal.Transform.ListUnmarshaller`2::.ctor(IUnmarshaller)
// 0x00000817 System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
// 0x00000818 System.Collections.Generic.List`1<I> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
// 0x00000819 System.Void Amazon.Runtime.Internal.Transform.DictionaryUnmarshaller`4::.ctor(TKeyUnmarshaller,TValueUnmarshaller)
// 0x0000081A System.Collections.Generic.Dictionary`2<TKey,TValue> Amazon.Runtime.Internal.Transform.DictionaryUnmarshaller`4::Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
// 0x0000081B System.Collections.Generic.Dictionary`2<TKey,TValue> Amazon.Runtime.Internal.Transform.DictionaryUnmarshaller`4::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
// 0x0000081C System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_MaintainResponseBody()
extern void UnmarshallerContext_get_MaintainResponseBody_mEE9520F845AEF875317FB56E2DD0429A98422D93 (void);
// 0x0000081D System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_MaintainResponseBody(System.Boolean)
extern void UnmarshallerContext_set_MaintainResponseBody_mDD39548A3482E4C6C005544A32A53468DD962322 (void);
// 0x0000081E System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsException()
extern void UnmarshallerContext_get_IsException_m88F8C42DABA97B6253A32CC60EEDC2D756C337F6 (void);
// 0x0000081F System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_IsException(System.Boolean)
extern void UnmarshallerContext_set_IsException_m2D4133C986F235F215588C6C87D8515E4B00F585 (void);
// 0x00000820 ThirdParty.Ionic.Zlib.CrcCalculatorStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CrcStream()
extern void UnmarshallerContext_get_CrcStream_m01B55B4F84F8411D97E5299E88DCA96BD9761680 (void);
// 0x00000821 System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_CrcStream(ThirdParty.Ionic.Zlib.CrcCalculatorStream)
extern void UnmarshallerContext_set_CrcStream_mDCD547E103C6E2B8F63A483422E48D66A60BAC4E (void);
// 0x00000822 System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_Crc32Result()
extern void UnmarshallerContext_get_Crc32Result_mFCCC6104016C906AE1716B3660E6166F2DEF085F (void);
// 0x00000823 System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_Crc32Result(System.Int32)
extern void UnmarshallerContext_set_Crc32Result_m48140B1D642B7A6F16B071E4A0D5D5E8683C25B9 (void);
// 0x00000824 Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_WebResponseData()
extern void UnmarshallerContext_get_WebResponseData_mF6135EFB850AEAAD2B212B7038C8C25AA38AB8F8 (void);
// 0x00000825 System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_WebResponseData(Amazon.Runtime.Internal.Transform.IWebResponseData)
extern void UnmarshallerContext_set_WebResponseData_m7F8BDA75500276956944AF7565D842B44E62C9C5 (void);
// 0x00000826 Amazon.Runtime.Internal.Util.CachingWrapperStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_WrappingStream()
extern void UnmarshallerContext_get_WrappingStream_mDD39993BF5ECDC711F69C9220A464260571C26DD (void);
// 0x00000827 System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::set_WrappingStream(Amazon.Runtime.Internal.Util.CachingWrapperStream)
extern void UnmarshallerContext_set_WrappingStream_mC885B682045B8C30D8083C7E76A987472AA8AA93 (void);
// 0x00000828 System.String Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_ResponseBody()
extern void UnmarshallerContext_get_ResponseBody_m4C477F3257EBA5FF72423F956F38E204D0435BD0 (void);
// 0x00000829 System.Byte[] Amazon.Runtime.Internal.Transform.UnmarshallerContext::GetResponseBodyBytes()
extern void UnmarshallerContext_GetResponseBodyBytes_m7AA3163269840F761D8DEC6A78CEA74C46C95DAE (void);
// 0x0000082A Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_ResponseData()
extern void UnmarshallerContext_get_ResponseData_m98279EBB176A30ADF40918880EB06C117D3EE41B (void);
// 0x0000082B System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::ValidateCRC32IfAvailable()
extern void UnmarshallerContext_ValidateCRC32IfAvailable_m053958C14D33CCACC19D2D652F69EE1E66A4337F (void);
// 0x0000082C System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::SetupCRCStream(Amazon.Runtime.Internal.Transform.IWebResponseData,System.IO.Stream,System.Int64)
extern void UnmarshallerContext_SetupCRCStream_m057BD25778592B090A9D103203DD25F94495C1DB (void);
// 0x0000082D System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String)
extern void UnmarshallerContext_TestExpression_m061116AF9393219F7251845A50ADEA8D0F5E6C38 (void);
// 0x0000082E System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.Int32)
extern void UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30 (void);
// 0x0000082F System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::ReadAtDepth(System.Int32)
extern void UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27 (void);
// 0x00000830 System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.String)
extern void UnmarshallerContext_TestExpression_m292897227E88BA17EFD6A33E58C0CB1FB0499A08 (void);
// 0x00000831 System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.Int32,System.String,System.Int32)
extern void UnmarshallerContext_TestExpression_m6CE0AA0A07F98BAF687E2EBD79351AAF973F8316 (void);
// 0x00000832 System.String Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentPath()
// 0x00000833 System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth()
// 0x00000834 System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read()
// 0x00000835 System.String Amazon.Runtime.Internal.Transform.UnmarshallerContext::ReadText()
// 0x00000836 System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartElement()
// 0x00000837 System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsEndElement()
// 0x00000838 System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_IsStartOfDocument()
// 0x00000839 System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::Dispose(System.Boolean)
extern void UnmarshallerContext_Dispose_m53267BEA8660CE432934A3A719023CD9A5FF971B (void);
// 0x0000083A System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::Dispose()
extern void UnmarshallerContext_Dispose_m58E88C15DE1FE4123B254D9ABEA02B976BEE18C3 (void);
// 0x0000083B System.Void Amazon.Runtime.Internal.Transform.UnmarshallerContext::.ctor()
extern void UnmarshallerContext__ctor_mF9E9D9E5E7F378B78A756BC0A906774C28E2F751 (void);
// 0x0000083C System.Xml.XmlReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_XmlReader()
extern void XmlUnmarshallerContext_get_XmlReader_m03B2DDDD8F9104D10ECA5FA556AAE569354872A1 (void);
// 0x0000083D System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::.ctor(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
extern void XmlUnmarshallerContext__ctor_mE871192CE85B0133EA0AD989939D662272DAB5C2 (void);
// 0x0000083E System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_CurrentPath()
extern void XmlUnmarshallerContext_get_CurrentPath_m86D70C3B648D6B1FDCE19DE622B9975BB9E1F5C5 (void);
// 0x0000083F System.Int32 Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_CurrentDepth()
extern void XmlUnmarshallerContext_get_CurrentDepth_mEBFD7E63555CA56328FAFDD3E27F494C363C15D2 (void);
// 0x00000840 System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::Read()
extern void XmlUnmarshallerContext_Read_mF1AD588F2B854E9FAE0D71277E3035B2FDF07A4B (void);
// 0x00000841 System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::ReadText()
extern void XmlUnmarshallerContext_ReadText_m90B0F0AE6F27DA057AE68C8E716D1A52ACBAC009 (void);
// 0x00000842 System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsStartElement()
extern void XmlUnmarshallerContext_get_IsStartElement_m393F61188E505F942E98C699DE20CA8BF45B8A8F (void);
// 0x00000843 System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsEndElement()
extern void XmlUnmarshallerContext_get_IsEndElement_m41D2018E60DFE15EB75CBB97D8A2D255A1F7EB4F (void);
// 0x00000844 System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsStartOfDocument()
extern void XmlUnmarshallerContext_get_IsStartOfDocument_mE1343FEB33D2C49D7E2BEAAAC126C7511CAFFBB2 (void);
// 0x00000845 System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::get_IsAttribute()
extern void XmlUnmarshallerContext_get_IsAttribute_mCD86E4DAE4DFAA8677AD77249C088FF0B8EF6BCA (void);
// 0x00000846 System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::StackToPath(System.Collections.Generic.Stack`1<System.String>)
extern void XmlUnmarshallerContext_StackToPath_m825552831BF4E3DA423E7AC3981C284DF481B1F2 (void);
// 0x00000847 System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::ReadElement()
extern void XmlUnmarshallerContext_ReadElement_m778FFE2A1CA1F480EB274622D260C9C522FA15B3 (void);
// 0x00000848 System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::Dispose(System.Boolean)
extern void XmlUnmarshallerContext_Dispose_m490F9FEE366AA657E4921757E9C9766478D91EB1 (void);
// 0x00000849 System.Void Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::.cctor()
extern void XmlUnmarshallerContext__cctor_mE1E47777AADC949921C51C9EA9937EDC1D13BAD7 (void);
// 0x0000084A System.Void Amazon.Runtime.Internal.Transform.HttpClientResponseData::.ctor(System.Net.Http.HttpResponseMessage,System.Net.Http.HttpClient,System.Boolean)
extern void HttpClientResponseData__ctor_m70D1079A5BAF5C501C4A2C9015831AB4603259C0 (void);
// 0x0000084B System.Net.HttpStatusCode Amazon.Runtime.Internal.Transform.HttpClientResponseData::get_StatusCode()
extern void HttpClientResponseData_get_StatusCode_mDA92BB2BEF3BA1BBDF1DB3D14295D34B898CA7B6 (void);
// 0x0000084C System.Void Amazon.Runtime.Internal.Transform.HttpClientResponseData::set_StatusCode(System.Net.HttpStatusCode)
extern void HttpClientResponseData_set_StatusCode_m038084D472C1D5D77EB3444D44C591043EE75522 (void);
// 0x0000084D System.Void Amazon.Runtime.Internal.Transform.HttpClientResponseData::set_IsSuccessStatusCode(System.Boolean)
extern void HttpClientResponseData_set_IsSuccessStatusCode_m8E9360B0084DA85637A85A11F1860D191E966E90 (void);
// 0x0000084E System.String Amazon.Runtime.Internal.Transform.HttpClientResponseData::get_ContentType()
extern void HttpClientResponseData_get_ContentType_m5CF5885F8847B1D6DAF0D87D1F98631F94D74F6E (void);
// 0x0000084F System.Void Amazon.Runtime.Internal.Transform.HttpClientResponseData::set_ContentType(System.String)
extern void HttpClientResponseData_set_ContentType_mF447D5D6B510AE8735E69A23753ED627F10F3A43 (void);
// 0x00000850 System.Int64 Amazon.Runtime.Internal.Transform.HttpClientResponseData::get_ContentLength()
extern void HttpClientResponseData_get_ContentLength_mA95E15F1F79F452C9B09FCC33C8A1AA54D9393F2 (void);
// 0x00000851 System.Void Amazon.Runtime.Internal.Transform.HttpClientResponseData::set_ContentLength(System.Int64)
extern void HttpClientResponseData_set_ContentLength_m769C7803F8FCABAA68F2F8820AE64E7EEC74922F (void);
// 0x00000852 System.String Amazon.Runtime.Internal.Transform.HttpClientResponseData::GetHeaderValue(System.String)
extern void HttpClientResponseData_GetHeaderValue_mB379D5FE88959D0DC741AA435AF6FFA17FE9CEC4 (void);
// 0x00000853 System.Boolean Amazon.Runtime.Internal.Transform.HttpClientResponseData::IsHeaderPresent(System.String)
extern void HttpClientResponseData_IsHeaderPresent_m30F357B63026706FE4608F6CF0ACF0DE61CEFD97 (void);
// 0x00000854 System.String[] Amazon.Runtime.Internal.Transform.HttpClientResponseData::GetHeaderNames()
extern void HttpClientResponseData_GetHeaderNames_m81B95FF8126FE160F31D3514138294357010D98D (void);
// 0x00000855 System.Void Amazon.Runtime.Internal.Transform.HttpClientResponseData::CopyHeaderValues(System.Net.Http.HttpResponseMessage)
extern void HttpClientResponseData_CopyHeaderValues_m27841F08DF2C9CE4637A42DE926221E55ACDC6FE (void);
// 0x00000856 System.String Amazon.Runtime.Internal.Transform.HttpClientResponseData::GetFirstHeaderValue(System.Net.Http.Headers.HttpHeaders,System.String)
extern void HttpClientResponseData_GetFirstHeaderValue_m144A7F4F5B4FAA39BE07FF562E83B4B8B86417A5 (void);
// 0x00000857 Amazon.Runtime.Internal.Transform.IHttpResponseBody Amazon.Runtime.Internal.Transform.HttpClientResponseData::get_ResponseBody()
extern void HttpClientResponseData_get_ResponseBody_m0F98A1C5A48B89DB121803B07E26B56642E7AA4E (void);
// 0x00000858 System.Void Amazon.Runtime.Internal.Transform.HttpResponseMessageBody::.ctor(System.Net.Http.HttpResponseMessage,System.Net.Http.HttpClient,System.Boolean)
extern void HttpResponseMessageBody__ctor_mAFA22E937E2989AA503DFB611BD3588B2C2B01DE (void);
// 0x00000859 System.IO.Stream Amazon.Runtime.Internal.Transform.HttpResponseMessageBody::OpenResponse()
extern void HttpResponseMessageBody_OpenResponse_mEAACBBE0E59A9905C409584E70A75AA9DCD10E5A (void);
// 0x0000085A System.Threading.Tasks.Task`1<System.IO.Stream> Amazon.Runtime.Internal.Transform.HttpResponseMessageBody::OpenResponseAsync()
extern void HttpResponseMessageBody_OpenResponseAsync_mD4D13F06B794ED60E9374FA5048FFBB7CA62DB3B (void);
// 0x0000085B System.Void Amazon.Runtime.Internal.Transform.HttpResponseMessageBody::Dispose()
extern void HttpResponseMessageBody_Dispose_m4473323B5C04E2F05D56A4AF9B6988801E94B176 (void);
// 0x0000085C System.Void Amazon.Runtime.Internal.Transform.HttpResponseMessageBody::Dispose(System.Boolean)
extern void HttpResponseMessageBody_Dispose_m49BBB2DE145533EB0EC1E19DEA4F201E22F8DE75 (void);
// 0x0000085D Amazon.Runtime.Internal.Settings.SettingsCollection Amazon.Runtime.Internal.Settings.IPersistenceManager::GetSettings(System.String)
// 0x0000085E Amazon.Runtime.Internal.Settings.SettingsCollection Amazon.Runtime.Internal.Settings.InMemoryPersistenceManager::GetSettings(System.String)
extern void InMemoryPersistenceManager_GetSettings_m0A9EDF687A4603B639FE692372F828455EB7009B (void);
// 0x0000085F System.Void Amazon.Runtime.Internal.Settings.InMemoryPersistenceManager::.ctor()
extern void InMemoryPersistenceManager__ctor_m9C1B7BC7E6061EF292169262D92C7C06FCCC3BA3 (void);
// 0x00000860 System.Void Amazon.Runtime.Internal.Settings.PersistenceManager::.cctor()
extern void PersistenceManager__cctor_m65BE274E472710BF3356BC83A45FEA8C3FCD4919 (void);
// 0x00000861 Amazon.Runtime.Internal.Settings.IPersistenceManager Amazon.Runtime.Internal.Settings.PersistenceManager::get_Instance()
extern void PersistenceManager_get_Instance_m184BC55A18A7E106F5F8FB191DDF160960FD3BE8 (void);
// 0x00000862 System.Void Amazon.Runtime.Internal.Settings.PersistenceManager::set_Instance(Amazon.Runtime.Internal.Settings.IPersistenceManager)
extern void PersistenceManager_set_Instance_m4334BD1CAF6918F9F6B8443B13FDD70D646E9B86 (void);
// 0x00000863 Amazon.Runtime.Internal.Settings.SettingsCollection Amazon.Runtime.Internal.Settings.PersistenceManager::GetSettings(System.String)
extern void PersistenceManager_GetSettings_mADE2264473AC6B47E73EB7E2B89C7A7DBC90BDE5 (void);
// 0x00000864 System.String Amazon.Runtime.Internal.Settings.PersistenceManager::GetSettingsStoreFolder()
extern void PersistenceManager_GetSettingsStoreFolder_m10A8A8AC242F6BA8A03677E69D856B7CF042AACC (void);
// 0x00000865 System.Boolean Amazon.Runtime.Internal.Settings.PersistenceManager::IsEncrypted(System.String)
extern void PersistenceManager_IsEncrypted_mB5B4EB5FC8FF92A1900355A64E10257F26891378 (void);
// 0x00000866 Amazon.Runtime.Internal.Settings.SettingsCollection Amazon.Runtime.Internal.Settings.PersistenceManager::loadSettingsType(System.String)
extern void PersistenceManager_loadSettingsType_mB0A7778060070487DBD29E5D331B69619929EDFF (void);
// 0x00000867 System.Void Amazon.Runtime.Internal.Settings.PersistenceManager::DecryptAnyEncryptedValues(System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void PersistenceManager_DecryptAnyEncryptedValues_mD66BD639BF73CCA5036C5BCF21DC34F9980EA7D4 (void);
// 0x00000868 System.String Amazon.Runtime.Internal.Settings.PersistenceManager::getFileFromType(System.String)
extern void PersistenceManager_getFileFromType_mFCB6A8D4674474F1AF1786818D1203CCD4F9CC48 (void);
// 0x00000869 System.Void Amazon.Runtime.Internal.Settings.PersistenceManager::.ctor()
extern void PersistenceManager__ctor_m21BB963082E733E352874993761BE7E314F94410 (void);
// 0x0000086A System.Void Amazon.Runtime.Internal.Settings.SettingsWatcher::Dispose()
extern void SettingsWatcher_Dispose_mE2877D6B7793815C65186B321D308898745B2AF1 (void);
// 0x0000086B System.Void Amazon.Runtime.Internal.Settings.SettingsWatcher::Dispose(System.Boolean)
extern void SettingsWatcher_Dispose_m3F7E7B14B903619166E544FF67CFC29892817533 (void);
// 0x0000086C System.Void Amazon.Runtime.Internal.Settings.SettingsCollection::.ctor()
extern void SettingsCollection__ctor_m80B01617D4A55C6AA7632F2C1DBC849432FC2D37 (void);
// 0x0000086D System.Void Amazon.Runtime.Internal.Settings.SettingsCollection::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void SettingsCollection__ctor_mB987F94277CFF117F7DEC3601C5912A3870538B6 (void);
// 0x0000086E System.Void Amazon.Runtime.Internal.Settings.SettingsCollection::set_InitializedEmpty(System.Boolean)
extern void SettingsCollection_set_InitializedEmpty_mE31FA906B0A88CA7AB7AAF997C4D9D6ABAD672EF (void);
// 0x0000086F System.Collections.IEnumerator Amazon.Runtime.Internal.Settings.SettingsCollection::System.Collections.IEnumerable.GetEnumerator()
extern void SettingsCollection_System_Collections_IEnumerable_GetEnumerator_mB7A549466820D744B5DD4DC19B38BBBF96A61393 (void);
// 0x00000870 System.Collections.Generic.IEnumerator`1<Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings> Amazon.Runtime.Internal.Settings.SettingsCollection::GetEnumerator()
extern void SettingsCollection_GetEnumerator_m79DFE4C6FF170762974BF7E5745417DB8AEB56A5 (void);
// 0x00000871 Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings Amazon.Runtime.Internal.Settings.SettingsCollection::get_Item(System.String)
extern void SettingsCollection_get_Item_m4E0FA5D22AE65F8F37E22727191391B0C289B7EE (void);
// 0x00000872 Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings Amazon.Runtime.Internal.Settings.SettingsCollection::NewObjectSettings(System.String)
extern void SettingsCollection_NewObjectSettings_m490A281213F84C156B48634771EBF5E39469952D (void);
// 0x00000873 System.Void Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings::.ctor(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ObjectSettings__ctor_m64E581AC49461AC737806745A5BDF50E3EBC140E (void);
// 0x00000874 System.String Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings::get_UniqueKey()
extern void ObjectSettings_get_UniqueKey_m7312E8A6A47D6501DA59DB1672904C8AC1525486 (void);
// 0x00000875 System.String Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings::get_Item(System.String)
extern void ObjectSettings_get_Item_mCBCF9DAC8B93B6ED105FBABDEABAA35FA4BE63E5 (void);
// 0x00000876 System.Collections.Generic.IEnumerable`1<System.String> Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings::get_Keys()
extern void ObjectSettings_get_Keys_mE37A1CFE62D1DD3FC39302192558164102592CBC (void);
// 0x00000877 System.Void Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__11__ctor_m20153A22BB2F12B09B9DC391189542CFB33D1A22 (void);
// 0x00000878 System.Void Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__11_System_IDisposable_Dispose_m8FCA25311749C595C065FAA5F91BEBAA1E6D5426 (void);
// 0x00000879 System.Boolean Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::MoveNext()
extern void U3CGetEnumeratorU3Ed__11_MoveNext_m8CE3EF6E9500E047253C118EFC818CA38966BC7E (void);
// 0x0000087A System.Void Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__11_U3CU3Em__Finally1_m9250318E741B953C390D2EBA04C35B301C30380E (void);
// 0x0000087B Amazon.Runtime.Internal.Settings.SettingsCollection/ObjectSettings Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::System.Collections.Generic.IEnumerator<Amazon.Runtime.Internal.Settings.SettingsCollection.ObjectSettings>.get_Current()
extern void U3CGetEnumeratorU3Ed__11_System_Collections_Generic_IEnumeratorU3CAmazon_Runtime_Internal_Settings_SettingsCollection_ObjectSettingsU3E_get_Current_m0522738F60385992B2C0E82EB07FDC81D1441B9A (void);
// 0x0000087C System.Void Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_Reset_m44794CDDC5D62355A67F666A9C81997DBAFF01FE (void);
// 0x0000087D System.Object Amazon.Runtime.Internal.Settings.SettingsCollection/<GetEnumerator>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_get_Current_m40917AA2601E7ECA679D9521E6E2079FDD54364E (void);
// 0x0000087E System.String Amazon.Runtime.Internal.Settings.UserCrypto::Decrypt(System.String)
extern void UserCrypto_Decrypt_m32BE52AC9DDF6ABF3898F170DDB4F815D3847716 (void);
// 0x0000087F System.String Amazon.Runtime.Internal.Settings.UserCrypto::Encrypt(System.String)
extern void UserCrypto_Encrypt_mC46091927A279E4177694DE1DCA6D4F296687CFF (void);
// 0x00000880 Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB Amazon.Runtime.Internal.Settings.UserCrypto::ConvertData(System.Byte[])
extern void UserCrypto_ConvertData_mF2E0CCBE456719C807A83B691C4AF00B5EBEC09D (void);
// 0x00000881 System.Boolean Amazon.Runtime.Internal.Settings.UserCrypto::CryptProtectData(Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB&,System.String,Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB&,System.IntPtr,Amazon.Runtime.Internal.Settings.UserCrypto/CRYPTPROTECT_PROMPTSTRUCT&,Amazon.Runtime.Internal.Settings.UserCrypto/CryptProtectFlags,Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB&)
extern void UserCrypto_CryptProtectData_m35CB475CFC28355EB1579A94059F23605201705E (void);
// 0x00000882 System.Boolean Amazon.Runtime.Internal.Settings.UserCrypto::CryptUnprotectData(Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB&,System.String,Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB&,System.IntPtr,Amazon.Runtime.Internal.Settings.UserCrypto/CRYPTPROTECT_PROMPTSTRUCT&,Amazon.Runtime.Internal.Settings.UserCrypto/CryptProtectFlags,Amazon.Runtime.Internal.Settings.UserCrypto/DATA_BLOB&)
extern void UserCrypto_CryptUnprotectData_m6FFA94F14F374B3A4443A916E64015A74707CF1B (void);
// 0x00000883 System.Boolean Amazon.Runtime.Internal.Settings.UserCrypto::get_IsUserCryptAvailable()
extern void UserCrypto_get_IsUserCryptAvailable_m7FB6CC4F64076F9FFDBD5178664E28CBD9EC77B9 (void);
// 0x00000884 System.Void Amazon.Runtime.Internal.Auth.AbstractAWSSigner::Sign(Amazon.Runtime.Internal.IRequest,Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.RequestMetrics,System.String,System.String)
// 0x00000885 Amazon.Runtime.Internal.Auth.ClientProtocol Amazon.Runtime.Internal.Auth.AbstractAWSSigner::get_Protocol()
// 0x00000886 System.Void Amazon.Runtime.Internal.Auth.AbstractAWSSigner::.ctor()
extern void AbstractAWSSigner__ctor_m2210CF67382A7A9F1D5C75C86CF365ADBE9D9C92 (void);
// 0x00000887 System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::.ctor()
extern void AWS4Signer__ctor_m0C5E9E6AFF7BD86B722135DDE17D770894463306 (void);
// 0x00000888 System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::.ctor(System.Boolean)
extern void AWS4Signer__ctor_mB5A14B9F33AE20576AB3316061E330EB1B2322E7 (void);
// 0x00000889 System.Boolean Amazon.Runtime.Internal.Auth.AWS4Signer::get_SignPayload()
extern void AWS4Signer_get_SignPayload_mE907B2FB3EF750B065F1C0D454816D9BDCC0A693 (void);
// 0x0000088A System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::set_SignPayload(System.Boolean)
extern void AWS4Signer_set_SignPayload_mB99439C13DC95FE5F3037223F4D2F0D955C193D5 (void);
// 0x0000088B Amazon.Runtime.Internal.Auth.ClientProtocol Amazon.Runtime.Internal.Auth.AWS4Signer::get_Protocol()
extern void AWS4Signer_get_Protocol_mADADEAD747FBE8D28B7EA1851CC567E43BA2B2DB (void);
// 0x0000088C System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::Sign(Amazon.Runtime.Internal.IRequest,Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.RequestMetrics,System.String,System.String)
extern void AWS4Signer_Sign_mAB14CA3C9B5EB5DEE4F5EE13A8E54C024D4997B7 (void);
// 0x0000088D Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Auth.AWS4Signer::SignRequest(Amazon.Runtime.Internal.IRequest,Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.RequestMetrics,System.String,System.String)
extern void AWS4Signer_SignRequest_m133716014A6F4073249471BA0DCBE0D3C486EAD3 (void);
// 0x0000088E System.DateTime Amazon.Runtime.Internal.Auth.AWS4Signer::InitializeHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Uri)
extern void AWS4Signer_InitializeHeaders_m2ECE67FD638341D8CB887C8CB788963E24DEC6DB (void);
// 0x0000088F System.DateTime Amazon.Runtime.Internal.Auth.AWS4Signer::InitializeHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Uri,System.DateTime)
extern void AWS4Signer_InitializeHeaders_mE4D30F98F4D92204D3AE3F3FA531A25984448303 (void);
// 0x00000890 System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::CleanHeaders(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AWS4Signer_CleanHeaders_m621F2FEBB2D608A1F419B6BAA27FA5CAC12D8DE0 (void);
// 0x00000891 Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeSignature(System.String,System.String,System.String,System.DateTime,System.String,System.String,System.String,Amazon.Runtime.Internal.Util.RequestMetrics)
extern void AWS4Signer_ComputeSignature_m0D5230C6205966C4476E807D48BFD87A0DF12D0E (void);
// 0x00000892 System.String Amazon.Runtime.Internal.Auth.AWS4Signer::FormatDateTime(System.DateTime,System.String)
extern void AWS4Signer_FormatDateTime_m3253B96360CEF42884B44308257942E669E834FD (void);
// 0x00000893 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComposeSigningKey(System.String,System.String,System.String,System.String)
extern void AWS4Signer_ComposeSigningKey_mB4F4CA9462AE352BBD51996BFD5C9A95092E735D (void);
// 0x00000894 System.String Amazon.Runtime.Internal.Auth.AWS4Signer::SetRequestBodyHash(Amazon.Runtime.Internal.IRequest,System.Boolean)
extern void AWS4Signer_SetRequestBodyHash_mDCA209DF67E384E30712AD61F28E04E93188B9E8 (void);
// 0x00000895 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::SignBlob(System.Byte[],System.String)
extern void AWS4Signer_SignBlob_m1346F7E2FDA3E19350BFE45B8BBC2C2CAFCA1059 (void);
// 0x00000896 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::SignBlob(System.Byte[],System.Byte[])
extern void AWS4Signer_SignBlob_m5849C72A3FA29A9571F3800C320CC296F5940969 (void);
// 0x00000897 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeKeyedHash(Amazon.Runtime.SigningAlgorithm,System.Byte[],System.String)
extern void AWS4Signer_ComputeKeyedHash_mCEFCC216272392E2C12FC28A5097592B5082FBCB (void);
// 0x00000898 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeKeyedHash(Amazon.Runtime.SigningAlgorithm,System.Byte[],System.Byte[])
extern void AWS4Signer_ComputeKeyedHash_m0CE367A1DABB20F7D887AF6B8937FE8AF70B5836 (void);
// 0x00000899 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeHash(System.String)
extern void AWS4Signer_ComputeHash_mD750A476074BFCF97AB830B5F6F454D7CD64F0BA (void);
// 0x0000089A System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::ComputeHash(System.Byte[])
extern void AWS4Signer_ComputeHash_mEE5A670DD54202F59E4C11A8D13FE5A05C82A10F (void);
// 0x0000089B System.String Amazon.Runtime.Internal.Auth.AWS4Signer::SetPayloadSignatureHeader(Amazon.Runtime.Internal.IRequest,System.String)
extern void AWS4Signer_SetPayloadSignatureHeader_m204DC176FAA0329FA19399BF3F464C88CBBCF31E (void);
// 0x0000089C System.String Amazon.Runtime.Internal.Auth.AWS4Signer::DetermineSigningRegion(Amazon.Runtime.IClientConfig,System.String,Amazon.RegionEndpoint,Amazon.Runtime.Internal.IRequest)
extern void AWS4Signer_DetermineSigningRegion_mB91334EE400FFF7C9598BD0872F4EBD12E9927A4 (void);
// 0x0000089D System.String Amazon.Runtime.Internal.Auth.AWS4Signer::DetermineService(Amazon.Runtime.IClientConfig)
extern void AWS4Signer_DetermineService_m90EAE7C10D4692EF4EA2BE101D05F42D54DB1DC5 (void);
// 0x0000089E System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeRequest(System.Uri,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Int32,System.String)
extern void AWS4Signer_CanonicalizeRequest_mCC536CD188B1C31DC34FAFDB51ADA25B97312000 (void);
// 0x0000089F System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeRequestHelper(System.Uri,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Int32,System.Boolean)
extern void AWS4Signer_CanonicalizeRequestHelper_m610FAD60AC36201DD6CC220ACAB3A808EA002868 (void);
// 0x000008A0 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer::SortAndPruneHeaders(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void AWS4Signer_SortAndPruneHeaders_m218F9F6BB7593BECC8CD2EC991F9E1DC45C397F0 (void);
// 0x000008A1 System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeHeaders(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void AWS4Signer_CanonicalizeHeaders_m751B2C6AE63BEB2FCE83CA27D3B32EB350EE5138 (void);
// 0x000008A2 System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeHeaderNames(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void AWS4Signer_CanonicalizeHeaderNames_m6B7BC32463DEE92D4383E27AA67DAE18246669F2 (void);
// 0x000008A3 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Amazon.Runtime.Internal.Auth.AWS4Signer::GetParametersToCanonicalize(Amazon.Runtime.Internal.IRequest)
extern void AWS4Signer_GetParametersToCanonicalize_mBE7D5D774177E1698AF3A0378DEA68D1E0C57E51 (void);
// 0x000008A4 System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeQueryParameters(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void AWS4Signer_CanonicalizeQueryParameters_mC50DE6A1854F63B9424354562394F80CA8501DDA (void);
// 0x000008A5 System.String Amazon.Runtime.Internal.Auth.AWS4Signer::CanonicalizeQueryParameters(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Boolean)
extern void AWS4Signer_CanonicalizeQueryParameters_mAD5B409CC39C81926146D09E28CE760DF2900F3F (void);
// 0x000008A6 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::GetRequestPayloadBytes(Amazon.Runtime.Internal.IRequest)
extern void AWS4Signer_GetRequestPayloadBytes_m835734FA621A2FE85C16696C12DB75F2F3DC4B42 (void);
// 0x000008A7 System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::.cctor()
extern void AWS4Signer__cctor_m58ECCDA2CA525308010ECD7D1DC2D626600285C9 (void);
// 0x000008A8 System.Void Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::.cctor()
extern void U3CU3Ec__cctor_mDA2383FAEC93E0DACEF31612513C2372B6076AAD (void);
// 0x000008A9 System.Void Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::.ctor()
extern void U3CU3Ec__ctor_mE156446C8A54E91CFE285947D1BE869D86B59E7B (void);
// 0x000008AA System.Boolean Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<GetParametersToCanonicalize>b__50_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CGetParametersToCanonicalizeU3Eb__50_0_m3103F2F8DEE7B6CF889319F471577907D912D8E0 (void);
// 0x000008AB System.String Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<CanonicalizeQueryParameters>b__54_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CCanonicalizeQueryParametersU3Eb__54_0_m0F9B35205A926697DCCEC05155EA51C6D8C3018E (void);
// 0x000008AC System.Void Amazon.Runtime.Internal.Auth.AWS4SigningResult::.ctor(System.String,System.DateTime,System.String,System.String,System.Byte[],System.Byte[])
extern void AWS4SigningResult__ctor_m589E62BD0D1CA8CED575FA89660FCB820A796B9C (void);
// 0x000008AD System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_AccessKeyId()
extern void AWS4SigningResult_get_AccessKeyId_m7D1FC51228C1FB5AEBC1B3A2D44BE5DA4108BE37 (void);
// 0x000008AE System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_ISO8601DateTime()
extern void AWS4SigningResult_get_ISO8601DateTime_mCD7F3CB9F3646FDAF64FA1078C29C2EE944262F9 (void);
// 0x000008AF System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_SignedHeaders()
extern void AWS4SigningResult_get_SignedHeaders_m1A77F6BD2F9360AC70122D8680423AD6A43671C0 (void);
// 0x000008B0 System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_Scope()
extern void AWS4SigningResult_get_Scope_m139C820A9C203453E3A33F98367EE15A7A4FAF45 (void);
// 0x000008B1 System.Byte[] Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_SigningKey()
extern void AWS4SigningResult_get_SigningKey_mE3C2F29839A800C58452767C6D1C45789D201376 (void);
// 0x000008B2 System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_Signature()
extern void AWS4SigningResult_get_Signature_m0F9BD6BEEB495639E8BA55C56F72A02B4D405CD6 (void);
// 0x000008B3 System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::get_ForAuthorizationHeader()
extern void AWS4SigningResult_get_ForAuthorizationHeader_m4CCC89C7EDF649064288A3B9D1221030DC39A101 (void);
// 0x000008B4 System.Void Amazon.Runtime.Internal.Auth.NullSigner::Sign(Amazon.Runtime.Internal.IRequest,Amazon.Runtime.IClientConfig,Amazon.Runtime.Internal.Util.RequestMetrics,System.String,System.String)
extern void NullSigner_Sign_mCED14E4BB9CC1A74C922E9DDFC01F55141B3B1E2 (void);
// 0x000008B5 Amazon.Runtime.Internal.Auth.ClientProtocol Amazon.Runtime.Internal.Auth.NullSigner::get_Protocol()
extern void NullSigner_get_Protocol_m06A447548CA02CA5B0968C4D0EE762403282DBF2 (void);
// 0x000008B6 System.Void Amazon.Runtime.Internal.Auth.NullSigner::.ctor()
extern void NullSigner__ctor_mEBD00FDBFBE3195CF706179F210A298542536FC7 (void);
static Il2CppMethodPointer s_methodPointers[2230] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonData_get_Count_m5B349CDFCFD373FB85577CB3DFEAD54422E43042,
	JsonData_get_IsArray_mB15F4E57CDC2823E471956AD207704304FA9FD9D,
	JsonData_get_IsBoolean_m6E007BC3AF652BD6F196494BFD8C16803E2AF288,
	JsonData_get_IsDouble_m718DDF5CC28F6E72E6D234298A4565E6032D73D8,
	JsonData_get_IsInt_mE316BCEFE761DE5CC69038B56009225E9BC73CDF,
	JsonData_get_IsUInt_m838B99A056D41CC5660B027657AC320C45750E1A,
	JsonData_get_IsLong_m209FC856C8C0A209FC149163EA9A923A1CA90ED4,
	JsonData_get_IsULong_mA9A1708684D8C1DB8948F7E44251718944656471,
	JsonData_get_IsObject_m117659025DFF9D30DC22607E67EEF30B7CDE1EF2,
	JsonData_get_IsString_mC71B7AE55DE68C51CABA61D97B9617AE44863C39,
	JsonData_System_Collections_ICollection_get_Count_m4B402678A0F6309D854747B85EA4F70FA9F153DD,
	JsonData_System_Collections_ICollection_get_SyncRoot_m29958F93B563136E6B8107B5957D27A60BBABF13,
	JsonData_System_Collections_IDictionary_get_IsReadOnly_m74E2910DDBA5DCA86E977649B2FC3C76597EA82F,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsArray_m157F98DC49902F21B50ADF20D0C4BED8098B4A00,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsBoolean_m584B9B491726979D49B14EBA96EFFB6E82844A78,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsDouble_mEFC058F68112A0F8D5959FDF31355A3E9CE47EBA,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsInt_m28110FB4E6676FFE327BF00DF4A6A10FEB287873,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsLong_m1D6A78079FB3C5985F49148DA69CC750FD6FFE10,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsObject_m6A96C3003B244304E86C20DA0B65FE0C5D890516,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_get_IsString_mCA097127A6C0D830D951263D295433368207BD3B,
	JsonData_System_Collections_IList_get_IsFixedSize_m093565897EC7BFDD7FB30C8AEE7F816939FF6EE2,
	JsonData_System_Collections_IList_get_IsReadOnly_mF028CD017F1EDA7DB8F03C4776D1D24A143AC6A1,
	JsonData_System_Collections_IDictionary_get_Item_m607F6D46E5E008D272025B7E4EB6701343433C72,
	JsonData_System_Collections_IDictionary_set_Item_mD4EEE468506982400D5B255908C098A5ECC7B19C,
	JsonData_System_Collections_IList_get_Item_m5282DB28977F405139B02A60EF8056E5C701A111,
	JsonData_System_Collections_IList_set_Item_mC710F8573D068F03EC06DA5875CF72D429ECDFA1,
	JsonData_get_PropertyNames_m09BE0C5184CA1FC5FF1F490CDEC2BEF83549A409,
	JsonData_get_Item_m9C7A7EA98ADC9F268DFB852FF55A2E99B842589C,
	JsonData_set_Item_mCE9E32482CB8C6BD05EBC1A3E197CCDF61C97E28,
	JsonData_get_Item_m45ECDD78142439A435485C0E217C60E6E093D5F8,
	JsonData_set_Item_m54A112315FBD6DB33CACA7B4DA83C0066D65F0CC,
	JsonData__ctor_m9480744DC59E7B5979FCC0DE5B39DFB9D6736275,
	JsonData__ctor_m272D41CC53EFC98B45A6AE6C800B318072B31834,
	JsonData_op_Explicit_mA62D8090A1F62CC82EF7D8512F7BE5B85210677B,
	JsonData_op_Explicit_m7B9165F06DA967CDFC166F072B19AB6C366E40DC,
	JsonData_System_Collections_ICollection_CopyTo_mBC470FD58CD453B47C3D40F4C1E3926820745DE2,
	JsonData_System_Collections_IDictionary_Add_m70FC6F0BB7DC07F37B44832506446A1B38A60762,
	JsonData_System_Collections_IDictionary_Contains_m760C9188C9DDF16FB97CADE5B36646733E4AC0CD,
	JsonData_System_Collections_IDictionary_GetEnumerator_m243920C141BAF13032866850513A10C750DDCE42,
	JsonData_System_Collections_IDictionary_Remove_mD13C0D7D9B7B3AF2BAEE42623585ED7CA63AC333,
	JsonData_System_Collections_IEnumerable_GetEnumerator_m5BC9AE38294467EFE94F5CCF294083603D7AC37F,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetBoolean_mB527C13EC7DCDF31DBCFB98E5E25409C52B43AAC,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetDouble_mB0DBC1F6649FEC71E5076F61966F28C7FB4A75E5,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetInt_m2818C0E556B1E3ED27D4DC3AFA1676FAE6E50AD4,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetUInt_mDA657A0459BE5F64B62FF72380882F4B2FEF30C8,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetLong_mF755BECBBC91B97BE46F130C067FA5A707245456,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetULong_m4388950E0CF3FF380CE2982504B035702E054D2B,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_GetString_m45104EDF1155E0AD001A68E8B7D8F6FFF7E75AE6,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetBoolean_m3978CCB92DF739730E6113ECB4428D63201C5A02,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetDouble_m09111419939071696F84C59B078D3662A61F053E,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetInt_m8C4573E2273D4ABC8F6438622C00D3879AB24708,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetUInt_m46A7674ADE879D3C49143A30F23BE877A541527B,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetLong_m9F400D26662373B6F2749C38A50B02ECDE586D74,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetULong_m1DA4B31B56992FDABB274DE1B9DD4B6E663C43A8,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_SetString_mFB2C16BA27AB3FCB449D291570943E50351C4863,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_ToJson_m7722E814127A81C083284F255AC0901F4BBB99CC,
	JsonData_ThirdParty_Json_LitJson_IJsonWrapper_ToJson_m72E2521D98EBB0E040C8DD49C1828E25F5A3BB1C,
	JsonData_System_Collections_IList_Add_m69528521024C2B26C8F6221EA8B7ED0D8FD75689,
	JsonData_System_Collections_IList_Clear_mDE1395499A2AC620E57E51722B115A8DD226F809,
	JsonData_System_Collections_IList_Contains_mB1EA78A6C254C165BA57DA0C075D92F82C01814D,
	JsonData_System_Collections_IList_IndexOf_m7DE1118576B5F070255E48C2526FA67836C74754,
	JsonData_System_Collections_IList_Insert_m7C3B667A028660BE7F988E0C4E6EE0BD0D319E76,
	JsonData_System_Collections_IList_Remove_m439F909368DEBAB46656BAC399047A897DC2275D,
	JsonData_System_Collections_IList_RemoveAt_m990800A90EF3544AF48C3F48A9E3788AE6AB1B82,
	JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m6234C6A9A4078ECE3E98AD9B18A00387B824288C,
	JsonData_EnsureCollection_mF9B11BD9275C219319FCFC919365B20691A22BCE,
	JsonData_EnsureDictionary_m0291DB0FC557B753A82895BED3D476573C223564,
	JsonData_EnsureList_mFCAC833A43D088674F27CBF77CADD823479F5782,
	JsonData_ToJsonData_mE2B2DAC7889552093592C335DBBA06C2CCD37925,
	JsonData_WriteJson_mF8A773CCBA4C3B33F0D979AECCEA2975C9CF2D8D,
	JsonData_Add_m6476490E580AE074857ECB17A069D446EF375BE7,
	JsonData_Equals_m233AFBBE2C058A3CC450788A5E27149E845FA188,
	JsonData_SetJsonType_m1B896834F17F4B3155C12211ED44672B3064D6E8,
	JsonData_ToJson_m33A3B263A33DA951B6E9809668FD9466AFFAA90C,
	JsonData_ToJson_m88BDAA865DD294DE2D92460B4873BBBCC6C9453D,
	JsonData_ToString_m8B05D69F04E418FC6E16EB311BA003351006799D,
	OrderedDictionaryEnumerator_get_Current_m967C64BED8A3372739BC5AFD25C99D752DE0D471,
	OrderedDictionaryEnumerator_get_Entry_mDAEE20FB3EF8AE1534C84F550C4E58BC24AD6ADD,
	OrderedDictionaryEnumerator_get_Key_mC31237F59ACB35F27E5E6447ED23E50413A0D297,
	OrderedDictionaryEnumerator_get_Value_m27B7CA61102DA9F947996A2013F9F3FA32B5E12B,
	OrderedDictionaryEnumerator__ctor_mDF82B39A521C648731F59BACD47BC90DFD91B82D,
	OrderedDictionaryEnumerator_MoveNext_mB19085F74D36FCAA3FA2136D2D468D426D892CBF,
	OrderedDictionaryEnumerator_Reset_m5BD38326A9877C3D2C886E431F6CDD2D6556DC4C,
	JsonException__ctor_m1B6ECFF6D071C36302819E4B46AE304C986C3CE7,
	JsonException__ctor_m0A0A59127FA0DCA1A377D3FC9CEB3ED14FB82CAE,
	ArrayMetadata_get_ElementType_mA0F8D763181E8C7897701F98499C20EC872E36E4,
	ArrayMetadata_set_ElementType_m0165398A5F11604DE1D2C59516C3E6C66C92D395,
	ArrayMetadata_get_IsArray_m9D939595CE680B7C8BC4B5EF5665022753A7E879,
	ArrayMetadata_set_IsArray_m1CD739BA6B4D869DED37B2CCBBBB2901BC068997,
	ArrayMetadata_get_IsList_m73EFF0512BB93D42859059FF0B98E9A4C26AB234,
	ArrayMetadata_set_IsList_m05E0BFF65E6B91FCB52EF51354B7A492A331C1F9,
	ObjectMetadata_get_ElementType_m4B736F26EDD2D73390884D9E93ACEF0588C0CEBC,
	ObjectMetadata_set_ElementType_m133FC236AEB7BD106D60E0BF1A46EB330D1A8157,
	ObjectMetadata_get_IsDictionary_m3D80DEBFB702D2C75364ABB5A9AEA71970F41DD9,
	ObjectMetadata_set_IsDictionary_m1D62DE45F5999CB46B0BABD7D44DED6A3E2B2DD7,
	ObjectMetadata_get_Properties_m9B4BE4987F4D9F01BD098A41645696DDD46C6A20,
	ObjectMetadata_set_Properties_mF467235BB451CB7F69B6FCFD8344EF01082D14B3,
	ExporterFunc__ctor_m262977CFB142C800F94C995354D5876B1B435960,
	ExporterFunc_Invoke_m7B91598C9CD54A29CE50C39EF8A2F88269425C7A,
	ExporterFunc_BeginInvoke_mD15F2FC001C3A4E0BD51A2117A29674756A4006D,
	ExporterFunc_EndInvoke_mBF01B06C5282AE2CCF9A916779107A50391FBBD2,
	ImporterFunc__ctor_m3FFC0BAA62E0AC747210CC0CF45449C0BECA6D36,
	ImporterFunc_Invoke_m11921490DF88C5298FA40E1B7C05D5694979504C,
	ImporterFunc_BeginInvoke_mDF97AE632BF9CFB3569786E87B42231DB392EA34,
	ImporterFunc_EndInvoke_m7345F8726BF2933CE6892FD790C43DFEAC547EAE,
	WrapperFactory__ctor_mD9AFC7DCBE92B0654B8AC7C89DD8773FB1257050,
	WrapperFactory_Invoke_m6C6184BEFDB603B8F1A5FBBBEDC10F89E21C941E,
	WrapperFactory_BeginInvoke_mF33E0466C3C2D83B24E106CD7C637041AFB3B79E,
	WrapperFactory_EndInvoke_m0B1EFB30BEDF1341D8B51B0B2C2A3DD29543BB5E,
	JsonMapper__cctor_m6DF38B88E837ABEFA4EBE2E6397F2211F82C6014,
	JsonMapper_AddArrayMetadata_mFC7141AB2B325123BA04E9F505FF1E6034B13BF4,
	JsonMapper_AddObjectMetadata_m91E003CBE9FA306F04CAA431804009395DAB8406,
	JsonMapper_AddTypeProperties_mDCF0A375DCD37DC89338042D41595D19FE74C1C1,
	JsonMapper_GetConvOp_mB5A6B80BB5F4650A18999FED99A8BBC669584E4F,
	JsonMapper_ReadValue_m74BC26C2D550CB99FEA1F31E9FD91E1869045E0A,
	JsonMapper_ValidateRequiredFields_m10A500DEA597284D4450409B3233786DF939B049,
	JsonMapper_ReadValue_m313D23BAAD03233EDC6B5751D458257D9ED9346F,
	JsonMapper_RegisterBaseExporters_mA135DEF8904D131E9DE28C116AFF4068AF764796,
	JsonMapper_RegisterBaseImporters_m7283762885A9CBC8FCF73C1409FE0A4B73F73706,
	JsonMapper_RegisterImporter_mE3C963E04CF124D1EF70D9AE8655B23673FB990B,
	JsonMapper_WriteValue_m0C04268F57BD995BC220C1A4AFEFB761AA76216E,
	JsonMapper_ToJson_mEC8DB1E520EC14692F869F5C766AD5E830B9FAEA,
	JsonMapper_ToObject_m3F0E12BE022763FFFF80FC0D2B6BF9E5BE669897,
	JsonMapper_ToObject_mC64EA7544FB2A1312A3A4C3B4C3674A6C5725818,
	NULL,
	JsonMapper_ToWrapper_m96BC581D45EFC12E1BAE33F71A9CA5F545477AC5,
	JsonMapper_ToWrapper_m097E495DC63458F82C36053A970F9C66100F5DBD,
	U3CU3Ec__cctor_mBC398A41CDF77CED40F737F11A282729CC16B760,
	U3CU3Ec__ctor_mCB72C11DA63562C2965ECBCF431F3A3855E4C514,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_0_m5E65AFAA97DE9886E8685F4CA74F17DE0935824A,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_1_mA03284DD8D466CF8951FA09E11693B2406649E5F,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_2_m044D9D87625F9C820A9A641FFD88E36265B6C661,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_3_m22DCF509B0371345B34C28811C11FD3EEA51251E,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_4_m5173AE1B7922832E705F23DD96D7B21716D8DFD8,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_5_mA7562BABCCAFCE6961ADAF5A86796F9BFED47362,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_6_m279BE783077BEFA61D3C10DA9A70948EA0293A0F,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_7_m9ACE3DE44C23B4412DCECBF5D8084F932FF47CE7,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_8_mA85CC3F8FF379190501B377EB0D9FC56490B5500,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_9_m7FDA939669F3EE3B1A7A1081B3ED54489D8DE637,
	U3CU3Ec_U3CRegisterBaseExportersU3Eb__25_10_m10D28BCEB66B6A4E20673897EAF82A4C462266AD,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_0_m1AAFB2154D45ABF1CC9B5B4843F511CB7405BD2D,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_1_m5A1FC8F094BF3D2F49B06F1EF0E0DE14FB05070F,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_2_mC4E29C0ACB005735FFF5736A51F9B7ED9793770F,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_3_m35500DB55A65C188F227633F534BE4455673F827,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_4_mDE5A5ECA09D872D5DC395843A92FBC74750DD3A9,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_5_mFC370259E48B292AF54B52D8042AFD81BCB78025,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_6_m805A79B2C7218A777DA94BD6FF71FEF1EAAB84D8,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_7_m3DD36AEEC6F91DFC05D993580AA4E7D243E8940C,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_8_m28757971BB5CBF7729FD1FCE4F452ACF4AE3A50D,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_9_mC7A02472079C1F330274BC251138A061F4A866D0,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_10_m97BA343722EEC0A7934DDE7ADD1C89A4FD6D5370,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_11_mC183338E2E99D595EECCCED5747FF8FBB213A0D8,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_12_m7FD0038BFC4FA2A8B791170ACCDD51CCCD5654F7,
	U3CU3Ec_U3CRegisterBaseImportersU3Eb__26_13_m2A9E4435AD064C727DA895AC5DB23CE8C6185FEE,
	U3CU3Ec_U3CToObjectU3Eb__32_0_m086D4879268A7374F7530400F470ECCF09B40328,
	U3CU3Ec_U3CToObjectU3Eb__33_0_m3A261637AFD7929969FD7768927FAFAA15F47DE5,
	JsonPropertyAttribute_get_Required_m13B37FCCC7DA7AA77F335DEFAFBED5BAAA93C670,
	JsonReader_get_Token_mDC6D348B59ADDD2A98EC43E990DA35128817E694,
	JsonReader_get_Value_m40B584C292D15A740F5305565CF463A9F6FDC08C,
	JsonReader__ctor_m4930899FDB533A49D0727E72ADA8A821A7174A8A,
	JsonReader__ctor_m8975A07F4B42FBECC211DB9D205A980F3864234F,
	JsonReader__ctor_mA92A67018D2730E591D98ED0C3B0FBABCFDCEA91,
	JsonReader_ProcessNumber_mB5B2DC7C8F5B98D63A8C2856FD153E0FF5D78F3D,
	JsonReader_ProcessSymbol_m9AABBF0B918EB9DF5E1BEA52EDCCE9DA803B3C17,
	JsonReader_ReadToken_mF5095828E4AD13219B5184DAB40DEAB964FEAA47,
	JsonReader_Close_mA49AE14270AD0CB3A01604F9D86D645923BE26FF,
	JsonReader_Read_m3B29CE85487560850A62C72849831E3223BC24E3,
	WriterContext__ctor_mDB04C45F94CFD3378EB5AF77BB8CF07AC0016E42,
	JsonWriter_get_TextWriter_m3B0CD8D100F5FC45F23EE48C9DE7984221DB4FBE,
	JsonWriter_get_Validate_m39DDCBB4EF53B79558375768981E9F287ACBF983,
	JsonWriter_set_Validate_mABDAB76B0EEDAE8C5A855CC4C406D233513C482D,
	JsonWriter__cctor_m73586093EC9DADDFCB5A43578B9E4D39CC2F8A91,
	JsonWriter__ctor_mC89F025DB721BAF8C9AF5DC458111401F2037B29,
	JsonWriter__ctor_mBDA08DAEF878E5223483D0AB61C9F1449EA435D2,
	JsonWriter__ctor_m5ED4B5D93FAD7484093D38FFF40DB5EAD705F0A5,
	JsonWriter_DoValidation_mF1EFBCD5D3075E48B693CC47D6B28AAF3BA32891,
	JsonWriter_Init_m7DEAD93F35483DD8295384E4EC59518C5E76CB49,
	JsonWriter_IntToHex_m1E3AE7A4927E90F19634899D52B915EE5AC993BE,
	JsonWriter_Indent_mEE145732A5897943B8D4238673F2FE490BA5D539,
	JsonWriter_Put_m50B5F196172094249CB3D0ADB3A42FA6C90EA774,
	JsonWriter_PutNewline_mBDA8FCFDFF049FEB70A09DD8F553E490336E7AEB,
	JsonWriter_PutNewline_m8AA28D5D50DB0E058FE55EDACE4DC1E6A10CDFB4,
	JsonWriter_PutString_mBB1EB05F0B758BE8A2CBC78C51A3C0FB9577F092,
	JsonWriter_Unindent_mDEF47B66501CBE3468100F8F7A16BE47FD9FDDD7,
	JsonWriter_ToString_mB8AA9F4B7FB6605A5C7C133C02C8FBE753F74315,
	JsonWriter_Reset_m6C1E9090D6DDA4707A1B49E19B1323DEAAD58EFC,
	JsonWriter_Write_m3DD35A09B85541A87CE43AA2A3D0EF2D17DA6D22,
	JsonWriter_Write_m5D3E11D3563E3BBE14C7F62DE18E902B0CB0A671,
	JsonWriter_Write_m53E153F56EE0CAE5F935851DDAE55BBA1D221D93,
	JsonWriter_Write_m3543CE25D30150FFED40FE4B1A0ED96855814B90,
	JsonWriter_Write_m8EB022145303FFA0142FEE32FEA3C57DD90009E5,
	JsonWriter_Write_mDB6EE13037A3ED7F214FCDC5EAACF7BE8C3204C9,
	JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B,
	JsonWriter_Write_mC5058DAFD1A17F59EF9C406C354D29808D3BEA4B,
	JsonWriter_WriteArrayEnd_m5B07BFF16123F3B2D07899B10B056F8DAD9758C6,
	JsonWriter_WriteArrayStart_mB1FE97CA88553F8E58669D8438BABEBD586F98C9,
	JsonWriter_WriteObjectEnd_m127A9318E08F34226D40487B6D833F540B86A679,
	JsonWriter_WriteObjectStart_m385A8272F17BCB16C9F994381B6BADCE1CFD6A6A,
	JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F,
	FsmContext__ctor_m2CA8508DF1A63875F7E9315B1F60AF3320E20373,
	Lexer_get_EndOfInput_mE3D2AC25B3A517770FE20377D01ABF3AE7E34313,
	Lexer_get_Token_m4A83BBBBD713C8840377E9EF76B0003106559431,
	Lexer_get_StringValue_m7282CF61FB4BF23F3AB564FBB4FF763A84E3449F,
	Lexer__cctor_m8026CFBC80EC6B79A1963F11C5226E8B85687DD3,
	Lexer__ctor_mDFA89019EB7B6A89FCD65DB608AE70B993D3CF70,
	Lexer_HexValue_m17E5475BDA450A9BD963F10B54E2F6D58149F8D9,
	Lexer_PopulateFsmTables_mF32F6DB96A934E032025F1EB171BFA51D68B07FF,
	Lexer_ProcessEscChar_m801DF0CF016F4FF388B3B28459EFEFCBCB094DE3,
	Lexer_State1_m04C4D5F9B2200D52DD57D6623C95163AE749627C,
	Lexer_State2_mFE6544971C20FB7EC5B3F7A37B009C4509B22528,
	Lexer_State3_m956807D4969B408DE982CA63DBBF0B81E104857A,
	Lexer_State4_m16FEB4679942332962DCE69C481062A249FE8591,
	Lexer_State5_m5CA39A23583D3BB06BE62BEDB5D9801086F94FE8,
	Lexer_State6_mC1BB562D862CE582B1367DC5D76BCFFAA59FB104,
	Lexer_State7_m2CDBF5AE90B892C7CED3CA83478D903AFDC5D611,
	Lexer_State8_m665E9647874B34DD74C9F860C158FDE059AF3F25,
	Lexer_State9_mE19F3AF9A5FB0DE45B22315F4F8025ECF1B92545,
	Lexer_State10_m622EB6FA43424C7E25ADFD3C822CBE0178AE9D99,
	Lexer_State11_m2EF6198F5C40F599126B8F13CF57AAFAC720039E,
	Lexer_State12_m8DAA8A83D0A7DD86F23D664D8318FED1A8A06423,
	Lexer_State13_m21D465032A22CBEA6F40499F5D02157273ABFC76,
	Lexer_State14_mC9246F0367D80001408BFC0FE4E4851AAC4F45F5,
	Lexer_State15_m6597BF5514B466EE3E0C853E61DC587380425DE0,
	Lexer_State16_m8F17B258F0DC016F07436E9C02489CF9ABE7FB82,
	Lexer_State17_mDB0CEF1FACC2A4259255F5272C8E9FB14A5748C2,
	Lexer_State18_mC9357EBE956FC522FE9B082F7D05CDE8516E08B2,
	Lexer_State19_mBA15B867A90E9F20DC84FCA5826674C221F20342,
	Lexer_State20_mEEFA027107B256314483E1A90D604B4527E11197,
	Lexer_State21_mAEC7C0DD1E280F050F7ADFD32D779D14E795BA3A,
	Lexer_State22_m7AC6AE2C616DC46C6943C2401EE62ED05B734911,
	Lexer_State23_m26C822CA740A53810D10801E269819F58CEA1459,
	Lexer_State24_m5D6C4DE5AB75CF790316616E9ED98FF94940303F,
	Lexer_State25_m1DDE3EC503297F16AB8B78C8549716AFE8C8648B,
	Lexer_State26_m2521D5E365C21AF1401752A005E42B9929F3C7B4,
	Lexer_State27_mC017DD08AF7644DBCF20F9C40C9992BED9FB3F53,
	Lexer_State28_m508A9BC52B0CB427E5647A4ACDA330A8A886CFC8,
	Lexer_GetChar_mB40CF3CB1C9FB312CE3C731FB0BB7EC12F6A39EB,
	Lexer_NextChar_m1CFFA57D4031525FC5CA338D252B50269C31F166,
	Lexer_NextToken_m2E950B886E97AE557217862E2566F81CCD1FC105,
	Lexer_UngetChar_m27545FEA0B82E0AF5C2C193613C7EBFAA948CA87,
	StateHandler__ctor_m4FCCA7028A029553722A6D5B3FA356FDCD2E3495,
	StateHandler_Invoke_mE436A82A2103774B11AF7B6EDCA8682903C0A8F7,
	StateHandler_BeginInvoke_mA6A4E54F83FA7B6B51E87E6E630659D6D79CF3AF,
	StateHandler_EndInvoke_mFBC00C6248F391903E4EA805C18624972A400335,
	CRC32_get_TotalBytesRead_mD4256BDDF15109AA5BB3A92C01E7901BA20F4664,
	CRC32_get_Crc32Result_m5C34C5A39CDA6D4D49C0134ECFF8CA5F2864D4F1,
	CRC32_SlurpBlock_m88E491281C2F632C946323D48D8CB51903524051,
	CRC32__cctor_m1C0A446866F0E8B165B312D54328BB6FBEB5760E,
	CRC32__ctor_m76575964AE07E44C7F93F6B6EE4CA3AC060238BD,
	CrcCalculatorStream__ctor_mEB7F08CF49E79830BE2D5D8C1B6208D7B112E047,
	CrcCalculatorStream_get_Crc32_mDB69F43A67470B477368C4D49639E2E77CE07505,
	CrcCalculatorStream_Read_mA62379E9EBB57BD3C029952843133372DF8FD9BD,
	CrcCalculatorStream_Write_mFA4D550FEC03AF3B378DE863AA571D227E1892F7,
	CrcCalculatorStream_get_CanRead_mE1F9A4CF15B24EA4282C6CED59A6B6C7EB07C20C,
	CrcCalculatorStream_get_CanSeek_m2754BD935F8271ADACA978448B6464519A8A1F84,
	CrcCalculatorStream_get_CanWrite_m5D2D28FD0EAF7FA8F00C59C534C5C2C98E9B1950,
	CrcCalculatorStream_Flush_m5D8BBBEC92AD0CFCB99BF174B8E0131AACA04823,
	CrcCalculatorStream_get_Length_mF4E6BFC137356ABE7D49D85C0FF89A86626A703F,
	CrcCalculatorStream_get_Position_m716EBD855E62DBD12B646088737378372A314538,
	CrcCalculatorStream_set_Position_m355E4C6F95477AA43672FCCB2B4A9A9F11DEF22F,
	CrcCalculatorStream_Seek_mAB455D6A9959E1AB74277DEBF80724B09972FAB4,
	ExceptionUtils_DetermineHttpStatusCode_m832ED3790938A17CC598D1DF1920571165868D59,
	NULL,
	NULL,
	AWSConfigs_get_ManualClockCorrection_m805F870491B9406F153304933920A3D73DCA954E,
	AWSConfigs_get_CorrectForClockSkew_m7FD93ED74C4CD59924C52A386BCD10636A80643B,
	AWSConfigs_set_ClockOffset_mA16DF92F5F2A10BD24A5D52C01D81ED888A868FB,
	AWSConfigs_get_AWSProfileName_mD4C6B99D9EBF6246CDA20AE59A0557FF249E1DFE,
	AWSConfigs_get_AWSProfilesLocation_mD3E2F6496CC61CD2BB20667B5BB590C9C1420B16,
	AWSConfigs_GetLoggingSetting_m1148B8C480C000674AD9C070A367F98E54FFBC1E,
	AWSConfigs_get_EndpointDefinition_mA88054E4EF572298E8185A0C989E057FDC507681,
	AWSConfigs_get_LoggingConfig_mB1F7486EDC8D8B5740E04672183D7324A7FCA2A5,
	AWSConfigs_get_ProxyConfig_mC04D7E8D83182605B86B37C968B9E3961424ABBC,
	AWSConfigs_get_UseAlternateUserAgentHeader_mA2003DCCC278378A98BF9862B4C5293F583F069B,
	AWSConfigs_get_RegionEndpoint_m126A32D8D2E72BE9A9B6C9C180ADEA423477D343,
	AWSConfigs_get_CSMConfig_mD4499CA01FDE37D7494EC4EE6BC1CF33F33183AB,
	AWSConfigs_add_PropertyChanged_mCB076570A47E9CECD0F4D4244A2074283792D481,
	AWSConfigs_remove_PropertyChanged_m7A1BA5DF145567443804B785912FB1249FFFE903,
	AWSConfigs_OnPropertyChanged_m3A46404400E79BFB2BAEEBD7CBBE1A58DDFA0866,
	AWSConfigs_GetConfigBool_mE49A29837AA84496ACFB2D01B6199F599E801C96,
	NULL,
	NULL,
	NULL,
	AWSConfigs_GetUtcNow_m5401E987A9C17AF9A354DF12E49A928F7A6FE820,
	AWSConfigs_GetConfig_mD3E3F762D75CCC9C2755CF701732A61C41B8F6E2,
	AWSConfigs_XmlSectionExists_mCEB187ED1044185D797E506A4DF868BFED321FED,
	AWSConfigs_get_HttpClientFactory_m78229ECA292AF41D8A1D1DC094725778383DF561,
	AWSConfigs_TraceListeners_m0BBE4821EE0A31935246512C35E1BAFA61EA0E5E,
	AWSConfigs__cctor_m60C9F1ABF3DEAE0B1738C68E6E0089DDC34F81F5,
	RegionEndpoint_GetBySystemName_mA6FECE8FE84FC5767B0CD1412F83F55853A3C23C,
	RegionEndpoint_GetRegionEndpointOverride_m3894BA13424B9DBD3D819615F73A947F9ACF3D1C,
	RegionEndpoint_GetEndpoint_m2C77EE8B62F2971AD60F0E72D4E4AC013051D8B5,
	RegionEndpoint_get_RegionEndpointProvider_mF4A33FC2FF01C62098F3DA25C093119A3E30A29B,
	RegionEndpoint__ctor_mD6BA45FE515A43C06404B9650B094A3C603B0DC2,
	RegionEndpoint_get_SystemName_mCA2FF58E84D00596AC3250F797AD2AC3B783FF9D,
	RegionEndpoint_set_SystemName_m98B17B4109A3DFB6C723FC34D060297FA1ECBBB6,
	RegionEndpoint_get_DisplayName_m9F7BF40A023CC420766759C7643189309742A812,
	RegionEndpoint_set_DisplayName_m7DDC78786C489CAC929012EF3FA130015572EF42,
	RegionEndpoint_get_InternedRegionEndpoint_m7B7F506B503AE5119F9770B7BEA85D1E57FDD084,
	RegionEndpoint_GetEndpointForService_mF3283610EE740039B5D0CFFC7CAA408BA2BEF440,
	RegionEndpoint_ToString_mF713241471D8370FBC2527AEF4FB73E9296742DC,
	RegionEndpoint__cctor_m1B1FC5C7EDDFF39FDDD15AA4D49E1BB9FEE72C95,
	Endpoint__ctor_mB3E50CA94CDFA257DA7E2EB77504CA4D59485D3B,
	Endpoint_get_Hostname_m68CD8A74E6E7DB12FC54D991176DC12D6A0C4925,
	Endpoint_set_Hostname_m25D4F046B53E36CA01DE38ED67B854336395EB0B,
	Endpoint_get_AuthRegion_m342C6FEF878A06504E6822261A6752BF328879AD,
	Endpoint_set_AuthRegion_m5ABD1D4B3686C2C959AB1FCFF0120D66CBDA2554,
	Endpoint_ToString_mD538AA4F9B47798139CA9C6B95E7EDD975558077,
	Endpoint_set_SignatureVersionOverride_m67F109FA152FA50029D59E3019A642326EE6C3A4,
	NULL,
	NULL,
	NULL,
	RegionEndpointProviderV2_get_Proxy_mF94D500F8D838A0A41E557F8F78E85E1881CBBE7,
	RegionEndpointProviderV2_GetRegionEndpoint_mF4F1BCE89EE0A1BD2E58F13E89CAD647152B0485,
	RegionEndpointProviderV2__ctor_m3CC64AE924D07D3BC151DB3DE54155F33C0F7BE7,
	RegionEndpoint_GetEndpointForService_mA9D111C60C0BA69854A8967D30EBDBFCACDAAF3E,
	RegionEndpoint_GetEndpointRule_mFC69A0B57CA3F3EA12DADAD44B2DED89A5197BA7,
	RegionEndpoint_GetEndpoint_mABB94426F48F4A256C5B27A378BC8DDAE15F8A31,
	RegionEndpoint_GetBySystemName_mB727B55DE528F073D2C346A95CDB594141148783,
	RegionEndpoint_LoadEndpointDefinitions_m96D9ECAB3D65F2F4173FB8915CBA0B0BB0612DE8,
	RegionEndpoint_LoadEndpointDefinitions_m8B7B15E3C44F1A745B56F4FEB1A40C209EE7F3BD,
	RegionEndpoint_ReadEndpointFile_m3A6DE81E2CCF4055EE27EEF58201CACD02DC5F5E,
	RegionEndpoint_LoadEndpointDefinitionsFromEmbeddedResource_m15B6E2A5A743FF27829AFC34A8899B9297AB71E1,
	RegionEndpoint_TryLoadEndpointDefinitionsFromAssemblyDir_m71D0F68FEEF275478D9C62C92BE6051C83D4929B,
	RegionEndpoint_LoadEndpointDefinitionFromFilePath_m0F2C0AE8212179A8A2CE5C1234CCC2094C7A325B,
	RegionEndpoint_LoadEndpointDefinitionFromWeb_m46EB57E8492CA7C57D956077345B067557C6CCB7,
	RegionEndpoint__ctor_mC91B8062C4B6AF72673C2050CB77E1F65313D75A,
	RegionEndpoint_get_SystemName_mA927AC85ACE3E746C6BCEE3F85AFA08491345AB5,
	RegionEndpoint_set_SystemName_mE32DA2C28922213E460BF7B162452D653DA98E6F,
	RegionEndpoint_get_DisplayName_m8CBFE99744D4B800EE260CE6E770C884892ED534,
	RegionEndpoint_set_DisplayName_mDDFA9C6B58D6D1331F5739BD546588A6B8E18B16,
	RegionEndpoint_ToString_m01D01303029AF41AF201F350DBED9FBEFCAD89D8,
	RegionEndpoint__cctor_m2C373CBC77F6556CEDD011B8624BD2ED389B91D0,
	RegionEndpointV3_get_RegionName_mB7CEB1A08A15E5A094FB058980751AD56B028974,
	RegionEndpointV3_set_RegionName_mBC114B5247B46D3F873AA93606B19883D91C6EFA,
	RegionEndpointV3_get_DisplayName_mD3913EEEF925BD31703A43466C887E014E98A673,
	RegionEndpointV3_set_DisplayName_m81A67F18FA5E4D9529CFD7D6773142BCC5F9F55A,
	RegionEndpointV3__ctor_mF380410DF9CEA1966337B78CD5B05784D56937A4,
	RegionEndpointV3_GetEndpointForService_m782EC58A7A5A345E29761CDCEFB82835AF6A87A4,
	RegionEndpointV3_CreateUnknownEndpoint_m4F444528E9BBBBC118C19C011ECC156E8A060B77,
	RegionEndpointV3_ParseAllServices_mF03BA09DE1E6B21748CD3EC9C185EA5D2D6B90A3,
	RegionEndpointV3_AddServiceToMap_m1E1A63BBB84733F80C38547FBEF98225E1016386,
	RegionEndpointV3_MergeJsonData_m465323F7CDFFA8A5A012DED8F1D302A70F0437F0,
	RegionEndpointV3_CreateEndpointAndAddToServiceMap_m8BB4C3A21B343D14A732B157A342D8E231BDDD29,
	RegionEndpointV3_CreateEndpointAndAddToServiceMap_m998113EB002B7CB3AD2459786F9356396774C4E9,
	RegionEndpointV3_DetermineSignatureOverride_m810D04421EC536935F10CB456EB9A75EBC6E45BE,
	RegionEndpointV3_DetermineAuthRegion_mF187F53CFE4E35F39719CC36A95E4489211DA105,
	ServiceMap_GetMap_mA9C35137A79E922509AAC730C45096A31F598435,
	ServiceMap_ContainsKey_m8993B60BF67FC0699BC197A7C2CFC6A022CAB2A9,
	ServiceMap_Add_m18F509BFF7C82668A2DF54259A4E19E62B07C266,
	ServiceMap_TryGetEndpoint_m80642BC1FB0618AB1A3F525B6EEEFDA615AF1D7C,
	ServiceMap__ctor_m9342C6CD7674E3A32B962274708B67A091E002E8,
	RegionEndpointProviderV3__ctor_m4E4238A951719DDB1BFA479376C1123D1D0A3518,
	RegionEndpointProviderV3_GetEndpointJsonSourceStream_m88FDC46A64728BBE302BEA6245CFBCFF07AA64C1,
	RegionEndpointProviderV3_GetUnknownRegionDescription_mF8388D54F8D3865CD4F818972B64568E29DE7113,
	RegionEndpointProviderV3_IsRegionInPartition_m159457AA81674F8C4E9A62C9B2DBA6389727DF30,
	RegionEndpointProviderV3_GetRegionEndpoint_m67099451DA46708CD8331DC88FB8F58F62806BFA,
	RegionEndpointProviderV3_GetNonstandardRegionEndpoint_m3B5AE2A1910AC3A24B1CEDD786A0422F1D3C21E0,
	RegionEndpointProviderV3__cctor_mE38D9B103AEEED843CE32437FC24792B81F5928F,
	ProxyConfig_get_Username_m81955ADE534F7677180E2939A6C3D1AF0A2228FC,
	ProxyConfig_get_Password_mD09CFD5FB9141132944E25757C50D5C421C20E51,
	ProxyConfig__ctor_m8648DADF98050B1FAC492D4B1E454E55FD475488,
	LoggingConfig_get_LogTo_mA85047D8D22C21160FAB8177971E9C376E84E80C,
	LoggingConfig_set_LogTo_mEDF5F4A32C52E71206C36282D6A5FAA00B89C4A9,
	LoggingConfig_get_LogResponses_m9F1FA413F3A9AF41C64AC4B219798FA570D18725,
	LoggingConfig_set_LogResponses_m0E16D7CDB69C7EEE2994914B6D34B24A62C898F3,
	LoggingConfig_get_LogResponsesSizeLimit_mAEF0745D43816A42B5D80B1DE17380FBB14EAF16,
	LoggingConfig_set_LogResponsesSizeLimit_m7E62A8B361B8EB37305DFFAF37FF0AF6F9D3D0B6,
	LoggingConfig_get_LogMetrics_m47AF794FF34CA33B117B030A865695FD0FB42000,
	LoggingConfig_set_LogMetrics_mE4CFC322110C8D56EA52CECB88D69FA8BD560FE4,
	LoggingConfig_get_LogMetricsFormat_m5C2206F419BF88D2F401258F965407456E4DD30E,
	LoggingConfig_get_LogMetricsCustomFormatter_mEFF5F77802440902877DF50BC11721A69FFB2E24,
	LoggingConfig__ctor_m124A902B7D0FBEA0E0D9D2D2AB865402B4F1A833,
	LoggingConfig__cctor_m98A3722C696C7B78A44BFAAEDBEB931D0FF13692,
	CSMConfig_get_CSMHost_mCD6A4223BC46188E253784DD6811D36F1022314B,
	CSMConfig_get_CSMPort_mB95D7D19420DAAC48311A4EC703E1A6F34E0AC44,
	CSMConfig_get_CSMClientId_m6994559572B87F47F0B6E55E46367E9F2490E8F7,
	CSMConfig_get_CSMEnabled_m04F6AA3E68C62699639115DF7BC9FEA121AAB165,
	CSMConfig__ctor_m9CF53339FBC3651492BBD00AC79A5CBB5A24ADC1,
	AWSSDKUtils_DetermineValidPathCharacters_m87039C0E22F72198C04358C291E4719009D6000F,
	AWSSDKUtils_GetParametersAsString_mA817B78E680075B6FA6DB166BDA251311DCB7DED,
	AWSSDKUtils_GetParametersAsString_m7F03BABD27CE1DF12845C29F1155111D69221C01,
	AWSSDKUtils_CanonicalizeResourcePath_m79BEAB577297145BC0CEA1E69DE47A6BAD9835D7,
	AWSSDKUtils_SplitResourcePathIntoSegments_m5B2A02550D2B22A233D7BC810D0FAB5ECF1CB4C1,
	AWSSDKUtils_JoinResourcePathSegments_m824C05DB153DD2AAB927E98AD73564439C9D0C4D,
	AWSSDKUtils_ResolveResourcePath_mE9E964E126AC91AC9F9A96BD4FC5A92A49086936,
	AWSSDKUtils_DetermineRegion_mC5F64BA18C180A7FCCDF749045E6891436E16F14,
	AWSSDKUtils_DetermineService_mC6E388DF8284F1110FF45BCB0C4AE2A8687033E3,
	AWSSDKUtils_GetTimeSpanInTicks_m0CF57CB556B76A2ACD6BD29931AB286EE24ADB79,
	AWSSDKUtils_ConvertDateTimetoMilliseconds_m41957A6CF1374E7F16A8BA453460254BF7FAFB74,
	AWSSDKUtils_ConvertTimeSpanToMilliseconds_mC6BC4968AC07422A848EC7F6E45CEB2EE9C9B6EA,
	AWSSDKUtils_ToHex_mCB2E490A5E54E9F0B6A237BCB2D2D6D1D739E1C9,
	NULL,
	AWSSDKUtils_get_Dispatcher_m953348051D1C1BA80934260FA081C240DC367B30,
	AWSSDKUtils_AreEqual_m126C2EF42BF063AA060D24284ECB2FB5AE0385CD,
	AWSSDKUtils_AreEqual_m4B639C0FA027C17F23CC93DB10278BA83016EDB8,
	NULL,
	AWSSDKUtils_UrlEncode_mFF751215F3D5B019B6FAB326BDE9333A9A0BAECE,
	AWSSDKUtils_UrlEncode_mBE8AAF67CADDE09C6C6B8591B8C0899C0F6B023A,
	AWSSDKUtils_ProtectEncodedSlashUrlEncode_m565AB1506E33A4AFAABEC122D4DD3877341FF9B7,
	AWSSDKUtils_get_CorrectedUtcNow_mE4AFBA5A0281699873807E635F0395877AAB9AE7,
	AWSSDKUtils_HasBidiControlCharacters_mD533478215E1E0E98A4D0FE7862FC07C98524430,
	AWSSDKUtils_IsBidiControlChar_mF572580CDDC1CC2DE37E5FB2D9E46442DD0C3F9C,
	AWSSDKUtils_ExecuteHttpRequest_m9D231BB7824DE3B76C5F6654220392D6B442DA57,
	AWSSDKUtils_CreateClient_mB840CDD823C8769EF0BC7614ED3B522E81B1081F,
	AWSSDKUtils_OpenStream_m614439B791BD93BF526647DF2B7B621C1B23229C,
	AWSSDKUtils_CompressSpaces_m1E23989796ADE51F9E1FDC9BBB9939EB826F8B5C,
	AWSSDKUtils_Sleep_mBB07FE79C9765EB2BAE1859B80E808E8701F144C,
	AWSSDKUtils__cctor_m5031AD0FC09F01DD36176D77433E4C23458F6510,
	U3CU3Ec__cctor_m9F819D44D3134E2B66A864E30A06909D319C826B,
	U3CU3Ec__ctor_m7CF9DBC020D7E5399DCD92ACB1F61A36C13DFDD4,
	U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__38_0_mE4233E9B88AD81B7F9BC0249E6EA1F0AB2B87FC7,
	U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__38_1_mE3AE06E0D1E273AB77A8E532E3D36E68FC308725,
	U3CU3Ec_U3CCanonicalizeResourcePathU3Eb__38_2_m0CE9E2321D125F9115A6EB1DD6E21F4ABA55372E,
	U3CU3Ec_U3CJoinResourcePathSegmentsU3Eb__40_1_mB0D9B08B2BC78C52C432D8BC042009E88E159B1B,
	U3CU3Ec__DisplayClass40_0__ctor_mFB5ACDB461169A42C990EB348C2CFD90DD5AD4BA,
	U3CU3Ec__DisplayClass40_0_U3CJoinResourcePathSegmentsU3Eb__0_m48A6C67BA4854BA88F8C9A465E71DC1282454A97,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass95_0__ctor_m200B865431333F10F6566235716D5EF111B04E3C,
	U3CU3Ec__DisplayClass95_0_U3CExecuteHttpRequestU3Eb__0_mAB8FFDDFF6E49F42F10F95AE58FA3594088D2AEB,
	U3CU3Ec__DisplayClass95_1__ctor_m6FD815F6AEDB2FE622329F40F111E2F60F1D22E9,
	U3CU3Ec__DisplayClass95_1_U3CExecuteHttpRequestU3Eb__1_m52F051D4B9969CBF7BC35CCB3414721B9D456615,
	CryptoUtilFactory_get_CryptoInstance_mBCEF74A8E153C93A0850F86336DCDC1077C7C406,
	CryptoUtilFactory__cctor_m45D3C87D61547B4F0B2C428227E76C534557FC30,
	CryptoUtil__ctor_m2C3D9C75BA325CC87892736B522CA36874953754,
	CryptoUtil_ComputeSHA256Hash_mB7172474091484EC6D47DB1C5988FE3C7BDBE660,
	CryptoUtil_ComputeSHA256Hash_m02C5B8052902DE688297BED65624EF2F24D73CB5,
	CryptoUtil_HMACSignBinary_m62AFD13C51F699D9E02675CB0F3677C253E403EC,
	CryptoUtil_CreateKeyedHashAlgorithm_m0B00B20BC0433B48F41A261B14CBCAFB5FFDF42C,
	CryptoUtil_get_SHA256HashAlgorithmInstance_m94DA4B020684F3CED1292E2EFCEAC4E73EE3D835,
	NULL,
	NULL,
	NULL,
	EC2InstanceMetadata_get_IsIMDSEnabled_m6D348242E40F583DEB58C5C4D2E9BB56D96AF4F2,
	EC2InstanceMetadata_get_Proxy_m44E9E5E80195713A75A93A64D56080954ABC18C6,
	EC2InstanceMetadata_get_Region_mE329F1E9B1E5A9066997D836362B50AAB9FEEC3F,
	EC2InstanceMetadata_get_IdentityDocument_mF6DD9B5AF6AC385A701D1DAE0A9061C6BC51E50B,
	EC2InstanceMetadata_GetData_mBD1DF9EF9367701BB656F7D59B4EDA6A379D59F6,
	EC2InstanceMetadata_GetData_m85B02C6690D54AD047EDBAE1717A3A0EAB84A043,
	EC2InstanceMetadata_FetchApiToken_m5C7D58DC4CE3A1EBDD128D4D23B66B0472562B01,
	EC2InstanceMetadata_ClearTokenFlag_m01EE97679B10CBFDACF163E56DEBEF2F1F15C379,
	EC2InstanceMetadata_GetItems_m5017CF779F1594E60C8E1F963218FEE2BD23B60A,
	EC2InstanceMetadata_GetItems_m5C57CE47D509B9D73240ED7754AE7275ECF519E3,
	EC2InstanceMetadata_PauseExponentially_m54E6DE11F9F8EFA15CCAD04461BF2BB2B084B430,
	EC2InstanceMetadata__cctor_m07CAB17F1FBB2C8EF424084DE359C6EBF000C7C4,
	IMDSDisabledException__ctor_mE7A37EB248C344319641A401E04AF13F9AE8A120,
	InternalSDKUtils_BuildUserAgentString_m2C1A262CB88B697FA16980D3485B3DF3BD7C93D4,
	InternalSDKUtils_GetExecutionEnvironment_mF8CB9381B07D8409438D77C017F4235C38AE1968,
	InternalSDKUtils_GetExecutionEnvironmentUserAgentString_m13F0316278ACBBDDC1D466E95020538455CB0C10,
	InternalSDKUtils__cctor_m755DA5C11DD548AF936FD2CC26A5F613C6565006,
	RootConfig_get_CSMConfig_mE24357A51A72DC9C705EA286180502BD8715A423,
	RootConfig_set_CSMConfig_m98B7688DFE7FAD711BFA1555380BB9B3BAE1F278,
	RootConfig_get_Logging_m4625DDD282B9B05DA95A92741359BE067FD2D6E8,
	RootConfig_set_Logging_m5E9E2542873931F0E929611F21B9A99BDD56FD9D,
	RootConfig_get_Proxy_m9E0480BE4E106446353942289AF7D2BB178B7162,
	RootConfig_set_Proxy_m1BE8FC28EE2B8C93A2E58C612C8B8212C8B5468F,
	RootConfig_get_EndpointDefinition_m3204E79A2D482EC5614416625F1BF0D9B4AF1DD9,
	RootConfig_set_EndpointDefinition_m6535665BFD93C547251D39119AED42E63366337B,
	RootConfig_get_Region_m284EF44B24F8EBB9672322B91B4134FFCFDF825F,
	RootConfig_set_Region_m453ED10087DC4B2A11408936814FB6142970CFC0,
	RootConfig_get_ProfileName_mF622E8B2F448542B28DC0EEDFC59244BAB988205,
	RootConfig_set_ProfileName_m22D6F68E19AFBE2F41CEE81A06CB64C0775614E3,
	RootConfig_get_ProfilesLocation_mF3FCB10B8A02626FFEBAE7F4A3DE96637840BEEE,
	RootConfig_set_ProfilesLocation_m100049E9E25819ED7EB08FE271E9E98639557F50,
	RootConfig_get_RegionEndpoint_m2461C25050777A9F2DF13F64E2B61A4140340DCA,
	RootConfig_set_UseSdkCache_m66A052C98A721A3B7F2CF81F4A812F321A1E0125,
	RootConfig_get_CorrectForClockSkew_m7AFB4F0C317A4C4FD94D3AC49C58E5131099CF49,
	RootConfig_set_CorrectForClockSkew_mFCBFE1909CD693B83D6DFCE3DC3018A0FB89C19B,
	RootConfig_get_UseAlternateUserAgentHeader_m7F2DA7D47D1FB6C0BD7A6CF6F022500B0292FEDE,
	RootConfig__ctor_m14B4194E7323349BA88DE0195C07E52568246C8B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeFactory_GetTypeInfo_m9755BC00A8A02BCB8A5988D5804DCB8DFFD32E83,
	TypeFactory__cctor_m813F49E6C072849BA06215B382631D454D48D2D0,
	AbstractTypeInfo__ctor_mE7A188B8B1B0A2CF9ED6DA54F4BFCD542C0073CE,
	AbstractTypeInfo_get_Type_m787F427069A926CBD0ACCD3F8D3746CA6E7E2E3F,
	AbstractTypeInfo_GetHashCode_mBB4A52B37BB447B116FE970B2F4382296D49BEA6,
	AbstractTypeInfo_Equals_m2AC0799B5983AB58418866DDB031C161DA42A94A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AbstractTypeInfo_get_FullName_m42F62612E904E29E17470FDA29D70FDB94E75CA9,
	TypeInfoWrapper__ctor_mF5F625F535BB8FC74A55C7F472C95AE2B1B3FEF4,
	TypeInfoWrapper_get_BaseType_m9E8EB5F5C03032DE8ED4818E71A4E1DBDB130D25,
	TypeInfoWrapper_GetInterface_m7C80F37C165494E4B2308537C3B88785C08FC290,
	TypeInfoWrapper_GetProperties_mAEED503C2BF7E6C3E72EA356216ABE293D0EE8AB,
	TypeInfoWrapper_GetFields_m454AC606A26C270131DAE0A423A7B64CCF2AA078,
	TypeInfoWrapper_GetField_mF7E05A1145989AD4AB0268D98CF1C433B8ABA83A,
	TypeInfoWrapper_get_IsClass_m10F5CC76276D71D7510CA672CBBBD5CEF9DAF736,
	TypeInfoWrapper_get_IsEnum_m3459DD5B738390A18737A84B129B5C8458A5C3D3,
	TypeInfoWrapper_GetMethod_m90037B2EDC2B633D75B04B8DCDEA56A148857481,
	TypeInfoWrapper_GetProperty_mF9FB40A13B7CADBDF926AD45B0B6CF04E0D59D53,
	TypeInfoWrapper_IsAssignableFrom_mDC79BF44EFE2EC9465F1D785A2EDC89AFC622E2E,
	TypeInfoWrapper_get_Assembly_m127B530E969421E28C6C3B00724172978EAE5A92,
	TypeInfoWrapper_GetConstructor_mD5F3FA72B928CA34A8E2477182899CA1F6F826EB,
	TypeInfoWrapper__cctor_m5A669CE8261177F1CC15E506BD9C1E5823A9C33E,
	U3CU3Ec__DisplayClass4_0__ctor_m89287B63729F734583D871E90819862289227078,
	U3CU3Ec__DisplayClass4_0_U3CGetInterfaceU3Eb__0_m4676D80C1D16FB2EA783FCBE727346E82338D900,
	EnvironmentVariableRetriever_GetEnvironmentVariable_m1955AFAAA656EDC1B0B399CA8B5AF408AE62CFFE,
	EnvironmentVariableRetriever__ctor_m151A495BAF90E6A87B3EBEE8CE47871DB3ADD11A,
	EnvironmentVariableSource__ctor_m8DB23ECCEC46CBBEFECE67CBEF609B9FA3EBCE76,
	EnvironmentVariableSource__cctor_mDD34CEEA80C34A83CC0E3B99F0D73AEE9ECC1A88,
	EnvironmentVariableSource_get_EnvironmentVariableRetriever_mECF810C4761E79E35D61A8B5932F374F49CB788E,
	EnvironmentVariableSource_get_Instance_m68B09747DB905CC1978B01E6206A3A5CF211D89B,
	NULL,
	NamedSettingsManager__ctor_m7FDB92E35DCD23CB86BD1C90639495B093482867,
	NamedSettingsManager_TryGetObject_m41151ADCB7A36AD85988B957160A21A131338D93,
	SettingsManager_get_IsAvailable_m352C1FF550B8C73C310C6806ED55D9FF8917EB02,
	SettingsManager__ctor_m34E17C8A2CE650DB50F89D5290C269F62D23092E,
	SettingsManager_get_SettingsType_m30F81BBD2789713AC9887E5025D938E982B2A0E8,
	SettingsManager_set_SettingsType_m5960C66BF818EC32834889E87F11AAA9CEE406D4,
	SettingsManager_TryGetObjectByProperty_m57BA5D26A47C607C8790806DD4FCEDDD67D027FE,
	SettingsManager_GetSettings_m2FEAB55B75E639F20EF33716B754B5D90287AFF2,
	SettingsManager_TryGetObjectSettings_m2BFCFA77EF935CD9E502E9F8B0317754FCA1834A,
	SettingsManager_EnsureAvailable_m293524507DB3F799E69E1FCE88F74B89661ED076,
	U3CU3Ec__DisplayClass16_0__ctor_mBD111B12B96C5D03D51DBFF7A5850DC89A39E58D,
	U3CU3Ec__DisplayClass16_0_U3CTryGetObjectSettingsU3Eb__0_m328E77FF18B34F07F5C77BDD3A8A35E11DB139F6,
	NULL,
	NULL,
	ServiceFactory__ctor_mA1F85302C39ECFA7930C3551B8BFF1710AE83C6C,
	NULL,
	NULL,
	ServiceFactory__cctor_m28A26D12D6756A922657F62C8959E9EA214EFDE1,
	AmazonClientException__ctor_m7B60CAA8E233ABD029FAD1CF1F1D0A425FBB16A4,
	AmazonClientException__ctor_mF58A0DA4D74C0A68D52D1A89BF8C61CFDAEA33C2,
	AmazonServiceClient_set_EndpointDiscoveryResolver_mD8ED6AD49FEAB0E7ADFE0D6F14B1C3624DE8EB21,
	AmazonServiceClient_get_RuntimePipeline_m666BA08F0997534D6F4D81431D5F34CB684050D5,
	AmazonServiceClient_set_RuntimePipeline_mE11F53789AF484FEFB2A831E18E1FD7CD9F205A8,
	AmazonServiceClient_get_Credentials_m080C193E757F0C6C3C6FACA2F5C7A4EE667CB1C0,
	AmazonServiceClient_set_Credentials_mB189B6BAC7FE34A3FD8B134C703CFB81E2F10FF2,
	AmazonServiceClient_get_Config_m7896DAF638A60C7D0CD95F2C92F7C95429A00FCF,
	AmazonServiceClient_set_Config_mF980BE678237045A0E7CE73D46170D67D8C1A38F,
	AmazonServiceClient_get_ServiceMetadata_mF868195EEB804B404AAE5B09B3B106115487C603,
	AmazonServiceClient_get_SupportResponseLogging_m22B71A38FC730AA6748E858D14A45AF76DDAB837,
	AmazonServiceClient_add_BeforeRequestEvent_m818F8CCC42067965654196C87B746679EB5045C4,
	AmazonServiceClient_remove_BeforeRequestEvent_m5D0A7FD127A8C3215D18A49C59680AA20766C1C6,
	AmazonServiceClient__ctor_mBF0730EE760A3209048451CB7B95CC3F796985AD,
	AmazonServiceClient_get_Signer_m67573336045A226123F21A0BD07C7248EFF6A481,
	AmazonServiceClient_set_Signer_m8D3241409ABFDDCF5465DD065A31DB72C162DFFB,
	AmazonServiceClient_Initialize_mFE10731C7B664841D4B5D9BAC00F3FDFC1926F6D,
	NULL,
	AmazonServiceClient_ProcessPreRequestHandlers_m646DB7F1C8E63E7840EEF3CF6E45C8F007419E9E,
	AmazonServiceClient_ProcessRequestHandlers_mD086F4AF4B1B5223FEF59963F9FF9FD1014FF215,
	AmazonServiceClient_ProcessResponseHandlers_m2A71998E649EE988F169E9985809C7101AF317C3,
	AmazonServiceClient_ProcessExceptionHandlers_m139EF81F5977833B08979EBE896DDFB9325C3EDC,
	AmazonServiceClient_Dispose_mA8A73A8F49A53E1CA5324B24A55F002E63F4352E,
	AmazonServiceClient_Dispose_mF1BDBFD1F83C742FA192AFFA3896B74DCEFDC4F7,
	AmazonServiceClient_ThrowIfDisposed_m0D7418634B3A02D10A974B7E9DBE7888444719BB,
	NULL,
	AmazonServiceClient_CustomizeRuntimePipeline_m1A584493513EDC4CB44F671B052FA91D88208A4C,
	AmazonServiceClient_BuildRuntimePipeline_mBD432FA9C1EF99F19C180AE782B79A1CF263EE62,
	AmazonServiceClient_ComposeUrl_m97D40D5DF7A42FA43B5B40A8D4A4811DC56A571D,
	AmazonServiceClient_DontUnescapePathDotsAndSlashes_m09466B48076FD4A85C3FF072E3A49E439499D4CB,
	AmazonServiceClient_SetupCSMHandler_m09B55B160BF4DA53A2B9441DDE79028AF9266E91,
	U3CU3Ec__cctor_m3B11D734C1828F5AEAA48062A9A1C1EDB6FD2DEA,
	U3CU3Ec__ctor_m6C8A395315DE182B761098654CC8ECD8076155A2,
	U3CU3Ec_U3CComposeUrlU3Eb__62_0_m072C70F108AD3F9040DDC45A21AFE646D5B3D90C,
	AmazonServiceException__ctor_m88B20ADAC7190F880CDB7F71F54B595D5A11F666,
	AmazonServiceException__ctor_m42C16BD3F963C32FEB57F93D1719CC90DB627D27,
	AmazonServiceException__ctor_m94B75970842642BE6786183DE6B8869BCAFF7ACB,
	AmazonServiceException__ctor_m6A5B1D6DD41F926B3921A96F5259093DDA64E487,
	AmazonServiceException_BuildGenericErrorMessage_m970CF299316AE7AAAF484943AB0E3ACB207C4ED9,
	AmazonServiceException_get_ErrorCode_m75826A8EC2D14F74E70526232C99F98B7C041D2A,
	AmazonServiceException_get_RequestId_m1D2210EE6BA445475A2808DEF65B7BDAF091EF08,
	AmazonServiceException_set_RequestId_m315D4D8F57B0E7D73F21B7D7E111DF88D2C674F9,
	AmazonServiceException_get_StatusCode_m010197FEFB2C378B5A1468F26C4D25441D2E4E07,
	AmazonServiceException_get_Retryable_mCC294299ADC8776E8326D9D147C7337123B79F36,
	RetryableDetails_get_Throttling_m7170652F32CB321788E87CF4ECE2C480575A1501,
	AmazonUnmarshallingException__ctor_m054743941E297950E9BA1A6A347C3F35EDE39DBA,
	AmazonUnmarshallingException__ctor_m4F19D2D72BE42684795E11878CD53FD8BF57AFAF,
	AmazonUnmarshallingException_get_LastKnownLocation_m125EB29655724CC65286914D205C57131C22B98B,
	AmazonUnmarshallingException_set_LastKnownLocation_m4EE878DAF0AE3328B94919195FEAEC0DB77BBB5A,
	AmazonUnmarshallingException_get_ResponseBody_m9D8553A581FAF5F4CCDF36335644A495DCAE109C,
	AmazonUnmarshallingException_set_ResponseBody_m40F84E8996C657ADA6F77DAFB15E1E6F9F901348,
	AmazonUnmarshallingException_get_Message_m806D2D3910A64060489B8095085E66CB043E6E44,
	AmazonUnmarshallingException_AppendFormat_m83E67B465618DB9E802DCA606C45855B7E3B0BDC,
	AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_StreamUploadProgressCallback_mE0AF7FC0693F8A85D1FC3869A412CC5A10CAA177,
	AmazonWebServiceRequest__ctor_m2F70AA8C6602940F5E5A6AEAE90FBED8247D9FB9,
	AmazonWebServiceRequest_FireBeforeRequestEvent_mC72CBA533E2E60AE5FDA10001315979D9444CDBB,
	AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_UseSigV4_m412C582CD42A6D7E416C3272CDD244B1BEF79AAF,
	AmazonWebServiceRequest_get_Expect100Continue_mFCD9C05B506D8BE115613A3F46BFD8F02FBFBA07,
	AmazonWebServiceRequest_GetExpect100Continue_m8AC2AAAA1DD5A9E1239C8613C46A4AB4186DEE80,
	AmazonWebServiceRequest_CreateSigner_m4369E2A18B540789A9482EF5CFCAA815F7021C6C,
	AmazonWebServiceRequest_GetSigner_m979A2525A61990A1A41E18CCF82DA42A9BFE02F8,
	AmazonWebServiceResponse_get_ResponseMetadata_m1BDDB043FE9F44E076FBED6BF4D08BE2D6ACCA1D,
	AmazonWebServiceResponse_set_ResponseMetadata_m0929AE71B25154E7425D9D40C2ACA914FE2865FA,
	AmazonWebServiceResponse_get_ContentLength_mFCF291D44F972FE27D2BA4C630C9F562A1384104,
	AmazonWebServiceResponse_set_ContentLength_m4C5E4B38EFCDB99622FDD4DBE4D542AE9BDA3110,
	AmazonWebServiceResponse_get_HttpStatusCode_mF9A27E46A989A83CF4AC58E052CFB9071B59654B,
	AmazonWebServiceResponse_set_HttpStatusCode_mE5792336FEED176D4987EA43118D46DA9542B160,
	AmazonWebServiceResponse__ctor_mBD52EF4DA0C1AC9176DC586E5BFC88F85F511788,
	AWSRegion_get_Region_m8917D9F0C32A90ED6803DD6035A7044346D3D3A4,
	AWSRegion_set_Region_mB70ECDCFDD779661E7DE6617C4E81A3A8BE3EFB2,
	AWSRegion_SetRegionFromName_mC03637E049081AF5273F2F0CCCE7ED2A4E05B5CB,
	AWSRegion__ctor_mA158F6E83C9E532A1C54EAF6A38AC064FD7E813F,
	AppConfigAWSRegion__ctor_mD6FD98570AF873CB3C6EB78CC015E346AACCF19B,
	EnvironmentVariableAWSRegion__ctor_m7106EB2A57B56C27F9047BD42C3784843A1D8D31,
	InstanceProfileAWSRegion__ctor_m3C29510FC9E67DDC5343CD21495748D37D3F390B,
	ProfileAWSRegion__ctor_m6F1FC59819C2630D1C259DD536A57E848B5A39A5,
	ProfileAWSRegion_Setup_m051F8B424419AE54579F925C6555A6679E771867,
	FallbackRegionFactory__cctor_mE5DA73FD84A3B8088E6B988F240E639A8023758E,
	FallbackRegionFactory_get_AllGenerators_m91FD3D6C134BB04D7FBB03849689A828C3AC0C44,
	FallbackRegionFactory_set_AllGenerators_mD213A68B42DCC5793C7423C4B5C5564E78CAEF46,
	FallbackRegionFactory_get_NonMetadataGenerators_mF1FA51D6894FF14E2917C483BDA3FE7855C42CE9,
	FallbackRegionFactory_set_NonMetadataGenerators_m515F25404FC75B4E202732360614A438D7EE04B5,
	FallbackRegionFactory_Reset_m59907C2F261973796CDA95937157C6E3D6879779,
	FallbackRegionFactory_GetRegionEndpoint_m6897B70F10A5566BD928929A7E34F54720F1D87A,
	FallbackRegionFactory_GetRegionEndpoint_mF07456A644CD99C27BDA7CC2FB89836B61D685AA,
	RegionGenerator__ctor_m2AD1FB11C62FA4D67DA4B8D4DFC8C705AFA7E374,
	RegionGenerator_Invoke_m6F52D04CCA59028F6EC796025C791238F2DD6565,
	RegionGenerator_BeginInvoke_m6A21CB9D64163AA946BC580459ECA54403CAE92F,
	RegionGenerator_EndInvoke_m81752F124FBED2958E07A4639D0FA0CD935C9B62,
	U3CU3Ec__cctor_m60A3B3D1125CC7482344C4E85FFD6E8798955B52,
	U3CU3Ec__ctor_m678EC579177D25204F7D308AEFB815C198A1D4D9,
	U3CU3Ec_U3CResetU3Eb__12_0_m14EF72EF709DB471C4FDDA57BEBE37A318099885,
	U3CU3Ec_U3CResetU3Eb__12_1_m8CBE8412EDEC3CEF0AB0B2797714EB06D351ACAC,
	U3CU3Ec_U3CResetU3Eb__12_2_m1814344C7C1B0C3200C0DFA20960A660BB41A3C2,
	U3CU3Ec_U3CResetU3Eb__12_3_m9FEA735FE4C10176E6C50ECCCCD0F2EDF7DCC258,
	U3CU3Ec_U3CResetU3Eb__12_4_mE2A855BD524E973FE32FB7D82E8F235F95AD4554,
	U3CU3Ec_U3CResetU3Eb__12_5_m4CA8D90B9B4E9D35A0AF62F7C81944D4D3BDDDA4,
	U3CU3Ec_U3CResetU3Eb__12_6_m7A8B4547E7C3563A1607FB58B3D38EF6A81DAAC6,
	NULL,
	ClientConfig_get_UseAlternateUserAgentHeader_mB995968596D86EA4C3F86EE5D0FD0A25C4176571,
	ClientConfig_get_RegionEndpoint_m80C9A3EFB58C0A50E2FD72830FBAE0C51D14FEF0,
	ClientConfig_set_RegionEndpoint_mC0C36203BCE0F2423D15D593A156B100A41A003E,
	NULL,
	ClientConfig_get_ServiceURL_mC15E70DF37E90F12CB9C3691A2C45EE9BFE52ACC,
	ClientConfig_get_UseHttp_m9481604979E4A54608D46F36C05D4B951E28908E,
	ClientConfig_DetermineServiceURL_mE544C57CAB2F7FAE0930C59A259F056CBF12A816,
	ClientConfig_GetUrl_m95B1CE1CAB4C8E2DC41055FDE69B02891C1FD00F,
	ClientConfig_get_AuthenticationRegion_m9D28EC5CE3E459D7394EB0B34C25497E9D7CCD7D,
	ClientConfig_set_AuthenticationRegion_mE889E4383A9E29BF1DC95BAB61EFC13D38C9B6FF,
	ClientConfig_get_AuthenticationServiceName_m2FFEFC4BDFB096831245668D5ED7F37C793FEF29,
	ClientConfig_set_AuthenticationServiceName_mA6A0E4F14EEA4C9A42061D4C18AEF69785CF83E1,
	ClientConfig_get_MaxErrorRetry_mE2A7F7B51E478FABE7A21E2FEE2189245FF7E934,
	ClientConfig_get_LogResponse_m5C721823B15B63CB40F1196FD50AFAD3A940BDD6,
	ClientConfig_get_ReadEntireResponse_mFE290934CE221EE00873AC73C41E2B4982A19BED,
	ClientConfig_get_BufferSize_m1BA2B8823FE547F3CF1920DF29A98E17EFC22540,
	ClientConfig_get_ProgressUpdateInterval_mA19752A937BC09D8645395ABD1B77B291CCDAE95,
	ClientConfig_get_ResignRetries_m0B9A8B53A897B8AECEA4A3B4A0DCD4A55D92DDBF,
	ClientConfig_get_AllowAutoRedirect_m5E00830CA97BFF67E6D8D918C534E8F833D7B929,
	ClientConfig_get_LogMetrics_mADAAEEB50CE79C72590C1F2797FD524E1E3885B6,
	ClientConfig_get_DisableLogging_mE5AA6E81FC5ADA31E26C9CAF4A0619BF0E18AC39,
	ClientConfig_get_ProxyCredentials_m9128B7ECE39CA6B29DC8F530C1C819EDA99AF5E2,
	ClientConfig__ctor_mF7458A55808D9FE37F6F49A9E7E1BBE807B3181D,
	ClientConfig_Initialize_m7F90A7048BD3D20D366E8DD2C1AF6AD66A9328EE,
	ClientConfig_get_Timeout_mF14C1C1ABBA09A73A885E48C63B197C5301023B8,
	ClientConfig_get_UseDualstackEndpoint_m36A52EE3B608E96BC55633EF97248ED52E1B1050,
	ClientConfig_get_ThrottleRetries_m9222EC99CD4482544EB543104151C7B511F08BB5,
	ClientConfig_Validate_m76BA2C392370797F8FA544AB856D34B82B65F974,
	ClientConfig_get_ClockOffset_mFF326BE6146583A2B98039B7551D314C67B12670,
	ClientConfig_get_DisableHostPrefixInjection_m47F08E0D0EEEE3B07A82D1B92013944BF2B3796E,
	ClientConfig_get_EndpointDiscoveryCacheLimit_mF61894B150EA0BA1CA4CD455C0EDD64E486EDBA0,
	ClientConfig_get_RetryMode_m160445D72F69CA1D77CE864155AC67975188747D,
	ClientConfig_get_FastFailRequests_m2DB2B6D28509361EB03619C7EC8877029EB33316,
	ClientConfig_get_CacheHttpClient_mBA013FDFF04D44B821A2E8E3D3EE20A95C1A016A,
	ClientConfig_get_HttpClientCacheSize_m594A9DEAB0E4B8EE58F51D2C3EF457F9CF8F87F8,
	ClientConfig_GetDefaultRegionEndpoint_mDCC4A0A294BE092CFFB9AE4C5121079C8057A3F7,
	ClientConfig_GetWebProxy_m1007629B8E16CCF84231B620D2FC3959AD75392E,
	ClientConfig_get_MaxConnectionsPerServer_m62F57F07092DFB2641B38B93452E6CF8837C40A2,
	ClientConfig_get_HttpClientFactory_mDDFE40B1D7108CEF000727555013D413D998F732,
	ClientConfig_CacheHttpClients_m8A21AD97C8518644E62BA568300AB96E94872A59,
	ClientConfig_DisposeHttpClients_mB2C51F60C2ECA614E0493365479FE236B67912B7,
	ClientConfig_CreateConfigUniqueString_m8B0E24E11F39E25BBB84B95F3CF16333F45454A7,
	ClientConfig_UseGlobalHttpClientCache_m78D7D6711B74611A01E38E683ED7A5AE0409D12F,
	ClientConfig__cctor_m0CD8DB005C47731A4ACFE8CB5080C32D039DE42D,
	ConstantClass__ctor_m2E56E06C64185F9EAEC5D3630DF2C35E6E8E2E9B,
	ConstantClass_get_Value_mD7D685AF95A226A4BB1990F8EA037319629925D1,
	ConstantClass_set_Value_m6780475A4086C5357CA1146CDE36646336E3D958,
	ConstantClass_ToString_m59D24CF5E76A91EFB9AB533D0C32CF9C5C61A646,
	ConstantClass_op_Implicit_m66D411C2C8F30059CBDF13282013A60AD5D95B90,
	ConstantClass_Intern_m378C71741BD211DD2A756EFDF320C6D603A501E1,
	NULL,
	ConstantClass_LoadFields_mE8E0523E6109959FFC625AEF9C77ADF9BA2ECF56,
	ConstantClass_GetHashCode_m2C6AE6F5DB7515722384F280AF845ABEEA3E8B7F,
	ConstantClass_Equals_mB56A1342C2CE4DDC7591822F4FDC72083CC92688,
	ConstantClass_Equals_m7357D3007D5FC9B70CE66BBA6EB099E769283459,
	ConstantClass_Equals_m56CFFDEAFF8B9E3F60BD362190D526E44CD68A0B,
	ConstantClass_op_Equality_mD7367B7F61F6CAF50AEE822393D7EDD80B62DFFD,
	ConstantClass_op_Inequality_m2D2895C8CC0CD5ECE29B6AE26FD4F95DD7F63D9D,
	ConstantClass__cctor_mBFA1C77C86AF58DF497985AED6660F46B1D94CC5,
	CorrectClockSkew_get_GlobalClockCorrection_m7DCE300816422EB94B5D0CE927EF302B2F793810,
	CorrectClockSkew_GetClockCorrectionForEndpoint_m7DDCFBA01F81402EC4D278C6AD5922CCCEBA6244,
	CorrectClockSkew_GetCorrectedUtcNowForEndpoint_m607062892AF9EF76ACB1091EBFE8E7C49B01A857,
	CorrectClockSkew_SetClockCorrectionForEndpoint_m806ABA5DFAEC7D00B4D5034AEB58A7F69C102CB5,
	CorrectClockSkew__cctor_m303C0F9372391238BEDAF195FE50EE825304C9F9,
	AnonymousAWSCredentials_GetCredentials_m46F4F14A5E1F3E23D0D6814C803716CF8134A7BC,
	AnonymousAWSCredentials__ctor_m1FB7877C947EF2EAF5360563F234290E1CE7C9F0,
	NULL,
	AWSCredentials_GetCredentialsAsync_mC6A301BB520C02D65E6DA9196AB8214EF58854C1,
	AWSCredentials__ctor_m40219DF0F5E9875EBD0127595453F37060A286A0,
	ImmutableCredentials_get_AccessKey_m509B5AE44459C85FBEADA67014CF2921BC8893C4,
	ImmutableCredentials_set_AccessKey_m04293A46CAAE7BEAF677E502A611FDB32A449D56,
	ImmutableCredentials_get_SecretKey_m8277ED2C42DC5F2180CFCC7C142DB6ABAB12BFAF,
	ImmutableCredentials_set_SecretKey_m71E51B0FB54A61F43B8C6CCA0B5D78B94BE74F63,
	ImmutableCredentials_get_Token_m084E4F55E325D68EBBE74EDAEEF101E29D6748E4,
	ImmutableCredentials_set_Token_m85A958A0783A5889E0F7429C0E8D624E0D70A7B9,
	ImmutableCredentials_get_UseToken_m562113AA82685945C8742DF8AD0E6B9BFA423EFB,
	ImmutableCredentials__ctor_mA1B9576B57F723C53AC9F591B930CCC964AA0227,
	ImmutableCredentials__ctor_mBE44F71C8EA5998875DBD0CD76C6BFAA934AB5F3,
	ImmutableCredentials_Copy_mAFF2F626B1B26F1ECB31335E9F3BABA72D043354,
	ImmutableCredentials_GetHashCode_mE9EE32697664541F246703BD98E90EDADFFC8A56,
	ImmutableCredentials_Equals_mAD33704068FBB88234B7F685CC76F768A6D1C68C,
	RefreshingAWSCredentials_get_PreemptExpiryTime_m26220A24D051767D3A64013B452E5AFE25C46A9C,
	RefreshingAWSCredentials_GetCredentials_m3EB48E5DC2E9522B2704EBF6454A45BA0E190702,
	RefreshingAWSCredentials_GetCredentialsAsync_mB595D535B6BFD152A8051EAB4C2C297BCEE6B7EA,
	RefreshingAWSCredentials_UpdateToGeneratedCredentials_m479C8757127ED2CF738B664584A40A30868A3DE3,
	RefreshingAWSCredentials_ShouldUpdateState_mA5FE6091272BEDDECE28169535EBB9B249318BA0,
	RefreshingAWSCredentials_GenerateNewCredentials_mC2EB5641B686E03CEE3B731A0B332B0BECF64AD8,
	RefreshingAWSCredentials_GenerateNewCredentialsAsync_m44B357056ED386C764C27F1458E49F94B90C1FE3,
	RefreshingAWSCredentials_Dispose_mC9D6C17509D5011D21E0C3A9844B7F8E250C84E3,
	RefreshingAWSCredentials_ClearCredentials_m4D98EBD5597C8737E00EAC748F8D8C39931761B5,
	RefreshingAWSCredentials_Dispose_mBB2058CBA8F9135C1D75A6270ABB6CD7B9F00F82,
	RefreshingAWSCredentials__ctor_m32CA1E300BA753E32D73A3169771C0F0D13FF3A9,
	RefreshingAWSCredentials_U3CGenerateNewCredentialsAsyncU3Eb__16_0_m0FC03D1BEEF1A6854CDAE4736471A16EDB04A301,
	CredentialsRefreshState_get_Credentials_m6C85D5BD9D58FEB3DE818A71424499EF455DADB2,
	CredentialsRefreshState_set_Credentials_m107E3DADE4BFD6157B147BE7438BD649F1A2AD01,
	CredentialsRefreshState_get_Expiration_mFA7346BBB61AEDB3A90A8012518B54433EF7B17A,
	CredentialsRefreshState_set_Expiration_m1D4B0D4D19FEC97F0DD52569E60D4AF367A481D5,
	CredentialsRefreshState__ctor_mF42D5DFDF74C5C5B1C3BE07234B86ED1A0E06C7E,
	CredentialsRefreshState_IsExpiredWithin_mD65E84E9F6A1462BA48466191470E97403229499,
	U3CGetCredentialsAsyncU3Ed__10_MoveNext_m005DC9454535AFC3DDDADBBC988058FF8A4875A2,
	U3CGetCredentialsAsyncU3Ed__10_SetStateMachine_mFA86F065CDB16A8B134F7E86E7E51EFEBE33081E,
	ExceptionEventHandler__ctor_mAF47BE12EEAE11B92679FC63A064BC1A4C016069,
	ExceptionEventHandler_Invoke_m52A024CF85D630A1C6496092ED7F1A5A1E2C8877,
	ExceptionEventHandler_BeginInvoke_mCA6D132EA956495AD7668881AB229FF40F14E3CD,
	ExceptionEventHandler_EndInvoke_mF6610B57B4466108CF26C583451F846477451370,
	ExceptionEventArgs__ctor_mEB9C374B8087936FF7AED1392F0E474EA89AD8F9,
	WebServiceExceptionEventArgs__ctor_m222BEA133F885D8DEBF6EA270C480BB86F23E48E,
	WebServiceExceptionEventArgs_set_Headers_m308C72B975C6934CC942A2690D429FC1544C04AA,
	WebServiceExceptionEventArgs_set_Parameters_m5E27A0995E6B14D58130418E0563E57BE1327AA1,
	WebServiceExceptionEventArgs_set_ServiceName_mEEA7C88A76BB3147789C6D9B9CAE7B45F5B3F5DB,
	WebServiceExceptionEventArgs_set_Endpoint_m753A6846890C931016AB3E9746D32CC29A3C7499,
	WebServiceExceptionEventArgs_set_Request_m975F088853F42DBE16625E28A06C8BC3F6B61631,
	WebServiceExceptionEventArgs_set_Exception_m670BD3838487CD3BC56C8111A55DBAF0B6AB98BC,
	WebServiceExceptionEventArgs_Create_mF3972242383310195B89A4565C3D6D63474CB317,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParameterValue__ctor_m14513C26A3DA99BECD8894F635142019EA2682DE,
	StringParameterValue_get_Value_m6805935D4A23333C6204D77BC47A40B5780B2F01,
	StringParameterValue_set_Value_m2BDDB0D689259FFAD473566188DD655BB972ECD1,
	StringParameterValue__ctor_m07D760E9C8B49EA94E35210577D82634CE99B393,
	StringListParameterValue_get_Value_m8E742CAEA3583E1393F735531020B553C977A593,
	StringListParameterValue_set_Value_mCBB88C38BF207C8182ABC8B9F2B3AEFA3C8E3D16,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HttpClientFactory_UseSDKHttpClientCaching_m80136D61159EB95ED0E94914232AD6118CAEDC8F,
	HttpClientFactory_DisposeHttpClientsAfterUse_m3C9064AF28E15E3AD3B0C270ABA2B99C8BC37891,
	HttpClientFactory_GetConfigUniqueString_mBF78F30875151039670D53710ABF89E947DD58FE,
	HttpRequestMessageFactory__ctor_m1223A8366BA23F0D90CBB87F06DF519B90D7D8BB,
	HttpRequestMessageFactory_CreateHttpRequest_m52AE6BB0D65B13AC0815CA07EA7C80DA8452DFEB,
	HttpRequestMessageFactory_Dispose_mD557F2C91E1506AE02B0E655359619CFDF00E97D,
	HttpRequestMessageFactory_Dispose_mECC3D9D8BF8068D3BD77799CDC4343B7E173BA65,
	HttpRequestMessageFactory_CreateHttpClientCache_m2025426D9CF87646E681E6487542C8F7CA920A51,
	HttpRequestMessageFactory_CreateHttpClient_mBFA721C6B0154392AEC7295DFF639D93134D0935,
	HttpRequestMessageFactory_CreateManagedHttpClient_m35F5C8EC4F9E3E3FF67240DD056FED8AB54DF00A,
	HttpRequestMessageFactory__cctor_mE9C1F676FFF18B77E6C3742F131A1282B6779612,
	HttpClientCache__ctor_m1637D80D04CC94B3EC13C477DFADDD8831424352,
	HttpClientCache_GetNextClient_mB7691BEC8B0306F67E2017F9F7AB348A3CB02AB5,
	HttpClientCache_Dispose_m754BC02BFA3AC4114E6895A86757DCD28C2E3FA7,
	HttpClientCache_Dispose_m983BDD4FC51B58EB011FD2770130E54AC9DAD92F,
	HttpWebRequestMessage__ctor_mB0046BCD5EF3615B24AE09ADD9AA9B0C1587EC02,
	HttpWebRequestMessage_set_Method_m633B297B3153DE58624F375C044C6E684B0F703B,
	HttpWebRequestMessage_ConfigureRequest_mB48B9450537567252FB4D7FC499E8C75CBC97B18,
	HttpWebRequestMessage_SetRequestHeaders_mF9FFDB919E7E9FAB5FB3703C5CB3D53517870736,
	HttpWebRequestMessage_GetResponseAsync_m093528F50DB252F5D33A06C9F20250CE885DE094,
	HttpWebRequestMessage_WriteToRequestBody_m9087C73D8AA358E048A45BF86BB45803E4345D9C,
	HttpWebRequestMessage_WriteToRequestBody_mC61F7A3E1A5C26A892511F9D53B9BC70F49B8711,
	HttpWebRequestMessage_GetRequestContentAsync_m261C2DAE58ECF3152277EFC83D28B4EF29B70400,
	HttpWebRequestMessage_WriteContentHeaders_mCA24144AE7A97EEE86CD74DFC07C4302DCAFDEE6,
	HttpWebRequestMessage_Dispose_mC202EC06AD67D3B90C975E0FF1C54B5643B0CFC7,
	HttpWebRequestMessage_Dispose_mDCF6D05433B8B6336A0A8E63D6D7EAE8B7920DA1,
	HttpWebRequestMessage_SetupProgressListeners_mEBE72A5CDA0BFF9649760F55AD61A5FB811C529D,
	HttpWebRequestMessage__cctor_m1C34BEC61216B03B1DEEC923357D9F70A43967F1,
	U3CGetResponseAsyncU3Ed__20_MoveNext_m929B5DA511CAF4416ADF67FCD460262B60BE78F8,
	U3CGetResponseAsyncU3Ed__20_SetStateMachine_m10865D2EFB43097DB415EFB5DB496B817C850ED5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RetryPolicy_get_MaxRetries_mA76CE36A06EFC6A3935BDB64429EA5B6C596BB53,
	RetryPolicy_set_MaxRetries_m36D4FB81E32CF4F8465CF6094FA4743C9D37F273,
	RetryPolicy_get_Logger_mE0AF8B9D9FC0BD25BBBAE14A26ADF916FF5610F2,
	RetryPolicy_set_Logger_m68A68C566F9C95673F60129AB1BD87D1EEC8E5A4,
	RetryPolicy_get_ThrottlingErrorCodes_m2CF20C9AD038CD8F4AA224BDB68351B424A3ED6D,
	RetryPolicy_get_TimeoutErrorCodesToRetryOn_mD151D14490561845F443491871D89725648AFFDC,
	RetryPolicy_get_HttpStatusCodesToRetryOn_mA009A938783A07A9CEB7717806D9967308C5CA5F,
	RetryPolicy_get_WebExceptionStatusesToRetryOn_m329A25793DD3B758615EA509FA6B03F6A120C330,
	RetryPolicy_get_RetryCapacity_m63B7EA20A13720F8E9A9C5C82641A54CEC0E97D3,
	RetryPolicy_set_RetryCapacity_m088CC1BB9F5588B6D254B74CFA9F6533E214CA7C,
	NULL,
	NULL,
	RetryPolicy_NotifySuccess_mF1BF382D1529CB7EFEE5EF0111F4B617D8C49C21,
	RetryPolicy_OnRetry_m17CB684AB1AD7DA4F642620BCFD24D2B3FE9B1A3,
	RetryPolicy_OnRetry_mFDE79FA3B43B3324625C020D30BBE7649D862869,
	RetryPolicy_IsThrottlingError_mEDA45FB14255BDE672AAF48EC30BCED5FF46AD63,
	RetryPolicy_IsTransientError_m46A215231514296D07C97B3647321A4721ACD621,
	RetryPolicy_IsServiceTimeoutError_m8737ABD27DB78C97175669CE54FE2E6F8CD5A704,
	RetryPolicy_IsClockskew_m22FFFD42064A5D7FF027E137345EB5ECF5DB05F2,
	RetryPolicy_TryParseDateHeader_mCF27A2B35632C45F76AF04C8B664E6B06E86BA14,
	RetryPolicy_TryParseExceptionMessage_m6A2FADB91AA2589F39C24320B19888060DBEFBE3,
	RetryPolicy_GetWebData_m185A6932F19DC8B57128FD63AF43A37136064630,
	RetryPolicy_RetryAsync_m3D741AF314A36B2C421CD7717A0A4098F6DA88E4,
	RetryPolicy_ObtainSendTokenAsync_m712BC6BA91B0559324AC38A62D843F5053324F6A,
	NULL,
	NULL,
	RetryPolicy__ctor_mB928C9108D858BA8596B5DDA0A0A84A1FCDE807C,
	RetryPolicy__cctor_m8A5CCBC746EBDEFF3BF439B125F040B2F9C2B576,
	U3CRetryAsyncU3Ed__57_MoveNext_m1678AA3BDDFA116D31DCD6FD44884DEEAF509ECF,
	U3CRetryAsyncU3Ed__57_SetStateMachine_mF497DCFE92BB472842C19A106F09007A860FFF71,
	PreRequestEventArgs__ctor_m9E55322D1287AECED08BB8B8F7E5F140F05335A6,
	PreRequestEventArgs_set_Request_m913512B993407176BDE17EED7CCDB3B8EA466979,
	PreRequestEventArgs_Create_m29F3F956990463D3CD7B9D3574D781827A9DE50C,
	PreRequestEventHandler__ctor_m0FA3CF4F9E61954A7D51A1C64939234A4564E3C4,
	PreRequestEventHandler_Invoke_mE82C1E06AE4144E0A5E30D23389430C5289A8B8C,
	PreRequestEventHandler_BeginInvoke_m21CCD614AF4E7102832B2D200DC0CD2EBF659627,
	PreRequestEventHandler_EndInvoke_m470A8927759AA6E6A9B89256F8FCF92E494729E6,
	RequestEventArgs__ctor_m606A1C3A35E8CD95ECBC60A763956C7A9ACF341D,
	WebServiceRequestEventArgs__ctor_m26CDF2DD6CA813BB2C91A5CF5C99E5F8E85BD575,
	WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5,
	WebServiceRequestEventArgs_set_Headers_m4C4474DD9D0B6975CEE99E3ACD66C38585195278,
	WebServiceRequestEventArgs_set_Parameters_m304A0C4EF2B41D03A1D35C78411F1240BDE41606,
	WebServiceRequestEventArgs_set_ParameterCollection_m63448435DC566936F2FACB1C97C9DAD1963E2F65,
	WebServiceRequestEventArgs_set_ServiceName_m0A8B8B8C719817D7B4AC4CC873E84672367B7692,
	WebServiceRequestEventArgs_set_Endpoint_m719E6225716C3AD740266C4DB15DDA9F11CD59A0,
	WebServiceRequestEventArgs_set_Request_m2E93015C782370542C385BF39139235D647C81B6,
	WebServiceRequestEventArgs_Create_mF007FE7AFF928E0F60DC0F2693034B98B19C9227,
	RequestEventHandler__ctor_m570E54DC4EF47898C92E313326A679EE57D8E3B5,
	RequestEventHandler_Invoke_m4D81B0B79F54EE87949AA4FA30AEB42227FDF009,
	RequestEventHandler_BeginInvoke_m78E9AFC9969D387315FCA143266D871435A5488D,
	RequestEventHandler_EndInvoke_m64B9F1FD6B812D7FABFB020ABD9EEC7E9E36FA5A,
	NULL,
	NULL,
	NULL,
	ResponseEventHandler__ctor_mE95AC96FFDAA846B237B061995D4C2699B822101,
	ResponseEventHandler_Invoke_m3669BBD4BD6AED2549E0EF32CADA9F1A3C7901C5,
	ResponseEventHandler_BeginInvoke_m09DBE6ACF8EF50503D7331FEF4278C7E70A6F20D,
	ResponseEventHandler_EndInvoke_m340EF4EFBB9BF2F42158CE1C843C1C46F116B18D,
	ResponseEventArgs__ctor_m0D378EF39FE4823A8AFE6E70F7E2EE2D1EFC430C,
	WebServiceResponseEventArgs__ctor_m79D0373CC1E803E2561891D2A45C08223B53F0FE,
	WebServiceResponseEventArgs_set_RequestHeaders_m0F160670222462F92D8AC6B4E49400F2C5098EAC,
	WebServiceResponseEventArgs_get_ResponseHeaders_mB3B05CF7F3CA81AD0E15756A6F1785D33BE103DA,
	WebServiceResponseEventArgs_set_ResponseHeaders_m437D99FE61C57DC60BFABE2FD4D5FD1FB6675C5F,
	WebServiceResponseEventArgs_set_Parameters_m35607FF08CF6574B51713FA9C4B042C9C70DDFA3,
	WebServiceResponseEventArgs_set_ServiceName_m08E80F5EC7828B0B97D34DA6C95E64A34531EA2C,
	WebServiceResponseEventArgs_set_Endpoint_m77D2AC3E87BB0C1997933A63273252B383496281,
	WebServiceResponseEventArgs_set_Request_m74545AC00FF2931AC9D2FEF9F6AB7359459E0722,
	WebServiceResponseEventArgs_set_Response_m6B8ADF83266A32FAB4DD7EF7C8494BF4E807B251,
	WebServiceResponseEventArgs_Create_m1E2408AEEE67540FF63E713DB0F11F512CB3249C,
	ResponseMetadata_get_RequestId_m28C6345C23135E757E664A3ED9773E2A8CA0A5B2,
	ResponseMetadata_set_RequestId_m411505C12EEDA793D530C7A65CD9C1198D5DFFBB,
	ResponseMetadata_get_Metadata_m0B17E9487DF8F850A0D9D130F0EBFCD83BD038C1,
	ResponseMetadata__ctor_mAB772A283F3A4C1F14D1F22A8CC837B34CCCB637,
	StreamTransferProgressArgs__ctor_m25C105F8AA3D53FE2B5255E3E0E8320DF282A894,
	StreamTransferProgressArgs_get_PercentDone_mFFF8215C86D6D191473C82E1F4F527FBB542CCD0,
	StreamTransferProgressArgs_ToString_m06B13291A479B52935A0BDE10EA3E2CC1D6A8596,
	MonitoringListener__ctor_m654872655C419AB72A018161969B5FE14DA1D36A,
	MonitoringListener__cctor_m825FDCA988FC906C350CF2DA12742053A2BE6C2D,
	MonitoringListener_get_Instance_m4F926136E26C14D14A24DB1DC11E7DD09709B3D8,
	MonitoringListener_PostMessagesOverUDPAsync_mF045DACA39A35CE18A8D5C15D023D0389686A072,
	MonitoringListener_Dispose_m7793DA0C212776DC5499744CBC0A7EFC0134C118,
	MonitoringListener_Dispose_m3158048A5161DF8048B15EE847BD97A64B78659E,
	U3CPostMessagesOverUDPAsyncU3Ed__10_MoveNext_mEDBD4C9D6810E90196AF654DF1B20F41C6AD1B00,
	U3CPostMessagesOverUDPAsyncU3Ed__10_SetStateMachine_mE5C9EEDE31FEEC56BCF88F148813303D2AA51CAC,
	CredentialProfile_get_Name_mB713E06DF9022D5FFE37AD44EB9CCF31B82F60D3,
	CredentialProfile_set_Name_m63BC3AD462D0CF3669C11B75A37B3A72BB68FFDF,
	CredentialProfile_get_Options_m4BEDDAF9941BA979CC731717C17A3FA7623749D3,
	CredentialProfile_set_Options_m7651DA7DA24A064F6178A6E6EC92DCA807641CFB,
	CredentialProfile_get_Region_mE253F69F7886AD25DA66A653E39109186664EC95,
	CredentialProfile_set_Region_m44E8C475F95D6C64B4AF534CA00C14267C09A795,
	CredentialProfile_get_UniqueKey_mB5E4CE2360F0A7A4ED514F50670DF31C51A7BDFB,
	CredentialProfile_set_UniqueKey_m0EA7516DBFD617FF619D864790F48CC9A653B3DA,
	CredentialProfile_get_EndpointDiscoveryEnabled_m3DD6687CA952054E30AB5E108C586BDC1A2493DC,
	CredentialProfile_set_EndpointDiscoveryEnabled_m266929EB8A3C01BBDB26483D547F0E2113AC7E97,
	CredentialProfile_set_S3UseArnRegion_mD66A32EE76676B6E0D9A398A0D6643F4ED720CCC,
	CredentialProfile_get_StsRegionalEndpoints_m629728B051B1A50E72E33D61D6E94DDC7A162A26,
	CredentialProfile_set_StsRegionalEndpoints_m7AD9C668F3A944E2CBD5A358F3A6918E996F9CD7,
	CredentialProfile_set_S3RegionalEndpoint_m54FD9379579C26441119FEBBCB3B3F0F21B1DC09,
	CredentialProfile_get_RetryMode_m73DB6E189368FE3F055AFEAA9F924EBED95D3A08,
	CredentialProfile_set_RetryMode_m244DD300B11793D33F00593DD421B8598F1050AE,
	CredentialProfile_get_MaxAttempts_m3AB54AC420F35373634610FAAE1C62EFDEB190B5,
	CredentialProfile_set_MaxAttempts_mF8CFD52861CBC44DCA46556C0E43E68F0FF890A7,
	CredentialProfile_get_Properties_mB2D597C31DD0808A0163AF3521862E607EB2AE46,
	CredentialProfile_set_Properties_mAF13B979A257CAFA4275C5DA2BA58491899B8154,
	CredentialProfile_get_CanCreateAWSCredentials_m9B0F5A0E9F3F7332BD7BEA6427263B5E273A3D1C,
	CredentialProfile_set_CredentialProfileStore_m8EA2A883DECA4473D43CE84A3DC15EA7EFD49097,
	CredentialProfile_get_ProfileType_mFD862D0784CA967C0E34F7610E8D9971047725C5,
	CredentialProfile__ctor_m2BD2CB9D2B7742BF59038E8C6B8B8EF70A72CBFD,
	CredentialProfile_GetPropertiesString_mAB1975A23364E9D6BECC078872F5BD3C90115D2D,
	CredentialProfile_ToString_mCED00D55E2C68AFAB61F9BBB53203A87979411BF,
	CredentialProfile_Equals_m6B6720C477C6B504D19C3275CE79DF788071FFEA,
	CredentialProfile_GetHashCode_mE404618614EE3088BFE003CE7A695E6F851ACA48,
	U3CU3Ec__cctor_m0D34C068838518B558D0F73A46006925242F5617,
	U3CU3Ec__ctor_mB856A3F46409ADD8DC9737A1DBC81E7B4F63E1C2,
	U3CU3Ec_U3CGetPropertiesStringU3Eb__59_0_m122F8CEC220CB294971DE637A4C938995AB94573,
	U3CU3Ec_U3CGetPropertiesStringU3Eb__59_1_mF517DD46DC06E589AF7F5DD5239B59259EE2BF69,
	CredentialProfileOptions_get_AccessKey_m54D2DE99D522F53500F3CD0FD9388812C2AF6BD3,
	CredentialProfileOptions_set_AccessKey_m8F3AD526CCB0CC1DAD563F0364ADEE05A91B85E9,
	CredentialProfileOptions_get_CredentialSource_m1D564B618015AF6BC155329EF0EE4D6DF8C16A52,
	CredentialProfileOptions_set_CredentialSource_mE1EF94AB93F049279711F301C2DAEF14F149C0A6,
	CredentialProfileOptions_get_EndpointName_mA13C59933C46404D3017FCB826EAC2DDBD5E7E8D,
	CredentialProfileOptions_set_EndpointName_mA51B88631A57B8BD75F330661072C58FB1961D8C,
	CredentialProfileOptions_get_ExternalID_m64A40A52D928228D64CD8C7B10DBF3101E5CF033,
	CredentialProfileOptions_set_ExternalID_mCB2664BFB5E632389C706417724A2D52BBBDE95D,
	CredentialProfileOptions_get_MfaSerial_m9F3C36B9F19852F4EC2BDA5C61CB7700160053C0,
	CredentialProfileOptions_set_MfaSerial_mA43E21320CC090A5D1F2D2EE9C763ACEDBCE5459,
	CredentialProfileOptions_get_RoleArn_mD33C1E609E6DC33FCBA1F100DB756B3CE18F6CD0,
	CredentialProfileOptions_set_RoleArn_m3705C867F98614A1B66A34EDADBBA009690EEE3A,
	CredentialProfileOptions_get_RoleSessionName_m59225087CF2D7BB1A79F122FBB9C3D6DF136F5F3,
	CredentialProfileOptions_set_RoleSessionName_m31748E87FCE77B513CBCB3D9C8BB9B692493819E,
	CredentialProfileOptions_get_SecretKey_mE9227433E5F455B2B3CCAB940DF1EF346D587439,
	CredentialProfileOptions_set_SecretKey_m3D8CC902AD4C18EBE5F63747675EDDD4536AB7AD,
	CredentialProfileOptions_get_SourceProfile_mCF1A8615E110EA66E829E79313B1AA7BAD904D36,
	CredentialProfileOptions_set_SourceProfile_m9E621D9FD6D7AB1FDD6933D6FC46CC1CD02DB6B8,
	CredentialProfileOptions_get_Token_m67BF5840EAD9E17CF3B5741B23F446A8D568F1F5,
	CredentialProfileOptions_set_Token_m9FD9FD44198997D786FF87FE35AF41A0D6BB70BB,
	CredentialProfileOptions_get_UserIdentity_m4239DE2133B4968497CE6EA73849891F2B1797B7,
	CredentialProfileOptions_set_UserIdentity_m90374CF6A6BB801369B7089063281017876E835C,
	CredentialProfileOptions_get_CredentialProcess_m06365B5DE3931629A94EAFE9651915EF32DCE968,
	CredentialProfileOptions_set_CredentialProcess_mA6FD2353B05476E70C062F1C9B9A3A0E2A493ED6,
	CredentialProfileOptions_get_WebIdentityTokenFile_m816A1C6AB30B2CF7F3D8C564558BFFDF5C357F21,
	CredentialProfileOptions_set_WebIdentityTokenFile_mA959CF718B74550D6CB663BD0880B8A635F4A742,
	CredentialProfileOptions_ToString_mD89BD0FBC6E76C42D96150A4C4053A58098DD9AE,
	CredentialProfileOptions_Equals_m4D0A5D591E178B91B9F1BD39C8818631B6751609,
	CredentialProfileOptions_GetHashCode_m1D89072C1958AF5B39849195488EC516B52BD373,
	CredentialProfileOptions__ctor_m633D1CD30EB8F55A25AEC00BBC9DFAE5ECCEBD61,
	CredentialProfileStoreChain_get_ProfilesLocation_mA618D2CDAF7EBE2C16FFD0F2BB91B7449C0AFA90,
	CredentialProfileStoreChain_set_ProfilesLocation_m58C470C2CCA2358F7B72478DA1FDE835881E8A77,
	CredentialProfileStoreChain__ctor_mBD8151AFF8C1C777D1CCC32CDB18368BE6A6A9D3,
	CredentialProfileStoreChain__ctor_mE0D8B61630360070022B476E6C5659BBA3907B27,
	CredentialProfileStoreChain_TryGetProfile_m115E1C55847B63C92C759EE337BC02D4436A45FF,
	NULL,
	NetSDKCredentialsFile__ctor_mF33C3D60477D87371BA807F37F37CADB7DA8CEDA,
	NetSDKCredentialsFile_TryGetProfile_mD3634409916E43015EB7FBD9150BA5D9A5BA9E01,
	NetSDKCredentialsFile__cctor_m923F91DFC39AF9945CCD83EF43E6B915D2130AFD,
	SharedCredentialsFile__cctor_mC2C6C46ED894B10F3E45BAD0C6F4025177FE9607,
	SharedCredentialsFile_get_FilePath_m13D49F7E2F5200941148407607A118BA8BD85596,
	SharedCredentialsFile_set_FilePath_m03BAF421690A3CECA50363586E082EDC0F03901F,
	SharedCredentialsFile__ctor_m51729BC7BA919C2E22830B0C17F50F77E143354F,
	SharedCredentialsFile_SetUpFilePath_m354B20B8157F2B077AF63DF90AB98760EB6D030E,
	SharedCredentialsFile_TryGetProfile_m620C076FF3591E6467424039A123D5B023B744CD,
	SharedCredentialsFile_Refresh_m49D2C93D9DC7D6099298D79A3FE68705A1E1AEEB,
	SharedCredentialsFile_TryGetProfile_m8758A295A2861E2E17D3871C4627FDB1EB1291FA,
	SharedCredentialsFile_TryGetSection_mA87F12C2AF2B0407E147579E79433A642BECDE6E,
	SharedCredentialsFile_IsSupportedProfileType_m88907B3DB9BC1FB6549EA37ED11459FF4968A666,
	CredentialProfilePropertyMapping__ctor_m808A38ABA5C086AC8ECF0A3AC08198DCAB37D74A,
	CredentialProfilePropertyMapping_ExtractProfileParts_m71C5B27F8F649607917478878E5F8CAA1D8340E9,
	CredentialProfilePropertyMapping__cctor_m990A6CD68F7B2C506B48D5C07555A2717340F033,
	U3CU3Ec__cctor_m5E0267D292EB7FA8706BFC30A5174454BC066A23,
	U3CU3Ec__ctor_m647B7D43D58F9689AABC207A7E908501B40836A8,
	U3CU3Ec_U3C_ctorU3Eb__4_0_m4B49B3668FA89288376C2E7AD7DE80AF971DDE85,
	U3CU3Ec_U3C_cctorU3Eb__11_0_m3F8EAD33F994A7572397261805364DCD235FB706,
	CredentialProfileTypeDetector_DetectProfileType_mC85E8602C55813422B39F16CED3520D928819327,
	CredentialProfileTypeDetector_GetPropertyNames_m52DCA1D68A9EF8E78D89136007DDB5CD59B05ADF,
	CredentialProfileTypeDetector__cctor_m7172C236AB6C4D0DDC2FD726A1BB77A6CBE003B9,
	CapacityManager_Dispose_mADAA054C494613C69974BE1D2CA882EEE12B9C96,
	CapacityManager_Dispose_mC5123115DA474A4C2A7BC5A95B803F7FBD009CF5,
	CapacityManager__ctor_m19D7FBA86E81FFFAF49D7AD3259584E9B817CAE1,
	CapacityManager__ctor_mAF4B10F1BD7879222AE94A7EEE64C26A7B675632,
	CapacityManager_TryAcquireCapacity_mDCAF2DA054EA178DFF1BDDA27CBE770C42407E9C,
	CapacityManager_ReleaseCapacity_m0344B0A0899653A82A83A86AC0C9A12482587AC8,
	CapacityManager_GetRetryCapacity_mBA5E2403E32B6F2669903571EFCCB4605157DF96,
	CapacityManager_TryGetRetryCapacity_m62667620B36BBE90401E23072AABBA7630FC4600,
	CapacityManager_AddNewRetryCapacity_mA988ADFCDBAE5F94E0638F20B151D90EB117F20B,
	CapacityManager_ReleaseCapacity_m61F8EAC09838A80C2B86C13AE75372A881E94C01,
	CapacityManager__cctor_m46CDE29DFF069945BA2E1C120FF3AB6C9A66AAC5,
	RetryCapacity_get_AvailableCapacity_mCD9C04903EC7590720F4C00841A1A69DF0628611,
	RetryCapacity_set_AvailableCapacity_mF627A8F4B70ACE8DFB8B93C56CB21DDDAD273261,
	RetryCapacity_get_MaxCapacity_m39DCE957C8A8D844D2906AC47F6056F1BDCE4F37,
	RetryCapacity__ctor_m4C9B75AB24624164768CF6257CECC4FABC0B6076,
	AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6,
	AWSPropertyAttribute_set_IsMinSet_mF2D9D9E5E5A2AFB8E96933B9A7D1F36BBF0ACAB1,
	AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B,
	AWSPropertyAttribute_set_IsMaxSet_m098F9CAB1E60CBA824EF649B7AF3F4180F08A0A6,
	AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880,
	AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D,
	DefaultRequest__ctor_mCC8FA797C2692B14AAF4BB563BB011A03642DAE3,
	DefaultRequest_get_RequestName_m0960B241AEAF6E581276C15D813458F427D66DA2,
	DefaultRequest_get_HttpMethod_m9FC095E23BC90C23CEBDF723443DD44F5D132402,
	DefaultRequest_set_HttpMethod_m773B4B5664377E728264D8D3C57D36250AE15DA4,
	DefaultRequest_get_UseQueryString_m3931BD2C759651F4B96BCA62F51B928DA32E6D8D,
	DefaultRequest_get_OriginalRequest_m06E93CA8E0C70CDB6D3F008DC7F21ACC85EE9C9A,
	DefaultRequest_get_Headers_mC1510D36397AEBD9B7CDE4197BD4882A9755962A,
	DefaultRequest_get_Parameters_m27D82D2B212D91834CAFFBBE323FEF2038D34AD2,
	DefaultRequest_get_ParameterCollection_m4A30C2F0C3461104F5BFF1C09572ECEE3C91838B,
	DefaultRequest_get_SubResources_m6934358912AB0465F91B635DC8E96524B233A575,
	DefaultRequest_get_Endpoint_m845E94EAE98049DAE107DF4C91D380CDBB77D2B7,
	DefaultRequest_set_Endpoint_mDC75E73C52BD13AB3672E044750EC6CEDD7267A5,
	DefaultRequest_get_ResourcePath_m1D95E3E7D008895DD98BB39945810B4CAF8357B9,
	DefaultRequest_set_ResourcePath_m279D4BC36B238F6786FEE6FF1891BAF886BB13B8,
	DefaultRequest_get_PathResources_m9786E119DC798D2929393A27311B1818E7332543,
	DefaultRequest_get_MarshallerVersion_mCA53CC55FEAC354A864D3E89B51DD15E0FDD12B8,
	DefaultRequest_set_MarshallerVersion_mE7F265B1EB7AF6805EB1EF4BBD338728B42FB14E,
	DefaultRequest_get_Content_mA0E5E2A2E2C9D84D90941D463D60476153CFABC5,
	DefaultRequest_set_Content_mD1D65AB53D468885128597B28113AD91B5124DBE,
	DefaultRequest_get_SetContentFromParameters_mB03C1E564A7BB85E7B1840F5EE7918AEBE977309,
	DefaultRequest_set_SetContentFromParameters_mD83CB98B12FDC98258DDBE891F195215600A56DD,
	DefaultRequest_get_ContentStream_m0B8B56F28A65CE6C877F9BF38A260A3E63D3B815,
	DefaultRequest_get_OriginalStreamPosition_m23A07C35FDAEED501ADC8B540525DE5960EDCDEF,
	DefaultRequest_ComputeContentStreamHash_m91DCD02670498A8220E323C3A38E5A5DD690365A,
	DefaultRequest_get_ServiceName_m60713D0D8427B1C943FEC2F1C0CFB3A981337B32,
	DefaultRequest_get_AlternateEndpoint_mD083189EF0A1562A97FD51676E83D113A4962C76,
	DefaultRequest_get_HostPrefix_m10D9DC3D8FB879A77D66612DABA751217A92A03A,
	DefaultRequest_set_HostPrefix_m6E4E12A783326EF99A979ECF1992D7C4AD28E1B5,
	DefaultRequest_get_Suppress404Exceptions_m9EBECC5B37C54D8027C015CE9A8E4C8714E02398,
	DefaultRequest_get_AWS4SignerResult_m2E075FE2E9038A89E9EB365206B9101FA627EF8C,
	DefaultRequest_get_UseChunkEncoding_m5375AF8E3FF31E65C632F3DEAD6E859885ECF329,
	DefaultRequest_set_UseSigV4_m4CA850E5CD4FB0F28EF2408B6D443B731AF10C7D,
	DefaultRequest_get_AuthenticationRegion_mEC4FD9325E607789DA3E0AB657F4997D1EBDCFFA,
	DefaultRequest_set_AuthenticationRegion_m7390CFC4C888DA4876576238BF48C2E5884028D4,
	DefaultRequest_get_DeterminedSigningRegion_mF6982E156E6F30E8611F4672B86241104D76C8DD,
	DefaultRequest_set_DeterminedSigningRegion_m3DF45EF335C58F20A251970CE54E8E61C842AD38,
	DefaultRequest_IsRequestStreamRewindable_mEB9DCABC62BBB38147555F5C0875D46837914041,
	DefaultRequest_MayContainRequestBody_m543A5B89D29195C6F706DBACB0C0E0353FF787A4,
	DefaultRequest_HasRequestBody_m3993A6C9EBAFDF99C940667E41C297FC48E78BB7,
	DefaultRequest_GetHeaderValue_m144A8CD7DF9F45713368E043911C827A185EB621,
	U3CU3Ec__cctor_m219E434C34F94BA18E02466FCCC89CE09CFDE904,
	U3CU3Ec__ctor_m781F38E383FE02F45DEC7EDD4994D08F67056453,
	U3CU3Ec_U3CComputeContentStreamHashU3Eb__68_0_mA6FF4428A581D9CF6FF0F5C85AE7839DD274C512,
	DiscoveryEndpointBase_get_Address_m477487CD6AC2144E8292EAF321ED01AC31A8FAA4,
	EndpointDiscoveryDataBase_get_Identifiers_m811566AB6803F875E4981394FB8B3E7EA5CB06BB,
	EndpointDiscoveryResolverBase__ctor_m8DDCBABF364689A3448107D1790F14AF803C0E5D,
	EndpointDiscoveryResolver__ctor_m5C1170C8ECE0E17EA9AF08C2C865685507023750,
	EndpointOperationContextBase__ctor_mBBE00029F9394734536361C9D016A0600878CCD2,
	EndpointOperationContext__ctor_m04D78DDCE20117D4C1046F505C1F8BA20BC3C5D7,
	ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06,
	ErrorResponse_set_Type_mE53555DD966531864E2D76017ACFB4BEBBACBBDC,
	ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD,
	ErrorResponse_set_Code_mB45D11E4AC2905CC68C3703BD276AA7D7FF352C4,
	ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188,
	ErrorResponse_set_Message_m7C450B5EFD404E19C84070BF859275A7F95C595C,
	ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34,
	ErrorResponse_set_RequestId_m7469C3C6D31632D79BBB4C5A6AB456D311D0FD41,
	ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6,
	ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF,
	ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461,
	ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156,
	ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C,
	NULL,
	NULL,
	InternalConfiguration_get_EndpointDiscoveryEnabled_mCE81E1B1BC2AD1FA28359A0C9132350EF0A94D07,
	InternalConfiguration_set_EndpointDiscoveryEnabled_mDE3516A6EB931D9DE0ED43832E3CD9D32F4E0AF3,
	InternalConfiguration_get_RetryMode_m85D04441ACF031F843F40340FE4D2B269735297E,
	InternalConfiguration_set_RetryMode_mAC0C563C32C2EC4FB8005DE17A71A95DE47907C4,
	InternalConfiguration_get_MaxAttempts_mA1F79F4677829F49F656D48A56F8E012B27BEBF2,
	InternalConfiguration_set_MaxAttempts_m16CC99498CF834DD7FBF28FF931D323333B44775,
	InternalConfiguration__ctor_m8BBCE021F0134246225C316719FE673862E07074,
	EnvironmentVariableInternalConfiguration__ctor_mB1C8F4EBE75AB65779B5E87B856BD76C56220A4D,
	EnvironmentVariableInternalConfiguration_TryGetEnvironmentVariable_m22C913542F67A5F58F68F3D343E076BB8D6FBB29,
	NULL,
	ProfileInternalConfiguration__ctor_m9B419E3A67C3B3BF25C275AAD07445086F319BC9,
	ProfileInternalConfiguration_Setup_m6D79F0188907415C288D3532747498F4FC09EA48,
	FallbackInternalConfigurationFactory__cctor_m0D712020F71D5ABCC8878850D16CF3491F5DEE49,
	FallbackInternalConfigurationFactory_Reset_mA02AAAAD24F8EB9B505EB543D9736D16E261EBA0,
	NULL,
	FallbackInternalConfigurationFactory_get_RetryMode_m9C4E3FEA407729E28AC31A35065398CD1FCD63BA,
	FallbackInternalConfigurationFactory_get_MaxAttempts_m6D112251D442489B5C15AF13714653A424D3C6F4,
	ConfigGenerator__ctor_mFD796D73FB51679A1F6D77CB8E4047B6AF787E7F,
	ConfigGenerator_Invoke_mD930904ACB5D86DA058039DE5275440E7230177A,
	ConfigGenerator_BeginInvoke_m9269D9BCAE8AC139F73D7A2DA956F53DDB473686,
	ConfigGenerator_EndInvoke_m67D13F7B8C485E8C8FE7979E6BE954D5CD99F695,
	U3CU3Ec__DisplayClass4_0__ctor_mF9905E1013D0D1B61A0265846F5385FDC2B9C2DE,
	U3CU3Ec__DisplayClass4_0_U3CResetU3Eb__3_m2D72D5EE0BD4283D73D54DF76ED41A1242280C8F,
	U3CU3Ec__DisplayClass4_0_U3CResetU3Eb__4_m0258D79B4B282ADC7AD4689F54F8BAE1F420763D,
	U3CU3Ec__cctor_m95E37308F75F94A0E6C5682EB2461BF0F654D3ED,
	U3CU3Ec__ctor_m19E4948641CC27025FF54FB466F763C81E45B73E,
	U3CU3Ec_U3CResetU3Eb__4_0_mF25A1FB57CE6CA69DD9D07ADE16F1B76D46C3B0B,
	U3CU3Ec_U3CResetU3Eb__4_1_mCFE46EBC3CFD6AB20267B2F393C6A683E2465AE2,
	U3CU3Ec_U3CResetU3Eb__4_2_mD0E22CEDAF497BE380B06736E3BAB23BE9DC1980,
	EndpointOperationDelegate__ctor_m0F9F904B122AF0C0641DB7BB34006EB8402ECC63,
	EndpointOperationDelegate_Invoke_m8D9F9EB0EAF2AB1BD2C6251B873DB6D498374E78,
	EndpointOperationDelegate_BeginInvoke_mD7B5641417D64DBE5C0DDF6AFE641C821CE99E8F,
	EndpointOperationDelegate_EndInvoke_mE3774E72F97D7E9992966F1E366A0C48D30DAD3F,
	InvokeOptionsBase__ctor_m61BE9BE2BB377F74B5265537270D1152C509C9F6,
	InvokeOptionsBase_get_RequestMarshaller_mF638ABC860AF48DEDF250B8A0C4B34F6006838D9,
	InvokeOptionsBase_set_RequestMarshaller_m8F3F59E9E9E5F5BFD51CDA1B87986151F34B2EDA,
	InvokeOptionsBase_get_ResponseUnmarshaller_m9E52A88C379AA1F997073F599691071A64252252,
	InvokeOptionsBase_set_ResponseUnmarshaller_m80053CE2242C5FB45F67617A493D4D15F3074FA7,
	InvokeOptionsBase_get_EndpointDiscoveryMarshaller_m6DF8A9D7F49C84CC812D39D35BD81B23A8DAAB5C,
	InvokeOptionsBase_get_EndpointOperation_m394BC4A6DE1373AE1D269CAF359E05A44C5E12C2,
	InvokeOptions__ctor_mC8DD478F8404260C05FD2E6FF4D26C944E216AB4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParametersDictionaryFacade__ctor_mA46D0CC4803CFA0532227183E0BD911A150CF2DE,
	ParametersDictionaryFacade_ParameterValueToString_mDC76C12BDFC47DF01C1A1678FF49B2C693BC7E33,
	ParametersDictionaryFacade_UpdateParameterValue_mECC3B2412F1BBE125124B05F5FEFC91B39AD93B1,
	ParametersDictionaryFacade_Add_mF25984CC26DDB348535108D6CFEF7DFCE470386E,
	ParametersDictionaryFacade_get_Count_mB2B9E778B9DB3AB12B887A3F45364B72E1D659BA,
	ParametersDictionaryFacade_ContainsKey_m544509E254D2B91361E3DD3C24087A7078A0B40D,
	ParametersDictionaryFacade_Remove_m128D7A50B77DADF41ADCFBEC71E3AACD74C2803F,
	ParametersDictionaryFacade_get_Item_m39F1E34DE4273C9E21CC28765BB1046006FD19AE,
	ParametersDictionaryFacade_set_Item_m7E018D41A02016019CE13E533181E5B0D9EFEECD,
	ParametersDictionaryFacade_get_Keys_m2407889BD7B5A6593D0BE8ADE3A20FF1B3093CA1,
	ParametersDictionaryFacade_TryGetValue_m42E0E06216BA90722DA938AB9CD473E436B6E248,
	ParametersDictionaryFacade_Remove_m085FF61C83511C16A76960801C6265E5982F258D,
	ParametersDictionaryFacade_get_Values_m1C1D6EAE66DE5BE9E80D70BCB9BC02B6527934F0,
	ParametersDictionaryFacade_Add_m7343C37917CEC82C6BF41A79A63900FC12948860,
	ParametersDictionaryFacade_Contains_m4857E89F63A65BCDBBE30C4F92F57CCA3EFD22E5,
	ParametersDictionaryFacade_CopyTo_m441A00076F7E8EC0182D326EFF8CA7CBF5F2388B,
	ParametersDictionaryFacade_get_IsReadOnly_m87E6F85E3B7EF8BBB1322BC8A709710FCB44DCFC,
	ParametersDictionaryFacade_GetEnumerator_m8106C6F93EFBE6BBC5E921ACFBDCD8198F97D8C9,
	ParametersDictionaryFacade_System_Collections_IEnumerable_GetEnumerator_m7BA5D4B55EF8E37179729DEF2E16BF0771984AD5,
	ParametersDictionaryFacade_Clear_mE60CBF431A8588ECA6E988F792F5922D5071B7CF,
	U3CGetEnumeratorU3Ed__23__ctor_mF2A47390FF6A0D9DF41AEDCF316A05038745DAD3,
	U3CGetEnumeratorU3Ed__23_System_IDisposable_Dispose_mCCB562FCB94036DF98F8DDD82D0CEBC92739E7D0,
	U3CGetEnumeratorU3Ed__23_MoveNext_m3CEFF87DF2AAA6DD792EE636DF5A6B355051494F,
	U3CGetEnumeratorU3Ed__23_U3CU3Em__Finally1_mC2F612DA526D2BF4D1F13C2EEBFD9E080903C396,
	U3CGetEnumeratorU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mB770895B34BB9A97BDD81972BAA878115A963E80,
	U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_Reset_m7CD8581DD59E442903A7DEA8F5A7A6019A5FD816,
	U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_get_Current_mE07999BA03923739E386C7B5BD271FC010E714A8,
	RuntimePipelineCustomizerRegistry_get_Instance_m0FCF38817786A1D0F32D0DC785EB0A056AA6F060,
	RuntimePipelineCustomizerRegistry__ctor_mA5D660F0E11641FDBB7FAB5D5C177AA4D868B234,
	RuntimePipelineCustomizerRegistry_ApplyCustomizations_mC32328A9DAFFE74313678DD33441554229F86DF0,
	RuntimePipelineCustomizerRegistry_Dispose_m4C0FA1C5411FFD6D4D648CBF1E4C7C68E7605976,
	RuntimePipelineCustomizerRegistry_Dispose_mE5B8C16C466436BBA2FE5FA389AB8CF709453BC8,
	RuntimePipelineCustomizerRegistry__cctor_m90D783B628137581BEDC98C9622B77B619D9DB3A,
	NULL,
	NULL,
	ServiceMetadata_get_ServiceId_m42AA96D72D2FD71B2EC4A9B4B33DC1B9A835A4AF,
	ServiceMetadata_get_OperationNameMapping_mAE5C256DC7C08383E6FCF215B18ED0D477D2DAD9,
	ServiceMetadata__ctor_mA39791F0BDFF6859A7D20D79422A56F7BFDAA1E2,
	StreamReadTracker__ctor_m71F55F97D28B19A39DFE9ED48E843175CD3169FB,
	StreamReadTracker_ReadProgress_m8662F4043206EB20524AC4012B7128E6B1E70983,
	ParameterCollection__ctor_m15DCE498499A9FB590FAA1DFCB2E2B6AFE483A5F,
	ParameterCollection_Add_m579741858D341585CE795481B02953B3A7DC52D0,
	ParameterCollection_GetSortedParametersList_mC858545EDE1D937A92DC4A2C9841308FE9DF61C8,
	ParameterCollection_GetParametersEnumerable_mFD6A666A3B962D2B53212BAFB5D24132C33A21DC,
	U3CGetParametersEnumerableU3Ed__4__ctor_mA3660F51DEA5FCCA18250DF86B1E4228CE3004BC,
	U3CGetParametersEnumerableU3Ed__4_System_IDisposable_Dispose_mB5E20B4637F27495FBB2EF920877949FCA12FFD1,
	U3CGetParametersEnumerableU3Ed__4_MoveNext_m8C6085C7C60018355B4B6F105C49874D084FD986,
	U3CGetParametersEnumerableU3Ed__4_U3CU3Em__Finally1_mD2F027189ABA718D43B212181B851FEAC2902CAD,
	U3CGetParametersEnumerableU3Ed__4_U3CU3Em__Finally2_mFFEF852C54083F2401306C74FCEE7F3A27854629,
	U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mD78AC053140A1678AA5390E3B84194BA638B732E,
	U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_Reset_m440496B3B80A3C4CA4E26640D5297498FDABED23,
	U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_get_Current_m4B1FA65DB4B72667C4995D7FA54F47701A8EF03C,
	U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m8556E7C7BFD90DCA9FB7837CB5D1FBC3C2F22D6B,
	U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerable_GetEnumerator_m583C549C1976AF018F67004591B37239E0FCA2EE,
	RequestContext__ctor_mBCA4AA9FD7A8C54147C3496790F1726A18C20838,
	RequestContext_get_Request_mA4471DE841DE086DCA9A7949E3E906BD9E8EA7BF,
	RequestContext_set_Request_m4D9502541AA3E426E0AD100D820F57097F0F3359,
	RequestContext_get_Metrics_m8245B5AD45A4136FEA4A41FD0E9AE03BF910137D,
	RequestContext_set_Metrics_mFBEC125FE4162520753F26D54BA2ECEE87D7173F,
	RequestContext_get_ClientConfig_m00A68FB365505864D204F7D9F0B4B3B0CA440976,
	RequestContext_set_ClientConfig_mB9CCDA771A29F9C7765C9AF67EAC5A88646874E0,
	RequestContext_get_Retries_mB59731D9636F968BAA1706BDC5C07626A3FE1219,
	RequestContext_set_Retries_mD88CBDC3B30D3A4BAA83FBE3BE13A6A977F378E6,
	RequestContext_get_LastCapacityType_m0B7619EE743164D5DC511D6A1DD843A467A8C7DF,
	RequestContext_get_EndpointDiscoveryRetries_m98C8DB853AA051758AC68367C68DF25D71A7E5A2,
	RequestContext_set_EndpointDiscoveryRetries_m76846D7FF5CAA564393ADB168341B125C1C23C79,
	RequestContext_get_IsSigned_mC3D6CB04C958B62F4D4423D216C4B01B8D43089D,
	RequestContext_set_IsSigned_m92398DD2A210E0A2E98BE81442B7BEAA1F89AF82,
	RequestContext_get_IsAsync_mE367ACF5C06381343028C5A45BDDDFAEF3164912,
	RequestContext_set_IsAsync_m9F1AF9842CB659049A616F2A850C6A594490C9AE,
	RequestContext_get_OriginalRequest_m76BC7D9FCAFA242D20FE9834A4E18D6F921ACF8D,
	RequestContext_set_OriginalRequest_m5D45287D4130E982EB4EF26BEBEC1ABF28B4DEBC,
	RequestContext_get_Marshaller_mE77303460FA245309D9033950B4594DFCC9D5F2F,
	RequestContext_set_Marshaller_mBC9EF52693055C503B97AA6E185EEB5FE965CDE3,
	RequestContext_get_Unmarshaller_m5A458EBF82A9432CF3C2614970F4A40960CC8116,
	RequestContext_set_Unmarshaller_mCE129D11F8052AFDB6CF9165B7E791DAB77BBE4E,
	RequestContext_get_Options_m0A2898FF3EABBAB4E978889AF1BB379BA7B7B24C,
	RequestContext_set_Options_m4F9A2FFAE721D3CAE9C9407B99FACA305FBCCA0A,
	RequestContext_get_ImmutableCredentials_mC7ABFF395C149FF56563340D9AE520AA4CF82BCB,
	RequestContext_set_ImmutableCredentials_mB8A770A9E8FFDB3EBD95C661A64BE87A4F682AB8,
	RequestContext_get_Signer_m2BAD0D8E49B7E99383817286165C3CFB88AD1836,
	RequestContext_get_CancellationToken_mAB9910A6A46E6F719840B02B74007BC8F9D14A35,
	RequestContext_set_CancellationToken_m416969291C198882377AB7F302BA0DBC4E953D17,
	RequestContext_get_RequestName_m5613C0F6CC6E513DDCC3D8C17162AEB132A2B596,
	RequestContext_get_CSMCallAttempt_m02134A8CD42466EBD1745D01C390776F393F6177,
	RequestContext_set_CSMCallAttempt_m6A1FF6F8CF854F9298D2910A9D746EECEE87DE8C,
	RequestContext_get_CSMCallEvent_m241604A1F65FA848A747968080BC8D4BE19D9CAA,
	RequestContext_set_CSMCallEvent_m2A7C298840A43F98784D2447CAC0C7437FC385DD,
	RequestContext_get_ServiceMetaData_m2E19199234CA5F2676479B2CE363ECB0DE386B46,
	RequestContext_set_ServiceMetaData_m088026368D0314986B0E3C377E9BFFF35C627F21,
	RequestContext_get_CSMEnabled_mEC4878BD651F342B235ACB9E5B04B1C39EC376BB,
	RequestContext_set_CSMEnabled_m3F7FD5E4D36DD00A8515633DB76CFA1F1CF9AF0E,
	RequestContext_get_IsLastExceptionRetryable_m9CBDEF39DDEB049FBC67FEB11EA2E369078E50DF,
	RequestContext_set_IsLastExceptionRetryable_m06ED2876291D5203B96981CC608CE51BA73BADAC,
	ResponseContext_get_Response_m8B11E0CAD36313831E9E73DE776625B055C6E41B,
	ResponseContext_set_Response_mC3E7EC2795FC2AEDE791C0D6C5DD9CBDC2C229D1,
	ResponseContext_get_HttpResponse_m0DE5A537E04426B2FBA39A54E1A6ED490714B12D,
	ResponseContext_set_HttpResponse_mCDB00703E92D7AF877659F9C077D0A478DB6DD30,
	ResponseContext__ctor_m3941F56DDFCFDF9C1506EDC4EFBA50D73E3F5F2D,
	ExecutionContext_get_RequestContext_m65AB9054F3B778D154885B4C08576F7E56D73471,
	ExecutionContext_set_RequestContext_mFD3FB7214104B3EF2E0ADA06F9E381EBD986E502,
	ExecutionContext_get_ResponseContext_mDA86EFB7136DEA63A6C462D56D0C19DBF2F5CB94,
	ExecutionContext_set_ResponseContext_m10DEA16A9002732D37F74B980F2EA9267682EDF5,
	ExecutionContext__ctor_mAE96DA25DD104F5D74DE18C60F7F4202A32A9E7E,
	ErrorHandler_get_ExceptionHandlers_mB572E61E5C35D98C90A040312353890DC48ED0C4,
	ErrorHandler__ctor_mC862AC6C257D7E5D46B62CAA34E0A88F2F4F9FF7,
	NULL,
	ErrorHandler_DisposeReponse_m01AE65AD6A675871B03EC14F70E6D35A732845A6,
	ErrorHandler_ProcessException_mAA5D010039EF1A7F34BF331AB1248F2385EC19AF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HttpErrorResponseExceptionHandler__ctor_m8ED40C0382C8603F8946448FA917B2AC4C58E096,
	HttpErrorResponseExceptionHandler_HandleException_mA006BA596A01A06A560FD584530B23A5FCFD2F2B,
	HttpErrorResponseExceptionHandler_HandleSuppressed404_mCFD5A5BFFF47028CAE3E6B9E787A2177055BAECC,
	CallbackHandler_get_OnPreInvoke_m1A71F567B0855CDC3FF0EB544116E7172AFF2E26,
	CallbackHandler_set_OnPreInvoke_m87C3C20DF9BD145545EE87C806FEB7D9E6371A50,
	CallbackHandler_get_OnPostInvoke_m508D36C9CF19EABD021159EABB46E9BDEB3D8323,
	CallbackHandler_set_OnPostInvoke_m2B7939C80173D9408D0D31C441EF9222C946900B,
	NULL,
	CallbackHandler_PreInvoke_m674FC13DD0B7C6B8809AE34B5BCDC27736E2B93C,
	CallbackHandler_PostInvoke_m869DEDC14CE5BA5E6F0673320213D7217838E065,
	CallbackHandler_RaiseOnPreInvoke_mD698C514D684A541657FF58A17E9083BCEF18AD6,
	CallbackHandler_RaiseOnPostInvoke_mBBD4E7F01897523794EB310B64270E488E1E93F8,
	CallbackHandler__ctor_m9AAB9136A90D8544DD26358345E87F3993947B26,
	NULL,
	NULL,
	NULL,
	CredentialsRetriever__ctor_m2A5A0CE664B352B004791B069C6EC78ADEB6B4D9,
	CredentialsRetriever_get_Credentials_m536C03A6F14228A70E0FE37416B02E246B0CC698,
	CredentialsRetriever_set_Credentials_m0443A627032FBFA8D7D571BE129870693CB7A87B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EndpointDiscoveryHandler_PreInvoke_mF159347BB4140DB025FB3BAAC9DEE45E091F8025,
	EndpointDiscoveryHandler_EvictCacheKeyForRequest_mB1790C70A241D1D9357891E007E4C8694328A189,
	EndpointDiscoveryHandler_DiscoverEndpoints_m15B9D071C481BC29CF3EC9E42D7BDC7C57DDFC55,
	EndpointDiscoveryHandler_ProcessEndpointDiscovery_mA0893AA63A1C08D020D0CC33D4B2BFE57DF1372A,
	EndpointDiscoveryHandler_OperationNameFromRequestName_m76E9A312258F42D810A862FFCC6DEBA869DC80EA,
	EndpointDiscoveryHandler_IsInvalidEndpointException_mEE55FD9E2BC849F0ECCB0342E73D7BA29102D669,
	EndpointDiscoveryHandler__ctor_m87958934ECE67D4C7AB3FD672411552D6E5AF9FD,
	NULL,
	NULL,
	NULL,
	NULL,
	EndpointResolver_PreInvoke_m41ACBA2E127B354C416E986AC393190E0F673C8D,
	EndpointResolver_DetermineEndpoint_m0A2617AF5BD3BDF6E697786A613573BFD23982B6,
	EndpointResolver_DetermineEndpoint_m1DD4CBB04D09805F4CB4D8BEF3AD361EB06442FE,
	EndpointResolver_InjectHostPrefix_m9866706C753ACB0424D043717A252EF5890B1C54,
	EndpointResolver__ctor_m7718BD253418587E6040FFF8DAE2D212FBDAB6B1,
	ErrorCallbackHandler_get_OnError_m128B66FA238E4109FE664413CA8D53BCE0A3721E,
	ErrorCallbackHandler_set_OnError_m9B39DBF861DDF1523501DE9C1C7FBC96885AA0A0,
	NULL,
	ErrorCallbackHandler_HandleException_m679AF3F2F1BCD187799DCC57695F126502E520D0,
	ErrorCallbackHandler__ctor_m678D0A72D39FA10575760B858E4DBA1D8BEF3BBF,
	NULL,
	NULL,
	NULL,
	NULL,
	Marshaller_PreInvoke_m087615AE4831D4F3E6D7B442287B29EA8DBD46F0,
	Marshaller__ctor_m9292D6F9F48C23C321693194EC5E014408875ECD,
	NULL,
	MetricsHandler__ctor_mD2DE7F55D1036EF0513C0CF4FEE70A7CD57B1518,
	NULL,
	NULL,
	NULL,
	NULL,
	Signer_PreInvoke_m04CACF5F3C3366422C7E8FAF3358E4B291F46185,
	Signer_ShouldSign_m5FF13C3682C0CA20BB62ADCF1DD6C50CDF0BFD56,
	Signer_SignRequest_m38A9FDE9169C258C16EF26C36754782565DE1CAA,
	Signer__ctor_m84FD1835BB4E6649ABFAE9CE18794CC21AD3E882,
	Unmarshaller__ctor_m0EA12C5E751B722603AB282C4FAA2ED13AC8038C,
	NULL,
	Unmarshaller_UnmarshallAsync_m9EFFEA0695F1F4D6EB0388FF9465AA6EB981F4A2,
	Unmarshaller_UnmarshallResponse_mA90C284C5760AD8C61F4FB0D1ABD1F6FB166483B,
	Unmarshaller_ShouldLogResponseBody_mB176DCD73A65EDB8AF17A436BF99538BA14C190D,
	NULL,
	NULL,
	NULL,
	U3CUnmarshallAsyncU3Ed__5_MoveNext_mEFF02A4DC2C96E2D42CB3555EEB8D738EDBC2090,
	U3CUnmarshallAsyncU3Ed__5_SetStateMachine_m92CF4477AB1C3D3C38CED8DE399CC58D1ED0F3F4,
	HttpErrorResponseException_get_Response_mF300FD82AF9B72A5FAC48955B57D1A6C16570391,
	HttpErrorResponseException_set_Response_m0EFA97C19C16304F4A116E23F4405DC3012630D9,
	HttpErrorResponseException__ctor_m9BB2D9A7978D65343179B44DEB08D6F953E87377,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PipelineHandler_get_Logger_m079669ADD1EC8F4E440F64144BA3AF4EE7958403,
	PipelineHandler_set_Logger_m0C70A5AFC9FA063FB8A3E49FB0E9DF35BD663BE5,
	PipelineHandler_get_InnerHandler_mC14FC8E11103E7D2DD80C869934B005486069356,
	PipelineHandler_set_InnerHandler_m2CA2C10774C68B7556C3BC7CC3FD7FAE1E0D6677,
	PipelineHandler_get_OuterHandler_m3C8B44F35F670546B89E7CEC831456D952E00814,
	PipelineHandler_set_OuterHandler_mDD4DE1D53C5122E7AB75AA43F93C38967F105C74,
	NULL,
	PipelineHandler_LogMetrics_mD28DC7BB77DE6F69FAF1C5F29EB4C1DEC74CCCFE,
	PipelineHandler__ctor_m24C7252C3D0420B488D8F8E92C13EAAE84F83247,
	AdaptiveRetryPolicy_get_TokenBucket_m262D3EE41D0717E08267DA1261D9F9BAC003C85B,
	AdaptiveRetryPolicy__ctor_m61A2CEB18BDC539B358A992C12D59A8D700ED6DA,
	AdaptiveRetryPolicy_OnRetry_m02A696DB08710AD44AA0A1FAB54BADA6DD681DB3,
	AdaptiveRetryPolicy_NotifySuccess_m978D3764F2E4C720C1ADBF2A911B16A0026F0BA9,
	AdaptiveRetryPolicy_RetryForExceptionAsync_m31E28DE033B5BAEE29B2FE06811AAC2FD8270531,
	AdaptiveRetryPolicy_WaitBeforeRetryAsync_m9096E0D15A927CE5C22651E4ECD46294031ED3B3,
	AdaptiveRetryPolicy_ObtainSendTokenAsync_m5C3B0456589FE9949204CF28185B0F6EA6C55F95,
	U3CObtainSendTokenAsyncU3Ed__11_MoveNext_mF2D1AE364315836D27E81DFC6A89EC6AE0A7E305,
	U3CObtainSendTokenAsyncU3Ed__11_SetStateMachine_m5B189B23DDAAE678549DE1625AD265E0EBE6E43D,
	DefaultRetryPolicy_get_MaxBackoffInMilliseconds_mAD8744FAF8A1B9D70F9D864F64C1A5B5FE28AE17,
	DefaultRetryPolicy__ctor_mC9EFB0166A32D407F246DC47E4EA555774598C88,
	DefaultRetryPolicy_CanRetry_m41659AFC3EFBAF722ED7FBD32CE77229945F31E4,
	DefaultRetryPolicy_OnRetry_mCD53101E707719478326F0417F0264FAAB443475,
	DefaultRetryPolicy_OnRetry_m3AE0B5F55BABB08A4F0D64D698607B8BE34111CF,
	DefaultRetryPolicy_NotifySuccess_m197BBAE717AA2F3D9793F83E5CFE15C8B2D250A0,
	DefaultRetryPolicy_RetryForExceptionSync_m6EE260E7E9903A9EFD7578AB702C6115C9BBBA0F,
	DefaultRetryPolicy_RetryLimitReached_m260DE3904A49C2415B5FE5926A1C112D6CDE7E0E,
	DefaultRetryPolicy_CalculateRetryDelay_m56D66364664BF00D921A1A0C836EBB399B2EA964,
	DefaultRetryPolicy_RetryForExceptionAsync_mCE233F4A7A6B93366E6BC5AED47474C9B01DCEEC,
	DefaultRetryPolicy_WaitBeforeRetryAsync_m7270337ADF17CF597542FAA913461B6775C813AE,
	DefaultRetryPolicy__cctor_m0090C0706D4897FFD2A6D2E75349BF78351C042F,
	RetryHandler_get_Logger_mFFC5E13E0606E42A8C0309E7A721636A9E696AA4,
	RetryHandler_set_Logger_m5A1DDC0D6EED44C07B33A544B53CB478A4579DE3,
	RetryHandler_get_RetryPolicy_mDC30A963F84526B46BCB050863888F12D4F572BC,
	RetryHandler_set_RetryPolicy_m63A981A434D1D7B794D90E8768339391FA299A0B,
	RetryHandler__ctor_m8F983301F21F601924F126906C335A1DC8F830CC,
	NULL,
	RetryHandler_PrepareForRetry_mF87FF727AC2AF75EED2596DC3077ED47496BA4E0,
	RetryHandler_LogForRetry_m13921D86E65D150383352886EDE7E69665FB8C2F,
	RetryHandler_LogForError_m241E5B1F92D0C01591E666A7778E6E06BB849DC0,
	NULL,
	NULL,
	NULL,
	StandardRetryPolicy_get_CapacityManagerInstance_m5B53C8DF35CF8F30F2651706E2F2DC69289BD0F7,
	StandardRetryPolicy_get_MaxBackoffInMilliseconds_m6662682822FF79F5655C62F39E86FEF381F21A04,
	StandardRetryPolicy__ctor_m721B68567D0DDB0ED43A32844F5B8DB800CC2447,
	StandardRetryPolicy_CanRetry_m929825BBE67DB18C7E23784E39F541E1B1690EA6,
	StandardRetryPolicy_OnRetry_m0A7813681FC6C3BCD3A01E77E82ED83FB7E48526,
	StandardRetryPolicy_OnRetry_m2320CEB7443DC67FEB2CE34E33F890B65DA8CA55,
	StandardRetryPolicy_NotifySuccess_m887B0689ADC1B5BD82C29374FF140FA78827D911,
	StandardRetryPolicy_RetryForExceptionSync_mE06331665738BE50218F5B7BAE760D1D10B08219,
	StandardRetryPolicy_RetryLimitReached_m09FFDB9EACC1D08D73244D6A23EA859F9AFFF97D,
	StandardRetryPolicy_CalculateRetryDelay_m7038A8C14A2EEB2A71DDC4A415D8F79DB7EC22FC,
	StandardRetryPolicy_RetryForExceptionAsync_m1431EEF8D53BFDF75600CCE841CD203A97A2961D,
	StandardRetryPolicy_WaitBeforeRetryAsync_m1ADC5FEEE6EDDEC092FA7ED5B8FBBE3194CDE5F0,
	StandardRetryPolicy__cctor_m508B256B8D8B1FFEE6D73EEEEDDF9B16481443CB,
	RuntimePipeline_get_Handler_mE947032E5BC33E6EA93489B97D340A61EF05107D,
	RuntimePipeline__ctor_mDBDA5003B800B66FAC2A1FE65C60445E482F3026,
	NULL,
	RuntimePipeline_AddHandler_m36440BF38C30A9159493A058DD60781372712CAF,
	NULL,
	NULL,
	RuntimePipeline_InsertHandler_mD9AADCB0037E4111D4B35114450439CDEE2223F1,
	RuntimePipeline_GetInnermostHandler_mCE9998B71F55C07800349CCCAF43D06528484440,
	RuntimePipeline_SetHandlerProperties_m4C3424ECB28F84ED10EA7DB0799BD796C273F6A5,
	RuntimePipeline_Dispose_mFB7683DAB71E95B4786C29A81BDAF197904BD033,
	RuntimePipeline_Dispose_m768257612CF16E565E799B20071CC38CBAD3070C,
	RuntimePipeline_ThrowIfDisposed_mA71613F23F4A21C635C9959E8C3E5200E98C3AEF,
	NULL,
	CSMCallAttemptHandler_CSMCallAttemptMetricsCapture_mEE265B739A42FBD97E5929AA0BF6402C9A55D816,
	CSMCallAttemptHandler_PreInvoke_mAB527B2E25555F4EA2B5DF709CEF8665FD4DCB9E,
	CSMCallAttemptHandler_CaptureSDKExceptionMessage_mCBEE9D9246EF9D0D90B3DA0089247773CF5F5EA3,
	CSMCallAttemptHandler_CaptureAmazonException_mF6692471E19EA3E775018DBA525CDC9BA8B3466A,
	CSMCallAttemptHandler__ctor_m6CF7C324C210DD41EF9F7CDEB0B2F1552203CBE3,
	NULL,
	NULL,
	NULL,
	NULL,
	CSMCallEventHandler_CSMCallEventMetricsCapture_m125407A8E7A4635F48FB04E0AF1A44E796DA9A6C,
	CSMCallEventHandler_CaptureCSMCallEventExceptionData_m6A6446AF068C18EAFA2DEF7B9F1B8FC6701901E9,
	CSMCallEventHandler_PreInvoke_mF09B7A794A6A5790D934DEE53FC9F4317D83CB8A,
	CSMCallEventHandler__ctor_mBA8342C811C44D44250DADCAA429B7F5C8028D5C,
	NULL,
	NULL,
	NULL,
	TokenBucket_get_FillRate_mA0A7ED0D0D33364D5A61E2BBBC7E6C3F664727F7,
	TokenBucket_set_FillRate_m72489ECECCF952D97E9A6C0D9B7E28CE87DEDC12,
	TokenBucket_get_MaxCapacity_mE1472754E58556B1E734686A99F455499093F774,
	TokenBucket_set_MaxCapacity_mC9EE0F436FA9DA0B77AB46F8DDACE7EE35C8942F,
	TokenBucket_get_CurrentCapacity_mA421C72A7EED744D51FF1C8042EFB16E090F2C6F,
	TokenBucket_set_CurrentCapacity_mCE6F1E8099C66C0A073A45B760E7E74A8E0EA8AB,
	TokenBucket_get_LastTimestamp_m0FE13E882A81F71410D028982FB12C3A2FCD9158,
	TokenBucket_set_LastTimestamp_mD6473615DB7CC4E992DE2A09264467D9E62B7B12,
	TokenBucket_get_MeasuredTxRate_m1A629CE8A0D664B7A488ECA0893CBA3E5B3344CE,
	TokenBucket_set_MeasuredTxRate_mFB5DE4F1071C968447A87E109189008485EE0EAD,
	TokenBucket_get_LastTxRateBucket_m02293BF2983E261302A22A14DF81AAF421ED0779,
	TokenBucket_set_LastTxRateBucket_m218A404F7B552EB50D6C40D0140C222BF1DB83E1,
	TokenBucket_get_RequestCount_m05B019782DEB0D4D21A960F659A6E1B3B45DEBA7,
	TokenBucket_set_RequestCount_mB65440290DFA7CA01D193C6815F774E4E70E5CD6,
	TokenBucket_get_LastMaxRate_m18DF834E4844CEC6844F4DEABBF8B6AE3A1CB658,
	TokenBucket_set_LastMaxRate_m6F3B8E232E1213DC18CD217A37A2F345B2E0D8A9,
	TokenBucket_get_LastThrottleTime_mD3267451748BD6074DEFB8ECA779D9E630D10765,
	TokenBucket_set_LastThrottleTime_m88EAAC3991614FD2C871E79FBF147CDB303B4CBC,
	TokenBucket_get_TimeWindow_m7916F196613D45C135BD7A1E263326F5B4993F91,
	TokenBucket_set_TimeWindow_mDB11F5A40AEEE3B6F25ACA9AB5768DC7263819F5,
	TokenBucket_get_Enabled_m776C13DE5371FB8CBCB2E21F2999FEE99B03F5FB,
	TokenBucket_set_Enabled_m095AB1B9C82FFA6598F942926581D6D1AF7820AF,
	TokenBucket__ctor_mC73F1B30FE63F3D64924C2B3988B5EF92038E18E,
	TokenBucket__ctor_m27AAA8A68D72B56A02262DBC4BA55EE07AB1E203,
	TokenBucket_TryAcquireTokenAsync_mC4D9588455662EC0320D548DA857AF578A5A7C3F,
	TokenBucket_SetupAcquireToken_m6BFB05D4BA6560A90074B22FB32B2EF8F8AAB196,
	TokenBucket_ObtainCapacity_m7800AF5B7DD26EA404F6A29906CCE05338039E75,
	TokenBucket_UpdateClientSendingRate_m9F6F700B112780CBA435167BFB914EA1C03826D2,
	TokenBucket_TokenBucketRefill_m89A8A2FEC0B0E44DE526D0310974D7F9B484C11F,
	TokenBucket_TokenBucketUpdateRate_m0B7C52D2C1F1C589019073109093EA04623524BB,
	TokenBucket_UpdateMeasuredRate_m085E3CE72BFF2DDA61EAFA35051CD5B7A98158AE,
	TokenBucket_CalculateTimeWindow_m2FB4A15344D62CB72AC672AE0EEE5BF25664FD15,
	TokenBucket_CUBICSuccess_mDB554F541B22FE9746354AC13424AADA9591F794,
	TokenBucket_CUBICThrottle_m60EEB958CD59B5D8854F91D37A3EA3C26709F9F0,
	TokenBucket_CalculateWait_mF67858CACC98BD4DA9EAE3A9F14A3432343BA250,
	TokenBucket_WaitForTokenAsync_m928EB18E092E08F143F8EBDC694B5E9C3CB91CD5,
	TokenBucket_GetTimestamp_m398D3868281BF899BB9E53B04CAAD0CB36F5C745,
	TokenBucket_GetTimeInSeconds_mCEC49103AA3DA6213A365B4D92286D2B02031CAB,
	TokenBucket__cctor_mEAA861DF00A661A389BC5A1006E784D6FB20B39B,
	U3CTryAcquireTokenAsyncU3Ed__55_MoveNext_m2F7C74047347318A1AB0C15C16CDF097C9E0E7EC,
	U3CTryAcquireTokenAsyncU3Ed__55_SetStateMachine_mC8FB6D650565057DAF0C7310E6CFC027923C0B62,
	U3CWaitForTokenAsyncU3Ed__67_MoveNext_mDBAC6DA4007E72A458F4CD637B22715939DC38E3,
	U3CWaitForTokenAsyncU3Ed__67_SetStateMachine_mCA1B76DFBDD3ED10552418255052F57DA8067186,
	CSMConfiguration_get_Host_mB62FF9EA748848CC74E83DB1F44655C0BD243E92,
	CSMConfiguration_set_Host_m9B1428EB43FB9DC145988A0AE89AF5A0285BF471,
	CSMConfiguration_get_Port_m1E23C410430AA481A44D5B334219AA366F8C1059,
	CSMConfiguration_set_Port_m51AFCCA8066C2479050A5D4395B625A824D47475,
	CSMConfiguration_get_Enabled_m81AD5D5A20D6A39D2F8A2620EBC7B89AD6731E32,
	CSMConfiguration_set_Enabled_mD6BC7CD6553DC314AAB1B598F5F260335F8427AB,
	CSMConfiguration_get_ClientId_m4CEA76CCE2A19DF9F341B90C1001B38755A436A6,
	CSMConfiguration_set_ClientId_m5A2E035C2DBA99F74DA66203FCE3F5303BB83E3E,
	CSMConfiguration__ctor_m2347F69B8A36555D9C22792B38210AF059B247C3,
	CSMFallbackConfigChain_get_AllGenerators_mB2A3806671D707BA6166978686F6B5BA9FAF31F4,
	CSMFallbackConfigChain_set_AllGenerators_mFF5CE6CA4C99BD98545E405EA4FB105E41EB54A4,
	CSMFallbackConfigChain_get_IsConfigSet_m31F9CCE5FC84C9C093A4DCD333B9E34A3769EB11,
	CSMFallbackConfigChain_set_IsConfigSet_mA9BEC2B921058B43BED14457DF2A83CD720AB1CF,
	CSMFallbackConfigChain_set_ConfigSource_mB2E85549468F1E0237B189424F548A851D043F68,
	CSMFallbackConfigChain_get_CSMConfiguration_m529E6F75EA90D53EA97DF81FCC1F8AC8E8225766,
	CSMFallbackConfigChain_set_CSMConfiguration_mB622A47543892199ABEF2C8F79E586D01B3CD5CC,
	CSMFallbackConfigChain__ctor_mE475D0EA772F6219E4281D56778E764237ACB8E6,
	CSMFallbackConfigChain_GetCSMConfig_m428FB35FC6AD6C018DA2A82D1A7434F14DF28C72,
	CSMFallbackConfigChain__cctor_m7119AD292023B64759A50E62C3F3080414B4FE6E,
	CSMFallbackConfigChain_U3C_ctorU3Eb__19_0_m2AA81B437DA8CF71B653F813766CAD9A12B224BE,
	CSMFallbackConfigChain_U3C_ctorU3Eb__19_1_mFC07E82E37B78AFB08278848459549CC77CD5658,
	CSMFallbackConfigChain_U3C_ctorU3Eb__19_2_mCEB6395641F132B61A623792AC2A39C8F41D102F,
	ConfigurationSource__ctor_m45246B3630682C48A8AD509A61BFF40D12603123,
	ConfigurationSource_Invoke_mBAC172A6559EAB45BD378F7042F0035D9EB1F21E,
	ConfigurationSource_BeginInvoke_m41CE3867E3DC26D6EDD76350928ABABB1E4AE6AE,
	ConfigurationSource_EndInvoke_mC824F9C1E861939E1781ABE58E84B67818D17328,
	ProfileCSMConfigs_get_ProfileName_mBDAFA68A6EB56A0C3F5779C8D3DCC4F9336F0519,
	ProfileCSMConfigs_set_ProfileName_m4AF99B4E75DB4445951F8F25191C5BF1E69090DE,
	ProfileCSMConfigs__ctor_mA2EF7F56AC946C979BBD995A1024623AC220F845,
	ProfileCSMConfigs_Setup_mD5D6F3521858634B4237CA5B5F572046C285E538,
	EnvironmentVariableCSMConfigs_get_environmentRetriever_m0DB2581E62898D47F6616E31AA36CAF0146E028E,
	EnvironmentVariableCSMConfigs__ctor_m98203DB9D48990DEEA63375C7C4D2990832F1E73,
	EnvironmentVariableCSMConfigs_SetupConfiguration_m595BFAB9E6A8FE2CE7A72791CB756A95B11BBA43,
	AppConfigCSMConfigs__ctor_mBE7D5A9DE2E53008B29690269C8158D8CBF1F897,
	CSMUtilities_SerializetoJsonAndPostOverUDPAsync_m1BC4CBD268D9855B6A6404405B6AD0248929E579,
	CSMUtilities_GetApiNameFromRequest_m03C1B1BFB9280589BDD4B1CF91B1AE39B68C6D2A,
	CSMUtilities_CreateUDPMessage_mF3A8E785B874AE941978B9DF9E0E08AE39EE5F1B,
	CSMUtilities_CreateUDPMessage_mB74C96B974CA490C38F97C3A6862F73BC32ED801,
	CSMUtilities_CreateUDPMessage_m6374AA68C6BBC48F82C5097D7F0A08CBDE08E9A6,
	DeterminedCSMConfiguration_get_CSMConfiguration_mAB96F0DC4AE5F8F1DB7727A094F2BA66BD933382,
	DeterminedCSMConfiguration_set_CSMConfiguration_m5D22C46AFCFD26C97C8F6E662F9D0475C4F9D8EF,
	DeterminedCSMConfiguration__ctor_m3CD1B062031C383204A047CFF07F079FA4254FB2,
	DeterminedCSMConfiguration__cctor_mE94297B48C485F1C51640E716E3A3BB1B14EBD55,
	DeterminedCSMConfiguration_get_Instance_m6487A4331D0A2930011CD6FF2DF23204D9DF00C9,
	MonitoringAPICall__ctor_m378C9985448873908D3C6C0E1B0510739212DFA8,
	MonitoringAPICall__ctor_mA947B2F3508E810AD7928BD39564199E662CDDEA,
	MonitoringAPICall_get_Api_m0421D8D4DA5663899D39BD23D46C40EBBA6F3483,
	MonitoringAPICall_set_Api_mC4C55733606CFBF05B305CB46D6E8B4B005CEE30,
	MonitoringAPICall_get_Service_m3B91F2CD211A542CBB96C9136BA39202067353A0,
	MonitoringAPICall_set_Service_m144B8CFF0ECB8CC57B168F4069A84BD6AA82920D,
	MonitoringAPICall_get_ClientId_m2A044772ADA95AC4480BC9951B69BF92044042B5,
	MonitoringAPICall_set_ClientId_m6C80B675D0CB589DCFCB208BCC6E9F3CA12183D4,
	MonitoringAPICall_get_Timestamp_m1A5EDDC9894C0FB1AEB4F00CCD8870B1A6604936,
	MonitoringAPICall_set_Timestamp_m03E081ACED1B45E8652C1DEFC3AE78220C6B2E22,
	MonitoringAPICall_get_Type_m838B0741BBF668D7B367C1C3BAF1309D7D4071ED,
	MonitoringAPICall_set_Type_m9AEF4F1CBD04509B7926C45ADC98C15936DA4AB3,
	MonitoringAPICall_get_Version_m1350F113A9E10FDB65F58362C721175A6F91B4AE,
	MonitoringAPICall_get_Region_m5CD08A43220F126CB25B2AB57CA2D3D774FD41A5,
	MonitoringAPICall_set_Region_m3D084EFEDF90D8B8449E28A372C780B999ED0E0B,
	MonitoringAPICall_get_UserAgent_mDCF593DAA9DF1950F9788E0DF4D42CEB1E627C02,
	MonitoringAPICall_set_UserAgent_m1F837089B138AD498DF2950E1F390D0D6C0D64E8,
	MonitoringAPICallAttempt__ctor_m4E326C45F9881D516F1D74B1CC72A4B7E66FBB7B,
	MonitoringAPICallAttempt_get_Fqdn_mC1C7B451BFB6364D78DB00E5070E111FAB35D0CF,
	MonitoringAPICallAttempt_set_Fqdn_m01BE278F2D17A95AB94C37F7FC59A4E9FD64313F,
	MonitoringAPICallAttempt_get_SessionToken_mFDA438C3142E23E75C4B0132490CF1F519C1FD80,
	MonitoringAPICallAttempt_set_SessionToken_m945A8D8440A19BCDD6576467B44894A81BFF9EA4,
	MonitoringAPICallAttempt_get_AccessKey_m96D3FB844360E8D74D7FBEAA11076D33EB01C515,
	MonitoringAPICallAttempt_set_AccessKey_m4A6EA9A4C5E28F7CF04F7B02EE0113183AD8F164,
	MonitoringAPICallAttempt_get_HttpStatusCode_mD24FDF30CD12CD3F88848993681D41F93781C502,
	MonitoringAPICallAttempt_set_HttpStatusCode_m987F92618CAD35F5BC8668334BBF5EAC3B0FBBDE,
	MonitoringAPICallAttempt_get_SdkExceptionMessage_m630E9AEDE21F51C10E55EFEC2C6C3230D0EF8871,
	MonitoringAPICallAttempt_set_SdkExceptionMessage_mBE3B617CA3F000E2B607171A82C93150132B395C,
	MonitoringAPICallAttempt_get_SdkException_m094D5FBD1FA02A53608668E5891F650005D7F0D0,
	MonitoringAPICallAttempt_set_SdkException_m96DA837D58F7B9E33292F7DAB80C6D616FE7246E,
	MonitoringAPICallAttempt_get_AWSException_m621BF4D5C58B7ABB9FEFCE8EF74F78C85428FE5E,
	MonitoringAPICallAttempt_set_AWSException_m8A014D4863F83034A00C45402AA37DF090DEFB24,
	MonitoringAPICallAttempt_get_AWSExceptionMessage_m1C9CA1F161FE48A9E07E0B75CA28C5B1B784599F,
	MonitoringAPICallAttempt_set_AWSExceptionMessage_m7112DBAA36E0128BC4BE3D4522EDF9062D57ABCA,
	MonitoringAPICallAttempt_get_XAmznRequestId_m45F79A7B23338AEE1F00F484DFFFAAF6A6EAE3E4,
	MonitoringAPICallAttempt_set_XAmznRequestId_mD2E1584FB7FFE82F4CF4830E1489132D01AE94F3,
	MonitoringAPICallAttempt_get_XAmzRequestId_mD13076615AEADA948BEDB6B9E9BED3C1992AA04F,
	MonitoringAPICallAttempt_set_XAmzRequestId_m641F0ABC8509CBC655349F822A3568D3B11B995E,
	MonitoringAPICallAttempt_get_XAmzId2_m63ECE3EA15B22180CEDB408A79A7F1B4BC76F37B,
	MonitoringAPICallAttempt_set_XAmzId2_m5CA9D84BDD1AEAD6F04509F43D3A0D8FA0E356D6,
	MonitoringAPICallAttempt_get_AttemptLatency_m16BA30F9971385B8E01E41FE78961DFBFF81E6C1,
	MonitoringAPICallAttempt_set_AttemptLatency_m0CAEAAC28246217A1C9964A1779C3B58F5D6D926,
	MonitoringAPICallEvent__ctor_m14D2BFF02B4B89557736106A001E2E97321B1E3F,
	MonitoringAPICallEvent_get_AttemptCount_m9FD13509E959B8D2845F6B343977E8C868076AAD,
	MonitoringAPICallEvent_set_AttemptCount_mBD8F813864ADCEF4408000597EF31D39219B723C,
	MonitoringAPICallEvent_get_Latency_m03DA86A6D65A9293B19F3C3A6BE18EEDD91E8344,
	MonitoringAPICallEvent_set_Latency_mC5CD2AE1112B756AE6BF5E7DC5B8B0516533FED6,
	MonitoringAPICallEvent_get_IsLastExceptionRetryable_m02D2DB01CD71002D7BD5F7E7B6370B338BD5781F,
	MonitoringAPICallEvent_set_IsLastExceptionRetryable_mAA7FD50220A6E6D50F202F17AFF8E6224D3F348B,
	MonitoringAPICallEvent_get_FinalSdkExceptionMessage_mB883D7A30AEFE8D0C95F7ACBC14FF17F3D3E8D58,
	MonitoringAPICallEvent_set_FinalSdkExceptionMessage_mCE4CEBB9D3195B4FCA1C97F06FDE129BF4E038D7,
	MonitoringAPICallEvent_get_FinalSdkException_m4C0783F4BFD0AECB34A62A8BD9482D985B4692F1,
	MonitoringAPICallEvent_set_FinalSdkException_m844468E3AC8FCEB66D06429B93F3CC1DBD348A95,
	MonitoringAPICallEvent_get_FinalAWSException_m915DD2A5206AEB58EF4B4C97BCC865DBBAD5AED4,
	MonitoringAPICallEvent_set_FinalAWSException_mAE895618DF6F8E6795E9BA79F7494C613C037BDF,
	MonitoringAPICallEvent_get_FinalAWSExceptionMessage_m57BF5E0DB609F04A09946FBC697EB8A73866CA08,
	MonitoringAPICallEvent_set_FinalAWSExceptionMessage_m5F69148E6FA2848E9476EDC98F4C7F66F23B26D6,
	MonitoringAPICallEvent_get_FinalHttpStatusCode_m52DF47D94212EBE72D665682FBFBE07B726989D8,
	MonitoringAPICallEvent_set_FinalHttpStatusCode_m1765EFAE3F3608A5C828F78249B1674D7829EF65,
	CachingWrapperStream_get_AllReadBytes_m47F8162EE81AF5D64CC11404AC5F016C410A571A,
	CachingWrapperStream_set_AllReadBytes_mF9E1FE6C9EAE04CB72D94F64A12E80D148792449,
	CachingWrapperStream_get_LoggableReadBytes_m7FFB64D94C17B8D09F174F9E6AFC3EB878D6DE2F,
	CachingWrapperStream__ctor_mAD1FC9B4C218F2A742DC707500C8B458E44BBAD6,
	CachingWrapperStream_Read_m7683473877085AC49C7B1E2E6BE92E6D177B84A6,
	CachingWrapperStream_ReadAsync_mCCF843346B0BF947D6ED985A9D134B554BE74A95,
	CachingWrapperStream_UpdateCacheAfterReading_m5685E68AEF5EB23AD6B0BCBF6A7B160281E239C2,
	CachingWrapperStream_get_CanSeek_m0357E132F878E076172CF55A0D78BEACA035B966,
	CachingWrapperStream_get_Position_m4EB3A0F3E288BFFDF4120611D41632982C7C3604,
	CachingWrapperStream_set_Position_mD595FEBC84AEEDB104F004409D838C63B62DA606,
	CachingWrapperStream_Seek_mF4D8D86DA75EEEA6123233236709A6F920E8A20D,
	CachingWrapperStream_U3CU3En__0_m732E60ABD6DCDDE8A24C7630D1DDB960A789B57D,
	U3CReadAsyncU3Ed__10_MoveNext_m4EF4BA3B7A01EDDE2FAA1C6D06393F3CDAABF912,
	U3CReadAsyncU3Ed__10_SetStateMachine_mFD24E47F2568DF602D75FEB1AD250C09AF1402F3,
	ChunkedUploadWrapperStream__ctor_m1BDA47C949DB38586059DAE2D7C27594755FB234,
	ChunkedUploadWrapperStream_Read_mFA4DA6548E819FE540E89D8EA3D81E6F70677042,
	ChunkedUploadWrapperStream_AdjustBufferAfterReading_mC77899CBD26862A0DEBB85284C575677B83971BD,
	ChunkedUploadWrapperStream_ReadAsync_m6A47C1B5537F49092C6FC4F3BEF907635D170E27,
	ChunkedUploadWrapperStream_FillInputBufferAsync_mE7F390F23DC7FB28A04E09403ADB3089687A2DB8,
	ChunkedUploadWrapperStream_get_HeaderSigningResult_m65E448AFAA7096A20053DAAB9072B2B487A1F1C8,
	ChunkedUploadWrapperStream_set_HeaderSigningResult_m83F964155AFC9D296239E73EF94E1C3B8113B65A,
	ChunkedUploadWrapperStream_get_PreviousChunkSignature_m92C109BB64841B864EDDBC37BB622F1A5CE4AF12,
	ChunkedUploadWrapperStream_set_PreviousChunkSignature_m352F26BF5C023E4CC80654ABE32CBA91012467A0,
	ChunkedUploadWrapperStream_ConstructOutputBufferChunk_m79FF0ED9F1C4F3A3AEB0FA31911DA1591B2CAEE7,
	ChunkedUploadWrapperStream_get_Length_mE7D8FFCD8E3AE17D40A22C2381E32E85B2777C4E,
	ChunkedUploadWrapperStream_get_CanSeek_mC190ECAD7D4DB3DA30348A9661C28CC15AFAF884,
	ChunkedUploadWrapperStream_ComputeChunkedContentLength_m8B4AF0121C8CD785ADBE826E7609209422AD6670,
	ChunkedUploadWrapperStream_CalculateChunkHeaderLength_m76FC0EB716A1D558283CD3C174C8EA9BD890E310,
	ChunkedUploadWrapperStream_FillInputBuffer_m4BE56644BF84AC4F059298C69414EFA837355C4D,
	ChunkedUploadWrapperStream_get_HasLength_m8672AAACA1194C82A706BA5D14CB36AFC1B0993D,
	ChunkedUploadWrapperStream__cctor_mAF3558049BF700DCB3E38083865F0FB1816ECF83,
	U3CU3Ec__cctor_m9A188949252BDA0A532BE23319623E576EB03379,
	U3CU3Ec__ctor_mA0066995D40AD9ABDC941DEC139DBF7AD622737A,
	U3CU3Ec_U3C_ctorU3Eb__14_0_mB73A460CA0E6EAF24674AD10169F55E96ECE283C,
	U3CReadAsyncU3Ed__17_MoveNext_mE30629A8299DF67D7AA06C25BD9F50149D44259E,
	U3CReadAsyncU3Ed__17_SetStateMachine_mAEBAEDD593301A03FA75541227C724FE80BE9FEB,
	U3CFillInputBufferAsyncU3Ed__18_MoveNext_m8DD329E339F709CACD5CE1C40669C0663D0F7DF0,
	U3CFillInputBufferAsyncU3Ed__18_SetStateMachine_mD3F93DDF86B76F1C0F6546C305314C798C2255EC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BackgroundInvoker__ctor_mFF8522C5BA8772D6664618D3AE810FF665B9E85C,
	U3CU3Ec__cctor_mACA4DF93CAF6BBDE9F02BADBE15DB6C751FA651E,
	U3CU3Ec__ctor_m61E6BDB336902817A9D75F7B75226CDD9D1FF736,
	U3CU3Ec_U3C_ctorU3Eb__0_0_m6FFA8B5369BFF78610EA050DDD601BB5E2388F65,
	EventStream_add_OnRead_m0DE98B157EC382ADADEF709A3151FC5EEAF22B64,
	EventStream_remove_OnRead_mEAA2C6160B35C2510258E99BBBD8FFA3E4C2934F,
	EventStream__ctor_mB410CB9A85D534E4ACDA49445802BFFC7A6BD2C8,
	EventStream_Dispose_m8555ACC56D3503B628903F9530BAB4F81F9688B4,
	EventStream_get_CanRead_m11FC188A2A0EE00F58F72D6CC9EF8F7BDCAC6AF0,
	EventStream_get_CanSeek_m8B4F82A12935EC18C98F490A8A74C5BB9CF27C19,
	EventStream_get_CanWrite_m2F933EA124DB953D5A65CB5769E606011D6352E0,
	EventStream_get_Length_m6B3FF4607A107DCEBAF210E0EC15F5497923F8D2,
	EventStream_get_Position_m5776C1F89D1FEE3A26A7F6FA7511F4DC42808173,
	EventStream_set_Position_m684B422FC6DC5789C6CBE008B0E115AEE40C4BEF,
	EventStream_get_ReadTimeout_mF64F0D1A1DEEEC5711F4FE83B802AE7FBBCF5896,
	EventStream_set_ReadTimeout_m9671CF92959B3672B5CA4C5D0578B6C6C41146E6,
	EventStream_get_WriteTimeout_m5ACAEA1B96ED57C6C613A431DBF5B8BF143C62FA,
	EventStream_Flush_m7646F0C57543D11A0EC8152BDC57FDD60193043E,
	EventStream_Read_mC60BA92A9CA5CBDF1689B61610D0FFDDCBC01010,
	EventStream_Seek_mCE12477EBD558597058515CDB4EA0F6BF40B0137,
	EventStream_Write_mE388A7EFC620210C772072AC5C7520C7CDE3DDA7,
	EventStream_WriteByte_m5319C32F067EBAC9DA767CA40CC8AE9949B2BB5B,
	EventStream_ReadAsync_mA6ECBA89A86A4B35D07528B73E5E2C5181996E37,
	EventStream_WriteAsync_mB399FEA07A6AFF74FF1409E5BD0A3454B5E703FB,
	ReadProgress__ctor_m03F90935CA2AD55754107277D6C16952D1B3DA48,
	ReadProgress_Invoke_m8BEC70D8C832F5DD3F56B56CDE4A0078AB977CB6,
	ReadProgress_BeginInvoke_m2932837D197A4B1987F5DF94436C7501F645BCA5,
	ReadProgress_EndInvoke_m434FD006A39EE4EEA06B98A845A53B0DC643A816,
	U3CReadAsyncU3Ed__33_MoveNext_mBC419EA13A9997E3E0E65F36F59433A8936F32B7,
	U3CReadAsyncU3Ed__33_SetStateMachine_m93DC06F8C05B23E12F7E832FF063A3D652DDA55E,
	Extensions_GetElapsedDateTimeTicks_mFC9EB180A1895BD3CFA90ADDC1BA0E8EBB37A3B0,
	Extensions_HasRequestData_m89B15A85A93DE8851680363F7237A1BAF07F6700,
	Extensions__cctor_m517EE48A13FF83498C691420BEDBE77B80E4803C,
	GuidUtils_TryParseNullableGuid_m1DC08B14DB69D1FDA63C794B038A3DB531046C5C,
	GuidUtils_TryParseGuid_mF4789C834B8D756A58E2F3EDE6CBFD0D7A225117,
	Hashing_Hash_m70FC42FA944667BD6E608BD6040E3695B3B0F1E0,
	Hashing_CombineHashes_m4098B2EDCAA2187D45472404BB15B32364853310,
	Hashing_CombineHashesInternal_m08307CB2102F2B02F59A64DFCEC9F6DA3C3C0448,
	U3CU3Ec__cctor_m70D8CEBCDA251FD223D213CEEBF2D7BE1F281696,
	U3CU3Ec__ctor_mD9C252C8649C45D02E4DB986B10EBC974FC4AE48,
	U3CU3Ec_U3CHashU3Eb__0_0_m7182EC3EFB00276A44E1D067B636972423269A53,
	HashStream_get_Algorithm_m77777CC969D213FAB4ABCBBA87FF03AB65FB9647,
	HashStream_set_CurrentPosition_mAC27EEE93E512BAB4450D2ED614850362DAF3F9A,
	HashStream_set_CalculatedHash_m78F1745C53826D49A4888F317B65495C587FBD72,
	HashStream_Reset_m0A7A55F7AD350FC5A3E6DDEA7CE456B5F9183AE8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InternalConsoleLogger__ctor_m82F71F6995A5D7518CEEF49C3F416F34B7566528,
	InternalConsoleLogger_Error_m8923FC249477CC668869717EBB99806926E41235,
	InternalConsoleLogger_Debug_m8E6ABC0E7BF0C1A2A801E722EE546EDEBF694431,
	InternalConsoleLogger_DebugFormat_mF95C2856A41926ADB444CCC2C51E5B093D253C29,
	InternalConsoleLogger_InfoFormat_mFECE6C230B64353D25358155EEB0088B48A3F867,
	InternalConsoleLogger_Log_mC56668F7CA67A8BE35FF8D02BF9D673CB996BEF0,
	Logger__ctor_m226C438F69FA7897F419F12BF02433E1F30A7CC9,
	Logger__ctor_m7C70EF7BF01939E4B459459527940F414CD92B21,
	Logger_ConfigsChanged_mB9CD24DFA4900D25B4EF825EA157F2D7EADD7CDD,
	Logger_ConfigureLoggers_m83A77BF4E5B3E832802B745B28F60A92571C9830,
	Logger_GetLogger_m18C612C6586A3F343F5CC865041C82A257CDFF1A,
	Logger_get_EmptyLogger_m5CC262E9F11F49DA598A22FC8A05CEF26C3ED9C5,
	Logger_Error_mD839C4FC38C8C01EF7836C7632549115BEA394BA,
	Logger_Debug_mD756AEAD6298C36D3C76F94FC478D15E3FE965B1,
	Logger_DebugFormat_m525735E1788943E3DE772B42C9A64BAF1C32307B,
	Logger_InfoFormat_m834B8C45A15D6C18D10E8DA641C376B1A96B7AE0,
	Logger__cctor_mDF4AC77A926156E5E8F1C4BDECCF54B5B4CA722F,
	InternalLogger_get_DeclaringType_mF8C58DF13E2E9F52A6C1C28533698C7FC23549A3,
	InternalLogger_set_DeclaringType_mB5E079C1381CB75406DC0A2B0C3F679FE6892E50,
	InternalLogger_get_IsEnabled_mC05569861EF1FD59E173C1CC3D538015554CE830,
	InternalLogger_set_IsEnabled_m60444B552DE4B9925465942D0E3155D5839B5316,
	InternalLogger__ctor_m9334AF9E260F1A402BFA66EA348462830BF63655,
	InternalLogger_get_IsErrorEnabled_m507CFD61D6D6477E52DE59EAC7D86392EFFE1FDA,
	InternalLogger_get_IsDebugEnabled_m3967843696A1A6750202CB9B43A4245E605477AD,
	InternalLogger_get_IsInfoEnabled_m3534FB989D4DA4C6AB76BDDBF6D5924F45184FD7,
	NULL,
	NULL,
	NULL,
	NULL,
	InternalLog4netLogger_loadStatics_m1DB77FE774C935E2A2C298E39C30EF7E866B3822,
	InternalLog4netLogger__ctor_mF9BBAAEC7E937622CE2761FD5714B1D7CA768E61,
	InternalLog4netLogger_get_IsErrorEnabled_m399891FE9A026ADF91B015BC5D7C4AF122AB2D68,
	InternalLog4netLogger_Error_m8C3018BBD99EA7D20D9D71034E5424DB9C0DAA57,
	InternalLog4netLogger_get_IsDebugEnabled_m1E304BB61279F29D4AAEEDF9C3DA33C8BE2C1314,
	InternalLog4netLogger_Debug_mFDBE196995454C86FCC9F1D7F760A2B6D9DB56F3,
	InternalLog4netLogger_DebugFormat_m7D57E2EC78D5CF1F90534253B2CA280E18BD92EF,
	InternalLog4netLogger_get_IsInfoEnabled_mA018A5BF4E6B43006F06EBB7BEE246B861292235,
	InternalLog4netLogger_InfoFormat_m4C7FB0FBD2A2E2E5553A5F3242BFCE901ED1E5FC,
	InternalLog4netLogger__cctor_mC290C95CFD5F684D5FCEE19DFF399B74C2566B02,
	LogMessage_get_Args_m64559F784FBCEF8CA2E60E453E9E2D284D4924CE,
	LogMessage_set_Args_m57166E78E7484C714F1CF706EE6ADED27C3430A2,
	LogMessage_get_Provider_m3A902C604D423FBCA0E6BAAEF89002A0D443B8BC,
	LogMessage_set_Provider_m44E3AF9A95855B86FEC00453D9B1BEEF4AEB12B8,
	LogMessage_get_Format_m60859CBB4FAE49145535300C94EE5B55598D2DF4,
	LogMessage_set_Format_m72DEAFAF7695EEB50DA0A52565923C01F4068ABD,
	LogMessage__ctor_mC9E82515541AEA85C7ACFE35081A77EC6E94657F,
	LogMessage_ToString_m1312E43025D02AB8AED0262CABBF6EE5D1936A45,
	NULL,
	NULL,
	NULL,
	RequestMetrics_get_CurrentTime_mED969D865585AEC8F03C5F700593D6B0C2DF4243,
	RequestMetrics_LogError_Locked_m233C41DA154865B536D979BC9FDC3C748576CF84,
	RequestMetrics_Log_mABEC2813301E3904E01BA6FC7D68798BC6B0E62C,
	RequestMetrics_Log_mEB8D48F09DE24292EF7A74F636EA985091FE0623,
	RequestMetrics_LogHelper_m06E3E60039285253B932C73671888A65B1C89D4C,
	RequestMetrics_ObjectToString_mFF490B51A276548C1AFF965D6A55F614FE66A639,
	RequestMetrics_get_Properties_m84BC22CAA8541BABE6FE73999D148C1ED76DBBF0,
	RequestMetrics_set_Properties_m3C55799F13A923439566FF92885EB32CE2EC716F,
	RequestMetrics_get_Timings_m16064050B633CBE0E591C154A132ABF4B0A28E26,
	RequestMetrics_set_Timings_mEF536D7BA4D9F52DA8869B9937C519F356B9DF67,
	RequestMetrics_get_Counters_m8A0BDC41BE680C0E804B86D01693558CFE0E5894,
	RequestMetrics_set_Counters_m04B53A30F43131DDBD3978F730C6ECD02257749D,
	RequestMetrics_get_IsEnabled_m1C66114D0BC5B5FADE7FA6A59F0DE19776471245,
	RequestMetrics_set_IsEnabled_m8F6AECA196BF20FB649C8AB8EA0A0BF3E7186E1E,
	RequestMetrics__ctor_m4DEA6AEFE678C87D18813DDF4E125663E8C26E6F,
	RequestMetrics_StartEvent_m271F4D4E110290B50567218EA4D1AB9500786831,
	RequestMetrics_StopEvent_m65411582B8651E4E69AB199C5AF1D27952F468BF,
	RequestMetrics_AddProperty_m54036FB59C030059BD4A888ABC19C8699B9944E9,
	RequestMetrics_SetCounter_mA6AB2F144BA0CDD66B940EA8EECB787EC9676392,
	RequestMetrics_GetErrors_m183C7743E86E8238695C0F749F9A50EFDF1A02ED,
	RequestMetrics_ToString_m5EB67C37EA7A40C4C3DAA372D25617E7E5C2F6F6,
	RequestMetrics_ToJSON_m4B74D16EEAE412F5675C245ABF52FC3257B8EE3D,
	U3CU3Ec__cctor_m45DBEC4240710FBF9DDD897040EB3A133982B7FA,
	U3CU3Ec__ctor_m64582B1B1D489F27A10EC6F7E68D4B133376A0BE,
	U3CU3Ec_U3CGetErrorsU3Eb__33_0_mDA926F44664C61BD942C1B6E69E63DF9A9A6658F,
	Timing__ctor_mDDB669A2B54B25B230401E85029CB7001E55DED6,
	Timing__ctor_mA2C4F0FBEDB3880C141663843F1869F870B1C37E,
	Timing_Stop_mD83878F0DD74A58043E146232FE859ADAD135517,
	Timing_get_IsFinished_mEB880282891D5893958B9981CDFF58ECF2BB304E,
	Timing_set_IsFinished_mDB14D3D301BF0DAAFC799C58EE20E6838623A380,
	Timing_get_ElapsedTicks_mEA31A7D897E6641EE65E4B691686803BFAEBC0C2,
	Timing_get_ElapsedTime_m26E1E7D21D9F519C1F2937DFCAEE11F24F1F6B19,
	TimingEvent__ctor_m5B0807C9E7E967910255FE1AAB69EC0160A6EDA3,
	TimingEvent_Dispose_m6E6298FDA146F8F908330A192613522208093CF8,
	TimingEvent_Dispose_mE1ED663542AB69392A35BE754137AC2BE80458E5,
	TimingEvent_Finalize_mDC7FEF9C68BACAD9C3F1057A21E1AFBCFA1BEE6F,
	MetricError_get_Metric_m0EFDCF995545075980F8C80E5597CEB829AADF6C,
	MetricError_set_Metric_m520EB610297922EE7EAB30E41E3C0F6E6EA091FA,
	MetricError_get_Message_m0CB9DC2D53A93DA95B07DD455CDC0FB60CD2C05F,
	MetricError_set_Message_m0541CC94D5A9606C37C1485C32A0C39133A5FE3E,
	MetricError_get_Exception_m6FC385BBE21A845875D34A44D1DC6B265419B139,
	MetricError_set_Exception_mB492556F5CDD058FF0A84D4FEEF2EDD032A6FF55,
	MetricError_get_Time_m287005246E7A6BED191E05ADB098B6BE5856F39C,
	MetricError_set_Time_m0FD2F80D3A72BC1D80EBA419A23833BF268747C4,
	MetricError__ctor_mEF8FCBFD3DD508E52D6BE9798AC603FDBEF7F58F,
	MetricError__ctor_m55C1C6FEF81AF627136BB5489BC1BA441246FE36,
	NonDisposingWrapperStream__ctor_mC2D061B812BAB078AD604D63D575E1A764584744,
	NonDisposingWrapperStream_Dispose_m58313ED55C0895044623DCBB212B952F015A4336,
	S3Uri_IsS3Uri_m1B7427D0B8B49DB624CC96478D6CEDC4C69972BF,
	S3Uri__cctor_m1A48D34B9D68760EF17A34A4F95B19026FD34550,
	StringUtils_FromString_m3EEFED8018E3DD19A48794362230C078FDE9CE35,
	StringUtils_FromMemoryStream_m8390D1D01AD7AC326C5B09E6E0D3116A28930BED,
	StringUtils_FromInt_m140C1228F73D061A604B3BC22F26E0E00A1B6A29,
	StringUtils__cctor_mB156AA15CF8BBF3FC07E3CF4A067FF5AD91A0FC1,
	WrapperStream_get_BaseStream_mFEB26EFB3BC758284D7CF3F725BD9E865E3D086A,
	WrapperStream_set_BaseStream_mD8B7FFBEA1AFC67B0B69D0AD97F28771F6767DFE,
	WrapperStream__ctor_m5AE4B4945227374B6961AC423E7541D9C3A0AD42,
	WrapperStream_GetNonWrapperBaseStream_mABA5E7B57D9FE5F69CCA894501506ECFFA9B04D8,
	WrapperStream_GetSeekableBaseStream_m121E5CED8444CBDD0CA27B3C0BE2DE38A7C59A71,
	WrapperStream_GetNonWrapperBaseStream_m21B77CDBE5EFF4E60BD37604577432F6707AC517,
	WrapperStream_SearchWrappedStream_mCC8BFAD19D8887D77FA547A3D27806405F31F97B,
	WrapperStream_SearchWrappedStream_m96D13AE5A10E6ED516C40CE1D20BA44BF1C81427,
	WrapperStream_get_CanRead_m4BA5A19D73B3F1373B817671BF3CCCCF7D800457,
	WrapperStream_get_CanSeek_m8404E6AE766A2C332BC11E9BE2C33F7C470DA25E,
	WrapperStream_get_CanWrite_m060705526E11DF9C4C9DD9C756DD139698FF9E46,
	WrapperStream_Dispose_mBDD2064ECF5945930F75CD0DCD3FF81ED704EB93,
	WrapperStream_get_Length_mEEA0CE603ED4708F7388999013DCE7F9E3FB3BC0,
	WrapperStream_get_Position_mC2B75A713474CDA16D2423263B1A460FEC7D7DC2,
	WrapperStream_set_Position_mC2B673FD3657BD19C39169533D3C71B390296F3C,
	WrapperStream_get_ReadTimeout_mE467214DD610D4F1324C7E11F60530DF251515F3,
	WrapperStream_set_ReadTimeout_mD92FA6636A63C7A04424399FF54552C3D75FB61D,
	WrapperStream_get_WriteTimeout_m5DF0633A0F0040A9B8B339F4731FF8E02343EC2E,
	WrapperStream_Flush_mCC0BD37433C8ECF9D34D4EF30D71B67762DFF291,
	WrapperStream_Read_mE1D853429ED4D36C770345B447186FF75F63C34B,
	WrapperStream_Seek_m7A7639B6DF8F8054454EBC6F1BC46A7FDF1E6573,
	WrapperStream_Write_mC2C2CAD74002505C6EED49613E5B9EA411B9C0A6,
	WrapperStream_ReadAsync_m475537AB27E220B8402E1DC2EDE77ED909C5D770,
	WrapperStream_WriteAsync_m58FC94640CFDB97CBF47133B7DBDA83C548B03B5,
	WrapperStream_get_HasLength_m2E69B14EFD028357E090139D0E88B6ABD0700FE3,
	IniFile__ctor_m4AF5DC976681C2EE54EC182057AD26F9DDE4C42E,
	IniFile_get_FilePath_m4038C59B30A6304F21A8C1B6FB0B73760144E30D,
	IniFile_get_Lines_m75B529190BCB6672BB957E2E76588F6F0A886333,
	IniFile_TryGetSection_mD24708E03892B3238BC90C5FA692E86A833DFDF4,
	IniFile_TryGetSection_mA28DECB79F6BCC697192AE71709A72A88FAA3720,
	IniFile_TryGetSection_m803CA08A6E71E7EE10AF86E32AEE7BBABA337E30,
	IniFile_ToString_m93380C8F0FEF816398A63D4515DBE7863A8F4992,
	IniFile_IsDuplicateProperty_mE6CC8DBA9637BBFC36E8BEF2F77E3B1D956645AE,
	IniFile_Validate_m92B0DAA6CAF532017A3693B08B11021F835F75D9,
	IniFile_TrySeekSection_mF214B91AE9C98ACBE8B7A712B1A41DD0E6EB3BEB,
	IniFile_TrySeekSection_mE0986EC719D4349E369F6CC7972A1D45AEA8871E,
	IniFile_SeekSection_m6BF76DBC2F525496EA150167FBA2DBEB8DF8EC1D,
	IniFile_SeekProperty_m843DDD5C093D9A833FD3BAB9F35ECFD7C7ED3CE4,
	IniFile_GetErrorMessage_mDAE8BE1DB235DD89E3D0CC9CB9AB71211295ECED,
	IniFile_IsCommentOrBlank_m50DA2EF2AD2FDF4153AE73AC4989A2BF71BA87FF,
	IniFile_IsSection_m4B68220FE6192BAB6C75FB703C8460ECA1B287F5,
	IniFile_TryParseSection_mBE7518C7101C96397498B3AA33CE7BE92961DA5B,
	IniFile_IsProperty_m19BA77923961BDAEF1246DA439D003E115460063,
	IniFile_TryParseProperty_m21F0281B31BE33199CA1F7C0EF35608177CB6C30,
	IniFile_GetLineMessage_m7DABC42217FC51118B815C31089E365A49DDE451,
	InternalSystemDiagnosticsLogger__ctor_m0AD0B53E18F5FDD5D3EA59F3C24D623DD5A193F5,
	InternalSystemDiagnosticsLogger_Error_m43F37752F40DDDA5E12BB8215DA6AA108827B464,
	InternalSystemDiagnosticsLogger_Debug_mA2001BC068D54399EA529AD2F7B6394C3F7806C2,
	InternalSystemDiagnosticsLogger_DebugFormat_m6F0D41299168CCA93DC6CD0388F93395618D7DF9,
	InternalSystemDiagnosticsLogger_InfoFormat_m9023523528D24439C250B64566BE630C5CD7D407,
	InternalSystemDiagnosticsLogger_get_IsDebugEnabled_mA7389402E736AFCEEB438699CA8B51F703442A27,
	InternalSystemDiagnosticsLogger_get_IsErrorEnabled_mE71EF1CA9833C8B263D977CF30E4A4A250399C36,
	InternalSystemDiagnosticsLogger_get_IsInfoEnabled_m48A0089397E2EA90A82DC0F7C0536842DA4A81B3,
	TraceSourceUtil_GetTraceSource_mF42F323087F386FF58B055429B46C47BC99A9E81,
	TraceSourceUtil_GetTraceSource_m365FBA77060CBA44B48F3E5C810594445C1A7188,
	TraceSourceUtil_GetTraceSourceWithListeners_mACAD6DF0BBD75747624EFD5083FF00AAD04240AC,
	OptimisticLockedTextFile_get_OriginalContents_m7ED6E4DB738F208C66A18E8BE114AFFE62296852,
	OptimisticLockedTextFile_set_OriginalContents_mBDD535792A93B7CA5602DAEADE392190C5D51AF3,
	OptimisticLockedTextFile_get_FilePath_m8B21A1CC348771C89308D256C7A25B7AE7884491,
	OptimisticLockedTextFile_set_FilePath_mFD1F1F01F35075BE40E6CC242C9E85DA541E4E34,
	OptimisticLockedTextFile_get_Lines_m6DF3631DE46A8EB2B3C5B0F96F89DB2D711BDBA9,
	OptimisticLockedTextFile_set_Lines_mCD018C2303B399C5A81BC7A2D90DAA0233CD0254,
	OptimisticLockedTextFile__ctor_m7E505437A9A232CCE77C44886E70AE1BBDEC14EB,
	OptimisticLockedTextFile_ToString_m1E3AC2B920FDFDE57FF0A7E09B1EAFE2725993F4,
	OptimisticLockedTextFile_Read_mE16D7D604A86DDF4DC79120521453445C3335181,
	OptimisticLockedTextFile_HasEnding_m9D234A7CE52DB8E707595A2CA27A0872656E9977,
	OptimisticLockedTextFile_ReadLinesWithEndings_mAF25338F1A89284E14268C8BE3EC8E3F5ECF3D33,
	ProfileIniFile_get_ProfileMarkerRequired_mBF818D67B6A16168B253C60CD65BD3CB4A6E29E4,
	ProfileIniFile_set_ProfileMarkerRequired_m78330C35440F6AAAA16C6B6115F7611E640FDCC9,
	ProfileIniFile__ctor_mEE9EFEF3E6393590F2CA0281FD1D30DB20FE7ECE,
	ProfileIniFile_TryGetSection_mF3EF38C926678C32CB05A3E3922DDB87598C4412,
	NULL,
	ExclusiveSynchronizationContext_get_ObjectException_m7D008D6F4B95533A1B9994CF182147BCA532C3A5,
	ExclusiveSynchronizationContext_set_ObjectException_m6C1CB681D339DED2069010AA0513E31E155D20DE,
	ExclusiveSynchronizationContext_Send_mA557C607D37719A5AE0B66D1BF41D65A5E6A9B53,
	ExclusiveSynchronizationContext_Post_m4D7D07238647EDFFFF8AF1E1FE905C00CE9E5E18,
	ExclusiveSynchronizationContext_EndMessageLoop_m8C1F5C9019C02B635B7B5F1B00C427773722D8D0,
	ExclusiveSynchronizationContext_BeginMessageLoop_m6B34068C382E5605750CE5F34B5438A24C0A19F0,
	ExclusiveSynchronizationContext_CreateCopy_m61AFBD0AA23F755BED7D900EAAE45AA5583F6735,
	ExclusiveSynchronizationContext__ctor_m74EE592107B49B31BAE200E05C0505D6C1224B89,
	ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m7BEA15F25CAD1B33751ADD0797A19CE192B808A6,
	NULL,
	NULL,
	NULL,
	NULL,
	ErrorResponseUnmarshaller_Unmarshall_m6A267750A35488F8A8F190AFA22EB15453900188,
	ErrorResponseUnmarshaller_PopulateErrorResponseFromXmlIfPossible_m64358A89E2969B7473F174843197DB3D2AFB9D4C,
	ErrorResponseUnmarshaller_TryReadContext_m25AD4E6349CC178AFD83FE4DC4008CCC64AF9DA4,
	ErrorResponseUnmarshaller_GetInstance_m3D989CD75ED16E77AC1E51F105B6AD3B2CAC2B3F,
	ErrorResponseUnmarshaller__ctor_m1C9FC3E1045E26E48B6295F57A4CC3B0E6D20BCC,
	NULL,
	MarshallerContext_set_Request_m8F802D68F09FC7F70F1BA36E3B4FEA893706D834,
	MarshallerContext__ctor_mEF233921C78C3C3F2A417E0AD8BCE26B777338A3,
	JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B,
	JsonMarshallerContext_set_Writer_m324F3C5EBDFBD98ED3E6B96565B40DCE1B066955,
	JsonMarshallerContext__ctor_m72EC1A523D8129B418D93DAA8CB9D73FF5DC8E76,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonErrorResponseUnmarshaller_Unmarshall_m057826DF6640BD156507A76F4BDA8CEB1C43EE9B,
	JsonErrorResponseUnmarshaller_GetValuesFromJsonIfPossible_mC7A98D5BEA9FE4539CB22926E5C6ADA335856E03,
	JsonErrorResponseUnmarshaller_TryReadContext_mCF2332A25F261074C57105A8D85AC824685424AA,
	JsonErrorResponseUnmarshaller_GetInstance_mF38659F6157C8EDF253734672FFD34FCD188AAA3,
	JsonErrorResponseUnmarshaller__ctor_m358A02D75D1D2357A7ED2E548FE2DC10E850C3D0,
	JsonUnmarshallerContext__ctor_mF3B6DEDDF1A70D283EFD799EDAAE8A952AC87D7D,
	JsonUnmarshallerContext_get_IsStartOfDocument_mEDB4C32061E285C35D6723BA16220A3C16677730,
	JsonUnmarshallerContext_get_IsEndElement_mD1C62D2E1B529CE910B6F891BE3E7A4F58E49556,
	JsonUnmarshallerContext_get_IsStartElement_m9A7721EC6688782AEC29B052B1581917EB4A0A8F,
	JsonUnmarshallerContext_get_CurrentDepth_mB2770FC3F3BE219BB79B508A49CCEA9AC65FCE84,
	JsonUnmarshallerContext_get_CurrentPath_m474A141F0CA1400939219B0C1C774C04E3CDAB4C,
	JsonUnmarshallerContext_Read_m4C00443ED8484C982F4D00E5BB31674C2AB9E6F1,
	JsonUnmarshallerContext_Peek_m0E18DC4029DEBF901D313C4798197DC307636127,
	JsonUnmarshallerContext_ReadText_mEE7D6100B8DC120E615F4DEC67CB3617D01A3C65,
	JsonUnmarshallerContext_get_CurrentTokenType_mA60F9E7148B1EF1BCD664D3AB068A81910999E19,
	JsonUnmarshallerContext_Peek_mBE31DFF89AA0D1B1E0F71C17D049D5193C9F8197,
	JsonUnmarshallerContext_StreamPeek_m415E13D43AFECBCA04B8CEC10EAAF0BD381070E7,
	JsonUnmarshallerContext_UpdateContext_m02339EFE5531AEC9DA39397C2A837E8A8CF3D5CA,
	JsonUnmarshallerContext_Dispose_m6E749BBC11043E6BA1C6EBE5F1A4678745E02170,
	PathSegment_get_SegmentType_m65B82F8564E2206BCE241B78F79E678AA696D575,
	PathSegment_set_SegmentType_m40F0CBA4B9A4C76E4256AD3195C964DD88906D2A,
	PathSegment_get_Value_m35D398000A8EA78DE5FAF47E7BC0BF1C44EDBDD2,
	PathSegment_set_Value_m909F2ED45DF5DCABEA9D81BC2761C3D8267F5101,
	JsonPathStack_get_CurrentDepth_mBDC7BB722BC988DF7E63D066A5395AADBA1517CA,
	JsonPathStack_get_CurrentPath_m9CC17AB933CEE8BCF6C9A71018A83792DCBE68D0,
	JsonPathStack_Push_mD8705841894D15A02C67353B2126D215646F15B6,
	JsonPathStack_Pop_m2532BF29F37DED88A341CDDE5037C0B05655CA8B,
	JsonPathStack_Peek_mA13FF5D7EFA8AB0D80C0FA4ED2AEF2D182B2ACB7,
	JsonPathStack_get_Count_m15E84330105BA5A6A0CA7C786FF31D7F81A0E374,
	JsonPathStack__ctor_mDF39374E807EA8054C68231642C94F7DA66514C6,
	ResponseUnmarshaller_CreateContext_mA2DE575FA22FEF2C23FED0F4C2241D2FB53081D6,
	ResponseUnmarshaller_get_HasStreamingProperty_m2BCF48C61431F1CE4EAFB156A3A3109EB9EE2F21,
	ResponseUnmarshaller_UnmarshallException_mDA4231C57076783038BA09C1D9A8E92107CB9673,
	ResponseUnmarshaller_UnmarshallResponse_m5461512CEB99C5E41B14A958382B28653D8EC32F,
	NULL,
	NULL,
	ResponseUnmarshaller_ShouldReadEntireResponse_m535B3768F38132F77853A04AFB87CF00AC7E088F,
	ResponseUnmarshaller__ctor_mCB109484E65959D2535B81596886962662A944E9,
	XmlResponseUnmarshaller_Unmarshall_m537436332683B6101DF5F6D2D07FF28E234726FA,
	XmlResponseUnmarshaller_UnmarshallException_m61E020B3B119BD1D5AC124ED11FC70C841063BC0,
	NULL,
	NULL,
	XmlResponseUnmarshaller_ConstructUnmarshallerContext_mBB63C0330008B79B8AE24A2BB14DFE007AC35515,
	XmlResponseUnmarshaller__ctor_m975DBC6E2BA1462E4CCD19A9329C837BE0F45B38,
	JsonResponseUnmarshaller_Unmarshall_m1DB481EA4F4497B6174B143260329ABAD3A9EF36,
	JsonResponseUnmarshaller_UnmarshallException_m70AD13D81D31B67CF5D9994964E078069A3B256C,
	NULL,
	NULL,
	JsonResponseUnmarshaller_ConstructUnmarshallerContext_m97DA0A34FB557F3EF055FD3D7667CB12329583B6,
	JsonResponseUnmarshaller_ShouldReadEntireResponse_m2233FE2C5943B8214213DCA5859029A3E76200CE,
	JsonResponseUnmarshaller__ctor_m7A471A9D41EBFE83FEA37BAAD40BCF2D485F9ADE,
	NULL,
	NULL,
	IntUnmarshaller__ctor_m2A97B34D2BD8CDDC53DD69EDC693779C15B23C27,
	IntUnmarshaller_get_Instance_m746BA04951EF77262464E3E09FDC12BFDAA4E321,
	IntUnmarshaller_Unmarshall_m5F85391FEA87FBA3EC21163FB10692428215B0CF,
	IntUnmarshaller_Unmarshall_m6DC0F5DE60C245CE4314BB8D8A7671441EDC46E7,
	IntUnmarshaller__cctor_m1B92338A3397BA99FCF703A99589BB8819ACA178,
	BoolUnmarshaller__ctor_mC0F123A1C614CE53BBCCE05504F759D96FB1CCD7,
	BoolUnmarshaller_get_Instance_mF9B455017CCF1BCF02671BEA6968CB71D3C95F29,
	BoolUnmarshaller_Unmarshall_m40EEAE1622C39195AAE166F0609AE96360A5E88C,
	BoolUnmarshaller_Unmarshall_m320688D5889AEDF067214028DD4EEC8755CF6A35,
	BoolUnmarshaller__cctor_m70231FE702B88CBCEE2334A4BB7CA3CA71EC187A,
	StringUnmarshaller__ctor_m4FF9B4A68C8061C9B9F26B4DF665A1A11457E25E,
	StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436,
	StringUnmarshaller_GetInstance_mCEB0A724B45D56FD3A6F22823237C506B296406F,
	StringUnmarshaller_Unmarshall_m37C8340144CE14394EA2171204879EB540ABAF3C,
	StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415,
	StringUnmarshaller__cctor_mCD18E97CCFB622332CBFED0E124DA772315DC10D,
	DateTimeUnmarshaller__ctor_m3621628E6EDB2450DB7C9A5411B2E3E8EA6FEF2A,
	DateTimeUnmarshaller_get_Instance_m6576E37458B18A1C1BA0E122716548A4F8410D01,
	DateTimeUnmarshaller_Unmarshall_mD43B95D52A49BCF94AD1422F4552ACCED54E0849,
	DateTimeUnmarshaller_Unmarshall_m9542A67765A9E84CB5DF4A7AEA56060F683D7BE1,
	DateTimeUnmarshaller_UnmarshallInternal_m3382B1041067FA8807203A2F2ADD47B16AD39189,
	DateTimeUnmarshaller__cctor_m1593E2C0738834E455503B4DF532789DD8C56BCA,
	ResponseMetadataUnmarshaller__ctor_mDB588D2F3B766EC43F2E5DC782FCC2F180DA3D36,
	ResponseMetadataUnmarshaller_get_Instance_mFC0B6EFC6832968A56181C722E74CB66E4F9CD02,
	ResponseMetadataUnmarshaller_Unmarshall_mC6E97054299B8F693E7DBBEAC59D2E984BE76C0A,
	ResponseMetadataUnmarshaller_Unmarshall_m375F4E691DCDDD0EC7F4F9C04317217EECBB9FB9,
	ResponseMetadataUnmarshaller__cctor_mF3BE41A5B7CDB7AEB4E9EC93FA10285A1F48E2C3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnmarshallerContext_get_MaintainResponseBody_mEE9520F845AEF875317FB56E2DD0429A98422D93,
	UnmarshallerContext_set_MaintainResponseBody_mDD39548A3482E4C6C005544A32A53468DD962322,
	UnmarshallerContext_get_IsException_m88F8C42DABA97B6253A32CC60EEDC2D756C337F6,
	UnmarshallerContext_set_IsException_m2D4133C986F235F215588C6C87D8515E4B00F585,
	UnmarshallerContext_get_CrcStream_m01B55B4F84F8411D97E5299E88DCA96BD9761680,
	UnmarshallerContext_set_CrcStream_mDCD547E103C6E2B8F63A483422E48D66A60BAC4E,
	UnmarshallerContext_get_Crc32Result_mFCCC6104016C906AE1716B3660E6166F2DEF085F,
	UnmarshallerContext_set_Crc32Result_m48140B1D642B7A6F16B071E4A0D5D5E8683C25B9,
	UnmarshallerContext_get_WebResponseData_mF6135EFB850AEAAD2B212B7038C8C25AA38AB8F8,
	UnmarshallerContext_set_WebResponseData_m7F8BDA75500276956944AF7565D842B44E62C9C5,
	UnmarshallerContext_get_WrappingStream_mDD39993BF5ECDC711F69C9220A464260571C26DD,
	UnmarshallerContext_set_WrappingStream_mC885B682045B8C30D8083C7E76A987472AA8AA93,
	UnmarshallerContext_get_ResponseBody_m4C477F3257EBA5FF72423F956F38E204D0435BD0,
	UnmarshallerContext_GetResponseBodyBytes_m7AA3163269840F761D8DEC6A78CEA74C46C95DAE,
	UnmarshallerContext_get_ResponseData_m98279EBB176A30ADF40918880EB06C117D3EE41B,
	UnmarshallerContext_ValidateCRC32IfAvailable_m053958C14D33CCACC19D2D652F69EE1E66A4337F,
	UnmarshallerContext_SetupCRCStream_m057BD25778592B090A9D103203DD25F94495C1DB,
	UnmarshallerContext_TestExpression_m061116AF9393219F7251845A50ADEA8D0F5E6C38,
	UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30,
	UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27,
	UnmarshallerContext_TestExpression_m292897227E88BA17EFD6A33E58C0CB1FB0499A08,
	UnmarshallerContext_TestExpression_m6CE0AA0A07F98BAF687E2EBD79351AAF973F8316,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnmarshallerContext_Dispose_m53267BEA8660CE432934A3A719023CD9A5FF971B,
	UnmarshallerContext_Dispose_m58E88C15DE1FE4123B254D9ABEA02B976BEE18C3,
	UnmarshallerContext__ctor_mF9E9D9E5E7F378B78A756BC0A906774C28E2F751,
	XmlUnmarshallerContext_get_XmlReader_m03B2DDDD8F9104D10ECA5FA556AAE569354872A1,
	XmlUnmarshallerContext__ctor_mE871192CE85B0133EA0AD989939D662272DAB5C2,
	XmlUnmarshallerContext_get_CurrentPath_m86D70C3B648D6B1FDCE19DE622B9975BB9E1F5C5,
	XmlUnmarshallerContext_get_CurrentDepth_mEBFD7E63555CA56328FAFDD3E27F494C363C15D2,
	XmlUnmarshallerContext_Read_mF1AD588F2B854E9FAE0D71277E3035B2FDF07A4B,
	XmlUnmarshallerContext_ReadText_m90B0F0AE6F27DA057AE68C8E716D1A52ACBAC009,
	XmlUnmarshallerContext_get_IsStartElement_m393F61188E505F942E98C699DE20CA8BF45B8A8F,
	XmlUnmarshallerContext_get_IsEndElement_m41D2018E60DFE15EB75CBB97D8A2D255A1F7EB4F,
	XmlUnmarshallerContext_get_IsStartOfDocument_mE1343FEB33D2C49D7E2BEAAAC126C7511CAFFBB2,
	XmlUnmarshallerContext_get_IsAttribute_mCD86E4DAE4DFAA8677AD77249C088FF0B8EF6BCA,
	XmlUnmarshallerContext_StackToPath_m825552831BF4E3DA423E7AC3981C284DF481B1F2,
	XmlUnmarshallerContext_ReadElement_m778FFE2A1CA1F480EB274622D260C9C522FA15B3,
	XmlUnmarshallerContext_Dispose_m490F9FEE366AA657E4921757E9C9766478D91EB1,
	XmlUnmarshallerContext__cctor_mE1E47777AADC949921C51C9EA9937EDC1D13BAD7,
	HttpClientResponseData__ctor_m70D1079A5BAF5C501C4A2C9015831AB4603259C0,
	HttpClientResponseData_get_StatusCode_mDA92BB2BEF3BA1BBDF1DB3D14295D34B898CA7B6,
	HttpClientResponseData_set_StatusCode_m038084D472C1D5D77EB3444D44C591043EE75522,
	HttpClientResponseData_set_IsSuccessStatusCode_m8E9360B0084DA85637A85A11F1860D191E966E90,
	HttpClientResponseData_get_ContentType_m5CF5885F8847B1D6DAF0D87D1F98631F94D74F6E,
	HttpClientResponseData_set_ContentType_mF447D5D6B510AE8735E69A23753ED627F10F3A43,
	HttpClientResponseData_get_ContentLength_mA95E15F1F79F452C9B09FCC33C8A1AA54D9393F2,
	HttpClientResponseData_set_ContentLength_m769C7803F8FCABAA68F2F8820AE64E7EEC74922F,
	HttpClientResponseData_GetHeaderValue_mB379D5FE88959D0DC741AA435AF6FFA17FE9CEC4,
	HttpClientResponseData_IsHeaderPresent_m30F357B63026706FE4608F6CF0ACF0DE61CEFD97,
	HttpClientResponseData_GetHeaderNames_m81B95FF8126FE160F31D3514138294357010D98D,
	HttpClientResponseData_CopyHeaderValues_m27841F08DF2C9CE4637A42DE926221E55ACDC6FE,
	HttpClientResponseData_GetFirstHeaderValue_m144A7F4F5B4FAA39BE07FF562E83B4B8B86417A5,
	HttpClientResponseData_get_ResponseBody_m0F98A1C5A48B89DB121803B07E26B56642E7AA4E,
	HttpResponseMessageBody__ctor_mAFA22E937E2989AA503DFB611BD3588B2C2B01DE,
	HttpResponseMessageBody_OpenResponse_mEAACBBE0E59A9905C409584E70A75AA9DCD10E5A,
	HttpResponseMessageBody_OpenResponseAsync_mD4D13F06B794ED60E9374FA5048FFBB7CA62DB3B,
	HttpResponseMessageBody_Dispose_m4473323B5C04E2F05D56A4AF9B6988801E94B176,
	HttpResponseMessageBody_Dispose_m49BBB2DE145533EB0EC1E19DEA4F201E22F8DE75,
	NULL,
	InMemoryPersistenceManager_GetSettings_m0A9EDF687A4603B639FE692372F828455EB7009B,
	InMemoryPersistenceManager__ctor_m9C1B7BC7E6061EF292169262D92C7C06FCCC3BA3,
	PersistenceManager__cctor_m65BE274E472710BF3356BC83A45FEA8C3FCD4919,
	PersistenceManager_get_Instance_m184BC55A18A7E106F5F8FB191DDF160960FD3BE8,
	PersistenceManager_set_Instance_m4334BD1CAF6918F9F6B8443B13FDD70D646E9B86,
	PersistenceManager_GetSettings_mADE2264473AC6B47E73EB7E2B89C7A7DBC90BDE5,
	PersistenceManager_GetSettingsStoreFolder_m10A8A8AC242F6BA8A03677E69D856B7CF042AACC,
	PersistenceManager_IsEncrypted_mB5B4EB5FC8FF92A1900355A64E10257F26891378,
	PersistenceManager_loadSettingsType_mB0A7778060070487DBD29E5D331B69619929EDFF,
	PersistenceManager_DecryptAnyEncryptedValues_mD66BD639BF73CCA5036C5BCF21DC34F9980EA7D4,
	PersistenceManager_getFileFromType_mFCB6A8D4674474F1AF1786818D1203CCD4F9CC48,
	PersistenceManager__ctor_m21BB963082E733E352874993761BE7E314F94410,
	SettingsWatcher_Dispose_mE2877D6B7793815C65186B321D308898745B2AF1,
	SettingsWatcher_Dispose_m3F7E7B14B903619166E544FF67CFC29892817533,
	SettingsCollection__ctor_m80B01617D4A55C6AA7632F2C1DBC849432FC2D37,
	SettingsCollection__ctor_mB987F94277CFF117F7DEC3601C5912A3870538B6,
	SettingsCollection_set_InitializedEmpty_mE31FA906B0A88CA7AB7AAF997C4D9D6ABAD672EF,
	SettingsCollection_System_Collections_IEnumerable_GetEnumerator_mB7A549466820D744B5DD4DC19B38BBBF96A61393,
	SettingsCollection_GetEnumerator_m79DFE4C6FF170762974BF7E5745417DB8AEB56A5,
	SettingsCollection_get_Item_m4E0FA5D22AE65F8F37E22727191391B0C289B7EE,
	SettingsCollection_NewObjectSettings_m490A281213F84C156B48634771EBF5E39469952D,
	ObjectSettings__ctor_m64E581AC49461AC737806745A5BDF50E3EBC140E,
	ObjectSettings_get_UniqueKey_m7312E8A6A47D6501DA59DB1672904C8AC1525486,
	ObjectSettings_get_Item_mCBCF9DAC8B93B6ED105FBABDEABAA35FA4BE63E5,
	ObjectSettings_get_Keys_mE37A1CFE62D1DD3FC39302192558164102592CBC,
	U3CGetEnumeratorU3Ed__11__ctor_m20153A22BB2F12B09B9DC391189542CFB33D1A22,
	U3CGetEnumeratorU3Ed__11_System_IDisposable_Dispose_m8FCA25311749C595C065FAA5F91BEBAA1E6D5426,
	U3CGetEnumeratorU3Ed__11_MoveNext_m8CE3EF6E9500E047253C118EFC818CA38966BC7E,
	U3CGetEnumeratorU3Ed__11_U3CU3Em__Finally1_m9250318E741B953C390D2EBA04C35B301C30380E,
	U3CGetEnumeratorU3Ed__11_System_Collections_Generic_IEnumeratorU3CAmazon_Runtime_Internal_Settings_SettingsCollection_ObjectSettingsU3E_get_Current_m0522738F60385992B2C0E82EB07FDC81D1441B9A,
	U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_Reset_m44794CDDC5D62355A67F666A9C81997DBAFF01FE,
	U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_get_Current_m40917AA2601E7ECA679D9521E6E2079FDD54364E,
	UserCrypto_Decrypt_m32BE52AC9DDF6ABF3898F170DDB4F815D3847716,
	UserCrypto_Encrypt_mC46091927A279E4177694DE1DCA6D4F296687CFF,
	UserCrypto_ConvertData_mF2E0CCBE456719C807A83B691C4AF00B5EBEC09D,
	UserCrypto_CryptProtectData_m35CB475CFC28355EB1579A94059F23605201705E,
	UserCrypto_CryptUnprotectData_m6FFA94F14F374B3A4443A916E64015A74707CF1B,
	UserCrypto_get_IsUserCryptAvailable_m7FB6CC4F64076F9FFDBD5178664E28CBD9EC77B9,
	NULL,
	NULL,
	AbstractAWSSigner__ctor_m2210CF67382A7A9F1D5C75C86CF365ADBE9D9C92,
	AWS4Signer__ctor_m0C5E9E6AFF7BD86B722135DDE17D770894463306,
	AWS4Signer__ctor_mB5A14B9F33AE20576AB3316061E330EB1B2322E7,
	AWS4Signer_get_SignPayload_mE907B2FB3EF750B065F1C0D454816D9BDCC0A693,
	AWS4Signer_set_SignPayload_mB99439C13DC95FE5F3037223F4D2F0D955C193D5,
	AWS4Signer_get_Protocol_mADADEAD747FBE8D28B7EA1851CC567E43BA2B2DB,
	AWS4Signer_Sign_mAB14CA3C9B5EB5DEE4F5EE13A8E54C024D4997B7,
	AWS4Signer_SignRequest_m133716014A6F4073249471BA0DCBE0D3C486EAD3,
	AWS4Signer_InitializeHeaders_m2ECE67FD638341D8CB887C8CB788963E24DEC6DB,
	AWS4Signer_InitializeHeaders_mE4D30F98F4D92204D3AE3F3FA531A25984448303,
	AWS4Signer_CleanHeaders_m621F2FEBB2D608A1F419B6BAA27FA5CAC12D8DE0,
	AWS4Signer_ComputeSignature_m0D5230C6205966C4476E807D48BFD87A0DF12D0E,
	AWS4Signer_FormatDateTime_m3253B96360CEF42884B44308257942E669E834FD,
	AWS4Signer_ComposeSigningKey_mB4F4CA9462AE352BBD51996BFD5C9A95092E735D,
	AWS4Signer_SetRequestBodyHash_mDCA209DF67E384E30712AD61F28E04E93188B9E8,
	AWS4Signer_SignBlob_m1346F7E2FDA3E19350BFE45B8BBC2C2CAFCA1059,
	AWS4Signer_SignBlob_m5849C72A3FA29A9571F3800C320CC296F5940969,
	AWS4Signer_ComputeKeyedHash_mCEFCC216272392E2C12FC28A5097592B5082FBCB,
	AWS4Signer_ComputeKeyedHash_m0CE367A1DABB20F7D887AF6B8937FE8AF70B5836,
	AWS4Signer_ComputeHash_mD750A476074BFCF97AB830B5F6F454D7CD64F0BA,
	AWS4Signer_ComputeHash_mEE5A670DD54202F59E4C11A8D13FE5A05C82A10F,
	AWS4Signer_SetPayloadSignatureHeader_m204DC176FAA0329FA19399BF3F464C88CBBCF31E,
	AWS4Signer_DetermineSigningRegion_mB91334EE400FFF7C9598BD0872F4EBD12E9927A4,
	AWS4Signer_DetermineService_m90EAE7C10D4692EF4EA2BE101D05F42D54DB1DC5,
	AWS4Signer_CanonicalizeRequest_mCC536CD188B1C31DC34FAFDB51ADA25B97312000,
	AWS4Signer_CanonicalizeRequestHelper_m610FAD60AC36201DD6CC220ACAB3A808EA002868,
	AWS4Signer_SortAndPruneHeaders_m218F9F6BB7593BECC8CD2EC991F9E1DC45C397F0,
	AWS4Signer_CanonicalizeHeaders_m751B2C6AE63BEB2FCE83CA27D3B32EB350EE5138,
	AWS4Signer_CanonicalizeHeaderNames_m6B7BC32463DEE92D4383E27AA67DAE18246669F2,
	AWS4Signer_GetParametersToCanonicalize_mBE7D5D774177E1698AF3A0378DEA68D1E0C57E51,
	AWS4Signer_CanonicalizeQueryParameters_mC50DE6A1854F63B9424354562394F80CA8501DDA,
	AWS4Signer_CanonicalizeQueryParameters_mAD5B409CC39C81926146D09E28CE760DF2900F3F,
	AWS4Signer_GetRequestPayloadBytes_m835734FA621A2FE85C16696C12DB75F2F3DC4B42,
	AWS4Signer__cctor_m58ECCDA2CA525308010ECD7D1DC2D626600285C9,
	U3CU3Ec__cctor_mDA2383FAEC93E0DACEF31612513C2372B6076AAD,
	U3CU3Ec__ctor_mE156446C8A54E91CFE285947D1BE869D86B59E7B,
	U3CU3Ec_U3CGetParametersToCanonicalizeU3Eb__50_0_m3103F2F8DEE7B6CF889319F471577907D912D8E0,
	U3CU3Ec_U3CCanonicalizeQueryParametersU3Eb__54_0_m0F9B35205A926697DCCEC05155EA51C6D8C3018E,
	AWS4SigningResult__ctor_m589E62BD0D1CA8CED575FA89660FCB820A796B9C,
	AWS4SigningResult_get_AccessKeyId_m7D1FC51228C1FB5AEBC1B3A2D44BE5DA4108BE37,
	AWS4SigningResult_get_ISO8601DateTime_mCD7F3CB9F3646FDAF64FA1078C29C2EE944262F9,
	AWS4SigningResult_get_SignedHeaders_m1A77F6BD2F9360AC70122D8680423AD6A43671C0,
	AWS4SigningResult_get_Scope_m139C820A9C203453E3A33F98367EE15A7A4FAF45,
	AWS4SigningResult_get_SigningKey_mE3C2F29839A800C58452767C6D1C45789D201376,
	AWS4SigningResult_get_Signature_m0F9BD6BEEB495639E8BA55C56F72A02B4D405CD6,
	AWS4SigningResult_get_ForAuthorizationHeader_m4CCC89C7EDF649064288A3B9D1221030DC39A101,
	NullSigner_Sign_mCED14E4BB9CC1A74C922E9DDFC01F55141B3B1E2,
	NullSigner_get_Protocol_m06A447548CA02CA5B0968C4D0EE762403282DBF2,
	NullSigner__ctor_mEBD00FDBFBE3195CF706179F210A298542536FC7,
};
extern void ArrayMetadata_get_ElementType_mA0F8D763181E8C7897701F98499C20EC872E36E4_AdjustorThunk (void);
extern void ArrayMetadata_set_ElementType_m0165398A5F11604DE1D2C59516C3E6C66C92D395_AdjustorThunk (void);
extern void ArrayMetadata_get_IsArray_m9D939595CE680B7C8BC4B5EF5665022753A7E879_AdjustorThunk (void);
extern void ArrayMetadata_set_IsArray_m1CD739BA6B4D869DED37B2CCBBBB2901BC068997_AdjustorThunk (void);
extern void ArrayMetadata_get_IsList_m73EFF0512BB93D42859059FF0B98E9A4C26AB234_AdjustorThunk (void);
extern void ArrayMetadata_set_IsList_m05E0BFF65E6B91FCB52EF51354B7A492A331C1F9_AdjustorThunk (void);
extern void ObjectMetadata_get_ElementType_m4B736F26EDD2D73390884D9E93ACEF0588C0CEBC_AdjustorThunk (void);
extern void ObjectMetadata_set_ElementType_m133FC236AEB7BD106D60E0BF1A46EB330D1A8157_AdjustorThunk (void);
extern void ObjectMetadata_get_IsDictionary_m3D80DEBFB702D2C75364ABB5A9AEA71970F41DD9_AdjustorThunk (void);
extern void ObjectMetadata_set_IsDictionary_m1D62DE45F5999CB46B0BABD7D44DED6A3E2B2DD7_AdjustorThunk (void);
extern void ObjectMetadata_get_Properties_m9B4BE4987F4D9F01BD098A41645696DDD46C6A20_AdjustorThunk (void);
extern void ObjectMetadata_set_Properties_mF467235BB451CB7F69B6FCFD8344EF01082D14B3_AdjustorThunk (void);
extern void U3CGetCredentialsAsyncU3Ed__10_MoveNext_m005DC9454535AFC3DDDADBBC988058FF8A4875A2_AdjustorThunk (void);
extern void U3CGetCredentialsAsyncU3Ed__10_SetStateMachine_mFA86F065CDB16A8B134F7E86E7E51EFEBE33081E_AdjustorThunk (void);
extern void U3CGetResponseAsyncU3Ed__20_MoveNext_m929B5DA511CAF4416ADF67FCD460262B60BE78F8_AdjustorThunk (void);
extern void U3CGetResponseAsyncU3Ed__20_SetStateMachine_m10865D2EFB43097DB415EFB5DB496B817C850ED5_AdjustorThunk (void);
extern void U3CRetryAsyncU3Ed__57_MoveNext_m1678AA3BDDFA116D31DCD6FD44884DEEAF509ECF_AdjustorThunk (void);
extern void U3CRetryAsyncU3Ed__57_SetStateMachine_mF497DCFE92BB472842C19A106F09007A860FFF71_AdjustorThunk (void);
extern void U3CPostMessagesOverUDPAsyncU3Ed__10_MoveNext_mEDBD4C9D6810E90196AF654DF1B20F41C6AD1B00_AdjustorThunk (void);
extern void U3CPostMessagesOverUDPAsyncU3Ed__10_SetStateMachine_mE5C9EEDE31FEEC56BCF88F148813303D2AA51CAC_AdjustorThunk (void);
extern void U3CUnmarshallAsyncU3Ed__5_MoveNext_mEFF02A4DC2C96E2D42CB3555EEB8D738EDBC2090_AdjustorThunk (void);
extern void U3CUnmarshallAsyncU3Ed__5_SetStateMachine_m92CF4477AB1C3D3C38CED8DE399CC58D1ED0F3F4_AdjustorThunk (void);
extern void U3CObtainSendTokenAsyncU3Ed__11_MoveNext_mF2D1AE364315836D27E81DFC6A89EC6AE0A7E305_AdjustorThunk (void);
extern void U3CObtainSendTokenAsyncU3Ed__11_SetStateMachine_m5B189B23DDAAE678549DE1625AD265E0EBE6E43D_AdjustorThunk (void);
extern void U3CTryAcquireTokenAsyncU3Ed__55_MoveNext_m2F7C74047347318A1AB0C15C16CDF097C9E0E7EC_AdjustorThunk (void);
extern void U3CTryAcquireTokenAsyncU3Ed__55_SetStateMachine_mC8FB6D650565057DAF0C7310E6CFC027923C0B62_AdjustorThunk (void);
extern void U3CWaitForTokenAsyncU3Ed__67_MoveNext_mDBAC6DA4007E72A458F4CD637B22715939DC38E3_AdjustorThunk (void);
extern void U3CWaitForTokenAsyncU3Ed__67_SetStateMachine_mCA1B76DFBDD3ED10552418255052F57DA8067186_AdjustorThunk (void);
extern void U3CReadAsyncU3Ed__10_MoveNext_m4EF4BA3B7A01EDDE2FAA1C6D06393F3CDAABF912_AdjustorThunk (void);
extern void U3CReadAsyncU3Ed__10_SetStateMachine_mFD24E47F2568DF602D75FEB1AD250C09AF1402F3_AdjustorThunk (void);
extern void U3CReadAsyncU3Ed__17_MoveNext_mE30629A8299DF67D7AA06C25BD9F50149D44259E_AdjustorThunk (void);
extern void U3CReadAsyncU3Ed__17_SetStateMachine_mAEBAEDD593301A03FA75541227C724FE80BE9FEB_AdjustorThunk (void);
extern void U3CFillInputBufferAsyncU3Ed__18_MoveNext_m8DD329E339F709CACD5CE1C40669C0663D0F7DF0_AdjustorThunk (void);
extern void U3CFillInputBufferAsyncU3Ed__18_SetStateMachine_mD3F93DDF86B76F1C0F6546C305314C798C2255EC_AdjustorThunk (void);
extern void U3CReadAsyncU3Ed__33_MoveNext_mBC419EA13A9997E3E0E65F36F59433A8936F32B7_AdjustorThunk (void);
extern void U3CReadAsyncU3Ed__33_SetStateMachine_m93DC06F8C05B23E12F7E832FF063A3D652DDA55E_AdjustorThunk (void);
extern void PathSegment_get_SegmentType_m65B82F8564E2206BCE241B78F79E678AA696D575_AdjustorThunk (void);
extern void PathSegment_set_SegmentType_m40F0CBA4B9A4C76E4256AD3195C964DD88906D2A_AdjustorThunk (void);
extern void PathSegment_get_Value_m35D398000A8EA78DE5FAF47E7BC0BF1C44EDBDD2_AdjustorThunk (void);
extern void PathSegment_set_Value_m909F2ED45DF5DCABEA9D81BC2761C3D8267F5101_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[40] = 
{
	{ 0x06000070, ArrayMetadata_get_ElementType_mA0F8D763181E8C7897701F98499C20EC872E36E4_AdjustorThunk },
	{ 0x06000071, ArrayMetadata_set_ElementType_m0165398A5F11604DE1D2C59516C3E6C66C92D395_AdjustorThunk },
	{ 0x06000072, ArrayMetadata_get_IsArray_m9D939595CE680B7C8BC4B5EF5665022753A7E879_AdjustorThunk },
	{ 0x06000073, ArrayMetadata_set_IsArray_m1CD739BA6B4D869DED37B2CCBBBB2901BC068997_AdjustorThunk },
	{ 0x06000074, ArrayMetadata_get_IsList_m73EFF0512BB93D42859059FF0B98E9A4C26AB234_AdjustorThunk },
	{ 0x06000075, ArrayMetadata_set_IsList_m05E0BFF65E6B91FCB52EF51354B7A492A331C1F9_AdjustorThunk },
	{ 0x06000076, ObjectMetadata_get_ElementType_m4B736F26EDD2D73390884D9E93ACEF0588C0CEBC_AdjustorThunk },
	{ 0x06000077, ObjectMetadata_set_ElementType_m133FC236AEB7BD106D60E0BF1A46EB330D1A8157_AdjustorThunk },
	{ 0x06000078, ObjectMetadata_get_IsDictionary_m3D80DEBFB702D2C75364ABB5A9AEA71970F41DD9_AdjustorThunk },
	{ 0x06000079, ObjectMetadata_set_IsDictionary_m1D62DE45F5999CB46B0BABD7D44DED6A3E2B2DD7_AdjustorThunk },
	{ 0x0600007A, ObjectMetadata_get_Properties_m9B4BE4987F4D9F01BD098A41645696DDD46C6A20_AdjustorThunk },
	{ 0x0600007B, ObjectMetadata_set_Properties_mF467235BB451CB7F69B6FCFD8344EF01082D14B3_AdjustorThunk },
	{ 0x06000302, U3CGetCredentialsAsyncU3Ed__10_MoveNext_m005DC9454535AFC3DDDADBBC988058FF8A4875A2_AdjustorThunk },
	{ 0x06000303, U3CGetCredentialsAsyncU3Ed__10_SetStateMachine_mFA86F065CDB16A8B134F7E86E7E51EFEBE33081E_AdjustorThunk },
	{ 0x0600037F, U3CGetResponseAsyncU3Ed__20_MoveNext_m929B5DA511CAF4416ADF67FCD460262B60BE78F8_AdjustorThunk },
	{ 0x06000380, U3CGetResponseAsyncU3Ed__20_SetStateMachine_m10865D2EFB43097DB415EFB5DB496B817C850ED5_AdjustorThunk },
	{ 0x060003A3, U3CRetryAsyncU3Ed__57_MoveNext_m1678AA3BDDFA116D31DCD6FD44884DEEAF509ECF_AdjustorThunk },
	{ 0x060003A4, U3CRetryAsyncU3Ed__57_SetStateMachine_mF497DCFE92BB472842C19A106F09007A860FFF71_AdjustorThunk },
	{ 0x060003D9, U3CPostMessagesOverUDPAsyncU3Ed__10_MoveNext_mEDBD4C9D6810E90196AF654DF1B20F41C6AD1B00_AdjustorThunk },
	{ 0x060003DA, U3CPostMessagesOverUDPAsyncU3Ed__10_SetStateMachine_mE5C9EEDE31FEEC56BCF88F148813303D2AA51CAC_AdjustorThunk },
	{ 0x06000594, U3CUnmarshallAsyncU3Ed__5_MoveNext_mEFF02A4DC2C96E2D42CB3555EEB8D738EDBC2090_AdjustorThunk },
	{ 0x06000595, U3CUnmarshallAsyncU3Ed__5_SetStateMachine_m92CF4477AB1C3D3C38CED8DE399CC58D1ED0F3F4_AdjustorThunk },
	{ 0x060005B8, U3CObtainSendTokenAsyncU3Ed__11_MoveNext_mF2D1AE364315836D27E81DFC6A89EC6AE0A7E305_AdjustorThunk },
	{ 0x060005B9, U3CObtainSendTokenAsyncU3Ed__11_SetStateMachine_m5B189B23DDAAE678549DE1625AD265E0EBE6E43D_AdjustorThunk },
	{ 0x06000623, U3CTryAcquireTokenAsyncU3Ed__55_MoveNext_m2F7C74047347318A1AB0C15C16CDF097C9E0E7EC_AdjustorThunk },
	{ 0x06000624, U3CTryAcquireTokenAsyncU3Ed__55_SetStateMachine_mC8FB6D650565057DAF0C7310E6CFC027923C0B62_AdjustorThunk },
	{ 0x06000625, U3CWaitForTokenAsyncU3Ed__67_MoveNext_mDBAC6DA4007E72A458F4CD637B22715939DC38E3_AdjustorThunk },
	{ 0x06000626, U3CWaitForTokenAsyncU3Ed__67_SetStateMachine_mCA1B76DFBDD3ED10552418255052F57DA8067186_AdjustorThunk },
	{ 0x0600069A, U3CReadAsyncU3Ed__10_MoveNext_m4EF4BA3B7A01EDDE2FAA1C6D06393F3CDAABF912_AdjustorThunk },
	{ 0x0600069B, U3CReadAsyncU3Ed__10_SetStateMachine_mFD24E47F2568DF602D75FEB1AD250C09AF1402F3_AdjustorThunk },
	{ 0x060006B0, U3CReadAsyncU3Ed__17_MoveNext_mE30629A8299DF67D7AA06C25BD9F50149D44259E_AdjustorThunk },
	{ 0x060006B1, U3CReadAsyncU3Ed__17_SetStateMachine_mAEBAEDD593301A03FA75541227C724FE80BE9FEB_AdjustorThunk },
	{ 0x060006B2, U3CFillInputBufferAsyncU3Ed__18_MoveNext_m8DD329E339F709CACD5CE1C40669C0663D0F7DF0_AdjustorThunk },
	{ 0x060006B3, U3CFillInputBufferAsyncU3Ed__18_SetStateMachine_mD3F93DDF86B76F1C0F6546C305314C798C2255EC_AdjustorThunk },
	{ 0x060006DB, U3CReadAsyncU3Ed__33_MoveNext_mBC419EA13A9997E3E0E65F36F59433A8936F32B7_AdjustorThunk },
	{ 0x060006DC, U3CReadAsyncU3Ed__33_SetStateMachine_m93DC06F8C05B23E12F7E832FF063A3D652DDA55E_AdjustorThunk },
	{ 0x060007D6, PathSegment_get_SegmentType_m65B82F8564E2206BCE241B78F79E678AA696D575_AdjustorThunk },
	{ 0x060007D7, PathSegment_set_SegmentType_m40F0CBA4B9A4C76E4256AD3195C964DD88906D2A_AdjustorThunk },
	{ 0x060007D8, PathSegment_get_Value_m35D398000A8EA78DE5FAF47E7BC0BF1C44EDBDD2_AdjustorThunk },
	{ 0x060007D9, PathSegment_set_Value_m909F2ED45DF5DCABEA9D81BC2761C3D8267F5101_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2230] = 
{
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4123,
	4144,
	4144,
	4145,
	4145,
	4169,
	3476,
	3406,
	3428,
	3428,
	3428,
	3429,
	3429,
	3450,
	4169,
	3450,
	4144,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4144,
	4169,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	4197,
	2558,
	1920,
	2555,
	1773,
	4169,
	2558,
	1920,
	2555,
	1773,
	4257,
	3450,
	6360,
	6309,
	1915,
	1920,
	2923,
	4169,
	3450,
	4169,
	4197,
	4123,
	4144,
	4144,
	4145,
	4145,
	4169,
	3476,
	3406,
	3428,
	3428,
	3429,
	3429,
	3450,
	4169,
	3450,
	2380,
	4257,
	2923,
	2380,
	1773,
	3450,
	3428,
	4169,
	4169,
	4169,
	4169,
	2558,
	6005,
	2380,
	2923,
	3428,
	4169,
	3450,
	4169,
	4169,
	4122,
	4169,
	4169,
	3450,
	4197,
	4257,
	3428,
	3450,
	4169,
	3450,
	4197,
	3476,
	4197,
	3476,
	4169,
	3450,
	4197,
	3476,
	4169,
	3450,
	1917,
	1920,
	623,
	3450,
	1917,
	2558,
	977,
	2558,
	1917,
	4169,
	1417,
	2558,
	6598,
	6502,
	6502,
	6502,
	5667,
	5667,
	6005,
	5667,
	6598,
	6598,
	5105,
	5108,
	6309,
	6309,
	6309,
	-1,
	5667,
	5667,
	6598,
	4257,
	1920,
	1920,
	1920,
	1920,
	1920,
	1920,
	1920,
	1920,
	1920,
	1920,
	1920,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	2558,
	4169,
	4169,
	4197,
	4144,
	4169,
	3450,
	3450,
	1926,
	3450,
	4257,
	4197,
	4257,
	4197,
	4257,
	4169,
	4197,
	3476,
	6598,
	4257,
	3450,
	3450,
	3428,
	4257,
	5983,
	4257,
	3450,
	4257,
	3476,
	3450,
	4257,
	4169,
	4257,
	3476,
	3404,
	3406,
	3428,
	3428,
	3429,
	3450,
	3429,
	4257,
	4257,
	4257,
	4257,
	3450,
	4257,
	4197,
	4144,
	4169,
	6598,
	3450,
	6195,
	6598,
	6180,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	6360,
	4197,
	4144,
	4197,
	4257,
	1917,
	2923,
	977,
	2923,
	4145,
	4144,
	1175,
	6598,
	4257,
	1916,
	4144,
	885,
	1175,
	4197,
	4197,
	4197,
	4257,
	4145,
	4145,
	3429,
	1382,
	6073,
	-1,
	-1,
	6555,
	6587,
	6508,
	6579,
	6579,
	6570,
	6579,
	6579,
	6579,
	6587,
	6579,
	6579,
	6502,
	6502,
	6502,
	5789,
	-1,
	-1,
	-1,
	6563,
	6309,
	6360,
	6579,
	6309,
	6598,
	6309,
	6309,
	5667,
	6579,
	1920,
	4169,
	3450,
	4169,
	3450,
	4169,
	1419,
	4169,
	6598,
	1189,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	1419,
	2558,
	6579,
	2558,
	4257,
	1419,
	2558,
	5667,
	6309,
	6598,
	6502,
	6502,
	6598,
	6587,
	6502,
	6502,
	1920,
	4169,
	3450,
	4169,
	3450,
	4169,
	6598,
	4169,
	3450,
	4169,
	3450,
	782,
	1419,
	1419,
	4257,
	1920,
	6005,
	1189,
	783,
	5667,
	6309,
	2562,
	2923,
	1199,
	1083,
	4257,
	4257,
	6579,
	6309,
	5322,
	2558,
	2558,
	6598,
	4169,
	4169,
	4257,
	4144,
	3428,
	4144,
	3428,
	4144,
	3428,
	4197,
	3476,
	4144,
	4169,
	4257,
	6598,
	4169,
	4144,
	4169,
	4014,
	4257,
	6579,
	6309,
	6309,
	4669,
	5667,
	5668,
	5667,
	6309,
	6309,
	6472,
	6209,
	6221,
	5668,
	-1,
	6579,
	5788,
	5788,
	-1,
	5668,
	5251,
	5668,
	6563,
	6360,
	6355,
	4501,
	4976,
	5667,
	6309,
	6498,
	6598,
	6598,
	4257,
	2558,
	2558,
	2558,
	2558,
	4257,
	2558,
	-1,
	-1,
	-1,
	4257,
	4169,
	4257,
	4169,
	6579,
	6598,
	4257,
	2558,
	2558,
	976,
	2555,
	6579,
	2558,
	2558,
	976,
	6587,
	6579,
	6579,
	6579,
	6309,
	5662,
	6305,
	6598,
	5268,
	4954,
	6498,
	6598,
	4257,
	6309,
	6579,
	6579,
	6598,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3476,
	4197,
	3476,
	4197,
	4257,
	4169,
	4169,
	2558,
	4169,
	4169,
	2558,
	1417,
	2558,
	2558,
	2923,
	4197,
	4197,
	4169,
	6309,
	6598,
	3450,
	4169,
	4144,
	2923,
	4169,
	4169,
	2558,
	4169,
	4169,
	2558,
	1417,
	2558,
	2923,
	4197,
	4197,
	2558,
	4169,
	3450,
	4169,
	2558,
	4169,
	4169,
	2558,
	4197,
	4197,
	1417,
	2558,
	2923,
	4169,
	2558,
	6598,
	4257,
	2923,
	2558,
	4257,
	4257,
	6598,
	4169,
	6579,
	2558,
	3450,
	1068,
	6587,
	3450,
	4169,
	3450,
	696,
	4169,
	5029,
	6598,
	4257,
	2923,
	4169,
	4169,
	4257,
	-1,
	-1,
	6598,
	3450,
	1920,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	4197,
	3450,
	3450,
	1920,
	4169,
	3450,
	4257,
	-1,
	3450,
	3450,
	3450,
	1920,
	4257,
	3476,
	4257,
	4169,
	3450,
	4257,
	6309,
	6502,
	6502,
	6598,
	4257,
	2697,
	3450,
	1920,
	1187,
	236,
	5662,
	4169,
	4169,
	3450,
	4144,
	4169,
	4197,
	781,
	400,
	4169,
	3450,
	4169,
	3450,
	4169,
	5469,
	4169,
	4257,
	1920,
	4197,
	4197,
	4197,
	4169,
	4169,
	4169,
	3450,
	4145,
	3429,
	4144,
	3428,
	4257,
	4169,
	3450,
	3450,
	4257,
	4257,
	4257,
	4257,
	3450,
	1920,
	6598,
	6579,
	6502,
	6579,
	6502,
	6598,
	6579,
	6315,
	1917,
	4169,
	1417,
	2558,
	6598,
	4257,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4197,
	4169,
	3450,
	4169,
	4169,
	4197,
	4169,
	4967,
	4169,
	3450,
	4169,
	3450,
	4144,
	4197,
	4197,
	4144,
	4145,
	4197,
	4197,
	4197,
	4197,
	4169,
	4257,
	4257,
	4036,
	4197,
	4197,
	4257,
	4245,
	4197,
	4144,
	4144,
	4197,
	4197,
	4144,
	6579,
	4169,
	4024,
	4169,
	6360,
	6360,
	6309,
	6360,
	6598,
	3450,
	4169,
	3450,
	4169,
	6309,
	4169,
	-1,
	6502,
	4144,
	2923,
	2923,
	2923,
	5788,
	5788,
	6598,
	6555,
	6475,
	6146,
	6011,
	6598,
	4169,
	4257,
	4169,
	4169,
	4257,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4197,
	1189,
	4257,
	4169,
	4144,
	2923,
	4245,
	4169,
	4169,
	6011,
	5790,
	4169,
	4169,
	3476,
	4257,
	4257,
	4257,
	4169,
	4169,
	3450,
	4119,
	3402,
	1911,
	2997,
	4257,
	3450,
	1917,
	1920,
	623,
	3450,
	4257,
	4257,
	3450,
	3450,
	3450,
	3450,
	3450,
	3450,
	5667,
	4169,
	4169,
	4169,
	4197,
	4169,
	4169,
	4169,
	4197,
	4197,
	4197,
	4197,
	4144,
	4144,
	4145,
	4197,
	4169,
	4036,
	4197,
	4197,
	4169,
	4245,
	4197,
	4144,
	4144,
	4197,
	4024,
	4197,
	4144,
	4169,
	4169,
	4257,
	4169,
	3450,
	3450,
	4169,
	3450,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	3450,
	4169,
	3450,
	4197,
	3476,
	4197,
	4144,
	3428,
	4144,
	4144,
	3428,
	4107,
	4169,
	3450,
	4169,
	3450,
	4169,
	4197,
	4197,
	3476,
	4169,
	3450,
	4169,
	3450,
	4169,
	4169,
	1506,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2558,
	2923,
	2923,
	2558,
	3450,
	2558,
	4257,
	3476,
	6309,
	6309,
	6309,
	6598,
	3450,
	4169,
	4257,
	3476,
	1189,
	3450,
	3450,
	3450,
	2546,
	782,
	1189,
	4169,
	3450,
	4257,
	3476,
	616,
	6598,
	4257,
	3450,
	3450,
	4169,
	3450,
	4169,
	3450,
	-1,
	4144,
	3428,
	4169,
	3450,
	4169,
	4169,
	4169,
	4169,
	4169,
	3450,
	2923,
	2923,
	3450,
	1510,
	1086,
	2923,
	1506,
	2923,
	1506,
	5785,
	5785,
	6309,
	1417,
	1417,
	1417,
	2558,
	4257,
	6598,
	4257,
	3450,
	4257,
	3450,
	6309,
	1917,
	1920,
	623,
	3450,
	4257,
	4257,
	4169,
	3450,
	3450,
	3450,
	3450,
	3450,
	3450,
	6309,
	1917,
	1920,
	623,
	3450,
	4197,
	4245,
	2558,
	1917,
	1920,
	623,
	3450,
	4257,
	4257,
	3450,
	4169,
	3450,
	3450,
	3450,
	3450,
	3450,
	3450,
	5272,
	4169,
	3450,
	4169,
	4257,
	1155,
	4144,
	4169,
	4257,
	6598,
	6579,
	2558,
	4257,
	3476,
	4257,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4023,
	3306,
	4014,
	3296,
	3296,
	4035,
	3321,
	3317,
	4031,
	3315,
	4024,
	3308,
	4169,
	3450,
	4197,
	3450,
	4017,
	1920,
	4169,
	4169,
	2923,
	4144,
	6598,
	4257,
	2534,
	2534,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	2923,
	4144,
	4257,
	4169,
	3450,
	4257,
	3450,
	1501,
	1501,
	4257,
	1501,
	6598,
	6598,
	4169,
	3450,
	3450,
	3450,
	1501,
	4257,
	1083,
	1501,
	6350,
	3450,
	390,
	6598,
	6598,
	4257,
	2923,
	2558,
	6066,
	6309,
	6598,
	4257,
	3476,
	1139,
	734,
	1504,
	1773,
	2558,
	1501,
	2558,
	5983,
	6598,
	4144,
	3428,
	4144,
	3428,
	3476,
	3476,
	3429,
	3476,
	3429,
	4257,
	1920,
	4169,
	4169,
	3450,
	4197,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	3450,
	4169,
	3450,
	4169,
	4144,
	3428,
	4169,
	3450,
	4197,
	3476,
	4169,
	4145,
	4169,
	4169,
	4169,
	4169,
	3450,
	4197,
	4169,
	4197,
	3476,
	4169,
	3450,
	4169,
	3450,
	4197,
	4197,
	4197,
	2558,
	6598,
	4257,
	2923,
	4169,
	4169,
	1920,
	1920,
	403,
	403,
	4144,
	3428,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4144,
	3428,
	4257,
	4169,
	4197,
	4014,
	3296,
	4031,
	3315,
	4024,
	3308,
	4257,
	4257,
	1501,
	-1,
	3450,
	1920,
	6598,
	6598,
	-1,
	6553,
	6552,
	1917,
	4169,
	1417,
	2558,
	4257,
	4169,
	4169,
	6598,
	4257,
	2092,
	2103,
	2098,
	1917,
	2558,
	977,
	2558,
	4257,
	4169,
	3450,
	4169,
	3450,
	4169,
	4169,
	4257,
	4169,
	4169,
	4197,
	4169,
	4169,
	4169,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	4144,
	3428,
	4169,
	3450,
	2558,
	4197,
	3476,
	4169,
	4145,
	4169,
	4169,
	4169,
	4169,
	4169,
	4197,
	4169,
	4197,
	4169,
	3450,
	4169,
	3450,
	4197,
	4197,
	4197,
	4169,
	4169,
	3450,
	6309,
	6005,
	1920,
	4144,
	2923,
	2923,
	2558,
	1920,
	4169,
	1501,
	2697,
	4169,
	3252,
	2697,
	1915,
	4197,
	4169,
	4169,
	4257,
	3428,
	4257,
	4197,
	4257,
	3978,
	4257,
	4169,
	6579,
	4257,
	1920,
	4257,
	3476,
	6598,
	4169,
	1920,
	4169,
	4169,
	4257,
	779,
	3428,
	4257,
	1920,
	4169,
	4169,
	3428,
	4257,
	4197,
	4257,
	4257,
	3978,
	4257,
	4169,
	4169,
	4169,
	1948,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4144,
	3428,
	4144,
	4144,
	3428,
	4197,
	3476,
	4197,
	3476,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	4107,
	3386,
	4169,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4197,
	3476,
	4197,
	3476,
	4169,
	3450,
	4169,
	3450,
	4257,
	4169,
	3450,
	4169,
	3450,
	1920,
	4169,
	3450,
	-1,
	6502,
	1506,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3450,
	1506,
	1506,
	4169,
	3450,
	4169,
	3450,
	-1,
	3450,
	3450,
	3450,
	3450,
	4257,
	-1,
	-1,
	-1,
	3450,
	4169,
	3450,
	-1,
	-1,
	-1,
	-1,
	-1,
	6502,
	6005,
	6008,
	5277,
	6309,
	6360,
	4257,
	-1,
	-1,
	-1,
	-1,
	3450,
	2558,
	5667,
	5272,
	4257,
	4169,
	3450,
	-1,
	1920,
	4257,
	-1,
	-1,
	-1,
	-1,
	6502,
	4257,
	-1,
	4257,
	-1,
	-1,
	-1,
	-1,
	6502,
	6360,
	6502,
	4257,
	3476,
	-1,
	2558,
	1417,
	5800,
	-1,
	-1,
	-1,
	4257,
	3450,
	4169,
	3450,
	3450,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	-1,
	3450,
	4257,
	4169,
	3450,
	1086,
	3450,
	1417,
	2558,
	1417,
	4257,
	3450,
	4144,
	3450,
	2923,
	1510,
	1086,
	3450,
	1506,
	2923,
	5576,
	1417,
	2558,
	6598,
	4169,
	3450,
	4169,
	3450,
	3450,
	-1,
	6502,
	1920,
	1920,
	-1,
	-1,
	-1,
	6579,
	4144,
	3450,
	2923,
	1510,
	1086,
	3450,
	1506,
	2923,
	5576,
	1417,
	2558,
	6598,
	4169,
	1920,
	-1,
	3450,
	-1,
	-1,
	6005,
	6309,
	3450,
	4257,
	3476,
	4257,
	-1,
	6005,
	3450,
	6005,
	6005,
	4257,
	-1,
	-1,
	-1,
	-1,
	3450,
	6005,
	3450,
	4257,
	-1,
	-1,
	-1,
	4022,
	3305,
	4022,
	3305,
	4123,
	3406,
	4022,
	3305,
	4123,
	3406,
	4123,
	3406,
	4145,
	3429,
	4123,
	3406,
	4123,
	3406,
	4123,
	3406,
	4197,
	3476,
	4257,
	345,
	943,
	2091,
	2347,
	3476,
	4257,
	3406,
	4257,
	4257,
	2206,
	2206,
	867,
	1405,
	4123,
	6565,
	6598,
	4257,
	3450,
	4257,
	3450,
	4169,
	3450,
	4144,
	3428,
	4197,
	3476,
	4169,
	3450,
	4257,
	4169,
	3450,
	4197,
	3476,
	3450,
	4169,
	3450,
	4257,
	4169,
	6598,
	4257,
	4257,
	4257,
	1917,
	4257,
	1417,
	3450,
	4169,
	3450,
	1920,
	1920,
	4169,
	3450,
	3450,
	3450,
	6309,
	5272,
	5785,
	5785,
	5667,
	4169,
	3450,
	4257,
	6598,
	6579,
	3450,
	4257,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4145,
	3429,
	4169,
	3450,
	4144,
	4169,
	3450,
	4169,
	3450,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4024,
	3308,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4145,
	3429,
	3450,
	4144,
	3428,
	4145,
	3429,
	4197,
	3476,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4024,
	3308,
	4169,
	3450,
	4169,
	1901,
	885,
	607,
	1175,
	4197,
	4145,
	3429,
	1382,
	607,
	4257,
	3450,
	1178,
	885,
	509,
	607,
	2546,
	4169,
	3450,
	4169,
	3450,
	3428,
	4145,
	4197,
	6214,
	6214,
	4144,
	4197,
	6598,
	6598,
	4257,
	2923,
	4257,
	3450,
	4257,
	3450,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4257,
	6598,
	4257,
	3450,
	3450,
	3450,
	1926,
	3476,
	4197,
	4197,
	4197,
	4145,
	4145,
	3429,
	4144,
	3428,
	4144,
	4257,
	885,
	1382,
	1175,
	3476,
	607,
	607,
	1917,
	3428,
	955,
	3450,
	4257,
	3450,
	6218,
	6360,
	6598,
	5785,
	5785,
	6202,
	6202,
	5576,
	6598,
	4257,
	2380,
	4169,
	3429,
	3450,
	4257,
	4257,
	1920,
	1189,
	1920,
	1189,
	3450,
	1189,
	1189,
	1920,
	1920,
	1147,
	4257,
	3450,
	1920,
	4257,
	6309,
	6579,
	1189,
	1189,
	1920,
	1920,
	6598,
	4169,
	3450,
	4197,
	3476,
	3450,
	4197,
	4197,
	4197,
	1189,
	1189,
	1920,
	1920,
	6598,
	3450,
	4197,
	1189,
	4197,
	1189,
	1920,
	4197,
	1920,
	6598,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	1189,
	4169,
	-1,
	-1,
	-1,
	4145,
	1147,
	5460,
	5460,
	5460,
	6309,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4197,
	3476,
	4257,
	2555,
	2555,
	1773,
	1756,
	4169,
	4169,
	4169,
	6598,
	4257,
	2555,
	4257,
	3429,
	3429,
	4197,
	3476,
	4145,
	4245,
	1915,
	3476,
	4257,
	4257,
	4144,
	3428,
	4169,
	3450,
	4169,
	3450,
	4119,
	3402,
	1147,
	747,
	3450,
	3476,
	6360,
	6598,
	6309,
	6309,
	6305,
	6598,
	4169,
	3450,
	3450,
	4169,
	4169,
	6309,
	2558,
	5667,
	4197,
	4197,
	4197,
	3476,
	4145,
	4145,
	3429,
	4144,
	3428,
	4144,
	4257,
	885,
	1382,
	1175,
	607,
	607,
	4197,
	3450,
	4169,
	4169,
	1501,
	1501,
	1068,
	4169,
	698,
	4257,
	1068,
	1501,
	1454,
	1037,
	2555,
	6360,
	6360,
	5785,
	6360,
	5316,
	2555,
	3450,
	1189,
	1189,
	1920,
	1920,
	4197,
	4197,
	4197,
	6309,
	5662,
	5662,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	3450,
	4169,
	4257,
	6360,
	6309,
	4197,
	3476,
	1926,
	1501,
	-1,
	4169,
	3450,
	1920,
	1920,
	4257,
	4257,
	4169,
	4257,
	3450,
	-1,
	-1,
	-1,
	-1,
	2558,
	6005,
	6360,
	6579,
	4257,
	-1,
	3450,
	3450,
	4169,
	3450,
	1920,
	-1,
	4145,
	4169,
	4144,
	4169,
	2923,
	2558,
	4169,
	4169,
	4169,
	2558,
	5085,
	6360,
	6579,
	4257,
	796,
	4197,
	4197,
	4197,
	4144,
	4169,
	4197,
	2894,
	4169,
	4144,
	4144,
	4144,
	4257,
	3476,
	4144,
	3428,
	4169,
	3450,
	4144,
	4169,
	3577,
	4299,
	4299,
	4144,
	4257,
	321,
	4197,
	976,
	2558,
	2558,
	632,
	1510,
	4257,
	2558,
	976,
	2558,
	976,
	632,
	4257,
	2558,
	976,
	2558,
	976,
	632,
	1510,
	4257,
	-1,
	-1,
	4257,
	6579,
	2380,
	2380,
	6598,
	4257,
	6579,
	2923,
	2923,
	6598,
	4257,
	6579,
	6579,
	2558,
	2558,
	6598,
	4257,
	6579,
	2194,
	2194,
	5497,
	6598,
	4257,
	6579,
	2558,
	2558,
	6598,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4197,
	3476,
	4197,
	3476,
	4169,
	3450,
	4144,
	3428,
	4169,
	3450,
	4169,
	3450,
	4169,
	4169,
	4169,
	4257,
	1188,
	2923,
	1504,
	2894,
	5788,
	5020,
	4169,
	4144,
	4197,
	4169,
	4197,
	4197,
	4197,
	3476,
	4257,
	4257,
	4169,
	796,
	4169,
	4144,
	4197,
	4169,
	4197,
	4197,
	4197,
	4197,
	6309,
	4257,
	3476,
	6598,
	1190,
	4144,
	3428,
	3476,
	4169,
	3450,
	4145,
	3429,
	2558,
	2923,
	4169,
	3450,
	1417,
	4169,
	1190,
	4169,
	4169,
	4257,
	3476,
	2558,
	2558,
	4257,
	6598,
	6579,
	6502,
	2558,
	6579,
	6360,
	2558,
	6502,
	6309,
	4257,
	4257,
	3476,
	4257,
	3450,
	3476,
	4169,
	4169,
	2558,
	2558,
	1920,
	4169,
	2558,
	4169,
	3428,
	4257,
	4197,
	4257,
	4169,
	4257,
	4169,
	6309,
	6309,
	6525,
	4436,
	4436,
	6587,
	401,
	4144,
	4257,
	4257,
	3476,
	4197,
	3476,
	4144,
	401,
	315,
	5535,
	5159,
	6502,
	4378,
	5642,
	4964,
	5668,
	5667,
	5667,
	5250,
	5250,
	6309,
	6309,
	5667,
	4964,
	6309,
	4354,
	4355,
	6309,
	6309,
	6309,
	6309,
	6309,
	5668,
	6309,
	6598,
	6598,
	4257,
	2697,
	2534,
	222,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	4169,
	401,
	4144,
	4257,
};
static const Il2CppTokenRangePair s_rgctxIndices[68] = 
{
	{ 0x02000030, { 19, 1 } },
	{ 0x020000C4, { 37, 10 } },
	{ 0x020000C5, { 47, 2 } },
	{ 0x020000C8, { 54, 9 } },
	{ 0x020000CA, { 68, 10 } },
	{ 0x020000CC, { 83, 9 } },
	{ 0x020000CF, { 98, 9 } },
	{ 0x020000D2, { 113, 9 } },
	{ 0x020000D5, { 128, 11 } },
	{ 0x020000D8, { 139, 8 } },
	{ 0x020000D9, { 151, 16 } },
	{ 0x020000DA, { 167, 2 } },
	{ 0x020000E0, { 175, 11 } },
	{ 0x020000E4, { 194, 9 } },
	{ 0x020000E6, { 208, 9 } },
	{ 0x020000FD, { 217, 2 } },
	{ 0x020000FE, { 219, 2 } },
	{ 0x020000FF, { 221, 11 } },
	{ 0x02000113, { 232, 5 } },
	{ 0x02000129, { 240, 1 } },
	{ 0x0200012A, { 241, 6 } },
	{ 0x0200013A, { 247, 2 } },
	{ 0x02000140, { 249, 12 } },
	{ 0x02000141, { 261, 10 } },
	{ 0x02000142, { 271, 11 } },
	{ 0x06000097, { 0, 2 } },
	{ 0x06000121, { 2, 1 } },
	{ 0x06000122, { 3, 1 } },
	{ 0x06000133, { 4, 2 } },
	{ 0x06000134, { 6, 2 } },
	{ 0x06000135, { 8, 2 } },
	{ 0x060001A3, { 10, 6 } },
	{ 0x060001A7, { 16, 3 } },
	{ 0x06000239, { 20, 3 } },
	{ 0x0600023A, { 23, 1 } },
	{ 0x0600024D, { 24, 1 } },
	{ 0x060002D1, { 25, 2 } },
	{ 0x06000494, { 27, 3 } },
	{ 0x06000499, { 30, 2 } },
	{ 0x06000545, { 32, 4 } },
	{ 0x06000548, { 36, 1 } },
	{ 0x06000556, { 49, 4 } },
	{ 0x0600055C, { 53, 1 } },
	{ 0x06000562, { 63, 4 } },
	{ 0x06000563, { 67, 1 } },
	{ 0x06000566, { 78, 4 } },
	{ 0x0600056E, { 82, 1 } },
	{ 0x06000571, { 92, 1 } },
	{ 0x06000579, { 93, 4 } },
	{ 0x0600057C, { 97, 1 } },
	{ 0x0600057F, { 107, 1 } },
	{ 0x06000582, { 108, 4 } },
	{ 0x06000584, { 112, 1 } },
	{ 0x06000587, { 122, 1 } },
	{ 0x0600058D, { 123, 4 } },
	{ 0x06000591, { 127, 1 } },
	{ 0x0600059C, { 147, 4 } },
	{ 0x060005AE, { 169, 1 } },
	{ 0x060005CB, { 170, 4 } },
	{ 0x060005CF, { 174, 1 } },
	{ 0x060005E1, { 186, 1 } },
	{ 0x060005E3, { 187, 1 } },
	{ 0x060005E4, { 188, 1 } },
	{ 0x060005EB, { 189, 4 } },
	{ 0x060005F1, { 193, 1 } },
	{ 0x060005F4, { 203, 4 } },
	{ 0x060005F9, { 207, 1 } },
	{ 0x060007A0, { 237, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[282] = 
{
	{ (Il2CppRGCTXDataType)1, 247 },
	{ (Il2CppRGCTXDataType)2, 247 },
	{ (Il2CppRGCTXDataType)3, 30504 },
	{ (Il2CppRGCTXDataType)2, 180 },
	{ (Il2CppRGCTXDataType)1, 3 },
	{ (Il2CppRGCTXDataType)3, 26473 },
	{ (Il2CppRGCTXDataType)3, 26477 },
	{ (Il2CppRGCTXDataType)1, 4 },
	{ (Il2CppRGCTXDataType)1, 2 },
	{ (Il2CppRGCTXDataType)2, 2 },
	{ (Il2CppRGCTXDataType)2, 1499 },
	{ (Il2CppRGCTXDataType)3, 327 },
	{ (Il2CppRGCTXDataType)2, 1501 },
	{ (Il2CppRGCTXDataType)3, 329 },
	{ (Il2CppRGCTXDataType)2, 2740 },
	{ (Il2CppRGCTXDataType)3, 330 },
	{ (Il2CppRGCTXDataType)3, 5059 },
	{ (Il2CppRGCTXDataType)3, 30351 },
	{ (Il2CppRGCTXDataType)3, 30299 },
	{ (Il2CppRGCTXDataType)3, 11341 },
	{ (Il2CppRGCTXDataType)1, 344 },
	{ (Il2CppRGCTXDataType)2, 344 },
	{ (Il2CppRGCTXDataType)3, 31211 },
	{ (Il2CppRGCTXDataType)1, 343 },
	{ (Il2CppRGCTXDataType)3, 31202 },
	{ (Il2CppRGCTXDataType)1, 125 },
	{ (Il2CppRGCTXDataType)2, 125 },
	{ (Il2CppRGCTXDataType)1, 5230 },
	{ (Il2CppRGCTXDataType)2, 5230 },
	{ (Il2CppRGCTXDataType)2, 173 },
	{ (Il2CppRGCTXDataType)3, 11875 },
	{ (Il2CppRGCTXDataType)3, 20669 },
	{ (Il2CppRGCTXDataType)3, 2757 },
	{ (Il2CppRGCTXDataType)2, 1896 },
	{ (Il2CppRGCTXDataType)3, 2756 },
	{ (Il2CppRGCTXDataType)3, 2758 },
	{ (Il2CppRGCTXDataType)3, 31132 },
	{ (Il2CppRGCTXDataType)3, 30493 },
	{ (Il2CppRGCTXDataType)3, 24335 },
	{ (Il2CppRGCTXDataType)3, 4694 },
	{ (Il2CppRGCTXDataType)3, 4794 },
	{ (Il2CppRGCTXDataType)3, 2802 },
	{ (Il2CppRGCTXDataType)3, 4793 },
	{ (Il2CppRGCTXDataType)2, 795 },
	{ (Il2CppRGCTXDataType)3, 2803 },
	{ (Il2CppRGCTXDataType)3, 2804 },
	{ (Il2CppRGCTXDataType)3, 2805 },
	{ (Il2CppRGCTXDataType)2, 560 },
	{ (Il2CppRGCTXDataType)3, 11386 },
	{ (Il2CppRGCTXDataType)3, 2745 },
	{ (Il2CppRGCTXDataType)2, 1892 },
	{ (Il2CppRGCTXDataType)3, 2744 },
	{ (Il2CppRGCTXDataType)3, 2746 },
	{ (Il2CppRGCTXDataType)3, 31127 },
	{ (Il2CppRGCTXDataType)3, 30048 },
	{ (Il2CppRGCTXDataType)3, 24330 },
	{ (Il2CppRGCTXDataType)3, 4690 },
	{ (Il2CppRGCTXDataType)3, 4786 },
	{ (Il2CppRGCTXDataType)3, 2785 },
	{ (Il2CppRGCTXDataType)3, 4785 },
	{ (Il2CppRGCTXDataType)3, 2786 },
	{ (Il2CppRGCTXDataType)3, 2787 },
	{ (Il2CppRGCTXDataType)3, 2788 },
	{ (Il2CppRGCTXDataType)3, 2748 },
	{ (Il2CppRGCTXDataType)2, 1893 },
	{ (Il2CppRGCTXDataType)3, 2747 },
	{ (Il2CppRGCTXDataType)3, 2749 },
	{ (Il2CppRGCTXDataType)3, 31128 },
	{ (Il2CppRGCTXDataType)3, 2790 },
	{ (Il2CppRGCTXDataType)3, 30217 },
	{ (Il2CppRGCTXDataType)3, 24332 },
	{ (Il2CppRGCTXDataType)3, 4691 },
	{ (Il2CppRGCTXDataType)3, 4788 },
	{ (Il2CppRGCTXDataType)3, 2789 },
	{ (Il2CppRGCTXDataType)3, 4787 },
	{ (Il2CppRGCTXDataType)3, 2791 },
	{ (Il2CppRGCTXDataType)3, 2792 },
	{ (Il2CppRGCTXDataType)3, 2793 },
	{ (Il2CppRGCTXDataType)3, 2751 },
	{ (Il2CppRGCTXDataType)2, 1894 },
	{ (Il2CppRGCTXDataType)3, 2750 },
	{ (Il2CppRGCTXDataType)3, 2752 },
	{ (Il2CppRGCTXDataType)3, 31129 },
	{ (Il2CppRGCTXDataType)3, 30287 },
	{ (Il2CppRGCTXDataType)3, 24333 },
	{ (Il2CppRGCTXDataType)3, 4692 },
	{ (Il2CppRGCTXDataType)3, 4790 },
	{ (Il2CppRGCTXDataType)3, 2794 },
	{ (Il2CppRGCTXDataType)3, 4789 },
	{ (Il2CppRGCTXDataType)3, 2795 },
	{ (Il2CppRGCTXDataType)3, 2796 },
	{ (Il2CppRGCTXDataType)3, 2797 },
	{ (Il2CppRGCTXDataType)3, 31130 },
	{ (Il2CppRGCTXDataType)3, 2754 },
	{ (Il2CppRGCTXDataType)2, 1895 },
	{ (Il2CppRGCTXDataType)3, 2753 },
	{ (Il2CppRGCTXDataType)3, 2755 },
	{ (Il2CppRGCTXDataType)3, 31131 },
	{ (Il2CppRGCTXDataType)3, 30490 },
	{ (Il2CppRGCTXDataType)3, 24334 },
	{ (Il2CppRGCTXDataType)3, 4693 },
	{ (Il2CppRGCTXDataType)3, 4792 },
	{ (Il2CppRGCTXDataType)3, 2798 },
	{ (Il2CppRGCTXDataType)3, 4791 },
	{ (Il2CppRGCTXDataType)3, 2799 },
	{ (Il2CppRGCTXDataType)3, 2800 },
	{ (Il2CppRGCTXDataType)3, 2801 },
	{ (Il2CppRGCTXDataType)3, 31133 },
	{ (Il2CppRGCTXDataType)3, 2766 },
	{ (Il2CppRGCTXDataType)2, 1899 },
	{ (Il2CppRGCTXDataType)3, 2765 },
	{ (Il2CppRGCTXDataType)3, 2767 },
	{ (Il2CppRGCTXDataType)3, 31134 },
	{ (Il2CppRGCTXDataType)3, 30979 },
	{ (Il2CppRGCTXDataType)3, 24339 },
	{ (Il2CppRGCTXDataType)3, 4696 },
	{ (Il2CppRGCTXDataType)3, 4798 },
	{ (Il2CppRGCTXDataType)3, 2810 },
	{ (Il2CppRGCTXDataType)3, 4797 },
	{ (Il2CppRGCTXDataType)3, 2811 },
	{ (Il2CppRGCTXDataType)3, 2812 },
	{ (Il2CppRGCTXDataType)3, 2813 },
	{ (Il2CppRGCTXDataType)3, 31136 },
	{ (Il2CppRGCTXDataType)3, 2772 },
	{ (Il2CppRGCTXDataType)2, 1901 },
	{ (Il2CppRGCTXDataType)3, 2771 },
	{ (Il2CppRGCTXDataType)3, 2773 },
	{ (Il2CppRGCTXDataType)3, 31137 },
	{ (Il2CppRGCTXDataType)3, 31434 },
	{ (Il2CppRGCTXDataType)3, 24348 },
	{ (Il2CppRGCTXDataType)3, 4698 },
	{ (Il2CppRGCTXDataType)3, 4802 },
	{ (Il2CppRGCTXDataType)3, 2820 },
	{ (Il2CppRGCTXDataType)3, 4801 },
	{ (Il2CppRGCTXDataType)3, 2821 },
	{ (Il2CppRGCTXDataType)2, 898 },
	{ (Il2CppRGCTXDataType)3, 2822 },
	{ (Il2CppRGCTXDataType)3, 2823 },
	{ (Il2CppRGCTXDataType)3, 2824 },
	{ (Il2CppRGCTXDataType)3, 12816 },
	{ (Il2CppRGCTXDataType)3, 29840 },
	{ (Il2CppRGCTXDataType)2, 4077 },
	{ (Il2CppRGCTXDataType)3, 12815 },
	{ (Il2CppRGCTXDataType)3, 12814 },
	{ (Il2CppRGCTXDataType)2, 3193 },
	{ (Il2CppRGCTXDataType)2, 4074 },
	{ (Il2CppRGCTXDataType)3, 12813 },
	{ (Il2CppRGCTXDataType)3, 2760 },
	{ (Il2CppRGCTXDataType)2, 1897 },
	{ (Il2CppRGCTXDataType)3, 2759 },
	{ (Il2CppRGCTXDataType)3, 2761 },
	{ (Il2CppRGCTXDataType)3, 12819 },
	{ (Il2CppRGCTXDataType)2, 3194 },
	{ (Il2CppRGCTXDataType)3, 12818 },
	{ (Il2CppRGCTXDataType)2, 4081 },
	{ (Il2CppRGCTXDataType)3, 24336 },
	{ (Il2CppRGCTXDataType)3, 4695 },
	{ (Il2CppRGCTXDataType)3, 4796 },
	{ (Il2CppRGCTXDataType)3, 2831 },
	{ (Il2CppRGCTXDataType)3, 4795 },
	{ (Il2CppRGCTXDataType)3, 12820 },
	{ (Il2CppRGCTXDataType)3, 12817 },
	{ (Il2CppRGCTXDataType)3, 2833 },
	{ (Il2CppRGCTXDataType)3, 2832 },
	{ (Il2CppRGCTXDataType)3, 2834 },
	{ (Il2CppRGCTXDataType)3, 2835 },
	{ (Il2CppRGCTXDataType)3, 2836 },
	{ (Il2CppRGCTXDataType)2, 4079 },
	{ (Il2CppRGCTXDataType)3, 29704 },
	{ (Il2CppRGCTXDataType)3, 30775 },
	{ (Il2CppRGCTXDataType)3, 2769 },
	{ (Il2CppRGCTXDataType)2, 1900 },
	{ (Il2CppRGCTXDataType)3, 2768 },
	{ (Il2CppRGCTXDataType)3, 2770 },
	{ (Il2CppRGCTXDataType)3, 31135 },
	{ (Il2CppRGCTXDataType)3, 2816 },
	{ (Il2CppRGCTXDataType)3, 31182 },
	{ (Il2CppRGCTXDataType)3, 24340 },
	{ (Il2CppRGCTXDataType)3, 4697 },
	{ (Il2CppRGCTXDataType)3, 4800 },
	{ (Il2CppRGCTXDataType)3, 2814 },
	{ (Il2CppRGCTXDataType)3, 4799 },
	{ (Il2CppRGCTXDataType)3, 2815 },
	{ (Il2CppRGCTXDataType)3, 2817 },
	{ (Il2CppRGCTXDataType)3, 2818 },
	{ (Il2CppRGCTXDataType)3, 2819 },
	{ (Il2CppRGCTXDataType)3, 30776 },
	{ (Il2CppRGCTXDataType)1, 340 },
	{ (Il2CppRGCTXDataType)1, 341 },
	{ (Il2CppRGCTXDataType)3, 2739 },
	{ (Il2CppRGCTXDataType)2, 1890 },
	{ (Il2CppRGCTXDataType)3, 2738 },
	{ (Il2CppRGCTXDataType)3, 2740 },
	{ (Il2CppRGCTXDataType)3, 31125 },
	{ (Il2CppRGCTXDataType)3, 30023 },
	{ (Il2CppRGCTXDataType)3, 24328 },
	{ (Il2CppRGCTXDataType)3, 4688 },
	{ (Il2CppRGCTXDataType)3, 4782 },
	{ (Il2CppRGCTXDataType)3, 2777 },
	{ (Il2CppRGCTXDataType)3, 4781 },
	{ (Il2CppRGCTXDataType)3, 2778 },
	{ (Il2CppRGCTXDataType)3, 2779 },
	{ (Il2CppRGCTXDataType)3, 2780 },
	{ (Il2CppRGCTXDataType)3, 2742 },
	{ (Il2CppRGCTXDataType)2, 1891 },
	{ (Il2CppRGCTXDataType)3, 2741 },
	{ (Il2CppRGCTXDataType)3, 2743 },
	{ (Il2CppRGCTXDataType)3, 31126 },
	{ (Il2CppRGCTXDataType)3, 30026 },
	{ (Il2CppRGCTXDataType)3, 24329 },
	{ (Il2CppRGCTXDataType)3, 4689 },
	{ (Il2CppRGCTXDataType)3, 4784 },
	{ (Il2CppRGCTXDataType)3, 2781 },
	{ (Il2CppRGCTXDataType)3, 4783 },
	{ (Il2CppRGCTXDataType)3, 2782 },
	{ (Il2CppRGCTXDataType)3, 2783 },
	{ (Il2CppRGCTXDataType)3, 2784 },
	{ (Il2CppRGCTXDataType)3, 15874 },
	{ (Il2CppRGCTXDataType)2, 4594 },
	{ (Il2CppRGCTXDataType)3, 5060 },
	{ (Il2CppRGCTXDataType)2, 2223 },
	{ (Il2CppRGCTXDataType)2, 5470 },
	{ (Il2CppRGCTXDataType)3, 22296 },
	{ (Il2CppRGCTXDataType)3, 3603 },
	{ (Il2CppRGCTXDataType)3, 3604 },
	{ (Il2CppRGCTXDataType)3, 3601 },
	{ (Il2CppRGCTXDataType)3, 22299 },
	{ (Il2CppRGCTXDataType)3, 22298 },
	{ (Il2CppRGCTXDataType)3, 3605 },
	{ (Il2CppRGCTXDataType)3, 3602 },
	{ (Il2CppRGCTXDataType)3, 22297 },
	{ (Il2CppRGCTXDataType)3, 1364 },
	{ (Il2CppRGCTXDataType)3, 19744 },
	{ (Il2CppRGCTXDataType)2, 2235 },
	{ (Il2CppRGCTXDataType)3, 5127 },
	{ (Il2CppRGCTXDataType)2, 5111 },
	{ (Il2CppRGCTXDataType)3, 19749 },
	{ (Il2CppRGCTXDataType)2, 1478 },
	{ (Il2CppRGCTXDataType)3, 146 },
	{ (Il2CppRGCTXDataType)3, 147 },
	{ (Il2CppRGCTXDataType)3, 29975 },
	{ (Il2CppRGCTXDataType)3, 11596 },
	{ (Il2CppRGCTXDataType)3, 24349 },
	{ (Il2CppRGCTXDataType)3, 4699 },
	{ (Il2CppRGCTXDataType)3, 4804 },
	{ (Il2CppRGCTXDataType)3, 29932 },
	{ (Il2CppRGCTXDataType)3, 4803 },
	{ (Il2CppRGCTXDataType)1, 678 },
	{ (Il2CppRGCTXDataType)2, 678 },
	{ (Il2CppRGCTXDataType)2, 1198 },
	{ (Il2CppRGCTXDataType)2, 4262 },
	{ (Il2CppRGCTXDataType)3, 12938 },
	{ (Il2CppRGCTXDataType)2, 1256 },
	{ (Il2CppRGCTXDataType)2, 4268 },
	{ (Il2CppRGCTXDataType)3, 12942 },
	{ (Il2CppRGCTXDataType)2, 4418 },
	{ (Il2CppRGCTXDataType)3, 15290 },
	{ (Il2CppRGCTXDataType)2, 4261 },
	{ (Il2CppRGCTXDataType)3, 12937 },
	{ (Il2CppRGCTXDataType)2, 4267 },
	{ (Il2CppRGCTXDataType)3, 12941 },
	{ (Il2CppRGCTXDataType)2, 4602 },
	{ (Il2CppRGCTXDataType)3, 15923 },
	{ (Il2CppRGCTXDataType)2, 1043 },
	{ (Il2CppRGCTXDataType)2, 4264 },
	{ (Il2CppRGCTXDataType)3, 12940 },
	{ (Il2CppRGCTXDataType)3, 15924 },
	{ (Il2CppRGCTXDataType)2, 1867 },
	{ (Il2CppRGCTXDataType)3, 1924 },
	{ (Il2CppRGCTXDataType)2, 4263 },
	{ (Il2CppRGCTXDataType)3, 12939 },
	{ (Il2CppRGCTXDataType)2, 4494 },
	{ (Il2CppRGCTXDataType)3, 15507 },
	{ (Il2CppRGCTXDataType)2, 1866 },
	{ (Il2CppRGCTXDataType)3, 1922 },
	{ (Il2CppRGCTXDataType)3, 15509 },
	{ (Il2CppRGCTXDataType)3, 15280 },
	{ (Il2CppRGCTXDataType)3, 15281 },
	{ (Il2CppRGCTXDataType)3, 5091 },
	{ (Il2CppRGCTXDataType)2, 2225 },
	{ (Il2CppRGCTXDataType)3, 5090 },
	{ (Il2CppRGCTXDataType)3, 15508 },
};
extern const CustomAttributesCacheGenerator g_AWSSDK_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AWSSDK_Core_CodeGenModule;
const Il2CppCodeGenModule g_AWSSDK_Core_CodeGenModule = 
{
	"AWSSDK.Core.dll",
	2230,
	s_methodPointers,
	40,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	68,
	s_rgctxIndices,
	282,
	s_rgctxValues,
	NULL,
	g_AWSSDK_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
