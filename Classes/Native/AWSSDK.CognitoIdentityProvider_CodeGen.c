﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderConfig::.ctor()
extern void AmazonCognitoIdentityProviderConfig__ctor_m4921B073B88C8BEC0D5CF65594C9121C7D796AA0 (void);
// 0x00000002 System.String Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderConfig::get_RegionEndpointServiceName()
extern void AmazonCognitoIdentityProviderConfig_get_RegionEndpointServiceName_m20D5FA91D7205FDFD3CE67B64EAC65A5C87F84B8 (void);
// 0x00000003 System.String Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderConfig::get_UserAgent()
extern void AmazonCognitoIdentityProviderConfig_get_UserAgent_m36D44C82D052A1314AA00FF2B71BBC6730E591E8 (void);
// 0x00000004 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderConfig::.cctor()
extern void AmazonCognitoIdentityProviderConfig__cctor_mAC848DEB24766BD03169FB3A3E09AF14219AC933 (void);
// 0x00000005 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void AmazonCognitoIdentityProviderException__ctor_mA18F41D16C3FBE741A609E9DEA916CE014593DD1 (void);
// 0x00000006 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderRequest::.ctor()
extern void AmazonCognitoIdentityProviderRequest__ctor_mBB1ECA5859934B1D64E37E666552D1B88DECF59B (void);
// 0x00000007 System.Void Amazon.CognitoIdentityProvider.AuthFlowType::.ctor(System.String)
extern void AuthFlowType__ctor_m375366DD175DB52C93693B2D0666FD6285651331 (void);
// 0x00000008 System.Void Amazon.CognitoIdentityProvider.AuthFlowType::.cctor()
extern void AuthFlowType__cctor_mD67E0B09CA6817ADB978495A7FE6F4B6E315273A (void);
// 0x00000009 System.Void Amazon.CognitoIdentityProvider.ChallengeNameType::.ctor(System.String)
extern void ChallengeNameType__ctor_mC1E2F885818E085076D84A13F3A459562CD32738 (void);
// 0x0000000A Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::FindValue(System.String)
extern void ChallengeNameType_FindValue_m56B70199907CEC45CC46DF03A428B1E0B80DE99B (void);
// 0x0000000B Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::op_Implicit(System.String)
extern void ChallengeNameType_op_Implicit_m7E2793BAD573CB051AFE5D01F0C99FDEDFFC858E (void);
// 0x0000000C System.Void Amazon.CognitoIdentityProvider.ChallengeNameType::.cctor()
extern void ChallengeNameType__cctor_m0A1615B57FCA57A767BB0A185C572117E9911A33 (void);
// 0x0000000D System.Void Amazon.CognitoIdentityProvider.DeliveryMediumType::.ctor(System.String)
extern void DeliveryMediumType__ctor_m45FEF39B126CB2B30AA1398CAC90F075E589CF20 (void);
// 0x0000000E Amazon.CognitoIdentityProvider.DeliveryMediumType Amazon.CognitoIdentityProvider.DeliveryMediumType::FindValue(System.String)
extern void DeliveryMediumType_FindValue_m9AC0087BA0D7B0A64F20B3B01526AE4F208B7A89 (void);
// 0x0000000F Amazon.CognitoIdentityProvider.DeliveryMediumType Amazon.CognitoIdentityProvider.DeliveryMediumType::op_Implicit(System.String)
extern void DeliveryMediumType_op_Implicit_m9159B29BE126608D107AD1B587310C7AEB80B655 (void);
// 0x00000010 System.Void Amazon.CognitoIdentityProvider.DeliveryMediumType::.cctor()
extern void DeliveryMediumType__cctor_m168DC9FAEC47ECDC277CEE7EBE0FB5346357D882 (void);
// 0x00000011 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
extern void AmazonCognitoIdentityProviderClient__ctor_m1247E41543FBEBA97A0AD5463D17EA29F628CDB8 (void);
// 0x00000012 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderConfig)
extern void AmazonCognitoIdentityProviderClient__ctor_mD6356DC84B0E6EA168693ABC018C7234E7AC2D60 (void);
// 0x00000013 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::CreateSigner()
extern void AmazonCognitoIdentityProviderClient_CreateSigner_m2E7E1445CFF26198F260EE5FA58401067F1F7B14 (void);
// 0x00000014 Amazon.Runtime.Internal.IServiceMetadata Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::get_ServiceMetadata()
extern void AmazonCognitoIdentityProviderClient_get_ServiceMetadata_m663FDFFBDE507FBDA74E67E5762AAEF0C85508E9 (void);
// 0x00000015 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::Dispose(System.Boolean)
extern void AmazonCognitoIdentityProviderClient_Dispose_m5D4ED35271139B0088C24758A8235CFF7293D675 (void);
// 0x00000016 System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::ConfirmForgotPasswordAsync(Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_ConfirmForgotPasswordAsync_mACF6C266684B882B46E8E6521C2A3816CB9931E1 (void);
// 0x00000017 System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.ConfirmSignUpResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::ConfirmSignUpAsync(Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_ConfirmSignUpAsync_m5F097E6AF876AD599EC7EFA156283F0ACC41AA8F (void);
// 0x00000018 System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.DeleteUserResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::DeleteUserAsync(Amazon.CognitoIdentityProvider.Model.DeleteUserRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_DeleteUserAsync_mB17BB2E167C19FCE759FDC170A2AAEEAD0437178 (void);
// 0x00000019 System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.ForgotPasswordResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::ForgotPasswordAsync(Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_ForgotPasswordAsync_m120F064DCB5769FF201C001F06AD79C463172E88 (void);
// 0x0000001A System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.GetUserResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::GetUserAsync(Amazon.CognitoIdentityProvider.Model.GetUserRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_GetUserAsync_mB1756AA60CCA93662D3EEDA2DF6CF343F0BB238B (void);
// 0x0000001B System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::InitiateAuthAsync(Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_InitiateAuthAsync_mD876E6B360D3E7734FE69CAC12D5A3A7CB860EF0 (void);
// 0x0000001C System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::ResendConfirmationCodeAsync(Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_ResendConfirmationCodeAsync_mF1EA63D0D331E46CDC2678179EF0878EFAC1595B (void);
// 0x0000001D System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::RespondToAuthChallengeAsync(Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_RespondToAuthChallengeAsync_m6176332B91BF3B41C2FF00D78EEEFA1570664C11 (void);
// 0x0000001E System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.SignUpResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::SignUpAsync(Amazon.CognitoIdentityProvider.Model.SignUpRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_SignUpAsync_m25590E3EB31AA192144594873642A8244A2830AF (void);
// 0x0000001F System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesResponse> Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::UpdateUserAttributesAsync(Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityProviderClient_UpdateUserAttributesAsync_mDFF886D198A2EC07B77915CDD6BDDC616E16D0F5 (void);
// 0x00000020 System.Void Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::.cctor()
extern void AmazonCognitoIdentityProviderClient__cctor_m209B3F451C7A1F51BEBAB0F9043F42DB54D48425 (void);
// 0x00000021 System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse> Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider::InitiateAuthAsync(Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest,System.Threading.CancellationToken)
// 0x00000022 System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse> Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider::RespondToAuthChallengeAsync(Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest,System.Threading.CancellationToken)
// 0x00000023 System.Void Amazon.CognitoIdentityProvider.Model.AliasExistsException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void AliasExistsException__ctor_m6939C70581B8E32E2CF5C6B74B10BB0AA1C62868 (void);
// 0x00000024 System.String Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType::get_AnalyticsEndpointId()
extern void AnalyticsMetadataType_get_AnalyticsEndpointId_mA46DC45C6CC11B0C6EA3E0C045F45F3794699F9A (void);
// 0x00000025 System.Boolean Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType::IsSetAnalyticsEndpointId()
extern void AnalyticsMetadataType_IsSetAnalyticsEndpointId_mD856FA274D8EE5CE9E91D8D43D546B87BA8CA434 (void);
// 0x00000026 System.String Amazon.CognitoIdentityProvider.Model.AttributeType::get_Name()
extern void AttributeType_get_Name_mEFE5983AFD25EBA2CDC0EFB5D860CA1DD127E061 (void);
// 0x00000027 System.Void Amazon.CognitoIdentityProvider.Model.AttributeType::set_Name(System.String)
extern void AttributeType_set_Name_mF6A4C7F2B360BE9EF0816A609BDA7A33BB2964BF (void);
// 0x00000028 System.Boolean Amazon.CognitoIdentityProvider.Model.AttributeType::IsSetName()
extern void AttributeType_IsSetName_m7567B0272BEDF176D2F5E97B209BB6C24689B2E4 (void);
// 0x00000029 System.String Amazon.CognitoIdentityProvider.Model.AttributeType::get_Value()
extern void AttributeType_get_Value_m4B9BA00C035D19CE65CC01B2173902FD6E4F2653 (void);
// 0x0000002A System.Void Amazon.CognitoIdentityProvider.Model.AttributeType::set_Value(System.String)
extern void AttributeType_set_Value_m72D17923311CBF7224CFBD02AC5B4B38C9CC74E8 (void);
// 0x0000002B System.Boolean Amazon.CognitoIdentityProvider.Model.AttributeType::IsSetValue()
extern void AttributeType_IsSetValue_m1F2A3E69D1A8CAD6905D1D832392C08280754CD3 (void);
// 0x0000002C System.Void Amazon.CognitoIdentityProvider.Model.AttributeType::.ctor()
extern void AttributeType__ctor_m027B0B5384D5FAE759DBB93A5BE17F87726037EE (void);
// 0x0000002D System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_AccessToken()
extern void AuthenticationResultType_get_AccessToken_m6D5BAA07E120818EB40BF3DA271698B28C325486 (void);
// 0x0000002E System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::set_AccessToken(System.String)
extern void AuthenticationResultType_set_AccessToken_m8B2CC21D97E454D1DAF1F4CDF9880B1E3181539E (void);
// 0x0000002F System.Int32 Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_ExpiresIn()
extern void AuthenticationResultType_get_ExpiresIn_m9D1E78AB44F6E8D25EB4355381429B6404155BD1 (void);
// 0x00000030 System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::set_ExpiresIn(System.Int32)
extern void AuthenticationResultType_set_ExpiresIn_m6922B7BE0CA2F2C10218FA5BD7DA40C1E7934590 (void);
// 0x00000031 System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_IdToken()
extern void AuthenticationResultType_get_IdToken_mE391F6EF0BF8DF2263ED4629BD49F52E622E8C75 (void);
// 0x00000032 System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::set_IdToken(System.String)
extern void AuthenticationResultType_set_IdToken_mCDF589F745A2C67F9E60E8897D6427502E0FE73D (void);
// 0x00000033 System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::set_NewDeviceMetadata(Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType)
extern void AuthenticationResultType_set_NewDeviceMetadata_m38C482C87DA5FF9099E09B7FE9541E99618D622B (void);
// 0x00000034 System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_RefreshToken()
extern void AuthenticationResultType_get_RefreshToken_m9B9C4F96A0C5A98F7763DA4EAB3499BD6A53E52A (void);
// 0x00000035 System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::set_RefreshToken(System.String)
extern void AuthenticationResultType_set_RefreshToken_mA868514AD00EA4C6331A7142FF6C5AA412F442DB (void);
// 0x00000036 System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::set_TokenType(System.String)
extern void AuthenticationResultType_set_TokenType_mD3F75E87386AB3B2DA8DF4918F158EB7468C9492 (void);
// 0x00000037 System.Void Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::.ctor()
extern void AuthenticationResultType__ctor_mA0B88BF5D3A07D6749A4860B02C63791A33AD989 (void);
// 0x00000038 System.Void Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType::set_AttributeName(System.String)
extern void CodeDeliveryDetailsType_set_AttributeName_mC3C75892D511FF8445652AD2E3DB38CF2560F801 (void);
// 0x00000039 System.Void Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType::set_DeliveryMedium(Amazon.CognitoIdentityProvider.DeliveryMediumType)
extern void CodeDeliveryDetailsType_set_DeliveryMedium_mF2E4C49C786897DC29E85240310F077B30F60CA2 (void);
// 0x0000003A System.Void Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType::set_Destination(System.String)
extern void CodeDeliveryDetailsType_set_Destination_m5C6604B9CFD6A2E731A85C4724A2910A4FB28070 (void);
// 0x0000003B System.Void Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType::.ctor()
extern void CodeDeliveryDetailsType__ctor_m296FB59D0E6EC6C26FED889509B718B998B8C107 (void);
// 0x0000003C System.Void Amazon.CognitoIdentityProvider.Model.CodeDeliveryFailureException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void CodeDeliveryFailureException__ctor_m00CED7E2B9BBCDD137CC202CAEA8CAA4DF485BB2 (void);
// 0x0000003D System.Void Amazon.CognitoIdentityProvider.Model.CodeMismatchException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void CodeMismatchException__ctor_mC0D0A75C760209B0FCA47A17A73436A61421CFC3 (void);
// 0x0000003E Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_AnalyticsMetadata()
extern void ConfirmForgotPasswordRequest_get_AnalyticsMetadata_mC0B2ADB4EDB4A95EDA934937930CDC49910B4FC3 (void);
// 0x0000003F System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetAnalyticsMetadata()
extern void ConfirmForgotPasswordRequest_IsSetAnalyticsMetadata_m4C844D3DC660065BB7231B182E050F8BFA5B5953 (void);
// 0x00000040 System.String Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_ClientId()
extern void ConfirmForgotPasswordRequest_get_ClientId_mF59E0C011B36B55DE42861F2376B5314D5D5C881 (void);
// 0x00000041 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::set_ClientId(System.String)
extern void ConfirmForgotPasswordRequest_set_ClientId_mCE07F23CF74610B923A6C2496A3072EE6004512B (void);
// 0x00000042 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetClientId()
extern void ConfirmForgotPasswordRequest_IsSetClientId_mB7BBDF3FE3320F10498675A1A2DAEF4F9B681C10 (void);
// 0x00000043 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_ClientMetadata()
extern void ConfirmForgotPasswordRequest_get_ClientMetadata_m4D53FE279DE89B4D8329AF21B69C584C63BE6754 (void);
// 0x00000044 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetClientMetadata()
extern void ConfirmForgotPasswordRequest_IsSetClientMetadata_mEDFB23445708E2091BFD5EA23EF159580BCA03E8 (void);
// 0x00000045 System.String Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_ConfirmationCode()
extern void ConfirmForgotPasswordRequest_get_ConfirmationCode_m3954BF128261939AF74C4324E821BB0274C972BB (void);
// 0x00000046 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::set_ConfirmationCode(System.String)
extern void ConfirmForgotPasswordRequest_set_ConfirmationCode_m40D383FC9B53B479528243977F7EB8E540A724D6 (void);
// 0x00000047 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetConfirmationCode()
extern void ConfirmForgotPasswordRequest_IsSetConfirmationCode_m7D19DBC91A264E2DF32F1812F8FAE64FCD5A42F2 (void);
// 0x00000048 System.String Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_Password()
extern void ConfirmForgotPasswordRequest_get_Password_m5F09F8DF004298E12BAEF327C0AF8023C8FA0454 (void);
// 0x00000049 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::set_Password(System.String)
extern void ConfirmForgotPasswordRequest_set_Password_mF46C73303A63B0B9D4993EBAFA3052DEE63400C8 (void);
// 0x0000004A System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetPassword()
extern void ConfirmForgotPasswordRequest_IsSetPassword_m43C053822E6C74378DDA1036CD620FBA90855547 (void);
// 0x0000004B System.String Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_SecretHash()
extern void ConfirmForgotPasswordRequest_get_SecretHash_m6F209AE1068F49148207BE1C276185A806A7B636 (void);
// 0x0000004C System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetSecretHash()
extern void ConfirmForgotPasswordRequest_IsSetSecretHash_mE01D6E679D1CCF55530EEFB6EDCB59CC1CFF701C (void);
// 0x0000004D Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_UserContextData()
extern void ConfirmForgotPasswordRequest_get_UserContextData_m1164E52B0569C3505241051932D7223E550B26FF (void);
// 0x0000004E System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetUserContextData()
extern void ConfirmForgotPasswordRequest_IsSetUserContextData_m7702412DDBFC9B9ADDE10BF85F97DD5ED48D1E9A (void);
// 0x0000004F System.String Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::get_Username()
extern void ConfirmForgotPasswordRequest_get_Username_mF6D3516F984BA1F6F367D6F5A363D8837F3D225E (void);
// 0x00000050 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::set_Username(System.String)
extern void ConfirmForgotPasswordRequest_set_Username_mAF5FB34173101A4652332211A96DAE259D845F84 (void);
// 0x00000051 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::IsSetUsername()
extern void ConfirmForgotPasswordRequest_IsSetUsername_m6231891A68F80E008575E65391E512B90E94F6F2 (void);
// 0x00000052 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::CreateSigner()
extern void ConfirmForgotPasswordRequest_CreateSigner_mECFA32762B2FFB78AF55A0481A24E14CE177C637 (void);
// 0x00000053 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest::.ctor()
extern void ConfirmForgotPasswordRequest__ctor_m70B4035E4979E3664855AC69077783A9D117BF87 (void);
// 0x00000054 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordResponse::.ctor()
extern void ConfirmForgotPasswordResponse__ctor_m2CD8CEB8D18BA0F49C582951F2B5192F3D4CF20C (void);
// 0x00000055 Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_AnalyticsMetadata()
extern void ConfirmSignUpRequest_get_AnalyticsMetadata_m8D5BE60E28EC30723F7A84ED36302DCFC04F7010 (void);
// 0x00000056 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetAnalyticsMetadata()
extern void ConfirmSignUpRequest_IsSetAnalyticsMetadata_m3C8AFCD667301811E33AFD0C80B3A0E688F0AF28 (void);
// 0x00000057 System.String Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_ClientId()
extern void ConfirmSignUpRequest_get_ClientId_mFFDFBC5599ABE75F2BE00D4B437BC1A020005357 (void);
// 0x00000058 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::set_ClientId(System.String)
extern void ConfirmSignUpRequest_set_ClientId_m92D9FC3DF80782B086E7C6312BE827FA34E17FE7 (void);
// 0x00000059 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetClientId()
extern void ConfirmSignUpRequest_IsSetClientId_m39D3AED5F70C39CD00B9A817DF2E206B692CE491 (void);
// 0x0000005A System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_ClientMetadata()
extern void ConfirmSignUpRequest_get_ClientMetadata_mD2F66D469E4D793B53DA3837AD8FDFB62EA5A389 (void);
// 0x0000005B System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetClientMetadata()
extern void ConfirmSignUpRequest_IsSetClientMetadata_mCA83EA686D4CF5CF8643AEEE96DAA08AC6A56D9A (void);
// 0x0000005C System.String Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_ConfirmationCode()
extern void ConfirmSignUpRequest_get_ConfirmationCode_m3BB2965266FC8B08DA8006ACC311FDBF60D877FE (void);
// 0x0000005D System.Void Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::set_ConfirmationCode(System.String)
extern void ConfirmSignUpRequest_set_ConfirmationCode_m3726D6E87CE236A8C1A4A99078E3F82783F47326 (void);
// 0x0000005E System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetConfirmationCode()
extern void ConfirmSignUpRequest_IsSetConfirmationCode_m4F2939025A21E2A9A1B41EBEA8489DA0734E3BBF (void);
// 0x0000005F System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_ForceAliasCreation()
extern void ConfirmSignUpRequest_get_ForceAliasCreation_m308CC477D25685DFB09377ACA419C792B1683F31 (void);
// 0x00000060 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetForceAliasCreation()
extern void ConfirmSignUpRequest_IsSetForceAliasCreation_m2AE743205A4B2A496726BB5B28790CD183479123 (void);
// 0x00000061 System.String Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_SecretHash()
extern void ConfirmSignUpRequest_get_SecretHash_m75E11506EBB250CA509357E0339C7E9B1B5EEF3B (void);
// 0x00000062 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetSecretHash()
extern void ConfirmSignUpRequest_IsSetSecretHash_m0DA2DC1924D70C67B90778855D0E6DF222473CE9 (void);
// 0x00000063 Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_UserContextData()
extern void ConfirmSignUpRequest_get_UserContextData_m3962F63D71B677ADC844415DB93EF0F511C5330A (void);
// 0x00000064 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetUserContextData()
extern void ConfirmSignUpRequest_IsSetUserContextData_m4863B2F90F157DFFCFE2A4727A95B1CFB6095283 (void);
// 0x00000065 System.String Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::get_Username()
extern void ConfirmSignUpRequest_get_Username_m84BA687C5E59E6BD0ADFFAEF80B435466C557AE2 (void);
// 0x00000066 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::set_Username(System.String)
extern void ConfirmSignUpRequest_set_Username_mFC3EDA77071127E72D1E59889BEF9C45A88853DF (void);
// 0x00000067 System.Boolean Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::IsSetUsername()
extern void ConfirmSignUpRequest_IsSetUsername_m7601D594FB2BD98DF7445429CB0C64F0536B47BF (void);
// 0x00000068 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::CreateSigner()
extern void ConfirmSignUpRequest_CreateSigner_mC4AFAF332BAEC45E07FE63612753D58DC26F36CE (void);
// 0x00000069 System.Void Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest::.ctor()
extern void ConfirmSignUpRequest__ctor_m99AB7BAF60FC29173F7EA5390E5B581CA92BB824 (void);
// 0x0000006A System.Void Amazon.CognitoIdentityProvider.Model.ConfirmSignUpResponse::.ctor()
extern void ConfirmSignUpResponse__ctor_mABE976DA3FC62B506977A58A45487C7199C848B7 (void);
// 0x0000006B System.String Amazon.CognitoIdentityProvider.Model.DeleteUserRequest::get_AccessToken()
extern void DeleteUserRequest_get_AccessToken_m129961148A6C6BF0BE2726001025BC01EF4FAF29 (void);
// 0x0000006C System.Void Amazon.CognitoIdentityProvider.Model.DeleteUserRequest::set_AccessToken(System.String)
extern void DeleteUserRequest_set_AccessToken_m81106B579EB5AF1A716BD2E3160BE7D4C0879C2C (void);
// 0x0000006D System.Boolean Amazon.CognitoIdentityProvider.Model.DeleteUserRequest::IsSetAccessToken()
extern void DeleteUserRequest_IsSetAccessToken_mEBB3895C59D311D59E7C1C0A2FC4B287A5CE84C0 (void);
// 0x0000006E Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.DeleteUserRequest::CreateSigner()
extern void DeleteUserRequest_CreateSigner_mEE0367EBA8445F97DEB7640B0D84958E7A4A448C (void);
// 0x0000006F System.Void Amazon.CognitoIdentityProvider.Model.DeleteUserRequest::.ctor()
extern void DeleteUserRequest__ctor_mAE8FC26428F32B8707F36664642B22B69D32FDA0 (void);
// 0x00000070 System.Void Amazon.CognitoIdentityProvider.Model.DeleteUserResponse::.ctor()
extern void DeleteUserResponse__ctor_m6CAE22C32FCD15C6F343764B0B5996523B6D9DEF (void);
// 0x00000071 System.Void Amazon.CognitoIdentityProvider.Model.ExpiredCodeException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void ExpiredCodeException__ctor_m9BDC5BA324B07BACCAA8AE0D5BA7E092366BD573 (void);
// 0x00000072 Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::get_AnalyticsMetadata()
extern void ForgotPasswordRequest_get_AnalyticsMetadata_m2190D893FDAE3859A41548565DE64BD9534F8D1C (void);
// 0x00000073 System.Boolean Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::IsSetAnalyticsMetadata()
extern void ForgotPasswordRequest_IsSetAnalyticsMetadata_mB3C07FF20A97BB134BC621C9F403F3151D15D87B (void);
// 0x00000074 System.String Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::get_ClientId()
extern void ForgotPasswordRequest_get_ClientId_mD835CA3FCC2FAD2F99B8FCDD80371677900644F9 (void);
// 0x00000075 System.Void Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::set_ClientId(System.String)
extern void ForgotPasswordRequest_set_ClientId_m7A2F9E2D3373AFE9A5022B29BB84ED3C7BCBDAF9 (void);
// 0x00000076 System.Boolean Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::IsSetClientId()
extern void ForgotPasswordRequest_IsSetClientId_m6FE62CFE42E8C439D8D49FB1FC7A15ABAC25080B (void);
// 0x00000077 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::get_ClientMetadata()
extern void ForgotPasswordRequest_get_ClientMetadata_mD2E2F4B37B09E752E80E42E3DC8BB2423E4CCB62 (void);
// 0x00000078 System.Boolean Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::IsSetClientMetadata()
extern void ForgotPasswordRequest_IsSetClientMetadata_mD261D3BFCE7F3CA3F62891F580D058739E02544A (void);
// 0x00000079 System.String Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::get_SecretHash()
extern void ForgotPasswordRequest_get_SecretHash_m74A16072233DA036E4A5993788E0CDDE53D08C42 (void);
// 0x0000007A System.Boolean Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::IsSetSecretHash()
extern void ForgotPasswordRequest_IsSetSecretHash_m6A48D452358B5D91489A76F9538DDB3316DE9FB0 (void);
// 0x0000007B Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::get_UserContextData()
extern void ForgotPasswordRequest_get_UserContextData_mE2000D5BED2D8F26DEF3FFD7065C897B378E4EFE (void);
// 0x0000007C System.Boolean Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::IsSetUserContextData()
extern void ForgotPasswordRequest_IsSetUserContextData_m7C1AF7622D31A61C48F1FB2FCBA563B56612254B (void);
// 0x0000007D System.String Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::get_Username()
extern void ForgotPasswordRequest_get_Username_mEEE1D5334881B2C4013555D7255599ECFF9D75A0 (void);
// 0x0000007E System.Void Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::set_Username(System.String)
extern void ForgotPasswordRequest_set_Username_mB7843A6D612299F3E91D66AF9B5B87613D477711 (void);
// 0x0000007F System.Boolean Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::IsSetUsername()
extern void ForgotPasswordRequest_IsSetUsername_m4910D5039B7FABC10D333BFB7749969471E8D030 (void);
// 0x00000080 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::CreateSigner()
extern void ForgotPasswordRequest_CreateSigner_m4154568E0930D555277A8B18F6B77463EB098B95 (void);
// 0x00000081 System.Void Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest::.ctor()
extern void ForgotPasswordRequest__ctor_mA6FA7BBE75173F4305B5444139C4E31FC51EC19C (void);
// 0x00000082 System.Void Amazon.CognitoIdentityProvider.Model.ForgotPasswordResponse::set_CodeDeliveryDetails(Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType)
extern void ForgotPasswordResponse_set_CodeDeliveryDetails_mE202180101219AFD576D05B22994E4862D3D30D1 (void);
// 0x00000083 System.Void Amazon.CognitoIdentityProvider.Model.ForgotPasswordResponse::.ctor()
extern void ForgotPasswordResponse__ctor_m28BEA4877669209F7DB4AFEA2071CDFE40C6CCE5 (void);
// 0x00000084 System.String Amazon.CognitoIdentityProvider.Model.GetUserRequest::get_AccessToken()
extern void GetUserRequest_get_AccessToken_mF820F3D1A0FBEC42F51EA3556AB9C660D3190F63 (void);
// 0x00000085 System.Void Amazon.CognitoIdentityProvider.Model.GetUserRequest::set_AccessToken(System.String)
extern void GetUserRequest_set_AccessToken_mD43AFC490FCD0B0D1F3B948B98CD153618BE7A59 (void);
// 0x00000086 System.Boolean Amazon.CognitoIdentityProvider.Model.GetUserRequest::IsSetAccessToken()
extern void GetUserRequest_IsSetAccessToken_mED7E473207E09A9DF63C412868A23DBD46A998F5 (void);
// 0x00000087 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.GetUserRequest::CreateSigner()
extern void GetUserRequest_CreateSigner_mE76CDD638C754CC0D505337FA8A0810D9C7E7ABB (void);
// 0x00000088 System.Void Amazon.CognitoIdentityProvider.Model.GetUserRequest::.ctor()
extern void GetUserRequest__ctor_m739E7B92C951841E9DD3D646B01E164C121508FB (void);
// 0x00000089 System.Void Amazon.CognitoIdentityProvider.Model.GetUserResponse::set_MFAOptions(System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.MFAOptionType>)
extern void GetUserResponse_set_MFAOptions_mA2777B15E093D0A7C9D43FAA7A8B99142DC4CE6A (void);
// 0x0000008A System.Void Amazon.CognitoIdentityProvider.Model.GetUserResponse::set_PreferredMfaSetting(System.String)
extern void GetUserResponse_set_PreferredMfaSetting_mD35F9E7189A65324EA90B1F1421F94C3F13F9D44 (void);
// 0x0000008B System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType> Amazon.CognitoIdentityProvider.Model.GetUserResponse::get_UserAttributes()
extern void GetUserResponse_get_UserAttributes_m1FD7121ED3BBD207B7ACD5EF8A319BF7F88302D3 (void);
// 0x0000008C System.Void Amazon.CognitoIdentityProvider.Model.GetUserResponse::set_UserAttributes(System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType>)
extern void GetUserResponse_set_UserAttributes_mD591332A7804F3DC61F6CB7CB6DE7355C63AD1B1 (void);
// 0x0000008D System.Void Amazon.CognitoIdentityProvider.Model.GetUserResponse::set_UserMFASettingList(System.Collections.Generic.List`1<System.String>)
extern void GetUserResponse_set_UserMFASettingList_m0F7569DF58358FD3F7D9D30E638B027AB3E4CDE1 (void);
// 0x0000008E System.Void Amazon.CognitoIdentityProvider.Model.GetUserResponse::set_Username(System.String)
extern void GetUserResponse_set_Username_m28EC674B05DBBB64FCA18008C6F579C2EAC85DEC (void);
// 0x0000008F System.Void Amazon.CognitoIdentityProvider.Model.GetUserResponse::.ctor()
extern void GetUserResponse__ctor_mB774455816E326B4D40CA706D0B8957E679648C7 (void);
// 0x00000090 Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_AnalyticsMetadata()
extern void InitiateAuthRequest_get_AnalyticsMetadata_m6DFD1FCD99B133528C1A9DFCEE7F39373D75F0F4 (void);
// 0x00000091 System.Boolean Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::IsSetAnalyticsMetadata()
extern void InitiateAuthRequest_IsSetAnalyticsMetadata_mF5CBC4527A5FF58E5E6875258F46C28CA2606DA1 (void);
// 0x00000092 Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_AuthFlow()
extern void InitiateAuthRequest_get_AuthFlow_m651D3A105FC751E2D24332E659AFD69455037F22 (void);
// 0x00000093 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::set_AuthFlow(Amazon.CognitoIdentityProvider.AuthFlowType)
extern void InitiateAuthRequest_set_AuthFlow_mD0AB0CD4A6044E83D4C8DB9E520984E4E6148649 (void);
// 0x00000094 System.Boolean Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::IsSetAuthFlow()
extern void InitiateAuthRequest_IsSetAuthFlow_mD63221A3A3B35520DC78773834254047E084CC6C (void);
// 0x00000095 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_AuthParameters()
extern void InitiateAuthRequest_get_AuthParameters_m8EDAAC67BDB894DFFD14574069FF458FE45766FA (void);
// 0x00000096 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::set_AuthParameters(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void InitiateAuthRequest_set_AuthParameters_m156E35FA9567233EE27CE4819499095448504CDF (void);
// 0x00000097 System.Boolean Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::IsSetAuthParameters()
extern void InitiateAuthRequest_IsSetAuthParameters_m77DCEDED7B3304AEF84A6ADAB9EE7C9BCA72C640 (void);
// 0x00000098 System.String Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_ClientId()
extern void InitiateAuthRequest_get_ClientId_m0AEE5901023CB30B93422875AFFB409A4B4BF6AA (void);
// 0x00000099 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::set_ClientId(System.String)
extern void InitiateAuthRequest_set_ClientId_m7AF95FACF7F24BA83DA0AB54F23102925DFF1E74 (void);
// 0x0000009A System.Boolean Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::IsSetClientId()
extern void InitiateAuthRequest_IsSetClientId_m3F1E548E0749B5026311B8EA6F53DAE9281B1D35 (void);
// 0x0000009B System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_ClientMetadata()
extern void InitiateAuthRequest_get_ClientMetadata_m1F2309645DE6EB054367FB7334070DF20816DDDE (void);
// 0x0000009C System.Boolean Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::IsSetClientMetadata()
extern void InitiateAuthRequest_IsSetClientMetadata_m5DB46F51E021C2312205E6BD32B65C65D8E33C51 (void);
// 0x0000009D Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_UserContextData()
extern void InitiateAuthRequest_get_UserContextData_m62C0E8BED33E1EE2B9E5FFEC552035F27E7E3C77 (void);
// 0x0000009E System.Boolean Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::IsSetUserContextData()
extern void InitiateAuthRequest_IsSetUserContextData_m71B0D88E89E2211C075DB111CF34C2105DFAD576 (void);
// 0x0000009F Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::CreateSigner()
extern void InitiateAuthRequest_CreateSigner_m59F8B57CC944A080BD9757F137609790EB185A58 (void);
// 0x000000A0 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::.ctor()
extern void InitiateAuthRequest__ctor_mC7F09D29F08F846B156E36B3B6C2256A164F5618 (void);
// 0x000000A1 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::set_AuthenticationResult(Amazon.CognitoIdentityProvider.Model.AuthenticationResultType)
extern void InitiateAuthResponse_set_AuthenticationResult_mD310039CABE8D1F619A69A9B85F79A93E45B8E5E (void);
// 0x000000A2 Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::get_ChallengeName()
extern void InitiateAuthResponse_get_ChallengeName_m7D4CCE67488E4D35933D39E8B54524B4A5484894 (void);
// 0x000000A3 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::set_ChallengeName(Amazon.CognitoIdentityProvider.ChallengeNameType)
extern void InitiateAuthResponse_set_ChallengeName_m6AEF15A158F1B795E595947D6A39E9157301EFFE (void);
// 0x000000A4 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::get_ChallengeParameters()
extern void InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083 (void);
// 0x000000A5 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::set_ChallengeParameters(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void InitiateAuthResponse_set_ChallengeParameters_m485956DEB9C245E74A8C3B785CD1B62458A0BC46 (void);
// 0x000000A6 System.String Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::get_Session()
extern void InitiateAuthResponse_get_Session_mDCD4361A025B34D001F279629CB1FABD4838B3DC (void);
// 0x000000A7 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::set_Session(System.String)
extern void InitiateAuthResponse_set_Session_mF833F05A560079C893B5A979D23ADF954DB893A8 (void);
// 0x000000A8 System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::.ctor()
extern void InitiateAuthResponse__ctor_m4A926424DE090C9C6578137A08D2903DE375C0D1 (void);
// 0x000000A9 System.Void Amazon.CognitoIdentityProvider.Model.InternalErrorException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InternalErrorException__ctor_m5F50C7AD34535531006083C86DE91DDD3FB81411 (void);
// 0x000000AA System.Void Amazon.CognitoIdentityProvider.Model.InvalidEmailRoleAccessPolicyException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidEmailRoleAccessPolicyException__ctor_m4A2A1ABC4A3E88F43C5513820633C8EC878E11A4 (void);
// 0x000000AB System.Void Amazon.CognitoIdentityProvider.Model.InvalidLambdaResponseException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidLambdaResponseException__ctor_m178258F9DA37EC2F66F604FF231C60AA548686D0 (void);
// 0x000000AC System.Void Amazon.CognitoIdentityProvider.Model.InvalidParameterException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidParameterException__ctor_m1E8C4D9E458F43B14CFF0944429AE055DA98C541 (void);
// 0x000000AD System.Void Amazon.CognitoIdentityProvider.Model.InvalidPasswordException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidPasswordException__ctor_mE50D7545D1B0C2FEF6B7EF375F77F9473E2D0DD7 (void);
// 0x000000AE System.Void Amazon.CognitoIdentityProvider.Model.InvalidSmsRoleAccessPolicyException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidSmsRoleAccessPolicyException__ctor_m24FD3E785C88938EF4CFE3F41092E8308848EEFB (void);
// 0x000000AF System.Void Amazon.CognitoIdentityProvider.Model.InvalidSmsRoleTrustRelationshipException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidSmsRoleTrustRelationshipException__ctor_m0636D728FF121431E250BF14A924C1D22FD2CFA3 (void);
// 0x000000B0 System.Void Amazon.CognitoIdentityProvider.Model.InvalidUserPoolConfigurationException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidUserPoolConfigurationException__ctor_mECB1917C9829F5C7426F42778441DF2F81B376F0 (void);
// 0x000000B1 System.Void Amazon.CognitoIdentityProvider.Model.LimitExceededException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void LimitExceededException__ctor_m7F67D5453DED1BA1DFFDF8E0AAE55F8FA9B55A90 (void);
// 0x000000B2 System.Void Amazon.CognitoIdentityProvider.Model.MFAMethodNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void MFAMethodNotFoundException__ctor_mD11EAB6725C9C40048F779F27CF16FC90EF6C3FD (void);
// 0x000000B3 System.Void Amazon.CognitoIdentityProvider.Model.MFAOptionType::set_AttributeName(System.String)
extern void MFAOptionType_set_AttributeName_mCFB36521CD484ED0668C625C429365914FF3EA9C (void);
// 0x000000B4 System.Void Amazon.CognitoIdentityProvider.Model.MFAOptionType::set_DeliveryMedium(Amazon.CognitoIdentityProvider.DeliveryMediumType)
extern void MFAOptionType_set_DeliveryMedium_m9F57162CB17515411121F5C40AAE3F0D054C6181 (void);
// 0x000000B5 System.Void Amazon.CognitoIdentityProvider.Model.MFAOptionType::.ctor()
extern void MFAOptionType__ctor_m78C9E37EB1A86C6164F4B55F133E9DE9D2A8C567 (void);
// 0x000000B6 System.Void Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType::set_DeviceGroupKey(System.String)
extern void NewDeviceMetadataType_set_DeviceGroupKey_m2D6F962AE896D064C46B9BFD32DA5E36EA7E08BA (void);
// 0x000000B7 System.Void Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType::set_DeviceKey(System.String)
extern void NewDeviceMetadataType_set_DeviceKey_mF105E48B8285CF675A60ADAD351B621D887681C1 (void);
// 0x000000B8 System.Void Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType::.ctor()
extern void NewDeviceMetadataType__ctor_m755159CCA94412866413D61F6AD42D65E90EBAFA (void);
// 0x000000B9 System.Void Amazon.CognitoIdentityProvider.Model.NotAuthorizedException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void NotAuthorizedException__ctor_mD612605E1BC6EB4B9A360AC2CEEEFEDEA143565D (void);
// 0x000000BA System.Void Amazon.CognitoIdentityProvider.Model.PasswordResetRequiredException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void PasswordResetRequiredException__ctor_m9D6F931ADA69FCBC0DE3AFB2B9D940189B8AFC56 (void);
// 0x000000BB Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::get_AnalyticsMetadata()
extern void ResendConfirmationCodeRequest_get_AnalyticsMetadata_m605D361B3B8C8327C844BBEEB6512C7FAFCFAE85 (void);
// 0x000000BC System.Boolean Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::IsSetAnalyticsMetadata()
extern void ResendConfirmationCodeRequest_IsSetAnalyticsMetadata_m9D90088A82FCBF0C69E86374AF633AA7146D486B (void);
// 0x000000BD System.String Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::get_ClientId()
extern void ResendConfirmationCodeRequest_get_ClientId_m4964812B82DBEB379124783F0F9F7FDB8BB67751 (void);
// 0x000000BE System.Void Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::set_ClientId(System.String)
extern void ResendConfirmationCodeRequest_set_ClientId_mE0E265CA6C04EE3A3D5C1F6C4A93562D13AC4011 (void);
// 0x000000BF System.Boolean Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::IsSetClientId()
extern void ResendConfirmationCodeRequest_IsSetClientId_m20DA1C33ED899B80AF73D6698A2B75683C4531CE (void);
// 0x000000C0 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::get_ClientMetadata()
extern void ResendConfirmationCodeRequest_get_ClientMetadata_m364C1B00999770E6E574E0C9B9B59E4618430809 (void);
// 0x000000C1 System.Boolean Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::IsSetClientMetadata()
extern void ResendConfirmationCodeRequest_IsSetClientMetadata_m4D43DFD7EDABE3D5F93729BB1AE6302EC4603A07 (void);
// 0x000000C2 System.String Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::get_SecretHash()
extern void ResendConfirmationCodeRequest_get_SecretHash_mA44031CFF261472D27299AA0342E590BE175C7C1 (void);
// 0x000000C3 System.Boolean Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::IsSetSecretHash()
extern void ResendConfirmationCodeRequest_IsSetSecretHash_m795AAA0D4E522BA567E53A0C48F02A3CB1AC147A (void);
// 0x000000C4 Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::get_UserContextData()
extern void ResendConfirmationCodeRequest_get_UserContextData_mC92ED5BD140FB5BFA566A41A3AF68E4CFC8002D1 (void);
// 0x000000C5 System.Boolean Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::IsSetUserContextData()
extern void ResendConfirmationCodeRequest_IsSetUserContextData_m4F83C73765E0788779299D58F0F25F1AC8A9FDBF (void);
// 0x000000C6 System.String Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::get_Username()
extern void ResendConfirmationCodeRequest_get_Username_mD7915E79C3BCD1BA2D9A5C9492CC758952C6F0CB (void);
// 0x000000C7 System.Void Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::set_Username(System.String)
extern void ResendConfirmationCodeRequest_set_Username_m9589AFD7B6148224EBBA962F02E7EB0B44E79C18 (void);
// 0x000000C8 System.Boolean Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::IsSetUsername()
extern void ResendConfirmationCodeRequest_IsSetUsername_mE5C80174C58F9DA1FEDB24A191E82E752EAD6734 (void);
// 0x000000C9 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::CreateSigner()
extern void ResendConfirmationCodeRequest_CreateSigner_m69C029623B7E6519D0FC48DEF68F09F78A073FF0 (void);
// 0x000000CA System.Void Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest::.ctor()
extern void ResendConfirmationCodeRequest__ctor_m9E709F5354E5389ED686FD2C8757F58C166B754D (void);
// 0x000000CB System.Void Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeResponse::set_CodeDeliveryDetails(Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType)
extern void ResendConfirmationCodeResponse_set_CodeDeliveryDetails_m6D3790009E1FF897E20B477E581D569B0547FB55 (void);
// 0x000000CC System.Void Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeResponse::.ctor()
extern void ResendConfirmationCodeResponse__ctor_mBF16CC4542106288794B3DD4DA68DD193B50716F (void);
// 0x000000CD System.Void Amazon.CognitoIdentityProvider.Model.ResourceNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void ResourceNotFoundException__ctor_m3A83B96D9B79F8EE5DB1DE21052FDCA54A54A020 (void);
// 0x000000CE Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_AnalyticsMetadata()
extern void RespondToAuthChallengeRequest_get_AnalyticsMetadata_m610E55AEF6DDE9EE8E2A30AEA03AD83243902810 (void);
// 0x000000CF System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetAnalyticsMetadata()
extern void RespondToAuthChallengeRequest_IsSetAnalyticsMetadata_mEAE463237C30D324A9EBEAC9DC230A6EC93D29CE (void);
// 0x000000D0 Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_ChallengeName()
extern void RespondToAuthChallengeRequest_get_ChallengeName_m106724388CFD3DC96C820517A8EB6C4AD73A7F1C (void);
// 0x000000D1 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_ChallengeName(Amazon.CognitoIdentityProvider.ChallengeNameType)
extern void RespondToAuthChallengeRequest_set_ChallengeName_mC3307C66F44FF605F9672F763CD0C77DD56ABCD2 (void);
// 0x000000D2 System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetChallengeName()
extern void RespondToAuthChallengeRequest_IsSetChallengeName_mDCBDBBBA26B8D738352F9EF5009923DAC53866D9 (void);
// 0x000000D3 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_ChallengeResponses()
extern void RespondToAuthChallengeRequest_get_ChallengeResponses_mF628D1CC2388ABC64C8AA8CCB942462F1CC347F5 (void);
// 0x000000D4 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_ChallengeResponses(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RespondToAuthChallengeRequest_set_ChallengeResponses_mB198D0AEB28A31F3BC175BC70CF2C82DCBA1BAAA (void);
// 0x000000D5 System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetChallengeResponses()
extern void RespondToAuthChallengeRequest_IsSetChallengeResponses_mA48871FE302E862B882B08B55AF02851966233EB (void);
// 0x000000D6 System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_ClientId()
extern void RespondToAuthChallengeRequest_get_ClientId_mFA9DB06F57FF9EFD332104AAEF42B349B7D2EB09 (void);
// 0x000000D7 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_ClientId(System.String)
extern void RespondToAuthChallengeRequest_set_ClientId_m5CD35765D3E001E538AF5FE4BF61469ADDA1EF71 (void);
// 0x000000D8 System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetClientId()
extern void RespondToAuthChallengeRequest_IsSetClientId_mCA373FA38DD2244A815410FDAF4F85F5A7BE7300 (void);
// 0x000000D9 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_ClientMetadata()
extern void RespondToAuthChallengeRequest_get_ClientMetadata_mD38A818DDFEB885CDC315DA9E25B4967CEC82B8C (void);
// 0x000000DA System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetClientMetadata()
extern void RespondToAuthChallengeRequest_IsSetClientMetadata_mB225E14408518D268E6929AA5EA7456A3EBC32FD (void);
// 0x000000DB System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_Session()
extern void RespondToAuthChallengeRequest_get_Session_m52A785ACFFBD8ABA7B3E84142C62EC327B3C8FAB (void);
// 0x000000DC System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_Session(System.String)
extern void RespondToAuthChallengeRequest_set_Session_m9A001CCF2391CD45F8FCCF2FF12669691744DB4D (void);
// 0x000000DD System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetSession()
extern void RespondToAuthChallengeRequest_IsSetSession_m74A17EDA0A258E7A0DF1A44B1FAE711D4990F0F6 (void);
// 0x000000DE Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_UserContextData()
extern void RespondToAuthChallengeRequest_get_UserContextData_mAA8795725AA3C3518D4AF9F050A33FD2004DB678 (void);
// 0x000000DF System.Boolean Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::IsSetUserContextData()
extern void RespondToAuthChallengeRequest_IsSetUserContextData_mCA79564D0EBA49B81D09A1FFE695AEBC1722B7E8 (void);
// 0x000000E0 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::CreateSigner()
extern void RespondToAuthChallengeRequest_CreateSigner_m46CA61AF5870888FCF728A423DAE30A092FEFBB9 (void);
// 0x000000E1 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::.ctor()
extern void RespondToAuthChallengeRequest__ctor_m96C934A734FB612A3DC464BE3396F64DD157E276 (void);
// 0x000000E2 Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_AuthenticationResult()
extern void RespondToAuthChallengeResponse_get_AuthenticationResult_mEA1392BB5EEE1EAF8C6CA2A4295ADC1CDC6C9FC0 (void);
// 0x000000E3 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::set_AuthenticationResult(Amazon.CognitoIdentityProvider.Model.AuthenticationResultType)
extern void RespondToAuthChallengeResponse_set_AuthenticationResult_mEAB48A6B4EA0BAFBBDBB34D639A5980AA00B6954 (void);
// 0x000000E4 Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_ChallengeName()
extern void RespondToAuthChallengeResponse_get_ChallengeName_m9DDD431CE7D58ABBF4D71D33B9B3DA93D1931475 (void);
// 0x000000E5 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::set_ChallengeName(Amazon.CognitoIdentityProvider.ChallengeNameType)
extern void RespondToAuthChallengeResponse_set_ChallengeName_mCBAC8E4D13C66EF276109F939CB9BF26A03380D2 (void);
// 0x000000E6 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_ChallengeParameters()
extern void RespondToAuthChallengeResponse_get_ChallengeParameters_mDCF21DF93EAE302BE3A1851D6631B5033C6BDB87 (void);
// 0x000000E7 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::set_ChallengeParameters(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RespondToAuthChallengeResponse_set_ChallengeParameters_m0D45D8964EF4A45791BBCCC90A75B2F1105888C8 (void);
// 0x000000E8 System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_Session()
extern void RespondToAuthChallengeResponse_get_Session_mE00CF70381F77B441D8F0D6CAC1E8EBF7A262D69 (void);
// 0x000000E9 System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::set_Session(System.String)
extern void RespondToAuthChallengeResponse_set_Session_m0472C71E0C1B86705074F7069D8D9AB1C6F16E9C (void);
// 0x000000EA System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::.ctor()
extern void RespondToAuthChallengeResponse__ctor_m4568F630BB959913B1FA35875684FC5A71EF0CE6 (void);
// 0x000000EB Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_AnalyticsMetadata()
extern void SignUpRequest_get_AnalyticsMetadata_mFDE53924C3DDC69AED987BE09BED363E8FCA4C08 (void);
// 0x000000EC System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetAnalyticsMetadata()
extern void SignUpRequest_IsSetAnalyticsMetadata_mF626D2687F6B2292B4446BF3FF5D42E7E14269D8 (void);
// 0x000000ED System.String Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_ClientId()
extern void SignUpRequest_get_ClientId_m38E5086D9C708126C2C76FFC24CECD75AD62F0E4 (void);
// 0x000000EE System.Void Amazon.CognitoIdentityProvider.Model.SignUpRequest::set_ClientId(System.String)
extern void SignUpRequest_set_ClientId_m8D3778B21304CBE645B7E5C1FD6328C440E1F572 (void);
// 0x000000EF System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetClientId()
extern void SignUpRequest_IsSetClientId_mDCF7A7656711B38F6D97457EF6F86C73063DAA51 (void);
// 0x000000F0 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_ClientMetadata()
extern void SignUpRequest_get_ClientMetadata_mD5DE92081FB2F80937D381847031AD9BC80B7F15 (void);
// 0x000000F1 System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetClientMetadata()
extern void SignUpRequest_IsSetClientMetadata_mBAD9B62130F1231090BE9B07319610205C23237F (void);
// 0x000000F2 System.String Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_Password()
extern void SignUpRequest_get_Password_mF33CBC2E11ADAB89A5F8677CD7C8922CEC7F28BC (void);
// 0x000000F3 System.Void Amazon.CognitoIdentityProvider.Model.SignUpRequest::set_Password(System.String)
extern void SignUpRequest_set_Password_m690273F4CCE0DB9CD7BB9B2AB50EED5DC1FC8E77 (void);
// 0x000000F4 System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetPassword()
extern void SignUpRequest_IsSetPassword_mA593D1AA1EE31BAF35FF26268EECD663A471F54E (void);
// 0x000000F5 System.String Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_SecretHash()
extern void SignUpRequest_get_SecretHash_m6C7D2FC42F085F3D9F27C28095480311A58E0310 (void);
// 0x000000F6 System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetSecretHash()
extern void SignUpRequest_IsSetSecretHash_mF2DA325380C867B9D668E66DABC378817E875B32 (void);
// 0x000000F7 System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType> Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_UserAttributes()
extern void SignUpRequest_get_UserAttributes_m11EBCEA1A4E9203C4A0E14B23604A66276242313 (void);
// 0x000000F8 System.Void Amazon.CognitoIdentityProvider.Model.SignUpRequest::set_UserAttributes(System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType>)
extern void SignUpRequest_set_UserAttributes_m34CF62D792215A4D38FD222FA8CDABA5F9C096C9 (void);
// 0x000000F9 System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetUserAttributes()
extern void SignUpRequest_IsSetUserAttributes_m06B378FAB402B919BF64FEF3C385934772E35BF7 (void);
// 0x000000FA Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_UserContextData()
extern void SignUpRequest_get_UserContextData_m5906D9EE87A0645DB2592EF655A8EA669E7D8530 (void);
// 0x000000FB System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetUserContextData()
extern void SignUpRequest_IsSetUserContextData_mD1CF945FF52DB151168F84A7FABFA2418CA948F2 (void);
// 0x000000FC System.String Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_Username()
extern void SignUpRequest_get_Username_mCD40DCB5F88CDB17EE7994226B3B3017FDC9A4DC (void);
// 0x000000FD System.Void Amazon.CognitoIdentityProvider.Model.SignUpRequest::set_Username(System.String)
extern void SignUpRequest_set_Username_m3B201A8BE8084BDA9A4E65A4DDD0881A0A98AC55 (void);
// 0x000000FE System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetUsername()
extern void SignUpRequest_IsSetUsername_mF24A6615286E6E90716AC19E9A685BECB0217F00 (void);
// 0x000000FF System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType> Amazon.CognitoIdentityProvider.Model.SignUpRequest::get_ValidationData()
extern void SignUpRequest_get_ValidationData_mB4DCC7EE24D1DB3E7049FD413E52EAC64A3346E7 (void);
// 0x00000100 System.Boolean Amazon.CognitoIdentityProvider.Model.SignUpRequest::IsSetValidationData()
extern void SignUpRequest_IsSetValidationData_m5BA0A95159BF6B2BC4F11D2A511F42A0338B7446 (void);
// 0x00000101 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.SignUpRequest::CreateSigner()
extern void SignUpRequest_CreateSigner_m4E237C14B28AE9354938831E365BC17C5C22A166 (void);
// 0x00000102 System.Void Amazon.CognitoIdentityProvider.Model.SignUpRequest::.ctor()
extern void SignUpRequest__ctor_m69DAC851235EA4C382648EED22C21D413ACFD1BF (void);
// 0x00000103 System.Void Amazon.CognitoIdentityProvider.Model.SignUpResponse::set_CodeDeliveryDetails(Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType)
extern void SignUpResponse_set_CodeDeliveryDetails_m1190DE473CB8B93189EE1D5A1DE9AD315E14AA7B (void);
// 0x00000104 System.Void Amazon.CognitoIdentityProvider.Model.SignUpResponse::set_UserConfirmed(System.Boolean)
extern void SignUpResponse_set_UserConfirmed_mF6C912CEF7FAA57ECC35C2DA4B5DFD4FFB743F92 (void);
// 0x00000105 System.Void Amazon.CognitoIdentityProvider.Model.SignUpResponse::set_UserSub(System.String)
extern void SignUpResponse_set_UserSub_m140FDBD8D5C644C6290A7BA413D71626BAF8C188 (void);
// 0x00000106 System.Void Amazon.CognitoIdentityProvider.Model.SignUpResponse::.ctor()
extern void SignUpResponse__ctor_m026A6F2E5FEBAAD0C30FB5765D745B8413C7F1E7 (void);
// 0x00000107 System.Void Amazon.CognitoIdentityProvider.Model.SoftwareTokenMFANotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void SoftwareTokenMFANotFoundException__ctor_m4D696C800E29286B689F040C179468802C0CABE9 (void);
// 0x00000108 System.Void Amazon.CognitoIdentityProvider.Model.TooManyFailedAttemptsException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void TooManyFailedAttemptsException__ctor_m1C21A11259B42C6BC5C0AE2CD895AD39485E9154 (void);
// 0x00000109 System.Void Amazon.CognitoIdentityProvider.Model.TooManyRequestsException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void TooManyRequestsException__ctor_m4E061C4F8174B859C59F080CCB55E31290664119 (void);
// 0x0000010A System.Void Amazon.CognitoIdentityProvider.Model.UnexpectedLambdaException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void UnexpectedLambdaException__ctor_m1C19A026D8391E366BDFC2B6E2E1C9626E55BDF1 (void);
// 0x0000010B System.String Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::get_AccessToken()
extern void UpdateUserAttributesRequest_get_AccessToken_m44327AE0992690CCC60411575928C98DCA7D470F (void);
// 0x0000010C System.Void Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::set_AccessToken(System.String)
extern void UpdateUserAttributesRequest_set_AccessToken_m2EEDD3E530D83AAB366533A9D94639F826B50DF7 (void);
// 0x0000010D System.Boolean Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::IsSetAccessToken()
extern void UpdateUserAttributesRequest_IsSetAccessToken_mBF7FE63F9CB150909A9CA2A1E429D7F2B7227D5C (void);
// 0x0000010E System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::get_ClientMetadata()
extern void UpdateUserAttributesRequest_get_ClientMetadata_m82FE7629C4D10670482BBA1F37F2FEF7FE54E84E (void);
// 0x0000010F System.Boolean Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::IsSetClientMetadata()
extern void UpdateUserAttributesRequest_IsSetClientMetadata_m1BA519FBC90FD05842E33E0206F6D09208E808C7 (void);
// 0x00000110 System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType> Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::get_UserAttributes()
extern void UpdateUserAttributesRequest_get_UserAttributes_mA05B158A6880F99C314B5EE96574D533E39FAE26 (void);
// 0x00000111 System.Void Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::set_UserAttributes(System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.AttributeType>)
extern void UpdateUserAttributesRequest_set_UserAttributes_mCF9C2328D7872428332D48440484CB4B42B5BCDC (void);
// 0x00000112 System.Boolean Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::IsSetUserAttributes()
extern void UpdateUserAttributesRequest_IsSetUserAttributes_m1BEFB283723D8BC8F1D73B93660158B58AA50DB0 (void);
// 0x00000113 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::CreateSigner()
extern void UpdateUserAttributesRequest_CreateSigner_m6D48459D0F85D836CDFD69ABCEE6146B48082E2E (void);
// 0x00000114 System.Void Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest::.ctor()
extern void UpdateUserAttributesRequest__ctor_mE6BBEE3D1C02E1C937E2D64BC20B480B25596B3E (void);
// 0x00000115 System.Void Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesResponse::set_CodeDeliveryDetailsList(System.Collections.Generic.List`1<Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType>)
extern void UpdateUserAttributesResponse_set_CodeDeliveryDetailsList_m9FC77F710C677154D642F498C8D0CE6B38872E4B (void);
// 0x00000116 System.Void Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesResponse::.ctor()
extern void UpdateUserAttributesResponse__ctor_mBC5C04ECB1061DE39321C442A686A20AA087F244 (void);
// 0x00000117 System.String Amazon.CognitoIdentityProvider.Model.UserContextDataType::get_EncodedData()
extern void UserContextDataType_get_EncodedData_mA9146BA037C81F92641B73C34A9DCA6BE897070E (void);
// 0x00000118 System.Boolean Amazon.CognitoIdentityProvider.Model.UserContextDataType::IsSetEncodedData()
extern void UserContextDataType_IsSetEncodedData_mE2D822989CC762CA11DB039BEEAFAB1F1D1DD727 (void);
// 0x00000119 System.Void Amazon.CognitoIdentityProvider.Model.UserLambdaValidationException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void UserLambdaValidationException__ctor_mAB839410A273A9A57D8A9F909ECEBAB33A6292AB (void);
// 0x0000011A System.Void Amazon.CognitoIdentityProvider.Model.UsernameExistsException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void UsernameExistsException__ctor_m6C6924F058BDDE3FD051D662684ADAF450E97D83 (void);
// 0x0000011B System.Void Amazon.CognitoIdentityProvider.Model.UserNotConfirmedException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void UserNotConfirmedException__ctor_mCAAAAD1B4E91FABC54439E76D22179047F8502FE (void);
// 0x0000011C System.Void Amazon.CognitoIdentityProvider.Model.UserNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void UserNotFoundException__ctor_m9C8F291A9EF29A33A96B088D9BDD43F8D2901B81 (void);
// 0x0000011D Amazon.CognitoIdentityProvider.Model.AliasExistsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AliasExistsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void AliasExistsExceptionUnmarshaller_Unmarshall_mA550B3805C577FA5D6CEB1AF4A81CAD06A2E7F3B (void);
// 0x0000011E Amazon.CognitoIdentityProvider.Model.AliasExistsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AliasExistsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void AliasExistsExceptionUnmarshaller_Unmarshall_m32CFF31B55E3826C688F1BF7ABB7A90AD2C949C1 (void);
// 0x0000011F Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AliasExistsExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AliasExistsExceptionUnmarshaller::get_Instance()
extern void AliasExistsExceptionUnmarshaller_get_Instance_mB06DD94DA853897876BFA0CA13E41EBA166F0D47 (void);
// 0x00000120 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AliasExistsExceptionUnmarshaller::.ctor()
extern void AliasExistsExceptionUnmarshaller__ctor_m6964BD7E4A2CF685C312EA49C306D94A2ACCD00D (void);
// 0x00000121 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AliasExistsExceptionUnmarshaller::.cctor()
extern void AliasExistsExceptionUnmarshaller__cctor_m643DD00CE9ABA2FEEF6958F0743A0177CAD48A6A (void);
// 0x00000122 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AnalyticsMetadataTypeMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType,Amazon.Runtime.Internal.Transform.JsonMarshallerContext)
extern void AnalyticsMetadataTypeMarshaller_Marshall_mD51F8B1B94729240C1708E261F109CF5EF599BBB (void);
// 0x00000123 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AnalyticsMetadataTypeMarshaller::.ctor()
extern void AnalyticsMetadataTypeMarshaller__ctor_m58C64BFC7D91FE411D67AA642F5958678996B8EA (void);
// 0x00000124 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AnalyticsMetadataTypeMarshaller::.cctor()
extern void AnalyticsMetadataTypeMarshaller__cctor_m0D6D4AD698057ACB43E3EA7DDD18C495793442DE (void);
// 0x00000125 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.AttributeType,Amazon.Runtime.Internal.Transform.JsonMarshallerContext)
extern void AttributeTypeMarshaller_Marshall_mD390B3AFCBFD66F285608B0C378DBCF9CA4D905B (void);
// 0x00000126 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeMarshaller::.ctor()
extern void AttributeTypeMarshaller__ctor_m25C50E1A6DFA0A15ADE9E4C21F75D467AFD5F11D (void);
// 0x00000127 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeMarshaller::.cctor()
extern void AttributeTypeMarshaller__cctor_mF84451B00786E24A58D5A95D097CADD1A7D242A9 (void);
// 0x00000128 Amazon.CognitoIdentityProvider.Model.AttributeType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentityProvider.Model.AttributeType,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void AttributeTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_AttributeTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m0CB4481F8BBB0189B1FBB5F3B8F5588C270BF542 (void);
// 0x00000129 Amazon.CognitoIdentityProvider.Model.AttributeType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void AttributeTypeUnmarshaller_Unmarshall_mB1C0E6C533A1E8C6A5E33A73748F12AB8B85B73B (void);
// 0x0000012A Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeUnmarshaller::get_Instance()
extern void AttributeTypeUnmarshaller_get_Instance_m18E421AF719663DA5ACBF47E36E82B68DA31A10F (void);
// 0x0000012B System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeUnmarshaller::.ctor()
extern void AttributeTypeUnmarshaller__ctor_m52CDD451ED7E3FC287017C20EBD0E4B55742FA49 (void);
// 0x0000012C System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AttributeTypeUnmarshaller::.cctor()
extern void AttributeTypeUnmarshaller__cctor_m4E830510327AB5C9F8F9A39E8AD3B267EDC2B64A (void);
// 0x0000012D Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AuthenticationResultTypeUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void AuthenticationResultTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_AuthenticationResultTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mE1045A88BB6A032709033217A47F88A583179D17 (void);
// 0x0000012E Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AuthenticationResultTypeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void AuthenticationResultTypeUnmarshaller_Unmarshall_m2B43899B8C5010DD43972BFBDC4E2C6AF0C4ED93 (void);
// 0x0000012F Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AuthenticationResultTypeUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AuthenticationResultTypeUnmarshaller::get_Instance()
extern void AuthenticationResultTypeUnmarshaller_get_Instance_m0910035B0302D287CC9E3E65B17AC269E3640D82 (void);
// 0x00000130 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AuthenticationResultTypeUnmarshaller::.ctor()
extern void AuthenticationResultTypeUnmarshaller__ctor_m382466A9CD197A35FC4F4C2B8E9EC8A1D18CF320 (void);
// 0x00000131 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.AuthenticationResultTypeUnmarshaller::.cctor()
extern void AuthenticationResultTypeUnmarshaller__cctor_mF74AA94CF7DEB8F63B048CE895B8886E0E4C4D72 (void);
// 0x00000132 Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryDetailsTypeUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void CodeDeliveryDetailsTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_CodeDeliveryDetailsTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m2FCEEFB6A6D293AE881C02C904DA9F976F798DDF (void);
// 0x00000133 Amazon.CognitoIdentityProvider.Model.CodeDeliveryDetailsType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryDetailsTypeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void CodeDeliveryDetailsTypeUnmarshaller_Unmarshall_mB750B9771380F3B03B1F33859DC1CB31DF6ED168 (void);
// 0x00000134 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryDetailsTypeUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryDetailsTypeUnmarshaller::get_Instance()
extern void CodeDeliveryDetailsTypeUnmarshaller_get_Instance_m2FCC02E3DFAB30932F6A690E7877B3964A17DE02 (void);
// 0x00000135 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryDetailsTypeUnmarshaller::.ctor()
extern void CodeDeliveryDetailsTypeUnmarshaller__ctor_mC370447E4E29D3413879A6945D263B268BEB1AB6 (void);
// 0x00000136 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryDetailsTypeUnmarshaller::.cctor()
extern void CodeDeliveryDetailsTypeUnmarshaller__cctor_mE1487388CCBD736A5EAFBBE5072BB84B0D1FB988 (void);
// 0x00000137 Amazon.CognitoIdentityProvider.Model.CodeDeliveryFailureException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryFailureExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void CodeDeliveryFailureExceptionUnmarshaller_Unmarshall_m615A6E8F568EB25093C00822F92322DC115DF2A3 (void);
// 0x00000138 Amazon.CognitoIdentityProvider.Model.CodeDeliveryFailureException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryFailureExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void CodeDeliveryFailureExceptionUnmarshaller_Unmarshall_m78DBE18E63AB3302BC0C67D166F556670A175874 (void);
// 0x00000139 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryFailureExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryFailureExceptionUnmarshaller::get_Instance()
extern void CodeDeliveryFailureExceptionUnmarshaller_get_Instance_m16F13759AF1DEF5237D878AC1BB4E0B6235086E2 (void);
// 0x0000013A System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryFailureExceptionUnmarshaller::.ctor()
extern void CodeDeliveryFailureExceptionUnmarshaller__ctor_m7A3EAC7BE70E2611B321469412AC1C320003A3E8 (void);
// 0x0000013B System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeDeliveryFailureExceptionUnmarshaller::.cctor()
extern void CodeDeliveryFailureExceptionUnmarshaller__cctor_mB8EB1D351DA24FEACCEFBB79471F011BAB46A4FA (void);
// 0x0000013C Amazon.CognitoIdentityProvider.Model.CodeMismatchException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeMismatchExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void CodeMismatchExceptionUnmarshaller_Unmarshall_m8A87C66CB17055A25255C8C0E5D8ACAB13AAEC3A (void);
// 0x0000013D Amazon.CognitoIdentityProvider.Model.CodeMismatchException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeMismatchExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void CodeMismatchExceptionUnmarshaller_Unmarshall_mAABA7C067C7D89EDEE6168943B23A7C3FC7B7D09 (void);
// 0x0000013E Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeMismatchExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeMismatchExceptionUnmarshaller::get_Instance()
extern void CodeMismatchExceptionUnmarshaller_get_Instance_m3C78B7E30FB1E96EBDE21ACC44E1347B315E3ED8 (void);
// 0x0000013F System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeMismatchExceptionUnmarshaller::.ctor()
extern void CodeMismatchExceptionUnmarshaller__ctor_m400794C85F5954DA64D1A1860F60E1F38DD8AB89 (void);
// 0x00000140 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.CodeMismatchExceptionUnmarshaller::.cctor()
extern void CodeMismatchExceptionUnmarshaller__cctor_m77C77030B28A9D01B3282EA91D6DB0109CFDCD14 (void);
// 0x00000141 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void ConfirmForgotPasswordRequestMarshaller_Marshall_m2B71CB2E264E3E181DB84F0A99A27C271C4F39EF (void);
// 0x00000142 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.ConfirmForgotPasswordRequest)
extern void ConfirmForgotPasswordRequestMarshaller_Marshall_m6C870B3ADA2285509FC854C62B5074FCBAC5779D (void);
// 0x00000143 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordRequestMarshaller::get_Instance()
extern void ConfirmForgotPasswordRequestMarshaller_get_Instance_mCD5363BF0CFDB6B569E79E96FA0BD19DB2FB4A42 (void);
// 0x00000144 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordRequestMarshaller::.ctor()
extern void ConfirmForgotPasswordRequestMarshaller__ctor_m2C61042C89321D6B361C39C1519B091707AE7B50 (void);
// 0x00000145 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordRequestMarshaller::.cctor()
extern void ConfirmForgotPasswordRequestMarshaller__cctor_m395A5C1FAD328F33FF08927796F4D101B77C6075 (void);
// 0x00000146 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ConfirmForgotPasswordResponseUnmarshaller_Unmarshall_m8DAD8533C2E31AF904E6667ECA3F97F292415206 (void);
// 0x00000147 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void ConfirmForgotPasswordResponseUnmarshaller_UnmarshallException_m4580CC2855CC960839355BF0D9849FCC4DC7FE73 (void);
// 0x00000148 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordResponseUnmarshaller::get_Instance()
extern void ConfirmForgotPasswordResponseUnmarshaller_get_Instance_m5FD1E2358ABAA78C6B23C653CB3E016DD5D843EA (void);
// 0x00000149 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordResponseUnmarshaller::.ctor()
extern void ConfirmForgotPasswordResponseUnmarshaller__ctor_m3270DBF7DDD976697311B346BC7D9047821230F0 (void);
// 0x0000014A System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmForgotPasswordResponseUnmarshaller::.cctor()
extern void ConfirmForgotPasswordResponseUnmarshaller__cctor_m6986E425C7D4B6503B69CFDC40F1F55F621C8677 (void);
// 0x0000014B Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void ConfirmSignUpRequestMarshaller_Marshall_mD956C835320745D342A1C4C12C0F2704F2259DF2 (void);
// 0x0000014C Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.ConfirmSignUpRequest)
extern void ConfirmSignUpRequestMarshaller_Marshall_mB71AAFE4FC451241A92A4CFD1A27B99DC368CF7B (void);
// 0x0000014D Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpRequestMarshaller::get_Instance()
extern void ConfirmSignUpRequestMarshaller_get_Instance_mC907BE7D67D1A158FF50E50C2AC55230ED9A6ECC (void);
// 0x0000014E System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpRequestMarshaller::.ctor()
extern void ConfirmSignUpRequestMarshaller__ctor_m6B1B3DEEBB94B360A22F9A011901811C38D2FD1D (void);
// 0x0000014F System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpRequestMarshaller::.cctor()
extern void ConfirmSignUpRequestMarshaller__cctor_m455E46D62DA246D9A4D01DD20445300385ACB8CD (void);
// 0x00000150 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ConfirmSignUpResponseUnmarshaller_Unmarshall_mE533E105BD1B36C76090A0D6FEB12ACBEC1E75FE (void);
// 0x00000151 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void ConfirmSignUpResponseUnmarshaller_UnmarshallException_mB7912422987ADA8469EA0CBFF16AA42DFE82C23E (void);
// 0x00000152 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpResponseUnmarshaller::get_Instance()
extern void ConfirmSignUpResponseUnmarshaller_get_Instance_mDBEF64097B651EC4DAD39A126BF896D286FEA58D (void);
// 0x00000153 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpResponseUnmarshaller::.ctor()
extern void ConfirmSignUpResponseUnmarshaller__ctor_m15FC0BAE8E902DCFBF47D4F1CA5DCCC26CD42FE2 (void);
// 0x00000154 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ConfirmSignUpResponseUnmarshaller::.cctor()
extern void ConfirmSignUpResponseUnmarshaller__cctor_m249E0387FBCEC40A88BCE430E71D61D20CBCA661 (void);
// 0x00000155 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void DeleteUserRequestMarshaller_Marshall_m9159B4AF57C4854AA087234DA8A900DBE73EE732 (void);
// 0x00000156 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.DeleteUserRequest)
extern void DeleteUserRequestMarshaller_Marshall_m9BED3FE76CD75332E9C1C28CF8948D0CBBC1BF0B (void);
// 0x00000157 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserRequestMarshaller::get_Instance()
extern void DeleteUserRequestMarshaller_get_Instance_m3E3A9CFB75E9CF676D64169294EAFB6826A6905D (void);
// 0x00000158 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserRequestMarshaller::.ctor()
extern void DeleteUserRequestMarshaller__ctor_mE535F025FE3CE845FBA34DB3B7716D0CC8E5B22D (void);
// 0x00000159 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserRequestMarshaller::.cctor()
extern void DeleteUserRequestMarshaller__cctor_m3F162A7DCC2942A66B90BE6ACFD087E3EAD00604 (void);
// 0x0000015A Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void DeleteUserResponseUnmarshaller_Unmarshall_m2CDFA5FE67D6F6CD5DA8B73BDEDB09E9BE240FF7 (void);
// 0x0000015B Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void DeleteUserResponseUnmarshaller_UnmarshallException_m06CCDE5AC5B57F93242B78CFCE71275227ECF454 (void);
// 0x0000015C Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserResponseUnmarshaller::get_Instance()
extern void DeleteUserResponseUnmarshaller_get_Instance_mE5FD1B7FED231DB3421FEE4B084CDBD35EF9E079 (void);
// 0x0000015D System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserResponseUnmarshaller::.ctor()
extern void DeleteUserResponseUnmarshaller__ctor_m90C8C80DF218EFCED5F6645A77561B32ED5A0446 (void);
// 0x0000015E System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.DeleteUserResponseUnmarshaller::.cctor()
extern void DeleteUserResponseUnmarshaller__cctor_mE5485579F9D08D40494EE1ADA2AF629483276DC8 (void);
// 0x0000015F Amazon.CognitoIdentityProvider.Model.ExpiredCodeException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ExpiredCodeExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ExpiredCodeExceptionUnmarshaller_Unmarshall_mC7483D2093143FCAF6DA11D1909B225D932C5D85 (void);
// 0x00000160 Amazon.CognitoIdentityProvider.Model.ExpiredCodeException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ExpiredCodeExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void ExpiredCodeExceptionUnmarshaller_Unmarshall_mE3A5832F58F3A4B01D680ADEA62299F9E418472E (void);
// 0x00000161 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ExpiredCodeExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ExpiredCodeExceptionUnmarshaller::get_Instance()
extern void ExpiredCodeExceptionUnmarshaller_get_Instance_m605BD07FF522043A4E4F808E7214A87908D3AEC5 (void);
// 0x00000162 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ExpiredCodeExceptionUnmarshaller::.ctor()
extern void ExpiredCodeExceptionUnmarshaller__ctor_m90C7A32D37A7F78CC057997DC0E85C8B541AB282 (void);
// 0x00000163 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ExpiredCodeExceptionUnmarshaller::.cctor()
extern void ExpiredCodeExceptionUnmarshaller__cctor_mD3F499A7295D50682CA823C5AD68D69DBDE412B8 (void);
// 0x00000164 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void ForgotPasswordRequestMarshaller_Marshall_mC5664F311D31E860CCEF01C2315D1B4904FB5246 (void);
// 0x00000165 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.ForgotPasswordRequest)
extern void ForgotPasswordRequestMarshaller_Marshall_m07466C4D41B543756F8683B2B69160C34DC0DC59 (void);
// 0x00000166 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordRequestMarshaller::get_Instance()
extern void ForgotPasswordRequestMarshaller_get_Instance_m1DB0D9AB3E3290C394F24BED2DD28FFC60894822 (void);
// 0x00000167 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordRequestMarshaller::.ctor()
extern void ForgotPasswordRequestMarshaller__ctor_m89178EBA7AD5E2D49A02C9AC905CA515578A77B8 (void);
// 0x00000168 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordRequestMarshaller::.cctor()
extern void ForgotPasswordRequestMarshaller__cctor_m24B15662604D7A7E6B280C9087557943BF96ED73 (void);
// 0x00000169 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ForgotPasswordResponseUnmarshaller_Unmarshall_m2DDB03297B2FEA2383678ECF0301A7F82FEB03F2 (void);
// 0x0000016A Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void ForgotPasswordResponseUnmarshaller_UnmarshallException_m5DD681FF0FF2A3C9F0B24929C62911E37C681D5C (void);
// 0x0000016B Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordResponseUnmarshaller::get_Instance()
extern void ForgotPasswordResponseUnmarshaller_get_Instance_m9DB41CB505C1DC794A471E6C692E5A655B832894 (void);
// 0x0000016C System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordResponseUnmarshaller::.ctor()
extern void ForgotPasswordResponseUnmarshaller__ctor_mC10DD05315CEF011EA29BF76C6FF746EF4740B34 (void);
// 0x0000016D System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ForgotPasswordResponseUnmarshaller::.cctor()
extern void ForgotPasswordResponseUnmarshaller__cctor_m9401BA0D9B0E0089FE4C32BA751DAA7AC5F3D73B (void);
// 0x0000016E Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void GetUserRequestMarshaller_Marshall_mFBF101EC3F15ADF797616CC404BC4BA2549462F5 (void);
// 0x0000016F Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.GetUserRequest)
extern void GetUserRequestMarshaller_Marshall_m783C179B3FD7992320AE779FC41CA28D51E20787 (void);
// 0x00000170 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserRequestMarshaller::get_Instance()
extern void GetUserRequestMarshaller_get_Instance_m1775D1FB993AA384CB83DD913774B688B50DA062 (void);
// 0x00000171 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserRequestMarshaller::.ctor()
extern void GetUserRequestMarshaller__ctor_m713CDA413424D7D63E0D8356B9AEEA07889D1302 (void);
// 0x00000172 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserRequestMarshaller::.cctor()
extern void GetUserRequestMarshaller__cctor_m360FA41A3948C6D848CE7D5E06F22A3E67D0FAFE (void);
// 0x00000173 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void GetUserResponseUnmarshaller_Unmarshall_m61D4B36FF734FBA16B1270835BC12441BD0096B4 (void);
// 0x00000174 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void GetUserResponseUnmarshaller_UnmarshallException_m910D50AC191F4C2DB4D29D145C610F8B0706106C (void);
// 0x00000175 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserResponseUnmarshaller::get_Instance()
extern void GetUserResponseUnmarshaller_get_Instance_m6BF0ABCFE3E51C8D3ED0A08576931FDB30456051 (void);
// 0x00000176 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserResponseUnmarshaller::.ctor()
extern void GetUserResponseUnmarshaller__ctor_m6116B09D3B14741F5C679F190F7CBD58D375820F (void);
// 0x00000177 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.GetUserResponseUnmarshaller::.cctor()
extern void GetUserResponseUnmarshaller__cctor_mF52634F5923E090E2AE496F01A491FA9C89D3A9A (void);
// 0x00000178 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void InitiateAuthRequestMarshaller_Marshall_mA55D3EA1BE872D6F8614298042B352A5264328BF (void);
// 0x00000179 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest)
extern void InitiateAuthRequestMarshaller_Marshall_mCFCABBFB5B16DD015DB6D14AD5B3DAD52CBB678A (void);
// 0x0000017A Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthRequestMarshaller::get_Instance()
extern void InitiateAuthRequestMarshaller_get_Instance_m6862A3204009CEF5C09EA963382169F622B25B3D (void);
// 0x0000017B System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthRequestMarshaller::.ctor()
extern void InitiateAuthRequestMarshaller__ctor_m739F51539300647D80EDA1436D0237494F262CF7 (void);
// 0x0000017C System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthRequestMarshaller::.cctor()
extern void InitiateAuthRequestMarshaller__cctor_mA69B7464DF6B001E506D5D7B79363627CE92BA50 (void);
// 0x0000017D Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InitiateAuthResponseUnmarshaller_Unmarshall_m2CF7826B087767115610C976EEBAF7027BACA8B9 (void);
// 0x0000017E Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void InitiateAuthResponseUnmarshaller_UnmarshallException_mADC340E929A836AFE4BC900983CCDE36738F1AFF (void);
// 0x0000017F Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthResponseUnmarshaller::get_Instance()
extern void InitiateAuthResponseUnmarshaller_get_Instance_m3DB021027B7AB1D88D46E53B880D17FB88421495 (void);
// 0x00000180 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthResponseUnmarshaller::.ctor()
extern void InitiateAuthResponseUnmarshaller__ctor_m4D25D2AE75DD2AAA2924D5D006251E1C05084C15 (void);
// 0x00000181 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InitiateAuthResponseUnmarshaller::.cctor()
extern void InitiateAuthResponseUnmarshaller__cctor_m8A8E9A604CED8A69805DF7DF3ED7D597E169CF95 (void);
// 0x00000182 Amazon.CognitoIdentityProvider.Model.InternalErrorException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InternalErrorExceptionUnmarshaller_Unmarshall_mAD3C4AB0052FFEDE3991EB8E6BF86BB95788F888 (void);
// 0x00000183 Amazon.CognitoIdentityProvider.Model.InternalErrorException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InternalErrorExceptionUnmarshaller_Unmarshall_m6C720E688F8A6430818854C4BD9438071DAA4FFD (void);
// 0x00000184 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::get_Instance()
extern void InternalErrorExceptionUnmarshaller_get_Instance_m4A99DD9E2AAE5BF7229EC7EE87FB7DAED91E20A4 (void);
// 0x00000185 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::.ctor()
extern void InternalErrorExceptionUnmarshaller__ctor_mD353123768EA76658F4A1EC2A7ABE7A79FF6528E (void);
// 0x00000186 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::.cctor()
extern void InternalErrorExceptionUnmarshaller__cctor_m739BE7929FB5EBCC7C846552C774FA48C4FB7D7E (void);
// 0x00000187 Amazon.CognitoIdentityProvider.Model.InvalidEmailRoleAccessPolicyException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidEmailRoleAccessPolicyExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidEmailRoleAccessPolicyExceptionUnmarshaller_Unmarshall_mBC7BD1FC2F66C5EAF9BCBA84BD7D939908B6684D (void);
// 0x00000188 Amazon.CognitoIdentityProvider.Model.InvalidEmailRoleAccessPolicyException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidEmailRoleAccessPolicyExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidEmailRoleAccessPolicyExceptionUnmarshaller_Unmarshall_m1D13B63B8FB3444C695C97ADE5B29306996B4045 (void);
// 0x00000189 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidEmailRoleAccessPolicyExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidEmailRoleAccessPolicyExceptionUnmarshaller::get_Instance()
extern void InvalidEmailRoleAccessPolicyExceptionUnmarshaller_get_Instance_m9BC774D998258BA7C1CDD07EB233838811F286C0 (void);
// 0x0000018A System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidEmailRoleAccessPolicyExceptionUnmarshaller::.ctor()
extern void InvalidEmailRoleAccessPolicyExceptionUnmarshaller__ctor_m744443DA89CBFDB60E3C44F893C7926723FFAA5C (void);
// 0x0000018B System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidEmailRoleAccessPolicyExceptionUnmarshaller::.cctor()
extern void InvalidEmailRoleAccessPolicyExceptionUnmarshaller__cctor_mE19AB9AB2FC4D0A13DF9171D3CE967480C7A3A5B (void);
// 0x0000018C Amazon.CognitoIdentityProvider.Model.InvalidLambdaResponseException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidLambdaResponseExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidLambdaResponseExceptionUnmarshaller_Unmarshall_m061D5E6C0682B877D988F8A2BD97D4643E3FF072 (void);
// 0x0000018D Amazon.CognitoIdentityProvider.Model.InvalidLambdaResponseException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidLambdaResponseExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidLambdaResponseExceptionUnmarshaller_Unmarshall_mDAA25B57B1608355505A0803979D373BDEF8E464 (void);
// 0x0000018E Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidLambdaResponseExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidLambdaResponseExceptionUnmarshaller::get_Instance()
extern void InvalidLambdaResponseExceptionUnmarshaller_get_Instance_m85971D16EA6E5F2DFE038AB99D13855CDF4BF3DA (void);
// 0x0000018F System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidLambdaResponseExceptionUnmarshaller::.ctor()
extern void InvalidLambdaResponseExceptionUnmarshaller__ctor_m3209E47C83262E372D0F0204C47EE3843313FF53 (void);
// 0x00000190 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidLambdaResponseExceptionUnmarshaller::.cctor()
extern void InvalidLambdaResponseExceptionUnmarshaller__cctor_mF4F076A834584E639D8A7EC3011222C37104AE96 (void);
// 0x00000191 Amazon.CognitoIdentityProvider.Model.InvalidParameterException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidParameterExceptionUnmarshaller_Unmarshall_m973C06878C7844576F33DED71A37682D56D676C8 (void);
// 0x00000192 Amazon.CognitoIdentityProvider.Model.InvalidParameterException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidParameterExceptionUnmarshaller_Unmarshall_m66CBAF9097D15F9144AF003E082466516D3EBDF7 (void);
// 0x00000193 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::get_Instance()
extern void InvalidParameterExceptionUnmarshaller_get_Instance_m007700C4DFACF9C7CF723954AE4E7EFB657D32F5 (void);
// 0x00000194 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::.ctor()
extern void InvalidParameterExceptionUnmarshaller__ctor_mB97B964E4A247A5AEA43C806B91106B50213B29D (void);
// 0x00000195 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::.cctor()
extern void InvalidParameterExceptionUnmarshaller__cctor_m3F68EE1625C44B5B0828A3CEC50DDBF345685E3A (void);
// 0x00000196 Amazon.CognitoIdentityProvider.Model.InvalidPasswordException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidPasswordExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidPasswordExceptionUnmarshaller_Unmarshall_m70EBD3C5E135C46F391FF4C0979361A0C50EC01A (void);
// 0x00000197 Amazon.CognitoIdentityProvider.Model.InvalidPasswordException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidPasswordExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidPasswordExceptionUnmarshaller_Unmarshall_m7A7CCB9AA123BD53A0EB8C5FC32E865DC82E1E4F (void);
// 0x00000198 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidPasswordExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidPasswordExceptionUnmarshaller::get_Instance()
extern void InvalidPasswordExceptionUnmarshaller_get_Instance_mA4F2693B34E0D741F816CD84F438DDF941712992 (void);
// 0x00000199 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidPasswordExceptionUnmarshaller::.ctor()
extern void InvalidPasswordExceptionUnmarshaller__ctor_m040BC92093DB9CF314778624E00C6D5534C5A792 (void);
// 0x0000019A System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidPasswordExceptionUnmarshaller::.cctor()
extern void InvalidPasswordExceptionUnmarshaller__cctor_mF63A0071C56EE07EA1EBE9E886A3D0F858A9DEB6 (void);
// 0x0000019B Amazon.CognitoIdentityProvider.Model.InvalidSmsRoleAccessPolicyException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleAccessPolicyExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidSmsRoleAccessPolicyExceptionUnmarshaller_Unmarshall_m3A40209863D9B8766BA538775D664FBE4B97B610 (void);
// 0x0000019C Amazon.CognitoIdentityProvider.Model.InvalidSmsRoleAccessPolicyException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleAccessPolicyExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidSmsRoleAccessPolicyExceptionUnmarshaller_Unmarshall_m69852F9DE7B846DB6A2FEE0B1827303BCAA41276 (void);
// 0x0000019D Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleAccessPolicyExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleAccessPolicyExceptionUnmarshaller::get_Instance()
extern void InvalidSmsRoleAccessPolicyExceptionUnmarshaller_get_Instance_m17ABA64B94AEDF0344E29DF1A1EFD4124173AA9D (void);
// 0x0000019E System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleAccessPolicyExceptionUnmarshaller::.ctor()
extern void InvalidSmsRoleAccessPolicyExceptionUnmarshaller__ctor_mA86D275B86E477740D47FA9EB78B06DE9DC8E430 (void);
// 0x0000019F System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleAccessPolicyExceptionUnmarshaller::.cctor()
extern void InvalidSmsRoleAccessPolicyExceptionUnmarshaller__cctor_m9EFA2588E8F590BC187285CCFFCBD5F3DAAA159B (void);
// 0x000001A0 Amazon.CognitoIdentityProvider.Model.InvalidSmsRoleTrustRelationshipException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleTrustRelationshipExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidSmsRoleTrustRelationshipExceptionUnmarshaller_Unmarshall_m215FBFE501E1B66580B06DD5B39DC36960A88010 (void);
// 0x000001A1 Amazon.CognitoIdentityProvider.Model.InvalidSmsRoleTrustRelationshipException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleTrustRelationshipExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidSmsRoleTrustRelationshipExceptionUnmarshaller_Unmarshall_mFA456803D2238442137DC83166572E2DC75F09CC (void);
// 0x000001A2 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleTrustRelationshipExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleTrustRelationshipExceptionUnmarshaller::get_Instance()
extern void InvalidSmsRoleTrustRelationshipExceptionUnmarshaller_get_Instance_m160B098973CD353A4E859E3FE5A39740D19A5520 (void);
// 0x000001A3 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleTrustRelationshipExceptionUnmarshaller::.ctor()
extern void InvalidSmsRoleTrustRelationshipExceptionUnmarshaller__ctor_m04AA1C0CB57A9A5CEF6160E482D5977E0D942A23 (void);
// 0x000001A4 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidSmsRoleTrustRelationshipExceptionUnmarshaller::.cctor()
extern void InvalidSmsRoleTrustRelationshipExceptionUnmarshaller__cctor_mC024084616A02B7C1FBD6EF3CC8C2AE14014C19F (void);
// 0x000001A5 Amazon.CognitoIdentityProvider.Model.InvalidUserPoolConfigurationException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidUserPoolConfigurationExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidUserPoolConfigurationExceptionUnmarshaller_Unmarshall_m9346FAE4D6C0AB62D9FCA552719B7326D36F3594 (void);
// 0x000001A6 Amazon.CognitoIdentityProvider.Model.InvalidUserPoolConfigurationException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidUserPoolConfigurationExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidUserPoolConfigurationExceptionUnmarshaller_Unmarshall_mBD72968228375879D3EE84334AFF30D2D1E8D0E1 (void);
// 0x000001A7 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidUserPoolConfigurationExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidUserPoolConfigurationExceptionUnmarshaller::get_Instance()
extern void InvalidUserPoolConfigurationExceptionUnmarshaller_get_Instance_mEF1439F07F68896F5D03C659C4845DEAAC0832F6 (void);
// 0x000001A8 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidUserPoolConfigurationExceptionUnmarshaller::.ctor()
extern void InvalidUserPoolConfigurationExceptionUnmarshaller__ctor_mA484FA3FDBB13BCD1B745F02A5754DB2E9517CD1 (void);
// 0x000001A9 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.InvalidUserPoolConfigurationExceptionUnmarshaller::.cctor()
extern void InvalidUserPoolConfigurationExceptionUnmarshaller__cctor_m90D2323E92E5F3FA57400DFD3D163E295847DEBB (void);
// 0x000001AA Amazon.CognitoIdentityProvider.Model.LimitExceededException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void LimitExceededExceptionUnmarshaller_Unmarshall_m6E49C614D9DA37638A3B13A7BED694F9467D267D (void);
// 0x000001AB Amazon.CognitoIdentityProvider.Model.LimitExceededException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void LimitExceededExceptionUnmarshaller_Unmarshall_mF0C2682436B10CC21FF4FC674437E12E003C40CE (void);
// 0x000001AC Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::get_Instance()
extern void LimitExceededExceptionUnmarshaller_get_Instance_mFD8D97DEFB2F4082548BBAECD887D2045ACB8F3F (void);
// 0x000001AD System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::.ctor()
extern void LimitExceededExceptionUnmarshaller__ctor_m8D11D904E2E2BA4E41E0B07FFFC831C56D501A9C (void);
// 0x000001AE System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::.cctor()
extern void LimitExceededExceptionUnmarshaller__cctor_mF292FA22998B13832A8F54145CD0CB04B49F29C1 (void);
// 0x000001AF Amazon.CognitoIdentityProvider.Model.MFAMethodNotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAMethodNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void MFAMethodNotFoundExceptionUnmarshaller_Unmarshall_mF5CD1A63B47B450A0935D5BB70ED837A63B15BC4 (void);
// 0x000001B0 Amazon.CognitoIdentityProvider.Model.MFAMethodNotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAMethodNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void MFAMethodNotFoundExceptionUnmarshaller_Unmarshall_mED4D3B8C19388ED97209B9A20BCB60AA1F2F4D66 (void);
// 0x000001B1 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAMethodNotFoundExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAMethodNotFoundExceptionUnmarshaller::get_Instance()
extern void MFAMethodNotFoundExceptionUnmarshaller_get_Instance_m5D77547A83B20913B0F12566082A34C4B6D3E41E (void);
// 0x000001B2 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAMethodNotFoundExceptionUnmarshaller::.ctor()
extern void MFAMethodNotFoundExceptionUnmarshaller__ctor_m2C3A88BD6190E384C459EB299AC19012028EF976 (void);
// 0x000001B3 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAMethodNotFoundExceptionUnmarshaller::.cctor()
extern void MFAMethodNotFoundExceptionUnmarshaller__cctor_m0663FB2CFD4087C8C006D64D03B530E3AB578832 (void);
// 0x000001B4 Amazon.CognitoIdentityProvider.Model.MFAOptionType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAOptionTypeUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentityProvider.Model.MFAOptionType,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void MFAOptionTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_MFAOptionTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m1F46208CA6563DB06C3FDCD927ECE185C62310AA (void);
// 0x000001B5 Amazon.CognitoIdentityProvider.Model.MFAOptionType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAOptionTypeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void MFAOptionTypeUnmarshaller_Unmarshall_m6E4884AAD0A1B582EC2D708C0A93E91346BA76DF (void);
// 0x000001B6 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAOptionTypeUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAOptionTypeUnmarshaller::get_Instance()
extern void MFAOptionTypeUnmarshaller_get_Instance_m54FD3855028D7BD6A20BEDA161033EA288E3B569 (void);
// 0x000001B7 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAOptionTypeUnmarshaller::.ctor()
extern void MFAOptionTypeUnmarshaller__ctor_m9553855CC5A32D6EAA249A045CD9E624FB7B2701 (void);
// 0x000001B8 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.MFAOptionTypeUnmarshaller::.cctor()
extern void MFAOptionTypeUnmarshaller__cctor_m8C1A498BD0145EA3D8BFBE7E9872857152A84722 (void);
// 0x000001B9 Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NewDeviceMetadataTypeUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void NewDeviceMetadataTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_NewDeviceMetadataTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mA3E8101A5340AE5A23C1BFFFAFBCEBF8836AFBB2 (void);
// 0x000001BA Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NewDeviceMetadataTypeUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void NewDeviceMetadataTypeUnmarshaller_Unmarshall_m601A2D49F40A323FF06C43CE27E3225523BBF57B (void);
// 0x000001BB Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NewDeviceMetadataTypeUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NewDeviceMetadataTypeUnmarshaller::get_Instance()
extern void NewDeviceMetadataTypeUnmarshaller_get_Instance_mEDCCD6F0F9900D421EFAE46A7BA8D6E61F1380B8 (void);
// 0x000001BC System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NewDeviceMetadataTypeUnmarshaller::.ctor()
extern void NewDeviceMetadataTypeUnmarshaller__ctor_m4B2F5764F04AB25895A7F03D8E8C28F6E04E1CE8 (void);
// 0x000001BD System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NewDeviceMetadataTypeUnmarshaller::.cctor()
extern void NewDeviceMetadataTypeUnmarshaller__cctor_m24D6C3A04FBBCD61D3A1490A8C68E023C1E82649 (void);
// 0x000001BE Amazon.CognitoIdentityProvider.Model.NotAuthorizedException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void NotAuthorizedExceptionUnmarshaller_Unmarshall_mB7479CC6F942707FBFB1CB4A0BAD1F4F039B3A9D (void);
// 0x000001BF Amazon.CognitoIdentityProvider.Model.NotAuthorizedException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void NotAuthorizedExceptionUnmarshaller_Unmarshall_mB6B390657DF755648F166EC6ADEB082C1F57B3D2 (void);
// 0x000001C0 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::get_Instance()
extern void NotAuthorizedExceptionUnmarshaller_get_Instance_m1A39FD14DFCB04135EEC14CAA1E7CE049181F4E6 (void);
// 0x000001C1 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::.ctor()
extern void NotAuthorizedExceptionUnmarshaller__ctor_m910253B83F6E1DDDDDA5C6A145B78584E4B25452 (void);
// 0x000001C2 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::.cctor()
extern void NotAuthorizedExceptionUnmarshaller__cctor_mB4DF0CF621560DDA03C54C88FB1F2D8A82064D1A (void);
// 0x000001C3 Amazon.CognitoIdentityProvider.Model.PasswordResetRequiredException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.PasswordResetRequiredExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void PasswordResetRequiredExceptionUnmarshaller_Unmarshall_m21BCE7DFA9727861D47EEBF781A809CB45CAB39B (void);
// 0x000001C4 Amazon.CognitoIdentityProvider.Model.PasswordResetRequiredException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.PasswordResetRequiredExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void PasswordResetRequiredExceptionUnmarshaller_Unmarshall_m3FA6AE9A7F8FF5DD240B677786A22585BD187771 (void);
// 0x000001C5 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.PasswordResetRequiredExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.PasswordResetRequiredExceptionUnmarshaller::get_Instance()
extern void PasswordResetRequiredExceptionUnmarshaller_get_Instance_m3E7C6D7187651B970D4E48308AD5CD80C1813B88 (void);
// 0x000001C6 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.PasswordResetRequiredExceptionUnmarshaller::.ctor()
extern void PasswordResetRequiredExceptionUnmarshaller__ctor_m9AE99017E017422F6A6E1E7762EE3F6A15EABDF8 (void);
// 0x000001C7 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.PasswordResetRequiredExceptionUnmarshaller::.cctor()
extern void PasswordResetRequiredExceptionUnmarshaller__cctor_m1D0A8427828A7DDC594D0DA8C69E1BD08DCE0377 (void);
// 0x000001C8 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void ResendConfirmationCodeRequestMarshaller_Marshall_mE988BD8DE672794B9E5D9F8E9838D0F330B44E45 (void);
// 0x000001C9 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.ResendConfirmationCodeRequest)
extern void ResendConfirmationCodeRequestMarshaller_Marshall_m90D4B2294A0499B524826E273613D6DD97710602 (void);
// 0x000001CA Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeRequestMarshaller::get_Instance()
extern void ResendConfirmationCodeRequestMarshaller_get_Instance_m2146B7FDC911F70EF478F8CC81228B95E79351A0 (void);
// 0x000001CB System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeRequestMarshaller::.ctor()
extern void ResendConfirmationCodeRequestMarshaller__ctor_m9184557D58B359EC59F7C9287BA5D4DA31E475BF (void);
// 0x000001CC System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeRequestMarshaller::.cctor()
extern void ResendConfirmationCodeRequestMarshaller__cctor_m6B0CA64AB2A26E56D412EF7DA9072FDA18127EEF (void);
// 0x000001CD Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ResendConfirmationCodeResponseUnmarshaller_Unmarshall_mC7D13C57D8535488B08B88AF8BC86E7CC15F32F1 (void);
// 0x000001CE Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void ResendConfirmationCodeResponseUnmarshaller_UnmarshallException_m8E8399256A25005B5E772EFB3F8DD1FE47AF8F7C (void);
// 0x000001CF Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeResponseUnmarshaller::get_Instance()
extern void ResendConfirmationCodeResponseUnmarshaller_get_Instance_mFE72134EE807487BFB86ED9A0EA627D286C6020B (void);
// 0x000001D0 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeResponseUnmarshaller::.ctor()
extern void ResendConfirmationCodeResponseUnmarshaller__ctor_m66A601EF35772C6C7CD2C6125730C7FA5440EAAF (void);
// 0x000001D1 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResendConfirmationCodeResponseUnmarshaller::.cctor()
extern void ResendConfirmationCodeResponseUnmarshaller__cctor_m9A815A98183E8B7C7BAEB2AB8D50F6DDE442E531 (void);
// 0x000001D2 Amazon.CognitoIdentityProvider.Model.ResourceNotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ResourceNotFoundExceptionUnmarshaller_Unmarshall_m52B0E7FCB10DDCB657B637D8BE923E24186B8F1D (void);
// 0x000001D3 Amazon.CognitoIdentityProvider.Model.ResourceNotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void ResourceNotFoundExceptionUnmarshaller_Unmarshall_m6E0FD237E6C03B1680C68E65DC882256668A90D7 (void);
// 0x000001D4 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::get_Instance()
extern void ResourceNotFoundExceptionUnmarshaller_get_Instance_m388330ABF5C3709C0E9335000E0AC78073D1115B (void);
// 0x000001D5 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.ctor()
extern void ResourceNotFoundExceptionUnmarshaller__ctor_mC7BDBB368CB7D666446D1942FEC706B1036ED4E7 (void);
// 0x000001D6 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.cctor()
extern void ResourceNotFoundExceptionUnmarshaller__cctor_mAC9763144DBB45FF77E0A369907F4F8B12931D4C (void);
// 0x000001D7 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void RespondToAuthChallengeRequestMarshaller_Marshall_m30EA522FCFEBB4654B322785D0826B2A645A625E (void);
// 0x000001D8 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest)
extern void RespondToAuthChallengeRequestMarshaller_Marshall_m994D2AABBF182DBE1D040DA60238FC0B615B47E9 (void);
// 0x000001D9 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeRequestMarshaller::get_Instance()
extern void RespondToAuthChallengeRequestMarshaller_get_Instance_m89F9A5F95DC5B86867FDC21762000E21F18F9DBE (void);
// 0x000001DA System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeRequestMarshaller::.ctor()
extern void RespondToAuthChallengeRequestMarshaller__ctor_mB02DE9140ED492DF65F6007FA1059412A703291A (void);
// 0x000001DB System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeRequestMarshaller::.cctor()
extern void RespondToAuthChallengeRequestMarshaller__cctor_mC15C60CA4AD93D1121AAAC23F52F2D358DD4710A (void);
// 0x000001DC Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void RespondToAuthChallengeResponseUnmarshaller_Unmarshall_m11941F2BC27E98FC4C4F961E47461F9EF08CD9D1 (void);
// 0x000001DD Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void RespondToAuthChallengeResponseUnmarshaller_UnmarshallException_m30EE0A104E00B0E2C808B2419DE74AEAB7EF705B (void);
// 0x000001DE Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeResponseUnmarshaller::get_Instance()
extern void RespondToAuthChallengeResponseUnmarshaller_get_Instance_mDAB8DB4724A9635F0F9D6A8C36689EF990AF7D0F (void);
// 0x000001DF System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeResponseUnmarshaller::.ctor()
extern void RespondToAuthChallengeResponseUnmarshaller__ctor_mC2AED72087DFBEC5068CED3DD5510E5373F5DFA4 (void);
// 0x000001E0 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.RespondToAuthChallengeResponseUnmarshaller::.cctor()
extern void RespondToAuthChallengeResponseUnmarshaller__cctor_m26E089E1223745BEC5446F92A7605462B78A0AD0 (void);
// 0x000001E1 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void SignUpRequestMarshaller_Marshall_m97C213502AD4D619CF65F005CA2E33E9AF48D748 (void);
// 0x000001E2 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.SignUpRequest)
extern void SignUpRequestMarshaller_Marshall_mC175F2E337B122EA16361FB201522F39A75B4EB4 (void);
// 0x000001E3 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpRequestMarshaller::get_Instance()
extern void SignUpRequestMarshaller_get_Instance_m728639ACDE5AD31615D3BA16A848493B780702E7 (void);
// 0x000001E4 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpRequestMarshaller::.ctor()
extern void SignUpRequestMarshaller__ctor_mE14042B97D45F5230FDE04A3E99FE3A0F3768A8B (void);
// 0x000001E5 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpRequestMarshaller::.cctor()
extern void SignUpRequestMarshaller__cctor_mEA09A284DA2CAB91CBDF6D171BF015CBD08AE396 (void);
// 0x000001E6 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void SignUpResponseUnmarshaller_Unmarshall_mDECD040F422E74DE3C4D5336126EF85564454E6A (void);
// 0x000001E7 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void SignUpResponseUnmarshaller_UnmarshallException_m1B2824F4BC1E0FEAD80C611B4D43621AF261575B (void);
// 0x000001E8 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpResponseUnmarshaller::get_Instance()
extern void SignUpResponseUnmarshaller_get_Instance_mB0E11FC26E679CFB26394536DC32722F6D3A16CB (void);
// 0x000001E9 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpResponseUnmarshaller::.ctor()
extern void SignUpResponseUnmarshaller__ctor_m792BF25A94C8D4E1F21945DB2BF1B66B7B285729 (void);
// 0x000001EA System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SignUpResponseUnmarshaller::.cctor()
extern void SignUpResponseUnmarshaller__cctor_m0F89FBC75CA7DDAB6096BD1D5A2B9B12C2115673 (void);
// 0x000001EB Amazon.CognitoIdentityProvider.Model.SoftwareTokenMFANotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SoftwareTokenMFANotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void SoftwareTokenMFANotFoundExceptionUnmarshaller_Unmarshall_m5ADD9FCDB2F3FAB15D7EEE617EA2A47AF0E4BA67 (void);
// 0x000001EC Amazon.CognitoIdentityProvider.Model.SoftwareTokenMFANotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SoftwareTokenMFANotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void SoftwareTokenMFANotFoundExceptionUnmarshaller_Unmarshall_m011083A03A3E4E06070BFA4AF0F97AA4730B8C0B (void);
// 0x000001ED Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SoftwareTokenMFANotFoundExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SoftwareTokenMFANotFoundExceptionUnmarshaller::get_Instance()
extern void SoftwareTokenMFANotFoundExceptionUnmarshaller_get_Instance_m96E5760BA542759D077D452BE26E9357627D80A5 (void);
// 0x000001EE System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SoftwareTokenMFANotFoundExceptionUnmarshaller::.ctor()
extern void SoftwareTokenMFANotFoundExceptionUnmarshaller__ctor_mC63A7540DC28CAD32C09384C390B5D276B125A47 (void);
// 0x000001EF System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.SoftwareTokenMFANotFoundExceptionUnmarshaller::.cctor()
extern void SoftwareTokenMFANotFoundExceptionUnmarshaller__cctor_mDCAF5BA3CCFBD43C5251BDA9C50C62B119426A65 (void);
// 0x000001F0 Amazon.CognitoIdentityProvider.Model.TooManyFailedAttemptsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyFailedAttemptsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void TooManyFailedAttemptsExceptionUnmarshaller_Unmarshall_mAC38B8B73C725E5EE7657EE0359F30A2E9A20B17 (void);
// 0x000001F1 Amazon.CognitoIdentityProvider.Model.TooManyFailedAttemptsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyFailedAttemptsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void TooManyFailedAttemptsExceptionUnmarshaller_Unmarshall_mCF997C8ED3F1C9EB1F478ECBD8E7309CAFF3509E (void);
// 0x000001F2 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyFailedAttemptsExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyFailedAttemptsExceptionUnmarshaller::get_Instance()
extern void TooManyFailedAttemptsExceptionUnmarshaller_get_Instance_m76E27648999332DCE7ABDC54ADD46280021B0733 (void);
// 0x000001F3 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyFailedAttemptsExceptionUnmarshaller::.ctor()
extern void TooManyFailedAttemptsExceptionUnmarshaller__ctor_mB133C90BAB6CF1D65E615A9AC7FA2547FBCDCB3B (void);
// 0x000001F4 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyFailedAttemptsExceptionUnmarshaller::.cctor()
extern void TooManyFailedAttemptsExceptionUnmarshaller__cctor_mF7A629BF26B1B1C5064C3BF7E1201ECBB2E38A52 (void);
// 0x000001F5 Amazon.CognitoIdentityProvider.Model.TooManyRequestsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void TooManyRequestsExceptionUnmarshaller_Unmarshall_m34F86163537CD2D93D4B4BD286F792C9FBE7A4BF (void);
// 0x000001F6 Amazon.CognitoIdentityProvider.Model.TooManyRequestsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void TooManyRequestsExceptionUnmarshaller_Unmarshall_m21802F42C1E75D3E110B963AC7A1AB36795017A9 (void);
// 0x000001F7 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::get_Instance()
extern void TooManyRequestsExceptionUnmarshaller_get_Instance_m1F06ACE17BE08CC013BD8200C6F5BC539E03BE4F (void);
// 0x000001F8 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::.ctor()
extern void TooManyRequestsExceptionUnmarshaller__ctor_mB12AAFC5529A60ECC35CC18615641D06B7E7E7AC (void);
// 0x000001F9 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::.cctor()
extern void TooManyRequestsExceptionUnmarshaller__cctor_m8F54FB0B086524BA5F4587FF83E30806EA4BAD3A (void);
// 0x000001FA Amazon.CognitoIdentityProvider.Model.UnexpectedLambdaException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UnexpectedLambdaExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void UnexpectedLambdaExceptionUnmarshaller_Unmarshall_mD634C3423A5CB0CA8AE09F7562EBFF5433C5056D (void);
// 0x000001FB Amazon.CognitoIdentityProvider.Model.UnexpectedLambdaException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UnexpectedLambdaExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void UnexpectedLambdaExceptionUnmarshaller_Unmarshall_m1D42A5E61C9B368D7A2C52AD594B8FA3F78C94BB (void);
// 0x000001FC Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UnexpectedLambdaExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UnexpectedLambdaExceptionUnmarshaller::get_Instance()
extern void UnexpectedLambdaExceptionUnmarshaller_get_Instance_mD078C37A8B7F3E8891C58D604A853E07C3A5C14F (void);
// 0x000001FD System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UnexpectedLambdaExceptionUnmarshaller::.ctor()
extern void UnexpectedLambdaExceptionUnmarshaller__ctor_mAC07D48887642AF0B915137AA13E0872868C70CB (void);
// 0x000001FE System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UnexpectedLambdaExceptionUnmarshaller::.cctor()
extern void UnexpectedLambdaExceptionUnmarshaller__cctor_m8BDE9A9A414E70272CA09C190E79F02EE9A5D4DF (void);
// 0x000001FF Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void UpdateUserAttributesRequestMarshaller_Marshall_m1D724BA63710DF81DF59B9A09C9FFF7D554722B2 (void);
// 0x00000200 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesRequestMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.UpdateUserAttributesRequest)
extern void UpdateUserAttributesRequestMarshaller_Marshall_m29C3A00C6C829EDB54128905EB1E43637DF88FA8 (void);
// 0x00000201 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesRequestMarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesRequestMarshaller::get_Instance()
extern void UpdateUserAttributesRequestMarshaller_get_Instance_m1556888BB0CABCAD1C7AD2B726591B67772FD9E4 (void);
// 0x00000202 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesRequestMarshaller::.ctor()
extern void UpdateUserAttributesRequestMarshaller__ctor_m8204FF70485968E8A445CC357F67748030DF5C62 (void);
// 0x00000203 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesRequestMarshaller::.cctor()
extern void UpdateUserAttributesRequestMarshaller__cctor_m3DF09A434157401D4FBECC1E6091D7954B8F046E (void);
// 0x00000204 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void UpdateUserAttributesResponseUnmarshaller_Unmarshall_mA228F34D0D6A00CA1E45ACD60CB0043514FDEC61 (void);
// 0x00000205 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void UpdateUserAttributesResponseUnmarshaller_UnmarshallException_mDA72888FB5A607E789DA921251FD595DA6331A6E (void);
// 0x00000206 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesResponseUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesResponseUnmarshaller::get_Instance()
extern void UpdateUserAttributesResponseUnmarshaller_get_Instance_m19F8954668C58B6D2C2FDAEE4A5A429C185E49EC (void);
// 0x00000207 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesResponseUnmarshaller::.ctor()
extern void UpdateUserAttributesResponseUnmarshaller__ctor_mDC33F34EEBF5C41E48A4401513A8219E4713FE2A (void);
// 0x00000208 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UpdateUserAttributesResponseUnmarshaller::.cctor()
extern void UpdateUserAttributesResponseUnmarshaller__cctor_m4C8C91B45C5253812F81A015ACBA09A085624A62 (void);
// 0x00000209 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserContextDataTypeMarshaller::Marshall(Amazon.CognitoIdentityProvider.Model.UserContextDataType,Amazon.Runtime.Internal.Transform.JsonMarshallerContext)
extern void UserContextDataTypeMarshaller_Marshall_m2729720D9F15BC059EBB0E08A877D69E5B09A637 (void);
// 0x0000020A System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserContextDataTypeMarshaller::.ctor()
extern void UserContextDataTypeMarshaller__ctor_m82DC4C34B65E3D2D8BAE16A5518DC492359423D9 (void);
// 0x0000020B System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserContextDataTypeMarshaller::.cctor()
extern void UserContextDataTypeMarshaller__cctor_mAE387C89F1518F1B5125CDD52968E05D75A05AC2 (void);
// 0x0000020C Amazon.CognitoIdentityProvider.Model.UserLambdaValidationException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserLambdaValidationExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void UserLambdaValidationExceptionUnmarshaller_Unmarshall_m9345CA2D65E0F913D707D947A51421F2A5521103 (void);
// 0x0000020D Amazon.CognitoIdentityProvider.Model.UserLambdaValidationException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserLambdaValidationExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void UserLambdaValidationExceptionUnmarshaller_Unmarshall_m40EB35D159F29A345B8F3D11660D05752D57D219 (void);
// 0x0000020E Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserLambdaValidationExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserLambdaValidationExceptionUnmarshaller::get_Instance()
extern void UserLambdaValidationExceptionUnmarshaller_get_Instance_m7CD7741A77FFA4E5B143BE574E87E1D5234ED574 (void);
// 0x0000020F System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserLambdaValidationExceptionUnmarshaller::.ctor()
extern void UserLambdaValidationExceptionUnmarshaller__ctor_m9FAA02E4D315FA4F0F3B2617078D4616652DBECD (void);
// 0x00000210 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserLambdaValidationExceptionUnmarshaller::.cctor()
extern void UserLambdaValidationExceptionUnmarshaller__cctor_m45B2C4C8135789F48094DBB7C3679CF372430A46 (void);
// 0x00000211 Amazon.CognitoIdentityProvider.Model.UsernameExistsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UsernameExistsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void UsernameExistsExceptionUnmarshaller_Unmarshall_mB9FA01AD28F6BDB2D8523227A5857B6B49011E63 (void);
// 0x00000212 Amazon.CognitoIdentityProvider.Model.UsernameExistsException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UsernameExistsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void UsernameExistsExceptionUnmarshaller_Unmarshall_m4F3B90F3519A024E669D5312DCC73AEF1C2EAC4A (void);
// 0x00000213 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UsernameExistsExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UsernameExistsExceptionUnmarshaller::get_Instance()
extern void UsernameExistsExceptionUnmarshaller_get_Instance_m2FE01427E5658E2B15AACB7CDD47B69D24B93407 (void);
// 0x00000214 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UsernameExistsExceptionUnmarshaller::.ctor()
extern void UsernameExistsExceptionUnmarshaller__ctor_m6D3EA4B51ADA53B9248B01494BF0BFC957090F7F (void);
// 0x00000215 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UsernameExistsExceptionUnmarshaller::.cctor()
extern void UsernameExistsExceptionUnmarshaller__cctor_mB3D27F00947A17E4DFDDED50F33108C13AED52B7 (void);
// 0x00000216 Amazon.CognitoIdentityProvider.Model.UserNotConfirmedException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotConfirmedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void UserNotConfirmedExceptionUnmarshaller_Unmarshall_m938021EFF65C35F62B19C5480654058049A5D85C (void);
// 0x00000217 Amazon.CognitoIdentityProvider.Model.UserNotConfirmedException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotConfirmedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void UserNotConfirmedExceptionUnmarshaller_Unmarshall_m9C7B621F7F7A1A9AF617FE8C4BAABC79ADFC2250 (void);
// 0x00000218 Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotConfirmedExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotConfirmedExceptionUnmarshaller::get_Instance()
extern void UserNotConfirmedExceptionUnmarshaller_get_Instance_mA4C8CA7D57C2C8610A672B21E12D7CBD35E72ED8 (void);
// 0x00000219 System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotConfirmedExceptionUnmarshaller::.ctor()
extern void UserNotConfirmedExceptionUnmarshaller__ctor_mDEEA3C33C9A2F576FEDC1FA74570B93738156701 (void);
// 0x0000021A System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotConfirmedExceptionUnmarshaller::.cctor()
extern void UserNotConfirmedExceptionUnmarshaller__cctor_mF9BD7314096AD723ACA8B3FA2ECED432EF6AA01B (void);
// 0x0000021B Amazon.CognitoIdentityProvider.Model.UserNotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void UserNotFoundExceptionUnmarshaller_Unmarshall_m6E13AC031C0249D3B3C6070448EE908D5E9FC86A (void);
// 0x0000021C Amazon.CognitoIdentityProvider.Model.UserNotFoundException Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void UserNotFoundExceptionUnmarshaller_Unmarshall_mBB768486036051A24123AD68CDB0FFDF25A8ED66 (void);
// 0x0000021D Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotFoundExceptionUnmarshaller Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotFoundExceptionUnmarshaller::get_Instance()
extern void UserNotFoundExceptionUnmarshaller_get_Instance_m8605C518577DFE6FAFAFBA905D874F46349315BB (void);
// 0x0000021E System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotFoundExceptionUnmarshaller::.ctor()
extern void UserNotFoundExceptionUnmarshaller__ctor_m95CB7EE520E00990650AFD2EE13F72B8699CB4FA (void);
// 0x0000021F System.Void Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations.UserNotFoundExceptionUnmarshaller::.cctor()
extern void UserNotFoundExceptionUnmarshaller__cctor_mE75CDED487CC323ACAF6567BAA7D9C6DA0DF3AE4 (void);
// 0x00000220 System.String Amazon.CognitoIdentityProvider.Internal.AmazonCognitoIdentityProviderMetadata::get_ServiceId()
extern void AmazonCognitoIdentityProviderMetadata_get_ServiceId_m4B1C055BA7B46E82ECDA4AFEDEBBB84F8DB8F5AE (void);
// 0x00000221 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Internal.AmazonCognitoIdentityProviderMetadata::get_OperationNameMapping()
extern void AmazonCognitoIdentityProviderMetadata_get_OperationNameMapping_mC34C6DA1F493AB113701D8FC1CB0EF86DE714D23 (void);
// 0x00000222 System.Void Amazon.CognitoIdentityProvider.Internal.AmazonCognitoIdentityProviderMetadata::.ctor()
extern void AmazonCognitoIdentityProviderMetadata__ctor_mE20A3FF26601A3D30B23D36D52D773DF2D14ACBE (void);
static Il2CppMethodPointer s_methodPointers[546] = 
{
	AmazonCognitoIdentityProviderConfig__ctor_m4921B073B88C8BEC0D5CF65594C9121C7D796AA0,
	AmazonCognitoIdentityProviderConfig_get_RegionEndpointServiceName_m20D5FA91D7205FDFD3CE67B64EAC65A5C87F84B8,
	AmazonCognitoIdentityProviderConfig_get_UserAgent_m36D44C82D052A1314AA00FF2B71BBC6730E591E8,
	AmazonCognitoIdentityProviderConfig__cctor_mAC848DEB24766BD03169FB3A3E09AF14219AC933,
	AmazonCognitoIdentityProviderException__ctor_mA18F41D16C3FBE741A609E9DEA916CE014593DD1,
	AmazonCognitoIdentityProviderRequest__ctor_mBB1ECA5859934B1D64E37E666552D1B88DECF59B,
	AuthFlowType__ctor_m375366DD175DB52C93693B2D0666FD6285651331,
	AuthFlowType__cctor_mD67E0B09CA6817ADB978495A7FE6F4B6E315273A,
	ChallengeNameType__ctor_mC1E2F885818E085076D84A13F3A459562CD32738,
	ChallengeNameType_FindValue_m56B70199907CEC45CC46DF03A428B1E0B80DE99B,
	ChallengeNameType_op_Implicit_m7E2793BAD573CB051AFE5D01F0C99FDEDFFC858E,
	ChallengeNameType__cctor_m0A1615B57FCA57A767BB0A185C572117E9911A33,
	DeliveryMediumType__ctor_m45FEF39B126CB2B30AA1398CAC90F075E589CF20,
	DeliveryMediumType_FindValue_m9AC0087BA0D7B0A64F20B3B01526AE4F208B7A89,
	DeliveryMediumType_op_Implicit_m9159B29BE126608D107AD1B587310C7AEB80B655,
	DeliveryMediumType__cctor_m168DC9FAEC47ECDC277CEE7EBE0FB5346357D882,
	AmazonCognitoIdentityProviderClient__ctor_m1247E41543FBEBA97A0AD5463D17EA29F628CDB8,
	AmazonCognitoIdentityProviderClient__ctor_mD6356DC84B0E6EA168693ABC018C7234E7AC2D60,
	AmazonCognitoIdentityProviderClient_CreateSigner_m2E7E1445CFF26198F260EE5FA58401067F1F7B14,
	AmazonCognitoIdentityProviderClient_get_ServiceMetadata_m663FDFFBDE507FBDA74E67E5762AAEF0C85508E9,
	AmazonCognitoIdentityProviderClient_Dispose_m5D4ED35271139B0088C24758A8235CFF7293D675,
	AmazonCognitoIdentityProviderClient_ConfirmForgotPasswordAsync_mACF6C266684B882B46E8E6521C2A3816CB9931E1,
	AmazonCognitoIdentityProviderClient_ConfirmSignUpAsync_m5F097E6AF876AD599EC7EFA156283F0ACC41AA8F,
	AmazonCognitoIdentityProviderClient_DeleteUserAsync_mB17BB2E167C19FCE759FDC170A2AAEEAD0437178,
	AmazonCognitoIdentityProviderClient_ForgotPasswordAsync_m120F064DCB5769FF201C001F06AD79C463172E88,
	AmazonCognitoIdentityProviderClient_GetUserAsync_mB1756AA60CCA93662D3EEDA2DF6CF343F0BB238B,
	AmazonCognitoIdentityProviderClient_InitiateAuthAsync_mD876E6B360D3E7734FE69CAC12D5A3A7CB860EF0,
	AmazonCognitoIdentityProviderClient_ResendConfirmationCodeAsync_mF1EA63D0D331E46CDC2678179EF0878EFAC1595B,
	AmazonCognitoIdentityProviderClient_RespondToAuthChallengeAsync_m6176332B91BF3B41C2FF00D78EEEFA1570664C11,
	AmazonCognitoIdentityProviderClient_SignUpAsync_m25590E3EB31AA192144594873642A8244A2830AF,
	AmazonCognitoIdentityProviderClient_UpdateUserAttributesAsync_mDFF886D198A2EC07B77915CDD6BDDC616E16D0F5,
	AmazonCognitoIdentityProviderClient__cctor_m209B3F451C7A1F51BEBAB0F9043F42DB54D48425,
	NULL,
	NULL,
	AliasExistsException__ctor_m6939C70581B8E32E2CF5C6B74B10BB0AA1C62868,
	AnalyticsMetadataType_get_AnalyticsEndpointId_mA46DC45C6CC11B0C6EA3E0C045F45F3794699F9A,
	AnalyticsMetadataType_IsSetAnalyticsEndpointId_mD856FA274D8EE5CE9E91D8D43D546B87BA8CA434,
	AttributeType_get_Name_mEFE5983AFD25EBA2CDC0EFB5D860CA1DD127E061,
	AttributeType_set_Name_mF6A4C7F2B360BE9EF0816A609BDA7A33BB2964BF,
	AttributeType_IsSetName_m7567B0272BEDF176D2F5E97B209BB6C24689B2E4,
	AttributeType_get_Value_m4B9BA00C035D19CE65CC01B2173902FD6E4F2653,
	AttributeType_set_Value_m72D17923311CBF7224CFBD02AC5B4B38C9CC74E8,
	AttributeType_IsSetValue_m1F2A3E69D1A8CAD6905D1D832392C08280754CD3,
	AttributeType__ctor_m027B0B5384D5FAE759DBB93A5BE17F87726037EE,
	AuthenticationResultType_get_AccessToken_m6D5BAA07E120818EB40BF3DA271698B28C325486,
	AuthenticationResultType_set_AccessToken_m8B2CC21D97E454D1DAF1F4CDF9880B1E3181539E,
	AuthenticationResultType_get_ExpiresIn_m9D1E78AB44F6E8D25EB4355381429B6404155BD1,
	AuthenticationResultType_set_ExpiresIn_m6922B7BE0CA2F2C10218FA5BD7DA40C1E7934590,
	AuthenticationResultType_get_IdToken_mE391F6EF0BF8DF2263ED4629BD49F52E622E8C75,
	AuthenticationResultType_set_IdToken_mCDF589F745A2C67F9E60E8897D6427502E0FE73D,
	AuthenticationResultType_set_NewDeviceMetadata_m38C482C87DA5FF9099E09B7FE9541E99618D622B,
	AuthenticationResultType_get_RefreshToken_m9B9C4F96A0C5A98F7763DA4EAB3499BD6A53E52A,
	AuthenticationResultType_set_RefreshToken_mA868514AD00EA4C6331A7142FF6C5AA412F442DB,
	AuthenticationResultType_set_TokenType_mD3F75E87386AB3B2DA8DF4918F158EB7468C9492,
	AuthenticationResultType__ctor_mA0B88BF5D3A07D6749A4860B02C63791A33AD989,
	CodeDeliveryDetailsType_set_AttributeName_mC3C75892D511FF8445652AD2E3DB38CF2560F801,
	CodeDeliveryDetailsType_set_DeliveryMedium_mF2E4C49C786897DC29E85240310F077B30F60CA2,
	CodeDeliveryDetailsType_set_Destination_m5C6604B9CFD6A2E731A85C4724A2910A4FB28070,
	CodeDeliveryDetailsType__ctor_m296FB59D0E6EC6C26FED889509B718B998B8C107,
	CodeDeliveryFailureException__ctor_m00CED7E2B9BBCDD137CC202CAEA8CAA4DF485BB2,
	CodeMismatchException__ctor_mC0D0A75C760209B0FCA47A17A73436A61421CFC3,
	ConfirmForgotPasswordRequest_get_AnalyticsMetadata_mC0B2ADB4EDB4A95EDA934937930CDC49910B4FC3,
	ConfirmForgotPasswordRequest_IsSetAnalyticsMetadata_m4C844D3DC660065BB7231B182E050F8BFA5B5953,
	ConfirmForgotPasswordRequest_get_ClientId_mF59E0C011B36B55DE42861F2376B5314D5D5C881,
	ConfirmForgotPasswordRequest_set_ClientId_mCE07F23CF74610B923A6C2496A3072EE6004512B,
	ConfirmForgotPasswordRequest_IsSetClientId_mB7BBDF3FE3320F10498675A1A2DAEF4F9B681C10,
	ConfirmForgotPasswordRequest_get_ClientMetadata_m4D53FE279DE89B4D8329AF21B69C584C63BE6754,
	ConfirmForgotPasswordRequest_IsSetClientMetadata_mEDFB23445708E2091BFD5EA23EF159580BCA03E8,
	ConfirmForgotPasswordRequest_get_ConfirmationCode_m3954BF128261939AF74C4324E821BB0274C972BB,
	ConfirmForgotPasswordRequest_set_ConfirmationCode_m40D383FC9B53B479528243977F7EB8E540A724D6,
	ConfirmForgotPasswordRequest_IsSetConfirmationCode_m7D19DBC91A264E2DF32F1812F8FAE64FCD5A42F2,
	ConfirmForgotPasswordRequest_get_Password_m5F09F8DF004298E12BAEF327C0AF8023C8FA0454,
	ConfirmForgotPasswordRequest_set_Password_mF46C73303A63B0B9D4993EBAFA3052DEE63400C8,
	ConfirmForgotPasswordRequest_IsSetPassword_m43C053822E6C74378DDA1036CD620FBA90855547,
	ConfirmForgotPasswordRequest_get_SecretHash_m6F209AE1068F49148207BE1C276185A806A7B636,
	ConfirmForgotPasswordRequest_IsSetSecretHash_mE01D6E679D1CCF55530EEFB6EDCB59CC1CFF701C,
	ConfirmForgotPasswordRequest_get_UserContextData_m1164E52B0569C3505241051932D7223E550B26FF,
	ConfirmForgotPasswordRequest_IsSetUserContextData_m7702412DDBFC9B9ADDE10BF85F97DD5ED48D1E9A,
	ConfirmForgotPasswordRequest_get_Username_mF6D3516F984BA1F6F367D6F5A363D8837F3D225E,
	ConfirmForgotPasswordRequest_set_Username_mAF5FB34173101A4652332211A96DAE259D845F84,
	ConfirmForgotPasswordRequest_IsSetUsername_m6231891A68F80E008575E65391E512B90E94F6F2,
	ConfirmForgotPasswordRequest_CreateSigner_mECFA32762B2FFB78AF55A0481A24E14CE177C637,
	ConfirmForgotPasswordRequest__ctor_m70B4035E4979E3664855AC69077783A9D117BF87,
	ConfirmForgotPasswordResponse__ctor_m2CD8CEB8D18BA0F49C582951F2B5192F3D4CF20C,
	ConfirmSignUpRequest_get_AnalyticsMetadata_m8D5BE60E28EC30723F7A84ED36302DCFC04F7010,
	ConfirmSignUpRequest_IsSetAnalyticsMetadata_m3C8AFCD667301811E33AFD0C80B3A0E688F0AF28,
	ConfirmSignUpRequest_get_ClientId_mFFDFBC5599ABE75F2BE00D4B437BC1A020005357,
	ConfirmSignUpRequest_set_ClientId_m92D9FC3DF80782B086E7C6312BE827FA34E17FE7,
	ConfirmSignUpRequest_IsSetClientId_m39D3AED5F70C39CD00B9A817DF2E206B692CE491,
	ConfirmSignUpRequest_get_ClientMetadata_mD2F66D469E4D793B53DA3837AD8FDFB62EA5A389,
	ConfirmSignUpRequest_IsSetClientMetadata_mCA83EA686D4CF5CF8643AEEE96DAA08AC6A56D9A,
	ConfirmSignUpRequest_get_ConfirmationCode_m3BB2965266FC8B08DA8006ACC311FDBF60D877FE,
	ConfirmSignUpRequest_set_ConfirmationCode_m3726D6E87CE236A8C1A4A99078E3F82783F47326,
	ConfirmSignUpRequest_IsSetConfirmationCode_m4F2939025A21E2A9A1B41EBEA8489DA0734E3BBF,
	ConfirmSignUpRequest_get_ForceAliasCreation_m308CC477D25685DFB09377ACA419C792B1683F31,
	ConfirmSignUpRequest_IsSetForceAliasCreation_m2AE743205A4B2A496726BB5B28790CD183479123,
	ConfirmSignUpRequest_get_SecretHash_m75E11506EBB250CA509357E0339C7E9B1B5EEF3B,
	ConfirmSignUpRequest_IsSetSecretHash_m0DA2DC1924D70C67B90778855D0E6DF222473CE9,
	ConfirmSignUpRequest_get_UserContextData_m3962F63D71B677ADC844415DB93EF0F511C5330A,
	ConfirmSignUpRequest_IsSetUserContextData_m4863B2F90F157DFFCFE2A4727A95B1CFB6095283,
	ConfirmSignUpRequest_get_Username_m84BA687C5E59E6BD0ADFFAEF80B435466C557AE2,
	ConfirmSignUpRequest_set_Username_mFC3EDA77071127E72D1E59889BEF9C45A88853DF,
	ConfirmSignUpRequest_IsSetUsername_m7601D594FB2BD98DF7445429CB0C64F0536B47BF,
	ConfirmSignUpRequest_CreateSigner_mC4AFAF332BAEC45E07FE63612753D58DC26F36CE,
	ConfirmSignUpRequest__ctor_m99AB7BAF60FC29173F7EA5390E5B581CA92BB824,
	ConfirmSignUpResponse__ctor_mABE976DA3FC62B506977A58A45487C7199C848B7,
	DeleteUserRequest_get_AccessToken_m129961148A6C6BF0BE2726001025BC01EF4FAF29,
	DeleteUserRequest_set_AccessToken_m81106B579EB5AF1A716BD2E3160BE7D4C0879C2C,
	DeleteUserRequest_IsSetAccessToken_mEBB3895C59D311D59E7C1C0A2FC4B287A5CE84C0,
	DeleteUserRequest_CreateSigner_mEE0367EBA8445F97DEB7640B0D84958E7A4A448C,
	DeleteUserRequest__ctor_mAE8FC26428F32B8707F36664642B22B69D32FDA0,
	DeleteUserResponse__ctor_m6CAE22C32FCD15C6F343764B0B5996523B6D9DEF,
	ExpiredCodeException__ctor_m9BDC5BA324B07BACCAA8AE0D5BA7E092366BD573,
	ForgotPasswordRequest_get_AnalyticsMetadata_m2190D893FDAE3859A41548565DE64BD9534F8D1C,
	ForgotPasswordRequest_IsSetAnalyticsMetadata_mB3C07FF20A97BB134BC621C9F403F3151D15D87B,
	ForgotPasswordRequest_get_ClientId_mD835CA3FCC2FAD2F99B8FCDD80371677900644F9,
	ForgotPasswordRequest_set_ClientId_m7A2F9E2D3373AFE9A5022B29BB84ED3C7BCBDAF9,
	ForgotPasswordRequest_IsSetClientId_m6FE62CFE42E8C439D8D49FB1FC7A15ABAC25080B,
	ForgotPasswordRequest_get_ClientMetadata_mD2E2F4B37B09E752E80E42E3DC8BB2423E4CCB62,
	ForgotPasswordRequest_IsSetClientMetadata_mD261D3BFCE7F3CA3F62891F580D058739E02544A,
	ForgotPasswordRequest_get_SecretHash_m74A16072233DA036E4A5993788E0CDDE53D08C42,
	ForgotPasswordRequest_IsSetSecretHash_m6A48D452358B5D91489A76F9538DDB3316DE9FB0,
	ForgotPasswordRequest_get_UserContextData_mE2000D5BED2D8F26DEF3FFD7065C897B378E4EFE,
	ForgotPasswordRequest_IsSetUserContextData_m7C1AF7622D31A61C48F1FB2FCBA563B56612254B,
	ForgotPasswordRequest_get_Username_mEEE1D5334881B2C4013555D7255599ECFF9D75A0,
	ForgotPasswordRequest_set_Username_mB7843A6D612299F3E91D66AF9B5B87613D477711,
	ForgotPasswordRequest_IsSetUsername_m4910D5039B7FABC10D333BFB7749969471E8D030,
	ForgotPasswordRequest_CreateSigner_m4154568E0930D555277A8B18F6B77463EB098B95,
	ForgotPasswordRequest__ctor_mA6FA7BBE75173F4305B5444139C4E31FC51EC19C,
	ForgotPasswordResponse_set_CodeDeliveryDetails_mE202180101219AFD576D05B22994E4862D3D30D1,
	ForgotPasswordResponse__ctor_m28BEA4877669209F7DB4AFEA2071CDFE40C6CCE5,
	GetUserRequest_get_AccessToken_mF820F3D1A0FBEC42F51EA3556AB9C660D3190F63,
	GetUserRequest_set_AccessToken_mD43AFC490FCD0B0D1F3B948B98CD153618BE7A59,
	GetUserRequest_IsSetAccessToken_mED7E473207E09A9DF63C412868A23DBD46A998F5,
	GetUserRequest_CreateSigner_mE76CDD638C754CC0D505337FA8A0810D9C7E7ABB,
	GetUserRequest__ctor_m739E7B92C951841E9DD3D646B01E164C121508FB,
	GetUserResponse_set_MFAOptions_mA2777B15E093D0A7C9D43FAA7A8B99142DC4CE6A,
	GetUserResponse_set_PreferredMfaSetting_mD35F9E7189A65324EA90B1F1421F94C3F13F9D44,
	GetUserResponse_get_UserAttributes_m1FD7121ED3BBD207B7ACD5EF8A319BF7F88302D3,
	GetUserResponse_set_UserAttributes_mD591332A7804F3DC61F6CB7CB6DE7355C63AD1B1,
	GetUserResponse_set_UserMFASettingList_m0F7569DF58358FD3F7D9D30E638B027AB3E4CDE1,
	GetUserResponse_set_Username_m28EC674B05DBBB64FCA18008C6F579C2EAC85DEC,
	GetUserResponse__ctor_mB774455816E326B4D40CA706D0B8957E679648C7,
	InitiateAuthRequest_get_AnalyticsMetadata_m6DFD1FCD99B133528C1A9DFCEE7F39373D75F0F4,
	InitiateAuthRequest_IsSetAnalyticsMetadata_mF5CBC4527A5FF58E5E6875258F46C28CA2606DA1,
	InitiateAuthRequest_get_AuthFlow_m651D3A105FC751E2D24332E659AFD69455037F22,
	InitiateAuthRequest_set_AuthFlow_mD0AB0CD4A6044E83D4C8DB9E520984E4E6148649,
	InitiateAuthRequest_IsSetAuthFlow_mD63221A3A3B35520DC78773834254047E084CC6C,
	InitiateAuthRequest_get_AuthParameters_m8EDAAC67BDB894DFFD14574069FF458FE45766FA,
	InitiateAuthRequest_set_AuthParameters_m156E35FA9567233EE27CE4819499095448504CDF,
	InitiateAuthRequest_IsSetAuthParameters_m77DCEDED7B3304AEF84A6ADAB9EE7C9BCA72C640,
	InitiateAuthRequest_get_ClientId_m0AEE5901023CB30B93422875AFFB409A4B4BF6AA,
	InitiateAuthRequest_set_ClientId_m7AF95FACF7F24BA83DA0AB54F23102925DFF1E74,
	InitiateAuthRequest_IsSetClientId_m3F1E548E0749B5026311B8EA6F53DAE9281B1D35,
	InitiateAuthRequest_get_ClientMetadata_m1F2309645DE6EB054367FB7334070DF20816DDDE,
	InitiateAuthRequest_IsSetClientMetadata_m5DB46F51E021C2312205E6BD32B65C65D8E33C51,
	InitiateAuthRequest_get_UserContextData_m62C0E8BED33E1EE2B9E5FFEC552035F27E7E3C77,
	InitiateAuthRequest_IsSetUserContextData_m71B0D88E89E2211C075DB111CF34C2105DFAD576,
	InitiateAuthRequest_CreateSigner_m59F8B57CC944A080BD9757F137609790EB185A58,
	InitiateAuthRequest__ctor_mC7F09D29F08F846B156E36B3B6C2256A164F5618,
	InitiateAuthResponse_set_AuthenticationResult_mD310039CABE8D1F619A69A9B85F79A93E45B8E5E,
	InitiateAuthResponse_get_ChallengeName_m7D4CCE67488E4D35933D39E8B54524B4A5484894,
	InitiateAuthResponse_set_ChallengeName_m6AEF15A158F1B795E595947D6A39E9157301EFFE,
	InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083,
	InitiateAuthResponse_set_ChallengeParameters_m485956DEB9C245E74A8C3B785CD1B62458A0BC46,
	InitiateAuthResponse_get_Session_mDCD4361A025B34D001F279629CB1FABD4838B3DC,
	InitiateAuthResponse_set_Session_mF833F05A560079C893B5A979D23ADF954DB893A8,
	InitiateAuthResponse__ctor_m4A926424DE090C9C6578137A08D2903DE375C0D1,
	InternalErrorException__ctor_m5F50C7AD34535531006083C86DE91DDD3FB81411,
	InvalidEmailRoleAccessPolicyException__ctor_m4A2A1ABC4A3E88F43C5513820633C8EC878E11A4,
	InvalidLambdaResponseException__ctor_m178258F9DA37EC2F66F604FF231C60AA548686D0,
	InvalidParameterException__ctor_m1E8C4D9E458F43B14CFF0944429AE055DA98C541,
	InvalidPasswordException__ctor_mE50D7545D1B0C2FEF6B7EF375F77F9473E2D0DD7,
	InvalidSmsRoleAccessPolicyException__ctor_m24FD3E785C88938EF4CFE3F41092E8308848EEFB,
	InvalidSmsRoleTrustRelationshipException__ctor_m0636D728FF121431E250BF14A924C1D22FD2CFA3,
	InvalidUserPoolConfigurationException__ctor_mECB1917C9829F5C7426F42778441DF2F81B376F0,
	LimitExceededException__ctor_m7F67D5453DED1BA1DFFDF8E0AAE55F8FA9B55A90,
	MFAMethodNotFoundException__ctor_mD11EAB6725C9C40048F779F27CF16FC90EF6C3FD,
	MFAOptionType_set_AttributeName_mCFB36521CD484ED0668C625C429365914FF3EA9C,
	MFAOptionType_set_DeliveryMedium_m9F57162CB17515411121F5C40AAE3F0D054C6181,
	MFAOptionType__ctor_m78C9E37EB1A86C6164F4B55F133E9DE9D2A8C567,
	NewDeviceMetadataType_set_DeviceGroupKey_m2D6F962AE896D064C46B9BFD32DA5E36EA7E08BA,
	NewDeviceMetadataType_set_DeviceKey_mF105E48B8285CF675A60ADAD351B621D887681C1,
	NewDeviceMetadataType__ctor_m755159CCA94412866413D61F6AD42D65E90EBAFA,
	NotAuthorizedException__ctor_mD612605E1BC6EB4B9A360AC2CEEEFEDEA143565D,
	PasswordResetRequiredException__ctor_m9D6F931ADA69FCBC0DE3AFB2B9D940189B8AFC56,
	ResendConfirmationCodeRequest_get_AnalyticsMetadata_m605D361B3B8C8327C844BBEEB6512C7FAFCFAE85,
	ResendConfirmationCodeRequest_IsSetAnalyticsMetadata_m9D90088A82FCBF0C69E86374AF633AA7146D486B,
	ResendConfirmationCodeRequest_get_ClientId_m4964812B82DBEB379124783F0F9F7FDB8BB67751,
	ResendConfirmationCodeRequest_set_ClientId_mE0E265CA6C04EE3A3D5C1F6C4A93562D13AC4011,
	ResendConfirmationCodeRequest_IsSetClientId_m20DA1C33ED899B80AF73D6698A2B75683C4531CE,
	ResendConfirmationCodeRequest_get_ClientMetadata_m364C1B00999770E6E574E0C9B9B59E4618430809,
	ResendConfirmationCodeRequest_IsSetClientMetadata_m4D43DFD7EDABE3D5F93729BB1AE6302EC4603A07,
	ResendConfirmationCodeRequest_get_SecretHash_mA44031CFF261472D27299AA0342E590BE175C7C1,
	ResendConfirmationCodeRequest_IsSetSecretHash_m795AAA0D4E522BA567E53A0C48F02A3CB1AC147A,
	ResendConfirmationCodeRequest_get_UserContextData_mC92ED5BD140FB5BFA566A41A3AF68E4CFC8002D1,
	ResendConfirmationCodeRequest_IsSetUserContextData_m4F83C73765E0788779299D58F0F25F1AC8A9FDBF,
	ResendConfirmationCodeRequest_get_Username_mD7915E79C3BCD1BA2D9A5C9492CC758952C6F0CB,
	ResendConfirmationCodeRequest_set_Username_m9589AFD7B6148224EBBA962F02E7EB0B44E79C18,
	ResendConfirmationCodeRequest_IsSetUsername_mE5C80174C58F9DA1FEDB24A191E82E752EAD6734,
	ResendConfirmationCodeRequest_CreateSigner_m69C029623B7E6519D0FC48DEF68F09F78A073FF0,
	ResendConfirmationCodeRequest__ctor_m9E709F5354E5389ED686FD2C8757F58C166B754D,
	ResendConfirmationCodeResponse_set_CodeDeliveryDetails_m6D3790009E1FF897E20B477E581D569B0547FB55,
	ResendConfirmationCodeResponse__ctor_mBF16CC4542106288794B3DD4DA68DD193B50716F,
	ResourceNotFoundException__ctor_m3A83B96D9B79F8EE5DB1DE21052FDCA54A54A020,
	RespondToAuthChallengeRequest_get_AnalyticsMetadata_m610E55AEF6DDE9EE8E2A30AEA03AD83243902810,
	RespondToAuthChallengeRequest_IsSetAnalyticsMetadata_mEAE463237C30D324A9EBEAC9DC230A6EC93D29CE,
	RespondToAuthChallengeRequest_get_ChallengeName_m106724388CFD3DC96C820517A8EB6C4AD73A7F1C,
	RespondToAuthChallengeRequest_set_ChallengeName_mC3307C66F44FF605F9672F763CD0C77DD56ABCD2,
	RespondToAuthChallengeRequest_IsSetChallengeName_mDCBDBBBA26B8D738352F9EF5009923DAC53866D9,
	RespondToAuthChallengeRequest_get_ChallengeResponses_mF628D1CC2388ABC64C8AA8CCB942462F1CC347F5,
	RespondToAuthChallengeRequest_set_ChallengeResponses_mB198D0AEB28A31F3BC175BC70CF2C82DCBA1BAAA,
	RespondToAuthChallengeRequest_IsSetChallengeResponses_mA48871FE302E862B882B08B55AF02851966233EB,
	RespondToAuthChallengeRequest_get_ClientId_mFA9DB06F57FF9EFD332104AAEF42B349B7D2EB09,
	RespondToAuthChallengeRequest_set_ClientId_m5CD35765D3E001E538AF5FE4BF61469ADDA1EF71,
	RespondToAuthChallengeRequest_IsSetClientId_mCA373FA38DD2244A815410FDAF4F85F5A7BE7300,
	RespondToAuthChallengeRequest_get_ClientMetadata_mD38A818DDFEB885CDC315DA9E25B4967CEC82B8C,
	RespondToAuthChallengeRequest_IsSetClientMetadata_mB225E14408518D268E6929AA5EA7456A3EBC32FD,
	RespondToAuthChallengeRequest_get_Session_m52A785ACFFBD8ABA7B3E84142C62EC327B3C8FAB,
	RespondToAuthChallengeRequest_set_Session_m9A001CCF2391CD45F8FCCF2FF12669691744DB4D,
	RespondToAuthChallengeRequest_IsSetSession_m74A17EDA0A258E7A0DF1A44B1FAE711D4990F0F6,
	RespondToAuthChallengeRequest_get_UserContextData_mAA8795725AA3C3518D4AF9F050A33FD2004DB678,
	RespondToAuthChallengeRequest_IsSetUserContextData_mCA79564D0EBA49B81D09A1FFE695AEBC1722B7E8,
	RespondToAuthChallengeRequest_CreateSigner_m46CA61AF5870888FCF728A423DAE30A092FEFBB9,
	RespondToAuthChallengeRequest__ctor_m96C934A734FB612A3DC464BE3396F64DD157E276,
	RespondToAuthChallengeResponse_get_AuthenticationResult_mEA1392BB5EEE1EAF8C6CA2A4295ADC1CDC6C9FC0,
	RespondToAuthChallengeResponse_set_AuthenticationResult_mEAB48A6B4EA0BAFBBDBB34D639A5980AA00B6954,
	RespondToAuthChallengeResponse_get_ChallengeName_m9DDD431CE7D58ABBF4D71D33B9B3DA93D1931475,
	RespondToAuthChallengeResponse_set_ChallengeName_mCBAC8E4D13C66EF276109F939CB9BF26A03380D2,
	RespondToAuthChallengeResponse_get_ChallengeParameters_mDCF21DF93EAE302BE3A1851D6631B5033C6BDB87,
	RespondToAuthChallengeResponse_set_ChallengeParameters_m0D45D8964EF4A45791BBCCC90A75B2F1105888C8,
	RespondToAuthChallengeResponse_get_Session_mE00CF70381F77B441D8F0D6CAC1E8EBF7A262D69,
	RespondToAuthChallengeResponse_set_Session_m0472C71E0C1B86705074F7069D8D9AB1C6F16E9C,
	RespondToAuthChallengeResponse__ctor_m4568F630BB959913B1FA35875684FC5A71EF0CE6,
	SignUpRequest_get_AnalyticsMetadata_mFDE53924C3DDC69AED987BE09BED363E8FCA4C08,
	SignUpRequest_IsSetAnalyticsMetadata_mF626D2687F6B2292B4446BF3FF5D42E7E14269D8,
	SignUpRequest_get_ClientId_m38E5086D9C708126C2C76FFC24CECD75AD62F0E4,
	SignUpRequest_set_ClientId_m8D3778B21304CBE645B7E5C1FD6328C440E1F572,
	SignUpRequest_IsSetClientId_mDCF7A7656711B38F6D97457EF6F86C73063DAA51,
	SignUpRequest_get_ClientMetadata_mD5DE92081FB2F80937D381847031AD9BC80B7F15,
	SignUpRequest_IsSetClientMetadata_mBAD9B62130F1231090BE9B07319610205C23237F,
	SignUpRequest_get_Password_mF33CBC2E11ADAB89A5F8677CD7C8922CEC7F28BC,
	SignUpRequest_set_Password_m690273F4CCE0DB9CD7BB9B2AB50EED5DC1FC8E77,
	SignUpRequest_IsSetPassword_mA593D1AA1EE31BAF35FF26268EECD663A471F54E,
	SignUpRequest_get_SecretHash_m6C7D2FC42F085F3D9F27C28095480311A58E0310,
	SignUpRequest_IsSetSecretHash_mF2DA325380C867B9D668E66DABC378817E875B32,
	SignUpRequest_get_UserAttributes_m11EBCEA1A4E9203C4A0E14B23604A66276242313,
	SignUpRequest_set_UserAttributes_m34CF62D792215A4D38FD222FA8CDABA5F9C096C9,
	SignUpRequest_IsSetUserAttributes_m06B378FAB402B919BF64FEF3C385934772E35BF7,
	SignUpRequest_get_UserContextData_m5906D9EE87A0645DB2592EF655A8EA669E7D8530,
	SignUpRequest_IsSetUserContextData_mD1CF945FF52DB151168F84A7FABFA2418CA948F2,
	SignUpRequest_get_Username_mCD40DCB5F88CDB17EE7994226B3B3017FDC9A4DC,
	SignUpRequest_set_Username_m3B201A8BE8084BDA9A4E65A4DDD0881A0A98AC55,
	SignUpRequest_IsSetUsername_mF24A6615286E6E90716AC19E9A685BECB0217F00,
	SignUpRequest_get_ValidationData_mB4DCC7EE24D1DB3E7049FD413E52EAC64A3346E7,
	SignUpRequest_IsSetValidationData_m5BA0A95159BF6B2BC4F11D2A511F42A0338B7446,
	SignUpRequest_CreateSigner_m4E237C14B28AE9354938831E365BC17C5C22A166,
	SignUpRequest__ctor_m69DAC851235EA4C382648EED22C21D413ACFD1BF,
	SignUpResponse_set_CodeDeliveryDetails_m1190DE473CB8B93189EE1D5A1DE9AD315E14AA7B,
	SignUpResponse_set_UserConfirmed_mF6C912CEF7FAA57ECC35C2DA4B5DFD4FFB743F92,
	SignUpResponse_set_UserSub_m140FDBD8D5C644C6290A7BA413D71626BAF8C188,
	SignUpResponse__ctor_m026A6F2E5FEBAAD0C30FB5765D745B8413C7F1E7,
	SoftwareTokenMFANotFoundException__ctor_m4D696C800E29286B689F040C179468802C0CABE9,
	TooManyFailedAttemptsException__ctor_m1C21A11259B42C6BC5C0AE2CD895AD39485E9154,
	TooManyRequestsException__ctor_m4E061C4F8174B859C59F080CCB55E31290664119,
	UnexpectedLambdaException__ctor_m1C19A026D8391E366BDFC2B6E2E1C9626E55BDF1,
	UpdateUserAttributesRequest_get_AccessToken_m44327AE0992690CCC60411575928C98DCA7D470F,
	UpdateUserAttributesRequest_set_AccessToken_m2EEDD3E530D83AAB366533A9D94639F826B50DF7,
	UpdateUserAttributesRequest_IsSetAccessToken_mBF7FE63F9CB150909A9CA2A1E429D7F2B7227D5C,
	UpdateUserAttributesRequest_get_ClientMetadata_m82FE7629C4D10670482BBA1F37F2FEF7FE54E84E,
	UpdateUserAttributesRequest_IsSetClientMetadata_m1BA519FBC90FD05842E33E0206F6D09208E808C7,
	UpdateUserAttributesRequest_get_UserAttributes_mA05B158A6880F99C314B5EE96574D533E39FAE26,
	UpdateUserAttributesRequest_set_UserAttributes_mCF9C2328D7872428332D48440484CB4B42B5BCDC,
	UpdateUserAttributesRequest_IsSetUserAttributes_m1BEFB283723D8BC8F1D73B93660158B58AA50DB0,
	UpdateUserAttributesRequest_CreateSigner_m6D48459D0F85D836CDFD69ABCEE6146B48082E2E,
	UpdateUserAttributesRequest__ctor_mE6BBEE3D1C02E1C937E2D64BC20B480B25596B3E,
	UpdateUserAttributesResponse_set_CodeDeliveryDetailsList_m9FC77F710C677154D642F498C8D0CE6B38872E4B,
	UpdateUserAttributesResponse__ctor_mBC5C04ECB1061DE39321C442A686A20AA087F244,
	UserContextDataType_get_EncodedData_mA9146BA037C81F92641B73C34A9DCA6BE897070E,
	UserContextDataType_IsSetEncodedData_mE2D822989CC762CA11DB039BEEAFAB1F1D1DD727,
	UserLambdaValidationException__ctor_mAB839410A273A9A57D8A9F909ECEBAB33A6292AB,
	UsernameExistsException__ctor_m6C6924F058BDDE3FD051D662684ADAF450E97D83,
	UserNotConfirmedException__ctor_mCAAAAD1B4E91FABC54439E76D22179047F8502FE,
	UserNotFoundException__ctor_m9C8F291A9EF29A33A96B088D9BDD43F8D2901B81,
	AliasExistsExceptionUnmarshaller_Unmarshall_mA550B3805C577FA5D6CEB1AF4A81CAD06A2E7F3B,
	AliasExistsExceptionUnmarshaller_Unmarshall_m32CFF31B55E3826C688F1BF7ABB7A90AD2C949C1,
	AliasExistsExceptionUnmarshaller_get_Instance_mB06DD94DA853897876BFA0CA13E41EBA166F0D47,
	AliasExistsExceptionUnmarshaller__ctor_m6964BD7E4A2CF685C312EA49C306D94A2ACCD00D,
	AliasExistsExceptionUnmarshaller__cctor_m643DD00CE9ABA2FEEF6958F0743A0177CAD48A6A,
	AnalyticsMetadataTypeMarshaller_Marshall_mD51F8B1B94729240C1708E261F109CF5EF599BBB,
	AnalyticsMetadataTypeMarshaller__ctor_m58C64BFC7D91FE411D67AA642F5958678996B8EA,
	AnalyticsMetadataTypeMarshaller__cctor_m0D6D4AD698057ACB43E3EA7DDD18C495793442DE,
	AttributeTypeMarshaller_Marshall_mD390B3AFCBFD66F285608B0C378DBCF9CA4D905B,
	AttributeTypeMarshaller__ctor_m25C50E1A6DFA0A15ADE9E4C21F75D467AFD5F11D,
	AttributeTypeMarshaller__cctor_mF84451B00786E24A58D5A95D097CADD1A7D242A9,
	AttributeTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_AttributeTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m0CB4481F8BBB0189B1FBB5F3B8F5588C270BF542,
	AttributeTypeUnmarshaller_Unmarshall_mB1C0E6C533A1E8C6A5E33A73748F12AB8B85B73B,
	AttributeTypeUnmarshaller_get_Instance_m18E421AF719663DA5ACBF47E36E82B68DA31A10F,
	AttributeTypeUnmarshaller__ctor_m52CDD451ED7E3FC287017C20EBD0E4B55742FA49,
	AttributeTypeUnmarshaller__cctor_m4E830510327AB5C9F8F9A39E8AD3B267EDC2B64A,
	AuthenticationResultTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_AuthenticationResultTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mE1045A88BB6A032709033217A47F88A583179D17,
	AuthenticationResultTypeUnmarshaller_Unmarshall_m2B43899B8C5010DD43972BFBDC4E2C6AF0C4ED93,
	AuthenticationResultTypeUnmarshaller_get_Instance_m0910035B0302D287CC9E3E65B17AC269E3640D82,
	AuthenticationResultTypeUnmarshaller__ctor_m382466A9CD197A35FC4F4C2B8E9EC8A1D18CF320,
	AuthenticationResultTypeUnmarshaller__cctor_mF74AA94CF7DEB8F63B048CE895B8886E0E4C4D72,
	CodeDeliveryDetailsTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_CodeDeliveryDetailsTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m2FCEEFB6A6D293AE881C02C904DA9F976F798DDF,
	CodeDeliveryDetailsTypeUnmarshaller_Unmarshall_mB750B9771380F3B03B1F33859DC1CB31DF6ED168,
	CodeDeliveryDetailsTypeUnmarshaller_get_Instance_m2FCC02E3DFAB30932F6A690E7877B3964A17DE02,
	CodeDeliveryDetailsTypeUnmarshaller__ctor_mC370447E4E29D3413879A6945D263B268BEB1AB6,
	CodeDeliveryDetailsTypeUnmarshaller__cctor_mE1487388CCBD736A5EAFBBE5072BB84B0D1FB988,
	CodeDeliveryFailureExceptionUnmarshaller_Unmarshall_m615A6E8F568EB25093C00822F92322DC115DF2A3,
	CodeDeliveryFailureExceptionUnmarshaller_Unmarshall_m78DBE18E63AB3302BC0C67D166F556670A175874,
	CodeDeliveryFailureExceptionUnmarshaller_get_Instance_m16F13759AF1DEF5237D878AC1BB4E0B6235086E2,
	CodeDeliveryFailureExceptionUnmarshaller__ctor_m7A3EAC7BE70E2611B321469412AC1C320003A3E8,
	CodeDeliveryFailureExceptionUnmarshaller__cctor_mB8EB1D351DA24FEACCEFBB79471F011BAB46A4FA,
	CodeMismatchExceptionUnmarshaller_Unmarshall_m8A87C66CB17055A25255C8C0E5D8ACAB13AAEC3A,
	CodeMismatchExceptionUnmarshaller_Unmarshall_mAABA7C067C7D89EDEE6168943B23A7C3FC7B7D09,
	CodeMismatchExceptionUnmarshaller_get_Instance_m3C78B7E30FB1E96EBDE21ACC44E1347B315E3ED8,
	CodeMismatchExceptionUnmarshaller__ctor_m400794C85F5954DA64D1A1860F60E1F38DD8AB89,
	CodeMismatchExceptionUnmarshaller__cctor_m77C77030B28A9D01B3282EA91D6DB0109CFDCD14,
	ConfirmForgotPasswordRequestMarshaller_Marshall_m2B71CB2E264E3E181DB84F0A99A27C271C4F39EF,
	ConfirmForgotPasswordRequestMarshaller_Marshall_m6C870B3ADA2285509FC854C62B5074FCBAC5779D,
	ConfirmForgotPasswordRequestMarshaller_get_Instance_mCD5363BF0CFDB6B569E79E96FA0BD19DB2FB4A42,
	ConfirmForgotPasswordRequestMarshaller__ctor_m2C61042C89321D6B361C39C1519B091707AE7B50,
	ConfirmForgotPasswordRequestMarshaller__cctor_m395A5C1FAD328F33FF08927796F4D101B77C6075,
	ConfirmForgotPasswordResponseUnmarshaller_Unmarshall_m8DAD8533C2E31AF904E6667ECA3F97F292415206,
	ConfirmForgotPasswordResponseUnmarshaller_UnmarshallException_m4580CC2855CC960839355BF0D9849FCC4DC7FE73,
	ConfirmForgotPasswordResponseUnmarshaller_get_Instance_m5FD1E2358ABAA78C6B23C653CB3E016DD5D843EA,
	ConfirmForgotPasswordResponseUnmarshaller__ctor_m3270DBF7DDD976697311B346BC7D9047821230F0,
	ConfirmForgotPasswordResponseUnmarshaller__cctor_m6986E425C7D4B6503B69CFDC40F1F55F621C8677,
	ConfirmSignUpRequestMarshaller_Marshall_mD956C835320745D342A1C4C12C0F2704F2259DF2,
	ConfirmSignUpRequestMarshaller_Marshall_mB71AAFE4FC451241A92A4CFD1A27B99DC368CF7B,
	ConfirmSignUpRequestMarshaller_get_Instance_mC907BE7D67D1A158FF50E50C2AC55230ED9A6ECC,
	ConfirmSignUpRequestMarshaller__ctor_m6B1B3DEEBB94B360A22F9A011901811C38D2FD1D,
	ConfirmSignUpRequestMarshaller__cctor_m455E46D62DA246D9A4D01DD20445300385ACB8CD,
	ConfirmSignUpResponseUnmarshaller_Unmarshall_mE533E105BD1B36C76090A0D6FEB12ACBEC1E75FE,
	ConfirmSignUpResponseUnmarshaller_UnmarshallException_mB7912422987ADA8469EA0CBFF16AA42DFE82C23E,
	ConfirmSignUpResponseUnmarshaller_get_Instance_mDBEF64097B651EC4DAD39A126BF896D286FEA58D,
	ConfirmSignUpResponseUnmarshaller__ctor_m15FC0BAE8E902DCFBF47D4F1CA5DCCC26CD42FE2,
	ConfirmSignUpResponseUnmarshaller__cctor_m249E0387FBCEC40A88BCE430E71D61D20CBCA661,
	DeleteUserRequestMarshaller_Marshall_m9159B4AF57C4854AA087234DA8A900DBE73EE732,
	DeleteUserRequestMarshaller_Marshall_m9BED3FE76CD75332E9C1C28CF8948D0CBBC1BF0B,
	DeleteUserRequestMarshaller_get_Instance_m3E3A9CFB75E9CF676D64169294EAFB6826A6905D,
	DeleteUserRequestMarshaller__ctor_mE535F025FE3CE845FBA34DB3B7716D0CC8E5B22D,
	DeleteUserRequestMarshaller__cctor_m3F162A7DCC2942A66B90BE6ACFD087E3EAD00604,
	DeleteUserResponseUnmarshaller_Unmarshall_m2CDFA5FE67D6F6CD5DA8B73BDEDB09E9BE240FF7,
	DeleteUserResponseUnmarshaller_UnmarshallException_m06CCDE5AC5B57F93242B78CFCE71275227ECF454,
	DeleteUserResponseUnmarshaller_get_Instance_mE5FD1B7FED231DB3421FEE4B084CDBD35EF9E079,
	DeleteUserResponseUnmarshaller__ctor_m90C8C80DF218EFCED5F6645A77561B32ED5A0446,
	DeleteUserResponseUnmarshaller__cctor_mE5485579F9D08D40494EE1ADA2AF629483276DC8,
	ExpiredCodeExceptionUnmarshaller_Unmarshall_mC7483D2093143FCAF6DA11D1909B225D932C5D85,
	ExpiredCodeExceptionUnmarshaller_Unmarshall_mE3A5832F58F3A4B01D680ADEA62299F9E418472E,
	ExpiredCodeExceptionUnmarshaller_get_Instance_m605BD07FF522043A4E4F808E7214A87908D3AEC5,
	ExpiredCodeExceptionUnmarshaller__ctor_m90C7A32D37A7F78CC057997DC0E85C8B541AB282,
	ExpiredCodeExceptionUnmarshaller__cctor_mD3F499A7295D50682CA823C5AD68D69DBDE412B8,
	ForgotPasswordRequestMarshaller_Marshall_mC5664F311D31E860CCEF01C2315D1B4904FB5246,
	ForgotPasswordRequestMarshaller_Marshall_m07466C4D41B543756F8683B2B69160C34DC0DC59,
	ForgotPasswordRequestMarshaller_get_Instance_m1DB0D9AB3E3290C394F24BED2DD28FFC60894822,
	ForgotPasswordRequestMarshaller__ctor_m89178EBA7AD5E2D49A02C9AC905CA515578A77B8,
	ForgotPasswordRequestMarshaller__cctor_m24B15662604D7A7E6B280C9087557943BF96ED73,
	ForgotPasswordResponseUnmarshaller_Unmarshall_m2DDB03297B2FEA2383678ECF0301A7F82FEB03F2,
	ForgotPasswordResponseUnmarshaller_UnmarshallException_m5DD681FF0FF2A3C9F0B24929C62911E37C681D5C,
	ForgotPasswordResponseUnmarshaller_get_Instance_m9DB41CB505C1DC794A471E6C692E5A655B832894,
	ForgotPasswordResponseUnmarshaller__ctor_mC10DD05315CEF011EA29BF76C6FF746EF4740B34,
	ForgotPasswordResponseUnmarshaller__cctor_m9401BA0D9B0E0089FE4C32BA751DAA7AC5F3D73B,
	GetUserRequestMarshaller_Marshall_mFBF101EC3F15ADF797616CC404BC4BA2549462F5,
	GetUserRequestMarshaller_Marshall_m783C179B3FD7992320AE779FC41CA28D51E20787,
	GetUserRequestMarshaller_get_Instance_m1775D1FB993AA384CB83DD913774B688B50DA062,
	GetUserRequestMarshaller__ctor_m713CDA413424D7D63E0D8356B9AEEA07889D1302,
	GetUserRequestMarshaller__cctor_m360FA41A3948C6D848CE7D5E06F22A3E67D0FAFE,
	GetUserResponseUnmarshaller_Unmarshall_m61D4B36FF734FBA16B1270835BC12441BD0096B4,
	GetUserResponseUnmarshaller_UnmarshallException_m910D50AC191F4C2DB4D29D145C610F8B0706106C,
	GetUserResponseUnmarshaller_get_Instance_m6BF0ABCFE3E51C8D3ED0A08576931FDB30456051,
	GetUserResponseUnmarshaller__ctor_m6116B09D3B14741F5C679F190F7CBD58D375820F,
	GetUserResponseUnmarshaller__cctor_mF52634F5923E090E2AE496F01A491FA9C89D3A9A,
	InitiateAuthRequestMarshaller_Marshall_mA55D3EA1BE872D6F8614298042B352A5264328BF,
	InitiateAuthRequestMarshaller_Marshall_mCFCABBFB5B16DD015DB6D14AD5B3DAD52CBB678A,
	InitiateAuthRequestMarshaller_get_Instance_m6862A3204009CEF5C09EA963382169F622B25B3D,
	InitiateAuthRequestMarshaller__ctor_m739F51539300647D80EDA1436D0237494F262CF7,
	InitiateAuthRequestMarshaller__cctor_mA69B7464DF6B001E506D5D7B79363627CE92BA50,
	InitiateAuthResponseUnmarshaller_Unmarshall_m2CF7826B087767115610C976EEBAF7027BACA8B9,
	InitiateAuthResponseUnmarshaller_UnmarshallException_mADC340E929A836AFE4BC900983CCDE36738F1AFF,
	InitiateAuthResponseUnmarshaller_get_Instance_m3DB021027B7AB1D88D46E53B880D17FB88421495,
	InitiateAuthResponseUnmarshaller__ctor_m4D25D2AE75DD2AAA2924D5D006251E1C05084C15,
	InitiateAuthResponseUnmarshaller__cctor_m8A8E9A604CED8A69805DF7DF3ED7D597E169CF95,
	InternalErrorExceptionUnmarshaller_Unmarshall_mAD3C4AB0052FFEDE3991EB8E6BF86BB95788F888,
	InternalErrorExceptionUnmarshaller_Unmarshall_m6C720E688F8A6430818854C4BD9438071DAA4FFD,
	InternalErrorExceptionUnmarshaller_get_Instance_m4A99DD9E2AAE5BF7229EC7EE87FB7DAED91E20A4,
	InternalErrorExceptionUnmarshaller__ctor_mD353123768EA76658F4A1EC2A7ABE7A79FF6528E,
	InternalErrorExceptionUnmarshaller__cctor_m739BE7929FB5EBCC7C846552C774FA48C4FB7D7E,
	InvalidEmailRoleAccessPolicyExceptionUnmarshaller_Unmarshall_mBC7BD1FC2F66C5EAF9BCBA84BD7D939908B6684D,
	InvalidEmailRoleAccessPolicyExceptionUnmarshaller_Unmarshall_m1D13B63B8FB3444C695C97ADE5B29306996B4045,
	InvalidEmailRoleAccessPolicyExceptionUnmarshaller_get_Instance_m9BC774D998258BA7C1CDD07EB233838811F286C0,
	InvalidEmailRoleAccessPolicyExceptionUnmarshaller__ctor_m744443DA89CBFDB60E3C44F893C7926723FFAA5C,
	InvalidEmailRoleAccessPolicyExceptionUnmarshaller__cctor_mE19AB9AB2FC4D0A13DF9171D3CE967480C7A3A5B,
	InvalidLambdaResponseExceptionUnmarshaller_Unmarshall_m061D5E6C0682B877D988F8A2BD97D4643E3FF072,
	InvalidLambdaResponseExceptionUnmarshaller_Unmarshall_mDAA25B57B1608355505A0803979D373BDEF8E464,
	InvalidLambdaResponseExceptionUnmarshaller_get_Instance_m85971D16EA6E5F2DFE038AB99D13855CDF4BF3DA,
	InvalidLambdaResponseExceptionUnmarshaller__ctor_m3209E47C83262E372D0F0204C47EE3843313FF53,
	InvalidLambdaResponseExceptionUnmarshaller__cctor_mF4F076A834584E639D8A7EC3011222C37104AE96,
	InvalidParameterExceptionUnmarshaller_Unmarshall_m973C06878C7844576F33DED71A37682D56D676C8,
	InvalidParameterExceptionUnmarshaller_Unmarshall_m66CBAF9097D15F9144AF003E082466516D3EBDF7,
	InvalidParameterExceptionUnmarshaller_get_Instance_m007700C4DFACF9C7CF723954AE4E7EFB657D32F5,
	InvalidParameterExceptionUnmarshaller__ctor_mB97B964E4A247A5AEA43C806B91106B50213B29D,
	InvalidParameterExceptionUnmarshaller__cctor_m3F68EE1625C44B5B0828A3CEC50DDBF345685E3A,
	InvalidPasswordExceptionUnmarshaller_Unmarshall_m70EBD3C5E135C46F391FF4C0979361A0C50EC01A,
	InvalidPasswordExceptionUnmarshaller_Unmarshall_m7A7CCB9AA123BD53A0EB8C5FC32E865DC82E1E4F,
	InvalidPasswordExceptionUnmarshaller_get_Instance_mA4F2693B34E0D741F816CD84F438DDF941712992,
	InvalidPasswordExceptionUnmarshaller__ctor_m040BC92093DB9CF314778624E00C6D5534C5A792,
	InvalidPasswordExceptionUnmarshaller__cctor_mF63A0071C56EE07EA1EBE9E886A3D0F858A9DEB6,
	InvalidSmsRoleAccessPolicyExceptionUnmarshaller_Unmarshall_m3A40209863D9B8766BA538775D664FBE4B97B610,
	InvalidSmsRoleAccessPolicyExceptionUnmarshaller_Unmarshall_m69852F9DE7B846DB6A2FEE0B1827303BCAA41276,
	InvalidSmsRoleAccessPolicyExceptionUnmarshaller_get_Instance_m17ABA64B94AEDF0344E29DF1A1EFD4124173AA9D,
	InvalidSmsRoleAccessPolicyExceptionUnmarshaller__ctor_mA86D275B86E477740D47FA9EB78B06DE9DC8E430,
	InvalidSmsRoleAccessPolicyExceptionUnmarshaller__cctor_m9EFA2588E8F590BC187285CCFFCBD5F3DAAA159B,
	InvalidSmsRoleTrustRelationshipExceptionUnmarshaller_Unmarshall_m215FBFE501E1B66580B06DD5B39DC36960A88010,
	InvalidSmsRoleTrustRelationshipExceptionUnmarshaller_Unmarshall_mFA456803D2238442137DC83166572E2DC75F09CC,
	InvalidSmsRoleTrustRelationshipExceptionUnmarshaller_get_Instance_m160B098973CD353A4E859E3FE5A39740D19A5520,
	InvalidSmsRoleTrustRelationshipExceptionUnmarshaller__ctor_m04AA1C0CB57A9A5CEF6160E482D5977E0D942A23,
	InvalidSmsRoleTrustRelationshipExceptionUnmarshaller__cctor_mC024084616A02B7C1FBD6EF3CC8C2AE14014C19F,
	InvalidUserPoolConfigurationExceptionUnmarshaller_Unmarshall_m9346FAE4D6C0AB62D9FCA552719B7326D36F3594,
	InvalidUserPoolConfigurationExceptionUnmarshaller_Unmarshall_mBD72968228375879D3EE84334AFF30D2D1E8D0E1,
	InvalidUserPoolConfigurationExceptionUnmarshaller_get_Instance_mEF1439F07F68896F5D03C659C4845DEAAC0832F6,
	InvalidUserPoolConfigurationExceptionUnmarshaller__ctor_mA484FA3FDBB13BCD1B745F02A5754DB2E9517CD1,
	InvalidUserPoolConfigurationExceptionUnmarshaller__cctor_m90D2323E92E5F3FA57400DFD3D163E295847DEBB,
	LimitExceededExceptionUnmarshaller_Unmarshall_m6E49C614D9DA37638A3B13A7BED694F9467D267D,
	LimitExceededExceptionUnmarshaller_Unmarshall_mF0C2682436B10CC21FF4FC674437E12E003C40CE,
	LimitExceededExceptionUnmarshaller_get_Instance_mFD8D97DEFB2F4082548BBAECD887D2045ACB8F3F,
	LimitExceededExceptionUnmarshaller__ctor_m8D11D904E2E2BA4E41E0B07FFFC831C56D501A9C,
	LimitExceededExceptionUnmarshaller__cctor_mF292FA22998B13832A8F54145CD0CB04B49F29C1,
	MFAMethodNotFoundExceptionUnmarshaller_Unmarshall_mF5CD1A63B47B450A0935D5BB70ED837A63B15BC4,
	MFAMethodNotFoundExceptionUnmarshaller_Unmarshall_mED4D3B8C19388ED97209B9A20BCB60AA1F2F4D66,
	MFAMethodNotFoundExceptionUnmarshaller_get_Instance_m5D77547A83B20913B0F12566082A34C4B6D3E41E,
	MFAMethodNotFoundExceptionUnmarshaller__ctor_m2C3A88BD6190E384C459EB299AC19012028EF976,
	MFAMethodNotFoundExceptionUnmarshaller__cctor_m0663FB2CFD4087C8C006D64D03B530E3AB578832,
	MFAOptionTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_MFAOptionTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m1F46208CA6563DB06C3FDCD927ECE185C62310AA,
	MFAOptionTypeUnmarshaller_Unmarshall_m6E4884AAD0A1B582EC2D708C0A93E91346BA76DF,
	MFAOptionTypeUnmarshaller_get_Instance_m54FD3855028D7BD6A20BEDA161033EA288E3B569,
	MFAOptionTypeUnmarshaller__ctor_m9553855CC5A32D6EAA249A045CD9E624FB7B2701,
	MFAOptionTypeUnmarshaller__cctor_m8C1A498BD0145EA3D8BFBE7E9872857152A84722,
	NewDeviceMetadataTypeUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentityProvider_Model_NewDeviceMetadataTypeU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mA3E8101A5340AE5A23C1BFFFAFBCEBF8836AFBB2,
	NewDeviceMetadataTypeUnmarshaller_Unmarshall_m601A2D49F40A323FF06C43CE27E3225523BBF57B,
	NewDeviceMetadataTypeUnmarshaller_get_Instance_mEDCCD6F0F9900D421EFAE46A7BA8D6E61F1380B8,
	NewDeviceMetadataTypeUnmarshaller__ctor_m4B2F5764F04AB25895A7F03D8E8C28F6E04E1CE8,
	NewDeviceMetadataTypeUnmarshaller__cctor_m24D6C3A04FBBCD61D3A1490A8C68E023C1E82649,
	NotAuthorizedExceptionUnmarshaller_Unmarshall_mB7479CC6F942707FBFB1CB4A0BAD1F4F039B3A9D,
	NotAuthorizedExceptionUnmarshaller_Unmarshall_mB6B390657DF755648F166EC6ADEB082C1F57B3D2,
	NotAuthorizedExceptionUnmarshaller_get_Instance_m1A39FD14DFCB04135EEC14CAA1E7CE049181F4E6,
	NotAuthorizedExceptionUnmarshaller__ctor_m910253B83F6E1DDDDDA5C6A145B78584E4B25452,
	NotAuthorizedExceptionUnmarshaller__cctor_mB4DF0CF621560DDA03C54C88FB1F2D8A82064D1A,
	PasswordResetRequiredExceptionUnmarshaller_Unmarshall_m21BCE7DFA9727861D47EEBF781A809CB45CAB39B,
	PasswordResetRequiredExceptionUnmarshaller_Unmarshall_m3FA6AE9A7F8FF5DD240B677786A22585BD187771,
	PasswordResetRequiredExceptionUnmarshaller_get_Instance_m3E7C6D7187651B970D4E48308AD5CD80C1813B88,
	PasswordResetRequiredExceptionUnmarshaller__ctor_m9AE99017E017422F6A6E1E7762EE3F6A15EABDF8,
	PasswordResetRequiredExceptionUnmarshaller__cctor_m1D0A8427828A7DDC594D0DA8C69E1BD08DCE0377,
	ResendConfirmationCodeRequestMarshaller_Marshall_mE988BD8DE672794B9E5D9F8E9838D0F330B44E45,
	ResendConfirmationCodeRequestMarshaller_Marshall_m90D4B2294A0499B524826E273613D6DD97710602,
	ResendConfirmationCodeRequestMarshaller_get_Instance_m2146B7FDC911F70EF478F8CC81228B95E79351A0,
	ResendConfirmationCodeRequestMarshaller__ctor_m9184557D58B359EC59F7C9287BA5D4DA31E475BF,
	ResendConfirmationCodeRequestMarshaller__cctor_m6B0CA64AB2A26E56D412EF7DA9072FDA18127EEF,
	ResendConfirmationCodeResponseUnmarshaller_Unmarshall_mC7D13C57D8535488B08B88AF8BC86E7CC15F32F1,
	ResendConfirmationCodeResponseUnmarshaller_UnmarshallException_m8E8399256A25005B5E772EFB3F8DD1FE47AF8F7C,
	ResendConfirmationCodeResponseUnmarshaller_get_Instance_mFE72134EE807487BFB86ED9A0EA627D286C6020B,
	ResendConfirmationCodeResponseUnmarshaller__ctor_m66A601EF35772C6C7CD2C6125730C7FA5440EAAF,
	ResendConfirmationCodeResponseUnmarshaller__cctor_m9A815A98183E8B7C7BAEB2AB8D50F6DDE442E531,
	ResourceNotFoundExceptionUnmarshaller_Unmarshall_m52B0E7FCB10DDCB657B637D8BE923E24186B8F1D,
	ResourceNotFoundExceptionUnmarshaller_Unmarshall_m6E0FD237E6C03B1680C68E65DC882256668A90D7,
	ResourceNotFoundExceptionUnmarshaller_get_Instance_m388330ABF5C3709C0E9335000E0AC78073D1115B,
	ResourceNotFoundExceptionUnmarshaller__ctor_mC7BDBB368CB7D666446D1942FEC706B1036ED4E7,
	ResourceNotFoundExceptionUnmarshaller__cctor_mAC9763144DBB45FF77E0A369907F4F8B12931D4C,
	RespondToAuthChallengeRequestMarshaller_Marshall_m30EA522FCFEBB4654B322785D0826B2A645A625E,
	RespondToAuthChallengeRequestMarshaller_Marshall_m994D2AABBF182DBE1D040DA60238FC0B615B47E9,
	RespondToAuthChallengeRequestMarshaller_get_Instance_m89F9A5F95DC5B86867FDC21762000E21F18F9DBE,
	RespondToAuthChallengeRequestMarshaller__ctor_mB02DE9140ED492DF65F6007FA1059412A703291A,
	RespondToAuthChallengeRequestMarshaller__cctor_mC15C60CA4AD93D1121AAAC23F52F2D358DD4710A,
	RespondToAuthChallengeResponseUnmarshaller_Unmarshall_m11941F2BC27E98FC4C4F961E47461F9EF08CD9D1,
	RespondToAuthChallengeResponseUnmarshaller_UnmarshallException_m30EE0A104E00B0E2C808B2419DE74AEAB7EF705B,
	RespondToAuthChallengeResponseUnmarshaller_get_Instance_mDAB8DB4724A9635F0F9D6A8C36689EF990AF7D0F,
	RespondToAuthChallengeResponseUnmarshaller__ctor_mC2AED72087DFBEC5068CED3DD5510E5373F5DFA4,
	RespondToAuthChallengeResponseUnmarshaller__cctor_m26E089E1223745BEC5446F92A7605462B78A0AD0,
	SignUpRequestMarshaller_Marshall_m97C213502AD4D619CF65F005CA2E33E9AF48D748,
	SignUpRequestMarshaller_Marshall_mC175F2E337B122EA16361FB201522F39A75B4EB4,
	SignUpRequestMarshaller_get_Instance_m728639ACDE5AD31615D3BA16A848493B780702E7,
	SignUpRequestMarshaller__ctor_mE14042B97D45F5230FDE04A3E99FE3A0F3768A8B,
	SignUpRequestMarshaller__cctor_mEA09A284DA2CAB91CBDF6D171BF015CBD08AE396,
	SignUpResponseUnmarshaller_Unmarshall_mDECD040F422E74DE3C4D5336126EF85564454E6A,
	SignUpResponseUnmarshaller_UnmarshallException_m1B2824F4BC1E0FEAD80C611B4D43621AF261575B,
	SignUpResponseUnmarshaller_get_Instance_mB0E11FC26E679CFB26394536DC32722F6D3A16CB,
	SignUpResponseUnmarshaller__ctor_m792BF25A94C8D4E1F21945DB2BF1B66B7B285729,
	SignUpResponseUnmarshaller__cctor_m0F89FBC75CA7DDAB6096BD1D5A2B9B12C2115673,
	SoftwareTokenMFANotFoundExceptionUnmarshaller_Unmarshall_m5ADD9FCDB2F3FAB15D7EEE617EA2A47AF0E4BA67,
	SoftwareTokenMFANotFoundExceptionUnmarshaller_Unmarshall_m011083A03A3E4E06070BFA4AF0F97AA4730B8C0B,
	SoftwareTokenMFANotFoundExceptionUnmarshaller_get_Instance_m96E5760BA542759D077D452BE26E9357627D80A5,
	SoftwareTokenMFANotFoundExceptionUnmarshaller__ctor_mC63A7540DC28CAD32C09384C390B5D276B125A47,
	SoftwareTokenMFANotFoundExceptionUnmarshaller__cctor_mDCAF5BA3CCFBD43C5251BDA9C50C62B119426A65,
	TooManyFailedAttemptsExceptionUnmarshaller_Unmarshall_mAC38B8B73C725E5EE7657EE0359F30A2E9A20B17,
	TooManyFailedAttemptsExceptionUnmarshaller_Unmarshall_mCF997C8ED3F1C9EB1F478ECBD8E7309CAFF3509E,
	TooManyFailedAttemptsExceptionUnmarshaller_get_Instance_m76E27648999332DCE7ABDC54ADD46280021B0733,
	TooManyFailedAttemptsExceptionUnmarshaller__ctor_mB133C90BAB6CF1D65E615A9AC7FA2547FBCDCB3B,
	TooManyFailedAttemptsExceptionUnmarshaller__cctor_mF7A629BF26B1B1C5064C3BF7E1201ECBB2E38A52,
	TooManyRequestsExceptionUnmarshaller_Unmarshall_m34F86163537CD2D93D4B4BD286F792C9FBE7A4BF,
	TooManyRequestsExceptionUnmarshaller_Unmarshall_m21802F42C1E75D3E110B963AC7A1AB36795017A9,
	TooManyRequestsExceptionUnmarshaller_get_Instance_m1F06ACE17BE08CC013BD8200C6F5BC539E03BE4F,
	TooManyRequestsExceptionUnmarshaller__ctor_mB12AAFC5529A60ECC35CC18615641D06B7E7E7AC,
	TooManyRequestsExceptionUnmarshaller__cctor_m8F54FB0B086524BA5F4587FF83E30806EA4BAD3A,
	UnexpectedLambdaExceptionUnmarshaller_Unmarshall_mD634C3423A5CB0CA8AE09F7562EBFF5433C5056D,
	UnexpectedLambdaExceptionUnmarshaller_Unmarshall_m1D42A5E61C9B368D7A2C52AD594B8FA3F78C94BB,
	UnexpectedLambdaExceptionUnmarshaller_get_Instance_mD078C37A8B7F3E8891C58D604A853E07C3A5C14F,
	UnexpectedLambdaExceptionUnmarshaller__ctor_mAC07D48887642AF0B915137AA13E0872868C70CB,
	UnexpectedLambdaExceptionUnmarshaller__cctor_m8BDE9A9A414E70272CA09C190E79F02EE9A5D4DF,
	UpdateUserAttributesRequestMarshaller_Marshall_m1D724BA63710DF81DF59B9A09C9FFF7D554722B2,
	UpdateUserAttributesRequestMarshaller_Marshall_m29C3A00C6C829EDB54128905EB1E43637DF88FA8,
	UpdateUserAttributesRequestMarshaller_get_Instance_m1556888BB0CABCAD1C7AD2B726591B67772FD9E4,
	UpdateUserAttributesRequestMarshaller__ctor_m8204FF70485968E8A445CC357F67748030DF5C62,
	UpdateUserAttributesRequestMarshaller__cctor_m3DF09A434157401D4FBECC1E6091D7954B8F046E,
	UpdateUserAttributesResponseUnmarshaller_Unmarshall_mA228F34D0D6A00CA1E45ACD60CB0043514FDEC61,
	UpdateUserAttributesResponseUnmarshaller_UnmarshallException_mDA72888FB5A607E789DA921251FD595DA6331A6E,
	UpdateUserAttributesResponseUnmarshaller_get_Instance_m19F8954668C58B6D2C2FDAEE4A5A429C185E49EC,
	UpdateUserAttributesResponseUnmarshaller__ctor_mDC33F34EEBF5C41E48A4401513A8219E4713FE2A,
	UpdateUserAttributesResponseUnmarshaller__cctor_m4C8C91B45C5253812F81A015ACBA09A085624A62,
	UserContextDataTypeMarshaller_Marshall_m2729720D9F15BC059EBB0E08A877D69E5B09A637,
	UserContextDataTypeMarshaller__ctor_m82DC4C34B65E3D2D8BAE16A5518DC492359423D9,
	UserContextDataTypeMarshaller__cctor_mAE387C89F1518F1B5125CDD52968E05D75A05AC2,
	UserLambdaValidationExceptionUnmarshaller_Unmarshall_m9345CA2D65E0F913D707D947A51421F2A5521103,
	UserLambdaValidationExceptionUnmarshaller_Unmarshall_m40EB35D159F29A345B8F3D11660D05752D57D219,
	UserLambdaValidationExceptionUnmarshaller_get_Instance_m7CD7741A77FFA4E5B143BE574E87E1D5234ED574,
	UserLambdaValidationExceptionUnmarshaller__ctor_m9FAA02E4D315FA4F0F3B2617078D4616652DBECD,
	UserLambdaValidationExceptionUnmarshaller__cctor_m45B2C4C8135789F48094DBB7C3679CF372430A46,
	UsernameExistsExceptionUnmarshaller_Unmarshall_mB9FA01AD28F6BDB2D8523227A5857B6B49011E63,
	UsernameExistsExceptionUnmarshaller_Unmarshall_m4F3B90F3519A024E669D5312DCC73AEF1C2EAC4A,
	UsernameExistsExceptionUnmarshaller_get_Instance_m2FE01427E5658E2B15AACB7CDD47B69D24B93407,
	UsernameExistsExceptionUnmarshaller__ctor_m6D3EA4B51ADA53B9248B01494BF0BFC957090F7F,
	UsernameExistsExceptionUnmarshaller__cctor_mB3D27F00947A17E4DFDDED50F33108C13AED52B7,
	UserNotConfirmedExceptionUnmarshaller_Unmarshall_m938021EFF65C35F62B19C5480654058049A5D85C,
	UserNotConfirmedExceptionUnmarshaller_Unmarshall_m9C7B621F7F7A1A9AF617FE8C4BAABC79ADFC2250,
	UserNotConfirmedExceptionUnmarshaller_get_Instance_mA4C8CA7D57C2C8610A672B21E12D7CBD35E72ED8,
	UserNotConfirmedExceptionUnmarshaller__ctor_mDEEA3C33C9A2F576FEDC1FA74570B93738156701,
	UserNotConfirmedExceptionUnmarshaller__cctor_mF9BD7314096AD723ACA8B3FA2ECED432EF6AA01B,
	UserNotFoundExceptionUnmarshaller_Unmarshall_m6E13AC031C0249D3B3C6070448EE908D5E9FC86A,
	UserNotFoundExceptionUnmarshaller_Unmarshall_mBB768486036051A24123AD68CDB0FFDF25A8ED66,
	UserNotFoundExceptionUnmarshaller_get_Instance_m8605C518577DFE6FAFAFBA905D874F46349315BB,
	UserNotFoundExceptionUnmarshaller__ctor_m95CB7EE520E00990650AFD2EE13F72B8699CB4FA,
	UserNotFoundExceptionUnmarshaller__cctor_mE75CDED487CC323ACAF6567BAA7D9C6DA0DF3AE4,
	AmazonCognitoIdentityProviderMetadata_get_ServiceId_m4B1C055BA7B46E82ECDA4AFEDEBBB84F8DB8F5AE,
	AmazonCognitoIdentityProviderMetadata_get_OperationNameMapping_mC34C6DA1F493AB113701D8FC1CB0EF86DE714D23,
	AmazonCognitoIdentityProviderMetadata__ctor_mE20A3FF26601A3D30B23D36D52D773DF2D14ACBE,
};
static const int32_t s_InvokerIndices[546] = 
{
	4257,
	4169,
	4169,
	6598,
	236,
	4257,
	3450,
	6598,
	3450,
	6309,
	6309,
	6598,
	3450,
	6309,
	6309,
	6598,
	1920,
	1920,
	4169,
	4169,
	3476,
	1415,
	1415,
	1415,
	1415,
	1415,
	1415,
	1415,
	1415,
	1415,
	1415,
	6598,
	1415,
	1415,
	236,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4257,
	4169,
	3450,
	4144,
	3428,
	4169,
	3450,
	3450,
	4169,
	3450,
	3450,
	4257,
	3450,
	3450,
	3450,
	4257,
	236,
	236,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4257,
	4257,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4197,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4257,
	4257,
	4169,
	3450,
	4197,
	4169,
	4257,
	4257,
	236,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4257,
	3450,
	4257,
	4169,
	3450,
	4197,
	4169,
	4257,
	3450,
	3450,
	4169,
	3450,
	3450,
	3450,
	4257,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	4257,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4257,
	236,
	236,
	236,
	236,
	236,
	236,
	236,
	236,
	236,
	236,
	3450,
	3450,
	4257,
	3450,
	3450,
	4257,
	236,
	236,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4257,
	3450,
	4257,
	236,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	4257,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4257,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	4257,
	3450,
	3476,
	3450,
	4257,
	236,
	236,
	236,
	236,
	4169,
	3450,
	4197,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	4257,
	3450,
	4257,
	4169,
	4197,
	236,
	236,
	236,
	236,
	2558,
	1417,
	6579,
	4257,
	6598,
	1920,
	4257,
	6598,
	1920,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	1920,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	4169,
	4169,
	4257,
};
extern const CustomAttributesCacheGenerator g_AWSSDK_CognitoIdentityProvider_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AWSSDK_CognitoIdentityProvider_CodeGenModule;
const Il2CppCodeGenModule g_AWSSDK_CognitoIdentityProvider_CodeGenModule = 
{
	"AWSSDK.CognitoIdentityProvider.dll",
	546,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AWSSDK_CognitoIdentityProvider_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
