﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// Amazon.Runtime.Internal.AWSPropertyAttribute
struct AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Amazon.Runtime.Internal.AWSPropertyAttribute
struct AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int64 Amazon.Runtime.Internal.AWSPropertyAttribute::min
	int64_t ___min_0;
	// System.Int64 Amazon.Runtime.Internal.AWSPropertyAttribute::max
	int64_t ___max_1;
	// System.Boolean Amazon.Runtime.Internal.AWSPropertyAttribute::<Required>k__BackingField
	bool ___U3CRequiredU3Ek__BackingField_2;
	// System.Boolean Amazon.Runtime.Internal.AWSPropertyAttribute::<IsMinSet>k__BackingField
	bool ___U3CIsMinSetU3Ek__BackingField_3;
	// System.Boolean Amazon.Runtime.Internal.AWSPropertyAttribute::<IsMaxSet>k__BackingField
	bool ___U3CIsMaxSetU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___min_0)); }
	inline int64_t get_min_0() const { return ___min_0; }
	inline int64_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int64_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___max_1)); }
	inline int64_t get_max_1() const { return ___max_1; }
	inline int64_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int64_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_U3CRequiredU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___U3CRequiredU3Ek__BackingField_2)); }
	inline bool get_U3CRequiredU3Ek__BackingField_2() const { return ___U3CRequiredU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CRequiredU3Ek__BackingField_2() { return &___U3CRequiredU3Ek__BackingField_2; }
	inline void set_U3CRequiredU3Ek__BackingField_2(bool value)
	{
		___U3CRequiredU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsMinSetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___U3CIsMinSetU3Ek__BackingField_3)); }
	inline bool get_U3CIsMinSetU3Ek__BackingField_3() const { return ___U3CIsMinSetU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsMinSetU3Ek__BackingField_3() { return &___U3CIsMinSetU3Ek__BackingField_3; }
	inline void set_U3CIsMinSetU3Ek__BackingField_3(bool value)
	{
		___U3CIsMinSetU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsMaxSetU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___U3CIsMaxSetU3Ek__BackingField_4)); }
	inline bool get_U3CIsMaxSetU3Ek__BackingField_4() const { return ___U3CIsMaxSetU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsMaxSetU3Ek__BackingField_4() { return &___U3CIsMaxSetU3Ek__BackingField_4; }
	inline void set_U3CIsMaxSetU3Ek__BackingField_4(bool value)
	{
		___U3CIsMaxSetU3Ek__BackingField_4 = value;
	}
};


// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Min(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Max(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880 (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Required(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, bool ___value0, const RuntimeMethod* method);
static void AWSSDK_CognitoIdentity_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[0];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x31\x2E\x30\x2E\x30"), NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[4];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x57\x53\x53\x44\x4B\x2E\x43\x6F\x67\x6E\x69\x74\x6F\x49\x64\x65\x6E\x74\x69\x74\x79"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[5];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x57\x65\x62\x20\x53\x65\x72\x76\x69\x63\x65\x73\x20\x53\x44\x4B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54\x20\x28\x4E\x65\x74\x53\x74\x61\x6E\x64\x61\x72\x64\x20\x32\x2E\x30\x29\x2D\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x43\x6F\x67\x6E\x69\x74\x6F\x20\x49\x64\x65\x6E\x74\x69\x74\x79\x2E\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x43\x6F\x67\x6E\x69\x74\x6F\x20\x69\x73\x20\x61\x20\x73\x65\x72\x76\x69\x63\x65\x20\x74\x68\x61\x74\x20\x6D\x61\x6B\x65\x73\x20\x69\x74\x20\x65\x61\x73\x79\x20\x74\x6F\x20\x73\x61\x76\x65\x20\x75\x73\x65\x72\x20\x64\x61\x74\x61\x2C\x20\x73\x75\x63\x68\x20\x61\x73\x20\x61\x70\x70\x20\x70\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x20\x6F\x72\x20\x67\x61\x6D\x65\x20\x73\x74\x61\x74\x65\x2C\x20\x69\x6E\x20\x74\x68\x65\x20\x41\x57\x53\x20\x43\x6C\x6F\x75\x64\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x77\x72\x69\x74\x69\x6E\x67\x20\x61\x6E\x79\x20\x62\x61\x63\x6B\x65\x6E\x64\x20\x63\x6F\x64\x65\x20\x6F\x72\x20\x6D\x61\x6E\x61\x67\x69\x6E\x67\x20\x61\x6E\x79\x20\x69\x6E\x66\x72\x61\x73\x74\x72\x75\x63\x74\x75\x72\x65\x2E\x20\x57\x69\x74\x68\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x43\x6F\x67\x6E\x69\x74\x6F\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x66\x6F\x63\x75\x73\x20\x6F\x6E\x20\x63\x72\x65\x61\x74\x69\x6E\x67\x20\x67\x72\x65\x61\x74\x20\x61\x70\x70\x20\x65\x78\x70\x65\x72\x69\x65\x6E\x63\x65\x73\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x68\x61\x76\x69\x6E\x67\x20\x74\x6F\x20\x77\x6F\x72\x72\x79\x20\x61\x62\x6F\x75\x74\x20\x62\x75\x69\x6C\x64\x69\x6E\x67\x20\x61\x6E\x64\x20\x6D\x61\x6E\x61\x67\x69\x6E\x67\x20\x61\x20\x62\x61\x63\x6B\x65\x6E\x64\x20\x73\x6F\x6C\x75\x74\x69\x6F\x6E\x20\x74\x6F\x20\x68\x61\x6E\x64\x6C\x65\x20\x69\x64\x65\x6E\x74\x69\x74\x79\x20\x6D\x61\x6E\x61\x67\x65\x6D\x65\x6E\x74\x2C\x20\x6E\x65\x74\x77\x6F\x72\x6B\x20\x73\x74\x61\x74\x65\x2C\x20\x73\x74\x6F\x72\x61\x67\x65\x2C\x20\x61\x6E\x64\x20\x73\x79\x6E\x63\x2E"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[6];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[7];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x61\x7A\x6F\x6E\x20\x57\x65\x62\x20\x53\x65\x72\x76\x69\x63\x65\x73\x20\x53\x44\x4B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[8];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x61\x7A\x6F\x6E\x2E\x63\x6F\x6D\x2C\x20\x49\x6E\x63"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[9];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\x32\x30\x30\x39\x2D\x32\x30\x32\x30\x20\x41\x6D\x61\x7A\x6F\x6E\x2E\x63\x6F\x6D\x2C\x20\x49\x6E\x63\x2E\x20\x6F\x72\x20\x69\x74\x73\x20\x61\x66\x66\x69\x6C\x69\x61\x74\x65\x73\x2E\x20\x41\x6C\x6C\x20\x52\x69\x67\x68\x74\x73\x20\x52\x65\x73\x65\x72\x76\x65\x64\x2E"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[10];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[11];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[12];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x33\x2E\x31\x30\x31\x2E\x31\x32\x33"), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[13];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, true, NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[14];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CAccountIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CIdentityPoolIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CUnAuthRoleArnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CAuthRoleArnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CLoginsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_AccountId_mFD716F20C332024EB6451626EB289652EF60DE5C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_AccountId_m4C60662CDCEE59A214A9E52D4F4A3B4DB7213871(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_IdentityPoolId_m05675F5728B4D5B22E9D1EFCC9C3E11ABE37CF01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_IdentityPoolId_m383C1E4CCC30C06769DC56DA43A9C5EE4ADF8687(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_UnAuthRoleArn_m8AB13E52FCF387418A0112308CC4AB7C2BAC67CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_UnAuthRoleArn_m95C4EAE379B90E37891399167036A5C8C1E59764(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_AuthRoleArn_m7D015E01B7C59D324A69DBD1AE96FD42BDF65531(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_AuthRoleArn_m7C215B3F5A6EA88F9F6A3B1804AB8651B15254BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_Logins_m2A5C730D3ED5F54AEBFCD2895989C28A60B3B879(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_Logins_m4041C4F1E605BD69496E7469725DE644B3A6F336(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GetIdentityIdAsync_mD72497165B81784DC891F0E1E3AC9F58C2F9A386(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_0_0_0_var), NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_RefreshIdentityAsync_m9DCA06AD0B2AF5AC8370CD392C58CAF39DEB0763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_0_0_0_var), NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GenerateNewCredentialsAsync_mDDA7ACD8FD9077EDA3EC620A8D57BBEB23CDEC0D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_0_0_0_var), NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GetCredentialsForRoleAsync_m1B7058BA1EB066807F8A53B2F1F0510F5A68C44D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_0_0_0_var), NULL);
	}
}
static void CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GetPoolCredentialsAsync_mC335FFDC27B3FF3EFDA900C96AE2C5352D7E57A8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_0_0_0_var), NULL);
	}
}
static void RefreshIdentityOptions_tE02B14E59217A71C2651A24633E36E8AEAB3502C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_U3COldIdentityIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_U3CNewIdentityIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_IdentityChangedArgs_set_OldIdentityId_m57CD3C9B5A788DF970FDA9FA43D59619C91FFF10(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_IdentityChangedArgs_set_NewIdentityId_mA6F59055D45797660071EDF2A0D2941B58A68F74(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CIdentityIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CLoginProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CLoginTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CFromCacheU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_get_IdentityId_mBB9A188EF26616327FCA450C6AB24C0631E4EE57(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_set_IdentityId_m096546285D286E36CEF28C0E44A97A11C455F88C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_get_LoginProvider_mADBDD700DBBAA6B2B98D487235A679B619FBA85C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_get_LoginToken_m5D27CC5E79D7281A37B19A672AE357F794924CF0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_set_FromCache_mAEE2CCBEB1465FAC2717C785A8DA28A60E547003(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass45_0_tD0FBB3510D95E8EC53FF02FFB5CABFCD238FB66B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_CustomAttributesCacheGenerator_U3CGetIdentityIdAsyncU3Ed__47_SetStateMachine_mB885C55C150DB3C1E0E333BC4025A7E48C8A9BEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_CustomAttributesCacheGenerator_U3CRefreshIdentityAsyncU3Ed__48_SetStateMachine_m6E25BA598D8051AAF0B1D1AD045CFA2B52FFABFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_CustomAttributesCacheGenerator_U3CGenerateNewCredentialsAsyncU3Ed__57_SetStateMachine_m1E2ED47C7984178238597DB655BB1C3895C04DB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_CustomAttributesCacheGenerator_U3CGetCredentialsForRoleAsyncU3Ed__58_SetStateMachine_m610D75E395FF843F8D80FA0178D947383E9B2C45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_CustomAttributesCacheGenerator_U3CGetPoolCredentialsAsyncU3Ed__59_SetStateMachine_mF363EC4A748410BD927F3911BF889C621DD0ACCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass64_0_tF19F1229003C186499AF199889743D7337DAAAFE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass65_0_tEE3387ADFC848A625E85CFDD87CB285C425A0F63_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass66_0_t9D4333DB9966A2ED2786D87EF3AE76FA49E5CA23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59_CustomAttributesCacheGenerator_GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59____CustomRoleArn_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 20LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 2048LL, NULL);
	}
}
static void GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59_CustomAttributesCacheGenerator_GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59____IdentityId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 55LL, NULL);
	}
}
static void GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59_CustomAttributesCacheGenerator_GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59____Logins_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 10LL, NULL);
	}
}
static void GetCredentialsForIdentityResponse_t96977582C0D282F1FD9107797AFAD8BA3AA76F12_CustomAttributesCacheGenerator_GetCredentialsForIdentityResponse_t96977582C0D282F1FD9107797AFAD8BA3AA76F12____IdentityId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 55LL, NULL);
	}
}
static void GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96_CustomAttributesCacheGenerator_GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96____AccountId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 15LL, NULL);
	}
}
static void GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96_CustomAttributesCacheGenerator_GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96____IdentityPoolId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 55LL, NULL);
	}
}
static void GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96_CustomAttributesCacheGenerator_GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96____Logins_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 10LL, NULL);
	}
}
static void GetIdResponse_t539071DF5116EC9D4D138DAFC1865FA3DEB8BB9E_CustomAttributesCacheGenerator_GetIdResponse_t539071DF5116EC9D4D138DAFC1865FA3DEB8BB9E____IdentityId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 55LL, NULL);
	}
}
static void GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58_CustomAttributesCacheGenerator_GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58____IdentityId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 55LL, NULL);
	}
}
static void GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58_CustomAttributesCacheGenerator_GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58____Logins_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 10LL, NULL);
	}
}
static void GetOpenIdTokenResponse_tAE340F5035993F698A541E6C28492764E3CF6B51_CustomAttributesCacheGenerator_GetOpenIdTokenResponse_tAE340F5035993F698A541E6C28492764E3CF6B51____IdentityId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 55LL, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AWSSDK_CognitoIdentity_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AWSSDK_CognitoIdentity_AttributeGenerators[60] = 
{
	RefreshIdentityOptions_tE02B14E59217A71C2651A24633E36E8AEAB3502C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass45_0_tD0FBB3510D95E8EC53FF02FFB5CABFCD238FB66B_CustomAttributesCacheGenerator,
	U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_CustomAttributesCacheGenerator,
	U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_CustomAttributesCacheGenerator,
	U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_CustomAttributesCacheGenerator,
	U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_CustomAttributesCacheGenerator,
	U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass64_0_tF19F1229003C186499AF199889743D7337DAAAFE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass65_0_tEE3387ADFC848A625E85CFDD87CB285C425A0F63_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass66_0_t9D4333DB9966A2ED2786D87EF3AE76FA49E5CA23_CustomAttributesCacheGenerator,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CAccountIdU3Ek__BackingField,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CIdentityPoolIdU3Ek__BackingField,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CUnAuthRoleArnU3Ek__BackingField,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CAuthRoleArnU3Ek__BackingField,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_U3CLoginsU3Ek__BackingField,
	IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_U3COldIdentityIdU3Ek__BackingField,
	IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_U3CNewIdentityIdU3Ek__BackingField,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CIdentityIdU3Ek__BackingField,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CLoginProviderU3Ek__BackingField,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CLoginTokenU3Ek__BackingField,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_U3CFromCacheU3Ek__BackingField,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_AccountId_mFD716F20C332024EB6451626EB289652EF60DE5C,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_AccountId_m4C60662CDCEE59A214A9E52D4F4A3B4DB7213871,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_IdentityPoolId_m05675F5728B4D5B22E9D1EFCC9C3E11ABE37CF01,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_IdentityPoolId_m383C1E4CCC30C06769DC56DA43A9C5EE4ADF8687,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_UnAuthRoleArn_m8AB13E52FCF387418A0112308CC4AB7C2BAC67CC,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_UnAuthRoleArn_m95C4EAE379B90E37891399167036A5C8C1E59764,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_AuthRoleArn_m7D015E01B7C59D324A69DBD1AE96FD42BDF65531,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_AuthRoleArn_m7C215B3F5A6EA88F9F6A3B1804AB8651B15254BA,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_get_Logins_m2A5C730D3ED5F54AEBFCD2895989C28A60B3B879,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_set_Logins_m4041C4F1E605BD69496E7469725DE644B3A6F336,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GetIdentityIdAsync_mD72497165B81784DC891F0E1E3AC9F58C2F9A386,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_RefreshIdentityAsync_m9DCA06AD0B2AF5AC8370CD392C58CAF39DEB0763,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GenerateNewCredentialsAsync_mDDA7ACD8FD9077EDA3EC620A8D57BBEB23CDEC0D,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GetCredentialsForRoleAsync_m1B7058BA1EB066807F8A53B2F1F0510F5A68C44D,
	CognitoAWSCredentials_t3E6758AF68599B48AF0DC5F93AE8B5771967E4CA_CustomAttributesCacheGenerator_CognitoAWSCredentials_GetPoolCredentialsAsync_mC335FFDC27B3FF3EFDA900C96AE2C5352D7E57A8,
	IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_IdentityChangedArgs_set_OldIdentityId_m57CD3C9B5A788DF970FDA9FA43D59619C91FFF10,
	IdentityChangedArgs_t0ACEA61736F072BC57DDB0768AA371E289F877A6_CustomAttributesCacheGenerator_IdentityChangedArgs_set_NewIdentityId_mA6F59055D45797660071EDF2A0D2941B58A68F74,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_get_IdentityId_mBB9A188EF26616327FCA450C6AB24C0631E4EE57,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_set_IdentityId_m096546285D286E36CEF28C0E44A97A11C455F88C,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_get_LoginProvider_mADBDD700DBBAA6B2B98D487235A679B619FBA85C,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_get_LoginToken_m5D27CC5E79D7281A37B19A672AE357F794924CF0,
	IdentityState_tE1D461CB4242BBD9B5C0A3FDE84E09BCD61189A1_CustomAttributesCacheGenerator_IdentityState_set_FromCache_mAEE2CCBEB1465FAC2717C785A8DA28A60E547003,
	U3CGetIdentityIdAsyncU3Ed__47_tB2FE6E90E759BEA32BC507977249F4562B03FC87_CustomAttributesCacheGenerator_U3CGetIdentityIdAsyncU3Ed__47_SetStateMachine_mB885C55C150DB3C1E0E333BC4025A7E48C8A9BEF,
	U3CRefreshIdentityAsyncU3Ed__48_t71F58DA7803899310797EE1CE8558F2F08D89023_CustomAttributesCacheGenerator_U3CRefreshIdentityAsyncU3Ed__48_SetStateMachine_m6E25BA598D8051AAF0B1D1AD045CFA2B52FFABFA,
	U3CGenerateNewCredentialsAsyncU3Ed__57_tEFD6E0F545ABBF5063CF509C85B2C080FA8ECD7E_CustomAttributesCacheGenerator_U3CGenerateNewCredentialsAsyncU3Ed__57_SetStateMachine_m1E2ED47C7984178238597DB655BB1C3895C04DB4,
	U3CGetCredentialsForRoleAsyncU3Ed__58_tF9B9858F66DED42B553D1641EE8A24EB10FFADC1_CustomAttributesCacheGenerator_U3CGetCredentialsForRoleAsyncU3Ed__58_SetStateMachine_m610D75E395FF843F8D80FA0178D947383E9B2C45,
	U3CGetPoolCredentialsAsyncU3Ed__59_t4851E339526F81C302E265EF9D1D753E8CCF68A0_CustomAttributesCacheGenerator_U3CGetPoolCredentialsAsyncU3Ed__59_SetStateMachine_mF363EC4A748410BD927F3911BF889C621DD0ACCE,
	GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59_CustomAttributesCacheGenerator_GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59____CustomRoleArn_PropertyInfo,
	GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59_CustomAttributesCacheGenerator_GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59____IdentityId_PropertyInfo,
	GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59_CustomAttributesCacheGenerator_GetCredentialsForIdentityRequest_tC2AF6D91DE547AF57112B506D1D717988B3E1E59____Logins_PropertyInfo,
	GetCredentialsForIdentityResponse_t96977582C0D282F1FD9107797AFAD8BA3AA76F12_CustomAttributesCacheGenerator_GetCredentialsForIdentityResponse_t96977582C0D282F1FD9107797AFAD8BA3AA76F12____IdentityId_PropertyInfo,
	GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96_CustomAttributesCacheGenerator_GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96____AccountId_PropertyInfo,
	GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96_CustomAttributesCacheGenerator_GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96____IdentityPoolId_PropertyInfo,
	GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96_CustomAttributesCacheGenerator_GetIdRequest_tF0350E62E052A5DF8538E63846FDD072ACF05D96____Logins_PropertyInfo,
	GetIdResponse_t539071DF5116EC9D4D138DAFC1865FA3DEB8BB9E_CustomAttributesCacheGenerator_GetIdResponse_t539071DF5116EC9D4D138DAFC1865FA3DEB8BB9E____IdentityId_PropertyInfo,
	GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58_CustomAttributesCacheGenerator_GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58____IdentityId_PropertyInfo,
	GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58_CustomAttributesCacheGenerator_GetOpenIdTokenRequest_t1B6C61716014A640BD6E58DDFEA6FCAAAF01CE58____Logins_PropertyInfo,
	GetOpenIdTokenResponse_tAE340F5035993F698A541E6C28492764E3CF6B51_CustomAttributesCacheGenerator_GetOpenIdTokenResponse_tAE340F5035993F698A541E6C28492764E3CF6B51____IdentityId_PropertyInfo,
	AWSSDK_CognitoIdentity_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CRequiredU3Ek__BackingField_2(L_0);
		return;
	}
}
