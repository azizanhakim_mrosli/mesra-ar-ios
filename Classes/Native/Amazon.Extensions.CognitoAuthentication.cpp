﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>
struct Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>>
struct Func_2_t8D6EC4F5423B4B1BBA170139EFACB64E74DDDF50;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>>
struct Func_2_t16374DF48AE4374245767B70C237F8918E244C3C;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>>
struct Func_2_t1CC0E58E7E15C4A5782AAC035620F0F8E0F7B2A2;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_tC51B0D1CF3D0A1E45BBD8B79324FE5CEA1C1CB72;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1A386BEF1855064FD5CC71F340A68881A52B4932;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD;
// System.Threading.Tasks.TaskFactory`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>
struct TaskFactory_1_t4E979A34E6ABACD380893EC448C3B44DCEFA0E60;
// System.Threading.Tasks.TaskFactory`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>
struct TaskFactory_1_tF8917CC2B280DB56DDCECCCEC69EA74A2AB7597F;
// System.Threading.Tasks.TaskFactory`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>
struct TaskFactory_1_tBFD4F2C09DDB8B228D23356F9F2CD281EA102216;
// System.Threading.Tasks.Task`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>
struct Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83;
// System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>
struct Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17;
// System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>
struct Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516;
// System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>
struct Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// Amazon.Runtime.AmazonServiceClient
struct AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A;
// Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType
struct AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71;
// Amazon.Extensions.CognitoAuthentication.AuthFlowResponse
struct AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43;
// Amazon.CognitoIdentityProvider.AuthFlowType
struct AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834;
// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType
struct AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Globalization.Calendar
struct Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3;
// Amazon.CognitoIdentityProvider.ChallengeNameType
struct ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E;
// Amazon.Extensions.CognitoAuthentication.CognitoDevice
struct CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760;
// Amazon.Extensions.CognitoAuthentication.CognitoUser
struct CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9;
// Amazon.Extensions.CognitoAuthentication.CognitoUserPool
struct CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665;
// Amazon.Extensions.CognitoAuthentication.CognitoUserSession
struct CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA;
// System.Globalization.CompareInfo
struct CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9;
// Amazon.Runtime.ConstantClass
struct ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6;
// System.Threading.ContextCallback
struct ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B;
// System.Globalization.CultureData
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90;
// System.Text.DecoderFallback
struct DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Text.EncoderFallback
struct EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// Amazon.Runtime.Internal.EndpointDiscoveryResolverBase
struct EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC;
// System.Exception
struct Exception_t;
// Amazon.Runtime.ExceptionEventHandler
struct ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB;
// System.Security.Cryptography.HMACSHA256
struct HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// Amazon.Extensions.CognitoAuthentication.HkdfSha256
struct HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F;
// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider
struct IAmazonCognitoIdentityProvider_tF6B784A34B482F25E4AB76909844BFA35B7B6B9F;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t38F64ED77DEB5E7EA3BFF6DCACCB2DE7D00BFB2A;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IFormatProvider
struct IFormatProvider_tF2AECC4B14F41D36718920D67F930CED940412DF;
// Amazon.Runtime.Internal.IServiceMetadata
struct IServiceMetadata_tC7743830F8447FD385AEF2ED9F417DBF9422F48F;
// Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest
struct InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B;
// Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse
struct InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C;
// Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest
struct InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType
struct NewDeviceMetadataType_t6551FB84BC8515F4D82EF9540ACF9C46B1E1CEA3;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D;
// Amazon.Runtime.Internal.ParameterCollection
struct ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3;
// Amazon.Runtime.PreRequestEventHandler
struct PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// Amazon.Runtime.RequestEventArgs
struct RequestEventArgs_tE1FA7940E62EB2306034D1E8ABD47B2D8ED44A56;
// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7;
// Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest
struct RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE;
// Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse
struct RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929;
// Amazon.Runtime.ResponseEventHandler
struct ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E;
// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673;
// System.Security.Cryptography.SHA256
struct SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Threading.Tasks.StackGuard
struct StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D;
// System.String
struct String_t;
// System.StringComparer
struct StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D;
// System.Globalization.TextInfo
struct TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C;
// System.Type
struct Type_t;
// System.Reflection.TypeInfo
struct TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// Amazon.CognitoIdentityProvider.Model.UserContextDataType
struct UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Amazon.Runtime.WebServiceRequestEventArgs
struct WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A;
// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0;

IL2CPP_EXTERN_C RuntimeClass* AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAmazonCognitoIdentityProvider_tF6B784A34B482F25E4AB76909844BFA35B7B6B9F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0311F42C1F9456B6C2CBD1CCC062B5F8B139A889;
IL2CPP_EXTERN_C String_t* _stringLiteral049AE51AD9A6D7846326B9188829C2D982D399B3;
IL2CPP_EXTERN_C String_t* _stringLiteral0BE6D6FEA9C0036EEDEFFD38B2D1C4EACC2B9D2F;
IL2CPP_EXTERN_C String_t* _stringLiteral1F1268E1AE201354BBC8D00D49BEAC846F12EDF7;
IL2CPP_EXTERN_C String_t* _stringLiteral3581B9DAF560030E2E31AE65668D9C6BEAA6728D;
IL2CPP_EXTERN_C String_t* _stringLiteral44D2155A9A3200DB4F66438C4A7020431E61BB80;
IL2CPP_EXTERN_C String_t* _stringLiteral48C75149E263D08DBE3F3CB86DF011FA96C010AF;
IL2CPP_EXTERN_C String_t* _stringLiteral4CB81BFE27645382DC38DCACB8A2C37AB347D6D2;
IL2CPP_EXTERN_C String_t* _stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C;
IL2CPP_EXTERN_C String_t* _stringLiteral51921D99887DD5ED233F87333EF648AE91A8BF7C;
IL2CPP_EXTERN_C String_t* _stringLiteral530150E04E6EC41BEE7DA49EB9C12C5E0289840D;
IL2CPP_EXTERN_C String_t* _stringLiteral560B461CD3B3EC699D4EEE2BF321914A093EBDB9;
IL2CPP_EXTERN_C String_t* _stringLiteral57F5D84CEA9F6D7E30D54586FAA16AC2EB4CBF63;
IL2CPP_EXTERN_C String_t* _stringLiteral5D54E959817188DBAD9E65FA3DB55F06B70F5E3C;
IL2CPP_EXTERN_C String_t* _stringLiteral6B02B1E1487008AA8DA0EBBF4799CBC796FE4D62;
IL2CPP_EXTERN_C String_t* _stringLiteral736AF32BCDCEA98E53845EF220EE293D0B73DF58;
IL2CPP_EXTERN_C String_t* _stringLiteral7D5CFB0848E7F707163817A374D1B4BFC889BDC0;
IL2CPP_EXTERN_C String_t* _stringLiteral83AF882DAE496029BF697F5B73C72F6DEAA7D63A;
IL2CPP_EXTERN_C String_t* _stringLiteral842B8C7345E8AE5608EE332378D4489B8AF9C37C;
IL2CPP_EXTERN_C String_t* _stringLiteral8616432AB4B76B87A73EC2E9EDA0A66F90F81D79;
IL2CPP_EXTERN_C String_t* _stringLiteral86202678AB0A41B628DDEEB4DD6DC622B9C6ED5E;
IL2CPP_EXTERN_C String_t* _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D;
IL2CPP_EXTERN_C String_t* _stringLiteral8B665A6C19BD67B26669FFF2993E266E6E2E4E0C;
IL2CPP_EXTERN_C String_t* _stringLiteral9C3DEEC0531EF0C653C66F20E39D7E107A8DFC4D;
IL2CPP_EXTERN_C String_t* _stringLiteralA1C11240057DA2500A6450044EFF8D777E00F26A;
IL2CPP_EXTERN_C String_t* _stringLiteralA1C3EDDE2152361A22D3D0D87C064EC546264D1C;
IL2CPP_EXTERN_C String_t* _stringLiteralA4CDF053D3B972088B7FFF38937974A6AB653925;
IL2CPP_EXTERN_C String_t* _stringLiteralAB16D8A617F31CEA5526DAECE9ED6F7764595D82;
IL2CPP_EXTERN_C String_t* _stringLiteralB1AF5447134F86E81C58631C3B6D93E2C8F97B1A;
IL2CPP_EXTERN_C String_t* _stringLiteralB9759268181E392BB5A3A5F594B01AD942FC7064;
IL2CPP_EXTERN_C String_t* _stringLiteralC0088BD9BDDD69F53812075FEBE4DE7AD5590ED0;
IL2CPP_EXTERN_C String_t* _stringLiteralC13B65C06BB3A788859D79A63DC8C530F152B2FB;
IL2CPP_EXTERN_C String_t* _stringLiteralC288B7612977CDB79FF44E3835AA0B4F6C422C31;
IL2CPP_EXTERN_C String_t* _stringLiteralCB07A09291C08815CF3D11E19DCD1076FA7C7EEE;
IL2CPP_EXTERN_C String_t* _stringLiteralCDBEC64046BDA8EB945EE3C9944B7D10CA374A4C;
IL2CPP_EXTERN_C String_t* _stringLiteralCE5A4549B4636DF6B4AC06434A3927594722F9E5;
IL2CPP_EXTERN_C String_t* _stringLiteralCFB3EFC4BEA99FC338B21324C64F1B4AEBD47192;
IL2CPP_EXTERN_C String_t* _stringLiteralD02A4A4D1C4146EC9570C4228E7416949C61E664;
IL2CPP_EXTERN_C String_t* _stringLiteralD444B40C468D66B19259CD3EFE1FF0409F040706;
IL2CPP_EXTERN_C String_t* _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE;
IL2CPP_EXTERN_C String_t* _stringLiteralD5F29C62B9F83E07CC0D9A5CD43FD5EE614A4A8A;
IL2CPP_EXTERN_C String_t* _stringLiteralDCE4227FFA1EC695E039F9C2BB1F760A67C3D925;
IL2CPP_EXTERN_C String_t* _stringLiteralDD096B8301B9807B9045A248F66BF18A2F33EC33;
IL2CPP_EXTERN_C String_t* _stringLiteralE8C29C59AD2C5A7B688373F667558973F9681B6B;
IL2CPP_EXTERN_C String_t* _stringLiteralEDAAB5C5D4BC79996A40BAC363247725901131D9;
IL2CPP_EXTERN_C String_t* _stringLiteralEDBBCE96E30F14D67FEFB0589C89F7A48EDA7C50;
IL2CPP_EXTERN_C String_t* _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024;
IL2CPP_EXTERN_C String_t* _stringLiteralFA46A75AEDB98EC4BA344FC277C99D2CBDE6A9B4;
IL2CPP_EXTERN_C String_t* _stringLiteralFBCFE8AE685D68F390182744A8EE501715BF4009;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6924C8F15A0B905ED3807EAB4462A1953849557C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_mB6450D099253BAA88CB077A27F0F2CE29BC7FCBD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_Create_mEF2A6F90F0E729A3E44F040FC6D54F8EF6736367_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_SetException_m38E596D3515939C2F19738981612DA38710F6C0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_SetResult_mAB1FB9ECA6777B761CB26DB9A9D1A72EE788DBF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_SetStateMachine_m6E3B2ACC94057E1637984A4678462C5B8E210FF1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6E9C9BE2FCAE466776F72764B7D19F77483DB6FE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncTaskMethodBuilder_1_get_Task_m7F62A8721A6D0863BCB16CB26A96CDD62355B2CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CognitoUserPool__ctor_m6619C54386E0E1725FB1EA008014B30596D19694_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CognitoUser__ctor_mF9AE181160DA6741A661389AED15CCD6664AC209_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConfiguredTaskAwaitable_1_GetAwaiter_m2B667096D2B4B58D37EA4D9C4858F5A45028C935_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConfiguredTaskAwaitable_1_GetAwaiter_mC0B99DDC353B6E7C81CA27D2D8B1A9C437AE56BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConfiguredTaskAwaiter_GetResult_m8EC35D81407E7E6F1059CF69E3F9BCE6118B9740_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConfiguredTaskAwaiter_GetResult_mCF811B9F3DF810C629A828BA855771A78CC124A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConfiguredTaskAwaiter_get_IsCompleted_m755C1F32EDA0DC621015D811895E509978A958DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConfiguredTaskAwaiter_get_IsCompleted_mC400DFEA6D067D16AF92FF753F90D1309A3F33CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m3A3DE48426936CEB09FE89B327E6D0A4B6A888C9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Task_1_ConfigureAwait_m260BD4536DEF805CC8207699944516A90738A2C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Task_1_ConfigureAwait_m5C984C4A2243EE418F8EF068BB84258F5B257C7E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_0_0_0_var;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D;
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tD01C010C5A79CCB241D3D7909563CB2137A37BB9 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___entries_1)); }
	inline EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___keys_7)); }
	inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___values_8)); }
	inline ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// Amazon.Runtime.AmazonServiceClient
struct AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.AmazonServiceClient::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.AmazonServiceClient::_logger
	Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 * ____logger_1;
	// Amazon.Runtime.Internal.EndpointDiscoveryResolverBase Amazon.Runtime.AmazonServiceClient::<EndpointDiscoveryResolver>k__BackingField
	EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC * ___U3CEndpointDiscoveryResolverU3Ek__BackingField_2;
	// Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::<RuntimePipeline>k__BackingField
	RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 * ___U3CRuntimePipelineU3Ek__BackingField_3;
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::<Credentials>k__BackingField
	AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * ___U3CCredentialsU3Ek__BackingField_4;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::<Config>k__BackingField
	RuntimeObject* ___U3CConfigU3Ek__BackingField_5;
	// Amazon.Runtime.Internal.IServiceMetadata Amazon.Runtime.AmazonServiceClient::<ServiceMetadata>k__BackingField
	RuntimeObject* ___U3CServiceMetadataU3Ek__BackingField_6;
	// Amazon.Runtime.PreRequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeMarshallingEvent
	PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 * ___mBeforeMarshallingEvent_7;
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeRequestEvent
	RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * ___mBeforeRequestEvent_8;
	// Amazon.Runtime.ResponseEventHandler Amazon.Runtime.AmazonServiceClient::mAfterResponseEvent
	ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E * ___mAfterResponseEvent_9;
	// Amazon.Runtime.ExceptionEventHandler Amazon.Runtime.AmazonServiceClient::mExceptionEvent
	ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB * ___mExceptionEvent_10;
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::<Signer>k__BackingField
	AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * ___U3CSignerU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ____logger_1)); }
	inline Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 * get__logger_1() const { return ____logger_1; }
	inline Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____logger_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEndpointDiscoveryResolverU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CEndpointDiscoveryResolverU3Ek__BackingField_2)); }
	inline EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC * get_U3CEndpointDiscoveryResolverU3Ek__BackingField_2() const { return ___U3CEndpointDiscoveryResolverU3Ek__BackingField_2; }
	inline EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC ** get_address_of_U3CEndpointDiscoveryResolverU3Ek__BackingField_2() { return &___U3CEndpointDiscoveryResolverU3Ek__BackingField_2; }
	inline void set_U3CEndpointDiscoveryResolverU3Ek__BackingField_2(EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC * value)
	{
		___U3CEndpointDiscoveryResolverU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEndpointDiscoveryResolverU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRuntimePipelineU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CRuntimePipelineU3Ek__BackingField_3)); }
	inline RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 * get_U3CRuntimePipelineU3Ek__BackingField_3() const { return ___U3CRuntimePipelineU3Ek__BackingField_3; }
	inline RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 ** get_address_of_U3CRuntimePipelineU3Ek__BackingField_3() { return &___U3CRuntimePipelineU3Ek__BackingField_3; }
	inline void set_U3CRuntimePipelineU3Ek__BackingField_3(RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 * value)
	{
		___U3CRuntimePipelineU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRuntimePipelineU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CCredentialsU3Ek__BackingField_4)); }
	inline AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * get_U3CCredentialsU3Ek__BackingField_4() const { return ___U3CCredentialsU3Ek__BackingField_4; }
	inline AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 ** get_address_of_U3CCredentialsU3Ek__BackingField_4() { return &___U3CCredentialsU3Ek__BackingField_4; }
	inline void set_U3CCredentialsU3Ek__BackingField_4(AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * value)
	{
		___U3CCredentialsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCredentialsU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CConfigU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CConfigU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CConfigU3Ek__BackingField_5() const { return ___U3CConfigU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CConfigU3Ek__BackingField_5() { return &___U3CConfigU3Ek__BackingField_5; }
	inline void set_U3CConfigU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CConfigU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConfigU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServiceMetadataU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CServiceMetadataU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CServiceMetadataU3Ek__BackingField_6() const { return ___U3CServiceMetadataU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CServiceMetadataU3Ek__BackingField_6() { return &___U3CServiceMetadataU3Ek__BackingField_6; }
	inline void set_U3CServiceMetadataU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CServiceMetadataU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CServiceMetadataU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_mBeforeMarshallingEvent_7() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mBeforeMarshallingEvent_7)); }
	inline PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 * get_mBeforeMarshallingEvent_7() const { return ___mBeforeMarshallingEvent_7; }
	inline PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 ** get_address_of_mBeforeMarshallingEvent_7() { return &___mBeforeMarshallingEvent_7; }
	inline void set_mBeforeMarshallingEvent_7(PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 * value)
	{
		___mBeforeMarshallingEvent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBeforeMarshallingEvent_7), (void*)value);
	}

	inline static int32_t get_offset_of_mBeforeRequestEvent_8() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mBeforeRequestEvent_8)); }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * get_mBeforeRequestEvent_8() const { return ___mBeforeRequestEvent_8; }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 ** get_address_of_mBeforeRequestEvent_8() { return &___mBeforeRequestEvent_8; }
	inline void set_mBeforeRequestEvent_8(RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * value)
	{
		___mBeforeRequestEvent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBeforeRequestEvent_8), (void*)value);
	}

	inline static int32_t get_offset_of_mAfterResponseEvent_9() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mAfterResponseEvent_9)); }
	inline ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E * get_mAfterResponseEvent_9() const { return ___mAfterResponseEvent_9; }
	inline ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E ** get_address_of_mAfterResponseEvent_9() { return &___mAfterResponseEvent_9; }
	inline void set_mAfterResponseEvent_9(ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E * value)
	{
		___mAfterResponseEvent_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mAfterResponseEvent_9), (void*)value);
	}

	inline static int32_t get_offset_of_mExceptionEvent_10() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mExceptionEvent_10)); }
	inline ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB * get_mExceptionEvent_10() const { return ___mExceptionEvent_10; }
	inline ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB ** get_address_of_mExceptionEvent_10() { return &___mExceptionEvent_10; }
	inline void set_mExceptionEvent_10(ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB * value)
	{
		___mExceptionEvent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mExceptionEvent_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSignerU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CSignerU3Ek__BackingField_11)); }
	inline AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * get_U3CSignerU3Ek__BackingField_11() const { return ___U3CSignerU3Ek__BackingField_11; }
	inline AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE ** get_address_of_U3CSignerU3Ek__BackingField_11() { return &___U3CSignerU3Ek__BackingField_11; }
	inline void set_U3CSignerU3Ek__BackingField_11(AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * value)
	{
		___U3CSignerU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSignerU3Ek__BackingField_11), (void*)value);
	}
};


// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55  : public RuntimeObject
{
public:
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonWebServiceRequest::mBeforeRequestEvent
	RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * ___mBeforeRequestEvent_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.StreamUploadProgressCallback>k__BackingField
	EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A * ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1;
	// System.Boolean Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.UseSigV4>k__BackingField
	bool ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_mBeforeRequestEvent_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55, ___mBeforeRequestEvent_0)); }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * get_mBeforeRequestEvent_0() const { return ___mBeforeRequestEvent_0; }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 ** get_address_of_mBeforeRequestEvent_0() { return &___mBeforeRequestEvent_0; }
	inline void set_mBeforeRequestEvent_0(RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * value)
	{
		___mBeforeRequestEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBeforeRequestEvent_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1)); }
	inline EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A * get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A ** get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A * value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2)); }
	inline bool get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2(bool value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// Amazon.Extensions.CognitoAuthentication.AuthFlowResponse
struct AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43  : public RuntimeObject
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::<SessionID>k__BackingField
	String_t* ___U3CSessionIDU3Ek__BackingField_0;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::<ChallengeName>k__BackingField
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___U3CChallengeNameU3Ek__BackingField_1;
	// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::<AuthenticationResult>k__BackingField
	AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___U3CAuthenticationResultU3Ek__BackingField_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::<ChallengeParameters>k__BackingField
	RuntimeObject* ___U3CChallengeParametersU3Ek__BackingField_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::<ClientMetadata>k__BackingField
	RuntimeObject* ___U3CClientMetadataU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSessionIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43, ___U3CSessionIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CSessionIDU3Ek__BackingField_0() const { return ___U3CSessionIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSessionIDU3Ek__BackingField_0() { return &___U3CSessionIDU3Ek__BackingField_0; }
	inline void set_U3CSessionIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CSessionIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSessionIDU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CChallengeNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43, ___U3CChallengeNameU3Ek__BackingField_1)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_U3CChallengeNameU3Ek__BackingField_1() const { return ___U3CChallengeNameU3Ek__BackingField_1; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_U3CChallengeNameU3Ek__BackingField_1() { return &___U3CChallengeNameU3Ek__BackingField_1; }
	inline void set_U3CChallengeNameU3Ek__BackingField_1(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___U3CChallengeNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CChallengeNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43, ___U3CAuthenticationResultU3Ek__BackingField_2)); }
	inline AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * get_U3CAuthenticationResultU3Ek__BackingField_2() const { return ___U3CAuthenticationResultU3Ek__BackingField_2; }
	inline AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 ** get_address_of_U3CAuthenticationResultU3Ek__BackingField_2() { return &___U3CAuthenticationResultU3Ek__BackingField_2; }
	inline void set_U3CAuthenticationResultU3Ek__BackingField_2(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * value)
	{
		___U3CAuthenticationResultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAuthenticationResultU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CChallengeParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43, ___U3CChallengeParametersU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CChallengeParametersU3Ek__BackingField_3() const { return ___U3CChallengeParametersU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CChallengeParametersU3Ek__BackingField_3() { return &___U3CChallengeParametersU3Ek__BackingField_3; }
	inline void set_U3CChallengeParametersU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CChallengeParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CChallengeParametersU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CClientMetadataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43, ___U3CClientMetadataU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CClientMetadataU3Ek__BackingField_4() const { return ___U3CClientMetadataU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CClientMetadataU3Ek__BackingField_4() { return &___U3CClientMetadataU3Ek__BackingField_4; }
	inline void set_U3CClientMetadataU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CClientMetadataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClientMetadataU3Ek__BackingField_4), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions
struct BigIntegerExtensions_t42369B6C9BAEC4D6511AEB0D7A1920FD1354F66B  : public RuntimeObject
{
public:

public:
};


// Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper
struct CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29  : public RuntimeObject
{
public:

public:
};

struct CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_ThreadStaticFields
{
public:
	// System.Security.Cryptography.SHA256 Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::sha256
	SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * ___sha256_0;

public:
	inline static int32_t get_offset_of_sha256_0() { return static_cast<int32_t>(offsetof(CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_ThreadStaticFields, ___sha256_0)); }
	inline SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * get_sha256_0() const { return ___sha256_0; }
	inline SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 ** get_address_of_sha256_0() { return &___sha256_0; }
	inline void set_sha256_0(SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * value)
	{
		___sha256_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sha256_0), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants
struct CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3  : public RuntimeObject
{
public:

public:
};

struct CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamSrpA
	String_t* ___ChlgParamSrpA_0;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamSrpB
	String_t* ___ChlgParamSrpB_1;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamSecretHash
	String_t* ___ChlgParamSecretHash_2;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamUsername
	String_t* ___ChlgParamUsername_3;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamChallengeName
	String_t* ___ChlgParamChallengeName_4;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamSalt
	String_t* ___ChlgParamSalt_5;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamSecretBlock
	String_t* ___ChlgParamSecretBlock_6;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamUserIDSrp
	String_t* ___ChlgParamUserIDSrp_7;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamRefreshToken
	String_t* ___ChlgParamRefreshToken_8;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamPassSecretBlock
	String_t* ___ChlgParamPassSecretBlock_9;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamPassSignature
	String_t* ___ChlgParamPassSignature_10;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamTimestamp
	String_t* ___ChlgParamTimestamp_11;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamDeliveryDest
	String_t* ___ChlgParamDeliveryDest_12;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamDeliveryMed
	String_t* ___ChlgParamDeliveryMed_13;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamSmsMfaCode
	String_t* ___ChlgParamSmsMfaCode_14;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamDeviceKey
	String_t* ___ChlgParamDeviceKey_15;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamUserAttrs
	String_t* ___ChlgParamUserAttrs_16;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamRequiredAttrs
	String_t* ___ChlgParamRequiredAttrs_17;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamUserAttrPrefix
	String_t* ___ChlgParamUserAttrPrefix_18;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamNewPassword
	String_t* ___ChlgParamNewPassword_19;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::ChlgParamPassword
	String_t* ___ChlgParamPassword_20;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::UserAttrEmail
	String_t* ___UserAttrEmail_21;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::UserAttrPhoneNumber
	String_t* ___UserAttrPhoneNumber_22;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::DeviceAttrName
	String_t* ___DeviceAttrName_23;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::DeviceAttrRemembered
	String_t* ___DeviceAttrRemembered_24;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::DeviceAttrNotRemembered
	String_t* ___DeviceAttrNotRemembered_25;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::DeviceChlgParamSalt
	String_t* ___DeviceChlgParamSalt_26;
	// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::DeviceChlgParamVerifier
	String_t* ___DeviceChlgParamVerifier_27;

public:
	inline static int32_t get_offset_of_ChlgParamSrpA_0() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamSrpA_0)); }
	inline String_t* get_ChlgParamSrpA_0() const { return ___ChlgParamSrpA_0; }
	inline String_t** get_address_of_ChlgParamSrpA_0() { return &___ChlgParamSrpA_0; }
	inline void set_ChlgParamSrpA_0(String_t* value)
	{
		___ChlgParamSrpA_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamSrpA_0), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamSrpB_1() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamSrpB_1)); }
	inline String_t* get_ChlgParamSrpB_1() const { return ___ChlgParamSrpB_1; }
	inline String_t** get_address_of_ChlgParamSrpB_1() { return &___ChlgParamSrpB_1; }
	inline void set_ChlgParamSrpB_1(String_t* value)
	{
		___ChlgParamSrpB_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamSrpB_1), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamSecretHash_2() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamSecretHash_2)); }
	inline String_t* get_ChlgParamSecretHash_2() const { return ___ChlgParamSecretHash_2; }
	inline String_t** get_address_of_ChlgParamSecretHash_2() { return &___ChlgParamSecretHash_2; }
	inline void set_ChlgParamSecretHash_2(String_t* value)
	{
		___ChlgParamSecretHash_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamSecretHash_2), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamUsername_3() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamUsername_3)); }
	inline String_t* get_ChlgParamUsername_3() const { return ___ChlgParamUsername_3; }
	inline String_t** get_address_of_ChlgParamUsername_3() { return &___ChlgParamUsername_3; }
	inline void set_ChlgParamUsername_3(String_t* value)
	{
		___ChlgParamUsername_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamUsername_3), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamChallengeName_4() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamChallengeName_4)); }
	inline String_t* get_ChlgParamChallengeName_4() const { return ___ChlgParamChallengeName_4; }
	inline String_t** get_address_of_ChlgParamChallengeName_4() { return &___ChlgParamChallengeName_4; }
	inline void set_ChlgParamChallengeName_4(String_t* value)
	{
		___ChlgParamChallengeName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamChallengeName_4), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamSalt_5() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamSalt_5)); }
	inline String_t* get_ChlgParamSalt_5() const { return ___ChlgParamSalt_5; }
	inline String_t** get_address_of_ChlgParamSalt_5() { return &___ChlgParamSalt_5; }
	inline void set_ChlgParamSalt_5(String_t* value)
	{
		___ChlgParamSalt_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamSalt_5), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamSecretBlock_6() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamSecretBlock_6)); }
	inline String_t* get_ChlgParamSecretBlock_6() const { return ___ChlgParamSecretBlock_6; }
	inline String_t** get_address_of_ChlgParamSecretBlock_6() { return &___ChlgParamSecretBlock_6; }
	inline void set_ChlgParamSecretBlock_6(String_t* value)
	{
		___ChlgParamSecretBlock_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamSecretBlock_6), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamUserIDSrp_7() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamUserIDSrp_7)); }
	inline String_t* get_ChlgParamUserIDSrp_7() const { return ___ChlgParamUserIDSrp_7; }
	inline String_t** get_address_of_ChlgParamUserIDSrp_7() { return &___ChlgParamUserIDSrp_7; }
	inline void set_ChlgParamUserIDSrp_7(String_t* value)
	{
		___ChlgParamUserIDSrp_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamUserIDSrp_7), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamRefreshToken_8() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamRefreshToken_8)); }
	inline String_t* get_ChlgParamRefreshToken_8() const { return ___ChlgParamRefreshToken_8; }
	inline String_t** get_address_of_ChlgParamRefreshToken_8() { return &___ChlgParamRefreshToken_8; }
	inline void set_ChlgParamRefreshToken_8(String_t* value)
	{
		___ChlgParamRefreshToken_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamRefreshToken_8), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamPassSecretBlock_9() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamPassSecretBlock_9)); }
	inline String_t* get_ChlgParamPassSecretBlock_9() const { return ___ChlgParamPassSecretBlock_9; }
	inline String_t** get_address_of_ChlgParamPassSecretBlock_9() { return &___ChlgParamPassSecretBlock_9; }
	inline void set_ChlgParamPassSecretBlock_9(String_t* value)
	{
		___ChlgParamPassSecretBlock_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamPassSecretBlock_9), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamPassSignature_10() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamPassSignature_10)); }
	inline String_t* get_ChlgParamPassSignature_10() const { return ___ChlgParamPassSignature_10; }
	inline String_t** get_address_of_ChlgParamPassSignature_10() { return &___ChlgParamPassSignature_10; }
	inline void set_ChlgParamPassSignature_10(String_t* value)
	{
		___ChlgParamPassSignature_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamPassSignature_10), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamTimestamp_11() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamTimestamp_11)); }
	inline String_t* get_ChlgParamTimestamp_11() const { return ___ChlgParamTimestamp_11; }
	inline String_t** get_address_of_ChlgParamTimestamp_11() { return &___ChlgParamTimestamp_11; }
	inline void set_ChlgParamTimestamp_11(String_t* value)
	{
		___ChlgParamTimestamp_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamTimestamp_11), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamDeliveryDest_12() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamDeliveryDest_12)); }
	inline String_t* get_ChlgParamDeliveryDest_12() const { return ___ChlgParamDeliveryDest_12; }
	inline String_t** get_address_of_ChlgParamDeliveryDest_12() { return &___ChlgParamDeliveryDest_12; }
	inline void set_ChlgParamDeliveryDest_12(String_t* value)
	{
		___ChlgParamDeliveryDest_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamDeliveryDest_12), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamDeliveryMed_13() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamDeliveryMed_13)); }
	inline String_t* get_ChlgParamDeliveryMed_13() const { return ___ChlgParamDeliveryMed_13; }
	inline String_t** get_address_of_ChlgParamDeliveryMed_13() { return &___ChlgParamDeliveryMed_13; }
	inline void set_ChlgParamDeliveryMed_13(String_t* value)
	{
		___ChlgParamDeliveryMed_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamDeliveryMed_13), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamSmsMfaCode_14() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamSmsMfaCode_14)); }
	inline String_t* get_ChlgParamSmsMfaCode_14() const { return ___ChlgParamSmsMfaCode_14; }
	inline String_t** get_address_of_ChlgParamSmsMfaCode_14() { return &___ChlgParamSmsMfaCode_14; }
	inline void set_ChlgParamSmsMfaCode_14(String_t* value)
	{
		___ChlgParamSmsMfaCode_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamSmsMfaCode_14), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamDeviceKey_15() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamDeviceKey_15)); }
	inline String_t* get_ChlgParamDeviceKey_15() const { return ___ChlgParamDeviceKey_15; }
	inline String_t** get_address_of_ChlgParamDeviceKey_15() { return &___ChlgParamDeviceKey_15; }
	inline void set_ChlgParamDeviceKey_15(String_t* value)
	{
		___ChlgParamDeviceKey_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamDeviceKey_15), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamUserAttrs_16() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamUserAttrs_16)); }
	inline String_t* get_ChlgParamUserAttrs_16() const { return ___ChlgParamUserAttrs_16; }
	inline String_t** get_address_of_ChlgParamUserAttrs_16() { return &___ChlgParamUserAttrs_16; }
	inline void set_ChlgParamUserAttrs_16(String_t* value)
	{
		___ChlgParamUserAttrs_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamUserAttrs_16), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamRequiredAttrs_17() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamRequiredAttrs_17)); }
	inline String_t* get_ChlgParamRequiredAttrs_17() const { return ___ChlgParamRequiredAttrs_17; }
	inline String_t** get_address_of_ChlgParamRequiredAttrs_17() { return &___ChlgParamRequiredAttrs_17; }
	inline void set_ChlgParamRequiredAttrs_17(String_t* value)
	{
		___ChlgParamRequiredAttrs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamRequiredAttrs_17), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamUserAttrPrefix_18() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamUserAttrPrefix_18)); }
	inline String_t* get_ChlgParamUserAttrPrefix_18() const { return ___ChlgParamUserAttrPrefix_18; }
	inline String_t** get_address_of_ChlgParamUserAttrPrefix_18() { return &___ChlgParamUserAttrPrefix_18; }
	inline void set_ChlgParamUserAttrPrefix_18(String_t* value)
	{
		___ChlgParamUserAttrPrefix_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamUserAttrPrefix_18), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamNewPassword_19() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamNewPassword_19)); }
	inline String_t* get_ChlgParamNewPassword_19() const { return ___ChlgParamNewPassword_19; }
	inline String_t** get_address_of_ChlgParamNewPassword_19() { return &___ChlgParamNewPassword_19; }
	inline void set_ChlgParamNewPassword_19(String_t* value)
	{
		___ChlgParamNewPassword_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamNewPassword_19), (void*)value);
	}

	inline static int32_t get_offset_of_ChlgParamPassword_20() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___ChlgParamPassword_20)); }
	inline String_t* get_ChlgParamPassword_20() const { return ___ChlgParamPassword_20; }
	inline String_t** get_address_of_ChlgParamPassword_20() { return &___ChlgParamPassword_20; }
	inline void set_ChlgParamPassword_20(String_t* value)
	{
		___ChlgParamPassword_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChlgParamPassword_20), (void*)value);
	}

	inline static int32_t get_offset_of_UserAttrEmail_21() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___UserAttrEmail_21)); }
	inline String_t* get_UserAttrEmail_21() const { return ___UserAttrEmail_21; }
	inline String_t** get_address_of_UserAttrEmail_21() { return &___UserAttrEmail_21; }
	inline void set_UserAttrEmail_21(String_t* value)
	{
		___UserAttrEmail_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UserAttrEmail_21), (void*)value);
	}

	inline static int32_t get_offset_of_UserAttrPhoneNumber_22() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___UserAttrPhoneNumber_22)); }
	inline String_t* get_UserAttrPhoneNumber_22() const { return ___UserAttrPhoneNumber_22; }
	inline String_t** get_address_of_UserAttrPhoneNumber_22() { return &___UserAttrPhoneNumber_22; }
	inline void set_UserAttrPhoneNumber_22(String_t* value)
	{
		___UserAttrPhoneNumber_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UserAttrPhoneNumber_22), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceAttrName_23() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___DeviceAttrName_23)); }
	inline String_t* get_DeviceAttrName_23() const { return ___DeviceAttrName_23; }
	inline String_t** get_address_of_DeviceAttrName_23() { return &___DeviceAttrName_23; }
	inline void set_DeviceAttrName_23(String_t* value)
	{
		___DeviceAttrName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceAttrName_23), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceAttrRemembered_24() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___DeviceAttrRemembered_24)); }
	inline String_t* get_DeviceAttrRemembered_24() const { return ___DeviceAttrRemembered_24; }
	inline String_t** get_address_of_DeviceAttrRemembered_24() { return &___DeviceAttrRemembered_24; }
	inline void set_DeviceAttrRemembered_24(String_t* value)
	{
		___DeviceAttrRemembered_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceAttrRemembered_24), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceAttrNotRemembered_25() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___DeviceAttrNotRemembered_25)); }
	inline String_t* get_DeviceAttrNotRemembered_25() const { return ___DeviceAttrNotRemembered_25; }
	inline String_t** get_address_of_DeviceAttrNotRemembered_25() { return &___DeviceAttrNotRemembered_25; }
	inline void set_DeviceAttrNotRemembered_25(String_t* value)
	{
		___DeviceAttrNotRemembered_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceAttrNotRemembered_25), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceChlgParamSalt_26() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___DeviceChlgParamSalt_26)); }
	inline String_t* get_DeviceChlgParamSalt_26() const { return ___DeviceChlgParamSalt_26; }
	inline String_t** get_address_of_DeviceChlgParamSalt_26() { return &___DeviceChlgParamSalt_26; }
	inline void set_DeviceChlgParamSalt_26(String_t* value)
	{
		___DeviceChlgParamSalt_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceChlgParamSalt_26), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceChlgParamVerifier_27() { return static_cast<int32_t>(offsetof(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields, ___DeviceChlgParamVerifier_27)); }
	inline String_t* get_DeviceChlgParamVerifier_27() const { return ___DeviceChlgParamVerifier_27; }
	inline String_t** get_address_of_DeviceChlgParamVerifier_27() { return &___DeviceChlgParamVerifier_27; }
	inline void set_DeviceChlgParamVerifier_27(String_t* value)
	{
		___DeviceChlgParamVerifier_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceChlgParamVerifier_27), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.CognitoDevice
struct CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760  : public RuntimeObject
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoDevice::<DeviceKey>k__BackingField
	String_t* ___U3CDeviceKeyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CDeviceKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760, ___U3CDeviceKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CDeviceKeyU3Ek__BackingField_0() const { return ___U3CDeviceKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDeviceKeyU3Ek__BackingField_0() { return &___U3CDeviceKeyU3Ek__BackingField_0; }
	inline void set_U3CDeviceKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CDeviceKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDeviceKeyU3Ek__BackingField_0), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.CognitoUser
struct CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9  : public RuntimeObject
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<ClientSecret>k__BackingField
	String_t* ___U3CClientSecretU3Ek__BackingField_0;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<SecretHash>k__BackingField
	String_t* ___U3CSecretHashU3Ek__BackingField_1;
	// Amazon.Extensions.CognitoAuthentication.CognitoUserSession Amazon.Extensions.CognitoAuthentication.CognitoUser::<SessionTokens>k__BackingField
	CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * ___U3CSessionTokensU3Ek__BackingField_2;
	// Amazon.Extensions.CognitoAuthentication.CognitoDevice Amazon.Extensions.CognitoAuthentication.CognitoUser::<Device>k__BackingField
	CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * ___U3CDeviceU3Ek__BackingField_3;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<UserID>k__BackingField
	String_t* ___U3CUserIDU3Ek__BackingField_4;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<Username>k__BackingField
	String_t* ___U3CUsernameU3Ek__BackingField_5;
	// Amazon.Extensions.CognitoAuthentication.CognitoUserPool Amazon.Extensions.CognitoAuthentication.CognitoUser::<UserPool>k__BackingField
	CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * ___U3CUserPoolU3Ek__BackingField_6;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<ClientID>k__BackingField
	String_t* ___U3CClientIDU3Ek__BackingField_7;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<Status>k__BackingField
	String_t* ___U3CStatusU3Ek__BackingField_8;
	// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUser::<Provider>k__BackingField
	RuntimeObject* ___U3CProviderU3Ek__BackingField_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Extensions.CognitoAuthentication.CognitoUser::<Attributes>k__BackingField
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___U3CAttributesU3Ek__BackingField_10;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::<PoolName>k__BackingField
	String_t* ___U3CPoolNameU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CClientSecretU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CClientSecretU3Ek__BackingField_0)); }
	inline String_t* get_U3CClientSecretU3Ek__BackingField_0() const { return ___U3CClientSecretU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CClientSecretU3Ek__BackingField_0() { return &___U3CClientSecretU3Ek__BackingField_0; }
	inline void set_U3CClientSecretU3Ek__BackingField_0(String_t* value)
	{
		___U3CClientSecretU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClientSecretU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSecretHashU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CSecretHashU3Ek__BackingField_1)); }
	inline String_t* get_U3CSecretHashU3Ek__BackingField_1() const { return ___U3CSecretHashU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSecretHashU3Ek__BackingField_1() { return &___U3CSecretHashU3Ek__BackingField_1; }
	inline void set_U3CSecretHashU3Ek__BackingField_1(String_t* value)
	{
		___U3CSecretHashU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSecretHashU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSessionTokensU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CSessionTokensU3Ek__BackingField_2)); }
	inline CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * get_U3CSessionTokensU3Ek__BackingField_2() const { return ___U3CSessionTokensU3Ek__BackingField_2; }
	inline CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA ** get_address_of_U3CSessionTokensU3Ek__BackingField_2() { return &___U3CSessionTokensU3Ek__BackingField_2; }
	inline void set_U3CSessionTokensU3Ek__BackingField_2(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * value)
	{
		___U3CSessionTokensU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSessionTokensU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDeviceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CDeviceU3Ek__BackingField_3)); }
	inline CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * get_U3CDeviceU3Ek__BackingField_3() const { return ___U3CDeviceU3Ek__BackingField_3; }
	inline CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 ** get_address_of_U3CDeviceU3Ek__BackingField_3() { return &___U3CDeviceU3Ek__BackingField_3; }
	inline void set_U3CDeviceU3Ek__BackingField_3(CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * value)
	{
		___U3CDeviceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDeviceU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUserIDU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CUserIDU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIDU3Ek__BackingField_4() const { return ___U3CUserIDU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIDU3Ek__BackingField_4() { return &___U3CUserIDU3Ek__BackingField_4; }
	inline void set_U3CUserIDU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIDU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserIDU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUsernameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CUsernameU3Ek__BackingField_5)); }
	inline String_t* get_U3CUsernameU3Ek__BackingField_5() const { return ___U3CUsernameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CUsernameU3Ek__BackingField_5() { return &___U3CUsernameU3Ek__BackingField_5; }
	inline void set_U3CUsernameU3Ek__BackingField_5(String_t* value)
	{
		___U3CUsernameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUsernameU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUserPoolU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CUserPoolU3Ek__BackingField_6)); }
	inline CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * get_U3CUserPoolU3Ek__BackingField_6() const { return ___U3CUserPoolU3Ek__BackingField_6; }
	inline CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 ** get_address_of_U3CUserPoolU3Ek__BackingField_6() { return &___U3CUserPoolU3Ek__BackingField_6; }
	inline void set_U3CUserPoolU3Ek__BackingField_6(CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * value)
	{
		___U3CUserPoolU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserPoolU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CClientIDU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CClientIDU3Ek__BackingField_7)); }
	inline String_t* get_U3CClientIDU3Ek__BackingField_7() const { return ___U3CClientIDU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CClientIDU3Ek__BackingField_7() { return &___U3CClientIDU3Ek__BackingField_7; }
	inline void set_U3CClientIDU3Ek__BackingField_7(String_t* value)
	{
		___U3CClientIDU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClientIDU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CStatusU3Ek__BackingField_8)); }
	inline String_t* get_U3CStatusU3Ek__BackingField_8() const { return ___U3CStatusU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CStatusU3Ek__BackingField_8() { return &___U3CStatusU3Ek__BackingField_8; }
	inline void set_U3CStatusU3Ek__BackingField_8(String_t* value)
	{
		___U3CStatusU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStatusU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CProviderU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CProviderU3Ek__BackingField_9() const { return ___U3CProviderU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CProviderU3Ek__BackingField_9() { return &___U3CProviderU3Ek__BackingField_9; }
	inline void set_U3CProviderU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CProviderU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CProviderU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAttributesU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CAttributesU3Ek__BackingField_10)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_U3CAttributesU3Ek__BackingField_10() const { return ___U3CAttributesU3Ek__BackingField_10; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_U3CAttributesU3Ek__BackingField_10() { return &___U3CAttributesU3Ek__BackingField_10; }
	inline void set_U3CAttributesU3Ek__BackingField_10(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___U3CAttributesU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAttributesU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPoolNameU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9, ___U3CPoolNameU3Ek__BackingField_11)); }
	inline String_t* get_U3CPoolNameU3Ek__BackingField_11() const { return ___U3CPoolNameU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CPoolNameU3Ek__BackingField_11() { return &___U3CPoolNameU3Ek__BackingField_11; }
	inline void set_U3CPoolNameU3Ek__BackingField_11(String_t* value)
	{
		___U3CPoolNameU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPoolNameU3Ek__BackingField_11), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.CognitoUserPool
struct CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665  : public RuntimeObject
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserPool::<PoolID>k__BackingField
	String_t* ___U3CPoolIDU3Ek__BackingField_0;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserPool::<ClientID>k__BackingField
	String_t* ___U3CClientIDU3Ek__BackingField_1;
	// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUserPool::<Provider>k__BackingField
	RuntimeObject* ___U3CProviderU3Ek__BackingField_2;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserPool::<ClientSecret>k__BackingField
	String_t* ___U3CClientSecretU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPoolIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665, ___U3CPoolIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CPoolIDU3Ek__BackingField_0() const { return ___U3CPoolIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPoolIDU3Ek__BackingField_0() { return &___U3CPoolIDU3Ek__BackingField_0; }
	inline void set_U3CPoolIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CPoolIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPoolIDU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CClientIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665, ___U3CClientIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CClientIDU3Ek__BackingField_1() const { return ___U3CClientIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CClientIDU3Ek__BackingField_1() { return &___U3CClientIDU3Ek__BackingField_1; }
	inline void set_U3CClientIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CClientIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClientIDU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665, ___U3CProviderU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CProviderU3Ek__BackingField_2() const { return ___U3CProviderU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CProviderU3Ek__BackingField_2() { return &___U3CProviderU3Ek__BackingField_2; }
	inline void set_U3CProviderU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CProviderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CProviderU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CClientSecretU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665, ___U3CClientSecretU3Ek__BackingField_3)); }
	inline String_t* get_U3CClientSecretU3Ek__BackingField_3() const { return ___U3CClientSecretU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CClientSecretU3Ek__BackingField_3() { return &___U3CClientSecretU3Ek__BackingField_3; }
	inline void set_U3CClientSecretU3Ek__BackingField_3(String_t* value)
	{
		___U3CClientSecretU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClientSecretU3Ek__BackingField_3), (void*)value);
	}
};


// Amazon.Runtime.ConstantClass
struct ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ConstantClass::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6, ___U3CValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueU3Ek__BackingField_2), (void*)value);
	}
};

struct ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_StaticFields
{
public:
	// System.Object Amazon.Runtime.ConstantClass::staticFieldsLock
	RuntimeObject * ___staticFieldsLock_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>> Amazon.Runtime.ConstantClass::staticFields
	Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB * ___staticFields_1;

public:
	inline static int32_t get_offset_of_staticFieldsLock_0() { return static_cast<int32_t>(offsetof(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_StaticFields, ___staticFieldsLock_0)); }
	inline RuntimeObject * get_staticFieldsLock_0() const { return ___staticFieldsLock_0; }
	inline RuntimeObject ** get_address_of_staticFieldsLock_0() { return &___staticFieldsLock_0; }
	inline void set_staticFieldsLock_0(RuntimeObject * value)
	{
		___staticFieldsLock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticFieldsLock_0), (void*)value);
	}

	inline static int32_t get_offset_of_staticFields_1() { return static_cast<int32_t>(offsetof(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_StaticFields, ___staticFields_1)); }
	inline Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB * get_staticFields_1() const { return ___staticFields_1; }
	inline Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB ** get_address_of_staticFields_1() { return &___staticFields_1; }
	inline void set_staticFields_1(Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB * value)
	{
		___staticFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticFields_1), (void*)value);
	}
};


// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___numInfo_10)); }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numInfo_10), (void*)value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dateTimeInfo_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textInfo_12)); }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_13), (void*)value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___englishname_14), (void*)value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativename_15), (void*)value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso3lang_16), (void*)value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso2lang_17), (void*)value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___win3lang_18), (void*)value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___territory_19), (void*)value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___native_calendar_names_20)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_calendar_names_20), (void*)value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___compareInfo_21)); }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compareInfo_21), (void*)value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___calendar_24)); }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calendar_24), (void*)value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_culture_25)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_culture_25), (void*)value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cached_serialized_form_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_cultureData_28)); }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cultureData_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariant_culture_info_0), (void*)value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_table_lock_1), (void*)value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_current_culture_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentUICulture_33), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentCulture_34), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_number_35), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_name_36), (void*)value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_55;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * ___dataItem_56;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_57;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_58;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * ___encoderFallback_59;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * ___decoderFallback_60;

public:
	inline static int32_t get_offset_of_m_codePage_55() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_codePage_55)); }
	inline int32_t get_m_codePage_55() const { return ___m_codePage_55; }
	inline int32_t* get_address_of_m_codePage_55() { return &___m_codePage_55; }
	inline void set_m_codePage_55(int32_t value)
	{
		___m_codePage_55 = value;
	}

	inline static int32_t get_offset_of_dataItem_56() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___dataItem_56)); }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * get_dataItem_56() const { return ___dataItem_56; }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E ** get_address_of_dataItem_56() { return &___dataItem_56; }
	inline void set_dataItem_56(CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * value)
	{
		___dataItem_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_56), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_57() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_deserializedFromEverett_57)); }
	inline bool get_m_deserializedFromEverett_57() const { return ___m_deserializedFromEverett_57; }
	inline bool* get_address_of_m_deserializedFromEverett_57() { return &___m_deserializedFromEverett_57; }
	inline void set_m_deserializedFromEverett_57(bool value)
	{
		___m_deserializedFromEverett_57 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_58() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_isReadOnly_58)); }
	inline bool get_m_isReadOnly_58() const { return ___m_isReadOnly_58; }
	inline bool* get_address_of_m_isReadOnly_58() { return &___m_isReadOnly_58; }
	inline void set_m_isReadOnly_58(bool value)
	{
		___m_isReadOnly_58 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_59() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___encoderFallback_59)); }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * get_encoderFallback_59() const { return ___encoderFallback_59; }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 ** get_address_of_encoderFallback_59() { return &___encoderFallback_59; }
	inline void set_encoderFallback_59(EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * value)
	{
		___encoderFallback_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_59), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_60() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___decoderFallback_60)); }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * get_decoderFallback_60() const { return ___decoderFallback_60; }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D ** get_address_of_decoderFallback_60() { return &___decoderFallback_60; }
	inline void set_decoderFallback_60(DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * value)
	{
		___decoderFallback_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_60), (void*)value);
	}
};

struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_61;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___encodings_8)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_61() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___s_InternalSyncObject_61)); }
	inline RuntimeObject * get_s_InternalSyncObject_61() const { return ___s_InternalSyncObject_61; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_61() { return &___s_InternalSyncObject_61; }
	inline void set_s_InternalSyncObject_61(RuntimeObject * value)
	{
		___s_InternalSyncObject_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_61), (void*)value);
	}
};


// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields, ___Empty_0)); }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31, ___HashValue_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HashValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};


// Amazon.Extensions.CognitoAuthentication.HkdfSha256
struct HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F  : public RuntimeObject
{
public:
	// System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::<Prk>k__BackingField
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3CPrkU3Ek__BackingField_0;
	// System.Security.Cryptography.HMACSHA256 Amazon.Extensions.CognitoAuthentication.HkdfSha256::<HmacSha256>k__BackingField
	HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * ___U3CHmacSha256U3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPrkU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F, ___U3CPrkU3Ek__BackingField_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3CPrkU3Ek__BackingField_0() const { return ___U3CPrkU3Ek__BackingField_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3CPrkU3Ek__BackingField_0() { return &___U3CPrkU3Ek__BackingField_0; }
	inline void set_U3CPrkU3Ek__BackingField_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3CPrkU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPrkU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CHmacSha256U3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F, ___U3CHmacSha256U3Ek__BackingField_1)); }
	inline HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * get_U3CHmacSha256U3Ek__BackingField_1() const { return ___U3CHmacSha256U3Ek__BackingField_1; }
	inline HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD ** get_address_of_U3CHmacSha256U3Ek__BackingField_1() { return &___U3CHmacSha256U3Ek__BackingField_1; }
	inline void set_U3CHmacSha256U3Ek__BackingField_1(HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * value)
	{
		___U3CHmacSha256U3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHmacSha256U3Ek__BackingField_1), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest
struct InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667  : public RuntimeObject
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667, ___U3CPasswordU3Ek__BackingField_0)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_0() const { return ___U3CPasswordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_0() { return &___U3CPasswordU3Ek__BackingField_0; }
	inline void set_U3CPasswordU3Ek__BackingField_0(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPasswordU3Ek__BackingField_0), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50  : public RuntimeObject
{
public:

public:
};


// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ResponseMetadata::requestIdField
	String_t* ___requestIdField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.ResponseMetadata::_metadata
	RuntimeObject* ____metadata_1;

public:
	inline static int32_t get_offset_of_requestIdField_0() { return static_cast<int32_t>(offsetof(ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4, ___requestIdField_0)); }
	inline String_t* get_requestIdField_0() const { return ___requestIdField_0; }
	inline String_t** get_address_of_requestIdField_0() { return &___requestIdField_0; }
	inline void set_requestIdField_0(String_t* value)
	{
		___requestIdField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___requestIdField_0), (void*)value);
	}

	inline static int32_t get_offset_of__metadata_1() { return static_cast<int32_t>(offsetof(ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4, ____metadata_1)); }
	inline RuntimeObject* get__metadata_1() const { return ____metadata_1; }
	inline RuntimeObject** get_address_of__metadata_1() { return &____metadata_1; }
	inline void set__metadata_1(RuntimeObject* value)
	{
		____metadata_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____metadata_1), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.StringComparer
struct StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6  : public RuntimeObject
{
public:

public:
};

struct StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_StaticFields
{
public:
	// System.StringComparer System.StringComparer::_invariantCulture
	StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * ____invariantCulture_0;
	// System.StringComparer System.StringComparer::_invariantCultureIgnoreCase
	StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * ____invariantCultureIgnoreCase_1;
	// System.StringComparer System.StringComparer::_ordinal
	StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * ____ordinal_2;
	// System.StringComparer System.StringComparer::_ordinalIgnoreCase
	StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * ____ordinalIgnoreCase_3;

public:
	inline static int32_t get_offset_of__invariantCulture_0() { return static_cast<int32_t>(offsetof(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_StaticFields, ____invariantCulture_0)); }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * get__invariantCulture_0() const { return ____invariantCulture_0; }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 ** get_address_of__invariantCulture_0() { return &____invariantCulture_0; }
	inline void set__invariantCulture_0(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * value)
	{
		____invariantCulture_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____invariantCulture_0), (void*)value);
	}

	inline static int32_t get_offset_of__invariantCultureIgnoreCase_1() { return static_cast<int32_t>(offsetof(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_StaticFields, ____invariantCultureIgnoreCase_1)); }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * get__invariantCultureIgnoreCase_1() const { return ____invariantCultureIgnoreCase_1; }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 ** get_address_of__invariantCultureIgnoreCase_1() { return &____invariantCultureIgnoreCase_1; }
	inline void set__invariantCultureIgnoreCase_1(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * value)
	{
		____invariantCultureIgnoreCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____invariantCultureIgnoreCase_1), (void*)value);
	}

	inline static int32_t get_offset_of__ordinal_2() { return static_cast<int32_t>(offsetof(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_StaticFields, ____ordinal_2)); }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * get__ordinal_2() const { return ____ordinal_2; }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 ** get_address_of__ordinal_2() { return &____ordinal_2; }
	inline void set__ordinal_2(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * value)
	{
		____ordinal_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ordinal_2), (void*)value);
	}

	inline static int32_t get_offset_of__ordinalIgnoreCase_3() { return static_cast<int32_t>(offsetof(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_StaticFields, ____ordinalIgnoreCase_3)); }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * get__ordinalIgnoreCase_3() const { return ____ordinalIgnoreCase_3; }
	inline StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 ** get_address_of__ordinalIgnoreCase_3() { return &____ordinalIgnoreCase_3; }
	inline void set__ordinalIgnoreCase_3(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * value)
	{
		____ordinalIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ordinalIgnoreCase_3), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>
struct ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0, ___m_task_0)); }
	inline Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>
struct ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED, ___m_task_0)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>
struct ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C, ___m_task_0)); }
	inline Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};


// System.Nullable`1<System.Int32>
struct Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient
struct AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87  : public AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB
{
public:

public:
};

struct AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_StaticFields
{
public:
	// Amazon.Runtime.Internal.IServiceMetadata Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient::serviceMetadata
	RuntimeObject* ___serviceMetadata_12;

public:
	inline static int32_t get_offset_of_serviceMetadata_12() { return static_cast<int32_t>(offsetof(AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_StaticFields, ___serviceMetadata_12)); }
	inline RuntimeObject* get_serviceMetadata_12() const { return ___serviceMetadata_12; }
	inline RuntimeObject** get_address_of_serviceMetadata_12() { return &___serviceMetadata_12; }
	inline void set_serviceMetadata_12(RuntimeObject* value)
	{
		___serviceMetadata_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serviceMetadata_12), (void*)value);
	}
};


// Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderRequest
struct AmazonCognitoIdentityProviderRequest_t295E7DC8F0100D19CF1521860058585BEE75F51C  : public AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55
{
public:

public:
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_defaultContextAction_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// Amazon.CognitoIdentityProvider.AuthFlowType
struct AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834  : public ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6
{
public:

public:
};

struct AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields
{
public:
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::ADMIN_NO_SRP_AUTH
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___ADMIN_NO_SRP_AUTH_3;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::ADMIN_USER_PASSWORD_AUTH
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___ADMIN_USER_PASSWORD_AUTH_4;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::CUSTOM_AUTH
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___CUSTOM_AUTH_5;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::REFRESH_TOKEN
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___REFRESH_TOKEN_6;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::REFRESH_TOKEN_AUTH
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___REFRESH_TOKEN_AUTH_7;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::USER_PASSWORD_AUTH
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___USER_PASSWORD_AUTH_8;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.AuthFlowType::USER_SRP_AUTH
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___USER_SRP_AUTH_9;

public:
	inline static int32_t get_offset_of_ADMIN_NO_SRP_AUTH_3() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___ADMIN_NO_SRP_AUTH_3)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_ADMIN_NO_SRP_AUTH_3() const { return ___ADMIN_NO_SRP_AUTH_3; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_ADMIN_NO_SRP_AUTH_3() { return &___ADMIN_NO_SRP_AUTH_3; }
	inline void set_ADMIN_NO_SRP_AUTH_3(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___ADMIN_NO_SRP_AUTH_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ADMIN_NO_SRP_AUTH_3), (void*)value);
	}

	inline static int32_t get_offset_of_ADMIN_USER_PASSWORD_AUTH_4() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___ADMIN_USER_PASSWORD_AUTH_4)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_ADMIN_USER_PASSWORD_AUTH_4() const { return ___ADMIN_USER_PASSWORD_AUTH_4; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_ADMIN_USER_PASSWORD_AUTH_4() { return &___ADMIN_USER_PASSWORD_AUTH_4; }
	inline void set_ADMIN_USER_PASSWORD_AUTH_4(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___ADMIN_USER_PASSWORD_AUTH_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ADMIN_USER_PASSWORD_AUTH_4), (void*)value);
	}

	inline static int32_t get_offset_of_CUSTOM_AUTH_5() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___CUSTOM_AUTH_5)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_CUSTOM_AUTH_5() const { return ___CUSTOM_AUTH_5; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_CUSTOM_AUTH_5() { return &___CUSTOM_AUTH_5; }
	inline void set_CUSTOM_AUTH_5(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___CUSTOM_AUTH_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CUSTOM_AUTH_5), (void*)value);
	}

	inline static int32_t get_offset_of_REFRESH_TOKEN_6() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___REFRESH_TOKEN_6)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_REFRESH_TOKEN_6() const { return ___REFRESH_TOKEN_6; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_REFRESH_TOKEN_6() { return &___REFRESH_TOKEN_6; }
	inline void set_REFRESH_TOKEN_6(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___REFRESH_TOKEN_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___REFRESH_TOKEN_6), (void*)value);
	}

	inline static int32_t get_offset_of_REFRESH_TOKEN_AUTH_7() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___REFRESH_TOKEN_AUTH_7)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_REFRESH_TOKEN_AUTH_7() const { return ___REFRESH_TOKEN_AUTH_7; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_REFRESH_TOKEN_AUTH_7() { return &___REFRESH_TOKEN_AUTH_7; }
	inline void set_REFRESH_TOKEN_AUTH_7(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___REFRESH_TOKEN_AUTH_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___REFRESH_TOKEN_AUTH_7), (void*)value);
	}

	inline static int32_t get_offset_of_USER_PASSWORD_AUTH_8() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___USER_PASSWORD_AUTH_8)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_USER_PASSWORD_AUTH_8() const { return ___USER_PASSWORD_AUTH_8; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_USER_PASSWORD_AUTH_8() { return &___USER_PASSWORD_AUTH_8; }
	inline void set_USER_PASSWORD_AUTH_8(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___USER_PASSWORD_AUTH_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USER_PASSWORD_AUTH_8), (void*)value);
	}

	inline static int32_t get_offset_of_USER_SRP_AUTH_9() { return static_cast<int32_t>(offsetof(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields, ___USER_SRP_AUTH_9)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get_USER_SRP_AUTH_9() const { return ___USER_SRP_AUTH_9; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of_USER_SRP_AUTH_9() { return &___USER_SRP_AUTH_9; }
	inline void set_USER_SRP_AUTH_9(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		___USER_SRP_AUTH_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USER_SRP_AUTH_9), (void*)value);
	}
};


// System.Numerics.BigInteger
struct BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 
{
public:
	// System.Int32 System.Numerics.BigInteger::_sign
	int32_t ____sign_0;
	// System.UInt32[] System.Numerics.BigInteger::_bits
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ____bits_1;

public:
	inline static int32_t get_offset_of__sign_0() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2, ____sign_0)); }
	inline int32_t get__sign_0() const { return ____sign_0; }
	inline int32_t* get_address_of__sign_0() { return &____sign_0; }
	inline void set__sign_0(int32_t value)
	{
		____sign_0 = value;
	}

	inline static int32_t get_offset_of__bits_1() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2, ____bits_1)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get__bits_1() const { return ____bits_1; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of__bits_1() { return &____bits_1; }
	inline void set__bits_1(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		____bits_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bits_1), (void*)value);
	}
};

struct BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields
{
public:
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnMinInt
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___s_bnMinInt_2;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnOneInt
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___s_bnOneInt_3;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnZeroInt
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___s_bnZeroInt_4;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnMinusOneInt
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___s_bnMinusOneInt_5;
	// System.Byte[] System.Numerics.BigInteger::s_success
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___s_success_6;

public:
	inline static int32_t get_offset_of_s_bnMinInt_2() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields, ___s_bnMinInt_2)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_s_bnMinInt_2() const { return ___s_bnMinInt_2; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_s_bnMinInt_2() { return &___s_bnMinInt_2; }
	inline void set_s_bnMinInt_2(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___s_bnMinInt_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_bnMinInt_2))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_s_bnOneInt_3() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields, ___s_bnOneInt_3)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_s_bnOneInt_3() const { return ___s_bnOneInt_3; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_s_bnOneInt_3() { return &___s_bnOneInt_3; }
	inline void set_s_bnOneInt_3(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___s_bnOneInt_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_bnOneInt_3))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_s_bnZeroInt_4() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields, ___s_bnZeroInt_4)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_s_bnZeroInt_4() const { return ___s_bnZeroInt_4; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_s_bnZeroInt_4() { return &___s_bnZeroInt_4; }
	inline void set_s_bnZeroInt_4(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___s_bnZeroInt_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_bnZeroInt_4))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_s_bnMinusOneInt_5() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields, ___s_bnMinusOneInt_5)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_s_bnMinusOneInt_5() const { return ___s_bnMinusOneInt_5; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_s_bnMinusOneInt_5() { return &___s_bnMinusOneInt_5; }
	inline void set_s_bnMinusOneInt_5(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___s_bnMinusOneInt_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_bnMinusOneInt_5))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_s_success_6() { return static_cast<int32_t>(offsetof(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields, ___s_success_6)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_s_success_6() const { return ___s_success_6; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_s_success_6() { return &___s_success_6; }
	inline void set_s_success_6(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___s_success_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_success_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Numerics.BigInteger
struct BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_marshaled_pinvoke
{
	int32_t ____sign_0;
	Il2CppSafeArray/*NONE*/* ____bits_1;
};
// Native definition for COM marshalling of System.Numerics.BigInteger
struct BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_marshaled_com
{
	int32_t ____sign_0;
	Il2CppSafeArray/*NONE*/* ____bits_1;
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD, ___m_source_0)); }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_source_0), (void*)value);
	}
};

struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActionToActionObjShunt_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_pinvoke
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_com
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};

// Amazon.CognitoIdentityProvider.ChallengeNameType
struct ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2  : public ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6
{
public:

public:
};

struct ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields
{
public:
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::ADMIN_NO_SRP_AUTH
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___ADMIN_NO_SRP_AUTH_3;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::CUSTOM_CHALLENGE
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___CUSTOM_CHALLENGE_4;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::DEVICE_PASSWORD_VERIFIER
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___DEVICE_PASSWORD_VERIFIER_5;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::DEVICE_SRP_AUTH
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___DEVICE_SRP_AUTH_6;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::MFA_SETUP
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___MFA_SETUP_7;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::NEW_PASSWORD_REQUIRED
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___NEW_PASSWORD_REQUIRED_8;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::PASSWORD_VERIFIER
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___PASSWORD_VERIFIER_9;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::SELECT_MFA_TYPE
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___SELECT_MFA_TYPE_10;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::SMS_MFA
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___SMS_MFA_11;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.ChallengeNameType::SOFTWARE_TOKEN_MFA
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___SOFTWARE_TOKEN_MFA_12;

public:
	inline static int32_t get_offset_of_ADMIN_NO_SRP_AUTH_3() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___ADMIN_NO_SRP_AUTH_3)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_ADMIN_NO_SRP_AUTH_3() const { return ___ADMIN_NO_SRP_AUTH_3; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_ADMIN_NO_SRP_AUTH_3() { return &___ADMIN_NO_SRP_AUTH_3; }
	inline void set_ADMIN_NO_SRP_AUTH_3(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___ADMIN_NO_SRP_AUTH_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ADMIN_NO_SRP_AUTH_3), (void*)value);
	}

	inline static int32_t get_offset_of_CUSTOM_CHALLENGE_4() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___CUSTOM_CHALLENGE_4)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_CUSTOM_CHALLENGE_4() const { return ___CUSTOM_CHALLENGE_4; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_CUSTOM_CHALLENGE_4() { return &___CUSTOM_CHALLENGE_4; }
	inline void set_CUSTOM_CHALLENGE_4(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___CUSTOM_CHALLENGE_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CUSTOM_CHALLENGE_4), (void*)value);
	}

	inline static int32_t get_offset_of_DEVICE_PASSWORD_VERIFIER_5() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___DEVICE_PASSWORD_VERIFIER_5)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_DEVICE_PASSWORD_VERIFIER_5() const { return ___DEVICE_PASSWORD_VERIFIER_5; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_DEVICE_PASSWORD_VERIFIER_5() { return &___DEVICE_PASSWORD_VERIFIER_5; }
	inline void set_DEVICE_PASSWORD_VERIFIER_5(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___DEVICE_PASSWORD_VERIFIER_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DEVICE_PASSWORD_VERIFIER_5), (void*)value);
	}

	inline static int32_t get_offset_of_DEVICE_SRP_AUTH_6() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___DEVICE_SRP_AUTH_6)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_DEVICE_SRP_AUTH_6() const { return ___DEVICE_SRP_AUTH_6; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_DEVICE_SRP_AUTH_6() { return &___DEVICE_SRP_AUTH_6; }
	inline void set_DEVICE_SRP_AUTH_6(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___DEVICE_SRP_AUTH_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DEVICE_SRP_AUTH_6), (void*)value);
	}

	inline static int32_t get_offset_of_MFA_SETUP_7() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___MFA_SETUP_7)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_MFA_SETUP_7() const { return ___MFA_SETUP_7; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_MFA_SETUP_7() { return &___MFA_SETUP_7; }
	inline void set_MFA_SETUP_7(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___MFA_SETUP_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MFA_SETUP_7), (void*)value);
	}

	inline static int32_t get_offset_of_NEW_PASSWORD_REQUIRED_8() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___NEW_PASSWORD_REQUIRED_8)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_NEW_PASSWORD_REQUIRED_8() const { return ___NEW_PASSWORD_REQUIRED_8; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_NEW_PASSWORD_REQUIRED_8() { return &___NEW_PASSWORD_REQUIRED_8; }
	inline void set_NEW_PASSWORD_REQUIRED_8(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___NEW_PASSWORD_REQUIRED_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NEW_PASSWORD_REQUIRED_8), (void*)value);
	}

	inline static int32_t get_offset_of_PASSWORD_VERIFIER_9() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___PASSWORD_VERIFIER_9)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_PASSWORD_VERIFIER_9() const { return ___PASSWORD_VERIFIER_9; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_PASSWORD_VERIFIER_9() { return &___PASSWORD_VERIFIER_9; }
	inline void set_PASSWORD_VERIFIER_9(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___PASSWORD_VERIFIER_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PASSWORD_VERIFIER_9), (void*)value);
	}

	inline static int32_t get_offset_of_SELECT_MFA_TYPE_10() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___SELECT_MFA_TYPE_10)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_SELECT_MFA_TYPE_10() const { return ___SELECT_MFA_TYPE_10; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_SELECT_MFA_TYPE_10() { return &___SELECT_MFA_TYPE_10; }
	inline void set_SELECT_MFA_TYPE_10(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___SELECT_MFA_TYPE_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SELECT_MFA_TYPE_10), (void*)value);
	}

	inline static int32_t get_offset_of_SMS_MFA_11() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___SMS_MFA_11)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_SMS_MFA_11() const { return ___SMS_MFA_11; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_SMS_MFA_11() { return &___SMS_MFA_11; }
	inline void set_SMS_MFA_11(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___SMS_MFA_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SMS_MFA_11), (void*)value);
	}

	inline static int32_t get_offset_of_SOFTWARE_TOKEN_MFA_12() { return static_cast<int32_t>(offsetof(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2_StaticFields, ___SOFTWARE_TOKEN_MFA_12)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get_SOFTWARE_TOKEN_MFA_12() const { return ___SOFTWARE_TOKEN_MFA_12; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of_SOFTWARE_TOKEN_MFA_12() { return &___SOFTWARE_TOKEN_MFA_12; }
	inline void set_SOFTWARE_TOKEN_MFA_12(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		___SOFTWARE_TOKEN_MFA_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SOFTWARE_TOKEN_MFA_12), (void*)value);
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t681B59FF58ABCA45D1694A36390AF83AAE2F7F08  : public HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31
{
public:
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___KeyValue_4;

public:
	inline static int32_t get_offset_of_KeyValue_4() { return static_cast<int32_t>(offsetof(KeyedHashAlgorithm_t681B59FF58ABCA45D1694A36390AF83AAE2F7F08, ___KeyValue_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_KeyValue_4() const { return ___KeyValue_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_KeyValue_4() { return &___KeyValue_4; }
	inline void set_KeyValue_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___KeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___KeyValue_4), (void*)value);
	}
};


// Amazon.Runtime.RequestEventArgs
struct RequestEventArgs_tE1FA7940E62EB2306034D1E8ABD47B2D8ED44A56  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:

public:
};


// System.Security.Cryptography.SHA256
struct SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452  : public HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>
struct AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E, ___m_task_2)); }
	inline Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

struct AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_defaultResultTask_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>
struct AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020, ___m_task_2)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

struct AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_defaultResultTask_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>
struct ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC 
{
public:
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1::m_configuredTaskAwaiter
	ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  ___m_configuredTaskAwaiter_0;

public:
	inline static int32_t get_offset_of_m_configuredTaskAwaiter_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC, ___m_configuredTaskAwaiter_0)); }
	inline ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  get_m_configuredTaskAwaiter_0() const { return ___m_configuredTaskAwaiter_0; }
	inline ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 * get_address_of_m_configuredTaskAwaiter_0() { return &___m_configuredTaskAwaiter_0; }
	inline void set_m_configuredTaskAwaiter_0(ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  value)
	{
		___m_configuredTaskAwaiter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_configuredTaskAwaiter_0))->___m_task_0), (void*)NULL);
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>
struct ConfiguredTaskAwaitable_1_t226372B9DEDA3AA0FC1B43D6C03CEC9111045F18 
{
public:
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1::m_configuredTaskAwaiter
	ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED  ___m_configuredTaskAwaiter_0;

public:
	inline static int32_t get_offset_of_m_configuredTaskAwaiter_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaitable_1_t226372B9DEDA3AA0FC1B43D6C03CEC9111045F18, ___m_configuredTaskAwaiter_0)); }
	inline ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED  get_m_configuredTaskAwaiter_0() const { return ___m_configuredTaskAwaiter_0; }
	inline ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * get_address_of_m_configuredTaskAwaiter_0() { return &___m_configuredTaskAwaiter_0; }
	inline void set_m_configuredTaskAwaiter_0(ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED  value)
	{
		___m_configuredTaskAwaiter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_configuredTaskAwaiter_0))->___m_task_0), (void*)NULL);
	}
};


// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>
struct ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB 
{
public:
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1::m_configuredTaskAwaiter
	ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  ___m_configuredTaskAwaiter_0;

public:
	inline static int32_t get_offset_of_m_configuredTaskAwaiter_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB, ___m_configuredTaskAwaiter_0)); }
	inline ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  get_m_configuredTaskAwaiter_0() const { return ___m_configuredTaskAwaiter_0; }
	inline ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C * get_address_of_m_configuredTaskAwaiter_0() { return &___m_configuredTaskAwaiter_0; }
	inline void set_m_configuredTaskAwaiter_0(ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  value)
	{
		___m_configuredTaskAwaiter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_configuredTaskAwaiter_0))->___m_task_0), (void*)NULL);
	}
};


// System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>
struct Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17  : public RuntimeObject
{
public:
	// T1 System.Tuple`2::m_Item1
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___m_Item1_0;
	// T2 System.Tuple`2::m_Item2
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___m_Item2_1;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17, ___m_Item1_0)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_m_Item1_0() const { return ___m_Item1_0; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Item1_0))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17, ___m_Item2_1)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_m_Item2_1() const { return ___m_Item2_1; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Item2_1))->____bits_1), (void*)NULL);
	}
};


// System.Reflection.Assembly
struct Assembly_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Assembly::_mono_assembly
	intptr_t ____mono_assembly_0;
	// System.Reflection.Assembly/ResolveEventHolder System.Reflection.Assembly::resolve_event_holder
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	// System.Object System.Reflection.Assembly::_evidence
	RuntimeObject * ____evidence_2;
	// System.Object System.Reflection.Assembly::_minimum
	RuntimeObject * ____minimum_3;
	// System.Object System.Reflection.Assembly::_optional
	RuntimeObject * ____optional_4;
	// System.Object System.Reflection.Assembly::_refuse
	RuntimeObject * ____refuse_5;
	// System.Object System.Reflection.Assembly::_granted
	RuntimeObject * ____granted_6;
	// System.Object System.Reflection.Assembly::_denied
	RuntimeObject * ____denied_7;
	// System.Boolean System.Reflection.Assembly::fromByteArray
	bool ___fromByteArray_8;
	// System.String System.Reflection.Assembly::assemblyName
	String_t* ___assemblyName_9;

public:
	inline static int32_t get_offset_of__mono_assembly_0() { return static_cast<int32_t>(offsetof(Assembly_t, ____mono_assembly_0)); }
	inline intptr_t get__mono_assembly_0() const { return ____mono_assembly_0; }
	inline intptr_t* get_address_of__mono_assembly_0() { return &____mono_assembly_0; }
	inline void set__mono_assembly_0(intptr_t value)
	{
		____mono_assembly_0 = value;
	}

	inline static int32_t get_offset_of_resolve_event_holder_1() { return static_cast<int32_t>(offsetof(Assembly_t, ___resolve_event_holder_1)); }
	inline ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * get_resolve_event_holder_1() const { return ___resolve_event_holder_1; }
	inline ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C ** get_address_of_resolve_event_holder_1() { return &___resolve_event_holder_1; }
	inline void set_resolve_event_holder_1(ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * value)
	{
		___resolve_event_holder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resolve_event_holder_1), (void*)value);
	}

	inline static int32_t get_offset_of__evidence_2() { return static_cast<int32_t>(offsetof(Assembly_t, ____evidence_2)); }
	inline RuntimeObject * get__evidence_2() const { return ____evidence_2; }
	inline RuntimeObject ** get_address_of__evidence_2() { return &____evidence_2; }
	inline void set__evidence_2(RuntimeObject * value)
	{
		____evidence_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____evidence_2), (void*)value);
	}

	inline static int32_t get_offset_of__minimum_3() { return static_cast<int32_t>(offsetof(Assembly_t, ____minimum_3)); }
	inline RuntimeObject * get__minimum_3() const { return ____minimum_3; }
	inline RuntimeObject ** get_address_of__minimum_3() { return &____minimum_3; }
	inline void set__minimum_3(RuntimeObject * value)
	{
		____minimum_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____minimum_3), (void*)value);
	}

	inline static int32_t get_offset_of__optional_4() { return static_cast<int32_t>(offsetof(Assembly_t, ____optional_4)); }
	inline RuntimeObject * get__optional_4() const { return ____optional_4; }
	inline RuntimeObject ** get_address_of__optional_4() { return &____optional_4; }
	inline void set__optional_4(RuntimeObject * value)
	{
		____optional_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optional_4), (void*)value);
	}

	inline static int32_t get_offset_of__refuse_5() { return static_cast<int32_t>(offsetof(Assembly_t, ____refuse_5)); }
	inline RuntimeObject * get__refuse_5() const { return ____refuse_5; }
	inline RuntimeObject ** get_address_of__refuse_5() { return &____refuse_5; }
	inline void set__refuse_5(RuntimeObject * value)
	{
		____refuse_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____refuse_5), (void*)value);
	}

	inline static int32_t get_offset_of__granted_6() { return static_cast<int32_t>(offsetof(Assembly_t, ____granted_6)); }
	inline RuntimeObject * get__granted_6() const { return ____granted_6; }
	inline RuntimeObject ** get_address_of__granted_6() { return &____granted_6; }
	inline void set__granted_6(RuntimeObject * value)
	{
		____granted_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____granted_6), (void*)value);
	}

	inline static int32_t get_offset_of__denied_7() { return static_cast<int32_t>(offsetof(Assembly_t, ____denied_7)); }
	inline RuntimeObject * get__denied_7() const { return ____denied_7; }
	inline RuntimeObject ** get_address_of__denied_7() { return &____denied_7; }
	inline void set__denied_7(RuntimeObject * value)
	{
		____denied_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____denied_7), (void*)value);
	}

	inline static int32_t get_offset_of_fromByteArray_8() { return static_cast<int32_t>(offsetof(Assembly_t, ___fromByteArray_8)); }
	inline bool get_fromByteArray_8() const { return ___fromByteArray_8; }
	inline bool* get_address_of_fromByteArray_8() { return &___fromByteArray_8; }
	inline void set_fromByteArray_8(bool value)
	{
		___fromByteArray_8 = value;
	}

	inline static int32_t get_offset_of_assemblyName_9() { return static_cast<int32_t>(offsetof(Assembly_t, ___assemblyName_9)); }
	inline String_t* get_assemblyName_9() const { return ___assemblyName_9; }
	inline String_t** get_address_of_assemblyName_9() { return &___assemblyName_9; }
	inline void set_assemblyName_9(String_t* value)
	{
		___assemblyName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assemblyName_9), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_pinvoke
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	char* ___assemblyName_9;
};
// Native definition for COM marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_com
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	Il2CppChar* ___assemblyName_9;
};

// Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper
struct AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847  : public RuntimeObject
{
public:

public:
};

struct AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields
{
public:
	// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::N
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___N_0;
	// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::g
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___g_1;
	// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::k
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___k_2;

public:
	inline static int32_t get_offset_of_N_0() { return static_cast<int32_t>(offsetof(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields, ___N_0)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_N_0() const { return ___N_0; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_N_0() { return &___N_0; }
	inline void set_N_0(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___N_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___N_0))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields, ___g_1)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_g_1() const { return ___g_1; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___g_1))->____bits_1), (void*)NULL);
	}

	inline static int32_t get_offset_of_k_2() { return static_cast<int32_t>(offsetof(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields, ___k_2)); }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  get_k_2() const { return ___k_2; }
	inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * get_address_of_k_2() { return &___k_2; }
	inline void set_k_2(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  value)
	{
		___k_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___k_2))->____bits_1), (void*)NULL);
	}
};


// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType
struct AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997  : public RuntimeObject
{
public:
	// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::_accessToken
	String_t* ____accessToken_0;
	// System.Nullable`1<System.Int32> Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::_expiresIn
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ____expiresIn_1;
	// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::_idToken
	String_t* ____idToken_2;
	// Amazon.CognitoIdentityProvider.Model.NewDeviceMetadataType Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::_newDeviceMetadata
	NewDeviceMetadataType_t6551FB84BC8515F4D82EF9540ACF9C46B1E1CEA3 * ____newDeviceMetadata_3;
	// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::_refreshToken
	String_t* ____refreshToken_4;
	// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::_tokenType
	String_t* ____tokenType_5;

public:
	inline static int32_t get_offset_of__accessToken_0() { return static_cast<int32_t>(offsetof(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997, ____accessToken_0)); }
	inline String_t* get__accessToken_0() const { return ____accessToken_0; }
	inline String_t** get_address_of__accessToken_0() { return &____accessToken_0; }
	inline void set__accessToken_0(String_t* value)
	{
		____accessToken_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____accessToken_0), (void*)value);
	}

	inline static int32_t get_offset_of__expiresIn_1() { return static_cast<int32_t>(offsetof(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997, ____expiresIn_1)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get__expiresIn_1() const { return ____expiresIn_1; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of__expiresIn_1() { return &____expiresIn_1; }
	inline void set__expiresIn_1(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		____expiresIn_1 = value;
	}

	inline static int32_t get_offset_of__idToken_2() { return static_cast<int32_t>(offsetof(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997, ____idToken_2)); }
	inline String_t* get__idToken_2() const { return ____idToken_2; }
	inline String_t** get_address_of__idToken_2() { return &____idToken_2; }
	inline void set__idToken_2(String_t* value)
	{
		____idToken_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____idToken_2), (void*)value);
	}

	inline static int32_t get_offset_of__newDeviceMetadata_3() { return static_cast<int32_t>(offsetof(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997, ____newDeviceMetadata_3)); }
	inline NewDeviceMetadataType_t6551FB84BC8515F4D82EF9540ACF9C46B1E1CEA3 * get__newDeviceMetadata_3() const { return ____newDeviceMetadata_3; }
	inline NewDeviceMetadataType_t6551FB84BC8515F4D82EF9540ACF9C46B1E1CEA3 ** get_address_of__newDeviceMetadata_3() { return &____newDeviceMetadata_3; }
	inline void set__newDeviceMetadata_3(NewDeviceMetadataType_t6551FB84BC8515F4D82EF9540ACF9C46B1E1CEA3 * value)
	{
		____newDeviceMetadata_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____newDeviceMetadata_3), (void*)value);
	}

	inline static int32_t get_offset_of__refreshToken_4() { return static_cast<int32_t>(offsetof(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997, ____refreshToken_4)); }
	inline String_t* get__refreshToken_4() const { return ____refreshToken_4; }
	inline String_t** get_address_of__refreshToken_4() { return &____refreshToken_4; }
	inline void set__refreshToken_4(String_t* value)
	{
		____refreshToken_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____refreshToken_4), (void*)value);
	}

	inline static int32_t get_offset_of__tokenType_5() { return static_cast<int32_t>(offsetof(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997, ____tokenType_5)); }
	inline String_t* get__tokenType_5() const { return ____tokenType_5; }
	inline String_t** get_address_of__tokenType_5() { return &____tokenType_5; }
	inline void set__tokenType_5(String_t* value)
	{
		____tokenType_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tokenType_5), (void*)value);
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Amazon.Extensions.CognitoAuthentication.CognitoUserSession
struct CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA  : public RuntimeObject
{
public:
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserSession::<IdToken>k__BackingField
	String_t* ___U3CIdTokenU3Ek__BackingField_0;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserSession::<AccessToken>k__BackingField
	String_t* ___U3CAccessTokenU3Ek__BackingField_1;
	// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserSession::<RefreshToken>k__BackingField
	String_t* ___U3CRefreshTokenU3Ek__BackingField_2;
	// System.DateTime Amazon.Extensions.CognitoAuthentication.CognitoUserSession::<ExpirationTime>k__BackingField
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___U3CExpirationTimeU3Ek__BackingField_3;
	// System.DateTime Amazon.Extensions.CognitoAuthentication.CognitoUserSession::<IssuedTime>k__BackingField
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___U3CIssuedTimeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIdTokenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA, ___U3CIdTokenU3Ek__BackingField_0)); }
	inline String_t* get_U3CIdTokenU3Ek__BackingField_0() const { return ___U3CIdTokenU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CIdTokenU3Ek__BackingField_0() { return &___U3CIdTokenU3Ek__BackingField_0; }
	inline void set_U3CIdTokenU3Ek__BackingField_0(String_t* value)
	{
		___U3CIdTokenU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CIdTokenU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA, ___U3CAccessTokenU3Ek__BackingField_1)); }
	inline String_t* get_U3CAccessTokenU3Ek__BackingField_1() const { return ___U3CAccessTokenU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAccessTokenU3Ek__BackingField_1() { return &___U3CAccessTokenU3Ek__BackingField_1; }
	inline void set_U3CAccessTokenU3Ek__BackingField_1(String_t* value)
	{
		___U3CAccessTokenU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAccessTokenU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRefreshTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA, ___U3CRefreshTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CRefreshTokenU3Ek__BackingField_2() const { return ___U3CRefreshTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRefreshTokenU3Ek__BackingField_2() { return &___U3CRefreshTokenU3Ek__BackingField_2; }
	inline void set_U3CRefreshTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CRefreshTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRefreshTokenU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CExpirationTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA, ___U3CExpirationTimeU3Ek__BackingField_3)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_U3CExpirationTimeU3Ek__BackingField_3() const { return ___U3CExpirationTimeU3Ek__BackingField_3; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_U3CExpirationTimeU3Ek__BackingField_3() { return &___U3CExpirationTimeU3Ek__BackingField_3; }
	inline void set_U3CExpirationTimeU3Ek__BackingField_3(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___U3CExpirationTimeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIssuedTimeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA, ___U3CIssuedTimeU3Ek__BackingField_4)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_U3CIssuedTimeU3Ek__BackingField_4() const { return ___U3CIssuedTimeU3Ek__BackingField_4; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_U3CIssuedTimeU3Ek__BackingField_4() { return &___U3CIssuedTimeU3Ek__BackingField_4; }
	inline void set_U3CIssuedTimeU3Ek__BackingField_4(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___U3CIssuedTimeU3Ek__BackingField_4 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Security.Cryptography.HMAC
struct HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188  : public KeyedHashAlgorithm_t681B59FF58ABCA45D1694A36390AF83AAE2F7F08
{
public:
	// System.Int32 System.Security.Cryptography.HMAC::blockSizeValue
	int32_t ___blockSizeValue_5;
	// System.String System.Security.Cryptography.HMAC::m_hashName
	String_t* ___m_hashName_6;
	// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HMAC::m_hash1
	HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * ___m_hash1_7;
	// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HMAC::m_hash2
	HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * ___m_hash2_8;
	// System.Byte[] System.Security.Cryptography.HMAC::m_inner
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_inner_9;
	// System.Byte[] System.Security.Cryptography.HMAC::m_outer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_outer_10;
	// System.Boolean System.Security.Cryptography.HMAC::m_hashing
	bool ___m_hashing_11;

public:
	inline static int32_t get_offset_of_blockSizeValue_5() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___blockSizeValue_5)); }
	inline int32_t get_blockSizeValue_5() const { return ___blockSizeValue_5; }
	inline int32_t* get_address_of_blockSizeValue_5() { return &___blockSizeValue_5; }
	inline void set_blockSizeValue_5(int32_t value)
	{
		___blockSizeValue_5 = value;
	}

	inline static int32_t get_offset_of_m_hashName_6() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___m_hashName_6)); }
	inline String_t* get_m_hashName_6() const { return ___m_hashName_6; }
	inline String_t** get_address_of_m_hashName_6() { return &___m_hashName_6; }
	inline void set_m_hashName_6(String_t* value)
	{
		___m_hashName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_hashName_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_hash1_7() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___m_hash1_7)); }
	inline HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * get_m_hash1_7() const { return ___m_hash1_7; }
	inline HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 ** get_address_of_m_hash1_7() { return &___m_hash1_7; }
	inline void set_m_hash1_7(HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * value)
	{
		___m_hash1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_hash1_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_hash2_8() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___m_hash2_8)); }
	inline HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * get_m_hash2_8() const { return ___m_hash2_8; }
	inline HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 ** get_address_of_m_hash2_8() { return &___m_hash2_8; }
	inline void set_m_hash2_8(HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * value)
	{
		___m_hash2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_hash2_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_inner_9() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___m_inner_9)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_inner_9() const { return ___m_inner_9; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_inner_9() { return &___m_inner_9; }
	inline void set_m_inner_9(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_inner_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_inner_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_outer_10() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___m_outer_10)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_outer_10() const { return ___m_outer_10; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_outer_10() { return &___m_outer_10; }
	inline void set_m_outer_10(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_outer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_outer_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_hashing_11() { return static_cast<int32_t>(offsetof(HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188, ___m_hashing_11)); }
	inline bool get_m_hashing_11() const { return ___m_hashing_11; }
	inline bool* get_address_of_m_hashing_11() { return &___m_hashing_11; }
	inline void set_m_hashing_11(bool value)
	{
		___m_hashing_11 = value;
	}
};


// System.Net.HttpStatusCode
struct HttpStatusCode_tFCB1BA96A101857DA7C390345DE43B77F9567D98 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpStatusCode_tFCB1BA96A101857DA7C390345DE43B77F9567D98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest
struct InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B  : public AmazonCognitoIdentityProviderRequest_t295E7DC8F0100D19CF1521860058585BEE75F51C
{
public:
	// Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::_analyticsMetadata
	AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C * ____analyticsMetadata_3;
	// Amazon.CognitoIdentityProvider.AuthFlowType Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::_authFlow
	AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ____authFlow_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::_authParameters
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ____authParameters_5;
	// System.String Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::_clientId
	String_t* ____clientId_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::_clientMetadata
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ____clientMetadata_7;
	// Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::_userContextData
	UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B * ____userContextData_8;

public:
	inline static int32_t get_offset_of__analyticsMetadata_3() { return static_cast<int32_t>(offsetof(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B, ____analyticsMetadata_3)); }
	inline AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C * get__analyticsMetadata_3() const { return ____analyticsMetadata_3; }
	inline AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C ** get_address_of__analyticsMetadata_3() { return &____analyticsMetadata_3; }
	inline void set__analyticsMetadata_3(AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C * value)
	{
		____analyticsMetadata_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____analyticsMetadata_3), (void*)value);
	}

	inline static int32_t get_offset_of__authFlow_4() { return static_cast<int32_t>(offsetof(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B, ____authFlow_4)); }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * get__authFlow_4() const { return ____authFlow_4; }
	inline AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 ** get_address_of__authFlow_4() { return &____authFlow_4; }
	inline void set__authFlow_4(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * value)
	{
		____authFlow_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authFlow_4), (void*)value);
	}

	inline static int32_t get_offset_of__authParameters_5() { return static_cast<int32_t>(offsetof(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B, ____authParameters_5)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get__authParameters_5() const { return ____authParameters_5; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of__authParameters_5() { return &____authParameters_5; }
	inline void set__authParameters_5(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		____authParameters_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authParameters_5), (void*)value);
	}

	inline static int32_t get_offset_of__clientId_6() { return static_cast<int32_t>(offsetof(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B, ____clientId_6)); }
	inline String_t* get__clientId_6() const { return ____clientId_6; }
	inline String_t** get_address_of__clientId_6() { return &____clientId_6; }
	inline void set__clientId_6(String_t* value)
	{
		____clientId_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____clientId_6), (void*)value);
	}

	inline static int32_t get_offset_of__clientMetadata_7() { return static_cast<int32_t>(offsetof(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B, ____clientMetadata_7)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get__clientMetadata_7() const { return ____clientMetadata_7; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of__clientMetadata_7() { return &____clientMetadata_7; }
	inline void set__clientMetadata_7(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		____clientMetadata_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____clientMetadata_7), (void*)value);
	}

	inline static int32_t get_offset_of__userContextData_8() { return static_cast<int32_t>(offsetof(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B, ____userContextData_8)); }
	inline UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B * get__userContextData_8() const { return ____userContextData_8; }
	inline UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B ** get_address_of__userContextData_8() { return &____userContextData_8; }
	inline void set__userContextData_8(UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B * value)
	{
		____userContextData_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____userContextData_8), (void*)value);
	}
};


// System.Globalization.NumberStyles
struct NumberStyles_t379EFBF2535E1C950DEC8042704BB663BF636594 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NumberStyles_t379EFBF2535E1C950DEC8042704BB663BF636594, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest
struct RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE  : public AmazonCognitoIdentityProviderRequest_t295E7DC8F0100D19CF1521860058585BEE75F51C
{
public:
	// Amazon.CognitoIdentityProvider.Model.AnalyticsMetadataType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_analyticsMetadata
	AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C * ____analyticsMetadata_3;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_challengeName
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ____challengeName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_challengeResponses
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ____challengeResponses_5;
	// System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_clientId
	String_t* ____clientId_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_clientMetadata
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ____clientMetadata_7;
	// System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_session
	String_t* ____session_8;
	// Amazon.CognitoIdentityProvider.Model.UserContextDataType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::_userContextData
	UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B * ____userContextData_9;

public:
	inline static int32_t get_offset_of__analyticsMetadata_3() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____analyticsMetadata_3)); }
	inline AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C * get__analyticsMetadata_3() const { return ____analyticsMetadata_3; }
	inline AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C ** get_address_of__analyticsMetadata_3() { return &____analyticsMetadata_3; }
	inline void set__analyticsMetadata_3(AnalyticsMetadataType_t3E907C008C6E4B633EAD4278B48146E9EBC2625C * value)
	{
		____analyticsMetadata_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____analyticsMetadata_3), (void*)value);
	}

	inline static int32_t get_offset_of__challengeName_4() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____challengeName_4)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get__challengeName_4() const { return ____challengeName_4; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of__challengeName_4() { return &____challengeName_4; }
	inline void set__challengeName_4(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		____challengeName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____challengeName_4), (void*)value);
	}

	inline static int32_t get_offset_of__challengeResponses_5() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____challengeResponses_5)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get__challengeResponses_5() const { return ____challengeResponses_5; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of__challengeResponses_5() { return &____challengeResponses_5; }
	inline void set__challengeResponses_5(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		____challengeResponses_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____challengeResponses_5), (void*)value);
	}

	inline static int32_t get_offset_of__clientId_6() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____clientId_6)); }
	inline String_t* get__clientId_6() const { return ____clientId_6; }
	inline String_t** get_address_of__clientId_6() { return &____clientId_6; }
	inline void set__clientId_6(String_t* value)
	{
		____clientId_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____clientId_6), (void*)value);
	}

	inline static int32_t get_offset_of__clientMetadata_7() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____clientMetadata_7)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get__clientMetadata_7() const { return ____clientMetadata_7; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of__clientMetadata_7() { return &____clientMetadata_7; }
	inline void set__clientMetadata_7(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		____clientMetadata_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____clientMetadata_7), (void*)value);
	}

	inline static int32_t get_offset_of__session_8() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____session_8)); }
	inline String_t* get__session_8() const { return ____session_8; }
	inline String_t** get_address_of__session_8() { return &____session_8; }
	inline void set__session_8(String_t* value)
	{
		____session_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____session_8), (void*)value);
	}

	inline static int32_t get_offset_of__userContextData_9() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE, ____userContextData_9)); }
	inline UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B * get__userContextData_9() const { return ____userContextData_9; }
	inline UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B ** get_address_of__userContextData_9() { return &____userContextData_9; }
	inline void set__userContextData_9(UserContextDataType_tD99E481F00D5CDD3570D075C2809F7C8BA63931B * value)
	{
		____userContextData_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____userContextData_9), (void*)value);
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.StringComparison
struct StringComparison_tCC9F72B9B1E2C3C6D2566DD0D3A61E1621048998 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringComparison_tCC9F72B9B1E2C3C6D2566DD0D3A61E1621048998, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_28;
	// System.Threading.Tasks.Task/ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * ___m_contingentProperties_33;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskScheduler_7)); }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_parent_8)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_28() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_continuationObject_28)); }
	inline RuntimeObject * get_m_continuationObject_28() const { return ___m_continuationObject_28; }
	inline RuntimeObject ** get_address_of_m_continuationObject_28() { return &___m_continuationObject_28; }
	inline void set_m_continuationObject_28(RuntimeObject * value)
	{
		___m_continuationObject_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_33() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_contingentProperties_33)); }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * get_m_contingentProperties_33() const { return ___m_contingentProperties_33; }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 ** get_address_of_m_contingentProperties_33() { return &___m_contingentProperties_33; }
	inline void set_m_contingentProperties_33(ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * value)
	{
		___m_contingentProperties_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_33), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_29;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_30;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * ___s_currentActiveTasks_31;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_32;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_taskCancelCallback_34;
	// System.Func`1<System.Threading.Tasks.Task/ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * ___s_createContingentProperties_35;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___s_completedTask_36;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * ___s_IsExceptionObservedByParentPredicate_37;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_ecCallback_38;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___s_IsTaskContinuationNullPredicate_39;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_29() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCompletionSentinel_29)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_29() const { return ___s_taskCompletionSentinel_29; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_29() { return &___s_taskCompletionSentinel_29; }
	inline void set_s_taskCompletionSentinel_29(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_29), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_30() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_asyncDebuggingEnabled_30)); }
	inline bool get_s_asyncDebuggingEnabled_30() const { return ___s_asyncDebuggingEnabled_30; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_30() { return &___s_asyncDebuggingEnabled_30; }
	inline void set_s_asyncDebuggingEnabled_30(bool value)
	{
		___s_asyncDebuggingEnabled_30 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_31() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_currentActiveTasks_31)); }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * get_s_currentActiveTasks_31() const { return ___s_currentActiveTasks_31; }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 ** get_address_of_s_currentActiveTasks_31() { return &___s_currentActiveTasks_31; }
	inline void set_s_currentActiveTasks_31(Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * value)
	{
		___s_currentActiveTasks_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_31), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_32() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_activeTasksLock_32)); }
	inline RuntimeObject * get_s_activeTasksLock_32() const { return ___s_activeTasksLock_32; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_32() { return &___s_activeTasksLock_32; }
	inline void set_s_activeTasksLock_32(RuntimeObject * value)
	{
		___s_activeTasksLock_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_32), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_34() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCancelCallback_34)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_taskCancelCallback_34() const { return ___s_taskCancelCallback_34; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_taskCancelCallback_34() { return &___s_taskCancelCallback_34; }
	inline void set_s_taskCancelCallback_34(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_taskCancelCallback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_34), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_35() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_createContingentProperties_35)); }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * get_s_createContingentProperties_35() const { return ___s_createContingentProperties_35; }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B ** get_address_of_s_createContingentProperties_35() { return &___s_createContingentProperties_35; }
	inline void set_s_createContingentProperties_35(Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * value)
	{
		___s_createContingentProperties_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_35), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_36() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_completedTask_36)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_s_completedTask_36() const { return ___s_completedTask_36; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_s_completedTask_36() { return &___s_completedTask_36; }
	inline void set_s_completedTask_36(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___s_completedTask_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_37() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsExceptionObservedByParentPredicate_37)); }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * get_s_IsExceptionObservedByParentPredicate_37() const { return ___s_IsExceptionObservedByParentPredicate_37; }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD ** get_address_of_s_IsExceptionObservedByParentPredicate_37() { return &___s_IsExceptionObservedByParentPredicate_37; }
	inline void set_s_IsExceptionObservedByParentPredicate_37(Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * value)
	{
		___s_IsExceptionObservedByParentPredicate_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_37), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_38() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_ecCallback_38)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_ecCallback_38() const { return ___s_ecCallback_38; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_ecCallback_38() { return &___s_ecCallback_38; }
	inline void set_s_ecCallback_38(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_ecCallback_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_38), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_39() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsTaskContinuationNullPredicate_39)); }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * get_s_IsTaskContinuationNullPredicate_39() const { return ___s_IsTaskContinuationNullPredicate_39; }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB ** get_address_of_s_IsTaskContinuationNullPredicate_39() { return &___s_IsTaskContinuationNullPredicate_39; }
	inline void set_s_IsTaskContinuationNullPredicate_39(Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * value)
	{
		___s_IsTaskContinuationNullPredicate_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_39), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// Amazon.Runtime.WebServiceRequestEventArgs
struct WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A  : public RequestEventArgs_tE1FA7940E62EB2306034D1E8ABD47B2D8ED44A56
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::<Parameters>k__BackingField
	RuntimeObject* ___U3CParametersU3Ek__BackingField_2;
	// Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.WebServiceRequestEventArgs::<ParameterCollection>k__BackingField
	ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 * ___U3CParameterCollectionU3Ek__BackingField_3;
	// System.String Amazon.Runtime.WebServiceRequestEventArgs::<ServiceName>k__BackingField
	String_t* ___U3CServiceNameU3Ek__BackingField_4;
	// System.Uri Amazon.Runtime.WebServiceRequestEventArgs::<Endpoint>k__BackingField
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___U3CEndpointU3Ek__BackingField_5;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.WebServiceRequestEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * ___U3CRequestU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHeadersU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A, ___U3CParametersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CParametersU3Ek__BackingField_2() const { return ___U3CParametersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CParametersU3Ek__BackingField_2() { return &___U3CParametersU3Ek__BackingField_2; }
	inline void set_U3CParametersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CParametersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParametersU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CParameterCollectionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A, ___U3CParameterCollectionU3Ek__BackingField_3)); }
	inline ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 * get_U3CParameterCollectionU3Ek__BackingField_3() const { return ___U3CParameterCollectionU3Ek__BackingField_3; }
	inline ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 ** get_address_of_U3CParameterCollectionU3Ek__BackingField_3() { return &___U3CParameterCollectionU3Ek__BackingField_3; }
	inline void set_U3CParameterCollectionU3Ek__BackingField_3(ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 * value)
	{
		___U3CParameterCollectionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParameterCollectionU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServiceNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A, ___U3CServiceNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CServiceNameU3Ek__BackingField_4() const { return ___U3CServiceNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServiceNameU3Ek__BackingField_4() { return &___U3CServiceNameU3Ek__BackingField_4; }
	inline void set_U3CServiceNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CServiceNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CServiceNameU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A, ___U3CEndpointU3Ek__BackingField_5)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_U3CEndpointU3Ek__BackingField_5() const { return ___U3CEndpointU3Ek__BackingField_5; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_U3CEndpointU3Ek__BackingField_5() { return &___U3CEndpointU3Ek__BackingField_5; }
	inline void set_U3CEndpointU3Ek__BackingField_5(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___U3CEndpointU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEndpointU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A, ___U3CRequestU3Ek__BackingField_6)); }
	inline AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * get_U3CRequestU3Ek__BackingField_6() const { return ___U3CRequestU3Ek__BackingField_6; }
	inline AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 ** get_address_of_U3CRequestU3Ek__BackingField_6() { return &___U3CRequestU3Ek__BackingField_6; }
	inline void set_U3CRequestU3Ek__BackingField_6(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * value)
	{
		___U3CRequestU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRequestU3Ek__BackingField_6), (void*)value);
	}
};


// System.Threading.Tasks.Task`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>
struct Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * ___m_result_40;

public:
	inline static int32_t get_offset_of_m_result_40() { return static_cast<int32_t>(offsetof(Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83, ___m_result_40)); }
	inline AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * get_m_result_40() const { return ___m_result_40; }
	inline AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 ** get_address_of_m_result_40() { return &___m_result_40; }
	inline void set_m_result_40(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * value)
	{
		___m_result_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_40), (void*)value);
	}
};

struct Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t4E979A34E6ABACD380893EC448C3B44DCEFA0E60 * ___s_Factory_41;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t8D6EC4F5423B4B1BBA170139EFACB64E74DDDF50 * ___TaskWhenAnyCast_42;

public:
	inline static int32_t get_offset_of_s_Factory_41() { return static_cast<int32_t>(offsetof(Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83_StaticFields, ___s_Factory_41)); }
	inline TaskFactory_1_t4E979A34E6ABACD380893EC448C3B44DCEFA0E60 * get_s_Factory_41() const { return ___s_Factory_41; }
	inline TaskFactory_1_t4E979A34E6ABACD380893EC448C3B44DCEFA0E60 ** get_address_of_s_Factory_41() { return &___s_Factory_41; }
	inline void set_s_Factory_41(TaskFactory_1_t4E979A34E6ABACD380893EC448C3B44DCEFA0E60 * value)
	{
		___s_Factory_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_41), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_42() { return static_cast<int32_t>(offsetof(Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83_StaticFields, ___TaskWhenAnyCast_42)); }
	inline Func_2_t8D6EC4F5423B4B1BBA170139EFACB64E74DDDF50 * get_TaskWhenAnyCast_42() const { return ___TaskWhenAnyCast_42; }
	inline Func_2_t8D6EC4F5423B4B1BBA170139EFACB64E74DDDF50 ** get_address_of_TaskWhenAnyCast_42() { return &___TaskWhenAnyCast_42; }
	inline void set_TaskWhenAnyCast_42(Func_2_t8D6EC4F5423B4B1BBA170139EFACB64E74DDDF50 * value)
	{
		___TaskWhenAnyCast_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_42), (void*)value);
	}
};


// System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>
struct Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * ___m_result_40;

public:
	inline static int32_t get_offset_of_m_result_40() { return static_cast<int32_t>(offsetof(Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52, ___m_result_40)); }
	inline InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * get_m_result_40() const { return ___m_result_40; }
	inline InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C ** get_address_of_m_result_40() { return &___m_result_40; }
	inline void set_m_result_40(InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * value)
	{
		___m_result_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_40), (void*)value);
	}
};

struct Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_tF8917CC2B280DB56DDCECCCEC69EA74A2AB7597F * ___s_Factory_41;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t16374DF48AE4374245767B70C237F8918E244C3C * ___TaskWhenAnyCast_42;

public:
	inline static int32_t get_offset_of_s_Factory_41() { return static_cast<int32_t>(offsetof(Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52_StaticFields, ___s_Factory_41)); }
	inline TaskFactory_1_tF8917CC2B280DB56DDCECCCEC69EA74A2AB7597F * get_s_Factory_41() const { return ___s_Factory_41; }
	inline TaskFactory_1_tF8917CC2B280DB56DDCECCCEC69EA74A2AB7597F ** get_address_of_s_Factory_41() { return &___s_Factory_41; }
	inline void set_s_Factory_41(TaskFactory_1_tF8917CC2B280DB56DDCECCCEC69EA74A2AB7597F * value)
	{
		___s_Factory_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_41), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_42() { return static_cast<int32_t>(offsetof(Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52_StaticFields, ___TaskWhenAnyCast_42)); }
	inline Func_2_t16374DF48AE4374245767B70C237F8918E244C3C * get_TaskWhenAnyCast_42() const { return ___TaskWhenAnyCast_42; }
	inline Func_2_t16374DF48AE4374245767B70C237F8918E244C3C ** get_address_of_TaskWhenAnyCast_42() { return &___TaskWhenAnyCast_42; }
	inline void set_TaskWhenAnyCast_42(Func_2_t16374DF48AE4374245767B70C237F8918E244C3C * value)
	{
		___TaskWhenAnyCast_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_42), (void*)value);
	}
};


// System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>
struct Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * ___m_result_40;

public:
	inline static int32_t get_offset_of_m_result_40() { return static_cast<int32_t>(offsetof(Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516, ___m_result_40)); }
	inline RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * get_m_result_40() const { return ___m_result_40; }
	inline RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 ** get_address_of_m_result_40() { return &___m_result_40; }
	inline void set_m_result_40(RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * value)
	{
		___m_result_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_40), (void*)value);
	}
};

struct Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_tBFD4F2C09DDB8B228D23356F9F2CD281EA102216 * ___s_Factory_41;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t1CC0E58E7E15C4A5782AAC035620F0F8E0F7B2A2 * ___TaskWhenAnyCast_42;

public:
	inline static int32_t get_offset_of_s_Factory_41() { return static_cast<int32_t>(offsetof(Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516_StaticFields, ___s_Factory_41)); }
	inline TaskFactory_1_tBFD4F2C09DDB8B228D23356F9F2CD281EA102216 * get_s_Factory_41() const { return ___s_Factory_41; }
	inline TaskFactory_1_tBFD4F2C09DDB8B228D23356F9F2CD281EA102216 ** get_address_of_s_Factory_41() { return &___s_Factory_41; }
	inline void set_s_Factory_41(TaskFactory_1_tBFD4F2C09DDB8B228D23356F9F2CD281EA102216 * value)
	{
		___s_Factory_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_41), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_42() { return static_cast<int32_t>(offsetof(Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516_StaticFields, ___TaskWhenAnyCast_42)); }
	inline Func_2_t1CC0E58E7E15C4A5782AAC035620F0F8E0F7B2A2 * get_TaskWhenAnyCast_42() const { return ___TaskWhenAnyCast_42; }
	inline Func_2_t1CC0E58E7E15C4A5782AAC035620F0F8E0F7B2A2 ** get_address_of_TaskWhenAnyCast_42() { return &___TaskWhenAnyCast_42; }
	inline void set_TaskWhenAnyCast_42(Func_2_t1CC0E58E7E15C4A5782AAC035620F0F8E0F7B2A2 * value)
	{
		___TaskWhenAnyCast_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_42), (void*)value);
	}
};


// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A  : public RuntimeObject
{
public:
	// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::responseMetadataField
	ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * ___responseMetadataField_0;
	// System.Int64 Amazon.Runtime.AmazonWebServiceResponse::contentLength
	int64_t ___contentLength_1;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::httpStatusCode
	int32_t ___httpStatusCode_2;

public:
	inline static int32_t get_offset_of_responseMetadataField_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A, ___responseMetadataField_0)); }
	inline ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * get_responseMetadataField_0() const { return ___responseMetadataField_0; }
	inline ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 ** get_address_of_responseMetadataField_0() { return &___responseMetadataField_0; }
	inline void set_responseMetadataField_0(ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * value)
	{
		___responseMetadataField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___responseMetadataField_0), (void*)value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_httpStatusCode_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A, ___httpStatusCode_2)); }
	inline int32_t get_httpStatusCode_2() const { return ___httpStatusCode_2; }
	inline int32_t* get_address_of_httpStatusCode_2() { return &___httpStatusCode_2; }
	inline void set_httpStatusCode_2(int32_t value)
	{
		___httpStatusCode_2 = value;
	}
};


// System.Security.Cryptography.HMACSHA256
struct HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD  : public HMAC_t67600C3E3919D1C0E5D407C85BCF7C9C486C9188
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81
struct U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 
{
public:
	// System.Int32 Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse> Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::<>t__builder
	AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  ___U3CU3Et__builder_1;
	// Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::srpRequest
	InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * ___srpRequest_2;
	// Amazon.Extensions.CognitoAuthentication.CognitoUser Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::<>4__this
	CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * ___U3CU3E4__this_3;
	// System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger> Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::<tupleAa>5__2
	Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___U3CtupleAaU3E5__2_4;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse> Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::<>u__1
	ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  ___U3CU3Eu__1_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse> Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::<>u__2
	ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  ___U3CU3Eu__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_srpRequest_2() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___srpRequest_2)); }
	inline InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * get_srpRequest_2() const { return ___srpRequest_2; }
	inline InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 ** get_address_of_srpRequest_2() { return &___srpRequest_2; }
	inline void set_srpRequest_2(InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * value)
	{
		___srpRequest_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___srpRequest_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___U3CU3E4__this_3)); }
	inline CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtupleAaU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___U3CtupleAaU3E5__2_4)); }
	inline Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * get_U3CtupleAaU3E5__2_4() const { return ___U3CtupleAaU3E5__2_4; }
	inline Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 ** get_address_of_U3CtupleAaU3E5__2_4() { return &___U3CtupleAaU3E5__2_4; }
	inline void set_U3CtupleAaU3E5__2_4(Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * value)
	{
		___U3CtupleAaU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtupleAaU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___U3CU3Eu__1_5)); }
	inline ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  value)
	{
		___U3CU3Eu__1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_5))->___m_task_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_6() { return static_cast<int32_t>(offsetof(U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153, ___U3CU3Eu__2_6)); }
	inline ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  get_U3CU3Eu__2_6() const { return ___U3CU3Eu__2_6; }
	inline ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C * get_address_of_U3CU3Eu__2_6() { return &___U3CU3Eu__2_6; }
	inline void set_U3CU3Eu__2_6(ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  value)
	{
		___U3CU3Eu__2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__2_6))->___m_task_0), (void*)NULL);
	}
};


// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse
struct InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C  : public AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A
{
public:
	// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::_authenticationResult
	AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ____authenticationResult_3;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::_challengeName
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ____challengeName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::_challengeParameters
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ____challengeParameters_5;
	// System.String Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::_session
	String_t* ____session_6;

public:
	inline static int32_t get_offset_of__authenticationResult_3() { return static_cast<int32_t>(offsetof(InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C, ____authenticationResult_3)); }
	inline AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * get__authenticationResult_3() const { return ____authenticationResult_3; }
	inline AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 ** get_address_of__authenticationResult_3() { return &____authenticationResult_3; }
	inline void set__authenticationResult_3(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * value)
	{
		____authenticationResult_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authenticationResult_3), (void*)value);
	}

	inline static int32_t get_offset_of__challengeName_4() { return static_cast<int32_t>(offsetof(InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C, ____challengeName_4)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get__challengeName_4() const { return ____challengeName_4; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of__challengeName_4() { return &____challengeName_4; }
	inline void set__challengeName_4(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		____challengeName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____challengeName_4), (void*)value);
	}

	inline static int32_t get_offset_of__challengeParameters_5() { return static_cast<int32_t>(offsetof(InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C, ____challengeParameters_5)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get__challengeParameters_5() const { return ____challengeParameters_5; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of__challengeParameters_5() { return &____challengeParameters_5; }
	inline void set__challengeParameters_5(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		____challengeParameters_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____challengeParameters_5), (void*)value);
	}

	inline static int32_t get_offset_of__session_6() { return static_cast<int32_t>(offsetof(InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C, ____session_6)); }
	inline String_t* get__session_6() const { return ____session_6; }
	inline String_t** get_address_of__session_6() { return &____session_6; }
	inline void set__session_6(String_t* value)
	{
		____session_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____session_6), (void*)value);
	}
};


// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7  : public MulticastDelegate_t
{
public:

public:
};


// Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse
struct RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929  : public AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A
{
public:
	// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::_authenticationResult
	AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ____authenticationResult_3;
	// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::_challengeName
	ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ____challengeName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::_challengeParameters
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ____challengeParameters_5;
	// System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::_session
	String_t* ____session_6;

public:
	inline static int32_t get_offset_of__authenticationResult_3() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929, ____authenticationResult_3)); }
	inline AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * get__authenticationResult_3() const { return ____authenticationResult_3; }
	inline AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 ** get_address_of__authenticationResult_3() { return &____authenticationResult_3; }
	inline void set__authenticationResult_3(AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * value)
	{
		____authenticationResult_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authenticationResult_3), (void*)value);
	}

	inline static int32_t get_offset_of__challengeName_4() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929, ____challengeName_4)); }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * get__challengeName_4() const { return ____challengeName_4; }
	inline ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 ** get_address_of__challengeName_4() { return &____challengeName_4; }
	inline void set__challengeName_4(ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * value)
	{
		____challengeName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____challengeName_4), (void*)value);
	}

	inline static int32_t get_offset_of__challengeParameters_5() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929, ____challengeParameters_5)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get__challengeParameters_5() const { return ____challengeParameters_5; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of__challengeParameters_5() { return &____challengeParameters_5; }
	inline void set__challengeParameters_5(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		____challengeParameters_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____challengeParameters_5), (void*)value);
	}

	inline static int32_t get_offset_of__session_6() { return static_cast<int32_t>(offsetof(RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929, ____session_6)); }
	inline String_t* get__session_6() const { return ____session_6; }
	inline String_t** get_address_of__session_6() { return &____session_6; }
	inline void set__session_6(String_t* value)
	{
		____session_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____session_6), (void*)value);
	}
};


// System.Reflection.TypeInfo
struct TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F  : public Type_t
{
public:

public:
};


// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* m_Items[1];

public:
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Tuple`2<!!0,!!1> System.Tuple::Create<System.Numerics.BigInteger,System.Numerics.BigInteger>(!!0,!!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214_gshared (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___item10, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___item21, const RuntimeMethod* method);
// !0 System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>::get_Item1()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_gshared_inline (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * __this, const RuntimeMethod* method);
// !1 System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>::get_Item2()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_gshared_inline (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * __this, const RuntimeMethod* method);
// T[] Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::Reverse<System.Byte>(T[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_gshared (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___array0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<!0> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020  AsyncTaskMethodBuilder_1_Create_m9A01E4B2FB83D8B9A71740D85F771F9A2E0DBAC9_gshared (const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::Start<Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m399CE8F2CAFC5BCCAECB2EB896A1A3DDCF1184DC_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * ___stateMachine0, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<!0> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::get_Task()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m42F33AB093A9AAB17C558FDBFF010443D1048400_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<!0> System.Threading.Tasks.Task`1<System.Object>::ConfigureAwait(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConfiguredTaskAwaitable_1_t226372B9DEDA3AA0FC1B43D6C03CEC9111045F18  Task_1_ConfigureAwait_m0C99499DCC096AEE2A6AD075391C61037CC3DAA1_gshared (Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * __this, bool ___continueOnCapturedContext0, const RuntimeMethod* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<!0> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::GetAwaiter()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED  ConfiguredTaskAwaitable_1_GetAwaiter_mFCE2327CEE19607ABB1CDCC8A6B145BDCF9820BC_gshared_inline (ConfiguredTaskAwaitable_1_t226372B9DEDA3AA0FC1B43D6C03CEC9111045F18 * __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::get_IsCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ConfiguredTaskAwaiter_get_IsCompleted_m5E3746D1B0661A5BCD45816E83766F228A077D20_gshared (ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>,Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81>(!!0&,!!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m08ECA315D1A00BEA0C00C4BB9C84DEC65F43F30F_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * ___awaiter0, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * ___stateMachine1, const RuntimeMethod* method);
// !0 System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ConfiguredTaskAwaiter_GetResult_mD385ED6B1C12DC6353D50409731FB1729FFD9FA5_gshared (ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<!0,!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m15FB53DEA7CD6487FD59ED68D6DB681FD332025C_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject* ___dictionary0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_SetException_m29521EB618E38AF72FF0C4094070C1489F4129B3_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, Exception_t * ___exception0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetResult(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_SetResult_m3E4AB12877D4FE377F26708CF6899C49360007FA_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject * ___result0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncTaskMethodBuilder_1_SetStateMachine_m736C84D61B4AB2FCD150BD3945C6874471A9224D_gshared (AsyncTaskMethodBuilder_1_tDD2A3BA099C327938EA03C35FDB1A7502BA73020 * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::FromUnsignedLittleEndianHex(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9 (String_t* ___hex0, const RuntimeMethod* method);
// System.Void System.Numerics.BigInteger::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BigInteger__ctor_m104B492675CC61CB48D17E18900DF23DCB7408D4 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::ToBigEndianByteArray(System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___self0, const RuntimeMethod* method);
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::CombineBytes(System.Byte[][])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368 (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* ___values0, const RuntimeMethod* method);
// System.Security.Cryptography.SHA256 Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::get_Sha256()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9 (const RuntimeMethod* method);
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9 (HashAlgorithm_t7F831BEF35F9D0AF5016FFB0E474AF9F93908F31 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, const RuntimeMethod* method);
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::FromUnsignedBigEndian(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytes0, const RuntimeMethod* method);
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::CreateBigIntegerRandom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  AuthenticationHelper_CreateBigIntegerRandom_mDF7B2E5C2136FF32F7F58A1EFF9E0732ACB31170 (const RuntimeMethod* method);
// System.Numerics.BigInteger System.Numerics.BigInteger::ModPow(System.Numerics.BigInteger,System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_ModPow_mD3EF2940961384A83E1004725E9A14DE4EB8E4C6 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___value0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___exponent1, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___modulus2, const RuntimeMethod* method);
// System.Tuple`2<!!0,!!1> System.Tuple::Create<System.Numerics.BigInteger,System.Numerics.BigInteger>(!!0,!!1)
inline Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___item10, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___item21, const RuntimeMethod* method)
{
	return ((  Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * (*) (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 , BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 , const RuntimeMethod*))Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214_gshared)(___item10, ___item21, method);
}
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::TrueMod(System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___self0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___other1, const RuntimeMethod* method);
// System.Numerics.BigInteger System.Numerics.BigInteger::get_Zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_get_Zero_mE10EE4CF6BAD05BC10D0D5012ECDCC1B81E438BF_inline (const RuntimeMethod* method);
// System.Boolean System.Numerics.BigInteger::Equals(System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BigInteger_Equals_m05302B320532185A473A0E9CBA352FA30BB43C9A (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * __this, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___other0, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Byte[] System.Convert::FromBase64String(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Convert_FromBase64String_mB2E4E2CD03B34DB7C2665694D5B2E967BC81E9A8 (String_t* ___s0, const RuntimeMethod* method);
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::GetPasswordAuthenticationKey(System.String,System.String,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>,System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7 (String_t* ___userID0, String_t* ___userPassword1, String_t* ___poolName2, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___Aa3, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___B4, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___salt5, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E (const RuntimeMethod* method);
// System.Void System.Security.Cryptography.HMACSHA256::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HMACSHA256__ctor_m0836F5FA98464B745CF97CC102DBE4497E17BEA9 (HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___key0, const RuntimeMethod* method);
// !0 System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>::get_Item1()
inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_inline (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * __this, const RuntimeMethod* method)
{
	return ((  BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  (*) (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 *, const RuntimeMethod*))Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_gshared_inline)(__this, method);
}
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Numerics.BigInteger System.Numerics.BigInteger::op_Multiply(System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_op_Multiply_m2C693A37027C26E76BFF4F974553ACA2D8DCC067 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___left0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___right1, const RuntimeMethod* method);
// System.Numerics.BigInteger System.Numerics.BigInteger::op_Subtraction(System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_op_Subtraction_m06627EF453D41B140BE6604D5C6D8F048B9D4B7C (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___left0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___right1, const RuntimeMethod* method);
// !1 System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>::get_Item2()
inline BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_inline (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * __this, const RuntimeMethod* method)
{
	return ((  BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  (*) (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 *, const RuntimeMethod*))Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_gshared_inline)(__this, method);
}
// System.Numerics.BigInteger System.Numerics.BigInteger::op_Addition(System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_op_Addition_m55A6D35945F71B25A38BD6688EFAC3FAF10C46B9 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___left0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___right1, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::.ctor(System.Byte[],System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HkdfSha256__ctor_mF448325D81511381115C9D024636F8386C6C2697 (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___salt0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___ikm1, const RuntimeMethod* method);
// System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::Expand(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___info0, int32_t ___length1, const RuntimeMethod* method);
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RandomNumberGenerator::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * RandomNumberGenerator_Create_m04A5418F8572F0498EE0659633B4C0620CB55721 (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Numerics.BigInteger System.Numerics.BigInteger::Parse(System.String,System.Globalization.NumberStyles)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_Parse_mCF195A7CFD256BCF1B8450A31ED9BA326C6C2337 (String_t* ___value0, int32_t ___style1, const RuntimeMethod* method);
// System.Numerics.BigInteger System.Numerics.BigInteger::op_Modulus(System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_op_Modulus_m90CD4DEC2293E0D71B73A33720AEE6CFEDECBE2E (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___dividend0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___divisor1, const RuntimeMethod* method);
// System.Int32 System.Numerics.BigInteger::get_Sign()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BigInteger_get_Sign_mB7A2E5A1C237EB3532F5AA6C13511B696B3EA660 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * __this, const RuntimeMethod* method);
// System.Byte[] System.Numerics.BigInteger::ToByteArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BigInteger_ToByteArray_mE55CBE13ADA8E09AFE2D10FF9AD2F373E4000B7B (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * __this, const RuntimeMethod* method);
// T[] Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::Reverse<System.Byte>(T[])
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___array0, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, const RuntimeMethod*))BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_gshared)(___array0, method);
}
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m40103AA97DC582C557B912CF4BBE86A4D166F803 (RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.Numerics.BigInteger::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BigInteger__ctor_mE86998DE086542EC44032A9A6F3978C08DB8DC1D (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___value0, const RuntimeMethod* method);
// System.Security.Cryptography.SHA256 System.Security.Cryptography.SHA256::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * SHA256_Create_mA93565DD491DC3380FB602957B00EA909FEB8A20 (const RuntimeMethod* method);
// System.String System.Convert::ToBase64String(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Convert_ToBase64String_mE6E1FE504EF1E99DB2F8B92180A82A5F1512EF6A (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___inArray0, const RuntimeMethod* method);
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725 (RuntimeArray * ___src0, int32_t ___srcOffset1, RuntimeArray * ___dst2, int32_t ___dstOffset3, int32_t ___count4, const RuntimeMethod* method);
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::get_Headers()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5_inline (WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * __this, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::GetAssemblyFileVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoAuthHelper_GetAssemblyFileVersion_mD4696F4A47F83B7C0A8213149128CC6167F81428 (const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Reflection.TypeInfo System.Reflection.IntrospectionExtensions::GetTypeInfo(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F * IntrospectionExtensions_GetTypeInfo_m77034F8576BE695819427C13103C591277C1B636 (Type_t * ___type0, const RuntimeMethod* method);
// System.Attribute System.Reflection.CustomAttributeExtensions::GetCustomAttribute(System.Reflection.Assembly,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71 * CustomAttributeExtensions_GetCustomAttribute_m6CC58E7580DB6F8280968AEF3CD8BD8A2BF27662 (Assembly_t * ___element0, Type_t * ___attributeType1, const RuntimeMethod* method);
// System.String System.Reflection.AssemblyFileVersionAttribute::get_Version()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AssemblyFileVersionAttribute_get_Version_m56763C51185F40666C40AF57A0E83A1B446C7597_inline (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
inline void Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserPool::get_PoolID()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___separator0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_PoolName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_PoolName_mB319666D9228CDCF803B56AE31006CB185FCBCAC_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_ClientSecret(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_ClientSecret_m0013271AF0F323BFF73DF5B7E422946882F58364_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::GetUserPoolSecretHash(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193 (String_t* ___userID0, String_t* ___clientID1, String_t* ___clientSecret2, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_SecretHash(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_UserID(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_UserID_mA7CF427A7E4ED35C62A09920A8D6D3E7B5A20266_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Username(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Status(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Status_m6AC74726AEECA36363F06AFC578491E8CB06A54A_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_UserPool(Amazon.Extensions.CognitoAuthentication.CognitoUserPool)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_UserPool_mEC8B2A8E3CE2AC7A66ADF8A201F4C2BA6DCCF9D0_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_ClientID(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_ClientID_m7D5C25601A2033D3887954A0A64B7DEBF0A8233F_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_SessionTokens(Amazon.Extensions.CognitoAuthentication.CognitoUserSession)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Attributes(System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Attributes_m6CFCBD03B13DA55D2275D8CD7F6DC9A25B5EF72A_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Provider(Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Provider_m384C1E82B82132EAA78A0C0D689A7EDF24C0CBBA_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Provider()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.RequestEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequestEventHandler__ctor_m570E54DC4EF47898C92E313326A679EE57D8E3B5 (RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Amazon.Runtime.AmazonServiceClient::add_BeforeRequestEvent(Amazon.Runtime.RequestEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonServiceClient_add_BeforeRequestEvent_m818F8CCC42067965654196C87B746679EB5045C4 (AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB * __this, RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * ___value0, const RuntimeMethod* method);
// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_IdToken()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AuthenticationResultType_get_IdToken_mE391F6EF0BF8DF2263ED4629BD49F52E622E8C75_inline (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method);
// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_AccessToken()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AuthenticationResultType_get_AccessToken_m6D5BAA07E120818EB40BF3DA271698B28C325486_inline (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C (const RuntimeMethod* method);
// System.String Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_RefreshToken()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AuthenticationResultType_get_RefreshToken_m9B9C4F96A0C5A98F7763DA4EAB3499BD6A53E52A_inline (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method);
// System.Int32 Amazon.CognitoIdentityProvider.Model.AuthenticationResultType::get_ExpiresIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AuthenticationResultType_get_ExpiresIn_m9D1E78AB44F6E8D25EB4355381429B6404155BD1 (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::AddSeconds(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  DateTime_AddSeconds_mCA0940A7E7C3ED40A86532349B7D4CB3A0F0DEAF (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, double ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::.ctor(System.String,System.String,System.String,System.DateTime,System.DateTime)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession__ctor_m8AB71BE7C28B14D88DDF96E4EDC7B36318FE7686 (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___idToken0, String_t* ___accessToken1, String_t* ___refreshToken2, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___issuedTime3, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___expirationTime4, const RuntimeMethod* method);
// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<!0> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::Create()
inline AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  AsyncTaskMethodBuilder_1_Create_mEF2A6F90F0E729A3E44F040FC6D54F8EF6736367 (const RuntimeMethod* method)
{
	return ((  AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  (*) (const RuntimeMethod*))AsyncTaskMethodBuilder_1_Create_m9A01E4B2FB83D8B9A71740D85F771F9A2E0DBAC9_gshared)(method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::Start<Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81>(!!0&)
inline void AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6E9C9BE2FCAE466776F72764B7D19F77483DB6FE (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * ___stateMachine0, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m399CE8F2CAFC5BCCAECB2EB896A1A3DDCF1184DC_gshared)(__this, ___stateMachine0, method);
}
// System.Threading.Tasks.Task`1<!0> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::get_Task()
inline Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * AsyncTaskMethodBuilder_1_get_Task_m7F62A8721A6D0863BCB16CB26A96CDD62355B2CB (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, const RuntimeMethod* method)
{
	return ((  Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_get_Task_m61DEC300353320E428E17DA0D59D61974F4415BB_gshared)(__this, method);
}
// System.String Amazon.Runtime.ConstantClass::op_Implicit(Amazon.Runtime.ConstantClass)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ConstantClass_op_Implicit_m66D411C2C8F30059CBDF13282013A60AD5D95B90 (ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6 * ___value0, const RuntimeMethod* method);
// Amazon.Extensions.CognitoAuthentication.CognitoUserSession Amazon.Extensions.CognitoAuthentication.CognitoUser::GetCognitoUserSession(Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * CognitoUser_GetCognitoUserSession_m29915544522BBCC536F1C1BAC2C8BBB2132993E3 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___authResult0, String_t* ___refreshTokenOverride1, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitiateAuthRequest__ctor_mC7F09D29F08F846B156E36B3B6C2256A164F5618 (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::set_AuthFlow(Amazon.CognitoIdentityProvider.AuthFlowType)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InitiateAuthRequest_set_AuthFlow_mD0AB0CD4A6044E83D4C8DB9E520984E4E6148649_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___value0, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_ClientID()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::set_ClientId(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InitiateAuthRequest_set_ClientId_m7AF95FACF7F24BA83DA0AB54F23102925DFF1E74_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, String_t* ___value0, const RuntimeMethod* method);
// System.StringComparer System.StringComparer::get_Ordinal()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * StringComparer_get_Ordinal_mF3B6370BEBD77351DB5218C867DCD669C47B8812_inline (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
inline void Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_m42F33AB093A9AAB17C558FDBFF010443D1048400_gshared)(__this, ___comparer0, method);
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Username()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, String_t* ___key0, String_t* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared)(__this, ___key0, ___value1, method);
}
// System.String System.Numerics.BigInteger::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BigInteger_ToString_mA75496825851550D19E92E5DCA527DF2AC430BF6 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 * __this, String_t* ___format0, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::set_AuthParameters(System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InitiateAuthRequest_set_AuthParameters_m156E35FA9567233EE27CE4819499095448504CDF_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_ClientSecret()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest::get_AuthParameters()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * InitiateAuthRequest_get_AuthParameters_m8EDAAC67BDB894DFFD14574069FF458FE45766FA_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, const RuntimeMethod* method);
// Amazon.Extensions.CognitoAuthentication.CognitoDevice Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Device()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.CognitoDevice::get_DeviceKey()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline (CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * __this, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_UserID()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_UserID_m858BEACD4ACC7D4EC6462D0A2229A7B4EA6EF72F_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_mD65682B0BB7933CC7A8561AE34DED02E4F3BBBE5 (String_t* ___a0, String_t* ___b1, int32_t ___comparisonType2, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::get_ChallengeParameters()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline (InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0)
inline String_t* Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared)(__this, ___key0, method);
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_PoolName()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_PoolName_mB595FBE47D57E0782C042D1D3DF9FF6F32FA62AA_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_UtcNow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  DateTime_get_UtcNow_m761E57F86226DDD94F0A2F4D98F0A8E27C74F090 (const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164 (const RuntimeMethod* method);
// System.String System.DateTime::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DateTime_ToString_mE44033D2750D165DED2A17A927381872EF9FC986 (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::AuthenticateUser(System.String,System.String,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1 (String_t* ___username0, String_t* ___password1, String_t* ___poolName2, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___tupleAa3, String_t* ___saltString4, String_t* ___srpbString5, String_t* ___secretBlockBase646, String_t* ___formattedTimestamp7, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_SecretHash()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_SecretHash_m1FCB2073B2703446B01998388FFD77B5056B8A98_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest__ctor_m96C934A734FB612A3DC464BE3396F64DD157E276 (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, const RuntimeMethod* method);
// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::get_ChallengeName()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * InitiateAuthResponse_get_ChallengeName_m7D4CCE67488E4D35933D39E8B54524B4A5484894_inline (InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * __this, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_ChallengeName(Amazon.CognitoIdentityProvider.ChallengeNameType)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_ChallengeName_mC3307C66F44FF605F9672F763CD0C77DD56ABCD2_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___value0, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_ClientId(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_ClientId_m5CD35765D3E001E538AF5FE4BF61469ADDA1EF71_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse::get_Session()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InitiateAuthResponse_get_Session_mDCD4361A025B34D001F279629CB1FABD4838B3DC_inline (InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * __this, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_Session(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_Session_m9A001CCF2391CD45F8FCCF2FF12669691744DB4D_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::set_ChallengeResponses(System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_ChallengeResponses_mB198D0AEB28A31F3BC175BC70CF2C82DCBA1BAAA_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_PoolID(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_PoolID_m989C7C62DC1F23F7DCFBF451FF8D0252E127835A_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_ClientID(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_ClientID_m511971B0104BDE5498C581F7F371A00878B73222_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_ClientSecret(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_ClientSecret_mA43DA79CA3AE2C9E11018C6FDB6624DE967F24AB_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_Provider(Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_Provider_m475CC10ECC82018ABDC254A949C3DA2864A8A30B_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUserPool::get_Provider()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* CognitoUserPool_get_Provider_mDA59C9E589F8E745E08190C267073925670785F0_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_IdToken(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_IdToken_m16577680A752E865B717BA8AD0B964C0B1F1A737_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_AccessToken(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_AccessToken_mA221525DBBEB9015769F330E192C39E601DEB16B_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_RefreshToken(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_RefreshToken_m4E68B941BBCE29ACA651470C2C9D556B8BB01424_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_IssuedTime(System.DateTime)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_IssuedTime_m837B035F367ADE8CB0805D8E2CD1255F4508CBAD_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_ExpirationTime(System.DateTime)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_ExpirationTime_mF772E4FABBD13882FFE731AD15AD2F7E1AD89408_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::set_HmacSha256(System.Security.Cryptography.HMACSHA256)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * ___value0, const RuntimeMethod* method);
// System.Security.Cryptography.HMACSHA256 Amazon.Extensions.CognitoAuthentication.HkdfSha256::get_HmacSha256()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::set_Prk(System.Byte[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HkdfSha256_set_Prk_m3BFB02703CF8FF382308FD49349D7A8753ABF281_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___value0, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::get_Prk()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* HkdfSha256_get_Prk_m3CB2E31FFA5572F37FB59373AE5E4B955B967F8A_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.String Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::get_Password()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869_inline (InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * __this, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mAD2F05A24C92A657CBCA8C43A9A373C53739A283 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method);
// System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger> Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::CreateAaTuple()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * AuthenticationHelper_CreateAaTuple_m953054F2E0C5CB363C35BC45F7DD4C854265716F (const RuntimeMethod* method);
// Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest Amazon.Extensions.CognitoAuthentication.CognitoUser::CreateSrpAuthRequest(System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * CognitoUser_CreateSrpAuthRequest_mBE2390FC87AA9842064CADD5EB303FA6AF9ECC40 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___tupleAa0, const RuntimeMethod* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<!0> System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>::ConfigureAwait(System.Boolean)
inline ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC  Task_1_ConfigureAwait_m260BD4536DEF805CC8207699944516A90738A2C1 (Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 * __this, bool ___continueOnCapturedContext0, const RuntimeMethod* method)
{
	return ((  ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC  (*) (Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 *, bool, const RuntimeMethod*))Task_1_ConfigureAwait_m0C99499DCC096AEE2A6AD075391C61037CC3DAA1_gshared)(__this, ___continueOnCapturedContext0, method);
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<!0> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>::GetAwaiter()
inline ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  ConfiguredTaskAwaitable_1_GetAwaiter_mC0B99DDC353B6E7C81CA27D2D8B1A9C437AE56BF_inline (ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC * __this, const RuntimeMethod* method)
{
	return ((  ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  (*) (ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC *, const RuntimeMethod*))ConfiguredTaskAwaitable_1_GetAwaiter_mFCE2327CEE19607ABB1CDCC8A6B145BDCF9820BC_gshared_inline)(__this, method);
}
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>::get_IsCompleted()
inline bool ConfiguredTaskAwaiter_get_IsCompleted_m755C1F32EDA0DC621015D811895E509978A958DD (ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 *, const RuntimeMethod*))ConfiguredTaskAwaiter_get_IsCompleted_m5E3746D1B0661A5BCD45816E83766F228A077D20_gshared)(__this, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>,Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81>(!!0&,!!1&)
inline void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6924C8F15A0B905ED3807EAB4462A1953849557C (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 * ___awaiter0, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 *, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m08ECA315D1A00BEA0C00C4BB9C84DEC65F43F30F_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// !0 System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse>::GetResult()
inline InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * ConfiguredTaskAwaiter_GetResult_mCF811B9F3DF810C629A828BA855771A78CC124A9 (ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 * __this, const RuntimeMethod* method)
{
	return ((  InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * (*) (ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 *, const RuntimeMethod*))ConfiguredTaskAwaiter_GetResult_mD385ED6B1C12DC6353D50409731FB1729FFD9FA5_gshared)(__this, method);
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::UpdateUsernameAndSecretHash(System.Collections.Generic.IDictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_UpdateUsernameAndSecretHash_m80C753373FEE74947273595CB468782F8C359126 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, RuntimeObject* ___challengeParameters0, const RuntimeMethod* method);
// Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest Amazon.Extensions.CognitoAuthentication.CognitoUser::CreateSrpPasswordVerifierAuthRequest(Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * ___challenge0, String_t* ___password1, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___tupleAa2, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest::get_ChallengeResponses()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * RespondToAuthChallengeRequest_get_ChallengeResponses_mF628D1CC2388ABC64C8AA8CCB942462F1CC347F5_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, const RuntimeMethod* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<!0> System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>::ConfigureAwait(System.Boolean)
inline ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB  Task_1_ConfigureAwait_m5C984C4A2243EE418F8EF068BB84258F5B257C7E (Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 * __this, bool ___continueOnCapturedContext0, const RuntimeMethod* method)
{
	return ((  ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB  (*) (Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 *, bool, const RuntimeMethod*))Task_1_ConfigureAwait_m0C99499DCC096AEE2A6AD075391C61037CC3DAA1_gshared)(__this, ___continueOnCapturedContext0, method);
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<!0> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>::GetAwaiter()
inline ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  ConfiguredTaskAwaitable_1_GetAwaiter_m2B667096D2B4B58D37EA4D9C4858F5A45028C935_inline (ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB * __this, const RuntimeMethod* method)
{
	return ((  ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  (*) (ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB *, const RuntimeMethod*))ConfiguredTaskAwaitable_1_GetAwaiter_mFCE2327CEE19607ABB1CDCC8A6B145BDCF9820BC_gshared_inline)(__this, method);
}
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>::get_IsCompleted()
inline bool ConfiguredTaskAwaiter_get_IsCompleted_mC400DFEA6D067D16AF92FF753F90D1309A3F33CE (ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C *, const RuntimeMethod*))ConfiguredTaskAwaiter_get_IsCompleted_m5E3746D1B0661A5BCD45816E83766F228A077D20_gshared)(__this, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>,Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81>(!!0&,!!1&)
inline void AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_mB6450D099253BAA88CB077A27F0F2CE29BC7FCBD (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C * ___awaiter0, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C *, U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m08ECA315D1A00BEA0C00C4BB9C84DEC65F43F30F_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// !0 System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse>::GetResult()
inline RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * ConfiguredTaskAwaiter_GetResult_m8EC35D81407E7E6F1059CF69E3F9BCE6118B9740 (ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C * __this, const RuntimeMethod* method)
{
	return ((  RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * (*) (ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C *, const RuntimeMethod*))ConfiguredTaskAwaiter_GetResult_mD385ED6B1C12DC6353D50409731FB1729FFD9FA5_gshared)(__this, method);
}
// Amazon.CognitoIdentityProvider.ChallengeNameType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_ChallengeName()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * RespondToAuthChallengeResponse_get_ChallengeName_m9DDD431CE7D58ABBF4D71D33B9B3DA93D1931475_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method);
// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_AuthenticationResult()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * RespondToAuthChallengeResponse_get_AuthenticationResult_mEA1392BB5EEE1EAF8C6CA2A4295ADC1CDC6C9FC0_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method);
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::UpdateSessionIfAuthenticationComplete(Amazon.CognitoIdentityProvider.ChallengeNameType,Amazon.CognitoIdentityProvider.Model.AuthenticationResultType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_UpdateSessionIfAuthenticationComplete_mAFC4BB94CC2465C1AE868AC184556708D60AB428 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___challengeName0, AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___authResult1, const RuntimeMethod* method);
// System.String Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_Session()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* RespondToAuthChallengeResponse_get_Session_mE00CF70381F77B441D8F0D6CAC1E8EBF7A262D69_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse::get_ChallengeParameters()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * RespondToAuthChallengeResponse_get_ChallengeParameters_mDCF21DF93EAE302BE3A1851D6631B5033C6BDB87_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method);
// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::get_ResponseMetadata()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * AmazonWebServiceResponse_get_ResponseMetadata_m1BDDB043FE9F44E076FBED6BF4D08BE2D6ACCA1D_inline (AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A * __this, const RuntimeMethod* method);
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.ResponseMetadata::get_Metadata()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ResponseMetadata_get_Metadata_m0B17E9487DF8F850A0D9D130F0EBFCD83BD038C1 (ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IDictionary`2<!0,!1>)
inline void Dictionary_2__ctor_m3A3DE48426936CEB09FE89B327E6D0A4B6A888C9 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, RuntimeObject* ___dictionary0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_m15FB53DEA7CD6487FD59ED68D6DB681FD332025C_gshared)(__this, ___dictionary0, method);
}
// System.Void Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::.ctor(System.String,Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,Amazon.CognitoIdentityProvider.ChallengeNameType,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthFlowResponse__ctor_m91D7AB173C699D4A5387CDC348C62C48E10B4F6B (AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * __this, String_t* ___sessionId0, AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___authenticationResult1, ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___challengeName2, RuntimeObject* ___challengeParameters3, RuntimeObject* ___clientMetadata4, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::SetException(System.Exception)
inline void AsyncTaskMethodBuilder_1_SetException_m38E596D3515939C2F19738981612DA38710F6C0E (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, Exception_t * ___exception0, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, Exception_t *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_SetException_m29521EB618E38AF72FF0C4094070C1489F4129B3_gshared)(__this, ___exception0, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::SetResult(!0)
inline void AsyncTaskMethodBuilder_1_SetResult_mAB1FB9ECA6777B761CB26DB9A9D1A72EE788DBF5 (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * ___result0, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 *, const RuntimeMethod*))AsyncTaskMethodBuilder_1_SetResult_m3E4AB12877D4FE377F26708CF6899C49360007FA_gshared)(__this, ___result0, method);
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209 (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
inline void AsyncTaskMethodBuilder_1_SetStateMachine_m6E3B2ACC94057E1637984A4678462C5B8E210FF1 (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method)
{
	((  void (*) (AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *, RuntimeObject*, const RuntimeMethod*))AsyncTaskMethodBuilder_1_SetStateMachine_m736C84D61B4AB2FCD150BD3945C6874471A9224D_gshared)(__this, ___stateMachine0, method);
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1 (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::.ctor(System.String,Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,Amazon.CognitoIdentityProvider.ChallengeNameType,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthFlowResponse__ctor_m91D7AB173C699D4A5387CDC348C62C48E10B4F6B (AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * __this, String_t* ___sessionId0, AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___authenticationResult1, ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___challengeName2, RuntimeObject* ___challengeParameters3, RuntimeObject* ___clientMetadata4, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sessionId0;
		__this->set_U3CSessionIDU3Ek__BackingField_0(L_0);
		ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_1 = ___challengeName2;
		__this->set_U3CChallengeNameU3Ek__BackingField_1(L_1);
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_2 = ___authenticationResult1;
		__this->set_U3CAuthenticationResultU3Ek__BackingField_2(L_2);
		RuntimeObject* L_3 = ___challengeParameters3;
		__this->set_U3CChallengeParametersU3Ek__BackingField_3(L_3);
		RuntimeObject* L_4 = ___clientMetadata4;
		__this->set_U3CClientMetadataU3Ek__BackingField_4(L_4);
		return;
	}
}
// Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::get_AuthenticationResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * AuthFlowResponse_get_AuthenticationResult_m69D5253C4F74C2918CF23F56A945B69AC9A51F52 (AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * __this, const RuntimeMethod* method)
{
	{
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_0 = __this->get_U3CAuthenticationResultU3Ek__BackingField_2();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthenticationHelper__cctor_m3AAA58B4C6C214BBE2F2E48208EA0C0022EE497F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86202678AB0A41B628DDEEB4DD6DC622B9C6ED5E);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	{
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_0;
		L_0 = BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9(_stringLiteral86202678AB0A41B628DDEEB4DD6DC622B9C6ED5E, /*hidden argument*/NULL);
		((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->set_N_0(L_0);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_1;
		memset((&L_1), 0, sizeof(L_1));
		BigInteger__ctor_m104B492675CC61CB48D17E18900DF23DCB7408D4((&L_1), 2, /*hidden argument*/NULL);
		((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->set_g_1(L_1);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_2 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)2);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_3 = L_2;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_4 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5;
		L_5 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_5);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_6 = L_3;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_7 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_g_1();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
		L_8 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_8);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9;
		L_9 = CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368(L_6, /*hidden argument*/NULL);
		V_0 = L_9;
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_10;
		L_10 = CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = V_0;
		NullCheck(L_10);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12;
		L_12 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_10, L_11, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_13;
		L_13 = BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5(L_12, /*hidden argument*/NULL);
		((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->set_k_2(L_13);
		return;
	}
}
// System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger> Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::CreateAaTuple()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * AuthenticationHelper_CreateAaTuple_m953054F2E0C5CB363C35BC45F7DD4C854265716F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_0;
		L_0 = AuthenticationHelper_CreateBigIntegerRandom_mDF7B2E5C2136FF32F7F58A1EFF9E0732ACB31170(/*hidden argument*/NULL);
		V_0 = L_0;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_1 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_g_1();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_2 = V_0;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_3 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_4;
		L_4 = BigInteger_ModPow_mD3EF2940961384A83E1004725E9A14DE4EB8E4C6(L_1, L_2, L_3, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_5 = V_0;
		Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_6;
		L_6 = Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214(L_4, L_5, /*hidden argument*/Tuple_Create_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_TisBigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_mBBA5D78154F1BB29E56E22C7B35A65133BE5D214_RuntimeMethod_var);
		return L_6;
	}
}
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::AuthenticateUser(System.String,System.String,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1 (String_t* ___username0, String_t* ___password1, String_t* ___poolName2, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___tupleAa3, String_t* ___saltString4, String_t* ___srpbString5, String_t* ___secretBlockBase646, String_t* ___formattedTimestamp7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_2;
	memset((&V_2), 0, sizeof(V_2));
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_5;
	memset((&V_5), 0, sizeof(V_5));
	HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * V_6 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		String_t* L_0 = ___srpbString5;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_1;
		L_1 = BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_3 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_4;
		L_4 = BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E(L_2, L_3, /*hidden argument*/NULL);
		V_5 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_5;
		L_5 = BigInteger_get_Zero_mE10EE4CF6BAD05BC10D0D5012ECDCC1B81E438BF_inline(/*hidden argument*/NULL);
		bool L_6;
		L_6 = BigInteger_Equals_m05302B320532185A473A0E9CBA352FA30BB43C9A((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 *)(&V_5), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_7 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D(L_7, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7D5CFB0848E7F707163817A374D1B4BFC889BDC0)), ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralA4CDF053D3B972088B7FFF38937974A6AB653925)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1_RuntimeMethod_var)));
	}

IL_0033:
	{
		String_t* L_8 = ___secretBlockBase646;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9;
		L_9 = Convert_FromBase64String_mB2E4E2CD03B34DB7C2665694D5B2E967BC81E9A8(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = ___saltString4;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_11;
		L_11 = BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		String_t* L_12 = ___username0;
		String_t* L_13 = ___password1;
		String_t* L_14 = ___poolName2;
		Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_15 = ___tupleAa3;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_16 = V_0;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18;
		L_18 = AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7(L_12, L_13, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_19 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)4);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_20 = L_19;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_21;
		L_21 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_22 = ___poolName2;
		NullCheck(L_21);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_23;
		L_23 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_21, L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_23);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_24 = L_20;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_25;
		L_25 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_26 = ___username0;
		NullCheck(L_25);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_27;
		L_27 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_25, L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_27);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_28 = L_24;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_29 = V_1;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_29);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_30 = L_28;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_31;
		L_31 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_32 = ___formattedTimestamp7;
		NullCheck(L_31);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_33;
		L_33 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_31, L_32);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_33);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_34;
		L_34 = CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368(L_30, /*hidden argument*/NULL);
		V_4 = L_34;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_35 = V_3;
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_36 = (HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD *)il2cpp_codegen_object_new(HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		HMACSHA256__ctor_m0836F5FA98464B745CF97CC102DBE4497E17BEA9(L_36, L_35, /*hidden argument*/NULL);
		V_6 = L_36;
	}

IL_0093:
	try
	{ // begin try (depth: 1)
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_37 = V_6;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_38 = V_4;
		NullCheck(L_37);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_39;
		L_39 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_37, L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		IL2CPP_LEAVE(0xAC, FINALLY_00a0);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a0;
	}

FINALLY_00a0:
	{ // begin finally (depth: 1)
		{
			HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_40 = V_6;
			if (!L_40)
			{
				goto IL_00ab;
			}
		}

IL_00a4:
		{
			HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_41 = V_6;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_41);
		}

IL_00ab:
		{
			IL2CPP_END_FINALLY(160)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(160)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
	}

IL_00ac:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_42 = V_7;
		return L_42;
	}
}
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::GetPasswordAuthenticationKey(System.String,System.String,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>,System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7 (String_t* ___userID0, String_t* ___userPassword1, String_t* ___poolName2, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___Aa3, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___B4, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___salt5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE5A4549B4636DF6B4AC06434A3927594722F9E5);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_5;
	memset((&V_5), 0, sizeof(V_5));
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_0 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)2);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_1 = L_0;
		Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_2 = ___Aa3;
		NullCheck(L_2);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_3;
		L_3 = Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_inline(L_2, /*hidden argument*/Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_RuntimeMethod_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4;
		L_4 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_4);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_5 = L_1;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_6 = ___B4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7;
		L_7 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_7);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
		L_8 = CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368(L_5, /*hidden argument*/NULL);
		V_0 = L_8;
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_9;
		L_9 = CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_0;
		NullCheck(L_9);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11;
		L_11 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_9, L_10, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_12;
		L_12 = BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_13;
		L_13 = BigInteger_get_Zero_mE10EE4CF6BAD05BC10D0D5012ECDCC1B81E438BF_inline(/*hidden argument*/NULL);
		bool L_14;
		L_14 = BigInteger_Equals_m05302B320532185A473A0E9CBA352FA30BB43C9A((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 *)(&V_1), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_004e;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_15 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_15, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral049AE51AD9A6D7846326B9188829C2D982D399B3)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7_RuntimeMethod_var)));
	}

IL_004e:
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_16 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)4);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_17 = L_16;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_18;
		L_18 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_19 = ___poolName2;
		NullCheck(L_18);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20;
		L_20 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_18, L_19);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_20);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_21 = L_17;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_22;
		L_22 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_23 = ___userID0;
		NullCheck(L_22);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24;
		L_24 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_22, L_23);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_24);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_25 = L_21;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_26;
		L_26 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		NullCheck(L_26);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_27;
		L_27 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_26, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_27);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_28 = L_25;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_29;
		L_29 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_30 = ___userPassword1;
		NullCheck(L_29);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_31;
		L_31 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_30);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_31);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_32;
		L_32 = CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368(L_28, /*hidden argument*/NULL);
		V_2 = L_32;
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_33;
		L_33 = CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_34 = V_2;
		NullCheck(L_33);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_35;
		L_35 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_33, L_34, /*hidden argument*/NULL);
		V_3 = L_35;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_36 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)2);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_37 = L_36;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_38 = ___salt5;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_39;
		L_39 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_39);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_39);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_40 = L_37;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_41 = V_3;
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(1), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_41);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_42;
		L_42 = CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368(L_40, /*hidden argument*/NULL);
		V_4 = L_42;
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_43;
		L_43 = CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_44 = V_4;
		NullCheck(L_43);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_45;
		L_45 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_43, L_44, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_46;
		L_46 = BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5(L_45, /*hidden argument*/NULL);
		V_5 = L_46;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_47 = ___B4;
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_48 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_k_2();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_49 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_g_1();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_50 = V_5;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_51 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_52;
		L_52 = BigInteger_ModPow_mD3EF2940961384A83E1004725E9A14DE4EB8E4C6(L_49, L_50, L_51, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_53;
		L_53 = BigInteger_op_Multiply_m2C693A37027C26E76BFF4F974553ACA2D8DCC067(L_48, L_52, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_54;
		L_54 = BigInteger_op_Subtraction_m06627EF453D41B140BE6604D5C6D8F048B9D4B7C(L_47, L_53, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_55 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_56;
		L_56 = BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E(L_54, L_55, /*hidden argument*/NULL);
		Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_57 = ___Aa3;
		NullCheck(L_57);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_58;
		L_58 = Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_inline(L_57, /*hidden argument*/Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_RuntimeMethod_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_59 = V_1;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_60 = V_5;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_61;
		L_61 = BigInteger_op_Multiply_m2C693A37027C26E76BFF4F974553ACA2D8DCC067(L_59, L_60, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_62;
		L_62 = BigInteger_op_Addition_m55A6D35945F71B25A38BD6688EFAC3FAF10C46B9(L_58, L_61, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_63 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_64;
		L_64 = BigInteger_ModPow_mD3EF2940961384A83E1004725E9A14DE4EB8E4C6(L_56, L_62, L_63, /*hidden argument*/NULL);
		V_6 = L_64;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_65 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_66;
		L_66 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_65, /*hidden argument*/NULL);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_67 = V_6;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_68;
		L_68 = BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0(L_67, /*hidden argument*/NULL);
		HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * L_69 = (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F *)il2cpp_codegen_object_new(HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F_il2cpp_TypeInfo_var);
		HkdfSha256__ctor_mF448325D81511381115C9D024636F8386C6C2697(L_69, L_66, L_68, /*hidden argument*/NULL);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_70;
		L_70 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		NullCheck(L_70);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_71;
		L_71 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_70, _stringLiteralCE5A4549B4636DF6B4AC06434A3927594722F9E5);
		NullCheck(L_69);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_72;
		L_72 = HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F(L_69, L_71, ((int32_t)16), /*hidden argument*/NULL);
		return L_72;
	}
}
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::CreateBigIntegerRandom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  AuthenticationHelper_CreateBigIntegerRandom_mDF7B2E5C2136FF32F7F58A1EFF9E0732ACB31170 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)128));
		V_0 = L_0;
		RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * L_1;
		L_1 = RandomNumberGenerator_Create_m04A5418F8572F0498EE0659633B4C0620CB55721(/*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * L_2 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = V_0;
		NullCheck(L_2);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_2, L_3);
		IL2CPP_LEAVE(0x24, FINALLY_001a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		{
			RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * L_4 = V_1;
			if (!L_4)
			{
				goto IL_0023;
			}
		}

IL_001d:
		{
			RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * L_5 = V_1;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_5);
		}

IL_0023:
		{
			IL2CPP_END_FINALLY(26)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x24, IL_0024)
	}

IL_0024:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = V_0;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_7;
		L_7 = BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::FromUnsignedLittleEndianHex(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9 (String_t* ___hex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___hex0;
		String_t* L_1;
		L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_2;
		L_2 = BigInteger_Parse_mCF195A7CFD256BCF1B8450A31ED9BA326C6C2337(L_1, ((int32_t)515), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::TrueMod(System.Numerics.BigInteger,System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___self0, BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___other1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_0 = ___self0;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_1 = ___other1;
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_2;
		L_2 = BigInteger_op_Modulus_m90CD4DEC2293E0D71B73A33720AEE6CFEDECBE2E(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3;
		L_3 = BigInteger_get_Sign_mB7A2E5A1C237EB3532F5AA6C13511B696B3EA660((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 *)(&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_4 = V_0;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_5 = ___other1;
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_6;
		L_6 = BigInteger_op_Addition_m55A6D35945F71B25A38BD6688EFAC3FAF10C46B9(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_001a:
	{
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_7 = V_0;
		return L_7;
	}
}
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::ToBigEndianByteArray(System.Numerics.BigInteger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0 (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  ___self0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0;
		L_0 = BigInteger_ToByteArray_mE55CBE13ADA8E09AFE2D10FF9AD2F373E4000B7B((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 *)(&___self0), /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1;
		L_1 = BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A(L_0, /*hidden argument*/BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_RuntimeMethod_var);
		return L_1;
	}
}
// System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::FromUnsignedBigEndian(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___bytes0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1;
		L_1 = BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A(L_0, /*hidden argument*/BigIntegerExtensions_Reverse_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8FDC6834228A8218A0E7E280E368B24F8BFE1C8A_RuntimeMethod_var);
		V_0 = L_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = V_0;
		NullCheck(L_2);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))), (int32_t)1)));
		V_1 = L_3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = V_0;
		NullCheck(L_6);
		Array_Copy_m40103AA97DC582C557B912CF4BBE86A4D166F803((RuntimeArray *)(RuntimeArray *)L_4, (RuntimeArray *)(RuntimeArray *)L_5, ((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))), /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = V_1;
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_8;
		memset((&L_8), 0, sizeof(L_8));
		BigInteger__ctor_mE86998DE086542EC44032A9A6F3978C08DB8DC1D((&L_8), L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Security.Cryptography.SHA256 Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::get_Sha256()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_0 = ((CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_il2cpp_TypeInfo_var))->get_sha256_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_1;
		L_1 = SHA256_Create_mA93565DD491DC3380FB602957B00EA909FEB8A20(/*hidden argument*/NULL);
		((CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_il2cpp_TypeInfo_var))->set_sha256_0(L_1);
	}

IL_0011:
	{
		SHA256_t7F98FBD5AA9E2F4AC1A5C13FF7122AFF1A920452 * L_2 = ((CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_il2cpp_TypeInfo_var))->get_sha256_0();
		return L_2;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::GetUserPoolSecretHash(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193 (String_t* ___userID0, String_t* ___clientID1, String_t* ___clientSecret2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	{
		String_t* L_0 = ___userID0;
		String_t* L_1 = ___clientID1;
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_3;
		L_3 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_4 = ___clientSecret2;
		NullCheck(L_3);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5;
		L_5 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, L_4);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_6;
		L_6 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_7 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
		L_8 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, L_7);
		V_1 = L_8;
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_9 = (HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD *)il2cpp_codegen_object_new(HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		HMACSHA256__ctor_m0836F5FA98464B745CF97CC102DBE4497E17BEA9(L_9, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_1;
		NullCheck(L_9);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11;
		L_11 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		String_t* L_12;
		L_12 = Convert_ToBase64String_mE6E1FE504EF1E99DB2F8B92180A82A5F1512EF6A(L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::CombineBytes(System.Byte[][])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368 (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* ___values0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		V_2 = 0;
		V_3 = 0;
		goto IL_0014;
	}

IL_0008:
	{
		int32_t L_0 = V_0;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_1 = ___values0;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length)))));
		int32_t L_5 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0014:
	{
		int32_t L_6 = V_3;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_7 = ___values0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_8 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_8);
		V_1 = L_9;
		V_4 = 0;
		goto IL_0047;
	}

IL_0026:
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_10 = ___values0;
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = V_1;
		int32_t L_15 = V_2;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_16 = ___values0;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_13, 0, (RuntimeArray *)(RuntimeArray *)L_14, L_15, ((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length))), /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_21 = ___values0;
		int32_t L_22 = V_4;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))));
		int32_t L_25 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_26 = V_4;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_27 = ___values0;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_27)->max_length))))))
		{
			goto IL_0026;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_28 = V_1;
		return L_28;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::ServiceClientBeforeRequestEvent(System.Object,Amazon.Runtime.RequestEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C (RuntimeObject * ___sender0, RequestEventArgs_tE1FA7940E62EB2306034D1E8ABD47B2D8ED44A56 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral48C75149E263D08DBE3F3CB86DF011FA96C010AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1AF5447134F86E81C58631C3B6D93E2C8F97B1A);
		s_Il2CppMethodInitialized = true;
	}
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		RequestEventArgs_tE1FA7940E62EB2306034D1E8ABD47B2D8ED44A56 * L_0 = ___e1;
		V_0 = ((WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A *)IsInstClass((RuntimeObject*)L_0, WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_il2cpp_TypeInfo_var));
		WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3;
		L_3 = WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4;
		L_4 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(4 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_3, _stringLiteral48C75149E263D08DBE3F3CB86DF011FA96C010AF);
		if (L_4)
		{
			goto IL_001d;
		}
	}

IL_001c:
	{
		return;
	}

IL_001d:
	{
		String_t* L_5;
		L_5 = CognitoAuthHelper_GetAssemblyFileVersion_mD4696F4A47F83B7C0A8213149128CC6167F81428(/*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralB1AF5447134F86E81C58631C3B6D93E2C8F97B1A, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * L_7 = V_0;
		NullCheck(L_7);
		RuntimeObject* L_8;
		L_8 = WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9;
		L_9 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(0 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_8, _stringLiteral48C75149E263D08DBE3F3CB86DF011FA96C010AF);
		String_t* L_10 = V_1;
		NullCheck(L_9);
		bool L_11;
		L_11 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_9, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_006b;
		}
	}
	{
		WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * L_12 = V_0;
		NullCheck(L_12);
		RuntimeObject* L_13;
		L_13 = WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5_inline(L_12, /*hidden argument*/NULL);
		WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * L_14 = V_0;
		NullCheck(L_14);
		RuntimeObject* L_15;
		L_15 = WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5_inline(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16;
		L_16 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(0 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_15, _stringLiteral48C75149E263D08DBE3F3CB86DF011FA96C010AF);
		String_t* L_17 = V_1;
		String_t* L_18;
		L_18 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(1 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_13, _stringLiteral48C75149E263D08DBE3F3CB86DF011FA96C010AF, L_18);
	}

IL_006b:
	{
		return;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::GetAssemblyFileVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoAuthHelper_GetAssemblyFileVersion_mD4696F4A47F83B7C0A8213149128CC6167F81428 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D54E959817188DBAD9E65FA3DB55F06B70F5E3C);
		s_Il2CppMethodInitialized = true;
	}
	AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * V_0 = NULL;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (CognitoAuthHelper_t378B94AAE0290728CE16CB5BE2441A80E54A4A29_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F * L_2;
		L_2 = IntrospectionExtensions_GetTypeInfo_m77034F8576BE695819427C13103C591277C1B636(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Assembly_t * L_3;
		L_3 = VirtFuncInvoker0< Assembly_t * >::Invoke(24 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_2);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_4 = { reinterpret_cast<intptr_t> (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F_0_0_0_var) };
		Type_t * L_5;
		L_5 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_4, /*hidden argument*/NULL);
		Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71 * L_6;
		L_6 = CustomAttributeExtensions_GetCustomAttribute_m6CC58E7580DB6F8280968AEF3CD8BD8A2BF27662(L_3, L_5, /*hidden argument*/NULL);
		V_0 = ((AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)IsInstSealed((RuntimeObject*)L_6, AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F_il2cpp_TypeInfo_var));
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = AssemblyFileVersionAttribute_get_Version_m56763C51185F40666C40AF57A0E83A1B446C7597_inline(L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0033:
	{
		return _stringLiteral5D54E959817188DBAD9E65FA3DB55F06B70F5E3C;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoConstants__cctor_mB8079C41D65E9D1DA7F75A388EEF26F9114D45CD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0BE6D6FEA9C0036EEDEFFD38B2D1C4EACC2B9D2F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1F1268E1AE201354BBC8D00D49BEAC846F12EDF7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3581B9DAF560030E2E31AE65668D9C6BEAA6728D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral44D2155A9A3200DB4F66438C4A7020431E61BB80);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4CB81BFE27645382DC38DCACB8A2C37AB347D6D2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51921D99887DD5ED233F87333EF648AE91A8BF7C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral530150E04E6EC41BEE7DA49EB9C12C5E0289840D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral560B461CD3B3EC699D4EEE2BF321914A093EBDB9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B02B1E1487008AA8DA0EBBF4799CBC796FE4D62);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral736AF32BCDCEA98E53845EF220EE293D0B73DF58);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral842B8C7345E8AE5608EE332378D4489B8AF9C37C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8616432AB4B76B87A73EC2E9EDA0A66F90F81D79);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C3DEEC0531EF0C653C66F20E39D7E107A8DFC4D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA1C3EDDE2152361A22D3D0D87C064EC546264D1C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAB16D8A617F31CEA5526DAECE9ED6F7764595D82);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB9759268181E392BB5A3A5F594B01AD942FC7064);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0088BD9BDDD69F53812075FEBE4DE7AD5590ED0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC13B65C06BB3A788859D79A63DC8C530F152B2FB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC288B7612977CDB79FF44E3835AA0B4F6C422C31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCFB3EFC4BEA99FC338B21324C64F1B4AEBD47192);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD02A4A4D1C4146EC9570C4228E7416949C61E664);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD444B40C468D66B19259CD3EFE1FF0409F040706);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDCE4227FFA1EC695E039F9C2BB1F760A67C3D925);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD096B8301B9807B9045A248F66BF18A2F33EC33);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE8C29C59AD2C5A7B688373F667558973F9681B6B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEDAAB5C5D4BC79996A40BAC363247725901131D9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFA46A75AEDB98EC4BA344FC277C99D2CBDE6A9B4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFBCFE8AE685D68F390182744A8EE501715BF4009);
		s_Il2CppMethodInitialized = true;
	}
	{
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamSrpA_0(_stringLiteralCFB3EFC4BEA99FC338B21324C64F1B4AEBD47192);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamSrpB_1(_stringLiteralB9759268181E392BB5A3A5F594B01AD942FC7064);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamSecretHash_2(_stringLiteral560B461CD3B3EC699D4EEE2BF321914A093EBDB9);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamUsername_3(_stringLiteralFA46A75AEDB98EC4BA344FC277C99D2CBDE6A9B4);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamChallengeName_4(_stringLiteral4CB81BFE27645382DC38DCACB8A2C37AB347D6D2);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamSalt_5(_stringLiteralA1C3EDDE2152361A22D3D0D87C064EC546264D1C);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamSecretBlock_6(_stringLiteralDCE4227FFA1EC695E039F9C2BB1F760A67C3D925);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamUserIDSrp_7(_stringLiteral736AF32BCDCEA98E53845EF220EE293D0B73DF58);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamRefreshToken_8(_stringLiteralE8C29C59AD2C5A7B688373F667558973F9681B6B);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamPassSecretBlock_9(_stringLiteralC13B65C06BB3A788859D79A63DC8C530F152B2FB);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamPassSignature_10(_stringLiteral3581B9DAF560030E2E31AE65668D9C6BEAA6728D);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamTimestamp_11(_stringLiteral1F1268E1AE201354BBC8D00D49BEAC846F12EDF7);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamDeliveryDest_12(_stringLiteral6B02B1E1487008AA8DA0EBBF4799CBC796FE4D62);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamDeliveryMed_13(_stringLiteral8616432AB4B76B87A73EC2E9EDA0A66F90F81D79);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamSmsMfaCode_14(_stringLiteral530150E04E6EC41BEE7DA49EB9C12C5E0289840D);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamDeviceKey_15(_stringLiteralFBCFE8AE685D68F390182744A8EE501715BF4009);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamUserAttrs_16(_stringLiteralDD096B8301B9807B9045A248F66BF18A2F33EC33);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamRequiredAttrs_17(_stringLiteralEDAAB5C5D4BC79996A40BAC363247725901131D9);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamUserAttrPrefix_18(_stringLiteralC288B7612977CDB79FF44E3835AA0B4F6C422C31);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamNewPassword_19(_stringLiteral0BE6D6FEA9C0036EEDEFFD38B2D1C4EACC2B9D2F);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_ChlgParamPassword_20(_stringLiteralAB16D8A617F31CEA5526DAECE9ED6F7764595D82);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_UserAttrEmail_21(_stringLiteral51921D99887DD5ED233F87333EF648AE91A8BF7C);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_UserAttrPhoneNumber_22(_stringLiteralD444B40C468D66B19259CD3EFE1FF0409F040706);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_DeviceAttrName_23(_stringLiteral44D2155A9A3200DB4F66438C4A7020431E61BB80);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_DeviceAttrRemembered_24(_stringLiteralC0088BD9BDDD69F53812075FEBE4DE7AD5590ED0);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_DeviceAttrNotRemembered_25(_stringLiteralD02A4A4D1C4146EC9570C4228E7416949C61E664);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_DeviceChlgParamSalt_26(_stringLiteral842B8C7345E8AE5608EE332378D4489B8AF9C37C);
		((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->set_DeviceChlgParamVerifier_27(_stringLiteral9C3DEEC0531EF0C653C66F20E39D7E107A8DFC4D);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Amazon.Extensions.CognitoAuthentication.CognitoDevice::get_DeviceKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094 (CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceKeyU3Ek__BackingField_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_ClientSecret()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CClientSecretU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_ClientSecret(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_ClientSecret_m0013271AF0F323BFF73DF5B7E422946882F58364 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientSecretU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_SecretHash()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUser_get_SecretHash_m1FCB2073B2703446B01998388FFD77B5056B8A98 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CSecretHashU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_SecretHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSecretHashU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_SessionTokens(Amazon.Extensions.CognitoAuthentication.CognitoUserSession)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * ___value0, const RuntimeMethod* method)
{
	{
		CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * L_0 = ___value0;
		__this->set_U3CSessionTokensU3Ek__BackingField_2(L_0);
		return;
	}
}
// Amazon.Extensions.CognitoAuthentication.CognitoDevice Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Device()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_0 = __this->get_U3CDeviceU3Ek__BackingField_3();
		return L_0;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_UserID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUser_get_UserID_m858BEACD4ACC7D4EC6462D0A2229A7B4EA6EF72F (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUserIDU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_UserID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_UserID_mA7CF427A7E4ED35C62A09920A8D6D3E7B5A20266 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUserIDU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Username()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Username(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_UserPool(Amazon.Extensions.CognitoAuthentication.CognitoUserPool)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_UserPool_mEC8B2A8E3CE2AC7A66ADF8A201F4C2BA6DCCF9D0 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * ___value0, const RuntimeMethod* method)
{
	{
		CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * L_0 = ___value0;
		__this->set_U3CUserPoolU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_ClientID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CClientIDU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_ClientID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_ClientID_m7D5C25601A2033D3887954A0A64B7DEBF0A8233F (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientIDU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Status(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_Status_m6AC74726AEECA36363F06AFC578491E8CB06A54A (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatusU3Ek__BackingField_8(L_0);
		return;
	}
}
// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Provider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CProviderU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Provider(Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_Provider_m384C1E82B82132EAA78A0C0D689A7EDF24C0CBBA (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CProviderU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Attributes(System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_Attributes_m6CFCBD03B13DA55D2275D8CD7F6DC9A25B5EF72A (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = ___value0;
		__this->set_U3CAttributesU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_PoolName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUser_get_PoolName_mB595FBE47D57E0782C042D1D3DF9FF6F32FA62AA (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPoolNameU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_PoolName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_set_PoolName_mB319666D9228CDCF803B56AE31006CB185FCBCAC (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPoolNameU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::.ctor(System.String,System.String,Amazon.Extensions.CognitoAuthentication.CognitoUserPool,Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser__ctor_mF9AE181160DA6741A661389AED15CCD6664AC209 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___userID0, String_t* ___clientID1, CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * ___pool2, RuntimeObject* ___provider3, String_t* ___clientSecret4, String_t* ___status5, String_t* ___username6, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___attributes7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C);
		s_Il2CppMethodInitialized = true;
	}
	AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 * V_0 = NULL;
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)il2cpp_codegen_object_new(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666(L_0, /*hidden argument*/Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var);
		__this->set_U3CAttributesU3Ek__BackingField_10(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * L_1 = ___pool2;
		NullCheck(L_1);
		String_t* L_2;
		L_2 = CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3;
		L_3 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_2, _stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0043;
		}
	}
	{
		CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * L_4 = ___pool2;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6_inline(L_4, /*hidden argument*/NULL);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_6 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_7 = L_6;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)95));
		NullCheck(L_5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8;
		L_8 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = 1;
		String_t* L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		CognitoUser_set_PoolName_mB319666D9228CDCF803B56AE31006CB185FCBCAC_inline(__this, L_10, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0043:
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_11 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_11, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralA1C11240057DA2500A6450044EFF8D777E00F26A)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CognitoUser__ctor_mF9AE181160DA6741A661389AED15CCD6664AC209_RuntimeMethod_var)));
	}

IL_004e:
	{
		String_t* L_12 = ___clientSecret4;
		CognitoUser_set_ClientSecret_m0013271AF0F323BFF73DF5B7E422946882F58364_inline(__this, L_12, /*hidden argument*/NULL);
		String_t* L_13 = ___clientSecret4;
		bool L_14;
		L_14 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_15 = ___userID0;
		String_t* L_16 = ___clientID1;
		String_t* L_17 = ___clientSecret4;
		String_t* L_18;
		L_18 = CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193(L_15, L_16, L_17, /*hidden argument*/NULL);
		CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3_inline(__this, L_18, /*hidden argument*/NULL);
	}

IL_006e:
	{
		String_t* L_19 = ___userID0;
		CognitoUser_set_UserID_mA7CF427A7E4ED35C62A09920A8D6D3E7B5A20266_inline(__this, L_19, /*hidden argument*/NULL);
		String_t* L_20 = ___userID0;
		CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C_inline(__this, L_20, /*hidden argument*/NULL);
		String_t* L_21 = ___username6;
		bool L_22;
		L_22 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_23 = ___username6;
		CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C_inline(__this, L_23, /*hidden argument*/NULL);
		goto IL_0096;
	}

IL_008f:
	{
		String_t* L_24 = ___userID0;
		CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C_inline(__this, L_24, /*hidden argument*/NULL);
	}

IL_0096:
	{
		String_t* L_25 = ___status5;
		CognitoUser_set_Status_m6AC74726AEECA36363F06AFC578491E8CB06A54A_inline(__this, L_25, /*hidden argument*/NULL);
		CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * L_26 = ___pool2;
		CognitoUser_set_UserPool_mEC8B2A8E3CE2AC7A66ADF8A201F4C2BA6DCCF9D0_inline(__this, L_26, /*hidden argument*/NULL);
		String_t* L_27 = ___clientID1;
		CognitoUser_set_ClientID_m7D5C25601A2033D3887954A0A64B7DEBF0A8233F_inline(__this, L_27, /*hidden argument*/NULL);
		CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6_inline(__this, (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA *)NULL, /*hidden argument*/NULL);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_28 = ___attributes7;
		if (!L_28)
		{
			goto IL_00bf;
		}
	}
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_29 = ___attributes7;
		CognitoUser_set_Attributes_m6CFCBD03B13DA55D2275D8CD7F6DC9A25B5EF72A_inline(__this, L_29, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		RuntimeObject* L_30 = ___provider3;
		CognitoUser_set_Provider_m384C1E82B82132EAA78A0C0D689A7EDF24C0CBBA_inline(__this, L_30, /*hidden argument*/NULL);
		RuntimeObject* L_31;
		L_31 = CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403_inline(__this, /*hidden argument*/NULL);
		V_0 = ((AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 *)IsInstClass((RuntimeObject*)L_31, AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_il2cpp_TypeInfo_var));
		AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 * L_32 = V_0;
		if (!L_32)
		{
			goto IL_00e8;
		}
	}
	{
		AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 * L_33 = V_0;
		RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * L_34 = (RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 *)il2cpp_codegen_object_new(RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7_il2cpp_TypeInfo_var);
		RequestEventHandler__ctor_m570E54DC4EF47898C92E313326A679EE57D8E3B5(L_34, NULL, (intptr_t)((intptr_t)CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_33);
		AmazonServiceClient_add_BeforeRequestEvent_m818F8CCC42067965654196C87B746679EB5045C4(L_33, L_34, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// Amazon.Extensions.CognitoAuthentication.CognitoUserSession Amazon.Extensions.CognitoAuthentication.CognitoUser::GetCognitoUserSession(Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * CognitoUser_GetCognitoUserSession_m29915544522BBCC536F1C1BAC2C8BBB2132993E3 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___authResult0, String_t* ___refreshTokenOverride1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  V_2;
	memset((&V_2), 0, sizeof(V_2));
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_0 = ___authResult0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = AuthenticationResultType_get_IdToken_mE391F6EF0BF8DF2263ED4629BD49F52E622E8C75_inline(L_0, /*hidden argument*/NULL);
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_2 = ___authResult0;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = AuthenticationResultType_get_AccessToken_m6D5BAA07E120818EB40BF3DA271698B28C325486_inline(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_4;
		L_4 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		V_2 = L_4;
		String_t* L_5 = ___refreshTokenOverride1;
		bool L_6;
		L_6 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_5, /*hidden argument*/NULL);
		G_B1_0 = L_1;
		if (L_6)
		{
			G_B2_0 = L_1;
			goto IL_001f;
		}
	}
	{
		String_t* L_7 = ___refreshTokenOverride1;
		V_1 = L_7;
		G_B3_0 = G_B1_0;
		goto IL_0026;
	}

IL_001f:
	{
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_8 = ___authResult0;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = AuthenticationResultType_get_RefreshToken_m9B9C4F96A0C5A98F7763DA4EAB3499BD6A53E52A_inline(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		G_B3_0 = G_B2_0;
	}

IL_0026:
	{
		String_t* L_10 = V_0;
		String_t* L_11 = V_1;
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_12 = V_2;
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_13 = ___authResult0;
		NullCheck(L_13);
		int32_t L_14;
		L_14 = AuthenticationResultType_get_ExpiresIn_m9D1E78AB44F6E8D25EB4355381429B6404155BD1(L_13, /*hidden argument*/NULL);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_15;
		L_15 = DateTime_AddSeconds_mCA0940A7E7C3ED40A86532349B7D4CB3A0F0DEAF((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_2), ((double)((double)L_14)), /*hidden argument*/NULL);
		CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * L_16 = (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA *)il2cpp_codegen_object_new(CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA_il2cpp_TypeInfo_var);
		CognitoUserSession__ctor_m8AB71BE7C28B14D88DDF96E4EDC7B36318FE7686(L_16, G_B3_0, L_10, L_11, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Threading.Tasks.Task`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse> Amazon.Extensions.CognitoAuthentication.CognitoUser::StartWithSrpAuthAsync(Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * CognitoUser_StartWithSrpAuthAsync_mE52637A0345C40DE1DCEE198F6F5BD57628E432C (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * ___srpRequest0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_Create_mEF2A6F90F0E729A3E44F040FC6D54F8EF6736367_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6E9C9BE2FCAE466776F72764B7D19F77483DB6FE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_get_Task_m7F62A8721A6D0863BCB16CB26A96CDD62355B2CB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153  V_0;
	memset((&V_0), 0, sizeof(V_0));
	AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		(&V_0)->set_U3CU3E4__this_3(__this);
		InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * L_0 = ___srpRequest0;
		(&V_0)->set_srpRequest_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E_il2cpp_TypeInfo_var);
		AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  L_1;
		L_1 = AsyncTaskMethodBuilder_1_Create_mEF2A6F90F0E729A3E44F040FC6D54F8EF6736367(/*hidden argument*/AsyncTaskMethodBuilder_1_Create_mEF2A6F90F0E729A3E44F040FC6D54F8EF6736367_RuntimeMethod_var);
		(&V_0)->set_U3CU3Et__builder_1(L_1);
		(&V_0)->set_U3CU3E1__state_0((-1));
		U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153  L_2 = V_0;
		AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E  L_3 = L_2.get_U3CU3Et__builder_1();
		V_1 = L_3;
		AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6E9C9BE2FCAE466776F72764B7D19F77483DB6FE((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)(&V_1), (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *)(&V_0), /*hidden argument*/AsyncTaskMethodBuilder_1_Start_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6E9C9BE2FCAE466776F72764B7D19F77483DB6FE_RuntimeMethod_var);
		AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * L_4 = (&V_0)->get_address_of_U3CU3Et__builder_1();
		Task_1_t5A46621D4A7ED2D9AAC3532C1245A79015A1FE83 * L_5;
		L_5 = AsyncTaskMethodBuilder_1_get_Task_m7F62A8721A6D0863BCB16CB26A96CDD62355B2CB((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)L_4, /*hidden argument*/AsyncTaskMethodBuilder_1_get_Task_m7F62A8721A6D0863BCB16CB26A96CDD62355B2CB_RuntimeMethod_var);
		return L_5;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::UpdateSessionIfAuthenticationComplete(Amazon.CognitoIdentityProvider.ChallengeNameType,Amazon.CognitoIdentityProvider.Model.AuthenticationResultType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_UpdateSessionIfAuthenticationComplete_mAFC4BB94CC2465C1AE868AC184556708D60AB428 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___challengeName0, AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * ___authResult1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * V_0 = NULL;
	{
		ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_0 = ___challengeName0;
		IL2CPP_RUNTIME_CLASS_INIT(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = ConstantClass_op_Implicit_m66D411C2C8F30059CBDF13282013A60AD5D95B90(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_3 = ___authResult1;
		CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * L_4;
		L_4 = CognitoUser_GetCognitoUserSession_m29915544522BBCC536F1C1BAC2C8BBB2132993E3(__this, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		V_0 = L_4;
		CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * L_5 = V_0;
		CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6_inline(__this, L_5, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest Amazon.Extensions.CognitoAuthentication.CognitoUser::CreateSrpAuthRequest(System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * CognitoUser_CreateSrpAuthRequest_mBE2390FC87AA9842064CADD5EB303FA6AF9ECC40 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___tupleAa0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * V_0 = NULL;
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_0 = (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B *)il2cpp_codegen_object_new(InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B_il2cpp_TypeInfo_var);
		InitiateAuthRequest__ctor_mC7F09D29F08F846B156E36B3B6C2256A164F5618(L_0, /*hidden argument*/NULL);
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_il2cpp_TypeInfo_var);
		AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * L_2 = ((AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_StaticFields*)il2cpp_codegen_static_fields_for(AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834_il2cpp_TypeInfo_var))->get_USER_SRP_AUTH_9();
		NullCheck(L_1);
		InitiateAuthRequest_set_AuthFlow_mD0AB0CD4A6044E83D4C8DB9E520984E4E6148649_inline(L_1, L_2, /*hidden argument*/NULL);
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_3 = L_1;
		String_t* L_4;
		L_4 = CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		InitiateAuthRequest_set_ClientId_m7AF95FACF7F24BA83DA0AB54F23102925DFF1E74_inline(L_3, L_4, /*hidden argument*/NULL);
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_5 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var);
		StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * L_6;
		L_6 = StringComparer_get_Ordinal_mF3B6370BEBD77351DB5218C867DCD669C47B8812_inline(/*hidden argument*/NULL);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_7 = (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)il2cpp_codegen_object_new(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0(L_7, L_6, /*hidden argument*/Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0_RuntimeMethod_var);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_8 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_9 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamUsername_3();
		String_t* L_10;
		L_10 = CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_8, L_9, L_10, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_11 = L_8;
		String_t* L_12 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSrpA_0();
		Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_13 = ___tupleAa0;
		NullCheck(L_13);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_14;
		L_14 = Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_inline(L_13, /*hidden argument*/Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_RuntimeMethod_var);
		V_1 = L_14;
		String_t* L_15;
		L_15 = BigInteger_ToString_mA75496825851550D19E92E5DCA527DF2AC430BF6((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 *)(&V_1), _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_11, L_12, L_15, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		NullCheck(L_5);
		InitiateAuthRequest_set_AuthParameters_m156E35FA9567233EE27CE4819499095448504CDF_inline(L_5, L_11, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_16;
		L_16 = CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline(__this, /*hidden argument*/NULL);
		bool L_17;
		L_17 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0090;
		}
	}
	{
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_18 = V_0;
		NullCheck(L_18);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_19;
		L_19 = InitiateAuthRequest_get_AuthParameters_m8EDAAC67BDB894DFFD14574069FF458FE45766FA_inline(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_20 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSecretHash_2();
		String_t* L_21;
		L_21 = CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline(__this, /*hidden argument*/NULL);
		String_t* L_22;
		L_22 = CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline(__this, /*hidden argument*/NULL);
		String_t* L_23;
		L_23 = CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline(__this, /*hidden argument*/NULL);
		String_t* L_24;
		L_24 = CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193(L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_19, L_20, L_24, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
	}

IL_0090:
	{
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_25;
		L_25 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(__this, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00c5;
		}
	}
	{
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_26;
		L_26 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27;
		L_27 = CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline(L_26, /*hidden argument*/NULL);
		bool L_28;
		L_28 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00c5;
		}
	}
	{
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_29 = V_0;
		NullCheck(L_29);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_30;
		L_30 = InitiateAuthRequest_get_AuthParameters_m8EDAAC67BDB894DFFD14574069FF458FE45766FA_inline(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_31 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamDeviceKey_15();
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_32;
		L_32 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33;
		L_33 = CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline(L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_30, L_31, L_33, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
	}

IL_00c5:
	{
		InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_34 = V_0;
		return L_34;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::UpdateUsernameAndSecretHash(System.Collections.Generic.IDictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUser_UpdateUsernameAndSecretHash_m80C753373FEE74947273595CB468782F8C359126 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, RuntimeObject* ___challengeParameters0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	{
		String_t* L_0;
		L_0 = CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline(__this, /*hidden argument*/NULL);
		bool L_1;
		L_1 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2;
		L_2 = CognitoUser_get_UserID_m858BEACD4ACC7D4EC6462D0A2229A7B4EA6EF72F_inline(__this, /*hidden argument*/NULL);
		String_t* L_3;
		L_3 = CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline(__this, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_Equals_mD65682B0BB7933CC7A8561AE34DED02E4F3BBBE5(L_2, L_3, 4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		RuntimeObject* L_5 = ___challengeParameters0;
		G_B4_0 = G_B3_0;
		if (!L_5)
		{
			G_B5_0 = G_B3_0;
			goto IL_0032;
		}
	}
	{
		RuntimeObject* L_6 = ___challengeParameters0;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_7 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamUsername_3();
		NullCheck(L_6);
		bool L_8;
		L_8 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(4 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_6, L_7);
		G_B6_0 = ((int32_t)(L_8));
		G_B6_1 = G_B4_0;
		goto IL_0033;
	}

IL_0032:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0033:
	{
		V_0 = (bool)G_B6_0;
		bool L_9 = V_0;
		if (((int32_t)((int32_t)G_B6_1|(int32_t)L_9)))
		{
			goto IL_0039;
		}
	}
	{
		return;
	}

IL_0039:
	{
		RuntimeObject* L_10 = ___challengeParameters0;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_11 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamUsername_3();
		NullCheck(L_10);
		bool L_12;
		L_12 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(4 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_10, L_11);
		if (!L_12)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_13 = ___challengeParameters0;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_14 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamUsername_3();
		NullCheck(L_13);
		String_t* L_15;
		L_15 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(0 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_13, L_14);
		CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C_inline(__this, L_15, /*hidden argument*/NULL);
	}

IL_0057:
	{
		String_t* L_16;
		L_16 = CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline(__this, /*hidden argument*/NULL);
		bool L_17;
		L_17 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0081;
		}
	}
	{
		String_t* L_18;
		L_18 = CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline(__this, /*hidden argument*/NULL);
		String_t* L_19;
		L_19 = CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline(__this, /*hidden argument*/NULL);
		String_t* L_20;
		L_20 = CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline(__this, /*hidden argument*/NULL);
		String_t* L_21;
		L_21 = CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193(L_18, L_19, L_20, /*hidden argument*/NULL);
		CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3_inline(__this, L_21, /*hidden argument*/NULL);
	}

IL_0081:
	{
		return;
	}
}
// Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest Amazon.Extensions.CognitoAuthentication.CognitoUser::CreateSrpPasswordVerifierAuthRequest(Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63 (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * ___challenge0, String_t* ___password1, Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * ___tupleAa2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral57F5D84CEA9F6D7E30D54586FAA16AC2EB4CBF63);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  V_4;
	memset((&V_4), 0, sizeof(V_4));
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * V_7 = NULL;
	BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  V_8;
	memset((&V_8), 0, sizeof(V_8));
	{
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_0 = ___challenge0;
		NullCheck(L_0);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_1;
		L_1 = InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_2 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamUsername_3();
		NullCheck(L_1);
		String_t* L_3;
		L_3 = Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141(L_1, L_2, /*hidden argument*/Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		V_0 = L_3;
		String_t* L_4;
		L_4 = CognitoUser_get_PoolName_mB595FBE47D57E0782C042D1D3DF9FF6F32FA62AA_inline(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_5 = ___challenge0;
		NullCheck(L_5);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_6;
		L_6 = InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline(L_5, /*hidden argument*/NULL);
		String_t* L_7 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSecretBlock_6();
		NullCheck(L_6);
		String_t* L_8;
		L_8 = Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		V_2 = L_8;
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_9 = ___challenge0;
		NullCheck(L_9);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_10;
		L_10 = InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline(L_9, /*hidden argument*/NULL);
		String_t* L_11 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSalt_5();
		NullCheck(L_10);
		String_t* L_12;
		L_12 = Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141(L_10, L_11, /*hidden argument*/Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		V_3 = L_12;
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_13 = ___challenge0;
		NullCheck(L_13);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_14;
		L_14 = InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline(L_13, /*hidden argument*/NULL);
		String_t* L_15 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSrpB_1();
		NullCheck(L_14);
		String_t* L_16;
		L_16 = Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141(L_14, L_15, /*hidden argument*/Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_17;
		L_17 = BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_18 = ((AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_StaticFields*)il2cpp_codegen_static_fields_for(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var))->get_N_0();
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_19;
		L_19 = BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E(L_17, L_18, /*hidden argument*/NULL);
		V_8 = L_19;
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_20;
		L_20 = BigInteger_get_Zero_mE10EE4CF6BAD05BC10D0D5012ECDCC1B81E438BF_inline(/*hidden argument*/NULL);
		bool L_21;
		L_21 = BigInteger_Equals_m05302B320532185A473A0E9CBA352FA30BB43C9A((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 *)(&V_8), L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0079;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_22 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D(L_22, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCDBEC64046BDA8EB945EE3C9944B7D10CA374A4C)), ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralEDBBCE96E30F14D67FEFB0589C89F7A48EDA7C50)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63_RuntimeMethod_var)));
	}

IL_0079:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_23;
		L_23 = DateTime_get_UtcNow_m761E57F86226DDD94F0A2F4D98F0A8E27C74F090(/*hidden argument*/NULL);
		V_4 = L_23;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_24;
		L_24 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_25;
		L_25 = DateTime_ToString_mE44033D2750D165DED2A17A927381872EF9FC986((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_4), _stringLiteral57F5D84CEA9F6D7E30D54586FAA16AC2EB4CBF63, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		String_t* L_26 = V_0;
		String_t* L_27 = ___password1;
		String_t* L_28 = V_1;
		Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_29 = ___tupleAa2;
		String_t* L_30 = V_3;
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_31 = ___challenge0;
		NullCheck(L_31);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_32;
		L_32 = InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_33 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSrpB_1();
		NullCheck(L_32);
		String_t* L_34;
		L_34 = Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141(L_32, L_33, /*hidden argument*/Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		String_t* L_35 = V_2;
		String_t* L_36 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_37;
		L_37 = AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1(L_26, L_27, L_28, L_29, L_30, L_34, L_35, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		String_t* L_38;
		L_38 = Convert_ToBase64String_mE6E1FE504EF1E99DB2F8B92180A82A5F1512EF6A(L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var);
		StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * L_39;
		L_39 = StringComparer_get_Ordinal_mF3B6370BEBD77351DB5218C867DCD669C47B8812_inline(/*hidden argument*/NULL);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_40 = (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)il2cpp_codegen_object_new(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0(L_40, L_39, /*hidden argument*/Dictionary_2__ctor_m4757015DBB2285304C7222BE41A89FCB05BE5BC0_RuntimeMethod_var);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_41 = L_40;
		String_t* L_42 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamPassSecretBlock_9();
		String_t* L_43 = V_2;
		NullCheck(L_41);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_41, L_42, L_43, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_44 = L_41;
		String_t* L_45 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamPassSignature_10();
		String_t* L_46 = V_6;
		NullCheck(L_44);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_44, L_45, L_46, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_47 = L_44;
		String_t* L_48 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamUsername_3();
		String_t* L_49 = V_0;
		NullCheck(L_47);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_47, L_48, L_49, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_50 = L_47;
		String_t* L_51 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamTimestamp_11();
		String_t* L_52 = V_5;
		NullCheck(L_50);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_50, L_51, L_52, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		V_7 = L_50;
		String_t* L_53;
		L_53 = CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline(__this, /*hidden argument*/NULL);
		bool L_54;
		L_54 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_53, /*hidden argument*/NULL);
		if (L_54)
		{
			goto IL_0131;
		}
	}
	{
		String_t* L_55;
		L_55 = CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline(__this, /*hidden argument*/NULL);
		String_t* L_56;
		L_56 = CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline(__this, /*hidden argument*/NULL);
		String_t* L_57;
		L_57 = CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline(__this, /*hidden argument*/NULL);
		String_t* L_58;
		L_58 = CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193(L_55, L_56, L_57, /*hidden argument*/NULL);
		CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3_inline(__this, L_58, /*hidden argument*/NULL);
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_59 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_60 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamSecretHash_2();
		String_t* L_61;
		L_61 = CognitoUser_get_SecretHash_m1FCB2073B2703446B01998388FFD77B5056B8A98_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_59, L_60, L_61, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
	}

IL_0131:
	{
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_62;
		L_62 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(__this, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_0162;
		}
	}
	{
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_63;
		L_63 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_63);
		String_t* L_64;
		L_64 = CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline(L_63, /*hidden argument*/NULL);
		bool L_65;
		L_65 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_64, /*hidden argument*/NULL);
		if (L_65)
		{
			goto IL_0162;
		}
	}
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_66 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		String_t* L_67 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamDeviceKey_15();
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_68;
		L_68 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_68);
		String_t* L_69;
		L_69 = CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline(L_68, /*hidden argument*/NULL);
		NullCheck(L_66);
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_66, L_67, L_69, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
	}

IL_0162:
	{
		RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_70 = (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE *)il2cpp_codegen_object_new(RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE_il2cpp_TypeInfo_var);
		RespondToAuthChallengeRequest__ctor_m96C934A734FB612A3DC464BE3396F64DD157E276(L_70, /*hidden argument*/NULL);
		RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_71 = L_70;
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_72 = ___challenge0;
		NullCheck(L_72);
		ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_73;
		L_73 = InitiateAuthResponse_get_ChallengeName_m7D4CCE67488E4D35933D39E8B54524B4A5484894_inline(L_72, /*hidden argument*/NULL);
		NullCheck(L_71);
		RespondToAuthChallengeRequest_set_ChallengeName_mC3307C66F44FF605F9672F763CD0C77DD56ABCD2_inline(L_71, L_73, /*hidden argument*/NULL);
		RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_74 = L_71;
		String_t* L_75;
		L_75 = CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_74);
		RespondToAuthChallengeRequest_set_ClientId_m5CD35765D3E001E538AF5FE4BF61469ADDA1EF71_inline(L_74, L_75, /*hidden argument*/NULL);
		RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_76 = L_74;
		InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_77 = ___challenge0;
		NullCheck(L_77);
		String_t* L_78;
		L_78 = InitiateAuthResponse_get_Session_mDCD4361A025B34D001F279629CB1FABD4838B3DC_inline(L_77, /*hidden argument*/NULL);
		NullCheck(L_76);
		RespondToAuthChallengeRequest_set_Session_m9A001CCF2391CD45F8FCCF2FF12669691744DB4D_inline(L_76, L_78, /*hidden argument*/NULL);
		RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_79 = L_76;
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_80 = V_7;
		NullCheck(L_79);
		RespondToAuthChallengeRequest_set_ChallengeResponses_mB198D0AEB28A31F3BC175BC70CF2C82DCBA1BAAA_inline(L_79, L_80, /*hidden argument*/NULL);
		return L_79;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Amazon.Extensions.CognitoAuthentication.CognitoUserPool::get_PoolID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6 (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPoolIDU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_PoolID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserPool_set_PoolID_m989C7C62DC1F23F7DCFBF451FF8D0252E127835A (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPoolIDU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_ClientID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserPool_set_ClientID_m511971B0104BDE5498C581F7F371A00878B73222 (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientIDU3Ek__BackingField_1(L_0);
		return;
	}
}
// Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUserPool::get_Provider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CognitoUserPool_get_Provider_mDA59C9E589F8E745E08190C267073925670785F0 (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CProviderU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_Provider(Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserPool_set_Provider_m475CC10ECC82018ABDC254A949C3DA2864A8A30B (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CProviderU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_ClientSecret(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserPool_set_ClientSecret_mA43DA79CA3AE2C9E11018C6FDB6624DE967F24AB (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientSecretU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::.ctor(System.String,System.String,Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserPool__ctor_m6619C54386E0E1725FB1EA008014B30596D19694 (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___poolID0, String_t* ___clientID1, RuntimeObject* ___provider2, String_t* ___clientSecret3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C);
		s_Il2CppMethodInitialized = true;
	}
	AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 * V_0 = NULL;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___poolID0;
		NullCheck(L_0);
		bool L_1;
		L_1 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_0, _stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_2 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0311F42C1F9456B6C2CBD1CCC062B5F8B139A889)), ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCB07A09291C08815CF3D11E19DCD1076FA7C7EEE)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CognitoUserPool__ctor_m6619C54386E0E1725FB1EA008014B30596D19694_RuntimeMethod_var)));
	}

IL_0023:
	{
		String_t* L_3 = ___poolID0;
		CognitoUserPool_set_PoolID_m989C7C62DC1F23F7DCFBF451FF8D0252E127835A_inline(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___clientID1;
		CognitoUserPool_set_ClientID_m511971B0104BDE5498C581F7F371A00878B73222_inline(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___clientSecret3;
		CognitoUserPool_set_ClientSecret_mA43DA79CA3AE2C9E11018C6FDB6624DE967F24AB_inline(__this, L_5, /*hidden argument*/NULL);
		RuntimeObject* L_6 = ___provider2;
		CognitoUserPool_set_Provider_m475CC10ECC82018ABDC254A949C3DA2864A8A30B_inline(__this, L_6, /*hidden argument*/NULL);
		RuntimeObject* L_7;
		L_7 = CognitoUserPool_get_Provider_mDA59C9E589F8E745E08190C267073925670785F0_inline(__this, /*hidden argument*/NULL);
		V_0 = ((AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 *)IsInstClass((RuntimeObject*)L_7, AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87_il2cpp_TypeInfo_var));
		AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		AmazonCognitoIdentityProviderClient_t6BDBB2F4AF565FF8CB155A5DE00EE040D2144B87 * L_9 = V_0;
		RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * L_10 = (RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 *)il2cpp_codegen_object_new(RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7_il2cpp_TypeInfo_var);
		RequestEventHandler__ctor_m570E54DC4EF47898C92E313326A679EE57D8E3B5(L_10, NULL, (intptr_t)((intptr_t)CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		AmazonServiceClient_add_BeforeRequestEvent_m818F8CCC42067965654196C87B746679EB5045C4(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_IdToken(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession_set_IdToken_m16577680A752E865B717BA8AD0B964C0B1F1A737 (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_AccessToken(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession_set_AccessToken_mA221525DBBEB9015769F330E192C39E601DEB16B (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccessTokenU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_RefreshToken(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession_set_RefreshToken_m4E68B941BBCE29ACA651470C2C9D556B8BB01424 (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CRefreshTokenU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_ExpirationTime(System.DateTime)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession_set_ExpirationTime_mF772E4FABBD13882FFE731AD15AD2F7E1AD89408 (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method)
{
	{
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_0 = ___value0;
		__this->set_U3CExpirationTimeU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_IssuedTime(System.DateTime)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession_set_IssuedTime_m837B035F367ADE8CB0805D8E2CD1255F4508CBAD (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method)
{
	{
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_0 = ___value0;
		__this->set_U3CIssuedTimeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::.ctor(System.String,System.String,System.String,System.DateTime,System.DateTime)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CognitoUserSession__ctor_m8AB71BE7C28B14D88DDF96E4EDC7B36318FE7686 (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___idToken0, String_t* ___accessToken1, String_t* ___refreshToken2, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___issuedTime3, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___expirationTime4, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___idToken0;
		CognitoUserSession_set_IdToken_m16577680A752E865B717BA8AD0B964C0B1F1A737_inline(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___accessToken1;
		CognitoUserSession_set_AccessToken_mA221525DBBEB9015769F330E192C39E601DEB16B_inline(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___refreshToken2;
		CognitoUserSession_set_RefreshToken_m4E68B941BBCE29ACA651470C2C9D556B8BB01424_inline(__this, L_2, /*hidden argument*/NULL);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_3 = ___issuedTime3;
		CognitoUserSession_set_IssuedTime_m837B035F367ADE8CB0805D8E2CD1255F4508CBAD_inline(__this, L_3, /*hidden argument*/NULL);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_4 = ___expirationTime4;
		CognitoUserSession_set_ExpirationTime_mF772E4FABBD13882FFE731AD15AD2F7E1AD89408_inline(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::get_Prk()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* HkdfSha256_get_Prk_m3CB2E31FFA5572F37FB59373AE5E4B955B967F8A (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_U3CPrkU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::set_Prk(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HkdfSha256_set_Prk_m3BFB02703CF8FF382308FD49349D7A8753ABF281 (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___value0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___value0;
		__this->set_U3CPrkU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Security.Cryptography.HMACSHA256 Amazon.Extensions.CognitoAuthentication.HkdfSha256::get_HmacSha256()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2 (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, const RuntimeMethod* method)
{
	{
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_0 = __this->get_U3CHmacSha256U3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::set_HmacSha256(System.Security.Cryptography.HMACSHA256)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107 (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * ___value0, const RuntimeMethod* method)
{
	{
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_0 = ___value0;
		__this->set_U3CHmacSha256U3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::.ctor(System.Byte[],System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HkdfSha256__ctor_mF448325D81511381115C9D024636F8386C6C2697 (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___salt0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___ikm1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___salt0;
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_1 = (HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD *)il2cpp_codegen_object_new(HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		HMACSHA256__ctor_m0836F5FA98464B745CF97CC102DBE4497E17BEA9(L_1, L_0, /*hidden argument*/NULL);
		HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107_inline(__this, L_1, /*hidden argument*/NULL);
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_2;
		L_2 = HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2_inline(__this, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___ikm1;
		NullCheck(L_2);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4;
		L_4 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_2, L_3, /*hidden argument*/NULL);
		HkdfSha256_set_Prk_m3BFB02703CF8FF382308FD49349D7A8753ABF281_inline(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::Expand(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___info0, int32_t ___length1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	uint8_t V_1 = 0x0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = ___length1;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)8160))))
		{
			goto IL_0026;
		}
	}
	{
		V_5 = ((int32_t)8160);
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_5), /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD5F29C62B9F83E07CC0D9A5CD43FD5EE614A4A8A)), L_1, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_3 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F_RuntimeMethod_var)));
	}

IL_0026:
	{
		int32_t L_4 = ___length1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_4);
		V_0 = L_5;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6;
		L_6 = HkdfSha256_get_Prk_m3CB2E31FFA5572F37FB59373AE5E4B955B967F8A_inline(__this, /*hidden argument*/NULL);
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_7 = (HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD *)il2cpp_codegen_object_new(HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD_il2cpp_TypeInfo_var);
		HMACSHA256__ctor_m0836F5FA98464B745CF97CC102DBE4497E17BEA9(L_7, L_6, /*hidden argument*/NULL);
		HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107_inline(__this, L_7, /*hidden argument*/NULL);
		V_1 = (uint8_t)1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)0);
		V_2 = L_8;
		int32_t L_9 = ___length1;
		V_4 = L_9;
		goto IL_009d;
	}

IL_004c:
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_10 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)3);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_11 = L_10;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = V_2;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_12);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_13 = L_11;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = ___info0;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_14);
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_15 = L_13;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_16 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)1);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = L_16;
		uint8_t L_18 = V_1;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_18);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_17);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19;
		L_19 = CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368(L_15, /*hidden argument*/NULL);
		V_3 = L_19;
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_20;
		L_20 = HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2_inline(__this, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_21 = V_3;
		NullCheck(L_20);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22;
		L_22 = HashAlgorithm_ComputeHash_m54AE40F9CD9E46736384369DBB5739FBCBDF67D9(L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_23 = V_2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = V_0;
		int32_t L_25 = ___length1;
		int32_t L_26 = V_4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_27 = V_2;
		NullCheck(L_27);
		int32_t L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_29;
		L_29 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(((int32_t)((int32_t)(((RuntimeArray*)L_27)->max_length))), L_28, /*hidden argument*/NULL);
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_23, 0, (RuntimeArray *)(RuntimeArray *)L_24, ((int32_t)il2cpp_codegen_subtract((int32_t)L_25, (int32_t)L_26)), L_29, /*hidden argument*/NULL);
		int32_t L_30 = V_4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_31 = V_2;
		NullCheck(L_31);
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_31)->max_length)))));
		uint8_t L_32 = V_1;
		V_1 = (uint8_t)((int32_t)((uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1))));
	}

IL_009d:
	{
		int32_t L_33 = V_4;
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_34 = V_0;
		return L_34;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::get_Password()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869 (InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::set_Password(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitiateSrpAuthRequest_set_Password_m4A51EE4021B555BB3AAA7AFB522CB93078788A9C (InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPasswordU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitiateSrpAuthRequest__ctor_m17B9D655D90C319F19F22FE58FF9EBB61EF2168C (InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209 (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6924C8F15A0B905ED3807EAB4462A1953849557C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_mB6450D099253BAA88CB077A27F0F2CE29BC7FCBD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_SetResult_mAB1FB9ECA6777B761CB26DB9A9D1A72EE788DBF5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfiguredTaskAwaitable_1_GetAwaiter_m2B667096D2B4B58D37EA4D9C4858F5A45028C935_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfiguredTaskAwaitable_1_GetAwaiter_mC0B99DDC353B6E7C81CA27D2D8B1A9C437AE56BF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfiguredTaskAwaiter_GetResult_m8EC35D81407E7E6F1059CF69E3F9BCE6118B9740_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfiguredTaskAwaiter_GetResult_mCF811B9F3DF810C629A828BA855771A78CC124A9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfiguredTaskAwaiter_get_IsCompleted_m755C1F32EDA0DC621015D811895E509978A958DD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfiguredTaskAwaiter_get_IsCompleted_mC400DFEA6D067D16AF92FF753F90D1309A3F33CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m3A3DE48426936CEB09FE89B327E6D0A4B6A888C9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IAmazonCognitoIdentityProvider_tF6B784A34B482F25E4AB76909844BFA35B7B6B9F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Task_1_ConfigureAwait_m260BD4536DEF805CC8207699944516A90738A2C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Task_1_ConfigureAwait_m5C984C4A2243EE418F8EF068BB84258F5B257C7E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * V_1 = NULL;
	AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * V_2 = NULL;
	InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * V_3 = NULL;
	InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * V_4 = NULL;
	RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * V_5 = NULL;
	bool V_6 = false;
	RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * V_7 = NULL;
	ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  V_8;
	memset((&V_8), 0, sizeof(V_8));
	CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  V_9;
	memset((&V_9), 0, sizeof(V_9));
	ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC  V_10;
	memset((&V_10), 0, sizeof(V_10));
	ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  V_11;
	memset((&V_11), 0, sizeof(V_11));
	ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB  V_12;
	memset((&V_12), 0, sizeof(V_12));
	Exception_t * V_13 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;
	int32_t G_B12_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = V_0;
			if (!L_2)
			{
				goto IL_00b1;
			}
		}

IL_0014:
		{
			int32_t L_3 = V_0;
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_01a6;
			}
		}

IL_001b:
		{
			InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * L_4 = __this->get_srpRequest_2();
			if (!L_4)
			{
				goto IL_0035;
			}
		}

IL_0023:
		{
			InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * L_5 = __this->get_srpRequest_2();
			NullCheck(L_5);
			String_t* L_6;
			L_6 = InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869_inline(L_5, /*hidden argument*/NULL);
			bool L_7;
			L_7 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0045;
			}
		}

IL_0035:
		{
			ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_8 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
			ArgumentNullException__ctor_mAD2F05A24C92A657CBCA8C43A9A373C53739A283(L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral8B665A6C19BD67B26669FFF2993E266E6E2E4E0C)), ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral83AF882DAE496029BF697F5B73C72F6DEAA7D63A)), /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209_RuntimeMethod_var)));
		}

IL_0045:
		{
			IL2CPP_RUNTIME_CLASS_INIT(AuthenticationHelper_t3AFA180E39D7B8FFE40633AAEAB789145059E847_il2cpp_TypeInfo_var);
			Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_9;
			L_9 = AuthenticationHelper_CreateAaTuple_m953054F2E0C5CB363C35BC45F7DD4C854265716F(/*hidden argument*/NULL);
			__this->set_U3CtupleAaU3E5__2_4(L_9);
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_10 = V_1;
			Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_11 = __this->get_U3CtupleAaU3E5__2_4();
			NullCheck(L_10);
			InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_12;
			L_12 = CognitoUser_CreateSrpAuthRequest_mBE2390FC87AA9842064CADD5EB303FA6AF9ECC40(L_10, L_11, /*hidden argument*/NULL);
			V_3 = L_12;
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_13 = V_1;
			NullCheck(L_13);
			RuntimeObject* L_14;
			L_14 = CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403_inline(L_13, /*hidden argument*/NULL);
			InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * L_15 = V_3;
			il2cpp_codegen_initobj((&V_9), sizeof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD ));
			CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  L_16 = V_9;
			NullCheck(L_14);
			Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 * L_17;
			L_17 = InterfaceFuncInvoker2< Task_1_t15675CDFC31D0CFADA0B282C7AE722937CAE6D52 *, InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B *, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  >::Invoke(0 /* System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse> Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider::InitiateAuthAsync(Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest,System.Threading.CancellationToken) */, IAmazonCognitoIdentityProvider_tF6B784A34B482F25E4AB76909844BFA35B7B6B9F_il2cpp_TypeInfo_var, L_14, L_15, L_16);
			NullCheck(L_17);
			ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC  L_18;
			L_18 = Task_1_ConfigureAwait_m260BD4536DEF805CC8207699944516A90738A2C1(L_17, (bool)0, /*hidden argument*/Task_1_ConfigureAwait_m260BD4536DEF805CC8207699944516A90738A2C1_RuntimeMethod_var);
			V_10 = L_18;
			ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  L_19;
			L_19 = ConfiguredTaskAwaitable_1_GetAwaiter_mC0B99DDC353B6E7C81CA27D2D8B1A9C437AE56BF_inline((ConfiguredTaskAwaitable_1_t69708B7F80FC474D8F68BFBA9FD58218241FE4CC *)(&V_10), /*hidden argument*/ConfiguredTaskAwaitable_1_GetAwaiter_mC0B99DDC353B6E7C81CA27D2D8B1A9C437AE56BF_RuntimeMethod_var);
			V_8 = L_19;
			bool L_20;
			L_20 = ConfiguredTaskAwaiter_get_IsCompleted_m755C1F32EDA0DC621015D811895E509978A958DD((ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 *)(&V_8), /*hidden argument*/ConfiguredTaskAwaiter_get_IsCompleted_m755C1F32EDA0DC621015D811895E509978A958DD_RuntimeMethod_var);
			if (L_20)
			{
				goto IL_00ce;
			}
		}

IL_008d:
		{
			int32_t L_21 = 0;
			V_0 = L_21;
			__this->set_U3CU3E1__state_0(L_21);
			ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  L_22 = V_8;
			__this->set_U3CU3Eu__1_5(L_22);
			AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * L_23 = __this->get_address_of_U3CU3Et__builder_1();
			AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6924C8F15A0B905ED3807EAB4462A1953849557C((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)L_23, (ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 *)(&V_8), (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *)__this, /*hidden argument*/AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_m6924C8F15A0B905ED3807EAB4462A1953849557C_RuntimeMethod_var);
			goto IL_0250;
		}

IL_00b1:
		{
			ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0  L_24 = __this->get_U3CU3Eu__1_5();
			V_8 = L_24;
			ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 * L_25 = __this->get_address_of_U3CU3Eu__1_5();
			il2cpp_codegen_initobj(L_25, sizeof(ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 ));
			int32_t L_26 = (-1);
			V_0 = L_26;
			__this->set_U3CU3E1__state_0(L_26);
		}

IL_00ce:
		{
			InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_27;
			L_27 = ConfiguredTaskAwaiter_GetResult_mCF811B9F3DF810C629A828BA855771A78CC124A9((ConfiguredTaskAwaiter_t311AA58EDB69A8019D1B5D00F38CEC3B9A7BFBB0 *)(&V_8), /*hidden argument*/ConfiguredTaskAwaiter_GetResult_mCF811B9F3DF810C629A828BA855771A78CC124A9_RuntimeMethod_var);
			V_4 = L_27;
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_28 = V_1;
			InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_29 = V_4;
			NullCheck(L_29);
			Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_30;
			L_30 = InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline(L_29, /*hidden argument*/NULL);
			NullCheck(L_28);
			CognitoUser_UpdateUsernameAndSecretHash_m80C753373FEE74947273595CB468782F8C359126(L_28, L_30, /*hidden argument*/NULL);
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_31 = V_1;
			InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * L_32 = V_4;
			InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * L_33 = __this->get_srpRequest_2();
			NullCheck(L_33);
			String_t* L_34;
			L_34 = InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869_inline(L_33, /*hidden argument*/NULL);
			Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * L_35 = __this->get_U3CtupleAaU3E5__2_4();
			NullCheck(L_31);
			RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_36;
			L_36 = CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63(L_31, L_32, L_34, L_35, /*hidden argument*/NULL);
			V_5 = L_36;
			RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_37 = V_5;
			if (!L_37)
			{
				goto IL_010f;
			}
		}

IL_0103:
		{
			RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_38 = V_5;
			NullCheck(L_38);
			Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_39;
			L_39 = RespondToAuthChallengeRequest_get_ChallengeResponses_mF628D1CC2388ABC64C8AA8CCB942462F1CC347F5_inline(L_38, /*hidden argument*/NULL);
			G_B12_0 = ((!(((RuntimeObject*)(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)L_39) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
			goto IL_0110;
		}

IL_010f:
		{
			G_B12_0 = 0;
		}

IL_0110:
		{
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_40 = V_1;
			NullCheck(L_40);
			CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_41;
			L_41 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(L_40, /*hidden argument*/NULL);
			G_B13_0 = G_B12_0;
			if (!L_41)
			{
				G_B14_0 = G_B12_0;
				goto IL_012d;
			}
		}

IL_0118:
		{
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_42 = V_1;
			NullCheck(L_42);
			CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_43;
			L_43 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(L_42, /*hidden argument*/NULL);
			NullCheck(L_43);
			String_t* L_44;
			L_44 = CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline(L_43, /*hidden argument*/NULL);
			bool L_45;
			L_45 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_44, /*hidden argument*/NULL);
			G_B15_0 = ((((int32_t)L_45) == ((int32_t)0))? 1 : 0);
			G_B15_1 = G_B13_0;
			goto IL_012e;
		}

IL_012d:
		{
			G_B15_0 = 0;
			G_B15_1 = G_B14_0;
		}

IL_012e:
		{
			V_6 = (bool)G_B15_0;
			bool L_46 = V_6;
			if (!((int32_t)((int32_t)G_B15_1&(int32_t)L_46)))
			{
				goto IL_0151;
			}
		}

IL_0135:
		{
			RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_47 = V_5;
			NullCheck(L_47);
			Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_48;
			L_48 = RespondToAuthChallengeRequest_get_ChallengeResponses_mF628D1CC2388ABC64C8AA8CCB942462F1CC347F5_inline(L_47, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var);
			String_t* L_49 = ((CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_StaticFields*)il2cpp_codegen_static_fields_for(CognitoConstants_tAF7C13F3F38860F4EED6F1ACDFF1A0B8D38B45B3_il2cpp_TypeInfo_var))->get_ChlgParamDeviceKey_15();
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_50 = V_1;
			NullCheck(L_50);
			CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_51;
			L_51 = CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline(L_50, /*hidden argument*/NULL);
			NullCheck(L_51);
			String_t* L_52;
			L_52 = CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline(L_51, /*hidden argument*/NULL);
			NullCheck(L_48);
			Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(L_48, L_49, L_52, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		}

IL_0151:
		{
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_53 = V_1;
			NullCheck(L_53);
			RuntimeObject* L_54;
			L_54 = CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403_inline(L_53, /*hidden argument*/NULL);
			RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * L_55 = V_5;
			il2cpp_codegen_initobj((&V_9), sizeof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD ));
			CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  L_56 = V_9;
			NullCheck(L_54);
			Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 * L_57;
			L_57 = InterfaceFuncInvoker2< Task_1_tD62DE81173FA18010D6E0BF8EE3EEAEF368DC516 *, RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE *, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  >::Invoke(1 /* System.Threading.Tasks.Task`1<Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeResponse> Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider::RespondToAuthChallengeAsync(Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest,System.Threading.CancellationToken) */, IAmazonCognitoIdentityProvider_tF6B784A34B482F25E4AB76909844BFA35B7B6B9F_il2cpp_TypeInfo_var, L_54, L_55, L_56);
			NullCheck(L_57);
			ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB  L_58;
			L_58 = Task_1_ConfigureAwait_m5C984C4A2243EE418F8EF068BB84258F5B257C7E(L_57, (bool)0, /*hidden argument*/Task_1_ConfigureAwait_m5C984C4A2243EE418F8EF068BB84258F5B257C7E_RuntimeMethod_var);
			V_12 = L_58;
			ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  L_59;
			L_59 = ConfiguredTaskAwaitable_1_GetAwaiter_m2B667096D2B4B58D37EA4D9C4858F5A45028C935_inline((ConfiguredTaskAwaitable_1_t002F1935C5EAB22262DEFE25C49DB29E4C4134AB *)(&V_12), /*hidden argument*/ConfiguredTaskAwaitable_1_GetAwaiter_m2B667096D2B4B58D37EA4D9C4858F5A45028C935_RuntimeMethod_var);
			V_11 = L_59;
			bool L_60;
			L_60 = ConfiguredTaskAwaiter_get_IsCompleted_mC400DFEA6D067D16AF92FF753F90D1309A3F33CE((ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C *)(&V_11), /*hidden argument*/ConfiguredTaskAwaiter_get_IsCompleted_mC400DFEA6D067D16AF92FF753F90D1309A3F33CE_RuntimeMethod_var);
			if (L_60)
			{
				goto IL_01c3;
			}
		}

IL_0182:
		{
			int32_t L_61 = 1;
			V_0 = L_61;
			__this->set_U3CU3E1__state_0(L_61);
			ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  L_62 = V_11;
			__this->set_U3CU3Eu__2_6(L_62);
			AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * L_63 = __this->get_address_of_U3CU3Et__builder_1();
			AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_mB6450D099253BAA88CB077A27F0F2CE29BC7FCBD((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)L_63, (ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C *)(&V_11), (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *)__this, /*hidden argument*/AsyncTaskMethodBuilder_1_AwaitUnsafeOnCompleted_TisConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C_TisU3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153_mB6450D099253BAA88CB077A27F0F2CE29BC7FCBD_RuntimeMethod_var);
			goto IL_0250;
		}

IL_01a6:
		{
			ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C  L_64 = __this->get_U3CU3Eu__2_6();
			V_11 = L_64;
			ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C * L_65 = __this->get_address_of_U3CU3Eu__2_6();
			il2cpp_codegen_initobj(L_65, sizeof(ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C ));
			int32_t L_66 = (-1);
			V_0 = L_66;
			__this->set_U3CU3E1__state_0(L_66);
		}

IL_01c3:
		{
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_67;
			L_67 = ConfiguredTaskAwaiter_GetResult_m8EC35D81407E7E6F1059CF69E3F9BCE6118B9740((ConfiguredTaskAwaiter_t9A4A8419380FDB6090E6E62728B28EA8E144BE3C *)(&V_11), /*hidden argument*/ConfiguredTaskAwaiter_GetResult_m8EC35D81407E7E6F1059CF69E3F9BCE6118B9740_RuntimeMethod_var);
			V_7 = L_67;
			CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * L_68 = V_1;
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_69 = V_7;
			NullCheck(L_69);
			ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_70;
			L_70 = RespondToAuthChallengeResponse_get_ChallengeName_m9DDD431CE7D58ABBF4D71D33B9B3DA93D1931475_inline(L_69, /*hidden argument*/NULL);
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_71 = V_7;
			NullCheck(L_71);
			AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_72;
			L_72 = RespondToAuthChallengeResponse_get_AuthenticationResult_mEA1392BB5EEE1EAF8C6CA2A4295ADC1CDC6C9FC0_inline(L_71, /*hidden argument*/NULL);
			NullCheck(L_68);
			CognitoUser_UpdateSessionIfAuthenticationComplete_mAFC4BB94CC2465C1AE868AC184556708D60AB428(L_68, L_70, L_72, /*hidden argument*/NULL);
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_73 = V_7;
			NullCheck(L_73);
			String_t* L_74;
			L_74 = RespondToAuthChallengeResponse_get_Session_mE00CF70381F77B441D8F0D6CAC1E8EBF7A262D69_inline(L_73, /*hidden argument*/NULL);
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_75 = V_7;
			NullCheck(L_75);
			AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_76;
			L_76 = RespondToAuthChallengeResponse_get_AuthenticationResult_mEA1392BB5EEE1EAF8C6CA2A4295ADC1CDC6C9FC0_inline(L_75, /*hidden argument*/NULL);
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_77 = V_7;
			NullCheck(L_77);
			ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_78;
			L_78 = RespondToAuthChallengeResponse_get_ChallengeName_m9DDD431CE7D58ABBF4D71D33B9B3DA93D1931475_inline(L_77, /*hidden argument*/NULL);
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_79 = V_7;
			NullCheck(L_79);
			Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_80;
			L_80 = RespondToAuthChallengeResponse_get_ChallengeParameters_mDCF21DF93EAE302BE3A1851D6631B5033C6BDB87_inline(L_79, /*hidden argument*/NULL);
			RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * L_81 = V_7;
			NullCheck(L_81);
			ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * L_82;
			L_82 = AmazonWebServiceResponse_get_ResponseMetadata_m1BDDB043FE9F44E076FBED6BF4D08BE2D6ACCA1D_inline(L_81, /*hidden argument*/NULL);
			NullCheck(L_82);
			RuntimeObject* L_83;
			L_83 = ResponseMetadata_get_Metadata_m0B17E9487DF8F850A0D9D130F0EBFCD83BD038C1(L_82, /*hidden argument*/NULL);
			Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_84 = (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)il2cpp_codegen_object_new(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m3A3DE48426936CEB09FE89B327E6D0A4B6A888C9(L_84, L_83, /*hidden argument*/Dictionary_2__ctor_m3A3DE48426936CEB09FE89B327E6D0A4B6A888C9_RuntimeMethod_var);
			AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * L_85 = (AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 *)il2cpp_codegen_object_new(AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43_il2cpp_TypeInfo_var);
			AuthFlowResponse__ctor_m91D7AB173C699D4A5387CDC348C62C48E10B4F6B(L_85, L_74, L_76, L_78, L_80, L_84, /*hidden argument*/NULL);
			V_2 = L_85;
			goto IL_0235;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0215;
		}
		throw e;
	}

CATCH_0215:
	{ // begin catch(System.Exception)
		V_13 = ((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *));
		__this->set_U3CU3E1__state_0(((int32_t)-2));
		__this->set_U3CtupleAaU3E5__2_4((Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 *)NULL);
		AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * L_86 = __this->get_address_of_U3CU3Et__builder_1();
		Exception_t * L_87 = V_13;
		AsyncTaskMethodBuilder_1_SetException_m38E596D3515939C2F19738981612DA38710F6C0E((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)L_86, L_87, /*hidden argument*/((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&AsyncTaskMethodBuilder_1_SetException_m38E596D3515939C2F19738981612DA38710F6C0E_RuntimeMethod_var)));
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0250;
	} // end catch (depth: 1)

IL_0235:
	{
		__this->set_U3CU3E1__state_0(((int32_t)-2));
		__this->set_U3CtupleAaU3E5__2_4((Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 *)NULL);
		AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * L_88 = __this->get_address_of_U3CU3Et__builder_1();
		AuthFlowResponse_t0B911E489487B2C841557A1643FB152F4EE29D43 * L_89 = V_2;
		AsyncTaskMethodBuilder_1_SetResult_mAB1FB9ECA6777B761CB26DB9A9D1A72EE788DBF5((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)L_88, L_89, /*hidden argument*/AsyncTaskMethodBuilder_1_SetResult_mAB1FB9ECA6777B761CB26DB9A9D1A72EE788DBF5_RuntimeMethod_var);
	}

IL_0250:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * _thisAdjusted = reinterpret_cast<U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *>(__this + _offset);
	U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209(_thisAdjusted, method);
}
// System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1 (U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncTaskMethodBuilder_1_SetStateMachine_m6E3B2ACC94057E1637984A4678462C5B8E210FF1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E * L_0 = __this->get_address_of_U3CU3Et__builder_1();
		RuntimeObject* L_1 = ___stateMachine0;
		AsyncTaskMethodBuilder_1_SetStateMachine_m6E3B2ACC94057E1637984A4678462C5B8E210FF1((AsyncTaskMethodBuilder_1_tACE0D0D674E69A8D832DE99FAE52128A0F3FE28E *)L_0, L_1, /*hidden argument*/AsyncTaskMethodBuilder_1_SetStateMachine_m6E3B2ACC94057E1637984A4678462C5B8E210FF1_RuntimeMethod_var);
		return;
	}
}
IL2CPP_EXTERN_C  void U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1_AdjustorThunk (RuntimeObject * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 * _thisAdjusted = reinterpret_cast<U3CStartWithSrpAuthAsyncU3Ed__81_t283AFAF36E8BD713B45C1BE9344118F064431153 *>(__this + _offset);
	U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1(_thisAdjusted, ___stateMachine0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  BigInteger_get_Zero_mE10EE4CF6BAD05BC10D0D5012ECDCC1B81E438BF_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var);
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_0 = ((BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_StaticFields*)il2cpp_codegen_static_fields_for(BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2_il2cpp_TypeInfo_var))->get_s_bnZeroInt_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5_inline (WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CHeadersU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AssemblyFileVersionAttribute_get_Version_m56763C51185F40666C40AF57A0E83A1B446C7597_inline (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__version_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPoolIDU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_PoolName_mB319666D9228CDCF803B56AE31006CB185FCBCAC_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPoolNameU3Ek__BackingField_11(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_ClientSecret_m0013271AF0F323BFF73DF5B7E422946882F58364_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientSecretU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSecretHashU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_UserID_mA7CF427A7E4ED35C62A09920A8D6D3E7B5A20266_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUserIDU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Status_m6AC74726AEECA36363F06AFC578491E8CB06A54A_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatusU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_UserPool_mEC8B2A8E3CE2AC7A66ADF8A201F4C2BA6DCCF9D0_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * ___value0, const RuntimeMethod* method)
{
	{
		CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * L_0 = ___value0;
		__this->set_U3CUserPoolU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_ClientID_m7D5C25601A2033D3887954A0A64B7DEBF0A8233F_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientIDU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * ___value0, const RuntimeMethod* method)
{
	{
		CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * L_0 = ___value0;
		__this->set_U3CSessionTokensU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Attributes_m6CFCBD03B13DA55D2275D8CD7F6DC9A25B5EF72A_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = ___value0;
		__this->set_U3CAttributesU3Ek__BackingField_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUser_set_Provider_m384C1E82B82132EAA78A0C0D689A7EDF24C0CBBA_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CProviderU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CProviderU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AuthenticationResultType_get_IdToken_mE391F6EF0BF8DF2263ED4629BD49F52E622E8C75_inline (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__idToken_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AuthenticationResultType_get_AccessToken_m6D5BAA07E120818EB40BF3DA271698B28C325486_inline (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__accessToken_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AuthenticationResultType_get_RefreshToken_m9B9C4F96A0C5A98F7763DA4EAB3499BD6A53E52A_inline (AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__refreshToken_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InitiateAuthRequest_set_AuthFlow_mD0AB0CD4A6044E83D4C8DB9E520984E4E6148649_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * ___value0, const RuntimeMethod* method)
{
	{
		AuthFlowType_t73A4250240A469B7DEAC4F487E302456422CC834 * L_0 = ___value0;
		__this->set__authFlow_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CClientIDU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InitiateAuthRequest_set_ClientId_m7AF95FACF7F24BA83DA0AB54F23102925DFF1E74_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__clientId_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * StringComparer_get_Ordinal_mF3B6370BEBD77351DB5218C867DCD669C47B8812_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var);
		StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6 * L_0 = ((StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_StaticFields*)il2cpp_codegen_static_fields_for(StringComparer_t69EC059128AD0CAE268CA1A1C33125DAC9D7F8D6_il2cpp_TypeInfo_var))->get__ordinal_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InitiateAuthRequest_set_AuthParameters_m156E35FA9567233EE27CE4819499095448504CDF_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = ___value0;
		__this->set__authParameters_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CClientSecretU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * InitiateAuthRequest_get_AuthParameters_m8EDAAC67BDB894DFFD14574069FF458FE45766FA_inline (InitiateAuthRequest_tDAB27A94DB8C181006D4F45EE053562AE3ECAA2B * __this, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = __this->get__authParameters_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * L_0 = __this->get_U3CDeviceU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094_inline (CognitoDevice_t56D53B20E3BBDCEDD22BFE4F1050840B6B856760 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceKeyU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_UserID_m858BEACD4ACC7D4EC6462D0A2229A7B4EA6EF72F_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUserIDU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * InitiateAuthResponse_get_ChallengeParameters_m3056051951C0A07DF01E6ABAF453EDDC204BC083_inline (InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * __this, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = __this->get__challengeParameters_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_PoolName_mB595FBE47D57E0782C042D1D3DF9FF6F32FA62AA_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPoolNameU3Ek__BackingField_11();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CognitoUser_get_SecretHash_m1FCB2073B2703446B01998388FFD77B5056B8A98_inline (CognitoUser_t8F2780373029CACF084D4CDFCC2DAF6B2BCD7FE9 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CSecretHashU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * InitiateAuthResponse_get_ChallengeName_m7D4CCE67488E4D35933D39E8B54524B4A5484894_inline (InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * __this, const RuntimeMethod* method)
{
	{
		ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_0 = __this->get__challengeName_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_ChallengeName_mC3307C66F44FF605F9672F763CD0C77DD56ABCD2_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * ___value0, const RuntimeMethod* method)
{
	{
		ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_0 = ___value0;
		__this->set__challengeName_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_ClientId_m5CD35765D3E001E538AF5FE4BF61469ADDA1EF71_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__clientId_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InitiateAuthResponse_get_Session_mDCD4361A025B34D001F279629CB1FABD4838B3DC_inline (InitiateAuthResponse_t9FF64374743CAC6BCB06A566838E059CD0F3B02C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__session_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_Session_m9A001CCF2391CD45F8FCCF2FF12669691744DB4D_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__session_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RespondToAuthChallengeRequest_set_ChallengeResponses_mB198D0AEB28A31F3BC175BC70CF2C82DCBA1BAAA_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___value0, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = ___value0;
		__this->set__challengeResponses_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_PoolID_m989C7C62DC1F23F7DCFBF451FF8D0252E127835A_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPoolIDU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_ClientID_m511971B0104BDE5498C581F7F371A00878B73222_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientIDU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_ClientSecret_mA43DA79CA3AE2C9E11018C6FDB6624DE967F24AB_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CClientSecretU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserPool_set_Provider_m475CC10ECC82018ABDC254A949C3DA2864A8A30B_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CProviderU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* CognitoUserPool_get_Provider_mDA59C9E589F8E745E08190C267073925670785F0_inline (CognitoUserPool_tCA685D636DEDEAE0E7D221DDB517AA2AD7D8A665 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CProviderU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_IdToken_m16577680A752E865B717BA8AD0B964C0B1F1A737_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_AccessToken_mA221525DBBEB9015769F330E192C39E601DEB16B_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccessTokenU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_RefreshToken_m4E68B941BBCE29ACA651470C2C9D556B8BB01424_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CRefreshTokenU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_IssuedTime_m837B035F367ADE8CB0805D8E2CD1255F4508CBAD_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method)
{
	{
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_0 = ___value0;
		__this->set_U3CIssuedTimeU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CognitoUserSession_set_ExpirationTime_mF772E4FABBD13882FFE731AD15AD2F7E1AD89408_inline (CognitoUserSession_t7C0BE020D68CD437EFC0BD287690A832CAB82DCA * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method)
{
	{
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_0 = ___value0;
		__this->set_U3CExpirationTimeU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * ___value0, const RuntimeMethod* method)
{
	{
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_0 = ___value0;
		__this->set_U3CHmacSha256U3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, const RuntimeMethod* method)
{
	{
		HMACSHA256_t3363AA554FC034FF2627F7535F6DB1A0A7F824CD * L_0 = __this->get_U3CHmacSha256U3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HkdfSha256_set_Prk_m3BFB02703CF8FF382308FD49349D7A8753ABF281_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___value0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___value0;
		__this->set_U3CPrkU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* HkdfSha256_get_Prk_m3CB2E31FFA5572F37FB59373AE5E4B955B967F8A_inline (HkdfSha256_tD8973F4152B8F7724B3BCB932479D41F390FC12F * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_U3CPrkU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869_inline (InitiateSrpAuthRequest_t8DA49552C6BAD2029357DAC737BB74FE845E8667 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * RespondToAuthChallengeRequest_get_ChallengeResponses_mF628D1CC2388ABC64C8AA8CCB942462F1CC347F5_inline (RespondToAuthChallengeRequest_tC3CD28614A7C29216719507985FEC75194DB77EE * __this, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = __this->get__challengeResponses_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * RespondToAuthChallengeResponse_get_ChallengeName_m9DDD431CE7D58ABBF4D71D33B9B3DA93D1931475_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method)
{
	{
		ChallengeNameType_t769F7FE5DE88FB526E33C62F5AB1F2EC24430BF2 * L_0 = __this->get__challengeName_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * RespondToAuthChallengeResponse_get_AuthenticationResult_mEA1392BB5EEE1EAF8C6CA2A4295ADC1CDC6C9FC0_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method)
{
	{
		AuthenticationResultType_tC54FC7709681EFF828037723086472F9237E5997 * L_0 = __this->get__authenticationResult_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* RespondToAuthChallengeResponse_get_Session_mE00CF70381F77B441D8F0D6CAC1E8EBF7A262D69_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__session_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * RespondToAuthChallengeResponse_get_ChallengeParameters_mDCF21DF93EAE302BE3A1851D6631B5033C6BDB87_inline (RespondToAuthChallengeResponse_tABE9567491BDF816E8B0863137ADB877ACFA2929 * __this, const RuntimeMethod* method)
{
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = __this->get__challengeParameters_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * AmazonWebServiceResponse_get_ResponseMetadata_m1BDDB043FE9F44E076FBED6BF4D08BE2D6ACCA1D_inline (AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A * __this, const RuntimeMethod* method)
{
	{
		ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * L_0 = __this->get_responseMetadataField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  Tuple_2_get_Item1_mE48D8896293D33B83F21461716F4DCBED0903B29_gshared_inline (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * __this, const RuntimeMethod* method)
{
	{
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_0 = (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 )__this->get_m_Item1_0();
		return (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  Tuple_2_get_Item2_mE9D5ADAF086B126CE86CDD125275E393D84CE51E_gshared_inline (Tuple_2_t2554113CB7B2EB98E44C92DBB1E278E17ED6AD17 * __this, const RuntimeMethod* method)
{
	{
		BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2  L_0 = (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 )__this->get_m_Item2_1();
		return (BigInteger_tB5F51572CFCA06393E28819DAD57C189747B58F2 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED  ConfiguredTaskAwaitable_1_GetAwaiter_mFCE2327CEE19607ABB1CDCC8A6B145BDCF9820BC_gshared_inline (ConfiguredTaskAwaitable_1_t226372B9DEDA3AA0FC1B43D6C03CEC9111045F18 * __this, const RuntimeMethod* method)
{
	{
		ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED  L_0 = (ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED )__this->get_m_configuredTaskAwaiter_0();
		return (ConfiguredTaskAwaiter_t2CE498F9A6CE5405242AE2D77F03E58985B7C3ED )L_0;
	}
}
