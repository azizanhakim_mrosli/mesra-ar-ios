﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// Amazon.Runtime.Internal.AWSPropertyAttribute
struct AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Amazon.Runtime.Internal.AWSPropertyAttribute
struct AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int64 Amazon.Runtime.Internal.AWSPropertyAttribute::min
	int64_t ___min_0;
	// System.Int64 Amazon.Runtime.Internal.AWSPropertyAttribute::max
	int64_t ___max_1;
	// System.Boolean Amazon.Runtime.Internal.AWSPropertyAttribute::<Required>k__BackingField
	bool ___U3CRequiredU3Ek__BackingField_2;
	// System.Boolean Amazon.Runtime.Internal.AWSPropertyAttribute::<IsMinSet>k__BackingField
	bool ___U3CIsMinSetU3Ek__BackingField_3;
	// System.Boolean Amazon.Runtime.Internal.AWSPropertyAttribute::<IsMaxSet>k__BackingField
	bool ___U3CIsMaxSetU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___min_0)); }
	inline int64_t get_min_0() const { return ___min_0; }
	inline int64_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int64_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___max_1)); }
	inline int64_t get_max_1() const { return ___max_1; }
	inline int64_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int64_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_U3CRequiredU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___U3CRequiredU3Ek__BackingField_2)); }
	inline bool get_U3CRequiredU3Ek__BackingField_2() const { return ___U3CRequiredU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CRequiredU3Ek__BackingField_2() { return &___U3CRequiredU3Ek__BackingField_2; }
	inline void set_U3CRequiredU3Ek__BackingField_2(bool value)
	{
		___U3CRequiredU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsMinSetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___U3CIsMinSetU3Ek__BackingField_3)); }
	inline bool get_U3CIsMinSetU3Ek__BackingField_3() const { return ___U3CIsMinSetU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsMinSetU3Ek__BackingField_3() { return &___U3CIsMinSetU3Ek__BackingField_3; }
	inline void set_U3CIsMinSetU3Ek__BackingField_3(bool value)
	{
		___U3CIsMinSetU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsMaxSetU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9, ___U3CIsMaxSetU3Ek__BackingField_4)); }
	inline bool get_U3CIsMaxSetU3Ek__BackingField_4() const { return ___U3CIsMaxSetU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsMaxSetU3Ek__BackingField_4() { return &___U3CIsMaxSetU3Ek__BackingField_4; }
	inline void set_U3CIsMaxSetU3Ek__BackingField_4(bool value)
	{
		___U3CIsMaxSetU3Ek__BackingField_4 = value;
	}
};


// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Required(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Min(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.AWSPropertyAttribute::set_Max(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880 (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, int64_t ___value0, const RuntimeMethod* method);
static void AWSSDK_Kinesis_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[0];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x57\x53\x53\x44\x4B\x2E\x4B\x69\x6E\x65\x73\x69\x73"), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, true, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[2];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x33\x2E\x31\x30\x30\x2E\x32\x32\x30"), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[3];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[4];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[5];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\x32\x30\x30\x39\x2D\x32\x30\x32\x30\x20\x41\x6D\x61\x7A\x6F\x6E\x2E\x63\x6F\x6D\x2C\x20\x49\x6E\x63\x2E\x20\x6F\x72\x20\x69\x74\x73\x20\x61\x66\x66\x69\x6C\x69\x61\x74\x65\x73\x2E\x20\x41\x6C\x6C\x20\x52\x69\x67\x68\x74\x73\x20\x52\x65\x73\x65\x72\x76\x65\x64\x2E"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x61\x7A\x6F\x6E\x2E\x63\x6F\x6D\x2C\x20\x49\x6E\x63"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[7];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x61\x7A\x6F\x6E\x20\x57\x65\x62\x20\x53\x65\x72\x76\x69\x63\x65\x73\x20\x53\x44\x4B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[8];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[9];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x57\x65\x62\x20\x53\x65\x72\x76\x69\x63\x65\x73\x20\x53\x44\x4B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54\x20\x28\x4E\x65\x74\x53\x74\x61\x6E\x64\x61\x72\x64\x20\x32\x2E\x30\x29\x2D\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x4B\x69\x6E\x65\x73\x69\x73\x2E\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x4B\x69\x6E\x65\x73\x69\x73\x20\x69\x73\x20\x61\x20\x66\x75\x6C\x6C\x79\x20\x6D\x61\x6E\x61\x67\x65\x64\x2C\x20\x63\x6C\x6F\x75\x64\x2D\x62\x61\x73\x65\x64\x20\x73\x65\x72\x76\x69\x63\x65\x20\x66\x6F\x72\x20\x72\x65\x61\x6C\x2D\x74\x69\x6D\x65\x20\x70\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x20\x6F\x66\x20\x6C\x61\x72\x67\x65\x2C\x20\x64\x69\x73\x74\x72\x69\x62\x75\x74\x65\x64\x20\x64\x61\x74\x61\x20\x73\x74\x72\x65\x61\x6D\x73\x2E"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[10];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x31\x2E\x30\x2E\x30"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[11];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[12];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[13];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[14];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_CustomAttributesCacheGenerator_PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD____Records_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 500LL, NULL);
	}
}
static void PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_CustomAttributesCacheGenerator_PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD____StreamName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 128LL, NULL);
	}
}
static void PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF_CustomAttributesCacheGenerator_PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF____Data_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 0LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 1048576LL, NULL);
	}
}
static void PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF_CustomAttributesCacheGenerator_PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF____PartitionKey_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 256LL, NULL);
	}
}
static void PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_CustomAttributesCacheGenerator_PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045____FailedRecordCount_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 100000LL, NULL);
	}
}
static void PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_CustomAttributesCacheGenerator_PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045____Records_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline(tmp, true, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 500LL, NULL);
	}
}
static void PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232_CustomAttributesCacheGenerator_PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232____ShardId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * tmp = (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 *)cache->attributes[0];
		AWSPropertyAttribute__ctor_m69DEB2E31E83D7AAF253F7D7B71C11CA70C1E60D(tmp, NULL);
		AWSPropertyAttribute_set_Min_mEC93133A77E7A38156C053F16D4A4741B99DA00B(tmp, 1LL, NULL);
		AWSPropertyAttribute_set_Max_m383A1D77B622C0A037FA6FEEAA3BFF79087DF880(tmp, 128LL, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AWSSDK_Kinesis_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AWSSDK_Kinesis_AttributeGenerators[8] = 
{
	PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_CustomAttributesCacheGenerator_PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD____Records_PropertyInfo,
	PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_CustomAttributesCacheGenerator_PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD____StreamName_PropertyInfo,
	PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF_CustomAttributesCacheGenerator_PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF____Data_PropertyInfo,
	PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF_CustomAttributesCacheGenerator_PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF____PartitionKey_PropertyInfo,
	PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_CustomAttributesCacheGenerator_PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045____FailedRecordCount_PropertyInfo,
	PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_CustomAttributesCacheGenerator_PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045____Records_PropertyInfo,
	PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232_CustomAttributesCacheGenerator_PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232____ShardId_PropertyInfo,
	AWSSDK_Kinesis_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6_inline (AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CRequiredU3Ek__BackingField_2(L_0);
		return;
	}
}
