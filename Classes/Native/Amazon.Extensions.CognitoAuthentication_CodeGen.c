﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::.ctor(System.String,Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,Amazon.CognitoIdentityProvider.ChallengeNameType,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AuthFlowResponse__ctor_m91D7AB173C699D4A5387CDC348C62C48E10B4F6B (void);
// 0x00000002 Amazon.CognitoIdentityProvider.Model.AuthenticationResultType Amazon.Extensions.CognitoAuthentication.AuthFlowResponse::get_AuthenticationResult()
extern void AuthFlowResponse_get_AuthenticationResult_m69D5253C4F74C2918CF23F56A945B69AC9A51F52 (void);
// 0x00000003 System.String Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::get_Password()
extern void InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869 (void);
// 0x00000004 System.Void Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::set_Password(System.String)
extern void InitiateSrpAuthRequest_set_Password_m4A51EE4021B555BB3AAA7AFB522CB93078788A9C (void);
// 0x00000005 System.Void Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest::.ctor()
extern void InitiateSrpAuthRequest__ctor_m17B9D655D90C319F19F22FE58FF9EBB61EF2168C (void);
// 0x00000006 System.String Amazon.Extensions.CognitoAuthentication.CognitoDevice::get_DeviceKey()
extern void CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094 (void);
// 0x00000007 System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_ClientSecret()
extern void CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67 (void);
// 0x00000008 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_ClientSecret(System.String)
extern void CognitoUser_set_ClientSecret_m0013271AF0F323BFF73DF5B7E422946882F58364 (void);
// 0x00000009 System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_SecretHash()
extern void CognitoUser_get_SecretHash_m1FCB2073B2703446B01998388FFD77B5056B8A98 (void);
// 0x0000000A System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_SecretHash(System.String)
extern void CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3 (void);
// 0x0000000B System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_SessionTokens(Amazon.Extensions.CognitoAuthentication.CognitoUserSession)
extern void CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6 (void);
// 0x0000000C Amazon.Extensions.CognitoAuthentication.CognitoDevice Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Device()
extern void CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579 (void);
// 0x0000000D System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_UserID()
extern void CognitoUser_get_UserID_m858BEACD4ACC7D4EC6462D0A2229A7B4EA6EF72F (void);
// 0x0000000E System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_UserID(System.String)
extern void CognitoUser_set_UserID_mA7CF427A7E4ED35C62A09920A8D6D3E7B5A20266 (void);
// 0x0000000F System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Username()
extern void CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B (void);
// 0x00000010 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Username(System.String)
extern void CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C (void);
// 0x00000011 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_UserPool(Amazon.Extensions.CognitoAuthentication.CognitoUserPool)
extern void CognitoUser_set_UserPool_mEC8B2A8E3CE2AC7A66ADF8A201F4C2BA6DCCF9D0 (void);
// 0x00000012 System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_ClientID()
extern void CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5 (void);
// 0x00000013 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_ClientID(System.String)
extern void CognitoUser_set_ClientID_m7D5C25601A2033D3887954A0A64B7DEBF0A8233F (void);
// 0x00000014 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Status(System.String)
extern void CognitoUser_set_Status_m6AC74726AEECA36363F06AFC578491E8CB06A54A (void);
// 0x00000015 Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUser::get_Provider()
extern void CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403 (void);
// 0x00000016 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Provider(Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider)
extern void CognitoUser_set_Provider_m384C1E82B82132EAA78A0C0D689A7EDF24C0CBBA (void);
// 0x00000017 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_Attributes(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void CognitoUser_set_Attributes_m6CFCBD03B13DA55D2275D8CD7F6DC9A25B5EF72A (void);
// 0x00000018 System.String Amazon.Extensions.CognitoAuthentication.CognitoUser::get_PoolName()
extern void CognitoUser_get_PoolName_mB595FBE47D57E0782C042D1D3DF9FF6F32FA62AA (void);
// 0x00000019 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::set_PoolName(System.String)
extern void CognitoUser_set_PoolName_mB319666D9228CDCF803B56AE31006CB185FCBCAC (void);
// 0x0000001A System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::.ctor(System.String,System.String,Amazon.Extensions.CognitoAuthentication.CognitoUserPool,Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void CognitoUser__ctor_mF9AE181160DA6741A661389AED15CCD6664AC209 (void);
// 0x0000001B Amazon.Extensions.CognitoAuthentication.CognitoUserSession Amazon.Extensions.CognitoAuthentication.CognitoUser::GetCognitoUserSession(Amazon.CognitoIdentityProvider.Model.AuthenticationResultType,System.String)
extern void CognitoUser_GetCognitoUserSession_m29915544522BBCC536F1C1BAC2C8BBB2132993E3 (void);
// 0x0000001C System.Threading.Tasks.Task`1<Amazon.Extensions.CognitoAuthentication.AuthFlowResponse> Amazon.Extensions.CognitoAuthentication.CognitoUser::StartWithSrpAuthAsync(Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest)
extern void CognitoUser_StartWithSrpAuthAsync_mE52637A0345C40DE1DCEE198F6F5BD57628E432C (void);
// 0x0000001D System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::UpdateSessionIfAuthenticationComplete(Amazon.CognitoIdentityProvider.ChallengeNameType,Amazon.CognitoIdentityProvider.Model.AuthenticationResultType)
extern void CognitoUser_UpdateSessionIfAuthenticationComplete_mAFC4BB94CC2465C1AE868AC184556708D60AB428 (void);
// 0x0000001E Amazon.CognitoIdentityProvider.Model.InitiateAuthRequest Amazon.Extensions.CognitoAuthentication.CognitoUser::CreateSrpAuthRequest(System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>)
extern void CognitoUser_CreateSrpAuthRequest_mBE2390FC87AA9842064CADD5EB303FA6AF9ECC40 (void);
// 0x0000001F System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser::UpdateUsernameAndSecretHash(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void CognitoUser_UpdateUsernameAndSecretHash_m80C753373FEE74947273595CB468782F8C359126 (void);
// 0x00000020 Amazon.CognitoIdentityProvider.Model.RespondToAuthChallengeRequest Amazon.Extensions.CognitoAuthentication.CognitoUser::CreateSrpPasswordVerifierAuthRequest(Amazon.CognitoIdentityProvider.Model.InitiateAuthResponse,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>)
extern void CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63 (void);
// 0x00000021 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::MoveNext()
extern void U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209 (void);
// 0x00000022 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUser/<StartWithSrpAuthAsync>d__81::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1 (void);
// 0x00000023 System.String Amazon.Extensions.CognitoAuthentication.CognitoUserPool::get_PoolID()
extern void CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6 (void);
// 0x00000024 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_PoolID(System.String)
extern void CognitoUserPool_set_PoolID_m989C7C62DC1F23F7DCFBF451FF8D0252E127835A (void);
// 0x00000025 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_ClientID(System.String)
extern void CognitoUserPool_set_ClientID_m511971B0104BDE5498C581F7F371A00878B73222 (void);
// 0x00000026 Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider Amazon.Extensions.CognitoAuthentication.CognitoUserPool::get_Provider()
extern void CognitoUserPool_get_Provider_mDA59C9E589F8E745E08190C267073925670785F0 (void);
// 0x00000027 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_Provider(Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider)
extern void CognitoUserPool_set_Provider_m475CC10ECC82018ABDC254A949C3DA2864A8A30B (void);
// 0x00000028 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::set_ClientSecret(System.String)
extern void CognitoUserPool_set_ClientSecret_mA43DA79CA3AE2C9E11018C6FDB6624DE967F24AB (void);
// 0x00000029 System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserPool::.ctor(System.String,System.String,Amazon.CognitoIdentityProvider.IAmazonCognitoIdentityProvider,System.String)
extern void CognitoUserPool__ctor_m6619C54386E0E1725FB1EA008014B30596D19694 (void);
// 0x0000002A System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_IdToken(System.String)
extern void CognitoUserSession_set_IdToken_m16577680A752E865B717BA8AD0B964C0B1F1A737 (void);
// 0x0000002B System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_AccessToken(System.String)
extern void CognitoUserSession_set_AccessToken_mA221525DBBEB9015769F330E192C39E601DEB16B (void);
// 0x0000002C System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_RefreshToken(System.String)
extern void CognitoUserSession_set_RefreshToken_m4E68B941BBCE29ACA651470C2C9D556B8BB01424 (void);
// 0x0000002D System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_ExpirationTime(System.DateTime)
extern void CognitoUserSession_set_ExpirationTime_mF772E4FABBD13882FFE731AD15AD2F7E1AD89408 (void);
// 0x0000002E System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::set_IssuedTime(System.DateTime)
extern void CognitoUserSession_set_IssuedTime_m837B035F367ADE8CB0805D8E2CD1255F4508CBAD (void);
// 0x0000002F System.Void Amazon.Extensions.CognitoAuthentication.CognitoUserSession::.ctor(System.String,System.String,System.String,System.DateTime,System.DateTime)
extern void CognitoUserSession__ctor_m8AB71BE7C28B14D88DDF96E4EDC7B36318FE7686 (void);
// 0x00000030 System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::get_Prk()
extern void HkdfSha256_get_Prk_m3CB2E31FFA5572F37FB59373AE5E4B955B967F8A (void);
// 0x00000031 System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::set_Prk(System.Byte[])
extern void HkdfSha256_set_Prk_m3BFB02703CF8FF382308FD49349D7A8753ABF281 (void);
// 0x00000032 System.Security.Cryptography.HMACSHA256 Amazon.Extensions.CognitoAuthentication.HkdfSha256::get_HmacSha256()
extern void HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2 (void);
// 0x00000033 System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::set_HmacSha256(System.Security.Cryptography.HMACSHA256)
extern void HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107 (void);
// 0x00000034 System.Void Amazon.Extensions.CognitoAuthentication.HkdfSha256::.ctor(System.Byte[],System.Byte[])
extern void HkdfSha256__ctor_mF448325D81511381115C9D024636F8386C6C2697 (void);
// 0x00000035 System.Byte[] Amazon.Extensions.CognitoAuthentication.HkdfSha256::Expand(System.Byte[],System.Int32)
extern void HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F (void);
// 0x00000036 System.Void Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::.cctor()
extern void AuthenticationHelper__cctor_m3AAA58B4C6C214BBE2F2E48208EA0C0022EE497F (void);
// 0x00000037 System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger> Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::CreateAaTuple()
extern void AuthenticationHelper_CreateAaTuple_m953054F2E0C5CB363C35BC45F7DD4C854265716F (void);
// 0x00000038 System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::AuthenticateUser(System.String,System.String,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>,System.String,System.String,System.String,System.String)
extern void AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1 (void);
// 0x00000039 System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::GetPasswordAuthenticationKey(System.String,System.String,System.String,System.Tuple`2<System.Numerics.BigInteger,System.Numerics.BigInteger>,System.Numerics.BigInteger,System.Numerics.BigInteger)
extern void AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7 (void);
// 0x0000003A System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.AuthenticationHelper::CreateBigIntegerRandom()
extern void AuthenticationHelper_CreateBigIntegerRandom_mDF7B2E5C2136FF32F7F58A1EFF9E0732ACB31170 (void);
// 0x0000003B System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::FromUnsignedLittleEndianHex(System.String)
extern void BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9 (void);
// 0x0000003C System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::TrueMod(System.Numerics.BigInteger,System.Numerics.BigInteger)
extern void BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E (void);
// 0x0000003D System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::ToBigEndianByteArray(System.Numerics.BigInteger)
extern void BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0 (void);
// 0x0000003E System.Numerics.BigInteger Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::FromUnsignedBigEndian(System.Byte[])
extern void BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5 (void);
// 0x0000003F T[] Amazon.Extensions.CognitoAuthentication.Util.BigIntegerExtensions::Reverse(T[])
// 0x00000040 System.Security.Cryptography.SHA256 Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::get_Sha256()
extern void CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9 (void);
// 0x00000041 System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::GetUserPoolSecretHash(System.String,System.String,System.String)
extern void CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193 (void);
// 0x00000042 System.Byte[] Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::CombineBytes(System.Byte[][])
extern void CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368 (void);
// 0x00000043 System.Void Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::ServiceClientBeforeRequestEvent(System.Object,Amazon.Runtime.RequestEventArgs)
extern void CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C (void);
// 0x00000044 System.String Amazon.Extensions.CognitoAuthentication.Util.CognitoAuthHelper::GetAssemblyFileVersion()
extern void CognitoAuthHelper_GetAssemblyFileVersion_mD4696F4A47F83B7C0A8213149128CC6167F81428 (void);
// 0x00000045 System.Void Amazon.Extensions.CognitoAuthentication.Util.CognitoConstants::.cctor()
extern void CognitoConstants__cctor_mB8079C41D65E9D1DA7F75A388EEF26F9114D45CD (void);
static Il2CppMethodPointer s_methodPointers[69] = 
{
	AuthFlowResponse__ctor_m91D7AB173C699D4A5387CDC348C62C48E10B4F6B,
	AuthFlowResponse_get_AuthenticationResult_m69D5253C4F74C2918CF23F56A945B69AC9A51F52,
	InitiateSrpAuthRequest_get_Password_m982ED4A1021C780D0958145837076E2EDDB8E869,
	InitiateSrpAuthRequest_set_Password_m4A51EE4021B555BB3AAA7AFB522CB93078788A9C,
	InitiateSrpAuthRequest__ctor_m17B9D655D90C319F19F22FE58FF9EBB61EF2168C,
	CognitoDevice_get_DeviceKey_m645E77D6F11CD1DCC2AD882688D7545ED658B094,
	CognitoUser_get_ClientSecret_m1FC5C231C995017E05BEF862CCC0734787595A67,
	CognitoUser_set_ClientSecret_m0013271AF0F323BFF73DF5B7E422946882F58364,
	CognitoUser_get_SecretHash_m1FCB2073B2703446B01998388FFD77B5056B8A98,
	CognitoUser_set_SecretHash_m9D3DA46F9CC88E12F6E9905DA6B37AEB298A59D3,
	CognitoUser_set_SessionTokens_m97CDECB1CFE6DE199A3B5BC3BDADE247C01E1CA6,
	CognitoUser_get_Device_m96C84E83FBAD170D2E8EEBD77ED7C43F5C271579,
	CognitoUser_get_UserID_m858BEACD4ACC7D4EC6462D0A2229A7B4EA6EF72F,
	CognitoUser_set_UserID_mA7CF427A7E4ED35C62A09920A8D6D3E7B5A20266,
	CognitoUser_get_Username_m27A2DFBD893FC3A039A8B46A4A9B8DD9C383B06B,
	CognitoUser_set_Username_m9061C7E14DE7DB98A3F76AEB5B320B6F9D1E1B3C,
	CognitoUser_set_UserPool_mEC8B2A8E3CE2AC7A66ADF8A201F4C2BA6DCCF9D0,
	CognitoUser_get_ClientID_mE50ACA9AE9D1DA740ECE011D86D123A1295375A5,
	CognitoUser_set_ClientID_m7D5C25601A2033D3887954A0A64B7DEBF0A8233F,
	CognitoUser_set_Status_m6AC74726AEECA36363F06AFC578491E8CB06A54A,
	CognitoUser_get_Provider_m430C8993A07AB586B01DC2E547401BA0537FE403,
	CognitoUser_set_Provider_m384C1E82B82132EAA78A0C0D689A7EDF24C0CBBA,
	CognitoUser_set_Attributes_m6CFCBD03B13DA55D2275D8CD7F6DC9A25B5EF72A,
	CognitoUser_get_PoolName_mB595FBE47D57E0782C042D1D3DF9FF6F32FA62AA,
	CognitoUser_set_PoolName_mB319666D9228CDCF803B56AE31006CB185FCBCAC,
	CognitoUser__ctor_mF9AE181160DA6741A661389AED15CCD6664AC209,
	CognitoUser_GetCognitoUserSession_m29915544522BBCC536F1C1BAC2C8BBB2132993E3,
	CognitoUser_StartWithSrpAuthAsync_mE52637A0345C40DE1DCEE198F6F5BD57628E432C,
	CognitoUser_UpdateSessionIfAuthenticationComplete_mAFC4BB94CC2465C1AE868AC184556708D60AB428,
	CognitoUser_CreateSrpAuthRequest_mBE2390FC87AA9842064CADD5EB303FA6AF9ECC40,
	CognitoUser_UpdateUsernameAndSecretHash_m80C753373FEE74947273595CB468782F8C359126,
	CognitoUser_CreateSrpPasswordVerifierAuthRequest_m95438D8D72A119C7014EADF2D560BE99093A1C63,
	U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209,
	U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1,
	CognitoUserPool_get_PoolID_m1EEF30C165C6AE77D3CC3715DADFDC524F651DA6,
	CognitoUserPool_set_PoolID_m989C7C62DC1F23F7DCFBF451FF8D0252E127835A,
	CognitoUserPool_set_ClientID_m511971B0104BDE5498C581F7F371A00878B73222,
	CognitoUserPool_get_Provider_mDA59C9E589F8E745E08190C267073925670785F0,
	CognitoUserPool_set_Provider_m475CC10ECC82018ABDC254A949C3DA2864A8A30B,
	CognitoUserPool_set_ClientSecret_mA43DA79CA3AE2C9E11018C6FDB6624DE967F24AB,
	CognitoUserPool__ctor_m6619C54386E0E1725FB1EA008014B30596D19694,
	CognitoUserSession_set_IdToken_m16577680A752E865B717BA8AD0B964C0B1F1A737,
	CognitoUserSession_set_AccessToken_mA221525DBBEB9015769F330E192C39E601DEB16B,
	CognitoUserSession_set_RefreshToken_m4E68B941BBCE29ACA651470C2C9D556B8BB01424,
	CognitoUserSession_set_ExpirationTime_mF772E4FABBD13882FFE731AD15AD2F7E1AD89408,
	CognitoUserSession_set_IssuedTime_m837B035F367ADE8CB0805D8E2CD1255F4508CBAD,
	CognitoUserSession__ctor_m8AB71BE7C28B14D88DDF96E4EDC7B36318FE7686,
	HkdfSha256_get_Prk_m3CB2E31FFA5572F37FB59373AE5E4B955B967F8A,
	HkdfSha256_set_Prk_m3BFB02703CF8FF382308FD49349D7A8753ABF281,
	HkdfSha256_get_HmacSha256_m09F35E1BAD6671AA526024670B2F3D3C41FFEAE2,
	HkdfSha256_set_HmacSha256_mB6A4A838742BACC8594B38C69255D324C37BF107,
	HkdfSha256__ctor_mF448325D81511381115C9D024636F8386C6C2697,
	HkdfSha256_Expand_m9380AAF3CE076D941AC4B0685306DB647EE0544F,
	AuthenticationHelper__cctor_m3AAA58B4C6C214BBE2F2E48208EA0C0022EE497F,
	AuthenticationHelper_CreateAaTuple_m953054F2E0C5CB363C35BC45F7DD4C854265716F,
	AuthenticationHelper_AuthenticateUser_m9850FEA3C9BF5E6F3940C30383CEDA82C07ACFB1,
	AuthenticationHelper_GetPasswordAuthenticationKey_m7D33A1E0B420D7C2D333868744C222389849D4D7,
	AuthenticationHelper_CreateBigIntegerRandom_mDF7B2E5C2136FF32F7F58A1EFF9E0732ACB31170,
	BigIntegerExtensions_FromUnsignedLittleEndianHex_m51C3F0C501860BC399AC123F9CFB78809B77DCE9,
	BigIntegerExtensions_TrueMod_mCD5A0713A61EE2EB6401273A1E3080B8429F641E,
	BigIntegerExtensions_ToBigEndianByteArray_m9DE58D8B8389E1991B4980E218734659D1DDC6A0,
	BigIntegerExtensions_FromUnsignedBigEndian_mFBD0F4D7FC1F895F1583640114A5DCEDF7E932A5,
	NULL,
	CognitoAuthHelper_get_Sha256_m39250F1B24B273F2D118903583E544C67A8FB9A9,
	CognitoAuthHelper_GetUserPoolSecretHash_m4994E458FA05904CA6504B72B0995301180B9193,
	CognitoAuthHelper_CombineBytes_m25B34DEE2A196753581D37B8A9FA374475D1C368,
	CognitoAuthHelper_ServiceClientBeforeRequestEvent_mEBC4D7A7FB31608A8A72637E2E2924B346B4956C,
	CognitoAuthHelper_GetAssemblyFileVersion_mD4696F4A47F83B7C0A8213149128CC6167F81428,
	CognitoConstants__cctor_mB8079C41D65E9D1DA7F75A388EEF26F9114D45CD,
};
extern void U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209_AdjustorThunk (void);
extern void U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000021, U3CStartWithSrpAuthAsyncU3Ed__81_MoveNext_mB436AD8C4FCB58EDDD0DC28065627210BD719209_AdjustorThunk },
	{ 0x06000022, U3CStartWithSrpAuthAsyncU3Ed__81_SetStateMachine_m935C8663BAB7842FA1DFD365F112633AD8FB90C1_AdjustorThunk },
};
static const int32_t s_InvokerIndices[69] = 
{
	401,
	4169,
	4169,
	3450,
	4257,
	4169,
	4169,
	3450,
	4169,
	3450,
	3450,
	4169,
	4169,
	3450,
	4169,
	3450,
	3450,
	4169,
	3450,
	3450,
	4169,
	3450,
	3450,
	4169,
	3450,
	95,
	1417,
	2558,
	1920,
	2558,
	3450,
	977,
	4257,
	3450,
	4169,
	3450,
	3450,
	4169,
	3450,
	3450,
	782,
	3450,
	3450,
	3450,
	3402,
	3402,
	396,
	4169,
	3450,
	4169,
	3450,
	1920,
	1416,
	6598,
	6579,
	4379,
	4495,
	6558,
	6134,
	5515,
	6293,
	6134,
	-1,
	6579,
	5272,
	6309,
	6005,
	6579,
	6598,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600003F, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 6753 },
};
extern const CustomAttributesCacheGenerator g_Amazon_Extensions_CognitoAuthentication_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Amazon_Extensions_CognitoAuthentication_CodeGenModule;
const Il2CppCodeGenModule g_Amazon_Extensions_CognitoAuthentication_CodeGenModule = 
{
	"Amazon.Extensions.CognitoAuthentication.dll",
	69,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_Amazon_Extensions_CognitoAuthentication_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
