﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials::get_IsIdentitySet()
extern void CognitoAWSCredentials_get_IsIdentitySet_m13AAABF428E9EFA80FB1C4D14B21DD75304AC256 (void);
// 0x00000002 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::UpdateIdentity(System.String)
extern void CognitoAWSCredentials_UpdateIdentity_mFE7B63CB907EDBAA4FC0E9160595476734551CE9 (void);
// 0x00000003 System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_AccountId()
extern void CognitoAWSCredentials_get_AccountId_mFD716F20C332024EB6451626EB289652EF60DE5C (void);
// 0x00000004 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_AccountId(System.String)
extern void CognitoAWSCredentials_set_AccountId_m4C60662CDCEE59A214A9E52D4F4A3B4DB7213871 (void);
// 0x00000005 System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_IdentityPoolId()
extern void CognitoAWSCredentials_get_IdentityPoolId_m05675F5728B4D5B22E9D1EFCC9C3E11ABE37CF01 (void);
// 0x00000006 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_IdentityPoolId(System.String)
extern void CognitoAWSCredentials_set_IdentityPoolId_m383C1E4CCC30C06769DC56DA43A9C5EE4ADF8687 (void);
// 0x00000007 System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_UnAuthRoleArn()
extern void CognitoAWSCredentials_get_UnAuthRoleArn_m8AB13E52FCF387418A0112308CC4AB7C2BAC67CC (void);
// 0x00000008 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_UnAuthRoleArn(System.String)
extern void CognitoAWSCredentials_set_UnAuthRoleArn_m95C4EAE379B90E37891399167036A5C8C1E59764 (void);
// 0x00000009 System.String Amazon.CognitoIdentity.CognitoAWSCredentials::get_AuthRoleArn()
extern void CognitoAWSCredentials_get_AuthRoleArn_m7D015E01B7C59D324A69DBD1AE96FD42BDF65531 (void);
// 0x0000000A System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_AuthRoleArn(System.String)
extern void CognitoAWSCredentials_set_AuthRoleArn_m7C215B3F5A6EA88F9F6A3B1804AB8651B15254BA (void);
// 0x0000000B System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.CognitoAWSCredentials::get_Logins()
extern void CognitoAWSCredentials_get_Logins_m2A5C730D3ED5F54AEBFCD2895989C28A60B3B879 (void);
// 0x0000000C System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void CognitoAWSCredentials_set_Logins_m4041C4F1E605BD69496E7469725DE644B3A6F336 (void);
// 0x0000000D System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetIdentityId(Amazon.CognitoIdentity.CognitoAWSCredentials/RefreshIdentityOptions)
extern void CognitoAWSCredentials_GetIdentityId_m83D93345B15F70974571CF7D43F9AC4562FA3A1E (void);
// 0x0000000E Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState Amazon.CognitoIdentity.CognitoAWSCredentials::RefreshIdentity()
extern void CognitoAWSCredentials_RefreshIdentity_m4C877CF3CD142BDFB483A7E84D6B10A39986127F (void);
// 0x0000000F System.Threading.Tasks.Task`1<System.String> Amazon.CognitoIdentity.CognitoAWSCredentials::GetIdentityIdAsync(Amazon.CognitoIdentity.CognitoAWSCredentials/RefreshIdentityOptions)
extern void CognitoAWSCredentials_GetIdentityIdAsync_mD72497165B81784DC891F0E1E3AC9F58C2F9A386 (void);
// 0x00000010 System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState> Amazon.CognitoIdentity.CognitoAWSCredentials::RefreshIdentityAsync()
extern void CognitoAWSCredentials_RefreshIdentityAsync_m9DCA06AD0B2AF5AC8370CD392C58CAF39DEB0763 (void);
// 0x00000011 System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials::ShouldRetry(Amazon.CognitoIdentity.AmazonCognitoIdentityException)
extern void CognitoAWSCredentials_ShouldRetry_m2BF997DB3C4D74A947D4B9591AB1235AD14406BF (void);
// 0x00000012 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,Amazon.RegionEndpoint)
extern void CognitoAWSCredentials__ctor_m412D51EE3B61F3EF3F413F2F7E5E4E0C93F78DDF (void);
// 0x00000013 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,System.String,System.String,System.String,Amazon.RegionEndpoint)
extern void CognitoAWSCredentials__ctor_m67305AEA80A45CBFE9EFBA90D1DC65936D7EC7F5 (void);
// 0x00000014 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.ctor(System.String,System.String,System.String,System.String,Amazon.CognitoIdentity.IAmazonCognitoIdentity,Amazon.SecurityToken.IAmazonSecurityTokenService)
extern void CognitoAWSCredentials__ctor_mD44A7437213EB568986091AF15DAE88EA96900CC (void);
// 0x00000015 System.Threading.Tasks.Task`1<Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState> Amazon.CognitoIdentity.CognitoAWSCredentials::GenerateNewCredentialsAsync()
extern void CognitoAWSCredentials_GenerateNewCredentialsAsync_mDDA7ACD8FD9077EDA3EC620A8D57BBEB23CDEC0D (void);
// 0x00000016 System.Threading.Tasks.Task`1<Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState> Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForRoleAsync(System.String)
extern void CognitoAWSCredentials_GetCredentialsForRoleAsync_m1B7058BA1EB066807F8A53B2F1F0510F5A68C44D (void);
// 0x00000017 System.Threading.Tasks.Task`1<Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState> Amazon.CognitoIdentity.CognitoAWSCredentials::GetPoolCredentialsAsync()
extern void CognitoAWSCredentials_GetPoolCredentialsAsync_mC335FFDC27B3FF3EFDA900C96AE2C5352D7E57A8 (void);
// 0x00000018 Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GenerateNewCredentials()
extern void CognitoAWSCredentials_GenerateNewCredentials_m8B202385BC854DB92E5D2E648FB2A0B12C4D3394 (void);
// 0x00000019 Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetPoolCredentials()
extern void CognitoAWSCredentials_GetPoolCredentials_m5812F8508FFEF3F6C35ADD730F68D63410223872 (void);
// 0x0000001A Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForRole(System.String)
extern void CognitoAWSCredentials_GetCredentialsForRole_m9F996CCA90EAD82DB284D7312153B70A425FA1ED (void);
// 0x0000001B Amazon.SecurityToken.Model.Credentials Amazon.CognitoIdentity.CognitoAWSCredentials::GetStsCredentials(Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityRequest)
extern void CognitoAWSCredentials_GetStsCredentials_mEA186FA8335FC391215241FF6E93F1CF561711CE (void);
// 0x0000001C Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse Amazon.CognitoIdentity.CognitoAWSCredentials::GetOpenId(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern void CognitoAWSCredentials_GetOpenId_m035E6A5E5BCFD3C0B214A163D428C07BA23C73A8 (void);
// 0x0000001D Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse Amazon.CognitoIdentity.CognitoAWSCredentials::GetCredentialsForIdentity(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern void CognitoAWSCredentials_GetCredentialsForIdentity_m975DB6BFE2263862731694747D258CA790E90F5E (void);
// 0x0000001E System.String Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedIdentityId()
extern void CognitoAWSCredentials_GetCachedIdentityId_m2BF6C19C11D4EFC1159E8A44AA2DDB8216F26377 (void);
// 0x0000001F System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheIdentityId(System.String)
extern void CognitoAWSCredentials_CacheIdentityId_m9C2F35E1D659CE2A0BB40934589FE9B1FD2F11EC (void);
// 0x00000020 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::ClearIdentityCache()
extern void CognitoAWSCredentials_ClearIdentityCache_m71332DFF639434FD29043A453FE73C48B5702939 (void);
// 0x00000021 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::CacheCredentials(Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState)
extern void CognitoAWSCredentials_CacheCredentials_m6812B334969D39748D96FE34B2C26177B814FA97 (void);
// 0x00000022 Amazon.Runtime.RefreshingAWSCredentials/CredentialsRefreshState Amazon.CognitoIdentity.CognitoAWSCredentials::GetCachedCredentials()
extern void CognitoAWSCredentials_GetCachedCredentials_m19B7E9463FD7D3A6AE9D9D6758CB9D9AD2FF5C59 (void);
// 0x00000023 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials::.cctor()
extern void CognitoAWSCredentials__cctor_mFF1116D5A081B1C1FFAC8562AFB61531E90A0B6D (void);
// 0x00000024 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::set_OldIdentityId(System.String)
extern void IdentityChangedArgs_set_OldIdentityId_m57CD3C9B5A788DF970FDA9FA43D59619C91FFF10 (void);
// 0x00000025 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::set_NewIdentityId(System.String)
extern void IdentityChangedArgs_set_NewIdentityId_mA6F59055D45797660071EDF2A0D2941B58A68F74 (void);
// 0x00000026 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityChangedArgs::.ctor(System.String,System.String)
extern void IdentityChangedArgs__ctor_m6C1763DD1181DCC22E85A5D924ABC33B7598F6EE (void);
// 0x00000027 System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_IdentityId()
extern void IdentityState_get_IdentityId_mBB9A188EF26616327FCA450C6AB24C0631E4EE57 (void);
// 0x00000028 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::set_IdentityId(System.String)
extern void IdentityState_set_IdentityId_m096546285D286E36CEF28C0E44A97A11C455F88C (void);
// 0x00000029 System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginProvider()
extern void IdentityState_get_LoginProvider_mADBDD700DBBAA6B2B98D487235A679B619FBA85C (void);
// 0x0000002A System.String Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginToken()
extern void IdentityState_get_LoginToken_m5D27CC5E79D7281A37B19A672AE357F794924CF0 (void);
// 0x0000002B System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::set_FromCache(System.Boolean)
extern void IdentityState_set_FromCache_mAEE2CCBEB1465FAC2717C785A8DA28A60E547003 (void);
// 0x0000002C System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::.ctor(System.String,System.Boolean)
extern void IdentityState__ctor_m5E5C2BC7C0CF154A91519B599FBED9CB23A93C89 (void);
// 0x0000002D System.Boolean Amazon.CognitoIdentity.CognitoAWSCredentials/IdentityState::get_LoginSpecified()
extern void IdentityState_get_LoginSpecified_mEB21EBBE919D40D5F3CF506D4FB605D888D7CECB (void);
// 0x0000002E System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mDB94458F1A95E18AD1321A2368F8669ADD7B0B27 (void);
// 0x0000002F System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetIdResponse> Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass45_0::<RefreshIdentity>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CRefreshIdentityU3Eb__0_mA53BB7E839847B5E483DAA2F76A28E33E0986983 (void);
// 0x00000030 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GetIdentityIdAsync>d__47::MoveNext()
extern void U3CGetIdentityIdAsyncU3Ed__47_MoveNext_m00FC6D0D3345FF3F6A2226D9022D682DF5A07000 (void);
// 0x00000031 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GetIdentityIdAsync>d__47::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetIdentityIdAsyncU3Ed__47_SetStateMachine_mB885C55C150DB3C1E0E333BC4025A7E48C8A9BEF (void);
// 0x00000032 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<RefreshIdentityAsync>d__48::MoveNext()
extern void U3CRefreshIdentityAsyncU3Ed__48_MoveNext_mD20AC5AD8B0943A6039672E182E20562A05C2E71 (void);
// 0x00000033 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<RefreshIdentityAsync>d__48::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRefreshIdentityAsyncU3Ed__48_SetStateMachine_m6E25BA598D8051AAF0B1D1AD045CFA2B52FFABFA (void);
// 0x00000034 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GenerateNewCredentialsAsync>d__57::MoveNext()
extern void U3CGenerateNewCredentialsAsyncU3Ed__57_MoveNext_m09E119787F68E4EF33D6E7DDED21929234EEDBD4 (void);
// 0x00000035 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GenerateNewCredentialsAsync>d__57::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGenerateNewCredentialsAsyncU3Ed__57_SetStateMachine_m1E2ED47C7984178238597DB655BB1C3895C04DB4 (void);
// 0x00000036 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GetCredentialsForRoleAsync>d__58::MoveNext()
extern void U3CGetCredentialsForRoleAsyncU3Ed__58_MoveNext_mF326983BD636AD5685B89C4C986AF7A5774C2228 (void);
// 0x00000037 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GetCredentialsForRoleAsync>d__58::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetCredentialsForRoleAsyncU3Ed__58_SetStateMachine_m610D75E395FF843F8D80FA0178D947383E9B2C45 (void);
// 0x00000038 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GetPoolCredentialsAsync>d__59::MoveNext()
extern void U3CGetPoolCredentialsAsyncU3Ed__59_MoveNext_mFBAF38779D4B18604BB6C77E366C300BF8B14090 (void);
// 0x00000039 System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<GetPoolCredentialsAsync>d__59::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetPoolCredentialsAsyncU3Ed__59_SetStateMachine_mF363EC4A748410BD927F3911BF889C621DD0ACCE (void);
// 0x0000003A System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass64_0::.ctor()
extern void U3CU3Ec__DisplayClass64_0__ctor_m3B10DE1786CF12287D49CE6E4316EA0BF3DEE70E (void);
// 0x0000003B System.Threading.Tasks.Task`1<Amazon.SecurityToken.Model.AssumeRoleWithWebIdentityResponse> Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass64_0::<GetStsCredentials>b__0()
extern void U3CU3Ec__DisplayClass64_0_U3CGetStsCredentialsU3Eb__0_m9E43296999F10EC1C0FEC55052C7F5528F5CEFF4 (void);
// 0x0000003C System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_m7BF04F68251781E95DBF18E044D6741770075E2D (void);
// 0x0000003D System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse> Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass65_0::<GetOpenId>b__0()
extern void U3CU3Ec__DisplayClass65_0_U3CGetOpenIdU3Eb__0_m0B52C53EC65FED7F5772A9E9AA7D46E4CF18E49E (void);
// 0x0000003E System.Void Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m36493693A0B481B7FF200A72C716557085811CFA (void);
// 0x0000003F System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse> Amazon.CognitoIdentity.CognitoAWSCredentials/<>c__DisplayClass66_0::<GetCredentialsForIdentity>b__0()
extern void U3CU3Ec__DisplayClass66_0_U3CGetCredentialsForIdentityU3Eb__0_m657514A1691AC6F4BF229630112208B0C9803A2C (void);
// 0x00000040 System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::.ctor()
extern void AmazonCognitoIdentityConfig__ctor_m0FEE123B8409EAA2AA42299EF109871A586572C3 (void);
// 0x00000041 System.String Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::get_RegionEndpointServiceName()
extern void AmazonCognitoIdentityConfig_get_RegionEndpointServiceName_mB8DB4C70AB81992549C5FDEC6577D3CA738ADAD9 (void);
// 0x00000042 System.String Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::get_UserAgent()
extern void AmazonCognitoIdentityConfig_get_UserAgent_m203661DF0ED06A6C6E79A7AEBA53EFD855B3DD48 (void);
// 0x00000043 System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityConfig::.cctor()
extern void AmazonCognitoIdentityConfig__cctor_mD3EA82CCA6F20C3C32038919ED2AD9097F3BB643 (void);
// 0x00000044 System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void AmazonCognitoIdentityException__ctor_mCF7A45F631393CF87B4FBAA63D2E844E6A053187 (void);
// 0x00000045 System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityRequest::.ctor()
extern void AmazonCognitoIdentityRequest__ctor_m2C3E447C170229E417C6E1BC1F6170F05CACAFCC (void);
// 0x00000046 System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
extern void AmazonCognitoIdentityClient__ctor_m9BCA5582B54BF239F97DA2A5DE547A5AB51E459B (void);
// 0x00000047 System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.CognitoIdentity.AmazonCognitoIdentityConfig)
extern void AmazonCognitoIdentityClient__ctor_m34C0E7D688FF6A81AB85C5481A0F68668825AF44 (void);
// 0x00000048 Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.CognitoIdentity.AmazonCognitoIdentityClient::CreateSigner()
extern void AmazonCognitoIdentityClient_CreateSigner_mAB445871A830781CED9D14EEBC624733B590C97E (void);
// 0x00000049 Amazon.Runtime.Internal.IServiceMetadata Amazon.CognitoIdentity.AmazonCognitoIdentityClient::get_ServiceMetadata()
extern void AmazonCognitoIdentityClient_get_ServiceMetadata_m00007804B67F9CE2A68AD501E91A11C51C1595F2 (void);
// 0x0000004A System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::Dispose(System.Boolean)
extern void AmazonCognitoIdentityClient_Dispose_m2C67C346C8E67B03F211FF3E542CAFA883057556 (void);
// 0x0000004B System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse> Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetCredentialsForIdentityAsync(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityClient_GetCredentialsForIdentityAsync_mCC60AE7379B304FE2E025B4E7F27BDEC77128038 (void);
// 0x0000004C System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetIdResponse> Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetIdAsync(Amazon.CognitoIdentity.Model.GetIdRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityClient_GetIdAsync_mEEBD17F1A517E34AB030C47C0EA07FEF9F0BFB0C (void);
// 0x0000004D System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse> Amazon.CognitoIdentity.AmazonCognitoIdentityClient::GetOpenIdTokenAsync(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest,System.Threading.CancellationToken)
extern void AmazonCognitoIdentityClient_GetOpenIdTokenAsync_m184443804B1F640774B92DB9DBA6154EA4BBAE55 (void);
// 0x0000004E System.Void Amazon.CognitoIdentity.AmazonCognitoIdentityClient::.cctor()
extern void AmazonCognitoIdentityClient__cctor_m507C9E82C7458451561002A8162C73C89E42B92A (void);
// 0x0000004F System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse> Amazon.CognitoIdentity.IAmazonCognitoIdentity::GetCredentialsForIdentityAsync(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest,System.Threading.CancellationToken)
// 0x00000050 System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetIdResponse> Amazon.CognitoIdentity.IAmazonCognitoIdentity::GetIdAsync(Amazon.CognitoIdentity.Model.GetIdRequest,System.Threading.CancellationToken)
// 0x00000051 System.Threading.Tasks.Task`1<Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse> Amazon.CognitoIdentity.IAmazonCognitoIdentity::GetOpenIdTokenAsync(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest,System.Threading.CancellationToken)
// 0x00000052 System.String Amazon.CognitoIdentity.Internal.AmazonCognitoIdentityMetadata::get_ServiceId()
extern void AmazonCognitoIdentityMetadata_get_ServiceId_m7CC609C4710210C573EB2660D5B94F970020CB32 (void);
// 0x00000053 System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.CognitoIdentity.Internal.AmazonCognitoIdentityMetadata::get_OperationNameMapping()
extern void AmazonCognitoIdentityMetadata_get_OperationNameMapping_mC5C36CC8C94D85E51A00E377D2CB8B0D273E6D38 (void);
// 0x00000054 System.Void Amazon.CognitoIdentity.Internal.AmazonCognitoIdentityMetadata::.ctor()
extern void AmazonCognitoIdentityMetadata__ctor_m26F0B9FA9FAFA44CDC7C6FF804310F0DF765BF50 (void);
// 0x00000055 Amazon.Runtime.ImmutableCredentials Amazon.CognitoIdentity.Model.Credentials::GetCredentials()
extern void Credentials_GetCredentials_mBD854DFEEF44537E2DCCAE42A0A802F8954C16F4 (void);
// 0x00000056 System.String Amazon.CognitoIdentity.Model.Credentials::get_AccessKeyId()
extern void Credentials_get_AccessKeyId_mF35D74FCD9495A43F18EDA713EEEDE37D0022402 (void);
// 0x00000057 System.Void Amazon.CognitoIdentity.Model.Credentials::set_AccessKeyId(System.String)
extern void Credentials_set_AccessKeyId_m245151319F8B0AB4CA00BDA3BBBB075864824D2C (void);
// 0x00000058 System.DateTime Amazon.CognitoIdentity.Model.Credentials::get_Expiration()
extern void Credentials_get_Expiration_m8BBAD90F48BC87CE74D5B9A527A8A3B2B3172111 (void);
// 0x00000059 System.Void Amazon.CognitoIdentity.Model.Credentials::set_Expiration(System.DateTime)
extern void Credentials_set_Expiration_m657A76D33641624ED58310FAB44547AB95805A94 (void);
// 0x0000005A System.String Amazon.CognitoIdentity.Model.Credentials::get_SecretKey()
extern void Credentials_get_SecretKey_m0A4DF49A8C334380ABAAFAA7B01347B16F8ABAF7 (void);
// 0x0000005B System.Void Amazon.CognitoIdentity.Model.Credentials::set_SecretKey(System.String)
extern void Credentials_set_SecretKey_mA87FB339155A48B2F0C9F0F45591486025300E0C (void);
// 0x0000005C System.String Amazon.CognitoIdentity.Model.Credentials::get_SessionToken()
extern void Credentials_get_SessionToken_mD13A0B97E4C124FC5E2F1B560F61332E2B685B9D (void);
// 0x0000005D System.Void Amazon.CognitoIdentity.Model.Credentials::set_SessionToken(System.String)
extern void Credentials_set_SessionToken_mB603A57AA73A9826FD8839D33B273FF78BD7E03A (void);
// 0x0000005E System.Void Amazon.CognitoIdentity.Model.Credentials::.ctor()
extern void Credentials__ctor_mC7C7D06A2B6D68DE98AECA4CFBFD0F6B06482612 (void);
// 0x0000005F System.Void Amazon.CognitoIdentity.Model.ExternalServiceException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void ExternalServiceException__ctor_mDD7AB86EED8917A11F1284F89A7785D0B52F3F5C (void);
// 0x00000060 System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_CustomRoleArn()
extern void GetCredentialsForIdentityRequest_get_CustomRoleArn_m42E728435CAF3CBC023A04284D133A2977615D2E (void);
// 0x00000061 System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetCustomRoleArn()
extern void GetCredentialsForIdentityRequest_IsSetCustomRoleArn_mF42421E2318AC63979224BDE6D20CC438A3586EE (void);
// 0x00000062 System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_IdentityId()
extern void GetCredentialsForIdentityRequest_get_IdentityId_m34E4189E29D902BF279C86CF219C2B279FBA2D43 (void);
// 0x00000063 System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::set_IdentityId(System.String)
extern void GetCredentialsForIdentityRequest_set_IdentityId_m8E3F575B31AC70DCD7FAD4BDD055621F1EDC964A (void);
// 0x00000064 System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetIdentityId()
extern void GetCredentialsForIdentityRequest_IsSetIdentityId_m72BE99A2AD7579D42AB59459A5DAD53B610C993E (void);
// 0x00000065 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::get_Logins()
extern void GetCredentialsForIdentityRequest_get_Logins_m49524DB1F8EF4848BB65CD8235BCD92A4EE49572 (void);
// 0x00000066 System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void GetCredentialsForIdentityRequest_set_Logins_mD387BB2C8F2D906BFD3D15BC3254BF0578DA93F8 (void);
// 0x00000067 System.Boolean Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::IsSetLogins()
extern void GetCredentialsForIdentityRequest_IsSetLogins_mF88C662ABB5D753F7A271AE8300E656B1B641E0B (void);
// 0x00000068 System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest::.ctor()
extern void GetCredentialsForIdentityRequest__ctor_m0BA8EEDCBDE36E56FFB6578AA38EC51E90803085 (void);
// 0x00000069 Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::get_Credentials()
extern void GetCredentialsForIdentityResponse_get_Credentials_m6BA24DA23B87F7BF1E5B6B65A5CB0162389CE34F (void);
// 0x0000006A System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::set_Credentials(Amazon.CognitoIdentity.Model.Credentials)
extern void GetCredentialsForIdentityResponse_set_Credentials_m2CA50A2B8CC9C628B982B436E9FFEC16C6325C5B (void);
// 0x0000006B System.String Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::get_IdentityId()
extern void GetCredentialsForIdentityResponse_get_IdentityId_m2B7ABEB049F9AAE01E51FDAB923A93988FA4F1D4 (void);
// 0x0000006C System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::set_IdentityId(System.String)
extern void GetCredentialsForIdentityResponse_set_IdentityId_m0933FFCF0581103AF5C175885C736E7935C19EF0 (void);
// 0x0000006D System.Void Amazon.CognitoIdentity.Model.GetCredentialsForIdentityResponse::.ctor()
extern void GetCredentialsForIdentityResponse__ctor_m63E3A4EDFF00BF294824BD26D84BFA7EAA4954F5 (void);
// 0x0000006E System.String Amazon.CognitoIdentity.Model.GetIdRequest::get_AccountId()
extern void GetIdRequest_get_AccountId_m529B4DFB41D3616D9E92A1CB987A180AE38FF37B (void);
// 0x0000006F System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_AccountId(System.String)
extern void GetIdRequest_set_AccountId_mF6E6187C743DF40D481B572628D7773F4D8D323E (void);
// 0x00000070 System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetAccountId()
extern void GetIdRequest_IsSetAccountId_m6555A37019439F92B7970AEC9EF550F9651CB25D (void);
// 0x00000071 System.String Amazon.CognitoIdentity.Model.GetIdRequest::get_IdentityPoolId()
extern void GetIdRequest_get_IdentityPoolId_m82C523C0DCD4F056A16E71A2A9A56ABF656F2D4F (void);
// 0x00000072 System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_IdentityPoolId(System.String)
extern void GetIdRequest_set_IdentityPoolId_m1C9DD7AD76D42860578FBEAA202AA250E69FC4F6 (void);
// 0x00000073 System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetIdentityPoolId()
extern void GetIdRequest_IsSetIdentityPoolId_m629C1548D05029B391AE0B3CD193024CFEBBE974 (void);
// 0x00000074 System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetIdRequest::get_Logins()
extern void GetIdRequest_get_Logins_mC65B200824F77A998EED06B48B63A77B95120C5A (void);
// 0x00000075 System.Void Amazon.CognitoIdentity.Model.GetIdRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void GetIdRequest_set_Logins_m8DF2D07F4346490B0A5AA83D07E9C5B7A2FC3D61 (void);
// 0x00000076 System.Boolean Amazon.CognitoIdentity.Model.GetIdRequest::IsSetLogins()
extern void GetIdRequest_IsSetLogins_mEE5AE23CB5E75DCD04F5F429AA2D56F5AACF31D5 (void);
// 0x00000077 System.Void Amazon.CognitoIdentity.Model.GetIdRequest::.ctor()
extern void GetIdRequest__ctor_m2F34B7FE9BF604881699BD49ABF1EC57B4C82DCC (void);
// 0x00000078 System.String Amazon.CognitoIdentity.Model.GetIdResponse::get_IdentityId()
extern void GetIdResponse_get_IdentityId_m7F227CF5D59D7FAA0DDAD02D1CDA95758F02E4AC (void);
// 0x00000079 System.Void Amazon.CognitoIdentity.Model.GetIdResponse::set_IdentityId(System.String)
extern void GetIdResponse_set_IdentityId_m25DF124C60C57D9BDA16A7CA48B57F5118275FF2 (void);
// 0x0000007A System.Void Amazon.CognitoIdentity.Model.GetIdResponse::.ctor()
extern void GetIdResponse__ctor_mA308AE6CC674166EC0C5590E399C63148B96DADC (void);
// 0x0000007B System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::get_IdentityId()
extern void GetOpenIdTokenRequest_get_IdentityId_mB1A3E80500CC8F394A2B1F3FDA4567C6D0D30473 (void);
// 0x0000007C System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::set_IdentityId(System.String)
extern void GetOpenIdTokenRequest_set_IdentityId_mC69B1083C1AE8C6319DF3C35B24C0A01D9C08A05 (void);
// 0x0000007D System.Boolean Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::IsSetIdentityId()
extern void GetOpenIdTokenRequest_IsSetIdentityId_m441754571DF725C952B7B17F25C359C22B073117 (void);
// 0x0000007E System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::get_Logins()
extern void GetOpenIdTokenRequest_get_Logins_m3C95985089A1380B5DEF5D47BF9B35A879204112 (void);
// 0x0000007F System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::set_Logins(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void GetOpenIdTokenRequest_set_Logins_mC92315FE6D9216DC17927C72EC7458D0CD54D20A (void);
// 0x00000080 System.Boolean Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::IsSetLogins()
extern void GetOpenIdTokenRequest_IsSetLogins_mDE5DADC3FD71AF83881F6A8E8EC78155A1E43E87 (void);
// 0x00000081 System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest::.ctor()
extern void GetOpenIdTokenRequest__ctor_m80A860EB7C1BA58E9D24CA9813904F0E7CF7C903 (void);
// 0x00000082 System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::get_IdentityId()
extern void GetOpenIdTokenResponse_get_IdentityId_mF9742F5021914F5A8DC7B4BE2BB140DAD6697CC8 (void);
// 0x00000083 System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::set_IdentityId(System.String)
extern void GetOpenIdTokenResponse_set_IdentityId_m3436360D9098D440F862C48CE6E5C9538F9CE421 (void);
// 0x00000084 System.String Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::get_Token()
extern void GetOpenIdTokenResponse_get_Token_m11F1C27CB022CB7EFA4698B87B8A2FE4B9C8129A (void);
// 0x00000085 System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::set_Token(System.String)
extern void GetOpenIdTokenResponse_set_Token_m1E9EDB656A7D151C1D5FAC7279AD8404FBA1561B (void);
// 0x00000086 System.Void Amazon.CognitoIdentity.Model.GetOpenIdTokenResponse::.ctor()
extern void GetOpenIdTokenResponse__ctor_m47A0DAEE658AEA56AF8168A1B9D6B792E8AB1E2D (void);
// 0x00000087 System.Void Amazon.CognitoIdentity.Model.InternalErrorException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InternalErrorException__ctor_m137363AAA8AAF15BE5C8178D478122461A96D1E8 (void);
// 0x00000088 System.Void Amazon.CognitoIdentity.Model.InvalidIdentityPoolConfigurationException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidIdentityPoolConfigurationException__ctor_m47664B25947956DB17BDDBFE5F000D2C72653E0C (void);
// 0x00000089 System.Void Amazon.CognitoIdentity.Model.InvalidParameterException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void InvalidParameterException__ctor_mBC304B89C69ABDE32ECCF36C30614E5C2E39CA7C (void);
// 0x0000008A System.Void Amazon.CognitoIdentity.Model.LimitExceededException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void LimitExceededException__ctor_mE91C7617C1BDF10B2791B5D366AE929820FF4D92 (void);
// 0x0000008B System.Void Amazon.CognitoIdentity.Model.NotAuthorizedException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void NotAuthorizedException__ctor_mB04D18389D774832BD29F44869FDF9940ADEB99F (void);
// 0x0000008C System.Void Amazon.CognitoIdentity.Model.ResourceConflictException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void ResourceConflictException__ctor_m98FC1278DA3E8EAB10AED6C5A5F436EA384A81F2 (void);
// 0x0000008D System.Void Amazon.CognitoIdentity.Model.ResourceNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void ResourceNotFoundException__ctor_m26098F5B0C9D890C9A2E84863A47D25D3F9756BB (void);
// 0x0000008E System.Void Amazon.CognitoIdentity.Model.TooManyRequestsException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
extern void TooManyRequestsException__ctor_mE0A1A5568FEDFBFC8BBB36B89BE07CBB28596D64 (void);
// 0x0000008F Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.CognitoIdentity.Model.Credentials,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
extern void CredentialsUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentity_Model_CredentialsU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m704132388546F3108E3A0BAB3A89F027D4A06C1C (void);
// 0x00000090 Amazon.CognitoIdentity.Model.Credentials Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void CredentialsUnmarshaller_Unmarshall_m423DB824F4A6DE2BD4AF8A2E0243D61054FBD1CE (void);
// 0x00000091 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::get_Instance()
extern void CredentialsUnmarshaller_get_Instance_m76F819ACF84CC3D2A423C2F244764C538291C3B6 (void);
// 0x00000092 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.ctor()
extern void CredentialsUnmarshaller__ctor_m14012A355AF371EE30794DCA597FDD6C86E6A385 (void);
// 0x00000093 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.CredentialsUnmarshaller::.cctor()
extern void CredentialsUnmarshaller__cctor_m22B59869A752F6B17935D40D617F614BE5CAEF40 (void);
// 0x00000094 Amazon.CognitoIdentity.Model.ExternalServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ExternalServiceExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ExternalServiceExceptionUnmarshaller_Unmarshall_m839AF8CBB150F65E824138C56EC0125FD4A737C1 (void);
// 0x00000095 Amazon.CognitoIdentity.Model.ExternalServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ExternalServiceExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void ExternalServiceExceptionUnmarshaller_Unmarshall_mC51A7AFC2D017D3047EFE09638E4A92C41111E2D (void);
// 0x00000096 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ExternalServiceExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ExternalServiceExceptionUnmarshaller::get_Instance()
extern void ExternalServiceExceptionUnmarshaller_get_Instance_mD2CD0B7BEFB5328208BF53FC5D4FC3263762E914 (void);
// 0x00000097 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ExternalServiceExceptionUnmarshaller::.ctor()
extern void ExternalServiceExceptionUnmarshaller__ctor_m98721D6A7F9B7D052236F6D69358DC26884F27AA (void);
// 0x00000098 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ExternalServiceExceptionUnmarshaller::.cctor()
extern void ExternalServiceExceptionUnmarshaller__cctor_m8125A445B3C75DCCF4561DB5F60B62BCDF12F848 (void);
// 0x00000099 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void GetCredentialsForIdentityRequestMarshaller_Marshall_m83EFD1CE72779522C69B1D11DADD211C2F1495A9 (void);
// 0x0000009A Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetCredentialsForIdentityRequest)
extern void GetCredentialsForIdentityRequestMarshaller_Marshall_m244CD4351D03FD70D4D99CAA610E88EF70824A92 (void);
// 0x0000009B Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::get_Instance()
extern void GetCredentialsForIdentityRequestMarshaller_get_Instance_m5260E183EC0FAC9A0D32DF53696DA78D053E65B9 (void);
// 0x0000009C System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::.ctor()
extern void GetCredentialsForIdentityRequestMarshaller__ctor_mBB32DB33C46EBEEA5B7F7A5F8D3B1EC476C539C6 (void);
// 0x0000009D System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityRequestMarshaller::.cctor()
extern void GetCredentialsForIdentityRequestMarshaller__cctor_mDB57A42C4DF0C594180638FAA7E37B69812974D7 (void);
// 0x0000009E Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void GetCredentialsForIdentityResponseUnmarshaller_Unmarshall_mEC9F9C5A545017A80D7D9673114EE03F0CB964C8 (void);
// 0x0000009F Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void GetCredentialsForIdentityResponseUnmarshaller_UnmarshallException_mE54A1CE4BC8A5AE58F8F9823CFA2BFC7A55B646D (void);
// 0x000000A0 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::get_Instance()
extern void GetCredentialsForIdentityResponseUnmarshaller_get_Instance_mB25E2CF51A33A0D5776B4F5D9CEBAE373C772027 (void);
// 0x000000A1 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::.ctor()
extern void GetCredentialsForIdentityResponseUnmarshaller__ctor_m3C998CED24541F0D170D0F57EFAA7067718AB0B2 (void);
// 0x000000A2 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetCredentialsForIdentityResponseUnmarshaller::.cctor()
extern void GetCredentialsForIdentityResponseUnmarshaller__cctor_mC88E011FDED6A8DA84AF353C162026910E287681 (void);
// 0x000000A3 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void GetIdRequestMarshaller_Marshall_m692F69EA825E7FFD788F7370C574419B22B05A0A (void);
// 0x000000A4 Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetIdRequest)
extern void GetIdRequestMarshaller_Marshall_m2C89D6D019768452814DD5139E8D27C772DCEB21 (void);
// 0x000000A5 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::get_Instance()
extern void GetIdRequestMarshaller_get_Instance_mD4A6F4DC4DC2C498B2B8B7446DCFB5EB45585741 (void);
// 0x000000A6 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::.ctor()
extern void GetIdRequestMarshaller__ctor_m3415657A14A282AC515BC7904FB89ADB641CCF7B (void);
// 0x000000A7 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdRequestMarshaller::.cctor()
extern void GetIdRequestMarshaller__cctor_m2372B1A39500983D3327397F71D68675699A9D8F (void);
// 0x000000A8 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void GetIdResponseUnmarshaller_Unmarshall_mD2360DDA9C7B58FB961306A4BC02B3D4166CF1E8 (void);
// 0x000000A9 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void GetIdResponseUnmarshaller_UnmarshallException_mED9E65300BBA68B8810396C27BEE543E8C94F029 (void);
// 0x000000AA Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::get_Instance()
extern void GetIdResponseUnmarshaller_get_Instance_mBD118C8FF883437C0C22CB43F82DE1A1D45C2565 (void);
// 0x000000AB System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::.ctor()
extern void GetIdResponseUnmarshaller__ctor_m48B964008DAB6834FCC6BD92C52A2FE85C1185D3 (void);
// 0x000000AC System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetIdResponseUnmarshaller::.cctor()
extern void GetIdResponseUnmarshaller__cctor_mA4B15853DB95BDC0B24F1326896D9CA0BCD2A3F7 (void);
// 0x000000AD Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
extern void GetOpenIdTokenRequestMarshaller_Marshall_m43AF2A22F00592ACF283BAC31BF5F20DB9C7DE1E (void);
// 0x000000AE Amazon.Runtime.Internal.IRequest Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::Marshall(Amazon.CognitoIdentity.Model.GetOpenIdTokenRequest)
extern void GetOpenIdTokenRequestMarshaller_Marshall_mF4D85DBA858A3235ED043D417634F97EC9678B69 (void);
// 0x000000AF Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::get_Instance()
extern void GetOpenIdTokenRequestMarshaller_get_Instance_m046B1C4DA6B6716848E749E93E0EE757D2884FC9 (void);
// 0x000000B0 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::.ctor()
extern void GetOpenIdTokenRequestMarshaller__ctor_m987E195E5C7768D5AD27D295E2C7FD76BD7125DA (void);
// 0x000000B1 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenRequestMarshaller::.cctor()
extern void GetOpenIdTokenRequestMarshaller__cctor_mCEED94A917308503E6CBB559C94EF265E7CE5295 (void);
// 0x000000B2 Amazon.Runtime.AmazonWebServiceResponse Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void GetOpenIdTokenResponseUnmarshaller_Unmarshall_mA3838F84D5E24384D0E87236429C004FAEF4E4B1 (void);
// 0x000000B3 Amazon.Runtime.AmazonServiceException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
extern void GetOpenIdTokenResponseUnmarshaller_UnmarshallException_m0351B4C2F8D714594CD629530825E90FC0C436DD (void);
// 0x000000B4 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::get_Instance()
extern void GetOpenIdTokenResponseUnmarshaller_get_Instance_m540A5ED7796EC31058585B14548F7A8CC3B5B70D (void);
// 0x000000B5 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::.ctor()
extern void GetOpenIdTokenResponseUnmarshaller__ctor_mDF0CF79D5D7FE1A89AD67226EAFC85A3EE677ECB (void);
// 0x000000B6 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.GetOpenIdTokenResponseUnmarshaller::.cctor()
extern void GetOpenIdTokenResponseUnmarshaller__cctor_m63CDD4A0F86F5BDAB9776B68A6CFF2DCD570E5BA (void);
// 0x000000B7 Amazon.CognitoIdentity.Model.InternalErrorException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InternalErrorExceptionUnmarshaller_Unmarshall_m743AFFF635754E530B716DB83AC94E81BF315655 (void);
// 0x000000B8 Amazon.CognitoIdentity.Model.InternalErrorException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InternalErrorExceptionUnmarshaller_Unmarshall_m4F792AF803E554159355525CC39E46DBB02BA973 (void);
// 0x000000B9 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::get_Instance()
extern void InternalErrorExceptionUnmarshaller_get_Instance_m14E288EDD83A87D73EEF657A327930E469D43D5F (void);
// 0x000000BA System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::.ctor()
extern void InternalErrorExceptionUnmarshaller__ctor_mA810EA833EE95737FE80F5E4D7058BA0CAD81E99 (void);
// 0x000000BB System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InternalErrorExceptionUnmarshaller::.cctor()
extern void InternalErrorExceptionUnmarshaller__cctor_mA9DE5068914FD4428A358D576924A06D8140CFCC (void);
// 0x000000BC Amazon.CognitoIdentity.Model.InvalidIdentityPoolConfigurationException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidIdentityPoolConfigurationExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidIdentityPoolConfigurationExceptionUnmarshaller_Unmarshall_m5716480C1E286F5AD86D2074739D4A14017431BC (void);
// 0x000000BD Amazon.CognitoIdentity.Model.InvalidIdentityPoolConfigurationException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidIdentityPoolConfigurationExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidIdentityPoolConfigurationExceptionUnmarshaller_Unmarshall_mB7B6A21837732FAD9B2A2F7FF0B6ADB8ECB47316 (void);
// 0x000000BE Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidIdentityPoolConfigurationExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidIdentityPoolConfigurationExceptionUnmarshaller::get_Instance()
extern void InvalidIdentityPoolConfigurationExceptionUnmarshaller_get_Instance_m1CE86706CF6AE7053210FA1D788EEF7C79B9E0F5 (void);
// 0x000000BF System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidIdentityPoolConfigurationExceptionUnmarshaller::.ctor()
extern void InvalidIdentityPoolConfigurationExceptionUnmarshaller__ctor_m5A69BE09AF03CD11B0390EE1DFAC20E2AF033342 (void);
// 0x000000C0 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidIdentityPoolConfigurationExceptionUnmarshaller::.cctor()
extern void InvalidIdentityPoolConfigurationExceptionUnmarshaller__cctor_m5DE975C8C3CD0896A35FBD9D76BB2DB653A79FB2 (void);
// 0x000000C1 Amazon.CognitoIdentity.Model.InvalidParameterException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void InvalidParameterExceptionUnmarshaller_Unmarshall_mEA07A14F066FCF857714E5BB7C37290FCA30F5EC (void);
// 0x000000C2 Amazon.CognitoIdentity.Model.InvalidParameterException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void InvalidParameterExceptionUnmarshaller_Unmarshall_mF4B19F1A3774FF3B95A493D31B457AF1D3074CDB (void);
// 0x000000C3 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::get_Instance()
extern void InvalidParameterExceptionUnmarshaller_get_Instance_m30C8B44C25E90D0063AEC51D5023F0466A10779F (void);
// 0x000000C4 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::.ctor()
extern void InvalidParameterExceptionUnmarshaller__ctor_m8DCE786CE8520EFF056C10A2EEE6A72C641E0161 (void);
// 0x000000C5 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.InvalidParameterExceptionUnmarshaller::.cctor()
extern void InvalidParameterExceptionUnmarshaller__cctor_m22E44FB50C0F1F7AB64EB7B31F2ACAF1B96EA9FC (void);
// 0x000000C6 Amazon.CognitoIdentity.Model.LimitExceededException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void LimitExceededExceptionUnmarshaller_Unmarshall_m07D59FF84747A6152EDABF56B2E76F497EAB45A5 (void);
// 0x000000C7 Amazon.CognitoIdentity.Model.LimitExceededException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void LimitExceededExceptionUnmarshaller_Unmarshall_m009B2FC5CB2A74F3FCEB7C10EB1326420E4C44BE (void);
// 0x000000C8 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::get_Instance()
extern void LimitExceededExceptionUnmarshaller_get_Instance_mE7EEAC692D54E88B42CEF50931F6D1B90195BB74 (void);
// 0x000000C9 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::.ctor()
extern void LimitExceededExceptionUnmarshaller__ctor_m0FA4BE208BCBB83CB94AF6A981613E2E32E4836D (void);
// 0x000000CA System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.LimitExceededExceptionUnmarshaller::.cctor()
extern void LimitExceededExceptionUnmarshaller__cctor_m357DE94118A01511C7496A604E23C1908CAD0F7F (void);
// 0x000000CB Amazon.CognitoIdentity.Model.NotAuthorizedException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void NotAuthorizedExceptionUnmarshaller_Unmarshall_mD4D00141F270BB91A457250418490586B5962FB6 (void);
// 0x000000CC Amazon.CognitoIdentity.Model.NotAuthorizedException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void NotAuthorizedExceptionUnmarshaller_Unmarshall_m7950308DF15C3317D608410FABB15E7CC9E4253A (void);
// 0x000000CD Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::get_Instance()
extern void NotAuthorizedExceptionUnmarshaller_get_Instance_m1A0F951BCEF760537F26D449E53E70E40CF26D07 (void);
// 0x000000CE System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::.ctor()
extern void NotAuthorizedExceptionUnmarshaller__ctor_m4709507130966B4FF0FCC0A82B917FA5531727F8 (void);
// 0x000000CF System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.NotAuthorizedExceptionUnmarshaller::.cctor()
extern void NotAuthorizedExceptionUnmarshaller__cctor_m6EEB9B4F05EF514AF88CDEF89E886ED18B3D64F1 (void);
// 0x000000D0 Amazon.CognitoIdentity.Model.ResourceConflictException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceConflictExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ResourceConflictExceptionUnmarshaller_Unmarshall_m2D18B682699E8332B55E1346A594819B70167B9F (void);
// 0x000000D1 Amazon.CognitoIdentity.Model.ResourceConflictException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceConflictExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void ResourceConflictExceptionUnmarshaller_Unmarshall_m3D85A1B179D9F36D883D188BCFF5F06AFDE086E2 (void);
// 0x000000D2 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceConflictExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceConflictExceptionUnmarshaller::get_Instance()
extern void ResourceConflictExceptionUnmarshaller_get_Instance_m7BF0193F15B8104B6482E7A09BC9A7DB87D5D9A9 (void);
// 0x000000D3 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceConflictExceptionUnmarshaller::.ctor()
extern void ResourceConflictExceptionUnmarshaller__ctor_m633BA3A01CCC4C0AE049086299A873E9F1C0B316 (void);
// 0x000000D4 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceConflictExceptionUnmarshaller::.cctor()
extern void ResourceConflictExceptionUnmarshaller__cctor_mB3EF64B286C565767B923237BCD114FD1BE40210 (void);
// 0x000000D5 Amazon.CognitoIdentity.Model.ResourceNotFoundException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void ResourceNotFoundExceptionUnmarshaller_Unmarshall_m2DB04823EB5381BDED62406D71B4D6FADC2AFDC1 (void);
// 0x000000D6 Amazon.CognitoIdentity.Model.ResourceNotFoundException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void ResourceNotFoundExceptionUnmarshaller_Unmarshall_mB0AD10A8346117E9DBE169761B39807645CBA926 (void);
// 0x000000D7 Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::get_Instance()
extern void ResourceNotFoundExceptionUnmarshaller_get_Instance_m7FBA339176C6CEB04ECC7487C3E579DD26E50619 (void);
// 0x000000D8 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.ctor()
extern void ResourceNotFoundExceptionUnmarshaller__ctor_m4CA6D9BDF6F4613676BCF9CA394C47DFF74B4211 (void);
// 0x000000D9 System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.cctor()
extern void ResourceNotFoundExceptionUnmarshaller__cctor_mA8E38F55D391C016916F64D5867C8FCECCB98B5A (void);
// 0x000000DA Amazon.CognitoIdentity.Model.TooManyRequestsException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
extern void TooManyRequestsExceptionUnmarshaller_Unmarshall_m0833255F5110F5397D9387C5655BF6B3C4BA4EC3 (void);
// 0x000000DB Amazon.CognitoIdentity.Model.TooManyRequestsException Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
extern void TooManyRequestsExceptionUnmarshaller_Unmarshall_m5633FFB3948EA11DB493BFE5E1D715F61CF10415 (void);
// 0x000000DC Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::get_Instance()
extern void TooManyRequestsExceptionUnmarshaller_get_Instance_m98B53A0904D91EF7346081D3543365CA23AE8EE5 (void);
// 0x000000DD System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::.ctor()
extern void TooManyRequestsExceptionUnmarshaller__ctor_m3BFF10BB65CCC24FAC994B06E0E9990DE848B75D (void);
// 0x000000DE System.Void Amazon.CognitoIdentity.Model.Internal.MarshallTransformations.TooManyRequestsExceptionUnmarshaller::.cctor()
extern void TooManyRequestsExceptionUnmarshaller__cctor_mEB4399A62F052D896C352C1F3CF55F2C596195B1 (void);
static Il2CppMethodPointer s_methodPointers[222] = 
{
	CognitoAWSCredentials_get_IsIdentitySet_m13AAABF428E9EFA80FB1C4D14B21DD75304AC256,
	CognitoAWSCredentials_UpdateIdentity_mFE7B63CB907EDBAA4FC0E9160595476734551CE9,
	CognitoAWSCredentials_get_AccountId_mFD716F20C332024EB6451626EB289652EF60DE5C,
	CognitoAWSCredentials_set_AccountId_m4C60662CDCEE59A214A9E52D4F4A3B4DB7213871,
	CognitoAWSCredentials_get_IdentityPoolId_m05675F5728B4D5B22E9D1EFCC9C3E11ABE37CF01,
	CognitoAWSCredentials_set_IdentityPoolId_m383C1E4CCC30C06769DC56DA43A9C5EE4ADF8687,
	CognitoAWSCredentials_get_UnAuthRoleArn_m8AB13E52FCF387418A0112308CC4AB7C2BAC67CC,
	CognitoAWSCredentials_set_UnAuthRoleArn_m95C4EAE379B90E37891399167036A5C8C1E59764,
	CognitoAWSCredentials_get_AuthRoleArn_m7D015E01B7C59D324A69DBD1AE96FD42BDF65531,
	CognitoAWSCredentials_set_AuthRoleArn_m7C215B3F5A6EA88F9F6A3B1804AB8651B15254BA,
	CognitoAWSCredentials_get_Logins_m2A5C730D3ED5F54AEBFCD2895989C28A60B3B879,
	CognitoAWSCredentials_set_Logins_m4041C4F1E605BD69496E7469725DE644B3A6F336,
	CognitoAWSCredentials_GetIdentityId_m83D93345B15F70974571CF7D43F9AC4562FA3A1E,
	CognitoAWSCredentials_RefreshIdentity_m4C877CF3CD142BDFB483A7E84D6B10A39986127F,
	CognitoAWSCredentials_GetIdentityIdAsync_mD72497165B81784DC891F0E1E3AC9F58C2F9A386,
	CognitoAWSCredentials_RefreshIdentityAsync_m9DCA06AD0B2AF5AC8370CD392C58CAF39DEB0763,
	CognitoAWSCredentials_ShouldRetry_m2BF997DB3C4D74A947D4B9591AB1235AD14406BF,
	CognitoAWSCredentials__ctor_m412D51EE3B61F3EF3F413F2F7E5E4E0C93F78DDF,
	CognitoAWSCredentials__ctor_m67305AEA80A45CBFE9EFBA90D1DC65936D7EC7F5,
	CognitoAWSCredentials__ctor_mD44A7437213EB568986091AF15DAE88EA96900CC,
	CognitoAWSCredentials_GenerateNewCredentialsAsync_mDDA7ACD8FD9077EDA3EC620A8D57BBEB23CDEC0D,
	CognitoAWSCredentials_GetCredentialsForRoleAsync_m1B7058BA1EB066807F8A53B2F1F0510F5A68C44D,
	CognitoAWSCredentials_GetPoolCredentialsAsync_mC335FFDC27B3FF3EFDA900C96AE2C5352D7E57A8,
	CognitoAWSCredentials_GenerateNewCredentials_m8B202385BC854DB92E5D2E648FB2A0B12C4D3394,
	CognitoAWSCredentials_GetPoolCredentials_m5812F8508FFEF3F6C35ADD730F68D63410223872,
	CognitoAWSCredentials_GetCredentialsForRole_m9F996CCA90EAD82DB284D7312153B70A425FA1ED,
	CognitoAWSCredentials_GetStsCredentials_mEA186FA8335FC391215241FF6E93F1CF561711CE,
	CognitoAWSCredentials_GetOpenId_m035E6A5E5BCFD3C0B214A163D428C07BA23C73A8,
	CognitoAWSCredentials_GetCredentialsForIdentity_m975DB6BFE2263862731694747D258CA790E90F5E,
	CognitoAWSCredentials_GetCachedIdentityId_m2BF6C19C11D4EFC1159E8A44AA2DDB8216F26377,
	CognitoAWSCredentials_CacheIdentityId_m9C2F35E1D659CE2A0BB40934589FE9B1FD2F11EC,
	CognitoAWSCredentials_ClearIdentityCache_m71332DFF639434FD29043A453FE73C48B5702939,
	CognitoAWSCredentials_CacheCredentials_m6812B334969D39748D96FE34B2C26177B814FA97,
	CognitoAWSCredentials_GetCachedCredentials_m19B7E9463FD7D3A6AE9D9D6758CB9D9AD2FF5C59,
	CognitoAWSCredentials__cctor_mFF1116D5A081B1C1FFAC8562AFB61531E90A0B6D,
	IdentityChangedArgs_set_OldIdentityId_m57CD3C9B5A788DF970FDA9FA43D59619C91FFF10,
	IdentityChangedArgs_set_NewIdentityId_mA6F59055D45797660071EDF2A0D2941B58A68F74,
	IdentityChangedArgs__ctor_m6C1763DD1181DCC22E85A5D924ABC33B7598F6EE,
	IdentityState_get_IdentityId_mBB9A188EF26616327FCA450C6AB24C0631E4EE57,
	IdentityState_set_IdentityId_m096546285D286E36CEF28C0E44A97A11C455F88C,
	IdentityState_get_LoginProvider_mADBDD700DBBAA6B2B98D487235A679B619FBA85C,
	IdentityState_get_LoginToken_m5D27CC5E79D7281A37B19A672AE357F794924CF0,
	IdentityState_set_FromCache_mAEE2CCBEB1465FAC2717C785A8DA28A60E547003,
	IdentityState__ctor_m5E5C2BC7C0CF154A91519B599FBED9CB23A93C89,
	IdentityState_get_LoginSpecified_mEB21EBBE919D40D5F3CF506D4FB605D888D7CECB,
	U3CU3Ec__DisplayClass45_0__ctor_mDB94458F1A95E18AD1321A2368F8669ADD7B0B27,
	U3CU3Ec__DisplayClass45_0_U3CRefreshIdentityU3Eb__0_mA53BB7E839847B5E483DAA2F76A28E33E0986983,
	U3CGetIdentityIdAsyncU3Ed__47_MoveNext_m00FC6D0D3345FF3F6A2226D9022D682DF5A07000,
	U3CGetIdentityIdAsyncU3Ed__47_SetStateMachine_mB885C55C150DB3C1E0E333BC4025A7E48C8A9BEF,
	U3CRefreshIdentityAsyncU3Ed__48_MoveNext_mD20AC5AD8B0943A6039672E182E20562A05C2E71,
	U3CRefreshIdentityAsyncU3Ed__48_SetStateMachine_m6E25BA598D8051AAF0B1D1AD045CFA2B52FFABFA,
	U3CGenerateNewCredentialsAsyncU3Ed__57_MoveNext_m09E119787F68E4EF33D6E7DDED21929234EEDBD4,
	U3CGenerateNewCredentialsAsyncU3Ed__57_SetStateMachine_m1E2ED47C7984178238597DB655BB1C3895C04DB4,
	U3CGetCredentialsForRoleAsyncU3Ed__58_MoveNext_mF326983BD636AD5685B89C4C986AF7A5774C2228,
	U3CGetCredentialsForRoleAsyncU3Ed__58_SetStateMachine_m610D75E395FF843F8D80FA0178D947383E9B2C45,
	U3CGetPoolCredentialsAsyncU3Ed__59_MoveNext_mFBAF38779D4B18604BB6C77E366C300BF8B14090,
	U3CGetPoolCredentialsAsyncU3Ed__59_SetStateMachine_mF363EC4A748410BD927F3911BF889C621DD0ACCE,
	U3CU3Ec__DisplayClass64_0__ctor_m3B10DE1786CF12287D49CE6E4316EA0BF3DEE70E,
	U3CU3Ec__DisplayClass64_0_U3CGetStsCredentialsU3Eb__0_m9E43296999F10EC1C0FEC55052C7F5528F5CEFF4,
	U3CU3Ec__DisplayClass65_0__ctor_m7BF04F68251781E95DBF18E044D6741770075E2D,
	U3CU3Ec__DisplayClass65_0_U3CGetOpenIdU3Eb__0_m0B52C53EC65FED7F5772A9E9AA7D46E4CF18E49E,
	U3CU3Ec__DisplayClass66_0__ctor_m36493693A0B481B7FF200A72C716557085811CFA,
	U3CU3Ec__DisplayClass66_0_U3CGetCredentialsForIdentityU3Eb__0_m657514A1691AC6F4BF229630112208B0C9803A2C,
	AmazonCognitoIdentityConfig__ctor_m0FEE123B8409EAA2AA42299EF109871A586572C3,
	AmazonCognitoIdentityConfig_get_RegionEndpointServiceName_mB8DB4C70AB81992549C5FDEC6577D3CA738ADAD9,
	AmazonCognitoIdentityConfig_get_UserAgent_m203661DF0ED06A6C6E79A7AEBA53EFD855B3DD48,
	AmazonCognitoIdentityConfig__cctor_mD3EA82CCA6F20C3C32038919ED2AD9097F3BB643,
	AmazonCognitoIdentityException__ctor_mCF7A45F631393CF87B4FBAA63D2E844E6A053187,
	AmazonCognitoIdentityRequest__ctor_m2C3E447C170229E417C6E1BC1F6170F05CACAFCC,
	AmazonCognitoIdentityClient__ctor_m9BCA5582B54BF239F97DA2A5DE547A5AB51E459B,
	AmazonCognitoIdentityClient__ctor_m34C0E7D688FF6A81AB85C5481A0F68668825AF44,
	AmazonCognitoIdentityClient_CreateSigner_mAB445871A830781CED9D14EEBC624733B590C97E,
	AmazonCognitoIdentityClient_get_ServiceMetadata_m00007804B67F9CE2A68AD501E91A11C51C1595F2,
	AmazonCognitoIdentityClient_Dispose_m2C67C346C8E67B03F211FF3E542CAFA883057556,
	AmazonCognitoIdentityClient_GetCredentialsForIdentityAsync_mCC60AE7379B304FE2E025B4E7F27BDEC77128038,
	AmazonCognitoIdentityClient_GetIdAsync_mEEBD17F1A517E34AB030C47C0EA07FEF9F0BFB0C,
	AmazonCognitoIdentityClient_GetOpenIdTokenAsync_m184443804B1F640774B92DB9DBA6154EA4BBAE55,
	AmazonCognitoIdentityClient__cctor_m507C9E82C7458451561002A8162C73C89E42B92A,
	NULL,
	NULL,
	NULL,
	AmazonCognitoIdentityMetadata_get_ServiceId_m7CC609C4710210C573EB2660D5B94F970020CB32,
	AmazonCognitoIdentityMetadata_get_OperationNameMapping_mC5C36CC8C94D85E51A00E377D2CB8B0D273E6D38,
	AmazonCognitoIdentityMetadata__ctor_m26F0B9FA9FAFA44CDC7C6FF804310F0DF765BF50,
	Credentials_GetCredentials_mBD854DFEEF44537E2DCCAE42A0A802F8954C16F4,
	Credentials_get_AccessKeyId_mF35D74FCD9495A43F18EDA713EEEDE37D0022402,
	Credentials_set_AccessKeyId_m245151319F8B0AB4CA00BDA3BBBB075864824D2C,
	Credentials_get_Expiration_m8BBAD90F48BC87CE74D5B9A527A8A3B2B3172111,
	Credentials_set_Expiration_m657A76D33641624ED58310FAB44547AB95805A94,
	Credentials_get_SecretKey_m0A4DF49A8C334380ABAAFAA7B01347B16F8ABAF7,
	Credentials_set_SecretKey_mA87FB339155A48B2F0C9F0F45591486025300E0C,
	Credentials_get_SessionToken_mD13A0B97E4C124FC5E2F1B560F61332E2B685B9D,
	Credentials_set_SessionToken_mB603A57AA73A9826FD8839D33B273FF78BD7E03A,
	Credentials__ctor_mC7C7D06A2B6D68DE98AECA4CFBFD0F6B06482612,
	ExternalServiceException__ctor_mDD7AB86EED8917A11F1284F89A7785D0B52F3F5C,
	GetCredentialsForIdentityRequest_get_CustomRoleArn_m42E728435CAF3CBC023A04284D133A2977615D2E,
	GetCredentialsForIdentityRequest_IsSetCustomRoleArn_mF42421E2318AC63979224BDE6D20CC438A3586EE,
	GetCredentialsForIdentityRequest_get_IdentityId_m34E4189E29D902BF279C86CF219C2B279FBA2D43,
	GetCredentialsForIdentityRequest_set_IdentityId_m8E3F575B31AC70DCD7FAD4BDD055621F1EDC964A,
	GetCredentialsForIdentityRequest_IsSetIdentityId_m72BE99A2AD7579D42AB59459A5DAD53B610C993E,
	GetCredentialsForIdentityRequest_get_Logins_m49524DB1F8EF4848BB65CD8235BCD92A4EE49572,
	GetCredentialsForIdentityRequest_set_Logins_mD387BB2C8F2D906BFD3D15BC3254BF0578DA93F8,
	GetCredentialsForIdentityRequest_IsSetLogins_mF88C662ABB5D753F7A271AE8300E656B1B641E0B,
	GetCredentialsForIdentityRequest__ctor_m0BA8EEDCBDE36E56FFB6578AA38EC51E90803085,
	GetCredentialsForIdentityResponse_get_Credentials_m6BA24DA23B87F7BF1E5B6B65A5CB0162389CE34F,
	GetCredentialsForIdentityResponse_set_Credentials_m2CA50A2B8CC9C628B982B436E9FFEC16C6325C5B,
	GetCredentialsForIdentityResponse_get_IdentityId_m2B7ABEB049F9AAE01E51FDAB923A93988FA4F1D4,
	GetCredentialsForIdentityResponse_set_IdentityId_m0933FFCF0581103AF5C175885C736E7935C19EF0,
	GetCredentialsForIdentityResponse__ctor_m63E3A4EDFF00BF294824BD26D84BFA7EAA4954F5,
	GetIdRequest_get_AccountId_m529B4DFB41D3616D9E92A1CB987A180AE38FF37B,
	GetIdRequest_set_AccountId_mF6E6187C743DF40D481B572628D7773F4D8D323E,
	GetIdRequest_IsSetAccountId_m6555A37019439F92B7970AEC9EF550F9651CB25D,
	GetIdRequest_get_IdentityPoolId_m82C523C0DCD4F056A16E71A2A9A56ABF656F2D4F,
	GetIdRequest_set_IdentityPoolId_m1C9DD7AD76D42860578FBEAA202AA250E69FC4F6,
	GetIdRequest_IsSetIdentityPoolId_m629C1548D05029B391AE0B3CD193024CFEBBE974,
	GetIdRequest_get_Logins_mC65B200824F77A998EED06B48B63A77B95120C5A,
	GetIdRequest_set_Logins_m8DF2D07F4346490B0A5AA83D07E9C5B7A2FC3D61,
	GetIdRequest_IsSetLogins_mEE5AE23CB5E75DCD04F5F429AA2D56F5AACF31D5,
	GetIdRequest__ctor_m2F34B7FE9BF604881699BD49ABF1EC57B4C82DCC,
	GetIdResponse_get_IdentityId_m7F227CF5D59D7FAA0DDAD02D1CDA95758F02E4AC,
	GetIdResponse_set_IdentityId_m25DF124C60C57D9BDA16A7CA48B57F5118275FF2,
	GetIdResponse__ctor_mA308AE6CC674166EC0C5590E399C63148B96DADC,
	GetOpenIdTokenRequest_get_IdentityId_mB1A3E80500CC8F394A2B1F3FDA4567C6D0D30473,
	GetOpenIdTokenRequest_set_IdentityId_mC69B1083C1AE8C6319DF3C35B24C0A01D9C08A05,
	GetOpenIdTokenRequest_IsSetIdentityId_m441754571DF725C952B7B17F25C359C22B073117,
	GetOpenIdTokenRequest_get_Logins_m3C95985089A1380B5DEF5D47BF9B35A879204112,
	GetOpenIdTokenRequest_set_Logins_mC92315FE6D9216DC17927C72EC7458D0CD54D20A,
	GetOpenIdTokenRequest_IsSetLogins_mDE5DADC3FD71AF83881F6A8E8EC78155A1E43E87,
	GetOpenIdTokenRequest__ctor_m80A860EB7C1BA58E9D24CA9813904F0E7CF7C903,
	GetOpenIdTokenResponse_get_IdentityId_mF9742F5021914F5A8DC7B4BE2BB140DAD6697CC8,
	GetOpenIdTokenResponse_set_IdentityId_m3436360D9098D440F862C48CE6E5C9538F9CE421,
	GetOpenIdTokenResponse_get_Token_m11F1C27CB022CB7EFA4698B87B8A2FE4B9C8129A,
	GetOpenIdTokenResponse_set_Token_m1E9EDB656A7D151C1D5FAC7279AD8404FBA1561B,
	GetOpenIdTokenResponse__ctor_m47A0DAEE658AEA56AF8168A1B9D6B792E8AB1E2D,
	InternalErrorException__ctor_m137363AAA8AAF15BE5C8178D478122461A96D1E8,
	InvalidIdentityPoolConfigurationException__ctor_m47664B25947956DB17BDDBFE5F000D2C72653E0C,
	InvalidParameterException__ctor_mBC304B89C69ABDE32ECCF36C30614E5C2E39CA7C,
	LimitExceededException__ctor_mE91C7617C1BDF10B2791B5D366AE929820FF4D92,
	NotAuthorizedException__ctor_mB04D18389D774832BD29F44869FDF9940ADEB99F,
	ResourceConflictException__ctor_m98FC1278DA3E8EAB10AED6C5A5F436EA384A81F2,
	ResourceNotFoundException__ctor_m26098F5B0C9D890C9A2E84863A47D25D3F9756BB,
	TooManyRequestsException__ctor_mE0A1A5568FEDFBFC8BBB36B89BE07CBB28596D64,
	CredentialsUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_CognitoIdentity_Model_CredentialsU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_m704132388546F3108E3A0BAB3A89F027D4A06C1C,
	CredentialsUnmarshaller_Unmarshall_m423DB824F4A6DE2BD4AF8A2E0243D61054FBD1CE,
	CredentialsUnmarshaller_get_Instance_m76F819ACF84CC3D2A423C2F244764C538291C3B6,
	CredentialsUnmarshaller__ctor_m14012A355AF371EE30794DCA597FDD6C86E6A385,
	CredentialsUnmarshaller__cctor_m22B59869A752F6B17935D40D617F614BE5CAEF40,
	ExternalServiceExceptionUnmarshaller_Unmarshall_m839AF8CBB150F65E824138C56EC0125FD4A737C1,
	ExternalServiceExceptionUnmarshaller_Unmarshall_mC51A7AFC2D017D3047EFE09638E4A92C41111E2D,
	ExternalServiceExceptionUnmarshaller_get_Instance_mD2CD0B7BEFB5328208BF53FC5D4FC3263762E914,
	ExternalServiceExceptionUnmarshaller__ctor_m98721D6A7F9B7D052236F6D69358DC26884F27AA,
	ExternalServiceExceptionUnmarshaller__cctor_m8125A445B3C75DCCF4561DB5F60B62BCDF12F848,
	GetCredentialsForIdentityRequestMarshaller_Marshall_m83EFD1CE72779522C69B1D11DADD211C2F1495A9,
	GetCredentialsForIdentityRequestMarshaller_Marshall_m244CD4351D03FD70D4D99CAA610E88EF70824A92,
	GetCredentialsForIdentityRequestMarshaller_get_Instance_m5260E183EC0FAC9A0D32DF53696DA78D053E65B9,
	GetCredentialsForIdentityRequestMarshaller__ctor_mBB32DB33C46EBEEA5B7F7A5F8D3B1EC476C539C6,
	GetCredentialsForIdentityRequestMarshaller__cctor_mDB57A42C4DF0C594180638FAA7E37B69812974D7,
	GetCredentialsForIdentityResponseUnmarshaller_Unmarshall_mEC9F9C5A545017A80D7D9673114EE03F0CB964C8,
	GetCredentialsForIdentityResponseUnmarshaller_UnmarshallException_mE54A1CE4BC8A5AE58F8F9823CFA2BFC7A55B646D,
	GetCredentialsForIdentityResponseUnmarshaller_get_Instance_mB25E2CF51A33A0D5776B4F5D9CEBAE373C772027,
	GetCredentialsForIdentityResponseUnmarshaller__ctor_m3C998CED24541F0D170D0F57EFAA7067718AB0B2,
	GetCredentialsForIdentityResponseUnmarshaller__cctor_mC88E011FDED6A8DA84AF353C162026910E287681,
	GetIdRequestMarshaller_Marshall_m692F69EA825E7FFD788F7370C574419B22B05A0A,
	GetIdRequestMarshaller_Marshall_m2C89D6D019768452814DD5139E8D27C772DCEB21,
	GetIdRequestMarshaller_get_Instance_mD4A6F4DC4DC2C498B2B8B7446DCFB5EB45585741,
	GetIdRequestMarshaller__ctor_m3415657A14A282AC515BC7904FB89ADB641CCF7B,
	GetIdRequestMarshaller__cctor_m2372B1A39500983D3327397F71D68675699A9D8F,
	GetIdResponseUnmarshaller_Unmarshall_mD2360DDA9C7B58FB961306A4BC02B3D4166CF1E8,
	GetIdResponseUnmarshaller_UnmarshallException_mED9E65300BBA68B8810396C27BEE543E8C94F029,
	GetIdResponseUnmarshaller_get_Instance_mBD118C8FF883437C0C22CB43F82DE1A1D45C2565,
	GetIdResponseUnmarshaller__ctor_m48B964008DAB6834FCC6BD92C52A2FE85C1185D3,
	GetIdResponseUnmarshaller__cctor_mA4B15853DB95BDC0B24F1326896D9CA0BCD2A3F7,
	GetOpenIdTokenRequestMarshaller_Marshall_m43AF2A22F00592ACF283BAC31BF5F20DB9C7DE1E,
	GetOpenIdTokenRequestMarshaller_Marshall_mF4D85DBA858A3235ED043D417634F97EC9678B69,
	GetOpenIdTokenRequestMarshaller_get_Instance_m046B1C4DA6B6716848E749E93E0EE757D2884FC9,
	GetOpenIdTokenRequestMarshaller__ctor_m987E195E5C7768D5AD27D295E2C7FD76BD7125DA,
	GetOpenIdTokenRequestMarshaller__cctor_mCEED94A917308503E6CBB559C94EF265E7CE5295,
	GetOpenIdTokenResponseUnmarshaller_Unmarshall_mA3838F84D5E24384D0E87236429C004FAEF4E4B1,
	GetOpenIdTokenResponseUnmarshaller_UnmarshallException_m0351B4C2F8D714594CD629530825E90FC0C436DD,
	GetOpenIdTokenResponseUnmarshaller_get_Instance_m540A5ED7796EC31058585B14548F7A8CC3B5B70D,
	GetOpenIdTokenResponseUnmarshaller__ctor_mDF0CF79D5D7FE1A89AD67226EAFC85A3EE677ECB,
	GetOpenIdTokenResponseUnmarshaller__cctor_m63CDD4A0F86F5BDAB9776B68A6CFF2DCD570E5BA,
	InternalErrorExceptionUnmarshaller_Unmarshall_m743AFFF635754E530B716DB83AC94E81BF315655,
	InternalErrorExceptionUnmarshaller_Unmarshall_m4F792AF803E554159355525CC39E46DBB02BA973,
	InternalErrorExceptionUnmarshaller_get_Instance_m14E288EDD83A87D73EEF657A327930E469D43D5F,
	InternalErrorExceptionUnmarshaller__ctor_mA810EA833EE95737FE80F5E4D7058BA0CAD81E99,
	InternalErrorExceptionUnmarshaller__cctor_mA9DE5068914FD4428A358D576924A06D8140CFCC,
	InvalidIdentityPoolConfigurationExceptionUnmarshaller_Unmarshall_m5716480C1E286F5AD86D2074739D4A14017431BC,
	InvalidIdentityPoolConfigurationExceptionUnmarshaller_Unmarshall_mB7B6A21837732FAD9B2A2F7FF0B6ADB8ECB47316,
	InvalidIdentityPoolConfigurationExceptionUnmarshaller_get_Instance_m1CE86706CF6AE7053210FA1D788EEF7C79B9E0F5,
	InvalidIdentityPoolConfigurationExceptionUnmarshaller__ctor_m5A69BE09AF03CD11B0390EE1DFAC20E2AF033342,
	InvalidIdentityPoolConfigurationExceptionUnmarshaller__cctor_m5DE975C8C3CD0896A35FBD9D76BB2DB653A79FB2,
	InvalidParameterExceptionUnmarshaller_Unmarshall_mEA07A14F066FCF857714E5BB7C37290FCA30F5EC,
	InvalidParameterExceptionUnmarshaller_Unmarshall_mF4B19F1A3774FF3B95A493D31B457AF1D3074CDB,
	InvalidParameterExceptionUnmarshaller_get_Instance_m30C8B44C25E90D0063AEC51D5023F0466A10779F,
	InvalidParameterExceptionUnmarshaller__ctor_m8DCE786CE8520EFF056C10A2EEE6A72C641E0161,
	InvalidParameterExceptionUnmarshaller__cctor_m22E44FB50C0F1F7AB64EB7B31F2ACAF1B96EA9FC,
	LimitExceededExceptionUnmarshaller_Unmarshall_m07D59FF84747A6152EDABF56B2E76F497EAB45A5,
	LimitExceededExceptionUnmarshaller_Unmarshall_m009B2FC5CB2A74F3FCEB7C10EB1326420E4C44BE,
	LimitExceededExceptionUnmarshaller_get_Instance_mE7EEAC692D54E88B42CEF50931F6D1B90195BB74,
	LimitExceededExceptionUnmarshaller__ctor_m0FA4BE208BCBB83CB94AF6A981613E2E32E4836D,
	LimitExceededExceptionUnmarshaller__cctor_m357DE94118A01511C7496A604E23C1908CAD0F7F,
	NotAuthorizedExceptionUnmarshaller_Unmarshall_mD4D00141F270BB91A457250418490586B5962FB6,
	NotAuthorizedExceptionUnmarshaller_Unmarshall_m7950308DF15C3317D608410FABB15E7CC9E4253A,
	NotAuthorizedExceptionUnmarshaller_get_Instance_m1A0F951BCEF760537F26D449E53E70E40CF26D07,
	NotAuthorizedExceptionUnmarshaller__ctor_m4709507130966B4FF0FCC0A82B917FA5531727F8,
	NotAuthorizedExceptionUnmarshaller__cctor_m6EEB9B4F05EF514AF88CDEF89E886ED18B3D64F1,
	ResourceConflictExceptionUnmarshaller_Unmarshall_m2D18B682699E8332B55E1346A594819B70167B9F,
	ResourceConflictExceptionUnmarshaller_Unmarshall_m3D85A1B179D9F36D883D188BCFF5F06AFDE086E2,
	ResourceConflictExceptionUnmarshaller_get_Instance_m7BF0193F15B8104B6482E7A09BC9A7DB87D5D9A9,
	ResourceConflictExceptionUnmarshaller__ctor_m633BA3A01CCC4C0AE049086299A873E9F1C0B316,
	ResourceConflictExceptionUnmarshaller__cctor_mB3EF64B286C565767B923237BCD114FD1BE40210,
	ResourceNotFoundExceptionUnmarshaller_Unmarshall_m2DB04823EB5381BDED62406D71B4D6FADC2AFDC1,
	ResourceNotFoundExceptionUnmarshaller_Unmarshall_mB0AD10A8346117E9DBE169761B39807645CBA926,
	ResourceNotFoundExceptionUnmarshaller_get_Instance_m7FBA339176C6CEB04ECC7487C3E579DD26E50619,
	ResourceNotFoundExceptionUnmarshaller__ctor_m4CA6D9BDF6F4613676BCF9CA394C47DFF74B4211,
	ResourceNotFoundExceptionUnmarshaller__cctor_mA8E38F55D391C016916F64D5867C8FCECCB98B5A,
	TooManyRequestsExceptionUnmarshaller_Unmarshall_m0833255F5110F5397D9387C5655BF6B3C4BA4EC3,
	TooManyRequestsExceptionUnmarshaller_Unmarshall_m5633FFB3948EA11DB493BFE5E1D715F61CF10415,
	TooManyRequestsExceptionUnmarshaller_get_Instance_m98B53A0904D91EF7346081D3543365CA23AE8EE5,
	TooManyRequestsExceptionUnmarshaller__ctor_m3BFF10BB65CCC24FAC994B06E0E9990DE848B75D,
	TooManyRequestsExceptionUnmarshaller__cctor_mEB4399A62F052D896C352C1F3CF55F2C596195B1,
};
extern void U3CGetIdentityIdAsyncU3Ed__47_MoveNext_m00FC6D0D3345FF3F6A2226D9022D682DF5A07000_AdjustorThunk (void);
extern void U3CGetIdentityIdAsyncU3Ed__47_SetStateMachine_mB885C55C150DB3C1E0E333BC4025A7E48C8A9BEF_AdjustorThunk (void);
extern void U3CRefreshIdentityAsyncU3Ed__48_MoveNext_mD20AC5AD8B0943A6039672E182E20562A05C2E71_AdjustorThunk (void);
extern void U3CRefreshIdentityAsyncU3Ed__48_SetStateMachine_m6E25BA598D8051AAF0B1D1AD045CFA2B52FFABFA_AdjustorThunk (void);
extern void U3CGenerateNewCredentialsAsyncU3Ed__57_MoveNext_m09E119787F68E4EF33D6E7DDED21929234EEDBD4_AdjustorThunk (void);
extern void U3CGenerateNewCredentialsAsyncU3Ed__57_SetStateMachine_m1E2ED47C7984178238597DB655BB1C3895C04DB4_AdjustorThunk (void);
extern void U3CGetCredentialsForRoleAsyncU3Ed__58_MoveNext_mF326983BD636AD5685B89C4C986AF7A5774C2228_AdjustorThunk (void);
extern void U3CGetCredentialsForRoleAsyncU3Ed__58_SetStateMachine_m610D75E395FF843F8D80FA0178D947383E9B2C45_AdjustorThunk (void);
extern void U3CGetPoolCredentialsAsyncU3Ed__59_MoveNext_mFBAF38779D4B18604BB6C77E366C300BF8B14090_AdjustorThunk (void);
extern void U3CGetPoolCredentialsAsyncU3Ed__59_SetStateMachine_mF363EC4A748410BD927F3911BF889C621DD0ACCE_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[10] = 
{
	{ 0x06000030, U3CGetIdentityIdAsyncU3Ed__47_MoveNext_m00FC6D0D3345FF3F6A2226D9022D682DF5A07000_AdjustorThunk },
	{ 0x06000031, U3CGetIdentityIdAsyncU3Ed__47_SetStateMachine_mB885C55C150DB3C1E0E333BC4025A7E48C8A9BEF_AdjustorThunk },
	{ 0x06000032, U3CRefreshIdentityAsyncU3Ed__48_MoveNext_mD20AC5AD8B0943A6039672E182E20562A05C2E71_AdjustorThunk },
	{ 0x06000033, U3CRefreshIdentityAsyncU3Ed__48_SetStateMachine_m6E25BA598D8051AAF0B1D1AD045CFA2B52FFABFA_AdjustorThunk },
	{ 0x06000034, U3CGenerateNewCredentialsAsyncU3Ed__57_MoveNext_m09E119787F68E4EF33D6E7DDED21929234EEDBD4_AdjustorThunk },
	{ 0x06000035, U3CGenerateNewCredentialsAsyncU3Ed__57_SetStateMachine_m1E2ED47C7984178238597DB655BB1C3895C04DB4_AdjustorThunk },
	{ 0x06000036, U3CGetCredentialsForRoleAsyncU3Ed__58_MoveNext_mF326983BD636AD5685B89C4C986AF7A5774C2228_AdjustorThunk },
	{ 0x06000037, U3CGetCredentialsForRoleAsyncU3Ed__58_SetStateMachine_m610D75E395FF843F8D80FA0178D947383E9B2C45_AdjustorThunk },
	{ 0x06000038, U3CGetPoolCredentialsAsyncU3Ed__59_MoveNext_mFBAF38779D4B18604BB6C77E366C300BF8B14090_AdjustorThunk },
	{ 0x06000039, U3CGetPoolCredentialsAsyncU3Ed__59_SetStateMachine_mF363EC4A748410BD927F3911BF889C621DD0ACCE_AdjustorThunk },
};
static const int32_t s_InvokerIndices[222] = 
{
	4197,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	4169,
	3450,
	2555,
	4169,
	2555,
	4169,
	2923,
	1920,
	401,
	249,
	4169,
	2558,
	4169,
	4169,
	4169,
	2558,
	2558,
	2558,
	2558,
	4169,
	3450,
	4257,
	3450,
	4169,
	6598,
	3450,
	3450,
	1920,
	4169,
	3450,
	4169,
	4169,
	3476,
	1926,
	4197,
	4257,
	4169,
	4257,
	3450,
	4257,
	3450,
	4257,
	3450,
	4257,
	3450,
	4257,
	3450,
	4257,
	4169,
	4257,
	4169,
	4257,
	4169,
	4257,
	4169,
	4169,
	6598,
	236,
	4257,
	1920,
	1920,
	4169,
	4169,
	3476,
	1415,
	1415,
	1415,
	6598,
	1415,
	1415,
	1415,
	4169,
	4169,
	4257,
	4169,
	4169,
	3450,
	4119,
	3402,
	4169,
	3450,
	4169,
	3450,
	4257,
	236,
	4169,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4257,
	4169,
	3450,
	4169,
	3450,
	4257,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4257,
	4169,
	3450,
	4257,
	4169,
	3450,
	4197,
	4169,
	3450,
	4197,
	4257,
	4169,
	3450,
	4169,
	3450,
	4257,
	236,
	236,
	236,
	236,
	236,
	236,
	236,
	236,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	2558,
	6579,
	4257,
	6598,
	2558,
	976,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
	2558,
	1417,
	6579,
	4257,
	6598,
};
extern const CustomAttributesCacheGenerator g_AWSSDK_CognitoIdentity_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AWSSDK_CognitoIdentity_CodeGenModule;
const Il2CppCodeGenModule g_AWSSDK_CognitoIdentity_CodeGenModule = 
{
	"AWSSDK.CognitoIdentity.dll",
	222,
	s_methodPointers,
	10,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AWSSDK_CognitoIdentity_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
