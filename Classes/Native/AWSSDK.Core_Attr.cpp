﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;
// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CRunSyncU3Eb__0U3Ed_tD5715C887076CAC33A7EE1570F29B477D8DE2B9C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.ThreadStaticAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
static void AWSSDK_Core_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[0];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x31\x2E\x30\x2E\x30"), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[1];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, true, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[2];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x33\x2E\x31\x30\x37\x2E\x33\x38"), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[3];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[4];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[5];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\x32\x30\x30\x39\x2D\x32\x30\x31\x35\x20\x41\x6D\x61\x7A\x6F\x6E\x2E\x63\x6F\x6D\x2C\x20\x49\x6E\x63\x2E\x20\x6F\x72\x20\x69\x74\x73\x20\x61\x66\x66\x69\x6C\x69\x61\x74\x65\x73\x2E\x20\x41\x6C\x6C\x20\x52\x69\x67\x68\x74\x73\x20\x52\x65\x73\x65\x72\x76\x65\x64\x2E"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x61\x7A\x6F\x6E\x2E\x63\x6F\x6D\x2C\x20\x49\x6E\x63"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[7];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x61\x7A\x6F\x6E\x20\x57\x65\x62\x20\x53\x65\x72\x76\x69\x63\x65\x73\x20\x53\x44\x4B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[8];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[9];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x6D\x61\x7A\x6F\x6E\x20\x57\x65\x62\x20\x53\x65\x72\x76\x69\x63\x65\x73\x20\x53\x44\x4B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54\x20\x28\x4E\x65\x74\x53\x74\x61\x6E\x64\x61\x72\x64\x20\x32\x2E\x30\x29\x2D\x20\x43\x6F\x72\x65\x20\x52\x75\x6E\x74\x69\x6D\x65"), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[10];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x57\x53\x53\x44\x4B\x2E\x43\x6F\x72\x65"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[11];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[12];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[13];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[14];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[15];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void JsonData_t60FCF27B7D55DE30481BF5C7FC22D05B34D9684D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CU3Ec_t24000A327F8AAC0CF82858F50F2AEB687554CB8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_t0105C7BFD5AD526EA908B0F9EB6C2D7EDE200454_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 128LL, NULL);
	}
}
static void JsonPropertyAttribute_t0105C7BFD5AD526EA908B0F9EB6C2D7EDE200454_CustomAttributesCacheGenerator_U3CRequiredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_t0105C7BFD5AD526EA908B0F9EB6C2D7EDE200454_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_Required_m13B37FCCC7DA7AA77F335DEFAFBED5BAAA93C670(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_CustomAttributesCacheGenerator_JsonWriter_Write_mC5058DAFD1A17F59EF9C406C354D29808D3BEA4B(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_U3CClockOffsetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_U3CHttpClientFactoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_AWSConfigs_set_ClockOffset_mA16DF92F5F2A10BD24A5D52C01D81ED888A868FB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_AWSConfigs_get_HttpClientFactory_m78229ECA292AF41D8A1D1DC094725778383DF561(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7____ClockOffset_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x20\x69\x6E\x20\x66\x61\x76\x6F\x72\x20\x6F\x66\x20\x49\x43\x6C\x69\x65\x6E\x74\x43\x6F\x6E\x66\x69\x67\x2E\x43\x6C\x6F\x63\x6B\x4F\x66\x66\x73\x65\x74"), NULL);
	}
}
static void LoggingOptions_t0D2EF8CFA44B04A3F804643F3E0B7C916D2725EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_U3CSystemNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_U3CDisplayNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_get_SystemName_mCA2FF58E84D00596AC3250F797AD2AC3B783FF9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_set_SystemName_m98B17B4109A3DFB6C723FC34D060297FA1ECBBB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_get_DisplayName_m9F7BF40A023CC420766759C7643189309742A812(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_set_DisplayName_m7DDC78786C489CAC929012EF3FA130015572EF42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_U3CHostnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_U3CAuthRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_U3CSignatureVersionOverrideU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_get_Hostname_m68CD8A74E6E7DB12FC54D991176DC12D6A0C4925(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_set_Hostname_m25D4F046B53E36CA01DE38ED67B854336395EB0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_get_AuthRegion_m342C6FEF878A06504E6822261A6752BF328879AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_set_AuthRegion_m5ABD1D4B3686C2C959AB1FCFF0120D66CBDA2554(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_set_SignatureVersionOverride_m67F109FA152FA50029D59E3019A642326EE6C3A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointProviderV2_t65AAD8ABE783A6575B71A33B81359628E132A1CD_CustomAttributesCacheGenerator_U3CProxyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointProviderV2_t65AAD8ABE783A6575B71A33B81359628E132A1CD_CustomAttributesCacheGenerator_RegionEndpointProviderV2_get_Proxy_mF94D500F8D838A0A41E557F8F78E85E1881CBBE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_U3CSystemNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_U3CDisplayNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_get_SystemName_mA927AC85ACE3E746C6BCEE3F85AFA08491345AB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_set_SystemName_mE32DA2C28922213E460BF7B162452D653DA98E6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_get_DisplayName_m8CBFE99744D4B800EE260CE6E770C884892ED534(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_set_DisplayName_mDDFA9C6B58D6D1331F5739BD546588A6B8E18B16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_U3CRegionNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_U3CDisplayNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_get_RegionName_mB7CEB1A08A15E5A094FB058980751AD56B028974(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_set_RegionName_mBC114B5247B46D3F873AA93606B19883D91C6EFA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_get_DisplayName_mD3913EEEF925BD31703A43466C887E014E98A673(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_set_DisplayName_m81A67F18FA5E4D9529CFD7D6773142BCC5F9F55A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_U3CUsernameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_U3CPasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_ProxyConfig_get_Username_m81955ADE534F7677180E2939A6C3D1AF0A2228FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_ProxyConfig_get_Password_mD09CFD5FB9141132944E25757C50D5C421C20E51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogResponsesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogResponsesSizeLimitU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogMetricsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogMetricsFormatU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogMetricsCustomFormatterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogResponses_m9F1FA413F3A9AF41C64AC4B219798FA570D18725(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_set_LogResponses_m0E16D7CDB69C7EEE2994914B6D34B24A62C898F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogResponsesSizeLimit_mAEF0745D43816A42B5D80B1DE17380FBB14EAF16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_set_LogResponsesSizeLimit_m7E62A8B361B8EB37305DFFAF37FF0AF6F9D3D0B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogMetrics_m47AF794FF34CA33B117B030A865695FD0FB42000(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_set_LogMetrics_mE4CFC322110C8D56EA52CECB88D69FA8BD560FE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogMetricsFormat_m5C2206F419BF88D2F401258F965407456E4DD30E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogMetricsCustomFormatter_mEFF5F77802440902877DF50BC11721A69FFB2E24(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMHostU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMPortU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMClientIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMHost_mCD6A4223BC46188E253784DD6811D36F1022314B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMPort_mB95D7D19420DAAC48311A4EC703E1A6F34E0AC44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMClientId_m6994559572B87F47F0B6E55E46367E9F2490E8F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMEnabled_m04F6AA3E68C62699639115DF7BC9FEA121AAB165(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSSDKUtils_t6F62F996003A26F02152C22ED49BE59B0482F653_CustomAttributesCacheGenerator_AWSSDKUtils_t6F62F996003A26F02152C22ED49BE59B0482F653____CorrectedUtcNow_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x64\x6F\x65\x73\x20\x6E\x6F\x74\x20\x61\x63\x63\x6F\x75\x6E\x74\x20\x66\x6F\x72\x20\x65\x6E\x64\x70\x6F\x69\x6E\x74\x20\x73\x70\x65\x63\x69\x66\x69\x63\x20\x63\x6C\x6F\x63\x6B\x20\x73\x6B\x65\x77\x2E\x20\x20\x55\x73\x65\x20\x43\x6F\x72\x72\x65\x63\x74\x43\x6C\x6F\x63\x6B\x53\x6B\x65\x77\x2E\x47\x65\x74\x43\x6F\x72\x72\x65\x63\x74\x65\x64\x55\x74\x63\x4E\x6F\x77\x46\x6F\x72\x45\x6E\x64\x70\x6F\x69\x6E\x74\x28\x29\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void U3CU3Ec_t4CD6B238955B57132508DD6AA550F8DE77545B3F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_tCAB3D15743476A4A196D2E161BBEB5D4C22B25F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass54_0_1_tB77BECAE61ADC193F9140D48E4A7F568A44C093D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass54_1_1_t9D9DA3D17247DEEE2609C174F772C4671D2E7A65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass95_0_tD97437A21AA3DF88028534BC3399B3173A68C1AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass95_1_t8A8477D37091446A96A28EEF05D08ECEE88A68A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CryptoUtil_tC47137C4AADE629CEC6E7078B8A08B8303B4801C_CustomAttributesCacheGenerator__hashAlgorithm(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * tmp = (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A(tmp, NULL);
	}
}
static void EC2InstanceMetadata_t92C7B038B0D3F8AD83A8A95542C4CA590002F0D6_CustomAttributesCacheGenerator_U3CProxyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EC2InstanceMetadata_t92C7B038B0D3F8AD83A8A95542C4CA590002F0D6_CustomAttributesCacheGenerator_EC2InstanceMetadata_get_Proxy_m44E9E5E80195713A75A93A64D56080954ABC18C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CCSMConfigU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CLoggingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CProxyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CEndpointDefinitionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CProfileNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CProfilesLocationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CUseSdkCacheU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CCorrectForClockSkewU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CUseAlternateUserAgentHeaderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_CSMConfig_mE24357A51A72DC9C705EA286180502BD8715A423(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_CSMConfig_m98B7688DFE7FAD711BFA1555380BB9B3BAE1F278(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_Logging_m4625DDD282B9B05DA95A92741359BE067FD2D6E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_Logging_m5E9E2542873931F0E929611F21B9A99BDD56FD9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_Proxy_m9E0480BE4E106446353942289AF7D2BB178B7162(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_Proxy_m1BE8FC28EE2B8C93A2E58C612C8B8212C8B5468F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_EndpointDefinition_m3204E79A2D482EC5614416625F1BF0D9B4AF1DD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_EndpointDefinition_m6535665BFD93C547251D39119AED42E63366337B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_Region_m284EF44B24F8EBB9672322B91B4134FFCFDF825F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_Region_m453ED10087DC4B2A11408936814FB6142970CFC0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_ProfileName_mF622E8B2F448542B28DC0EEDFC59244BAB988205(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_ProfileName_m22D6F68E19AFBE2F41CEE81A06CB64C0775614E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_ProfilesLocation_mF3FCB10B8A02626FFEBAE7F4A3DE96637840BEEE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_ProfilesLocation_m100049E9E25819ED7EB08FE271E9E98639557F50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_UseSdkCache_m66A052C98A721A3B7F2CF81F4A812F321A1E0125(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_CorrectForClockSkew_m7AFB4F0C317A4C4FD94D3AC49C58E5131099CF49(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_CorrectForClockSkew_mFCBFE1909CD693B83D6DFCE3DC3018A0FB89C19B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_UseAlternateUserAgentHeader_m7F2DA7D47D1FB6C0BD7A6CF6F022500B0292FEDE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t813B290E59288F94D5BA9A78011B02752217BA41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnvironmentVariableSource_t04DF820E28329CFAB3DFFF134EC4E1AB9804A4B7_CustomAttributesCacheGenerator_U3CEnvironmentVariableRetrieverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnvironmentVariableSource_t04DF820E28329CFAB3DFFF134EC4E1AB9804A4B7_CustomAttributesCacheGenerator_EnvironmentVariableSource_get_EnvironmentVariableRetriever_mECF810C4761E79E35D61A8B5932F374F49CB788E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SettingsManager_t384AD01E5513FEE12B314542F8C666B286BF8E70_CustomAttributesCacheGenerator_U3CSettingsTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SettingsManager_t384AD01E5513FEE12B314542F8C666B286BF8E70_CustomAttributesCacheGenerator_SettingsManager_get_SettingsType_m30F81BBD2789713AC9887E5025D938E982B2A0E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SettingsManager_t384AD01E5513FEE12B314542F8C666B286BF8E70_CustomAttributesCacheGenerator_SettingsManager_set_SettingsType_m5960C66BF818EC32834889E87F11AAA9CEE406D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t1301D624244D6D06A8DD4BD6F964609934C97DB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CEndpointDiscoveryResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CRuntimePipelineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CCredentialsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CConfigU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CServiceMetadataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CSignerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_EndpointDiscoveryResolver_mD8ED6AD49FEAB0E7ADFE0D6F14B1C3624DE8EB21(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_RuntimePipeline_m666BA08F0997534D6F4D81431D5F34CB684050D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_RuntimePipeline_mE11F53789AF484FEFB2A831E18E1FD7CD9F205A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_Credentials_m080C193E757F0C6C3C6FACA2F5C7A4EE667CB1C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_Credentials_mB189B6BAC7FE34A3FD8B134C703CFB81E2F10FF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_Config_m7896DAF638A60C7D0CD95F2C92F7C95429A00FCF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_Config_mF980BE678237045A0E7CE73D46170D67D8C1A38F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_ServiceMetadata_mF868195EEB804B404AAE5B09B3B106115487C603(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_Signer_m67573336045A226123F21A0BD07C7248EFF6A481(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_Signer_m8D3241409ABFDDCF5465DD065A31DB72C162DFFB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t53C9F32CD66CD3E13B876A8F1CA21B30638D2261_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryableDetails_t30365FED75CE6F46B7D556D855885998D27EFBCF_CustomAttributesCacheGenerator_U3CThrottlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryableDetails_t30365FED75CE6F46B7D556D855885998D27EFBCF_CustomAttributesCacheGenerator_RetryableDetails_get_Throttling_m7170652F32CB321788E87CF4ECE2C480575A1501(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_U3CLastKnownLocationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_U3CResponseBodyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_get_LastKnownLocation_m125EB29655724CC65286914D205C57131C22B98B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_set_LastKnownLocation_m4EE878DAF0AE3328B94919195FEAEC0DB77BBB5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_get_ResponseBody_m9D8553A581FAF5F4CCDF36335644A495DCAE109C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_set_ResponseBody_m40F84E8996C657ADA6F77DAFB15E1E6F9F901348(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_StreamUploadProgressCallback_mE0AF7FC0693F8A85D1FC3869A412CC5A10CAA177(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_UseSigV4_m412C582CD42A6D7E416C3272CDD244B1BEF79AAF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSRegion_t2D84AE52D83CD4318C4E8DB5F3C22CD6314D91EB_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSRegion_t2D84AE52D83CD4318C4E8DB5F3C22CD6314D91EB_CustomAttributesCacheGenerator_AWSRegion_get_Region_m8917D9F0C32A90ED6803DD6035A7044346D3D3A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSRegion_t2D84AE52D83CD4318C4E8DB5F3C22CD6314D91EB_CustomAttributesCacheGenerator_AWSRegion_set_Region_mB70ECDCFDD779661E7DE6617C4E81A3A8BE3EFB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_U3CAllGeneratorsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_U3CNonMetadataGeneratorsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_get_AllGenerators_m91FD3D6C134BB04D7FBB03849689A828C3AC0C44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_set_AllGenerators_mD213A68B42DCC5793C7423C4B5C5564E78CAEF46(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_get_NonMetadataGenerators_mF1FA51D6894FF14E2917C483BDA3FE7855C42CE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_set_NonMetadataGenerators_m515F25404FC75B4E202732360614A438D7EE04B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t42EF3E5DD1BDB7B6B560BAB6BC7A3CEEF25C0831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_U3CFastFailRequestsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_U3CMaxConnectionsPerServerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_U3CHttpClientFactoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_get_FastFailRequests_m2DB2B6D28509361EB03619C7EC8877029EB33316(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_get_MaxConnectionsPerServer_m62F57F07092DFB2641B38B93452E6CF8837C40A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_get_HttpClientFactory_mDDFE40B1D7108CEF000727555013D413D998F732(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099____ReadEntireResponse_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x64\x6F\x65\x73\x20\x6E\x6F\x74\x20\x65\x66\x66\x65\x63\x74\x20\x72\x65\x73\x70\x6F\x6E\x73\x65\x20\x70\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x20\x61\x6E\x64\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x2E\x54\x6F\x20\x65\x6E\x61\x62\x6C\x65\x20\x72\x65\x73\x70\x6F\x6E\x73\x65\x20\x6C\x6F\x67\x67\x69\x6E\x67\x2C\x20\x74\x68\x65\x20\x43\x6C\x69\x65\x6E\x74\x43\x6F\x6E\x66\x69\x67\x2E\x4C\x6F\x67\x52\x65\x73\x70\x6F\x6E\x73\x65\x20\x61\x6E\x64\x20\x41\x57\x53\x43\x6F\x6E\x66\x69\x67\x73\x2E\x4C\x6F\x67\x67\x69\x6E\x67\x43\x6F\x6E\x66\x69\x67\x2E\x4C\x6F\x67\x52\x65\x73\x70\x6F\x6E\x73\x65\x73\x20\x70\x72\x6F\x70\x65\x72\x74\x69\x65\x73\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x2E"), NULL);
	}
}
static void ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_CustomAttributesCacheGenerator_ConstantClass_get_Value_mD7D685AF95A226A4BB1990F8EA037319629925D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_CustomAttributesCacheGenerator_ConstantClass_set_Value_m6780475A4086C5357CA1146CDE36646336E3D958(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_U3CAccessKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_U3CSecretKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_U3CTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_get_AccessKey_m509B5AE44459C85FBEADA67014CF2921BC8893C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_set_AccessKey_m04293A46CAAE7BEAF677E502A611FDB32A449D56(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_get_SecretKey_m8277ED2C42DC5F2180CFCC7C142DB6ABAB12BFAF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_set_SecretKey_m71E51B0FB54A61F43B8C6CCA0B5D78B94BE74F63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_get_Token_m084E4F55E325D68EBBE74EDAEEF101E29D6748E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_set_Token_m85A958A0783A5889E0F7429C0E8D624E0D70A7B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RefreshingAWSCredentials_tCB6E7C1C325EC27C7094A39AA68494071564423E_CustomAttributesCacheGenerator_RefreshingAWSCredentials_GetCredentialsAsync_mB595D535B6BFD152A8051EAB4C2C297BCEE6B7EA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_0_0_0_var), NULL);
	}
}
static void RefreshingAWSCredentials_tCB6E7C1C325EC27C7094A39AA68494071564423E_CustomAttributesCacheGenerator_RefreshingAWSCredentials_U3CGenerateNewCredentialsAsyncU3Eb__16_0_m0FC03D1BEEF1A6854CDAE4736471A16EDB04A301(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_U3CCredentialsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_U3CExpirationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_get_Credentials_m6C85D5BD9D58FEB3DE818A71424499EF455DADB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_set_Credentials_m107E3DADE4BFD6157B147BE7438BD649F1A2AD01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_get_Expiration_mFA7346BBB61AEDB3A90A8012518B54433EF7B17A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_set_Expiration_m1D4B0D4D19FEC97F0DD52569E60D4AF367A481D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_CustomAttributesCacheGenerator_U3CGetCredentialsAsyncU3Ed__10_SetStateMachine_mFA86F065CDB16A8B134F7E86E7E51EFEBE33081E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CHeadersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CServiceNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CEndpointU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Headers_m308C72B975C6934CC942A2690D429FC1544C04AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Parameters_m5E27A0995E6B14D58130418E0563E57BE1327AA1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_ServiceName_mEEA7C88A76BB3147789C6D9B9CAE7B45F5B3F5DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Endpoint_m753A6846890C931016AB3E9746D32CC29A3C7499(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Request_m975F088853F42DBE16625E28A06C8BC3F6B61631(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Exception_m670BD3838487CD3BC56C8111A55DBAF0B6AB98BC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IClientConfig_t38F64ED77DEB5E7EA3BFF6DCACCB2DE7D00BFB2A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void StringParameterValue_t96B48C9CA9280B1C549BB43C7FFB20216343E1D4_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringParameterValue_t96B48C9CA9280B1C549BB43C7FFB20216343E1D4_CustomAttributesCacheGenerator_StringParameterValue_get_Value_m6805935D4A23333C6204D77BC47A40B5780B2F01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringParameterValue_t96B48C9CA9280B1C549BB43C7FFB20216343E1D4_CustomAttributesCacheGenerator_StringParameterValue_set_Value_m2BDDB0D689259FFAD473566188DD655BB972ECD1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringListParameterValue_t7405475FB1AFE1E7AC8CB5C62759DAF8B4758C12_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringListParameterValue_t7405475FB1AFE1E7AC8CB5C62759DAF8B4758C12_CustomAttributesCacheGenerator_StringListParameterValue_get_Value_m8E742CAEA3583E1393F735531020B553C977A593(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringListParameterValue_t7405475FB1AFE1E7AC8CB5C62759DAF8B4758C12_CustomAttributesCacheGenerator_StringListParameterValue_set_Value_mCBB88C38BF207C8182ABC8B9F2B3AEFA3C8E3D16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void HttpRequestMessageFactory_t523C2321C4F177AC497E3C6337EF6B7E968E1942_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void HttpWebRequestMessage_t0836FD55486E3D9263D90C09EF313941DF89D056_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void HttpWebRequestMessage_t0836FD55486E3D9263D90C09EF313941DF89D056_CustomAttributesCacheGenerator_HttpWebRequestMessage_GetResponseAsync_m093528F50DB252F5D33A06C9F20250CE885DE094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_0_0_0_var), NULL);
	}
}
static void U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_CustomAttributesCacheGenerator_U3CGetResponseAsyncU3Ed__20_SetStateMachine_m10865D2EFB43097DB415EFB5DB496B817C850ED5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CMaxRetriesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CLoggerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CThrottlingErrorCodesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CTimeoutErrorCodesToRetryOnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CErrorCodesToRetryOnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CHttpStatusCodesToRetryOnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CWebExceptionStatusesToRetryOnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CRetryCapacityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_MaxRetries_mA76CE36A06EFC6A3935BDB64429EA5B6C596BB53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_set_MaxRetries_m36D4FB81E32CF4F8465CF6094FA4743C9D37F273(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_Logger_mE0AF8B9D9FC0BD25BBBAE14A26ADF916FF5610F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_set_Logger_m68A68C566F9C95673F60129AB1BD87D1EEC8E5A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_ThrottlingErrorCodes_m2CF20C9AD038CD8F4AA224BDB68351B424A3ED6D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_TimeoutErrorCodesToRetryOn_mD151D14490561845F443491871D89725648AFFDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_HttpStatusCodesToRetryOn_mA009A938783A07A9CEB7717806D9967308C5CA5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_WebExceptionStatusesToRetryOn_m329A25793DD3B758615EA509FA6B03F6A120C330(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_RetryCapacity_m63B7EA20A13720F8E9A9C5C82641A54CEC0E97D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_set_RetryCapacity_m088CC1BB9F5588B6D254B74CFA9F6533E214CA7C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_RetryAsync_m3D741AF314A36B2C421CD7717A0A4098F6DA88E4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_0_0_0_var), NULL);
	}
}
static void U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_CustomAttributesCacheGenerator_U3CRetryAsyncU3Ed__57_SetStateMachine_mF497DCFE92BB472842C19A106F09007A860FFF71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PreRequestEventArgs_t7F28C1A6A13B2A6A91DDCB8A6EFFFA9FEAD6C659_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PreRequestEventArgs_t7F28C1A6A13B2A6A91DDCB8A6EFFFA9FEAD6C659_CustomAttributesCacheGenerator_PreRequestEventArgs_set_Request_m913512B993407176BDE17EED7CCDB3B8EA466979(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CHeadersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CParameterCollectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CServiceNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CEndpointU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Headers_m4C4474DD9D0B6975CEE99E3ACD66C38585195278(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Parameters_m304A0C4EF2B41D03A1D35C78411F1240BDE41606(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_ParameterCollection_m63448435DC566936F2FACB1C97C9DAD1963E2F65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_ServiceName_m0A8B8B8C719817D7B4AC4CC873E84672367B7692(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Endpoint_m719E6225716C3AD740266C4DB15DDA9F11CD59A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Request_m2E93015C782370542C385BF39139235D647C81B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A____Parameters_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x6D\x65\x74\x65\x72\x73\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x20\x69\x6E\x20\x66\x61\x76\x6F\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x50\x61\x72\x61\x6D\x65\x74\x65\x72\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x70\x72\x6F\x70\x65\x72\x74\x79"), NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CRequestHeadersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CResponseHeadersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CServiceNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CEndpointU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CResponseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_RequestHeaders_m0F160670222462F92D8AC6B4E49400F2C5098EAC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_get_ResponseHeaders_mB3B05CF7F3CA81AD0E15756A6F1785D33BE103DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_ResponseHeaders_m437D99FE61C57DC60BFABE2FD4D5FD1FB6675C5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Parameters_m35607FF08CF6574B51713FA9C4B042C9C70DDFA3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_ServiceName_m08E80F5EC7828B0B97D34DA6C95E64A34531EA2C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Endpoint_m77D2AC3E87BB0C1997933A63273252B383496281(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Request_m74545AC00FF2931AC9D2FEF9F6AB7359459E0722(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Response_m6B8ADF83266A32FAB4DD7EF7C8494BF4E807B251(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringListener_tFD9A684F08F51994C058855FD9498C3FB667EC9C_CustomAttributesCacheGenerator_MonitoringListener_PostMessagesOverUDPAsync_mF045DACA39A35CE18A8D5C15D023D0389686A072(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_0_0_0_var), NULL);
	}
}
static void U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_CustomAttributesCacheGenerator_U3CPostMessagesOverUDPAsyncU3Ed__10_SetStateMachine_mE5C9EEDE31FEEC56BCF88F148813303D2AA51CAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CUniqueKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CEndpointDiscoveryEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CS3UseArnRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CStsRegionalEndpointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CS3RegionalEndpointU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CRetryModeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CMaxAttemptsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CCredentialProfileStoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_Name_mB713E06DF9022D5FFE37AD44EB9CCF31B82F60D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_Name_m63BC3AD462D0CF3669C11B75A37B3A72BB68FFDF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_Options_m4BEDDAF9941BA979CC731717C17A3FA7623749D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_Options_m7651DA7DA24A064F6178A6E6EC92DCA807641CFB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_Region_mE253F69F7886AD25DA66A653E39109186664EC95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_Region_m44E8C475F95D6C64B4AF534CA00C14267C09A795(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_UniqueKey_mB5E4CE2360F0A7A4ED514F50670DF31C51A7BDFB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_UniqueKey_m0EA7516DBFD617FF619D864790F48CC9A653B3DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_EndpointDiscoveryEnabled_m3DD6687CA952054E30AB5E108C586BDC1A2493DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_EndpointDiscoveryEnabled_m266929EB8A3C01BBDB26483D547F0E2113AC7E97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_S3UseArnRegion_mD66A32EE76676B6E0D9A398A0D6643F4ED720CCC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_StsRegionalEndpoints_m629728B051B1A50E72E33D61D6E94DDC7A162A26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_StsRegionalEndpoints_m7AD9C668F3A944E2CBD5A358F3A6918E996F9CD7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_S3RegionalEndpoint_m54FD9379579C26441119FEBBCB3B3F0F21B1DC09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_RetryMode_m73DB6E189368FE3F055AFEAA9F924EBED95D3A08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_RetryMode_m244DD300B11793D33F00593DD421B8598F1050AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_MaxAttempts_m3AB54AC420F35373634610FAAE1C62EFDEB190B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_MaxAttempts_mF8CFD52861CBC44DCA46556C0E43E68F0FF890A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_CredentialProfileStore_m8EA2A883DECA4473D43CE84A3DC15EA7EFD49097(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB0D11661A398B9230F0E9F816F3AAA033F15DE31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CAccessKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CCredentialSourceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CEndpointNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CExternalIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CMfaSerialU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CRoleArnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CRoleSessionNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CSecretKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CSourceProfileU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CUserIdentityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CCredentialProcessU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CWebIdentityTokenFileU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_AccessKey_m54D2DE99D522F53500F3CD0FD9388812C2AF6BD3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_AccessKey_m8F3AD526CCB0CC1DAD563F0364ADEE05A91B85E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_CredentialSource_m1D564B618015AF6BC155329EF0EE4D6DF8C16A52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_CredentialSource_mE1EF94AB93F049279711F301C2DAEF14F149C0A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_EndpointName_mA13C59933C46404D3017FCB826EAC2DDBD5E7E8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_EndpointName_mA51B88631A57B8BD75F330661072C58FB1961D8C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_ExternalID_m64A40A52D928228D64CD8C7B10DBF3101E5CF033(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_ExternalID_mCB2664BFB5E632389C706417724A2D52BBBDE95D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_MfaSerial_m9F3C36B9F19852F4EC2BDA5C61CB7700160053C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_MfaSerial_mA43E21320CC090A5D1F2D2EE9C763ACEDBCE5459(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_RoleArn_mD33C1E609E6DC33FCBA1F100DB756B3CE18F6CD0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_RoleArn_m3705C867F98614A1B66A34EDADBBA009690EEE3A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_RoleSessionName_m59225087CF2D7BB1A79F122FBB9C3D6DF136F5F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_RoleSessionName_m31748E87FCE77B513CBCB3D9C8BB9B692493819E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_SecretKey_mE9227433E5F455B2B3CCAB940DF1EF346D587439(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_SecretKey_m3D8CC902AD4C18EBE5F63747675EDDD4536AB7AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_SourceProfile_mCF1A8615E110EA66E829E79313B1AA7BAD904D36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_SourceProfile_m9E621D9FD6D7AB1FDD6933D6FC46CC1CD02DB6B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_Token_m67BF5840EAD9E17CF3B5741B23F446A8D568F1F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_Token_m9FD9FD44198997D786FF87FE35AF41A0D6BB70BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_UserIdentity_m4239DE2133B4968497CE6EA73849891F2B1797B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_UserIdentity_m90374CF6A6BB801369B7089063281017876E835C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_CredentialProcess_m06365B5DE3931629A94EAFE9651915EF32DCE968(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_CredentialProcess_mA6FD2353B05476E70C062F1C9B9A3A0E2A493ED6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_WebIdentityTokenFile_m816A1C6AB30B2CF7F3D8C564558BFFDF5C357F21(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_WebIdentityTokenFile_mA959CF718B74550D6CB663BD0880B8A635F4A742(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileStoreChain_t2F0486F073800BE502EEB9339172AC51CB9A527F_CustomAttributesCacheGenerator_U3CProfilesLocationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileStoreChain_t2F0486F073800BE502EEB9339172AC51CB9A527F_CustomAttributesCacheGenerator_CredentialProfileStoreChain_get_ProfilesLocation_mA618D2CDAF7EBE2C16FFD0F2BB91B7449C0AFA90(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialProfileStoreChain_t2F0486F073800BE502EEB9339172AC51CB9A527F_CustomAttributesCacheGenerator_CredentialProfileStoreChain_set_ProfilesLocation_m58C470C2CCA2358F7B72478DA1FDE835881E8A77(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SharedCredentialsFile_t5F16CF3C8539649DC88C10EC62C1B0750AE3F1B9_CustomAttributesCacheGenerator_U3CFilePathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SharedCredentialsFile_t5F16CF3C8539649DC88C10EC62C1B0750AE3F1B9_CustomAttributesCacheGenerator_SharedCredentialsFile_get_FilePath_m13D49F7E2F5200941148407607A118BA8BD85596(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SharedCredentialsFile_t5F16CF3C8539649DC88C10EC62C1B0750AE3F1B9_CustomAttributesCacheGenerator_SharedCredentialsFile_set_FilePath_m03BAF421690A3CECA50363586E082EDC0F03901F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t181D379D505CDBA8390F0FB0769870FE540C321F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryCapacity_t4E0969F86179452923C21FA89AC1FB3DDA20E245_CustomAttributesCacheGenerator_U3CAvailableCapacityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryCapacity_t4E0969F86179452923C21FA89AC1FB3DDA20E245_CustomAttributesCacheGenerator_RetryCapacity_get_AvailableCapacity_mCD9C04903EC7590720F4C00841A1A69DF0628611(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryCapacity_t4E0969F86179452923C21FA89AC1FB3DDA20E245_CustomAttributesCacheGenerator_RetryCapacity_set_AvailableCapacity_mF627A8F4B70ACE8DFB8B93C56CB21DDDAD273261(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 128LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_U3CRequiredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_U3CIsMinSetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_U3CIsMaxSetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_AWSPropertyAttribute_set_IsMinSet_mF2D9D9E5E5A2AFB8E96933B9A7D1F36BBF0ACAB1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_AWSPropertyAttribute_set_IsMaxSet_m098F9CAB1E60CBA824EF649B7AF3F4180F08A0A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CSetContentFromParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CHostPrefixU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CSuppress404ExceptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CAWS4SignerResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CUseChunkEncodingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CUseSigV4U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CAuthenticationRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CDeterminedSigningRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_SetContentFromParameters_mB03C1E564A7BB85E7B1840F5EE7918AEBE977309(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_SetContentFromParameters_mD83CB98B12FDC98258DDBE891F195215600A56DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_HostPrefix_m10D9DC3D8FB879A77D66612DABA751217A92A03A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_HostPrefix_m6E4E12A783326EF99A979ECF1992D7C4AD28E1B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_Suppress404Exceptions_m9EBECC5B37C54D8027C015CE9A8E4C8714E02398(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_AWS4SignerResult_m2E075FE2E9038A89E9EB365206B9101FA627EF8C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_UseChunkEncoding_m5375AF8E3FF31E65C632F3DEAD6E859885ECF329(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_UseSigV4_m4CA850E5CD4FB0F28EF2408B6D443B731AF10C7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_AuthenticationRegion_mEC4FD9325E607789DA3E0AB657F4997D1EBDCFFA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_AuthenticationRegion_m7390CFC4C888DA4876576238BF48C2E5884028D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_DeterminedSigningRegion_mF6982E156E6F30E8611F4672B86241104D76C8DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_DeterminedSigningRegion_m3DF45EF335C58F20A251970CE54E8E61C842AD38(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t6C9DD78418BF61F8E3566CB83D5B900BEA088C86_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CRequestIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CInnerExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CStatusCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_Type_mE53555DD966531864E2D76017ACFB4BEBBACBBDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_Code_mB45D11E4AC2905CC68C3703BD276AA7D7FF352C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_Message_m7C450B5EFD404E19C84070BF859275A7F95C595C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_RequestId_m7469C3C6D31632D79BBB4C5A6AB456D311D0FD41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_U3CEndpointDiscoveryEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_U3CRetryModeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_U3CMaxAttemptsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_get_EndpointDiscoveryEnabled_mCE81E1B1BC2AD1FA28359A0C9132350EF0A94D07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_set_EndpointDiscoveryEnabled_mDE3516A6EB931D9DE0ED43832E3CD9D32F4E0AF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_get_RetryMode_m85D04441ACF031F843F40340FE4D2B269735297E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_set_RetryMode_mAC0C563C32C2EC4FB8005DE17A71A95DE47907C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_get_MaxAttempts_mA1F79F4677829F49F656D48A56F8E012B27BEBF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_set_MaxAttempts_m16CC99498CF834DD7FBF28FF931D323333B44775(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t63A67453B701F5C1F910CA5FC5934B605214B83C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t15DF3D19CDE36F1AE8DA2EB8FDF3A2F98C7C9778_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParametersDictionaryFacade_tAB6A1312B817FA9D47CC1707489B9B1D7194E0EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ParametersDictionaryFacade_tAB6A1312B817FA9D47CC1707489B9B1D7194E0EF_CustomAttributesCacheGenerator_ParametersDictionaryFacade_GetEnumerator_m8106C6F93EFBE6BBC5E921ACFBDCD8198F97D8C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23__ctor_mF2A47390FF6A0D9DF41AEDCF316A05038745DAD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_IDisposable_Dispose_mCCB562FCB94036DF98F8DDD82D0CEBC92739E7D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mB770895B34BB9A97BDD81972BAA878115A963E80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_Reset_m7CD8581DD59E442903A7DEA8F5A7A6019A5FD816(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_get_Current_mE07999BA03923739E386C7B5BD271FC010E714A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RuntimePipelineCustomizerRegistry_tD94A7CA5DEFCA6F032EEEEFFE1D6601BCE48A67E_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RuntimePipelineCustomizerRegistry_tD94A7CA5DEFCA6F032EEEEFFE1D6601BCE48A67E_CustomAttributesCacheGenerator_RuntimePipelineCustomizerRegistry_get_Instance_m0FCF38817786A1D0F32D0DC785EB0A056AA6F060(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_U3CServiceIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_U3COperationNameMappingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_ServiceMetadata_get_ServiceId_m42AA96D72D2FD71B2EC4A9B4B33DC1B9A835A4AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_ServiceMetadata_get_OperationNameMapping_mAE5C256DC7C08383E6FCF215B18ED0D477D2DAD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3_CustomAttributesCacheGenerator_ParameterCollection_GetParametersEnumerable_mFD6A666A3B962D2B53212BAFB5D24132C33A21DC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_0_0_0_var), NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4__ctor_mA3660F51DEA5FCCA18250DF86B1E4228CE3004BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_IDisposable_Dispose_mB5E20B4637F27495FBB2EF920877949FCA12FFD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mD78AC053140A1678AA5390E3B84194BA638B732E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_Reset_m440496B3B80A3C4CA4E26640D5297498FDABED23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_get_Current_m4B1FA65DB4B72667C4995D7FA54F47701A8EF03C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m8556E7C7BFD90DCA9FB7837CB5D1FBC3C2F22D6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerable_GetEnumerator_m583C549C1976AF018F67004591B37239E0FCA2EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CMetricsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CClientConfigU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CRetriesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CLastCapacityTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CEndpointDiscoveryRetriesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CIsSignedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CIsAsyncU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3COriginalRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CMarshallerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CUnmarshallerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CImmutableCredentialsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCancellationTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCSMCallAttemptU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCSMCallEventU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCSMEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CIsLastExceptionRetryableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Request_mA4471DE841DE086DCA9A7949E3E906BD9E8EA7BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Request_m4D9502541AA3E426E0AD100D820F57097F0F3359(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Metrics_m8245B5AD45A4136FEA4A41FD0E9AE03BF910137D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Metrics_mFBEC125FE4162520753F26D54BA2ECEE87D7173F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_ClientConfig_m00A68FB365505864D204F7D9F0B4B3B0CA440976(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_ClientConfig_mB9CCDA771A29F9C7765C9AF67EAC5A88646874E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Retries_mB59731D9636F968BAA1706BDC5C07626A3FE1219(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Retries_mD88CBDC3B30D3A4BAA83FBE3BE13A6A977F378E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_LastCapacityType_m0B7619EE743164D5DC511D6A1DD843A467A8C7DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_EndpointDiscoveryRetries_m98C8DB853AA051758AC68367C68DF25D71A7E5A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_EndpointDiscoveryRetries_m76846D7FF5CAA564393ADB168341B125C1C23C79(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_IsSigned_mC3D6CB04C958B62F4D4423D216C4B01B8D43089D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_IsSigned_m92398DD2A210E0A2E98BE81442B7BEAA1F89AF82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_IsAsync_mE367ACF5C06381343028C5A45BDDDFAEF3164912(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_IsAsync_m9F1AF9842CB659049A616F2A850C6A594490C9AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_OriginalRequest_m76BC7D9FCAFA242D20FE9834A4E18D6F921ACF8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_OriginalRequest_m5D45287D4130E982EB4EF26BEBEC1ABF28B4DEBC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Marshaller_mE77303460FA245309D9033950B4594DFCC9D5F2F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Marshaller_mBC9EF52693055C503B97AA6E185EEB5FE965CDE3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Unmarshaller_m5A458EBF82A9432CF3C2614970F4A40960CC8116(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Unmarshaller_mCE129D11F8052AFDB6CF9165B7E791DAB77BBE4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Options_m0A2898FF3EABBAB4E978889AF1BB379BA7B7B24C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Options_m4F9A2FFAE721D3CAE9C9407B99FACA305FBCCA0A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_ImmutableCredentials_mC7ABFF395C149FF56563340D9AE520AA4CF82BCB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_ImmutableCredentials_mB8A770A9E8FFDB3EBD95C661A64BE87A4F682AB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CancellationToken_mAB9910A6A46E6F719840B02B74007BC8F9D14A35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CancellationToken_m416969291C198882377AB7F302BA0DBC4E953D17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CSMCallAttempt_m02134A8CD42466EBD1745D01C390776F393F6177(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CSMCallAttempt_m6A1FF6F8CF854F9298D2910A9D746EECEE87DE8C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CSMCallEvent_m241604A1F65FA848A747968080BC8D4BE19D9CAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CSMCallEvent_m2A7C298840A43F98784D2447CAC0C7437FC385DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CSMEnabled_mEC4878BD651F342B235ACB9E5B04B1C39EC376BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CSMEnabled_m3F7FD5E4D36DD00A8515633DB76CFA1F1CF9AF0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_IsLastExceptionRetryable_m9CBDEF39DDEB049FBC67FEB11EA2E369078E50DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_IsLastExceptionRetryable_m06ED2876291D5203B96981CC608CE51BA73BADAC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_U3CResponseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_U3CHttpResponseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_get_Response_m8B11E0CAD36313831E9E73DE776625B055C6E41B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_set_Response_mC3E7EC2795FC2AEDE791C0D6C5DD9CBDC2C229D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_get_HttpResponse_m0DE5A537E04426B2FBA39A54E1A6ED490714B12D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_set_HttpResponse_mCDB00703E92D7AF877659F9C077D0A478DB6DD30(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_U3CRequestContextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_U3CResponseContextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_get_RequestContext_m65AB9054F3B778D154885B4C08576F7E56D73471(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_set_RequestContext_mFD3FB7214104B3EF2E0ADA06F9E381EBD986E502(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_get_ResponseContext_mDA86EFB7136DEA63A6C462D56D0C19DBF2F5CB94(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_set_ResponseContext_m10DEA16A9002732D37F74B980F2EA9267682EDF5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorHandler_tFCB7945CCC6EEA033CA840E9909955687982A160_CustomAttributesCacheGenerator_ErrorHandler_InvokeAsync_mA6B08B34BE400DBEAADB3A4E6097AD9C114F6976(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_0_0_0_var), NULL);
	}
}
static void ErrorHandler_tFCB7945CCC6EEA033CA840E9909955687982A160_CustomAttributesCacheGenerator_ErrorHandler_U3CU3En__0_mF715B16526759528B2C5252D3705A0F6787CEEC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__5_1_SetStateMachine_m6C936EB22B847D8C4FB9DF854E7DDC489602AACD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_U3COnPreInvokeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_U3COnPostInvokeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_get_OnPreInvoke_m1A71F567B0855CDC3FF0EB544116E7172AFF2E26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_set_OnPreInvoke_m87C3C20DF9BD145545EE87C806FEB7D9E6371A50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_get_OnPostInvoke_m508D36C9CF19EABD021159EABB46E9BDEB3D8323(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_set_OnPostInvoke_m2B7939C80173D9408D0D31C441EF9222C946900B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_InvokeAsync_mD2CE9B7F99C27F4A11AF5F4F3892B01CB4DB69AF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_0_0_0_var), NULL);
	}
}
static void CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_U3CU3En__0_m99A29A787A9E307C23A725ABCBF0977CF9B01957(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__9_1_SetStateMachine_m3FA454185367AA9E4935CE16E5D4838C152E8043(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_U3CCredentialsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_get_Credentials_m536C03A6F14228A70E0FE37416B02E246B0CC698(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_set_Credentials_m0443A627032FBFA8D7D571BE129870693CB7A87B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_InvokeAsync_m0500C6F2F2934873A912C2D01C22E822BF4C98AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_0_0_0_var), NULL);
	}
}
static void CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_U3CU3En__0_m381E56F0CA3FDABF5BD7E6DC56C9430C2DA9179B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__7_1_SetStateMachine_m14ED844340B84CECECD68A317CBCB8390060D9BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EndpointDiscoveryHandler_tA77AF8EE23E7B35767508F63735536B66D405300_CustomAttributesCacheGenerator_EndpointDiscoveryHandler_InvokeAsync_m6D13E1678D1E0A52944C09740348C958C7DD9155(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_0_0_0_var), NULL);
	}
}
static void EndpointDiscoveryHandler_tA77AF8EE23E7B35767508F63735536B66D405300_CustomAttributesCacheGenerator_EndpointDiscoveryHandler_U3CU3En__0_m2103B2E379DC02BFADE47B5DEEDF68AB1EA085E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__2_1_SetStateMachine_m73731708A60A11B80100A0FFE6D77BABA067894F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_U3COnErrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_get_OnError_m128B66FA238E4109FE664413CA8D53BCE0A3721E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_set_OnError_m9B39DBF861DDF1523501DE9C1C7FBC96885AA0A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_InvokeAsync_m67CDAECDE835AD1F92A6079D00C8DE21B9DCD1B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_0_0_0_var), NULL);
	}
}
static void ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_U3CU3En__0_m85B2BD6389E6CBAC7CA3EC56D33C9B3794CD7750(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__5_1_SetStateMachine_m2EAF45F2868E4827975EDD3BF5D00D11C17609EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MetricsHandler_t5CD352FBFEBC2E7D15E6F5C2967C16474C80749B_CustomAttributesCacheGenerator_MetricsHandler_InvokeAsync_m50E5AA830E434CFC5E6EE6D21DB392C0134BD70A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_0_0_0_var), NULL);
	}
}
static void MetricsHandler_t5CD352FBFEBC2E7D15E6F5C2967C16474C80749B_CustomAttributesCacheGenerator_MetricsHandler_U3CU3En__0_m312BE1777AAF271AABB3F829A258431AAA7F0227(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__1_1_SetStateMachine_m405C9753A6E0E355B161BE54E594ACF8A1F1BB31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Unmarshaller_tEE40FB698DB46E39196C5E02D4937749BADC6713_CustomAttributesCacheGenerator_Unmarshaller_InvokeAsync_mE3D983ABC1F8CADA9FE88BB2D54BC0EFD47AFAFA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_0_0_0_var), NULL);
	}
}
static void Unmarshaller_tEE40FB698DB46E39196C5E02D4937749BADC6713_CustomAttributesCacheGenerator_Unmarshaller_UnmarshallAsync_m9EFFEA0695F1F4D6EB0388FF9465AA6EB981F4A2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_0_0_0_var), NULL);
	}
}
static void Unmarshaller_tEE40FB698DB46E39196C5E02D4937749BADC6713_CustomAttributesCacheGenerator_Unmarshaller_U3CU3En__0_m68B879761E284CB01A6D45D37BBADEFC62F8FD5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__3_1_SetStateMachine_mE8DD9FA58D93C16816CCD426815A8525D60B25AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_CustomAttributesCacheGenerator_U3CUnmarshallAsyncU3Ed__5_SetStateMachine_m92CF4477AB1C3D3C38CED8DE399CC58D1ED0F3F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HttpErrorResponseException_tC1C3C48976A8F3328CF4D595A8F7719A8404666A_CustomAttributesCacheGenerator_U3CResponseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpErrorResponseException_tC1C3C48976A8F3328CF4D595A8F7719A8404666A_CustomAttributesCacheGenerator_HttpErrorResponseException_get_Response_mF300FD82AF9B72A5FAC48955B57D1A6C16570391(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpErrorResponseException_tC1C3C48976A8F3328CF4D595A8F7719A8404666A_CustomAttributesCacheGenerator_HttpErrorResponseException_set_Response_m0EFA97C19C16304F4A116E23F4405DC3012630D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_U3CCallbackSenderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_get_CallbackSender_m11E5FE0ACD345673110BAE605A36FE1D0F2D6BDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_set_CallbackSender_m89A6908A3E9381F2F661872F672796B6D7144F3C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_InvokeAsync_mCB61B52F5E726D85622BC0AF4F5D9D3B6C8994FE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_0_0_0_var), NULL);
	}
}
static void HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_CompleteFailedRequest_m28E2B31B70445939BC4AFC3E5007979C83E7BA9C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_0_0_0_var), NULL);
	}
}
static void U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__9_1_SetStateMachine_m43FA2CD35446D0B3E748E3D50EBD7E8BFB9449F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_CustomAttributesCacheGenerator_U3CCompleteFailedRequestU3Ed__10_SetStateMachine_m6F75F272B99096ADAE6E3C618A382D197006EDBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_U3CLoggerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_U3CInnerHandlerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_U3COuterHandlerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_get_Logger_m079669ADD1EC8F4E440F64144BA3AF4EE7958403(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_set_Logger_m0C70A5AFC9FA063FB8A3E49FB0E9DF35BD663BE5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_get_InnerHandler_mC14FC8E11103E7D2DD80C869934B005486069356(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_set_InnerHandler_m2CA2C10774C68B7556C3BC7CC3FD7FAE1E0D6677(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_get_OuterHandler_m3C8B44F35F670546B89E7CEC831456D952E00814(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_set_OuterHandler_mDD4DE1D53C5122E7AB75AA43F93C38967F105C74(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AdaptiveRetryPolicy_t27319F5E3F091520A863559FBF7F427567C3195B_CustomAttributesCacheGenerator_U3CTokenBucketU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AdaptiveRetryPolicy_t27319F5E3F091520A863559FBF7F427567C3195B_CustomAttributesCacheGenerator_AdaptiveRetryPolicy_get_TokenBucket_m262D3EE41D0717E08267DA1261D9F9BAC003C85B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AdaptiveRetryPolicy_t27319F5E3F091520A863559FBF7F427567C3195B_CustomAttributesCacheGenerator_AdaptiveRetryPolicy_ObtainSendTokenAsync_m5C3B0456589FE9949204CF28185B0F6EA6C55F95(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_0_0_0_var), NULL);
	}
}
static void U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_CustomAttributesCacheGenerator_U3CObtainSendTokenAsyncU3Ed__11_SetStateMachine_m5B189B23DDAAE678549DE1625AD265E0EBE6E43D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DefaultRetryPolicy_t6B435B4C8CD1D13531C9DDC6D30A62E49F835DCF_CustomAttributesCacheGenerator_U3CMaxBackoffInMillisecondsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRetryPolicy_t6B435B4C8CD1D13531C9DDC6D30A62E49F835DCF_CustomAttributesCacheGenerator_DefaultRetryPolicy_get_MaxBackoffInMilliseconds_mAD8744FAF8A1B9D70F9D864F64C1A5B5FE28AE17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_U3CRetryPolicyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_get_RetryPolicy_mDC30A963F84526B46BCB050863888F12D4F572BC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_set_RetryPolicy_m63A981A434D1D7B794D90E8768339391FA299A0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_InvokeAsync_m38CF21A4236E185FA9F75B0BA13A982D9533C420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_0_0_0_var), NULL);
	}
}
static void RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_U3CU3En__0_m268DDA826AAEEAB2E0491EA7272D432560C35F91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__10_1_SetStateMachine_mCD78BFB0C0BBFEDD46583821A43BEDE743C96325(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_U3CCapacityManagerInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_U3CMaxBackoffInMillisecondsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_StandardRetryPolicy_get_CapacityManagerInstance_m5B53C8DF35CF8F30F2651706E2F2DC69289BD0F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_StandardRetryPolicy_get_MaxBackoffInMilliseconds_m6662682822FF79F5655C62F39E86FEF381F21A04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMCallAttemptHandler_t86AB3E0F743BF8160269931783BAD12F67C6B787_CustomAttributesCacheGenerator_CSMCallAttemptHandler_InvokeAsync_mD3264007D3D20F97F327ABEC4FB7D97D0CCC9ADF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_0_0_0_var), NULL);
	}
}
static void CSMCallAttemptHandler_t86AB3E0F743BF8160269931783BAD12F67C6B787_CustomAttributesCacheGenerator_CSMCallAttemptHandler_U3CU3En__0_m4177B682048AD54BFA3A41B664C421F69C374E25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__1_1_SetStateMachine_mC61E00A059BED7F1CC76F1E8979C2E26DE2B6BF5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CSMCallEventHandler_t051E26E9C1965113A789BD402C5D1F6DEA419B23_CustomAttributesCacheGenerator_CSMCallEventHandler_InvokeAsync_mB663921736555D69C1AD092018719CE322FA8335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_0_0_0_var), NULL);
	}
}
static void CSMCallEventHandler_t051E26E9C1965113A789BD402C5D1F6DEA419B23_CustomAttributesCacheGenerator_CSMCallEventHandler_U3CU3En__0_m0168D5033BC97341665371FD48B3EC213B980992(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__2_1_SetStateMachine_mFEF219175BD131DF200E75E4DD47B6BE843D8DC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CFillRateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CMaxCapacityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CCurrentCapacityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastTimestampU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CMeasuredTxRateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastTxRateBucketU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CRequestCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastMaxRateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastThrottleTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CTimeWindowU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_FillRate_mA0A7ED0D0D33364D5A61E2BBBC7E6C3F664727F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_FillRate_m72489ECECCF952D97E9A6C0D9B7E28CE87DEDC12(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_MaxCapacity_mE1472754E58556B1E734686A99F455499093F774(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_MaxCapacity_mC9EE0F436FA9DA0B77AB46F8DDACE7EE35C8942F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_CurrentCapacity_mA421C72A7EED744D51FF1C8042EFB16E090F2C6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_CurrentCapacity_mCE6F1E8099C66C0A073A45B760E7E74A8E0EA8AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastTimestamp_m0FE13E882A81F71410D028982FB12C3A2FCD9158(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastTimestamp_mD6473615DB7CC4E992DE2A09264467D9E62B7B12(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_MeasuredTxRate_m1A629CE8A0D664B7A488ECA0893CBA3E5B3344CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_MeasuredTxRate_mFB5DE4F1071C968447A87E109189008485EE0EAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastTxRateBucket_m02293BF2983E261302A22A14DF81AAF421ED0779(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastTxRateBucket_m218A404F7B552EB50D6C40D0140C222BF1DB83E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_RequestCount_m05B019782DEB0D4D21A960F659A6E1B3B45DEBA7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_RequestCount_mB65440290DFA7CA01D193C6815F774E4E70E5CD6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastMaxRate_m18DF834E4844CEC6844F4DEABBF8B6AE3A1CB658(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastMaxRate_m6F3B8E232E1213DC18CD217A37A2F345B2E0D8A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastThrottleTime_mD3267451748BD6074DEFB8ECA779D9E630D10765(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastThrottleTime_m88EAAC3991614FD2C871E79FBF147CDB303B4CBC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_TimeWindow_m7916F196613D45C135BD7A1E263326F5B4993F91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_TimeWindow_mDB11F5A40AEEE3B6F25ACA9AB5768DC7263819F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_Enabled_m776C13DE5371FB8CBCB2E21F2999FEE99B03F5FB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_Enabled_m095AB1B9C82FFA6598F942926581D6D1AF7820AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_TryAcquireTokenAsync_mC4D9588455662EC0320D548DA857AF578A5A7C3F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_0_0_0_var), NULL);
	}
}
static void TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_WaitForTokenAsync_m928EB18E092E08F143F8EBDC694B5E9C3CB91CD5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_0_0_0_var), NULL);
	}
}
static void U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_CustomAttributesCacheGenerator_U3CTryAcquireTokenAsyncU3Ed__55_SetStateMachine_mC8FB6D650565057DAF0C7310E6CFC027923C0B62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_CustomAttributesCacheGenerator_U3CWaitForTokenAsyncU3Ed__67_SetStateMachine_mCA1B76DFBDD3ED10552418255052F57DA8067186(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CHostU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CPortU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CClientIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_Host_mB62FF9EA748848CC74E83DB1F44655C0BD243E92(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_Host_m9B1428EB43FB9DC145988A0AE89AF5A0285BF471(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_Port_m1E23C410430AA481A44D5B334219AA366F8C1059(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_Port_m51AFCCA8066C2479050A5D4395B625A824D47475(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_Enabled_m81AD5D5A20D6A39D2F8A2620EBC7B89AD6731E32(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_Enabled_mD6BC7CD6553DC314AAB1B598F5F260335F8427AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_ClientId_m4CEA76CCE2A19DF9F341B90C1001B38755A436A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_ClientId_m5A2E035C2DBA99F74DA66203FCE3F5303BB83E3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CAllGeneratorsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CIsConfigSetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CConfigSourceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CCSMConfigurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_get_AllGenerators_mB2A3806671D707BA6166978686F6B5BA9FAF31F4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_AllGenerators_mFF5CE6CA4C99BD98545E405EA4FB105E41EB54A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_get_IsConfigSet_m31F9CCE5FC84C9C093A4DCD333B9E34A3769EB11(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_IsConfigSet_mA9BEC2B921058B43BED14457DF2A83CD720AB1CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_ConfigSource_mB2E85549468F1E0237B189424F548A851D043F68(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_get_CSMConfiguration_m529E6F75EA90D53EA97DF81FCC1F8AC8E8225766(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_CSMConfiguration_mB622A47543892199ABEF2C8F79E586D01B3CD5CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_U3C_ctorU3Eb__19_0_m2AA81B437DA8CF71B653F813766CAD9A12B224BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_U3C_ctorU3Eb__19_1_mFC07E82E37B78AFB08278848459549CC77CD5658(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_U3C_ctorU3Eb__19_2_mCEB6395641F132B61A623792AC2A39C8F41D102F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfileCSMConfigs_t61934EAD438F3CF47A010D583891DC379530CD18_CustomAttributesCacheGenerator_U3CProfileNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfileCSMConfigs_t61934EAD438F3CF47A010D583891DC379530CD18_CustomAttributesCacheGenerator_ProfileCSMConfigs_get_ProfileName_mBDAFA68A6EB56A0C3F5779C8D3DCC4F9336F0519(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfileCSMConfigs_t61934EAD438F3CF47A010D583891DC379530CD18_CustomAttributesCacheGenerator_ProfileCSMConfigs_set_ProfileName_m4AF99B4E75DB4445951F8F25191C5BF1E69090DE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnvironmentVariableCSMConfigs_t0762DD2415B673F53C345E6E12328035CB268269_CustomAttributesCacheGenerator_U3CenvironmentRetrieverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnvironmentVariableCSMConfigs_t0762DD2415B673F53C345E6E12328035CB268269_CustomAttributesCacheGenerator_EnvironmentVariableCSMConfigs_get_environmentRetriever_m0DB2581E62898D47F6616E31AA36CAF0146E028E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeterminedCSMConfiguration_t21AED8927102140FEFAD6AD2FEEC44D89B5679CA_CustomAttributesCacheGenerator_U3CCSMConfigurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeterminedCSMConfiguration_t21AED8927102140FEFAD6AD2FEEC44D89B5679CA_CustomAttributesCacheGenerator_DeterminedCSMConfiguration_get_CSMConfiguration_mAB96F0DC4AE5F8F1DB7727A094F2BA66BD933382(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeterminedCSMConfiguration_t21AED8927102140FEFAD6AD2FEEC44D89B5679CA_CustomAttributesCacheGenerator_DeterminedCSMConfiguration_set_CSMConfiguration_m5D22C46AFCFD26C97C8F6E662F9D0475C4F9D8EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CApiU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CServiceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CClientIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CTimestampU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CVersionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CUserAgentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Api_m0421D8D4DA5663899D39BD23D46C40EBBA6F3483(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Api_mC4C55733606CFBF05B305CB46D6E8B4B005CEE30(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Service_m3B91F2CD211A542CBB96C9136BA39202067353A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Service_m144B8CFF0ECB8CC57B168F4069A84BD6AA82920D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_ClientId_m2A044772ADA95AC4480BC9951B69BF92044042B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_ClientId_m6C80B675D0CB589DCFCB208BCC6E9F3CA12183D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Timestamp_m1A5EDDC9894C0FB1AEB4F00CCD8870B1A6604936(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Timestamp_m03E081ACED1B45E8652C1DEFC3AE78220C6B2E22(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Type_m838B0741BBF668D7B367C1C3BAF1309D7D4071ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Type_m9AEF4F1CBD04509B7926C45ADC98C15936DA4AB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Version_m1350F113A9E10FDB65F58362C721175A6F91B4AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Region_m5CD08A43220F126CB25B2AB57CA2D3D774FD41A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Region_m3D084EFEDF90D8B8449E28A372C780B999ED0E0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_UserAgent_mDCF593DAA9DF1950F9788E0DF4D42CEB1E627C02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_UserAgent_m1F837089B138AD498DF2950E1F390D0D6C0D64E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CFqdnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CSessionTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAccessKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CHttpStatusCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CSdkExceptionMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CSdkExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAWSExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAWSExceptionMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CXAmznRequestIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CXAmzRequestIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CXAmzId2U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAttemptLatencyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_Fqdn_mC1C7B451BFB6364D78DB00E5070E111FAB35D0CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_Fqdn_m01BE278F2D17A95AB94C37F7FC59A4E9FD64313F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_SessionToken_mFDA438C3142E23E75C4B0132490CF1F519C1FD80(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_SessionToken_m945A8D8440A19BCDD6576467B44894A81BFF9EA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AccessKey_m96D3FB844360E8D74D7FBEAA11076D33EB01C515(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AccessKey_m4A6EA9A4C5E28F7CF04F7B02EE0113183AD8F164(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_HttpStatusCode_mD24FDF30CD12CD3F88848993681D41F93781C502(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_HttpStatusCode_m987F92618CAD35F5BC8668334BBF5EAC3B0FBBDE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_SdkExceptionMessage_m630E9AEDE21F51C10E55EFEC2C6C3230D0EF8871(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_SdkExceptionMessage_mBE3B617CA3F000E2B607171A82C93150132B395C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_SdkException_m094D5FBD1FA02A53608668E5891F650005D7F0D0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_SdkException_m96DA837D58F7B9E33292F7DAB80C6D616FE7246E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AWSException_m621BF4D5C58B7ABB9FEFCE8EF74F78C85428FE5E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AWSException_m8A014D4863F83034A00C45402AA37DF090DEFB24(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AWSExceptionMessage_m1C9CA1F161FE48A9E07E0B75CA28C5B1B784599F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AWSExceptionMessage_m7112DBAA36E0128BC4BE3D4522EDF9062D57ABCA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_XAmznRequestId_m45F79A7B23338AEE1F00F484DFFFAAF6A6EAE3E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_XAmznRequestId_mD2E1584FB7FFE82F4CF4830E1489132D01AE94F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_XAmzRequestId_mD13076615AEADA948BEDB6B9E9BED3C1992AA04F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_XAmzRequestId_m641F0ABC8509CBC655349F822A3568D3B11B995E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_XAmzId2_m63ECE3EA15B22180CEDB408A79A7F1B4BC76F37B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_XAmzId2_m5CA9D84BDD1AEAD6F04509F43D3A0D8FA0E356D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AttemptLatency_m16BA30F9971385B8E01E41FE78961DFBFF81E6C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AttemptLatency_m0CAEAAC28246217A1C9964A1779C3B58F5D6D926(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CAttemptCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CLatencyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CIsLastExceptionRetryableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalSdkExceptionMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalSdkExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalAWSExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalAWSExceptionMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalHttpStatusCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_AttemptCount_m9FD13509E959B8D2845F6B343977E8C868076AAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_AttemptCount_mBD8F813864ADCEF4408000597EF31D39219B723C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_Latency_m03DA86A6D65A9293B19F3C3A6BE18EEDD91E8344(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_Latency_mC5CD2AE1112B756AE6BF5E7DC5B8B0516533FED6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_IsLastExceptionRetryable_m02D2DB01CD71002D7BD5F7E7B6370B338BD5781F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_IsLastExceptionRetryable_mAA7FD50220A6E6D50F202F17AFF8E6224D3F348B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalSdkExceptionMessage_mB883D7A30AEFE8D0C95F7ACBC14FF17F3D3E8D58(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalSdkExceptionMessage_mCE4CEBB9D3195B4FCA1C97F06FDE129BF4E038D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalSdkException_m4C0783F4BFD0AECB34A62A8BD9482D985B4692F1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalSdkException_m844468E3AC8FCEB66D06429B93F3CC1DBD348A95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalAWSException_m915DD2A5206AEB58EF4B4C97BCC865DBBAD5AED4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalAWSException_mAE895618DF6F8E6795E9BA79F7494C613C037BDF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalAWSExceptionMessage_m57BF5E0DB609F04A09946FBC697EB8A73866CA08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalAWSExceptionMessage_m5F69148E6FA2848E9476EDC98F4C7F66F23B26D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalHttpStatusCode_m52DF47D94212EBE72D665682FBFBE07B726989D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalHttpStatusCode_m1765EFAE3F3608A5C828F78249B1674D7829EF65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_U3CAllReadBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_get_AllReadBytes_m47F8162EE81AF5D64CC11404AC5F016C410A571A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_set_AllReadBytes_mF9E1FE6C9EAE04CB72D94F64A12E80D148792449(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_ReadAsync_mCCF843346B0BF947D6ED985A9D134B554BE74A95(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_0_0_0_var), NULL);
	}
}
static void CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_U3CU3En__0_m732E60ABD6DCDDE8A24C7630D1DDB960A789B57D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_CustomAttributesCacheGenerator_U3CReadAsyncU3Ed__10_SetStateMachine_mFD24E47F2568DF602D75FEB1AD250C09AF1402F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_U3CHeaderSigningResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_U3CPreviousChunkSignatureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_ReadAsync_m6A47C1B5537F49092C6FC4F3BEF907635D170E27(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_0_0_0_var), NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_FillInputBufferAsync_mE7F390F23DC7FB28A04E09403ADB3089687A2DB8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_0_0_0_var), NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_get_HeaderSigningResult_m65E448AFAA7096A20053DAAB9072B2B487A1F1C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_set_HeaderSigningResult_m83F964155AFC9D296239E73EF94E1C3B8113B65A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_get_PreviousChunkSignature_m92C109BB64841B864EDDBC37BB622F1A5CE4AF12(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_set_PreviousChunkSignature_m352F26BF5C023E4CC80654ABE32CBA91012467A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4FAE7D91E2A2589B2E870B88D30A3F801EDF5A30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_CustomAttributesCacheGenerator_U3CReadAsyncU3Ed__17_SetStateMachine_mAEBAEDD593301A03FA75541227C724FE80BE9FEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_CustomAttributesCacheGenerator_U3CFillInputBufferAsyncU3Ed__18_SetStateMachine_mD3F93DDF86B76F1C0F6546C305314C798C2255EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BackgroundDispatcher_1_t4309C58161837D4C2ED66BBBC5E12B50DCEA9D51_CustomAttributesCacheGenerator_U3CIsRunningU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BackgroundDispatcher_1_t4309C58161837D4C2ED66BBBC5E12B50DCEA9D51_CustomAttributesCacheGenerator_BackgroundDispatcher_1_set_IsRunning_mC3571AF9EECBBB8F5797C73408F5173EC608073F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tCC87698EEF5F4D2B6C3ABCF5B59E25BDCD5A31C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_OnRead(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_EventStream_add_OnRead_m0DE98B157EC382ADADEF709A3151FC5EEAF22B64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_EventStream_remove_OnRead_mEAA2C6160B35C2510258E99BBBD8FFA3E4C2934F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_EventStream_ReadAsync_mA6ECBA89A86A4B35D07528B73E5E2C5181996E37(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_0_0_0_var), NULL);
	}
}
static void U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_CustomAttributesCacheGenerator_U3CReadAsyncU3Ed__33_SetStateMachine_m93DC06F8C05B23E12F7E832FF063A3D652DDA55E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Extensions_tD20EE6984D3F1330D9B257EDFD79576477E10E56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_tD20EE6984D3F1330D9B257EDFD79576477E10E56_CustomAttributesCacheGenerator_Extensions_GetElapsedDateTimeTicks_mFC9EB180A1895BD3CFA90ADDC1BA0E8EBB37A3B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_tD20EE6984D3F1330D9B257EDFD79576477E10E56_CustomAttributesCacheGenerator_Extensions_HasRequestData_m89B15A85A93DE8851680363F7237A1BAF07F6700(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Hashing_t9718D4B72E54CFFB39DBC808B4C061F78204D1AB_CustomAttributesCacheGenerator_Hashing_Hash_m70FC42FA944667BD6E608BD6040E3695B3B0F1E0____value0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Hashing_t9718D4B72E54CFFB39DBC808B4C061F78204D1AB_CustomAttributesCacheGenerator_Hashing_CombineHashes_m4098B2EDCAA2187D45472404BB15B32364853310____hashes0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec_tE4260B2D727D37C5129AD683490F0D7398ABA3C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_U3CAlgorithmU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_U3CCurrentPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_U3CCalculatedHashU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_HashStream_get_Algorithm_m77777CC969D213FAB4ABCBBA87FF03AB65FB9647(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_HashStream_set_CurrentPosition_mAC27EEE93E512BAB4450D2ED614850362DAF3F9A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_HashStream_set_CalculatedHash_m78F1745C53826D49A4888F317B65495C587FBD72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_InfoFormat_m1222504C7D4DEFF4EDF128701F5D075CD54B8987____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_Debug_m663D987F6E8EDC2BFEC5FB0EBB0EE7F9109F5090____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_DebugFormat_m085D13025E284600A2E931572BEC4E534DEE4065____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_Error_mF49ADFF7AB503ACFE042F4DD0DC860413640CF0B____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_Error_m8923FC249477CC668869717EBB99806926E41235____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_Debug_m8E6ABC0E7BF0C1A2A801E722EE546EDEBF694431____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_DebugFormat_mF95C2856A41926ADB444CCC2C51E5B093D253C29____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_InfoFormat_mFECE6C230B64353D25358155EEB0088B48A3F867____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_Error_mD839C4FC38C8C01EF7836C7632549115BEA394BA____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_Debug_mD756AEAD6298C36D3C76F94FC478D15E3FE965B1____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_DebugFormat_m525735E1788943E3DE772B42C9A64BAF1C32307B____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_InfoFormat_m834B8C45A15D6C18D10E8DA641C376B1A96B7AE0____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_U3CDeclaringTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_U3CIsEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_get_DeclaringType_mF8C58DF13E2E9F52A6C1C28533698C7FC23549A3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_set_DeclaringType_mB5E079C1381CB75406DC0A2B0C3F679FE6892E50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_get_IsEnabled_mC05569861EF1FD59E173C1CC3D538015554CE830(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_set_IsEnabled_m60444B552DE4B9925465942D0E3155D5839B5316(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_Error_mE7512D2E9A793AA701FCC1D3A99E3C25B8D55C31____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_Debug_m4553948E51DBC458D053FE02F962A237AD126C31____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_DebugFormat_m27AC3A09EE77336FDFD860D467F9BA66CA5A2618____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_InfoFormat_mEEB16A13BD30041D563F3DB5CCB94770C12B94A9____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_Error_m8C3018BBD99EA7D20D9D71034E5424DB9C0DAA57____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_Debug_mFDBE196995454C86FCC9F1D7F760A2B6D9DB56F3____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_DebugFormat_m7D57E2EC78D5CF1F90534253B2CA280E18BD92EF____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_InfoFormat_m4C7FB0FBD2A2E2E5553A5F3242BFCE901ED1E5FC____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_U3CArgsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_U3CProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_U3CFormatU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_get_Args_m64559F784FBCEF8CA2E60E453E9E2D284D4924CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_set_Args_m57166E78E7484C714F1CF706EE6ADED27C3430A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_get_Provider_m3A902C604D423FBCA0E6BAAEF89002A0D443B8BC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_set_Provider_m44E3AF9A95855B86FEC00453D9B1BEEF4AEB12B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_get_Format_m60859CBB4FAE49145535300C94EE5B55598D2DF4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_set_Format_m72DEAFAF7695EEB50DA0A52565923C01F4068ABD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage__ctor_mC9E82515541AEA85C7ACFE35081A77EC6E94657F____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void LruCache_2_tE3253D91CEC5F96B76E3E2D86DFB2CD7F28D5572_CustomAttributesCacheGenerator_U3CMaxEntriesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruCache_2_tE3253D91CEC5F96B76E3E2D86DFB2CD7F28D5572_CustomAttributesCacheGenerator_LruCache_2_set_MaxEntries_m44DB2EB5F53278EC1AC5CEB9C6B1165B8C0DE33A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruList_2_tB7EF1BC2EB94A00EA0AC42D4E0393E30EC78905D_CustomAttributesCacheGenerator_U3CHeadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruList_2_tB7EF1BC2EB94A00EA0AC42D4E0393E30EC78905D_CustomAttributesCacheGenerator_U3CTailU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CNextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CPreviousU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CPropertiesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CTimingsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CCountersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CIsEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_LogError_Locked_m233C41DA154865B536D979BC9FDC3C748576CF84____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_LogHelper_m06E3E60039285253B932C73671888A65B1C89D4C____metricValues2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_Properties_m84BC22CAA8541BABE6FE73999D148C1ED76DBBF0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_Properties_m3C55799F13A923439566FF92885EB32CE2EC716F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_Timings_m16064050B633CBE0E591C154A132ABF4B0A28E26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_Timings_mEF536D7BA4D9F52DA8869B9937C519F356B9DF67(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_Counters_m8A0BDC41BE680C0E804B86D01693558CFE0E5894(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_Counters_m04B53A30F43131DDBD3978F730C6ECD02257749D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_IsEnabled_m1C66114D0BC5B5FADE7FA6A59F0DE19776471245(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_IsEnabled_m8F6AECA196BF20FB649C8AB8EA0A0BF3E7186E1E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tCA7C77076A9B4EC73A50376D420A8CB4ACE46898_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Timing_t4D1C5000BC2696A4879EB7862DED30F6882A453E_CustomAttributesCacheGenerator_U3CIsFinishedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Timing_t4D1C5000BC2696A4879EB7862DED30F6882A453E_CustomAttributesCacheGenerator_Timing_get_IsFinished_mEB880282891D5893958B9981CDFF58ECF2BB304E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Timing_t4D1C5000BC2696A4879EB7862DED30F6882A453E_CustomAttributesCacheGenerator_Timing_set_IsFinished_mDB14D3D301BF0DAAFC799C58EE20E6838623A380(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CMetricU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Metric_m0EFDCF995545075980F8C80E5597CEB829AADF6C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Metric_m520EB610297922EE7EAB30E41E3C0F6E6EA091FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Message_m0CB9DC2D53A93DA95B07DD455CDC0FB60CD2C05F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Message_m0541CC94D5A9606C37C1485C32A0C39133A5FE3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Exception_m6FC385BBE21A845875D34A44D1DC6B265419B139(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Exception_mB492556F5CDD058FF0A84D4FEEF2EDD032A6FF55(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Time_m287005246E7A6BED191E05ADB098B6BE5856F39C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Time_m0FD2F80D3A72BC1D80EBA419A23833BF268747C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError__ctor_mEF8FCBFD3DD508E52D6BE9798AC603FDBEF7F58F____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError__ctor_m55C1C6FEF81AF627136BB5489BC1BA441246FE36____args3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void WrapperStream_t4676047E9C65B07194C4FF80564CF1077A7550CA_CustomAttributesCacheGenerator_U3CBaseStreamU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WrapperStream_t4676047E9C65B07194C4FF80564CF1077A7550CA_CustomAttributesCacheGenerator_WrapperStream_get_BaseStream_mFEB26EFB3BC758284D7CF3F725BD9E865E3D086A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WrapperStream_t4676047E9C65B07194C4FF80564CF1077A7550CA_CustomAttributesCacheGenerator_WrapperStream_set_BaseStream_mD8B7FFBEA1AFC67B0B69D0AD97F28771F6767DFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_Error_m43F37752F40DDDA5E12BB8215DA6AA108827B464____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_Debug_mA2001BC068D54399EA529AD2F7B6394C3F7806C2____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_DebugFormat_m6F0D41299168CCA93DC6CD0388F93395618D7DF9____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_InfoFormat_m9023523528D24439C250B64566BE630C5CD7D407____arguments1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_U3COriginalContentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_U3CFilePathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_U3CLinesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_get_OriginalContents_m7ED6E4DB738F208C66A18E8BE114AFFE62296852(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_set_OriginalContents_mBDD535792A93B7CA5602DAEADE392190C5D51AF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_get_FilePath_m8B21A1CC348771C89308D256C7A25B7AE7884491(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_set_FilePath_mFD1F1F01F35075BE40E6CC242C9E85DA541E4E34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_get_Lines_m6DF3631DE46A8EB2B3C5B0F96F89DB2D711BDBA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_set_Lines_mCD018C2303B399C5A81BC7A2D90DAA0233CD0254(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfileIniFile_t9CA7AFA6EB1CC84E3A7D05997EA461D7E67335A1_CustomAttributesCacheGenerator_U3CProfileMarkerRequiredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfileIniFile_t9CA7AFA6EB1CC84E3A7D05997EA461D7E67335A1_CustomAttributesCacheGenerator_ProfileIniFile_get_ProfileMarkerRequired_mBF818D67B6A16168B253C60CD65BD3CB4A6E29E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfileIniFile_t9CA7AFA6EB1CC84E3A7D05997EA461D7E67335A1_CustomAttributesCacheGenerator_ProfileIniFile_set_ProfileMarkerRequired_m78330C35440F6AAAA16C6B6115F7611E640FDCC9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_U3CObjectExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_get_ObjectException_m7D008D6F4B95533A1B9994CF182147BCA532C3A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_set_ObjectException_m6C1CB681D339DED2069010AA0513E31E155D20DE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m7BEA15F25CAD1B33751ADD0797A19CE192B808A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_1_t3EA451AC0DDD9C0ED3D317639390BBA5A58AD3B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_1_t3EA451AC0DDD9C0ED3D317639390BBA5A58AD3B0_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass1_0_1_U3CRunSyncU3Eb__0_m23E3F67F44F64EC01B7E4803D29A295B755266AF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CRunSyncU3Eb__0U3Ed_tD5715C887076CAC33A7EE1570F29B477D8DE2B9C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CRunSyncU3Eb__0U3Ed_tD5715C887076CAC33A7EE1570F29B477D8DE2B9C_0_0_0_var), NULL);
	}
}
static void U3CU3CRunSyncU3Eb__0U3Ed_tD5715C887076CAC33A7EE1570F29B477D8DE2B9C_CustomAttributesCacheGenerator_U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m15C37DB8F45A8E255F4B5CE58495CEE840F31DA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952_CustomAttributesCacheGenerator_MarshallerContext_set_Request_m8F802D68F09FC7F70F1BA36E3B4FEA893706D834(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_CustomAttributesCacheGenerator_U3CWriterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_CustomAttributesCacheGenerator_JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_CustomAttributesCacheGenerator_JsonMarshallerContext_set_Writer_m324F3C5EBDFBD98ED3E6B96565B40DCE1B066955(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_U3CSegmentTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_get_SegmentType_m65B82F8564E2206BCE241B78F79E678AA696D575(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_set_SegmentType_m40F0CBA4B9A4C76E4256AD3195C964DD88906D2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_get_Value_m35D398000A8EA78DE5FAF47E7BC0BF1C44EDBDD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_set_Value_m909F2ED45DF5DCABEA9D81BC2761C3D8267F5101(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CMaintainResponseBodyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CIsExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CCrcStreamU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CCrc32ResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CWebResponseDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CWrappingStreamU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_MaintainResponseBody_mEE9520F845AEF875317FB56E2DD0429A98422D93(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_MaintainResponseBody_mDD39548A3482E4C6C005544A32A53468DD962322(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_IsException_m88F8C42DABA97B6253A32CC60EEDC2D756C337F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_IsException_m2D4133C986F235F215588C6C87D8515E4B00F585(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_CrcStream_m01B55B4F84F8411D97E5299E88DCA96BD9761680(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_CrcStream_mDCD547E103C6E2B8F63A483422E48D66A60BAC4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_Crc32Result_mFCCC6104016C906AE1716B3660E6166F2DEF085F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_Crc32Result_m48140B1D642B7A6F16B071E4A0D5D5E8683C25B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_WebResponseData_mF6135EFB850AEAAD2B212B7038C8C25AA38AB8F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_WebResponseData_m7F8BDA75500276956944AF7565D842B44E62C9C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_WrappingStream_mDD39993BF5ECDC711F69C9220A464260571C26DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_WrappingStream_mC885B682045B8C30D8083C7E76A987472AA8AA93(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CStatusCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CIsSuccessStatusCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CContentTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CContentLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_get_StatusCode_mDA92BB2BEF3BA1BBDF1DB3D14295D34B898CA7B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_StatusCode_m038084D472C1D5D77EB3444D44C591043EE75522(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_IsSuccessStatusCode_m8E9360B0084DA85637A85A11F1860D191E966E90(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_get_ContentType_m5CF5885F8847B1D6DAF0D87D1F98631F94D74F6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_ContentType_mF447D5D6B510AE8735E69A23753ED627F10F3A43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_get_ContentLength_mA95E15F1F79F452C9B09FCC33C8A1AA54D9393F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_ContentLength_m769C7803F8FCABAA68F2F8820AE64E7EEC74922F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpResponseMessageBody_tF5C8524A8FBBE81FC4774C13D8AA8E7AD6AB4984_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void PersistenceManager_tB518F8E4B3AEF363860FD6FBEB48AE7E0F51B290_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PersistenceManager_tB518F8E4B3AEF363860FD6FBEB48AE7E0F51B290_CustomAttributesCacheGenerator_PersistenceManager_get_Instance_m184BC55A18A7E106F5F8FB191DDF160960FD3BE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PersistenceManager_tB518F8E4B3AEF363860FD6FBEB48AE7E0F51B290_CustomAttributesCacheGenerator_PersistenceManager_set_Instance_m4334BD1CAF6918F9F6B8443B13FDD70D646E9B86(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator_U3CInitializedEmptyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator_SettingsCollection_set_InitializedEmpty_mE31FA906B0A88CA7AB7AAF997C4D9D6ABAD672EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator_SettingsCollection_GetEnumerator_m79DFE4C6FF170762974BF7E5745417DB8AEB56A5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_0_0_0_var), NULL);
	}
}
static void ObjectSettings_t2DC1BD2C3C2071E2A6C836E5C43C4D378FC4CC4B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11__ctor_m20153A22BB2F12B09B9DC391189542CFB33D1A22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_IDisposable_Dispose_m8FCA25311749C595C065FAA5F91BEBAA1E6D5426(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_Collections_Generic_IEnumeratorU3CAmazon_Runtime_Internal_Settings_SettingsCollection_ObjectSettingsU3E_get_Current_m0522738F60385992B2C0E82EB07FDC81D1441B9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_Reset_m44794CDDC5D62355A67F666A9C81997DBAFF01FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_get_Current_m40917AA2601E7ECA679D9521E6E2079FDD54364E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CryptProtectPromptFlags_t5D1A609026565A27DCDE1674622D0362ABAE951A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void CryptProtectFlags_tD970CD868F7EC0574E730C7E5243847368E6A243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_CustomAttributesCacheGenerator_U3CSignPayloadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_CustomAttributesCacheGenerator_AWS4Signer_get_SignPayload_mE907B2FB3EF750B065F1C0D454816D9BDCC0A693(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_CustomAttributesCacheGenerator_AWS4Signer_set_SignPayload_mB99439C13DC95FE5F3037223F4D2F0D955C193D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tD423636BD478C42277BC86BE7751B7A5B5E6DD4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t32149D43F679ABD319E8D27FBCE0928550712FAE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AWSSDK_Core_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AWSSDK_Core_AttributeGenerators[936] = 
{
	JsonData_t60FCF27B7D55DE30481BF5C7FC22D05B34D9684D_CustomAttributesCacheGenerator,
	U3CU3Ec_t24000A327F8AAC0CF82858F50F2AEB687554CB8E_CustomAttributesCacheGenerator,
	JsonPropertyAttribute_t0105C7BFD5AD526EA908B0F9EB6C2D7EDE200454_CustomAttributesCacheGenerator,
	LoggingOptions_t0D2EF8CFA44B04A3F804643F3E0B7C916D2725EE_CustomAttributesCacheGenerator,
	U3CU3Ec_t4CD6B238955B57132508DD6AA550F8DE77545B3F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_tCAB3D15743476A4A196D2E161BBEB5D4C22B25F6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass54_0_1_tB77BECAE61ADC193F9140D48E4A7F568A44C093D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass54_1_1_t9D9DA3D17247DEEE2609C174F772C4671D2E7A65_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass95_0_tD97437A21AA3DF88028534BC3399B3173A68C1AD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass95_1_t8A8477D37091446A96A28EEF05D08ECEE88A68A1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t813B290E59288F94D5BA9A78011B02752217BA41_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t1301D624244D6D06A8DD4BD6F964609934C97DB6_CustomAttributesCacheGenerator,
	U3CU3Ec_t53C9F32CD66CD3E13B876A8F1CA21B30638D2261_CustomAttributesCacheGenerator,
	U3CU3Ec_t42EF3E5DD1BDB7B6B560BAB6BC7A3CEEF25C0831_CustomAttributesCacheGenerator,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator,
	U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_CustomAttributesCacheGenerator,
	IClientConfig_t38F64ED77DEB5E7EA3BFF6DCACCB2DE7D00BFB2A_CustomAttributesCacheGenerator,
	HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24_CustomAttributesCacheGenerator,
	HttpRequestMessageFactory_t523C2321C4F177AC497E3C6337EF6B7E968E1942_CustomAttributesCacheGenerator,
	HttpWebRequestMessage_t0836FD55486E3D9263D90C09EF313941DF89D056_CustomAttributesCacheGenerator,
	U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_CustomAttributesCacheGenerator,
	U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_CustomAttributesCacheGenerator,
	U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_CustomAttributesCacheGenerator,
	U3CU3Ec_tB0D11661A398B9230F0E9F816F3AAA033F15DE31_CustomAttributesCacheGenerator,
	U3CU3Ec_t181D379D505CDBA8390F0FB0769870FE540C321F_CustomAttributesCacheGenerator,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator,
	U3CU3Ec_t6C9DD78418BF61F8E3566CB83D5B900BEA088C86_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t63A67453B701F5C1F910CA5FC5934B605214B83C_CustomAttributesCacheGenerator,
	U3CU3Ec_t15DF3D19CDE36F1AE8DA2EB8FDF3A2F98C7C9778_CustomAttributesCacheGenerator,
	ParametersDictionaryFacade_tAB6A1312B817FA9D47CC1707489B9B1D7194E0EF_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_CustomAttributesCacheGenerator,
	U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_CustomAttributesCacheGenerator,
	U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_CustomAttributesCacheGenerator,
	U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_CustomAttributesCacheGenerator,
	U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_CustomAttributesCacheGenerator,
	U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_CustomAttributesCacheGenerator,
	U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_CustomAttributesCacheGenerator,
	U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_CustomAttributesCacheGenerator,
	U3CU3Ec_t4FAE7D91E2A2589B2E870B88D30A3F801EDF5A30_CustomAttributesCacheGenerator,
	U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_CustomAttributesCacheGenerator,
	U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_CustomAttributesCacheGenerator,
	U3CU3Ec_tCC87698EEF5F4D2B6C3ABCF5B59E25BDCD5A31C1_CustomAttributesCacheGenerator,
	U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_CustomAttributesCacheGenerator,
	Extensions_tD20EE6984D3F1330D9B257EDFD79576477E10E56_CustomAttributesCacheGenerator,
	U3CU3Ec_tE4260B2D727D37C5129AD683490F0D7398ABA3C8_CustomAttributesCacheGenerator,
	U3CU3Ec_tCA7C77076A9B4EC73A50376D420A8CB4ACE46898_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_1_t3EA451AC0DDD9C0ED3D317639390BBA5A58AD3B0_CustomAttributesCacheGenerator,
	HttpResponseMessageBody_tF5C8524A8FBBE81FC4774C13D8AA8E7AD6AB4984_CustomAttributesCacheGenerator,
	SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator,
	ObjectSettings_t2DC1BD2C3C2071E2A6C836E5C43C4D378FC4CC4B_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator,
	CryptProtectPromptFlags_t5D1A609026565A27DCDE1674622D0362ABAE951A_CustomAttributesCacheGenerator,
	CryptProtectFlags_tD970CD868F7EC0574E730C7E5243847368E6A243_CustomAttributesCacheGenerator,
	U3CU3Ec_tD423636BD478C42277BC86BE7751B7A5B5E6DD4E_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t32149D43F679ABD319E8D27FBCE0928550712FAE_CustomAttributesCacheGenerator,
	JsonPropertyAttribute_t0105C7BFD5AD526EA908B0F9EB6C2D7EDE200454_CustomAttributesCacheGenerator_U3CRequiredU3Ek__BackingField,
	AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_U3CClockOffsetU3Ek__BackingField,
	AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_U3CHttpClientFactoryU3Ek__BackingField,
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_U3CSystemNameU3Ek__BackingField,
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_U3CDisplayNameU3Ek__BackingField,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_U3CHostnameU3Ek__BackingField,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_U3CAuthRegionU3Ek__BackingField,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_U3CSignatureVersionOverrideU3Ek__BackingField,
	RegionEndpointProviderV2_t65AAD8ABE783A6575B71A33B81359628E132A1CD_CustomAttributesCacheGenerator_U3CProxyU3Ek__BackingField,
	RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_U3CSystemNameU3Ek__BackingField,
	RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_U3CDisplayNameU3Ek__BackingField,
	RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_U3CRegionNameU3Ek__BackingField,
	RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_U3CDisplayNameU3Ek__BackingField,
	ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_U3CUsernameU3Ek__BackingField,
	ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_U3CPasswordU3Ek__BackingField,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogResponsesU3Ek__BackingField,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogResponsesSizeLimitU3Ek__BackingField,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogMetricsU3Ek__BackingField,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogMetricsFormatU3Ek__BackingField,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_U3CLogMetricsCustomFormatterU3Ek__BackingField,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMHostU3Ek__BackingField,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMPortU3Ek__BackingField,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMClientIdU3Ek__BackingField,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_U3CCSMEnabledU3Ek__BackingField,
	CryptoUtil_tC47137C4AADE629CEC6E7078B8A08B8303B4801C_CustomAttributesCacheGenerator__hashAlgorithm,
	EC2InstanceMetadata_t92C7B038B0D3F8AD83A8A95542C4CA590002F0D6_CustomAttributesCacheGenerator_U3CProxyU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CCSMConfigU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CLoggingU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CProxyU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CEndpointDefinitionU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CProfileNameU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CProfilesLocationU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CUseSdkCacheU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CCorrectForClockSkewU3Ek__BackingField,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_U3CUseAlternateUserAgentHeaderU3Ek__BackingField,
	EnvironmentVariableSource_t04DF820E28329CFAB3DFFF134EC4E1AB9804A4B7_CustomAttributesCacheGenerator_U3CEnvironmentVariableRetrieverU3Ek__BackingField,
	SettingsManager_t384AD01E5513FEE12B314542F8C666B286BF8E70_CustomAttributesCacheGenerator_U3CSettingsTypeU3Ek__BackingField,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CEndpointDiscoveryResolverU3Ek__BackingField,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CRuntimePipelineU3Ek__BackingField,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CCredentialsU3Ek__BackingField,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CConfigU3Ek__BackingField,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CServiceMetadataU3Ek__BackingField,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_U3CSignerU3Ek__BackingField,
	RetryableDetails_t30365FED75CE6F46B7D556D855885998D27EFBCF_CustomAttributesCacheGenerator_U3CThrottlingU3Ek__BackingField,
	AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_U3CLastKnownLocationU3Ek__BackingField,
	AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_U3CResponseBodyU3Ek__BackingField,
	AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField,
	AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField,
	AWSRegion_t2D84AE52D83CD4318C4E8DB5F3C22CD6314D91EB_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField,
	FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_U3CAllGeneratorsU3Ek__BackingField,
	FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_U3CNonMetadataGeneratorsU3Ek__BackingField,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_U3CFastFailRequestsU3Ek__BackingField,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_U3CMaxConnectionsPerServerU3Ek__BackingField,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_U3CHttpClientFactoryU3Ek__BackingField,
	ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_U3CAccessKeyU3Ek__BackingField,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_U3CSecretKeyU3Ek__BackingField,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_U3CTokenU3Ek__BackingField,
	CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_U3CCredentialsU3Ek__BackingField,
	CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_U3CExpirationU3Ek__BackingField,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CHeadersU3Ek__BackingField,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CServiceNameU3Ek__BackingField,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CEndpointU3Ek__BackingField,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_U3CExceptionU3Ek__BackingField,
	StringParameterValue_t96B48C9CA9280B1C549BB43C7FFB20216343E1D4_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	StringListParameterValue_t7405475FB1AFE1E7AC8CB5C62759DAF8B4758C12_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CMaxRetriesU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CLoggerU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CThrottlingErrorCodesU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CTimeoutErrorCodesToRetryOnU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CErrorCodesToRetryOnU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CHttpStatusCodesToRetryOnU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CWebExceptionStatusesToRetryOnU3Ek__BackingField,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_U3CRetryCapacityU3Ek__BackingField,
	PreRequestEventArgs_t7F28C1A6A13B2A6A91DDCB8A6EFFFA9FEAD6C659_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CHeadersU3Ek__BackingField,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CParameterCollectionU3Ek__BackingField,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CServiceNameU3Ek__BackingField,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CEndpointU3Ek__BackingField,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CRequestHeadersU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CResponseHeadersU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CServiceNameU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CEndpointU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_U3CResponseU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CUniqueKeyU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CEndpointDiscoveryEnabledU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CS3UseArnRegionU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CStsRegionalEndpointsU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CS3RegionalEndpointU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CRetryModeU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CMaxAttemptsU3Ek__BackingField,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_U3CCredentialProfileStoreU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CAccessKeyU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CCredentialSourceU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CEndpointNameU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CExternalIDU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CMfaSerialU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CRoleArnU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CRoleSessionNameU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CSecretKeyU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CSourceProfileU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CTokenU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CUserIdentityU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CCredentialProcessU3Ek__BackingField,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_U3CWebIdentityTokenFileU3Ek__BackingField,
	CredentialProfileStoreChain_t2F0486F073800BE502EEB9339172AC51CB9A527F_CustomAttributesCacheGenerator_U3CProfilesLocationU3Ek__BackingField,
	SharedCredentialsFile_t5F16CF3C8539649DC88C10EC62C1B0750AE3F1B9_CustomAttributesCacheGenerator_U3CFilePathU3Ek__BackingField,
	RetryCapacity_t4E0969F86179452923C21FA89AC1FB3DDA20E245_CustomAttributesCacheGenerator_U3CAvailableCapacityU3Ek__BackingField,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_U3CRequiredU3Ek__BackingField,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_U3CIsMinSetU3Ek__BackingField,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_U3CIsMaxSetU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CSetContentFromParametersU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CHostPrefixU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CSuppress404ExceptionsU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CAWS4SignerResultU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CUseChunkEncodingU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CUseSigV4U3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CAuthenticationRegionU3Ek__BackingField,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_U3CDeterminedSigningRegionU3Ek__BackingField,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CCodeU3Ek__BackingField,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CMessageU3Ek__BackingField,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CRequestIdU3Ek__BackingField,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CInnerExceptionU3Ek__BackingField,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_U3CStatusCodeU3Ek__BackingField,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_U3CEndpointDiscoveryEnabledU3Ek__BackingField,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_U3CRetryModeU3Ek__BackingField,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_U3CMaxAttemptsU3Ek__BackingField,
	RuntimePipelineCustomizerRegistry_tD94A7CA5DEFCA6F032EEEEFFE1D6601BCE48A67E_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_U3CServiceIdU3Ek__BackingField,
	ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_U3COperationNameMappingU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CMetricsU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CClientConfigU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CRetriesU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CLastCapacityTypeU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CEndpointDiscoveryRetriesU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CIsSignedU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CIsAsyncU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3COriginalRequestU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CMarshallerU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CUnmarshallerU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CImmutableCredentialsU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCancellationTokenU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCSMCallAttemptU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCSMCallEventU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CCSMEnabledU3Ek__BackingField,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_U3CIsLastExceptionRetryableU3Ek__BackingField,
	ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_U3CResponseU3Ek__BackingField,
	ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_U3CHttpResponseU3Ek__BackingField,
	ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_U3CRequestContextU3Ek__BackingField,
	ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_U3CResponseContextU3Ek__BackingField,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_U3COnPreInvokeU3Ek__BackingField,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_U3COnPostInvokeU3Ek__BackingField,
	CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_U3CCredentialsU3Ek__BackingField,
	ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_U3COnErrorU3Ek__BackingField,
	HttpErrorResponseException_tC1C3C48976A8F3328CF4D595A8F7719A8404666A_CustomAttributesCacheGenerator_U3CResponseU3Ek__BackingField,
	HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_U3CCallbackSenderU3Ek__BackingField,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_U3CLoggerU3Ek__BackingField,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_U3CInnerHandlerU3Ek__BackingField,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_U3COuterHandlerU3Ek__BackingField,
	AdaptiveRetryPolicy_t27319F5E3F091520A863559FBF7F427567C3195B_CustomAttributesCacheGenerator_U3CTokenBucketU3Ek__BackingField,
	DefaultRetryPolicy_t6B435B4C8CD1D13531C9DDC6D30A62E49F835DCF_CustomAttributesCacheGenerator_U3CMaxBackoffInMillisecondsU3Ek__BackingField,
	RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_U3CRetryPolicyU3Ek__BackingField,
	StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_U3CCapacityManagerInstanceU3Ek__BackingField,
	StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_U3CMaxBackoffInMillisecondsU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CFillRateU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CMaxCapacityU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CCurrentCapacityU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastTimestampU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CMeasuredTxRateU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastTxRateBucketU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CRequestCountU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastMaxRateU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CLastThrottleTimeU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CTimeWindowU3Ek__BackingField,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_U3CEnabledU3Ek__BackingField,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CHostU3Ek__BackingField,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CPortU3Ek__BackingField,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CEnabledU3Ek__BackingField,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_U3CClientIdU3Ek__BackingField,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CAllGeneratorsU3Ek__BackingField,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CIsConfigSetU3Ek__BackingField,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CConfigSourceU3Ek__BackingField,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_U3CCSMConfigurationU3Ek__BackingField,
	ProfileCSMConfigs_t61934EAD438F3CF47A010D583891DC379530CD18_CustomAttributesCacheGenerator_U3CProfileNameU3Ek__BackingField,
	EnvironmentVariableCSMConfigs_t0762DD2415B673F53C345E6E12328035CB268269_CustomAttributesCacheGenerator_U3CenvironmentRetrieverU3Ek__BackingField,
	DeterminedCSMConfiguration_t21AED8927102140FEFAD6AD2FEEC44D89B5679CA_CustomAttributesCacheGenerator_U3CCSMConfigurationU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CApiU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CServiceU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CClientIdU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CTimestampU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CVersionU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CRegionU3Ek__BackingField,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_U3CUserAgentU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CFqdnU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CSessionTokenU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAccessKeyU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CHttpStatusCodeU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CSdkExceptionMessageU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CSdkExceptionU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAWSExceptionU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAWSExceptionMessageU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CXAmznRequestIdU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CXAmzRequestIdU3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CXAmzId2U3Ek__BackingField,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_U3CAttemptLatencyU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CAttemptCountU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CLatencyU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CIsLastExceptionRetryableU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalSdkExceptionMessageU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalSdkExceptionU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalAWSExceptionU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalAWSExceptionMessageU3Ek__BackingField,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_U3CFinalHttpStatusCodeU3Ek__BackingField,
	CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_U3CAllReadBytesU3Ek__BackingField,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_U3CHeaderSigningResultU3Ek__BackingField,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_U3CPreviousChunkSignatureU3Ek__BackingField,
	BackgroundDispatcher_1_t4309C58161837D4C2ED66BBBC5E12B50DCEA9D51_CustomAttributesCacheGenerator_U3CIsRunningU3Ek__BackingField,
	EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_OnRead,
	HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_U3CAlgorithmU3Ek__BackingField,
	HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_U3CCurrentPositionU3Ek__BackingField,
	HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_U3CCalculatedHashU3Ek__BackingField,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_U3CDeclaringTypeU3Ek__BackingField,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_U3CIsEnabledU3Ek__BackingField,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_U3CArgsU3Ek__BackingField,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_U3CProviderU3Ek__BackingField,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_U3CFormatU3Ek__BackingField,
	LruCache_2_tE3253D91CEC5F96B76E3E2D86DFB2CD7F28D5572_CustomAttributesCacheGenerator_U3CMaxEntriesU3Ek__BackingField,
	LruList_2_tB7EF1BC2EB94A00EA0AC42D4E0393E30EC78905D_CustomAttributesCacheGenerator_U3CHeadU3Ek__BackingField,
	LruList_2_tB7EF1BC2EB94A00EA0AC42D4E0393E30EC78905D_CustomAttributesCacheGenerator_U3CTailU3Ek__BackingField,
	LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CKeyU3Ek__BackingField,
	LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CNextU3Ek__BackingField,
	LruListItem_2_t9D293C6F99F887120AD00FDBE2F85E0CB957E42A_CustomAttributesCacheGenerator_U3CPreviousU3Ek__BackingField,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CPropertiesU3Ek__BackingField,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CTimingsU3Ek__BackingField,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CCountersU3Ek__BackingField,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_U3CIsEnabledU3Ek__BackingField,
	Timing_t4D1C5000BC2696A4879EB7862DED30F6882A453E_CustomAttributesCacheGenerator_U3CIsFinishedU3Ek__BackingField,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CMetricU3Ek__BackingField,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CMessageU3Ek__BackingField,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CExceptionU3Ek__BackingField,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_U3CTimeU3Ek__BackingField,
	WrapperStream_t4676047E9C65B07194C4FF80564CF1077A7550CA_CustomAttributesCacheGenerator_U3CBaseStreamU3Ek__BackingField,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_U3COriginalContentsU3Ek__BackingField,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_U3CFilePathU3Ek__BackingField,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_U3CLinesU3Ek__BackingField,
	ProfileIniFile_t9CA7AFA6EB1CC84E3A7D05997EA461D7E67335A1_CustomAttributesCacheGenerator_U3CProfileMarkerRequiredU3Ek__BackingField,
	ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_U3CObjectExceptionU3Ek__BackingField,
	MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952_CustomAttributesCacheGenerator_U3CRequestU3Ek__BackingField,
	JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_CustomAttributesCacheGenerator_U3CWriterU3Ek__BackingField,
	PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_U3CSegmentTypeU3Ek__BackingField,
	PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CMaintainResponseBodyU3Ek__BackingField,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CIsExceptionU3Ek__BackingField,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CCrcStreamU3Ek__BackingField,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CCrc32ResultU3Ek__BackingField,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CWebResponseDataU3Ek__BackingField,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_U3CWrappingStreamU3Ek__BackingField,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CStatusCodeU3Ek__BackingField,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CIsSuccessStatusCodeU3Ek__BackingField,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CContentTypeU3Ek__BackingField,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_U3CContentLengthU3Ek__BackingField,
	PersistenceManager_tB518F8E4B3AEF363860FD6FBEB48AE7E0F51B290_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator_U3CInitializedEmptyU3Ek__BackingField,
	AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_CustomAttributesCacheGenerator_U3CSignPayloadU3Ek__BackingField,
	JsonPropertyAttribute_t0105C7BFD5AD526EA908B0F9EB6C2D7EDE200454_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_Required_m13B37FCCC7DA7AA77F335DEFAFBED5BAAA93C670,
	JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_CustomAttributesCacheGenerator_JsonWriter_Write_mC5058DAFD1A17F59EF9C406C354D29808D3BEA4B,
	AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_AWSConfigs_set_ClockOffset_mA16DF92F5F2A10BD24A5D52C01D81ED888A868FB,
	AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_AWSConfigs_get_HttpClientFactory_m78229ECA292AF41D8A1D1DC094725778383DF561,
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_get_SystemName_mCA2FF58E84D00596AC3250F797AD2AC3B783FF9D,
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_set_SystemName_m98B17B4109A3DFB6C723FC34D060297FA1ECBBB6,
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_get_DisplayName_m9F7BF40A023CC420766759C7643189309742A812,
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_CustomAttributesCacheGenerator_RegionEndpoint_set_DisplayName_m7DDC78786C489CAC929012EF3FA130015572EF42,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_get_Hostname_m68CD8A74E6E7DB12FC54D991176DC12D6A0C4925,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_set_Hostname_m25D4F046B53E36CA01DE38ED67B854336395EB0B,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_get_AuthRegion_m342C6FEF878A06504E6822261A6752BF328879AD,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_set_AuthRegion_m5ABD1D4B3686C2C959AB1FCFF0120D66CBDA2554,
	Endpoint_tCF76BDD3C744BD0DD3AACF3A1DD800BBD1D75DB9_CustomAttributesCacheGenerator_Endpoint_set_SignatureVersionOverride_m67F109FA152FA50029D59E3019A642326EE6C3A4,
	RegionEndpointProviderV2_t65AAD8ABE783A6575B71A33B81359628E132A1CD_CustomAttributesCacheGenerator_RegionEndpointProviderV2_get_Proxy_mF94D500F8D838A0A41E557F8F78E85E1881CBBE7,
	RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_get_SystemName_mA927AC85ACE3E746C6BCEE3F85AFA08491345AB5,
	RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_set_SystemName_mE32DA2C28922213E460BF7B162452D653DA98E6F,
	RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_get_DisplayName_m8CBFE99744D4B800EE260CE6E770C884892ED534,
	RegionEndpoint_tA823D347F835867FA4BA8770A06887F7B908EA62_CustomAttributesCacheGenerator_RegionEndpoint_set_DisplayName_mDDFA9C6B58D6D1331F5739BD546588A6B8E18B16,
	RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_get_RegionName_mB7CEB1A08A15E5A094FB058980751AD56B028974,
	RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_set_RegionName_mBC114B5247B46D3F873AA93606B19883D91C6EFA,
	RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_get_DisplayName_mD3913EEEF925BD31703A43466C887E014E98A673,
	RegionEndpointV3_t84A10994AD0D3C0EB1AEF0207DD22A4EA17F8FF6_CustomAttributesCacheGenerator_RegionEndpointV3_set_DisplayName_m81A67F18FA5E4D9529CFD7D6773142BCC5F9F55A,
	ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_ProxyConfig_get_Username_m81955ADE534F7677180E2939A6C3D1AF0A2228FC,
	ProxyConfig_t3E83F09EB0B96BD2A431C14C2A86572B111B0F01_CustomAttributesCacheGenerator_ProxyConfig_get_Password_mD09CFD5FB9141132944E25757C50D5C421C20E51,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogResponses_m9F1FA413F3A9AF41C64AC4B219798FA570D18725,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_set_LogResponses_m0E16D7CDB69C7EEE2994914B6D34B24A62C898F3,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogResponsesSizeLimit_mAEF0745D43816A42B5D80B1DE17380FBB14EAF16,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_set_LogResponsesSizeLimit_m7E62A8B361B8EB37305DFFAF37FF0AF6F9D3D0B6,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogMetrics_m47AF794FF34CA33B117B030A865695FD0FB42000,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_set_LogMetrics_mE4CFC322110C8D56EA52CECB88D69FA8BD560FE4,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogMetricsFormat_m5C2206F419BF88D2F401258F965407456E4DD30E,
	LoggingConfig_tAC32C93334D8AD854E9C29C2015F8487ED70D3F3_CustomAttributesCacheGenerator_LoggingConfig_get_LogMetricsCustomFormatter_mEFF5F77802440902877DF50BC11721A69FFB2E24,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMHost_mCD6A4223BC46188E253784DD6811D36F1022314B,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMPort_mB95D7D19420DAAC48311A4EC703E1A6F34E0AC44,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMClientId_m6994559572B87F47F0B6E55E46367E9F2490E8F7,
	CSMConfig_tC879A6402C1E78B853ADEF88FC617B4896AAC6D4_CustomAttributesCacheGenerator_CSMConfig_get_CSMEnabled_m04F6AA3E68C62699639115DF7BC9FEA121AAB165,
	EC2InstanceMetadata_t92C7B038B0D3F8AD83A8A95542C4CA590002F0D6_CustomAttributesCacheGenerator_EC2InstanceMetadata_get_Proxy_m44E9E5E80195713A75A93A64D56080954ABC18C6,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_CSMConfig_mE24357A51A72DC9C705EA286180502BD8715A423,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_CSMConfig_m98B7688DFE7FAD711BFA1555380BB9B3BAE1F278,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_Logging_m4625DDD282B9B05DA95A92741359BE067FD2D6E8,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_Logging_m5E9E2542873931F0E929611F21B9A99BDD56FD9D,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_Proxy_m9E0480BE4E106446353942289AF7D2BB178B7162,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_Proxy_m1BE8FC28EE2B8C93A2E58C612C8B8212C8B5468F,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_EndpointDefinition_m3204E79A2D482EC5614416625F1BF0D9B4AF1DD9,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_EndpointDefinition_m6535665BFD93C547251D39119AED42E63366337B,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_Region_m284EF44B24F8EBB9672322B91B4134FFCFDF825F,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_Region_m453ED10087DC4B2A11408936814FB6142970CFC0,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_ProfileName_mF622E8B2F448542B28DC0EEDFC59244BAB988205,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_ProfileName_m22D6F68E19AFBE2F41CEE81A06CB64C0775614E3,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_ProfilesLocation_mF3FCB10B8A02626FFEBAE7F4A3DE96637840BEEE,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_ProfilesLocation_m100049E9E25819ED7EB08FE271E9E98639557F50,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_UseSdkCache_m66A052C98A721A3B7F2CF81F4A812F321A1E0125,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_CorrectForClockSkew_m7AFB4F0C317A4C4FD94D3AC49C58E5131099CF49,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_set_CorrectForClockSkew_mFCBFE1909CD693B83D6DFCE3DC3018A0FB89C19B,
	RootConfig_t8DBAE41C0294AF3EC1DE8D74E9DA6A9C41AE8E7C_CustomAttributesCacheGenerator_RootConfig_get_UseAlternateUserAgentHeader_m7F2DA7D47D1FB6C0BD7A6CF6F022500B0292FEDE,
	EnvironmentVariableSource_t04DF820E28329CFAB3DFFF134EC4E1AB9804A4B7_CustomAttributesCacheGenerator_EnvironmentVariableSource_get_EnvironmentVariableRetriever_mECF810C4761E79E35D61A8B5932F374F49CB788E,
	SettingsManager_t384AD01E5513FEE12B314542F8C666B286BF8E70_CustomAttributesCacheGenerator_SettingsManager_get_SettingsType_m30F81BBD2789713AC9887E5025D938E982B2A0E8,
	SettingsManager_t384AD01E5513FEE12B314542F8C666B286BF8E70_CustomAttributesCacheGenerator_SettingsManager_set_SettingsType_m5960C66BF818EC32834889E87F11AAA9CEE406D4,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_EndpointDiscoveryResolver_mD8ED6AD49FEAB0E7ADFE0D6F14B1C3624DE8EB21,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_RuntimePipeline_m666BA08F0997534D6F4D81431D5F34CB684050D5,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_RuntimePipeline_mE11F53789AF484FEFB2A831E18E1FD7CD9F205A8,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_Credentials_m080C193E757F0C6C3C6FACA2F5C7A4EE667CB1C0,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_Credentials_mB189B6BAC7FE34A3FD8B134C703CFB81E2F10FF2,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_Config_m7896DAF638A60C7D0CD95F2C92F7C95429A00FCF,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_Config_mF980BE678237045A0E7CE73D46170D67D8C1A38F,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_ServiceMetadata_mF868195EEB804B404AAE5B09B3B106115487C603,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_get_Signer_m67573336045A226123F21A0BD07C7248EFF6A481,
	AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB_CustomAttributesCacheGenerator_AmazonServiceClient_set_Signer_m8D3241409ABFDDCF5465DD065A31DB72C162DFFB,
	RetryableDetails_t30365FED75CE6F46B7D556D855885998D27EFBCF_CustomAttributesCacheGenerator_RetryableDetails_get_Throttling_m7170652F32CB321788E87CF4ECE2C480575A1501,
	AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_get_LastKnownLocation_m125EB29655724CC65286914D205C57131C22B98B,
	AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_set_LastKnownLocation_m4EE878DAF0AE3328B94919195FEAEC0DB77BBB5A,
	AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_get_ResponseBody_m9D8553A581FAF5F4CCDF36335644A495DCAE109C,
	AmazonUnmarshallingException_t48B3343D96D74E6941ABC30B040F26146DD567B0_CustomAttributesCacheGenerator_AmazonUnmarshallingException_set_ResponseBody_m40F84E8996C657ADA6F77DAFB15E1E6F9F901348,
	AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_StreamUploadProgressCallback_mE0AF7FC0693F8A85D1FC3869A412CC5A10CAA177,
	AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55_CustomAttributesCacheGenerator_AmazonWebServiceRequest_Amazon_Runtime_Internal_IAmazonWebServiceRequest_get_UseSigV4_m412C582CD42A6D7E416C3272CDD244B1BEF79AAF,
	AWSRegion_t2D84AE52D83CD4318C4E8DB5F3C22CD6314D91EB_CustomAttributesCacheGenerator_AWSRegion_get_Region_m8917D9F0C32A90ED6803DD6035A7044346D3D3A4,
	AWSRegion_t2D84AE52D83CD4318C4E8DB5F3C22CD6314D91EB_CustomAttributesCacheGenerator_AWSRegion_set_Region_mB70ECDCFDD779661E7DE6617C4E81A3A8BE3EFB2,
	FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_get_AllGenerators_m91FD3D6C134BB04D7FBB03849689A828C3AC0C44,
	FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_set_AllGenerators_mD213A68B42DCC5793C7423C4B5C5564E78CAEF46,
	FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_get_NonMetadataGenerators_mF1FA51D6894FF14E2917C483BDA3FE7855C42CE9,
	FallbackRegionFactory_t19395CDCB0233EB4137EFA3F30F27E0E8301030F_CustomAttributesCacheGenerator_FallbackRegionFactory_set_NonMetadataGenerators_m515F25404FC75B4E202732360614A438D7EE04B5,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_get_FastFailRequests_m2DB2B6D28509361EB03619C7EC8877029EB33316,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_get_MaxConnectionsPerServer_m62F57F07092DFB2641B38B93452E6CF8837C40A2,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_get_HttpClientFactory_mDDFE40B1D7108CEF000727555013D413D998F732,
	ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_CustomAttributesCacheGenerator_ConstantClass_get_Value_mD7D685AF95A226A4BB1990F8EA037319629925D1,
	ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_CustomAttributesCacheGenerator_ConstantClass_set_Value_m6780475A4086C5357CA1146CDE36646336E3D958,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_get_AccessKey_m509B5AE44459C85FBEADA67014CF2921BC8893C4,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_set_AccessKey_m04293A46CAAE7BEAF677E502A611FDB32A449D56,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_get_SecretKey_m8277ED2C42DC5F2180CFCC7C142DB6ABAB12BFAF,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_set_SecretKey_m71E51B0FB54A61F43B8C6CCA0B5D78B94BE74F63,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_get_Token_m084E4F55E325D68EBBE74EDAEEF101E29D6748E4,
	ImmutableCredentials_tAB71E38D182E39A4DC56F581DF07B6D8731CC90A_CustomAttributesCacheGenerator_ImmutableCredentials_set_Token_m85A958A0783A5889E0F7429C0E8D624E0D70A7B9,
	RefreshingAWSCredentials_tCB6E7C1C325EC27C7094A39AA68494071564423E_CustomAttributesCacheGenerator_RefreshingAWSCredentials_GetCredentialsAsync_mB595D535B6BFD152A8051EAB4C2C297BCEE6B7EA,
	RefreshingAWSCredentials_tCB6E7C1C325EC27C7094A39AA68494071564423E_CustomAttributesCacheGenerator_RefreshingAWSCredentials_U3CGenerateNewCredentialsAsyncU3Eb__16_0_m0FC03D1BEEF1A6854CDAE4736471A16EDB04A301,
	CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_get_Credentials_m6C85D5BD9D58FEB3DE818A71424499EF455DADB2,
	CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_set_Credentials_m107E3DADE4BFD6157B147BE7438BD649F1A2AD01,
	CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_get_Expiration_mFA7346BBB61AEDB3A90A8012518B54433EF7B17A,
	CredentialsRefreshState_t051797BE4BC4135D651B9DC73F8BBC57DAB9F842_CustomAttributesCacheGenerator_CredentialsRefreshState_set_Expiration_m1D4B0D4D19FEC97F0DD52569E60D4AF367A481D5,
	U3CGetCredentialsAsyncU3Ed__10_t1E870876D1562C4B4A47E2049F4F2768568339E8_CustomAttributesCacheGenerator_U3CGetCredentialsAsyncU3Ed__10_SetStateMachine_mFA86F065CDB16A8B134F7E86E7E51EFEBE33081E,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Headers_m308C72B975C6934CC942A2690D429FC1544C04AA,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Parameters_m5E27A0995E6B14D58130418E0563E57BE1327AA1,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_ServiceName_mEEA7C88A76BB3147789C6D9B9CAE7B45F5B3F5DB,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Endpoint_m753A6846890C931016AB3E9746D32CC29A3C7499,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Request_m975F088853F42DBE16625E28A06C8BC3F6B61631,
	WebServiceExceptionEventArgs_t2732C6B3CD5E55B367057EA59A7BB675AB32130C_CustomAttributesCacheGenerator_WebServiceExceptionEventArgs_set_Exception_m670BD3838487CD3BC56C8111A55DBAF0B6AB98BC,
	StringParameterValue_t96B48C9CA9280B1C549BB43C7FFB20216343E1D4_CustomAttributesCacheGenerator_StringParameterValue_get_Value_m6805935D4A23333C6204D77BC47A40B5780B2F01,
	StringParameterValue_t96B48C9CA9280B1C549BB43C7FFB20216343E1D4_CustomAttributesCacheGenerator_StringParameterValue_set_Value_m2BDDB0D689259FFAD473566188DD655BB972ECD1,
	StringListParameterValue_t7405475FB1AFE1E7AC8CB5C62759DAF8B4758C12_CustomAttributesCacheGenerator_StringListParameterValue_get_Value_m8E742CAEA3583E1393F735531020B553C977A593,
	StringListParameterValue_t7405475FB1AFE1E7AC8CB5C62759DAF8B4758C12_CustomAttributesCacheGenerator_StringListParameterValue_set_Value_mCBB88C38BF207C8182ABC8B9F2B3AEFA3C8E3D16,
	HttpWebRequestMessage_t0836FD55486E3D9263D90C09EF313941DF89D056_CustomAttributesCacheGenerator_HttpWebRequestMessage_GetResponseAsync_m093528F50DB252F5D33A06C9F20250CE885DE094,
	U3CGetResponseAsyncU3Ed__20_t29F1FA8567DBED8334CDE2F0FAE7CF5A611730B6_CustomAttributesCacheGenerator_U3CGetResponseAsyncU3Ed__20_SetStateMachine_m10865D2EFB43097DB415EFB5DB496B817C850ED5,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_MaxRetries_mA76CE36A06EFC6A3935BDB64429EA5B6C596BB53,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_set_MaxRetries_m36D4FB81E32CF4F8465CF6094FA4743C9D37F273,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_Logger_mE0AF8B9D9FC0BD25BBBAE14A26ADF916FF5610F2,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_set_Logger_m68A68C566F9C95673F60129AB1BD87D1EEC8E5A4,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_ThrottlingErrorCodes_m2CF20C9AD038CD8F4AA224BDB68351B424A3ED6D,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_TimeoutErrorCodesToRetryOn_mD151D14490561845F443491871D89725648AFFDC,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_HttpStatusCodesToRetryOn_mA009A938783A07A9CEB7717806D9967308C5CA5F,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_WebExceptionStatusesToRetryOn_m329A25793DD3B758615EA509FA6B03F6A120C330,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_get_RetryCapacity_m63B7EA20A13720F8E9A9C5C82641A54CEC0E97D3,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_set_RetryCapacity_m088CC1BB9F5588B6D254B74CFA9F6533E214CA7C,
	RetryPolicy_t4BD5215807D90CEB6454D82F4C3AF78ED90A5B32_CustomAttributesCacheGenerator_RetryPolicy_RetryAsync_m3D741AF314A36B2C421CD7717A0A4098F6DA88E4,
	U3CRetryAsyncU3Ed__57_tE2357B52617EC70D3C7BC85C1C022BCAD2D78D3A_CustomAttributesCacheGenerator_U3CRetryAsyncU3Ed__57_SetStateMachine_mF497DCFE92BB472842C19A106F09007A860FFF71,
	PreRequestEventArgs_t7F28C1A6A13B2A6A91DDCB8A6EFFFA9FEAD6C659_CustomAttributesCacheGenerator_PreRequestEventArgs_set_Request_m913512B993407176BDE17EED7CCDB3B8EA466979,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_get_Headers_mFD75BFBB8C1CDC475FD07A2CA94054C8A7BF94F5,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Headers_m4C4474DD9D0B6975CEE99E3ACD66C38585195278,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Parameters_m304A0C4EF2B41D03A1D35C78411F1240BDE41606,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_ParameterCollection_m63448435DC566936F2FACB1C97C9DAD1963E2F65,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_ServiceName_m0A8B8B8C719817D7B4AC4CC873E84672367B7692,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Endpoint_m719E6225716C3AD740266C4DB15DDA9F11CD59A0,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_set_Request_m2E93015C782370542C385BF39139235D647C81B6,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_RequestHeaders_m0F160670222462F92D8AC6B4E49400F2C5098EAC,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_get_ResponseHeaders_mB3B05CF7F3CA81AD0E15756A6F1785D33BE103DA,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_ResponseHeaders_m437D99FE61C57DC60BFABE2FD4D5FD1FB6675C5F,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Parameters_m35607FF08CF6574B51713FA9C4B042C9C70DDFA3,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_ServiceName_m08E80F5EC7828B0B97D34DA6C95E64A34531EA2C,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Endpoint_m77D2AC3E87BB0C1997933A63273252B383496281,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Request_m74545AC00FF2931AC9D2FEF9F6AB7359459E0722,
	WebServiceResponseEventArgs_tD13B958627B2D9107B58D25AAA0EDFBCA4CE4C30_CustomAttributesCacheGenerator_WebServiceResponseEventArgs_set_Response_m6B8ADF83266A32FAB4DD7EF7C8494BF4E807B251,
	MonitoringListener_tFD9A684F08F51994C058855FD9498C3FB667EC9C_CustomAttributesCacheGenerator_MonitoringListener_PostMessagesOverUDPAsync_mF045DACA39A35CE18A8D5C15D023D0389686A072,
	U3CPostMessagesOverUDPAsyncU3Ed__10_t220FF8270E5726CB83332084513840009530AB4F_CustomAttributesCacheGenerator_U3CPostMessagesOverUDPAsyncU3Ed__10_SetStateMachine_mE5C9EEDE31FEEC56BCF88F148813303D2AA51CAC,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_Name_mB713E06DF9022D5FFE37AD44EB9CCF31B82F60D3,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_Name_m63BC3AD462D0CF3669C11B75A37B3A72BB68FFDF,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_Options_m4BEDDAF9941BA979CC731717C17A3FA7623749D3,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_Options_m7651DA7DA24A064F6178A6E6EC92DCA807641CFB,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_Region_mE253F69F7886AD25DA66A653E39109186664EC95,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_Region_m44E8C475F95D6C64B4AF534CA00C14267C09A795,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_UniqueKey_mB5E4CE2360F0A7A4ED514F50670DF31C51A7BDFB,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_UniqueKey_m0EA7516DBFD617FF619D864790F48CC9A653B3DA,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_EndpointDiscoveryEnabled_m3DD6687CA952054E30AB5E108C586BDC1A2493DC,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_EndpointDiscoveryEnabled_m266929EB8A3C01BBDB26483D547F0E2113AC7E97,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_S3UseArnRegion_mD66A32EE76676B6E0D9A398A0D6643F4ED720CCC,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_StsRegionalEndpoints_m629728B051B1A50E72E33D61D6E94DDC7A162A26,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_StsRegionalEndpoints_m7AD9C668F3A944E2CBD5A358F3A6918E996F9CD7,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_S3RegionalEndpoint_m54FD9379579C26441119FEBBCB3B3F0F21B1DC09,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_RetryMode_m73DB6E189368FE3F055AFEAA9F924EBED95D3A08,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_RetryMode_m244DD300B11793D33F00593DD421B8598F1050AE,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_get_MaxAttempts_m3AB54AC420F35373634610FAAE1C62EFDEB190B5,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_MaxAttempts_mF8CFD52861CBC44DCA46556C0E43E68F0FF890A7,
	CredentialProfile_t3BFE9CA041274644AEAA5ACD8FB4B0C3C2B25F32_CustomAttributesCacheGenerator_CredentialProfile_set_CredentialProfileStore_m8EA2A883DECA4473D43CE84A3DC15EA7EFD49097,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_AccessKey_m54D2DE99D522F53500F3CD0FD9388812C2AF6BD3,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_AccessKey_m8F3AD526CCB0CC1DAD563F0364ADEE05A91B85E9,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_CredentialSource_m1D564B618015AF6BC155329EF0EE4D6DF8C16A52,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_CredentialSource_mE1EF94AB93F049279711F301C2DAEF14F149C0A6,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_EndpointName_mA13C59933C46404D3017FCB826EAC2DDBD5E7E8D,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_EndpointName_mA51B88631A57B8BD75F330661072C58FB1961D8C,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_ExternalID_m64A40A52D928228D64CD8C7B10DBF3101E5CF033,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_ExternalID_mCB2664BFB5E632389C706417724A2D52BBBDE95D,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_MfaSerial_m9F3C36B9F19852F4EC2BDA5C61CB7700160053C0,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_MfaSerial_mA43E21320CC090A5D1F2D2EE9C763ACEDBCE5459,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_RoleArn_mD33C1E609E6DC33FCBA1F100DB756B3CE18F6CD0,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_RoleArn_m3705C867F98614A1B66A34EDADBBA009690EEE3A,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_RoleSessionName_m59225087CF2D7BB1A79F122FBB9C3D6DF136F5F3,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_RoleSessionName_m31748E87FCE77B513CBCB3D9C8BB9B692493819E,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_SecretKey_mE9227433E5F455B2B3CCAB940DF1EF346D587439,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_SecretKey_m3D8CC902AD4C18EBE5F63747675EDDD4536AB7AD,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_SourceProfile_mCF1A8615E110EA66E829E79313B1AA7BAD904D36,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_SourceProfile_m9E621D9FD6D7AB1FDD6933D6FC46CC1CD02DB6B8,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_Token_m67BF5840EAD9E17CF3B5741B23F446A8D568F1F5,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_Token_m9FD9FD44198997D786FF87FE35AF41A0D6BB70BB,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_UserIdentity_m4239DE2133B4968497CE6EA73849891F2B1797B7,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_UserIdentity_m90374CF6A6BB801369B7089063281017876E835C,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_CredentialProcess_m06365B5DE3931629A94EAFE9651915EF32DCE968,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_CredentialProcess_mA6FD2353B05476E70C062F1C9B9A3A0E2A493ED6,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_get_WebIdentityTokenFile_m816A1C6AB30B2CF7F3D8C564558BFFDF5C357F21,
	CredentialProfileOptions_tFCFE5746C07504107CB233256AA45AAC22D29329_CustomAttributesCacheGenerator_CredentialProfileOptions_set_WebIdentityTokenFile_mA959CF718B74550D6CB663BD0880B8A635F4A742,
	CredentialProfileStoreChain_t2F0486F073800BE502EEB9339172AC51CB9A527F_CustomAttributesCacheGenerator_CredentialProfileStoreChain_get_ProfilesLocation_mA618D2CDAF7EBE2C16FFD0F2BB91B7449C0AFA90,
	CredentialProfileStoreChain_t2F0486F073800BE502EEB9339172AC51CB9A527F_CustomAttributesCacheGenerator_CredentialProfileStoreChain_set_ProfilesLocation_m58C470C2CCA2358F7B72478DA1FDE835881E8A77,
	SharedCredentialsFile_t5F16CF3C8539649DC88C10EC62C1B0750AE3F1B9_CustomAttributesCacheGenerator_SharedCredentialsFile_get_FilePath_m13D49F7E2F5200941148407607A118BA8BD85596,
	SharedCredentialsFile_t5F16CF3C8539649DC88C10EC62C1B0750AE3F1B9_CustomAttributesCacheGenerator_SharedCredentialsFile_set_FilePath_m03BAF421690A3CECA50363586E082EDC0F03901F,
	RetryCapacity_t4E0969F86179452923C21FA89AC1FB3DDA20E245_CustomAttributesCacheGenerator_RetryCapacity_get_AvailableCapacity_mCD9C04903EC7590720F4C00841A1A69DF0628611,
	RetryCapacity_t4E0969F86179452923C21FA89AC1FB3DDA20E245_CustomAttributesCacheGenerator_RetryCapacity_set_AvailableCapacity_mF627A8F4B70ACE8DFB8B93C56CB21DDDAD273261,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_AWSPropertyAttribute_set_Required_mB56403323E8D1876EB7CC34B8522B7101A5011F6,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_AWSPropertyAttribute_set_IsMinSet_mF2D9D9E5E5A2AFB8E96933B9A7D1F36BBF0ACAB1,
	AWSPropertyAttribute_t6FCAE5FAD002733C1B098703358BCB89F42376B9_CustomAttributesCacheGenerator_AWSPropertyAttribute_set_IsMaxSet_m098F9CAB1E60CBA824EF649B7AF3F4180F08A0A6,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_SetContentFromParameters_mB03C1E564A7BB85E7B1840F5EE7918AEBE977309,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_SetContentFromParameters_mD83CB98B12FDC98258DDBE891F195215600A56DD,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_HostPrefix_m10D9DC3D8FB879A77D66612DABA751217A92A03A,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_HostPrefix_m6E4E12A783326EF99A979ECF1992D7C4AD28E1B5,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_Suppress404Exceptions_m9EBECC5B37C54D8027C015CE9A8E4C8714E02398,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_AWS4SignerResult_m2E075FE2E9038A89E9EB365206B9101FA627EF8C,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_UseChunkEncoding_m5375AF8E3FF31E65C632F3DEAD6E859885ECF329,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_UseSigV4_m4CA850E5CD4FB0F28EF2408B6D443B731AF10C7D,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_AuthenticationRegion_mEC4FD9325E607789DA3E0AB657F4997D1EBDCFFA,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_AuthenticationRegion_m7390CFC4C888DA4876576238BF48C2E5884028D4,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_get_DeterminedSigningRegion_mF6982E156E6F30E8611F4672B86241104D76C8DD,
	DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_CustomAttributesCacheGenerator_DefaultRequest_set_DeterminedSigningRegion_m3DF45EF335C58F20A251970CE54E8E61C842AD38,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_Type_mE53555DD966531864E2D76017ACFB4BEBBACBBDC,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_Code_mB45D11E4AC2905CC68C3703BD276AA7D7FF352C4,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_Message_m7C450B5EFD404E19C84070BF859275A7F95C595C,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_RequestId_m7469C3C6D31632D79BBB4C5A6AB456D311D0FD41,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461,
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_CustomAttributesCacheGenerator_ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_get_EndpointDiscoveryEnabled_mCE81E1B1BC2AD1FA28359A0C9132350EF0A94D07,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_set_EndpointDiscoveryEnabled_mDE3516A6EB931D9DE0ED43832E3CD9D32F4E0AF3,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_get_RetryMode_m85D04441ACF031F843F40340FE4D2B269735297E,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_set_RetryMode_mAC0C563C32C2EC4FB8005DE17A71A95DE47907C4,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_get_MaxAttempts_mA1F79F4677829F49F656D48A56F8E012B27BEBF2,
	InternalConfiguration_t60D4BEE9FF2AF7AF801BD810EDF0C860527BB7F8_CustomAttributesCacheGenerator_InternalConfiguration_set_MaxAttempts_m16CC99498CF834DD7FBF28FF931D323333B44775,
	ParametersDictionaryFacade_tAB6A1312B817FA9D47CC1707489B9B1D7194E0EF_CustomAttributesCacheGenerator_ParametersDictionaryFacade_GetEnumerator_m8106C6F93EFBE6BBC5E921ACFBDCD8198F97D8C9,
	U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23__ctor_mF2A47390FF6A0D9DF41AEDCF316A05038745DAD3,
	U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_IDisposable_Dispose_mCCB562FCB94036DF98F8DDD82D0CEBC92739E7D0,
	U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mB770895B34BB9A97BDD81972BAA878115A963E80,
	U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_Reset_m7CD8581DD59E442903A7DEA8F5A7A6019A5FD816,
	U3CGetEnumeratorU3Ed__23_t98794B90E876D94AEDD743553428BF44DCF5EB9D_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__23_System_Collections_IEnumerator_get_Current_mE07999BA03923739E386C7B5BD271FC010E714A8,
	RuntimePipelineCustomizerRegistry_tD94A7CA5DEFCA6F032EEEEFFE1D6601BCE48A67E_CustomAttributesCacheGenerator_RuntimePipelineCustomizerRegistry_get_Instance_m0FCF38817786A1D0F32D0DC785EB0A056AA6F060,
	ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_ServiceMetadata_get_ServiceId_m42AA96D72D2FD71B2EC4A9B4B33DC1B9A835A4AF,
	ServiceMetadata_tD74B346ABD44BD5B38F7E6EF365CC3C77F89A2F7_CustomAttributesCacheGenerator_ServiceMetadata_get_OperationNameMapping_mAE5C256DC7C08383E6FCF215B18ED0D477D2DAD9,
	ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3_CustomAttributesCacheGenerator_ParameterCollection_GetParametersEnumerable_mFD6A666A3B962D2B53212BAFB5D24132C33A21DC,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4__ctor_mA3660F51DEA5FCCA18250DF86B1E4228CE3004BC,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_IDisposable_Dispose_mB5E20B4637F27495FBB2EF920877949FCA12FFD1,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mD78AC053140A1678AA5390E3B84194BA638B732E,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_Reset_m440496B3B80A3C4CA4E26640D5297498FDABED23,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerator_get_Current_m4B1FA65DB4B72667C4995D7FA54F47701A8EF03C,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m8556E7C7BFD90DCA9FB7837CB5D1FBC3C2F22D6B,
	U3CGetParametersEnumerableU3Ed__4_t6A8027859F356B7A290D4CDDBCE8F64D1FBFCF03_CustomAttributesCacheGenerator_U3CGetParametersEnumerableU3Ed__4_System_Collections_IEnumerable_GetEnumerator_m583C549C1976AF018F67004591B37239E0FCA2EE,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Request_mA4471DE841DE086DCA9A7949E3E906BD9E8EA7BF,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Request_m4D9502541AA3E426E0AD100D820F57097F0F3359,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Metrics_m8245B5AD45A4136FEA4A41FD0E9AE03BF910137D,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Metrics_mFBEC125FE4162520753F26D54BA2ECEE87D7173F,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_ClientConfig_m00A68FB365505864D204F7D9F0B4B3B0CA440976,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_ClientConfig_mB9CCDA771A29F9C7765C9AF67EAC5A88646874E0,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Retries_mB59731D9636F968BAA1706BDC5C07626A3FE1219,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Retries_mD88CBDC3B30D3A4BAA83FBE3BE13A6A977F378E6,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_LastCapacityType_m0B7619EE743164D5DC511D6A1DD843A467A8C7DF,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_EndpointDiscoveryRetries_m98C8DB853AA051758AC68367C68DF25D71A7E5A2,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_EndpointDiscoveryRetries_m76846D7FF5CAA564393ADB168341B125C1C23C79,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_IsSigned_mC3D6CB04C958B62F4D4423D216C4B01B8D43089D,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_IsSigned_m92398DD2A210E0A2E98BE81442B7BEAA1F89AF82,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_IsAsync_mE367ACF5C06381343028C5A45BDDDFAEF3164912,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_IsAsync_m9F1AF9842CB659049A616F2A850C6A594490C9AE,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_OriginalRequest_m76BC7D9FCAFA242D20FE9834A4E18D6F921ACF8D,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_OriginalRequest_m5D45287D4130E982EB4EF26BEBEC1ABF28B4DEBC,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Marshaller_mE77303460FA245309D9033950B4594DFCC9D5F2F,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Marshaller_mBC9EF52693055C503B97AA6E185EEB5FE965CDE3,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Unmarshaller_m5A458EBF82A9432CF3C2614970F4A40960CC8116,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Unmarshaller_mCE129D11F8052AFDB6CF9165B7E791DAB77BBE4E,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_Options_m0A2898FF3EABBAB4E978889AF1BB379BA7B7B24C,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_Options_m4F9A2FFAE721D3CAE9C9407B99FACA305FBCCA0A,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_ImmutableCredentials_mC7ABFF395C149FF56563340D9AE520AA4CF82BCB,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_ImmutableCredentials_mB8A770A9E8FFDB3EBD95C661A64BE87A4F682AB8,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CancellationToken_mAB9910A6A46E6F719840B02B74007BC8F9D14A35,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CancellationToken_m416969291C198882377AB7F302BA0DBC4E953D17,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CSMCallAttempt_m02134A8CD42466EBD1745D01C390776F393F6177,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CSMCallAttempt_m6A1FF6F8CF854F9298D2910A9D746EECEE87DE8C,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CSMCallEvent_m241604A1F65FA848A747968080BC8D4BE19D9CAA,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CSMCallEvent_m2A7C298840A43F98784D2447CAC0C7437FC385DD,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_CSMEnabled_mEC4878BD651F342B235ACB9E5B04B1C39EC376BB,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_CSMEnabled_m3F7FD5E4D36DD00A8515633DB76CFA1F1CF9AF0E,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_get_IsLastExceptionRetryable_m9CBDEF39DDEB049FBC67FEB11EA2E369078E50DF,
	RequestContext_tAA24FE65EF39263D42192B52C6441E85C0EC017F_CustomAttributesCacheGenerator_RequestContext_set_IsLastExceptionRetryable_m06ED2876291D5203B96981CC608CE51BA73BADAC,
	ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_get_Response_m8B11E0CAD36313831E9E73DE776625B055C6E41B,
	ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_set_Response_mC3E7EC2795FC2AEDE791C0D6C5DD9CBDC2C229D1,
	ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_get_HttpResponse_m0DE5A537E04426B2FBA39A54E1A6ED490714B12D,
	ResponseContext_tA1740CCF8472ACF4625253C802C156E66AF08871_CustomAttributesCacheGenerator_ResponseContext_set_HttpResponse_mCDB00703E92D7AF877659F9C077D0A478DB6DD30,
	ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_get_RequestContext_m65AB9054F3B778D154885B4C08576F7E56D73471,
	ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_set_RequestContext_mFD3FB7214104B3EF2E0ADA06F9E381EBD986E502,
	ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_get_ResponseContext_mDA86EFB7136DEA63A6C462D56D0C19DBF2F5CB94,
	ExecutionContext_tC19894DF1001E633D3E10238C64CC4C5A932622C_CustomAttributesCacheGenerator_ExecutionContext_set_ResponseContext_m10DEA16A9002732D37F74B980F2EA9267682EDF5,
	ErrorHandler_tFCB7945CCC6EEA033CA840E9909955687982A160_CustomAttributesCacheGenerator_ErrorHandler_InvokeAsync_mA6B08B34BE400DBEAADB3A4E6097AD9C114F6976,
	ErrorHandler_tFCB7945CCC6EEA033CA840E9909955687982A160_CustomAttributesCacheGenerator_ErrorHandler_U3CU3En__0_mF715B16526759528B2C5252D3705A0F6787CEEC3,
	U3CInvokeAsyncU3Ed__5_1_t9D7AAC970FD594C9779BF519F0E3D319FD1C5820_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__5_1_SetStateMachine_m6C936EB22B847D8C4FB9DF854E7DDC489602AACD,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_get_OnPreInvoke_m1A71F567B0855CDC3FF0EB544116E7172AFF2E26,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_set_OnPreInvoke_m87C3C20DF9BD145545EE87C806FEB7D9E6371A50,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_get_OnPostInvoke_m508D36C9CF19EABD021159EABB46E9BDEB3D8323,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_set_OnPostInvoke_m2B7939C80173D9408D0D31C441EF9222C946900B,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_InvokeAsync_mD2CE9B7F99C27F4A11AF5F4F3892B01CB4DB69AF,
	CallbackHandler_t47987B2B27334ED4F62A2A00877F14AD2EEBF829_CustomAttributesCacheGenerator_CallbackHandler_U3CU3En__0_m99A29A787A9E307C23A725ABCBF0977CF9B01957,
	U3CInvokeAsyncU3Ed__9_1_tFF98FCA56A860F31270C95AE885AAACDE647862D_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__9_1_SetStateMachine_m3FA454185367AA9E4935CE16E5D4838C152E8043,
	CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_get_Credentials_m536C03A6F14228A70E0FE37416B02E246B0CC698,
	CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_set_Credentials_m0443A627032FBFA8D7D571BE129870693CB7A87B,
	CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_InvokeAsync_m0500C6F2F2934873A912C2D01C22E822BF4C98AE,
	CredentialsRetriever_tC055A83EE1AB21DA043BCFC05C95309E39A0D49A_CustomAttributesCacheGenerator_CredentialsRetriever_U3CU3En__0_m381E56F0CA3FDABF5BD7E6DC56C9430C2DA9179B,
	U3CInvokeAsyncU3Ed__7_1_t500523FA4CC27F9FBF19E4983256B4D157ACEB59_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__7_1_SetStateMachine_m14ED844340B84CECECD68A317CBCB8390060D9BD,
	EndpointDiscoveryHandler_tA77AF8EE23E7B35767508F63735536B66D405300_CustomAttributesCacheGenerator_EndpointDiscoveryHandler_InvokeAsync_m6D13E1678D1E0A52944C09740348C958C7DD9155,
	EndpointDiscoveryHandler_tA77AF8EE23E7B35767508F63735536B66D405300_CustomAttributesCacheGenerator_EndpointDiscoveryHandler_U3CU3En__0_m2103B2E379DC02BFADE47B5DEEDF68AB1EA085E5,
	U3CInvokeAsyncU3Ed__2_1_tBE2F45307EB426F060985B0E89D9C29D88AFDF02_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__2_1_SetStateMachine_m73731708A60A11B80100A0FFE6D77BABA067894F,
	ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_get_OnError_m128B66FA238E4109FE664413CA8D53BCE0A3721E,
	ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_set_OnError_m9B39DBF861DDF1523501DE9C1C7FBC96885AA0A0,
	ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_InvokeAsync_m67CDAECDE835AD1F92A6079D00C8DE21B9DCD1B5,
	ErrorCallbackHandler_t75975A44C28F892E669C6427C70D27B7A026B0D7_CustomAttributesCacheGenerator_ErrorCallbackHandler_U3CU3En__0_m85B2BD6389E6CBAC7CA3EC56D33C9B3794CD7750,
	U3CInvokeAsyncU3Ed__5_1_tFC0985043A07EBE4F82A97B33AB979FD09271A8F_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__5_1_SetStateMachine_m2EAF45F2868E4827975EDD3BF5D00D11C17609EE,
	MetricsHandler_t5CD352FBFEBC2E7D15E6F5C2967C16474C80749B_CustomAttributesCacheGenerator_MetricsHandler_InvokeAsync_m50E5AA830E434CFC5E6EE6D21DB392C0134BD70A,
	MetricsHandler_t5CD352FBFEBC2E7D15E6F5C2967C16474C80749B_CustomAttributesCacheGenerator_MetricsHandler_U3CU3En__0_m312BE1777AAF271AABB3F829A258431AAA7F0227,
	U3CInvokeAsyncU3Ed__1_1_tFF67E726AEA099B7C779AC6A6917AC647A0662BB_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__1_1_SetStateMachine_m405C9753A6E0E355B161BE54E594ACF8A1F1BB31,
	Unmarshaller_tEE40FB698DB46E39196C5E02D4937749BADC6713_CustomAttributesCacheGenerator_Unmarshaller_InvokeAsync_mE3D983ABC1F8CADA9FE88BB2D54BC0EFD47AFAFA,
	Unmarshaller_tEE40FB698DB46E39196C5E02D4937749BADC6713_CustomAttributesCacheGenerator_Unmarshaller_UnmarshallAsync_m9EFFEA0695F1F4D6EB0388FF9465AA6EB981F4A2,
	Unmarshaller_tEE40FB698DB46E39196C5E02D4937749BADC6713_CustomAttributesCacheGenerator_Unmarshaller_U3CU3En__0_m68B879761E284CB01A6D45D37BBADEFC62F8FD5A,
	U3CInvokeAsyncU3Ed__3_1_t18B339FB3AFAA53D304CF67A542973C6EC47C029_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__3_1_SetStateMachine_mE8DD9FA58D93C16816CCD426815A8525D60B25AD,
	U3CUnmarshallAsyncU3Ed__5_tDCD1BB6078980DFB3A0B2071345512F1E701B30C_CustomAttributesCacheGenerator_U3CUnmarshallAsyncU3Ed__5_SetStateMachine_m92CF4477AB1C3D3C38CED8DE399CC58D1ED0F3F4,
	HttpErrorResponseException_tC1C3C48976A8F3328CF4D595A8F7719A8404666A_CustomAttributesCacheGenerator_HttpErrorResponseException_get_Response_mF300FD82AF9B72A5FAC48955B57D1A6C16570391,
	HttpErrorResponseException_tC1C3C48976A8F3328CF4D595A8F7719A8404666A_CustomAttributesCacheGenerator_HttpErrorResponseException_set_Response_m0EFA97C19C16304F4A116E23F4405DC3012630D9,
	HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_get_CallbackSender_m11E5FE0ACD345673110BAE605A36FE1D0F2D6BDC,
	HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_set_CallbackSender_m89A6908A3E9381F2F661872F672796B6D7144F3C,
	HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_InvokeAsync_mCB61B52F5E726D85622BC0AF4F5D9D3B6C8994FE,
	HttpHandler_1_t80768FFECBBC5BDFB4E7DACB9AC85ECF72832E17_CustomAttributesCacheGenerator_HttpHandler_1_CompleteFailedRequest_m28E2B31B70445939BC4AFC3E5007979C83E7BA9C,
	U3CInvokeAsyncU3Ed__9_1_t83D8BAFE8A0376C2B21CBBF4AEF73878C17B5599_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__9_1_SetStateMachine_m43FA2CD35446D0B3E748E3D50EBD7E8BFB9449F9,
	U3CCompleteFailedRequestU3Ed__10_t0C21AA212E0D46C4636184493DD7FAC09C414AEB_CustomAttributesCacheGenerator_U3CCompleteFailedRequestU3Ed__10_SetStateMachine_m6F75F272B99096ADAE6E3C618A382D197006EDBD,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_get_Logger_m079669ADD1EC8F4E440F64144BA3AF4EE7958403,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_set_Logger_m0C70A5AFC9FA063FB8A3E49FB0E9DF35BD663BE5,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_get_InnerHandler_mC14FC8E11103E7D2DD80C869934B005486069356,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_set_InnerHandler_m2CA2C10774C68B7556C3BC7CC3FD7FAE1E0D6677,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_get_OuterHandler_m3C8B44F35F670546B89E7CEC831456D952E00814,
	PipelineHandler_tBF8ACADA4CD08607E83EDD030C134277EC57E565_CustomAttributesCacheGenerator_PipelineHandler_set_OuterHandler_mDD4DE1D53C5122E7AB75AA43F93C38967F105C74,
	AdaptiveRetryPolicy_t27319F5E3F091520A863559FBF7F427567C3195B_CustomAttributesCacheGenerator_AdaptiveRetryPolicy_get_TokenBucket_m262D3EE41D0717E08267DA1261D9F9BAC003C85B,
	AdaptiveRetryPolicy_t27319F5E3F091520A863559FBF7F427567C3195B_CustomAttributesCacheGenerator_AdaptiveRetryPolicy_ObtainSendTokenAsync_m5C3B0456589FE9949204CF28185B0F6EA6C55F95,
	U3CObtainSendTokenAsyncU3Ed__11_tE698E100ECBB25D061A654ECF71A4BD4EAF80959_CustomAttributesCacheGenerator_U3CObtainSendTokenAsyncU3Ed__11_SetStateMachine_m5B189B23DDAAE678549DE1625AD265E0EBE6E43D,
	DefaultRetryPolicy_t6B435B4C8CD1D13531C9DDC6D30A62E49F835DCF_CustomAttributesCacheGenerator_DefaultRetryPolicy_get_MaxBackoffInMilliseconds_mAD8744FAF8A1B9D70F9D864F64C1A5B5FE28AE17,
	RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_get_RetryPolicy_mDC30A963F84526B46BCB050863888F12D4F572BC,
	RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_set_RetryPolicy_m63A981A434D1D7B794D90E8768339391FA299A0B,
	RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_InvokeAsync_m38CF21A4236E185FA9F75B0BA13A982D9533C420,
	RetryHandler_t0DEEEE40F4BF60B11647914CD39945708A91ACF8_CustomAttributesCacheGenerator_RetryHandler_U3CU3En__0_m268DDA826AAEEAB2E0491EA7272D432560C35F91,
	U3CInvokeAsyncU3Ed__10_1_t1F1F94436648D4E4999ED4621CEADD95B5CABDE0_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__10_1_SetStateMachine_mCD78BFB0C0BBFEDD46583821A43BEDE743C96325,
	StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_StandardRetryPolicy_get_CapacityManagerInstance_m5B53C8DF35CF8F30F2651706E2F2DC69289BD0F7,
	StandardRetryPolicy_t0C49B1547FAED58B7E92AFF5B314CA1A8FA66D9E_CustomAttributesCacheGenerator_StandardRetryPolicy_get_MaxBackoffInMilliseconds_m6662682822FF79F5655C62F39E86FEF381F21A04,
	CSMCallAttemptHandler_t86AB3E0F743BF8160269931783BAD12F67C6B787_CustomAttributesCacheGenerator_CSMCallAttemptHandler_InvokeAsync_mD3264007D3D20F97F327ABEC4FB7D97D0CCC9ADF,
	CSMCallAttemptHandler_t86AB3E0F743BF8160269931783BAD12F67C6B787_CustomAttributesCacheGenerator_CSMCallAttemptHandler_U3CU3En__0_m4177B682048AD54BFA3A41B664C421F69C374E25,
	U3CInvokeAsyncU3Ed__1_1_t8C2F5F563205CE638E16FC41D04654C824473344_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__1_1_SetStateMachine_mC61E00A059BED7F1CC76F1E8979C2E26DE2B6BF5,
	CSMCallEventHandler_t051E26E9C1965113A789BD402C5D1F6DEA419B23_CustomAttributesCacheGenerator_CSMCallEventHandler_InvokeAsync_mB663921736555D69C1AD092018719CE322FA8335,
	CSMCallEventHandler_t051E26E9C1965113A789BD402C5D1F6DEA419B23_CustomAttributesCacheGenerator_CSMCallEventHandler_U3CU3En__0_m0168D5033BC97341665371FD48B3EC213B980992,
	U3CInvokeAsyncU3Ed__2_1_tB26CC2C751BDDF477FB7F1D29CCAB66CA25DABC6_CustomAttributesCacheGenerator_U3CInvokeAsyncU3Ed__2_1_SetStateMachine_mFEF219175BD131DF200E75E4DD47B6BE843D8DC8,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_FillRate_mA0A7ED0D0D33364D5A61E2BBBC7E6C3F664727F7,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_FillRate_m72489ECECCF952D97E9A6C0D9B7E28CE87DEDC12,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_MaxCapacity_mE1472754E58556B1E734686A99F455499093F774,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_MaxCapacity_mC9EE0F436FA9DA0B77AB46F8DDACE7EE35C8942F,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_CurrentCapacity_mA421C72A7EED744D51FF1C8042EFB16E090F2C6F,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_CurrentCapacity_mCE6F1E8099C66C0A073A45B760E7E74A8E0EA8AB,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastTimestamp_m0FE13E882A81F71410D028982FB12C3A2FCD9158,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastTimestamp_mD6473615DB7CC4E992DE2A09264467D9E62B7B12,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_MeasuredTxRate_m1A629CE8A0D664B7A488ECA0893CBA3E5B3344CE,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_MeasuredTxRate_mFB5DE4F1071C968447A87E109189008485EE0EAD,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastTxRateBucket_m02293BF2983E261302A22A14DF81AAF421ED0779,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastTxRateBucket_m218A404F7B552EB50D6C40D0140C222BF1DB83E1,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_RequestCount_m05B019782DEB0D4D21A960F659A6E1B3B45DEBA7,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_RequestCount_mB65440290DFA7CA01D193C6815F774E4E70E5CD6,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastMaxRate_m18DF834E4844CEC6844F4DEABBF8B6AE3A1CB658,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastMaxRate_m6F3B8E232E1213DC18CD217A37A2F345B2E0D8A9,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_LastThrottleTime_mD3267451748BD6074DEFB8ECA779D9E630D10765,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_LastThrottleTime_m88EAAC3991614FD2C871E79FBF147CDB303B4CBC,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_TimeWindow_m7916F196613D45C135BD7A1E263326F5B4993F91,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_TimeWindow_mDB11F5A40AEEE3B6F25ACA9AB5768DC7263819F5,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_get_Enabled_m776C13DE5371FB8CBCB2E21F2999FEE99B03F5FB,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_set_Enabled_m095AB1B9C82FFA6598F942926581D6D1AF7820AF,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_TryAcquireTokenAsync_mC4D9588455662EC0320D548DA857AF578A5A7C3F,
	TokenBucket_t2AE88BDCF577F702EAD31B5894140F6212DC73BB_CustomAttributesCacheGenerator_TokenBucket_WaitForTokenAsync_m928EB18E092E08F143F8EBDC694B5E9C3CB91CD5,
	U3CTryAcquireTokenAsyncU3Ed__55_t671FCF42A18EBF5DF15C21420B9F02604648F950_CustomAttributesCacheGenerator_U3CTryAcquireTokenAsyncU3Ed__55_SetStateMachine_mC8FB6D650565057DAF0C7310E6CFC027923C0B62,
	U3CWaitForTokenAsyncU3Ed__67_t7156E12B5F701C46577989AD99A524E500208581_CustomAttributesCacheGenerator_U3CWaitForTokenAsyncU3Ed__67_SetStateMachine_mCA1B76DFBDD3ED10552418255052F57DA8067186,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_Host_mB62FF9EA748848CC74E83DB1F44655C0BD243E92,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_Host_m9B1428EB43FB9DC145988A0AE89AF5A0285BF471,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_Port_m1E23C410430AA481A44D5B334219AA366F8C1059,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_Port_m51AFCCA8066C2479050A5D4395B625A824D47475,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_Enabled_m81AD5D5A20D6A39D2F8A2620EBC7B89AD6731E32,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_Enabled_mD6BC7CD6553DC314AAB1B598F5F260335F8427AB,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_get_ClientId_m4CEA76CCE2A19DF9F341B90C1001B38755A436A6,
	CSMConfiguration_t1B421816D1C2E81A5EC836832E2FC12E29120AFB_CustomAttributesCacheGenerator_CSMConfiguration_set_ClientId_m5A2E035C2DBA99F74DA66203FCE3F5303BB83E3E,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_get_AllGenerators_mB2A3806671D707BA6166978686F6B5BA9FAF31F4,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_AllGenerators_mFF5CE6CA4C99BD98545E405EA4FB105E41EB54A4,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_get_IsConfigSet_m31F9CCE5FC84C9C093A4DCD333B9E34A3769EB11,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_IsConfigSet_mA9BEC2B921058B43BED14457DF2A83CD720AB1CF,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_ConfigSource_mB2E85549468F1E0237B189424F548A851D043F68,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_get_CSMConfiguration_m529E6F75EA90D53EA97DF81FCC1F8AC8E8225766,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_set_CSMConfiguration_mB622A47543892199ABEF2C8F79E586D01B3CD5CC,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_U3C_ctorU3Eb__19_0_m2AA81B437DA8CF71B653F813766CAD9A12B224BE,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_U3C_ctorU3Eb__19_1_mFC07E82E37B78AFB08278848459549CC77CD5658,
	CSMFallbackConfigChain_t2E82D9E989381643249DDD92993B9D7DCCF5A72D_CustomAttributesCacheGenerator_CSMFallbackConfigChain_U3C_ctorU3Eb__19_2_mCEB6395641F132B61A623792AC2A39C8F41D102F,
	ProfileCSMConfigs_t61934EAD438F3CF47A010D583891DC379530CD18_CustomAttributesCacheGenerator_ProfileCSMConfigs_get_ProfileName_mBDAFA68A6EB56A0C3F5779C8D3DCC4F9336F0519,
	ProfileCSMConfigs_t61934EAD438F3CF47A010D583891DC379530CD18_CustomAttributesCacheGenerator_ProfileCSMConfigs_set_ProfileName_m4AF99B4E75DB4445951F8F25191C5BF1E69090DE,
	EnvironmentVariableCSMConfigs_t0762DD2415B673F53C345E6E12328035CB268269_CustomAttributesCacheGenerator_EnvironmentVariableCSMConfigs_get_environmentRetriever_m0DB2581E62898D47F6616E31AA36CAF0146E028E,
	DeterminedCSMConfiguration_t21AED8927102140FEFAD6AD2FEEC44D89B5679CA_CustomAttributesCacheGenerator_DeterminedCSMConfiguration_get_CSMConfiguration_mAB96F0DC4AE5F8F1DB7727A094F2BA66BD933382,
	DeterminedCSMConfiguration_t21AED8927102140FEFAD6AD2FEEC44D89B5679CA_CustomAttributesCacheGenerator_DeterminedCSMConfiguration_set_CSMConfiguration_m5D22C46AFCFD26C97C8F6E662F9D0475C4F9D8EF,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Api_m0421D8D4DA5663899D39BD23D46C40EBBA6F3483,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Api_mC4C55733606CFBF05B305CB46D6E8B4B005CEE30,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Service_m3B91F2CD211A542CBB96C9136BA39202067353A0,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Service_m144B8CFF0ECB8CC57B168F4069A84BD6AA82920D,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_ClientId_m2A044772ADA95AC4480BC9951B69BF92044042B5,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_ClientId_m6C80B675D0CB589DCFCB208BCC6E9F3CA12183D4,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Timestamp_m1A5EDDC9894C0FB1AEB4F00CCD8870B1A6604936,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Timestamp_m03E081ACED1B45E8652C1DEFC3AE78220C6B2E22,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Type_m838B0741BBF668D7B367C1C3BAF1309D7D4071ED,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Type_m9AEF4F1CBD04509B7926C45ADC98C15936DA4AB3,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Version_m1350F113A9E10FDB65F58362C721175A6F91B4AE,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_Region_m5CD08A43220F126CB25B2AB57CA2D3D774FD41A5,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_Region_m3D084EFEDF90D8B8449E28A372C780B999ED0E0B,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_get_UserAgent_mDCF593DAA9DF1950F9788E0DF4D42CEB1E627C02,
	MonitoringAPICall_tA2A9C0729C576A447581D26A2733D5A2F179F593_CustomAttributesCacheGenerator_MonitoringAPICall_set_UserAgent_m1F837089B138AD498DF2950E1F390D0D6C0D64E8,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_Fqdn_mC1C7B451BFB6364D78DB00E5070E111FAB35D0CF,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_Fqdn_m01BE278F2D17A95AB94C37F7FC59A4E9FD64313F,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_SessionToken_mFDA438C3142E23E75C4B0132490CF1F519C1FD80,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_SessionToken_m945A8D8440A19BCDD6576467B44894A81BFF9EA4,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AccessKey_m96D3FB844360E8D74D7FBEAA11076D33EB01C515,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AccessKey_m4A6EA9A4C5E28F7CF04F7B02EE0113183AD8F164,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_HttpStatusCode_mD24FDF30CD12CD3F88848993681D41F93781C502,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_HttpStatusCode_m987F92618CAD35F5BC8668334BBF5EAC3B0FBBDE,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_SdkExceptionMessage_m630E9AEDE21F51C10E55EFEC2C6C3230D0EF8871,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_SdkExceptionMessage_mBE3B617CA3F000E2B607171A82C93150132B395C,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_SdkException_m094D5FBD1FA02A53608668E5891F650005D7F0D0,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_SdkException_m96DA837D58F7B9E33292F7DAB80C6D616FE7246E,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AWSException_m621BF4D5C58B7ABB9FEFCE8EF74F78C85428FE5E,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AWSException_m8A014D4863F83034A00C45402AA37DF090DEFB24,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AWSExceptionMessage_m1C9CA1F161FE48A9E07E0B75CA28C5B1B784599F,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AWSExceptionMessage_m7112DBAA36E0128BC4BE3D4522EDF9062D57ABCA,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_XAmznRequestId_m45F79A7B23338AEE1F00F484DFFFAAF6A6EAE3E4,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_XAmznRequestId_mD2E1584FB7FFE82F4CF4830E1489132D01AE94F3,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_XAmzRequestId_mD13076615AEADA948BEDB6B9E9BED3C1992AA04F,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_XAmzRequestId_m641F0ABC8509CBC655349F822A3568D3B11B995E,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_XAmzId2_m63ECE3EA15B22180CEDB408A79A7F1B4BC76F37B,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_XAmzId2_m5CA9D84BDD1AEAD6F04509F43D3A0D8FA0E356D6,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_get_AttemptLatency_m16BA30F9971385B8E01E41FE78961DFBFF81E6C1,
	MonitoringAPICallAttempt_tAFAC16E3C1311A9F6B9A9E93F132962E9E449B03_CustomAttributesCacheGenerator_MonitoringAPICallAttempt_set_AttemptLatency_m0CAEAAC28246217A1C9964A1779C3B58F5D6D926,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_AttemptCount_m9FD13509E959B8D2845F6B343977E8C868076AAD,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_AttemptCount_mBD8F813864ADCEF4408000597EF31D39219B723C,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_Latency_m03DA86A6D65A9293B19F3C3A6BE18EEDD91E8344,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_Latency_mC5CD2AE1112B756AE6BF5E7DC5B8B0516533FED6,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_IsLastExceptionRetryable_m02D2DB01CD71002D7BD5F7E7B6370B338BD5781F,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_IsLastExceptionRetryable_mAA7FD50220A6E6D50F202F17AFF8E6224D3F348B,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalSdkExceptionMessage_mB883D7A30AEFE8D0C95F7ACBC14FF17F3D3E8D58,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalSdkExceptionMessage_mCE4CEBB9D3195B4FCA1C97F06FDE129BF4E038D7,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalSdkException_m4C0783F4BFD0AECB34A62A8BD9482D985B4692F1,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalSdkException_m844468E3AC8FCEB66D06429B93F3CC1DBD348A95,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalAWSException_m915DD2A5206AEB58EF4B4C97BCC865DBBAD5AED4,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalAWSException_mAE895618DF6F8E6795E9BA79F7494C613C037BDF,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalAWSExceptionMessage_m57BF5E0DB609F04A09946FBC697EB8A73866CA08,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalAWSExceptionMessage_m5F69148E6FA2848E9476EDC98F4C7F66F23B26D6,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_get_FinalHttpStatusCode_m52DF47D94212EBE72D665682FBFBE07B726989D8,
	MonitoringAPICallEvent_t104AEA248FC44D35E924F3E7289C9B3D1066E497_CustomAttributesCacheGenerator_MonitoringAPICallEvent_set_FinalHttpStatusCode_m1765EFAE3F3608A5C828F78249B1674D7829EF65,
	CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_get_AllReadBytes_m47F8162EE81AF5D64CC11404AC5F016C410A571A,
	CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_set_AllReadBytes_mF9E1FE6C9EAE04CB72D94F64A12E80D148792449,
	CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_ReadAsync_mCCF843346B0BF947D6ED985A9D134B554BE74A95,
	CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D_CustomAttributesCacheGenerator_CachingWrapperStream_U3CU3En__0_m732E60ABD6DCDDE8A24C7630D1DDB960A789B57D,
	U3CReadAsyncU3Ed__10_t1CF21CECEA107CFC3C1F43F700171076E9B8AE3C_CustomAttributesCacheGenerator_U3CReadAsyncU3Ed__10_SetStateMachine_mFD24E47F2568DF602D75FEB1AD250C09AF1402F3,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_ReadAsync_m6A47C1B5537F49092C6FC4F3BEF907635D170E27,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_FillInputBufferAsync_mE7F390F23DC7FB28A04E09403ADB3089687A2DB8,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_get_HeaderSigningResult_m65E448AFAA7096A20053DAAB9072B2B487A1F1C8,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_set_HeaderSigningResult_m83F964155AFC9D296239E73EF94E1C3B8113B65A,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_get_PreviousChunkSignature_m92C109BB64841B864EDDBC37BB622F1A5CE4AF12,
	ChunkedUploadWrapperStream_t0E41F36E7789E676C09EF34524BBCDA5BC190E69_CustomAttributesCacheGenerator_ChunkedUploadWrapperStream_set_PreviousChunkSignature_m352F26BF5C023E4CC80654ABE32CBA91012467A0,
	U3CReadAsyncU3Ed__17_tF92032D9AB2DBC82D4B39C1E5DCFB01311313097_CustomAttributesCacheGenerator_U3CReadAsyncU3Ed__17_SetStateMachine_mAEBAEDD593301A03FA75541227C724FE80BE9FEB,
	U3CFillInputBufferAsyncU3Ed__18_tBF2BB7FC7050F6473FB6ABBFBC8779654FD6E204_CustomAttributesCacheGenerator_U3CFillInputBufferAsyncU3Ed__18_SetStateMachine_mD3F93DDF86B76F1C0F6546C305314C798C2255EC,
	BackgroundDispatcher_1_t4309C58161837D4C2ED66BBBC5E12B50DCEA9D51_CustomAttributesCacheGenerator_BackgroundDispatcher_1_set_IsRunning_mC3571AF9EECBBB8F5797C73408F5173EC608073F,
	EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_EventStream_add_OnRead_m0DE98B157EC382ADADEF709A3151FC5EEAF22B64,
	EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_EventStream_remove_OnRead_mEAA2C6160B35C2510258E99BBBD8FFA3E4C2934F,
	EventStream_tEFE204E36D8E114314F36595E09309646BB20E1C_CustomAttributesCacheGenerator_EventStream_ReadAsync_mA6ECBA89A86A4B35D07528B73E5E2C5181996E37,
	U3CReadAsyncU3Ed__33_t8996690A5175535506BC51BDD189646E683DA84D_CustomAttributesCacheGenerator_U3CReadAsyncU3Ed__33_SetStateMachine_m93DC06F8C05B23E12F7E832FF063A3D652DDA55E,
	Extensions_tD20EE6984D3F1330D9B257EDFD79576477E10E56_CustomAttributesCacheGenerator_Extensions_GetElapsedDateTimeTicks_mFC9EB180A1895BD3CFA90ADDC1BA0E8EBB37A3B0,
	Extensions_tD20EE6984D3F1330D9B257EDFD79576477E10E56_CustomAttributesCacheGenerator_Extensions_HasRequestData_m89B15A85A93DE8851680363F7237A1BAF07F6700,
	HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_HashStream_get_Algorithm_m77777CC969D213FAB4ABCBBA87FF03AB65FB9647,
	HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_HashStream_set_CurrentPosition_mAC27EEE93E512BAB4450D2ED614850362DAF3F9A,
	HashStream_t692F178884639D225A17582E215281E7C92718FE_CustomAttributesCacheGenerator_HashStream_set_CalculatedHash_m78F1745C53826D49A4888F317B65495C587FBD72,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_get_DeclaringType_mF8C58DF13E2E9F52A6C1C28533698C7FC23549A3,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_set_DeclaringType_mB5E079C1381CB75406DC0A2B0C3F679FE6892E50,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_get_IsEnabled_mC05569861EF1FD59E173C1CC3D538015554CE830,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_set_IsEnabled_m60444B552DE4B9925465942D0E3155D5839B5316,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_get_Args_m64559F784FBCEF8CA2E60E453E9E2D284D4924CE,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_set_Args_m57166E78E7484C714F1CF706EE6ADED27C3430A2,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_get_Provider_m3A902C604D423FBCA0E6BAAEF89002A0D443B8BC,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_set_Provider_m44E3AF9A95855B86FEC00453D9B1BEEF4AEB12B8,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_get_Format_m60859CBB4FAE49145535300C94EE5B55598D2DF4,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage_set_Format_m72DEAFAF7695EEB50DA0A52565923C01F4068ABD,
	LruCache_2_tE3253D91CEC5F96B76E3E2D86DFB2CD7F28D5572_CustomAttributesCacheGenerator_LruCache_2_set_MaxEntries_m44DB2EB5F53278EC1AC5CEB9C6B1165B8C0DE33A,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_Properties_m84BC22CAA8541BABE6FE73999D148C1ED76DBBF0,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_Properties_m3C55799F13A923439566FF92885EB32CE2EC716F,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_Timings_m16064050B633CBE0E591C154A132ABF4B0A28E26,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_Timings_mEF536D7BA4D9F52DA8869B9937C519F356B9DF67,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_Counters_m8A0BDC41BE680C0E804B86D01693558CFE0E5894,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_Counters_m04B53A30F43131DDBD3978F730C6ECD02257749D,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_get_IsEnabled_m1C66114D0BC5B5FADE7FA6A59F0DE19776471245,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_set_IsEnabled_m8F6AECA196BF20FB649C8AB8EA0A0BF3E7186E1E,
	Timing_t4D1C5000BC2696A4879EB7862DED30F6882A453E_CustomAttributesCacheGenerator_Timing_get_IsFinished_mEB880282891D5893958B9981CDFF58ECF2BB304E,
	Timing_t4D1C5000BC2696A4879EB7862DED30F6882A453E_CustomAttributesCacheGenerator_Timing_set_IsFinished_mDB14D3D301BF0DAAFC799C58EE20E6838623A380,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Metric_m0EFDCF995545075980F8C80E5597CEB829AADF6C,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Metric_m520EB610297922EE7EAB30E41E3C0F6E6EA091FA,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Message_m0CB9DC2D53A93DA95B07DD455CDC0FB60CD2C05F,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Message_m0541CC94D5A9606C37C1485C32A0C39133A5FE3E,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Exception_m6FC385BBE21A845875D34A44D1DC6B265419B139,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Exception_mB492556F5CDD058FF0A84D4FEEF2EDD032A6FF55,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_get_Time_m287005246E7A6BED191E05ADB098B6BE5856F39C,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError_set_Time_m0FD2F80D3A72BC1D80EBA419A23833BF268747C4,
	WrapperStream_t4676047E9C65B07194C4FF80564CF1077A7550CA_CustomAttributesCacheGenerator_WrapperStream_get_BaseStream_mFEB26EFB3BC758284D7CF3F725BD9E865E3D086A,
	WrapperStream_t4676047E9C65B07194C4FF80564CF1077A7550CA_CustomAttributesCacheGenerator_WrapperStream_set_BaseStream_mD8B7FFBEA1AFC67B0B69D0AD97F28771F6767DFE,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_get_OriginalContents_m7ED6E4DB738F208C66A18E8BE114AFFE62296852,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_set_OriginalContents_mBDD535792A93B7CA5602DAEADE392190C5D51AF3,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_get_FilePath_m8B21A1CC348771C89308D256C7A25B7AE7884491,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_set_FilePath_mFD1F1F01F35075BE40E6CC242C9E85DA541E4E34,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_get_Lines_m6DF3631DE46A8EB2B3C5B0F96F89DB2D711BDBA9,
	OptimisticLockedTextFile_t0C41E6FC5DDD5652A235911505C8D6E80B89687F_CustomAttributesCacheGenerator_OptimisticLockedTextFile_set_Lines_mCD018C2303B399C5A81BC7A2D90DAA0233CD0254,
	ProfileIniFile_t9CA7AFA6EB1CC84E3A7D05997EA461D7E67335A1_CustomAttributesCacheGenerator_ProfileIniFile_get_ProfileMarkerRequired_mBF818D67B6A16168B253C60CD65BD3CB4A6E29E4,
	ProfileIniFile_t9CA7AFA6EB1CC84E3A7D05997EA461D7E67335A1_CustomAttributesCacheGenerator_ProfileIniFile_set_ProfileMarkerRequired_m78330C35440F6AAAA16C6B6115F7611E640FDCC9,
	ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_get_ObjectException_m7D008D6F4B95533A1B9994CF182147BCA532C3A5,
	ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_set_ObjectException_m6C1CB681D339DED2069010AA0513E31E155D20DE,
	ExclusiveSynchronizationContext_t8ADBD5CF224A8E9DE4AC838E9B38E6A05EC4C784_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m7BEA15F25CAD1B33751ADD0797A19CE192B808A6,
	U3CU3Ec__DisplayClass1_0_1_t3EA451AC0DDD9C0ED3D317639390BBA5A58AD3B0_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass1_0_1_U3CRunSyncU3Eb__0_m23E3F67F44F64EC01B7E4803D29A295B755266AF,
	U3CU3CRunSyncU3Eb__0U3Ed_tD5715C887076CAC33A7EE1570F29B477D8DE2B9C_CustomAttributesCacheGenerator_U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m15C37DB8F45A8E255F4B5CE58495CEE840F31DA9,
	MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952_CustomAttributesCacheGenerator_MarshallerContext_set_Request_m8F802D68F09FC7F70F1BA36E3B4FEA893706D834,
	JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_CustomAttributesCacheGenerator_JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B,
	JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_CustomAttributesCacheGenerator_JsonMarshallerContext_set_Writer_m324F3C5EBDFBD98ED3E6B96565B40DCE1B066955,
	PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_get_SegmentType_m65B82F8564E2206BCE241B78F79E678AA696D575,
	PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_set_SegmentType_m40F0CBA4B9A4C76E4256AD3195C964DD88906D2A,
	PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_get_Value_m35D398000A8EA78DE5FAF47E7BC0BF1C44EDBDD2,
	PathSegment_t6FEC6BF05335C4C55D3E26889F5D1B4F0FD7491A_CustomAttributesCacheGenerator_PathSegment_set_Value_m909F2ED45DF5DCABEA9D81BC2761C3D8267F5101,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_MaintainResponseBody_mEE9520F845AEF875317FB56E2DD0429A98422D93,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_MaintainResponseBody_mDD39548A3482E4C6C005544A32A53468DD962322,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_IsException_m88F8C42DABA97B6253A32CC60EEDC2D756C337F6,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_IsException_m2D4133C986F235F215588C6C87D8515E4B00F585,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_CrcStream_m01B55B4F84F8411D97E5299E88DCA96BD9761680,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_CrcStream_mDCD547E103C6E2B8F63A483422E48D66A60BAC4E,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_Crc32Result_mFCCC6104016C906AE1716B3660E6166F2DEF085F,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_Crc32Result_m48140B1D642B7A6F16B071E4A0D5D5E8683C25B9,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_WebResponseData_mF6135EFB850AEAAD2B212B7038C8C25AA38AB8F8,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_WebResponseData_m7F8BDA75500276956944AF7565D842B44E62C9C5,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_get_WrappingStream_mDD39993BF5ECDC711F69C9220A464260571C26DD,
	UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B_CustomAttributesCacheGenerator_UnmarshallerContext_set_WrappingStream_mC885B682045B8C30D8083C7E76A987472AA8AA93,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_get_StatusCode_mDA92BB2BEF3BA1BBDF1DB3D14295D34B898CA7B6,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_StatusCode_m038084D472C1D5D77EB3444D44C591043EE75522,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_IsSuccessStatusCode_m8E9360B0084DA85637A85A11F1860D191E966E90,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_get_ContentType_m5CF5885F8847B1D6DAF0D87D1F98631F94D74F6E,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_ContentType_mF447D5D6B510AE8735E69A23753ED627F10F3A43,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_get_ContentLength_mA95E15F1F79F452C9B09FCC33C8A1AA54D9393F2,
	HttpClientResponseData_tDF6C4878B1B4F1BD5D3374099A37F8D1F6F24328_CustomAttributesCacheGenerator_HttpClientResponseData_set_ContentLength_m769C7803F8FCABAA68F2F8820AE64E7EEC74922F,
	PersistenceManager_tB518F8E4B3AEF363860FD6FBEB48AE7E0F51B290_CustomAttributesCacheGenerator_PersistenceManager_get_Instance_m184BC55A18A7E106F5F8FB191DDF160960FD3BE8,
	PersistenceManager_tB518F8E4B3AEF363860FD6FBEB48AE7E0F51B290_CustomAttributesCacheGenerator_PersistenceManager_set_Instance_m4334BD1CAF6918F9F6B8443B13FDD70D646E9B86,
	SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator_SettingsCollection_set_InitializedEmpty_mE31FA906B0A88CA7AB7AAF997C4D9D6ABAD672EF,
	SettingsCollection_tC059FD0D29912E236722DA883CEC23E116BB9D7D_CustomAttributesCacheGenerator_SettingsCollection_GetEnumerator_m79DFE4C6FF170762974BF7E5745417DB8AEB56A5,
	U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11__ctor_m20153A22BB2F12B09B9DC391189542CFB33D1A22,
	U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_IDisposable_Dispose_m8FCA25311749C595C065FAA5F91BEBAA1E6D5426,
	U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_Collections_Generic_IEnumeratorU3CAmazon_Runtime_Internal_Settings_SettingsCollection_ObjectSettingsU3E_get_Current_m0522738F60385992B2C0E82EB07FDC81D1441B9A,
	U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_Reset_m44794CDDC5D62355A67F666A9C81997DBAFF01FE,
	U3CGetEnumeratorU3Ed__11_tC7C2B110A3B74BC1BCD6BF7CA6184743855ED774_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__11_System_Collections_IEnumerator_get_Current_m40917AA2601E7ECA679D9521E6E2079FDD54364E,
	AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_CustomAttributesCacheGenerator_AWS4Signer_get_SignPayload_mE907B2FB3EF750B065F1C0D454816D9BDCC0A693,
	AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_CustomAttributesCacheGenerator_AWS4Signer_set_SignPayload_mB99439C13DC95FE5F3037223F4D2F0D955C193D5,
	Hashing_t9718D4B72E54CFFB39DBC808B4C061F78204D1AB_CustomAttributesCacheGenerator_Hashing_Hash_m70FC42FA944667BD6E608BD6040E3695B3B0F1E0____value0,
	Hashing_t9718D4B72E54CFFB39DBC808B4C061F78204D1AB_CustomAttributesCacheGenerator_Hashing_CombineHashes_m4098B2EDCAA2187D45472404BB15B32364853310____hashes0,
	ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_InfoFormat_m1222504C7D4DEFF4EDF128701F5D075CD54B8987____args1,
	ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_Debug_m663D987F6E8EDC2BFEC5FB0EBB0EE7F9109F5090____args2,
	ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_DebugFormat_m085D13025E284600A2E931572BEC4E534DEE4065____args1,
	ILogger_tF991BAC7AE06A6465BB94CA023ADAFCF87D65D85_CustomAttributesCacheGenerator_ILogger_Error_mF49ADFF7AB503ACFE042F4DD0DC860413640CF0B____args2,
	InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_Error_m8923FC249477CC668869717EBB99806926E41235____args2,
	InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_Debug_m8E6ABC0E7BF0C1A2A801E722EE546EDEBF694431____args2,
	InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_DebugFormat_mF95C2856A41926ADB444CCC2C51E5B093D253C29____arguments1,
	InternalConsoleLogger_tEFAD245FA2F40E7614F4D391CA2A91B8E05E93B3_CustomAttributesCacheGenerator_InternalConsoleLogger_InfoFormat_mFECE6C230B64353D25358155EEB0088B48A3F867____arguments1,
	Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_Error_mD839C4FC38C8C01EF7836C7632549115BEA394BA____args2,
	Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_Debug_mD756AEAD6298C36D3C76F94FC478D15E3FE965B1____args2,
	Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_DebugFormat_m525735E1788943E3DE772B42C9A64BAF1C32307B____args1,
	Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466_CustomAttributesCacheGenerator_Logger_InfoFormat_m834B8C45A15D6C18D10E8DA641C376B1A96B7AE0____args1,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_Error_mE7512D2E9A793AA701FCC1D3A99E3C25B8D55C31____args2,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_Debug_m4553948E51DBC458D053FE02F962A237AD126C31____args2,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_DebugFormat_m27AC3A09EE77336FDFD860D467F9BA66CA5A2618____arguments1,
	InternalLogger_t33DBBB782F2BEAF353FC7912A896C30346E8070F_CustomAttributesCacheGenerator_InternalLogger_InfoFormat_mEEB16A13BD30041D563F3DB5CCB94770C12B94A9____arguments1,
	InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_Error_m8C3018BBD99EA7D20D9D71034E5424DB9C0DAA57____args2,
	InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_Debug_mFDBE196995454C86FCC9F1D7F760A2B6D9DB56F3____args2,
	InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_DebugFormat_m7D57E2EC78D5CF1F90534253B2CA280E18BD92EF____arguments1,
	InternalLog4netLogger_tEA7BF3BC31185AC8FEB7CA0F4CBAFE99EF347753_CustomAttributesCacheGenerator_InternalLog4netLogger_InfoFormat_m4C7FB0FBD2A2E2E5553A5F3242BFCE901ED1E5FC____arguments1,
	LogMessage_t54370882040E61ED910C20B64ECC6BD983719F70_CustomAttributesCacheGenerator_LogMessage__ctor_mC9E82515541AEA85C7ACFE35081A77EC6E94657F____args2,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_LogError_Locked_m233C41DA154865B536D979BC9FDC3C748576CF84____args2,
	RequestMetrics_t7A388CC27038A5DE2D2291D69091156219FF2F73_CustomAttributesCacheGenerator_RequestMetrics_LogHelper_m06E3E60039285253B932C73671888A65B1C89D4C____metricValues2,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError__ctor_mEF8FCBFD3DD508E52D6BE9798AC603FDBEF7F58F____args2,
	MetricError_t0FC6CA64BA981B756F600CC92B62AA102BDE790C_CustomAttributesCacheGenerator_MetricError__ctor_m55C1C6FEF81AF627136BB5489BC1BA441246FE36____args3,
	InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_Error_m43F37752F40DDDA5E12BB8215DA6AA108827B464____args2,
	InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_Debug_mA2001BC068D54399EA529AD2F7B6394C3F7806C2____args2,
	InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_DebugFormat_m6F0D41299168CCA93DC6CD0388F93395618D7DF9____args1,
	InternalSystemDiagnosticsLogger_t387325021FC20B0C2671B6DC5F00C26C2B65CBB3_CustomAttributesCacheGenerator_InternalSystemDiagnosticsLogger_InfoFormat_m9023523528D24439C250B64566BE630C5CD7D407____arguments1,
	AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7_CustomAttributesCacheGenerator_AWSConfigs_tC9BD03CDAF2A8DAC2C8406A19A2FE4C01E727DC7____ClockOffset_PropertyInfo,
	AWSSDKUtils_t6F62F996003A26F02152C22ED49BE59B0482F653_CustomAttributesCacheGenerator_AWSSDKUtils_t6F62F996003A26F02152C22ED49BE59B0482F653____CorrectedUtcNow_PropertyInfo,
	ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_CustomAttributesCacheGenerator_ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099____ReadEntireResponse_PropertyInfo,
	WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A_CustomAttributesCacheGenerator_WebServiceRequestEventArgs_tE1B283EF264FC92E46CD2F0A20EE51FFD539860A____Parameters_PropertyInfo,
	AWSSDK_Core_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
