﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<Amazon.RegionEndpoint,Amazon.RegionEndpoint>
struct Dictionary_2_t17E91A0A878220377BD1CC373D5F93529B46F9A2;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint>
struct Dictionary_2_t4BA118A7A726F5C0B3CF2922C17F6E49A7B4BA0D;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>
struct Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<Amazon.Kinesis.Model.PutRecordsResponse>>
struct Func_2_t2C541A67AA77D0D282702143303CAFD1FEE63B13;
// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>
struct HashSet_1_t5B8880CAAF26A02176440210489F689BE557D267;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t0DE5AA701B682A891412350E63D3E441F98F205C;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.EndpointDiscoveryDataBase,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_tB4001E4D10A356BC336FF55A7ED1B3BFEA16BA9F;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_tFBA2AC89E47DBE72D84B41BDB7C0BBF711452800;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D;
// Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>
struct ListUnmarshaller_2_t8BDE52D0EFF3CA7CD00D31DF6349AA636C0D959A;
// Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.Kinesis.Model.PutRecordsResultEntry,Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller>
struct ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry>
struct List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8;
// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry>
struct List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_tF2F8B5476F614882C00CEDDE027482B818D7FF1D;
// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>
struct Stack_1_t913945E3D311A059DC7DFC94BA47E5B1DC5A89B9;
// System.Threading.Tasks.TaskFactory`1<Amazon.Kinesis.Model.PutRecordsResponse>
struct TaskFactory_1_t2AA05B7A6F4895470B4B35E5E8DD7D5EBEDFCFD3;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17;
// System.Threading.Tasks.Task`1<Amazon.Kinesis.Model.PutRecordsResponse>
struct Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// Amazon.Kinesis.Model.PutRecordsRequestEntry[]
struct PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0;
// Amazon.Kinesis.Model.PutRecordsResultEntry[]
struct PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// Amazon.Runtime.Internal.Auth.AWS4Signer
struct AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t7CF5A4B7035B9EF17A794BE62E7F3F0BC88B45B0;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE;
// Amazon.Kinesis.AmazonKinesisClient
struct AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0;
// Amazon.Kinesis.AmazonKinesisConfig
struct AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D;
// Amazon.Kinesis.AmazonKinesisException
struct AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B;
// Amazon.Kinesis.Internal.AmazonKinesisMetadata
struct AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD;
// Amazon.Kinesis.AmazonKinesisRequest
struct AmazonKinesisRequest_t6FADAEC3B6BFB9A305B4B0D75615BCC7DD5B0635;
// Amazon.Runtime.AmazonServiceClient
struct AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB;
// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A;
// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D;
// System.Globalization.Calendar
struct Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3;
// Amazon.Runtime.ClientConfig
struct ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E;
// System.Globalization.CompareInfo
struct CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9;
// Amazon.Runtime.ConstantClass
struct ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6;
// System.Threading.ContextCallback
struct ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B;
// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct CrcCalculatorStream_t3B289C1840AE2D3C4EDDF8A7D59061A1B39AFB55;
// System.Globalization.CultureData
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90;
// System.Text.DecoderFallback
struct DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D;
// Amazon.Runtime.Internal.DefaultRequest
struct DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4;
// System.Text.EncoderFallback
struct EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// Amazon.Kinesis.EncryptionType
struct EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F;
// Amazon.Runtime.Internal.EndpointDiscoveryResolverBase
struct EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC;
// Amazon.Runtime.Internal.EndpointOperationDelegate
struct EndpointOperationDelegate_t7188E39FDC66C6C6AF51AC9FF3E40758EAA4D1B1;
// Amazon.Runtime.Internal.ErrorResponse
struct ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0;
// System.Exception
struct Exception_t;
// Amazon.Runtime.ExceptionEventHandler
struct ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// Amazon.Runtime.HttpClientFactory
struct HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t38F64ED77DEB5E7EA3BFF6DCACCB2DE7D00BFB2A;
// System.Net.ICredentials
struct ICredentials_t7F4F7C8E1E36461DC5388554FF404E2203D30C48;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IFormatProvider
struct IFormatProvider_tF2AECC4B14F41D36718920D67F930CED940412DF;
// Amazon.Internal.IRegionEndpointProvider
struct IRegionEndpointProvider_tDF0043F63E33DBB1A3A56309A0F68A7C8F5825F6;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE;
// Amazon.Runtime.Internal.IServiceMetadata
struct IServiceMetadata_tC7743830F8447FD385AEF2ED9F417DBF9422F48F;
// System.Net.IWebProxy
struct IWebProxy_t27B6C29F0B62B717A9778AD251E31C21A91C32B3;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t6ACECED1319CE803CF2AD8551987F3FB71939538;
// Amazon.Runtime.Internal.Transform.IntUnmarshaller
struct IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4;
// Amazon.Kinesis.Model.InvalidArgumentException
struct InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller
struct InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4;
// Amazon.Runtime.Internal.InvokeOptions
struct InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55;
// Amazon.Runtime.Internal.InvokeOptionsBase
struct InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4;
// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller
struct JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB;
// Amazon.Runtime.Internal.Transform.JsonMarshallerContext
struct JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315;
// ThirdParty.Json.LitJson.JsonReader
struct JsonReader_tCCF81BC14AE9F7B6A52B16914C5918E260CDF515;
// Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller
struct JsonResponseUnmarshaller_t6A1B8EBE5327D6B9A39E51DB1A23C2593CFA1AFF;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686;
// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B;
// Amazon.Kinesis.Model.KMSAccessDeniedException
struct KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller
struct KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B;
// Amazon.Kinesis.Model.KMSDisabledException
struct KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller
struct KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF;
// Amazon.Kinesis.Model.KMSInvalidStateException
struct KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller
struct KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728;
// Amazon.Kinesis.Model.KMSNotFoundException
struct KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller
struct KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1;
// Amazon.Kinesis.Model.KMSOptInRequiredException
struct KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller
struct KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5;
// Amazon.Kinesis.Model.KMSThrottlingException
struct KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller
struct KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466;
// System.IO.MemoryStream
struct MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C;
// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D;
// Amazon.Runtime.Internal.ParameterCollection
struct ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3;
// Amazon.Runtime.PreRequestEventHandler
struct PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12;
// Amazon.Kinesis.Model.ProvisionedThroughputExceededException
struct ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller
struct ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7;
// Amazon.Kinesis.Model.PutRecordsRequest
struct PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD;
// Amazon.Kinesis.Model.PutRecordsRequestEntry
struct PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller
struct PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller
struct PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15;
// Amazon.Kinesis.Model.PutRecordsResponse
struct PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller
struct PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C;
// Amazon.Kinesis.Model.PutRecordsResultEntry
struct PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller
struct PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA;
// Amazon.RegionEndpoint
struct RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A;
// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7;
// Amazon.Kinesis.Model.ResourceNotFoundException
struct ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B;
// Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller
struct ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01;
// Amazon.Runtime.ResponseEventHandler
struct ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E;
// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// System.Threading.Tasks.StackGuard
struct StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// System.IO.StreamReader
struct StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// Amazon.Runtime.Internal.Transform.StringUnmarshaller
struct StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7;
// System.IO.StringWriter
struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D;
// System.Globalization.TextInfo
struct TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C;
// System.IO.TextWriter
struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643;
// System.Text.UnicodeEncoding
struct UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68;
// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// ThirdParty.Json.LitJson.WriterContext
struct WriterContext_t78CC992BA30308B8C47F7D1F7DFC20E2CF7E86DA;
// System.Xml.XmlReader
struct XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_tD1257418720567D67A7C49A7498914FE937F1520;
// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack
struct JsonPathStack_tA5E036E6D34C4F9396F396BDD6701446556647F2;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0;

IL2CPP_EXTERN_C RuntimeClass* AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InternalSDKUtils_t279A42889919AF1D76C646A48A51BC4025B7FE13_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringUtils_tFB5F525A8B6AA6C5D7F0478BEE0B948D6C43EC55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1021DBAB771621B55019EBDCA7C9CA5DB3FDB1B6;
IL2CPP_EXTERN_C String_t* _stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D;
IL2CPP_EXTERN_C String_t* _stringLiteral1F77D28E6879315E73D66F867E162905627E1407;
IL2CPP_EXTERN_C String_t* _stringLiteral2E0AA02ACB9B6C72DBE03DAF6927F1E835035F10;
IL2CPP_EXTERN_C String_t* _stringLiteral3321C3E441B4DD2FCC481DE41D1977249A6A5B81;
IL2CPP_EXTERN_C String_t* _stringLiteral33D4E43B349E3E198FD7BC4359029E3D92563948;
IL2CPP_EXTERN_C String_t* _stringLiteral375CB1C404C0385792B7E52009524648F40DCCD9;
IL2CPP_EXTERN_C String_t* _stringLiteral4552726700ED073D423A03C5215DC945DA4BF29D;
IL2CPP_EXTERN_C String_t* _stringLiteral4F34DBAAEC28C6F84BF9626DF38FA6484F34C45E;
IL2CPP_EXTERN_C String_t* _stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18;
IL2CPP_EXTERN_C String_t* _stringLiteral5B8F1DE7E8271919EB50CE5D116B0F803ACEFB60;
IL2CPP_EXTERN_C String_t* _stringLiteral605A1381100CA3BAE8A2FC12B6AA3B519873344D;
IL2CPP_EXTERN_C String_t* _stringLiteral6CD2848583A4EAD77430A317E3B6DB2BCCDB1003;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteral8BDEA0B4D2FABE3E1DFEE0187E022E1AD5F29CED;
IL2CPP_EXTERN_C String_t* _stringLiteral94CC7D9BC870205BF912A085D58A7FB72033D488;
IL2CPP_EXTERN_C String_t* _stringLiteralA128CFBDC22969D81529BF1140C8705F47CB988C;
IL2CPP_EXTERN_C String_t* _stringLiteralADC42BB0C23D6440C9A28C5EE4DA00970F75D59A;
IL2CPP_EXTERN_C String_t* _stringLiteralB4D8750C59CE123025990AD22F8E27C3459AF91F;
IL2CPP_EXTERN_C String_t* _stringLiteralBBB8F4B2D76068F57D1096B3199EACDDB534F144;
IL2CPP_EXTERN_C String_t* _stringLiteralBEB5E070F7FC787CED4C5F275ACCA71B0365907A;
IL2CPP_EXTERN_C String_t* _stringLiteralC1C97CAE5706ED016D99773A34B00C678F4C2A6C;
IL2CPP_EXTERN_C String_t* _stringLiteralC1D7C6FF863D0BF9263EEE11BFD63292BD62B7F2;
IL2CPP_EXTERN_C String_t* _stringLiteralC8E504213A7C2697E5C226BAE5003D6F866C3523;
IL2CPP_EXTERN_C String_t* _stringLiteralCDD783E351A9472256912F82543292477274E595;
IL2CPP_EXTERN_C String_t* _stringLiteralD4FF15F0C367172440AA7A961D856E09E93C8108;
IL2CPP_EXTERN_C String_t* _stringLiteralDCFD6604DC9979C11967D249B507578379EDBF28;
IL2CPP_EXTERN_C String_t* _stringLiteralDE401658E1A4AA158FAA8FD47AEF1BA4DD7AEB4A;
IL2CPP_EXTERN_C String_t* _stringLiteralDE4288194BDB19413685247524F3FBE96A06954F;
IL2CPP_EXTERN_C String_t* _stringLiteralDE754822751080A8DD149E86BE95EC91307E3EF3;
IL2CPP_EXTERN_C String_t* _stringLiteralE8A3A0730E87DF5520C478865697FDBE4DB24387;
IL2CPP_EXTERN_C String_t* _stringLiteralF5DFCDED6F5B9810386DA4E3C49944FC9A2BDBCB;
IL2CPP_EXTERN_C String_t* _stringLiteralF9490EED751287724BE78361B0E48EA1B8E24C62;
IL2CPP_EXTERN_C String_t* _stringLiteralFE605B72EB9FCBE11100F2C6C4D7EF8582CB6FD0;
IL2CPP_EXTERN_C const RuntimeMethod* AmazonServiceClient_InvokeAsync_TisPutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_m0418AEF71BCD7A4F1376054D52A3DB4A35638A56_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ConstantClass_FindValue_TisEncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_m45BFB226A317DFED373E14B26E0D3493151FC468_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m374496B3B8F3532FC52B465384E6C950DDA79F8B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m28D1709F9D0841D7B183C81F0C0ADF48E0514269_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m48595B6CB451BC53D2617A05AC16BEE5AFE23788_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m2C4D23C823D545395FDBC79BF090573AEA5260C9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ListUnmarshaller_2_Unmarshall_mDFDC8DA37111C8507A123F81BDABC8FB3735857B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ListUnmarshaller_2__ctor_m89D5E1C842B667C35641BB71F900B26FADB8E27A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m9D2B1DB4562661F72F9B89936F8868F603AE171C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m67E3D76494E3E22BBF70C37077DD5978721472D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m6D7DA5C1785CDE161C1059E3ED6AD426AE0D697E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mC4281CDDAF1829194CAF38FB27521B5859B00B23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PutRecordsResultEntryUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_Kinesis_Model_PutRecordsResultEntryU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mC56E04FBA392907DB6AD240FF3EA1156F56ED5C4_RuntimeMethod_var;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t4ACBC9157BC8206A033A06F8C7E2A2328B226AEA 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___entries_1)); }
	inline EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___keys_7)); }
	inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___values_8)); }
	inline ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.Kinesis.Model.PutRecordsResultEntry,Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller>
struct ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8  : public RuntimeObject
{
public:
	// IUnmarshaller Amazon.Runtime.Internal.Transform.ListUnmarshaller`2::iUnmarshaller
	PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * ___iUnmarshaller_0;

public:
	inline static int32_t get_offset_of_iUnmarshaller_0() { return static_cast<int32_t>(offsetof(ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8, ___iUnmarshaller_0)); }
	inline PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * get_iUnmarshaller_0() const { return ___iUnmarshaller_0; }
	inline PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA ** get_address_of_iUnmarshaller_0() { return &___iUnmarshaller_0; }
	inline void set_iUnmarshaller_0(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * value)
	{
		___iUnmarshaller_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iUnmarshaller_0), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry>
struct List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8, ____items_1)); }
	inline PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0* get__items_1() const { return ____items_1; }
	inline PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8_StaticFields, ____emptyArray_5)); }
	inline PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PutRecordsRequestEntryU5BU5D_t20FA0A6CEAD6D2DBC36578D4789CC20A4C89EFB0* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry>
struct List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4, ____items_1)); }
	inline PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455* get__items_1() const { return ____items_1; }
	inline PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4_StaticFields, ____emptyArray_5)); }
	inline PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PutRecordsResultEntryU5BU5D_tC52EE4B5C48B3DC3BD6F4A3F8002A77DF7E42455* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06  : public RuntimeObject
{
public:

public:
};


// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Auth.AWS4Signer Amazon.Runtime.Internal.Auth.AbstractAWSSigner::_aws4Signer
	AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D * ____aws4Signer_0;

public:
	inline static int32_t get_offset_of__aws4Signer_0() { return static_cast<int32_t>(offsetof(AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE, ____aws4Signer_0)); }
	inline AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D * get__aws4Signer_0() const { return ____aws4Signer_0; }
	inline AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D ** get_address_of__aws4Signer_0() { return &____aws4Signer_0; }
	inline void set__aws4Signer_0(AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D * value)
	{
		____aws4Signer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____aws4Signer_0), (void*)value);
	}
};


// Amazon.Kinesis.Internal.AmazonKinesisMetadata
struct AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD  : public RuntimeObject
{
public:

public:
};


// Amazon.Runtime.AmazonServiceClient
struct AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.AmazonServiceClient::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.AmazonServiceClient::_logger
	Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 * ____logger_1;
	// Amazon.Runtime.Internal.EndpointDiscoveryResolverBase Amazon.Runtime.AmazonServiceClient::<EndpointDiscoveryResolver>k__BackingField
	EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC * ___U3CEndpointDiscoveryResolverU3Ek__BackingField_2;
	// Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::<RuntimePipeline>k__BackingField
	RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 * ___U3CRuntimePipelineU3Ek__BackingField_3;
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::<Credentials>k__BackingField
	AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * ___U3CCredentialsU3Ek__BackingField_4;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::<Config>k__BackingField
	RuntimeObject* ___U3CConfigU3Ek__BackingField_5;
	// Amazon.Runtime.Internal.IServiceMetadata Amazon.Runtime.AmazonServiceClient::<ServiceMetadata>k__BackingField
	RuntimeObject* ___U3CServiceMetadataU3Ek__BackingField_6;
	// Amazon.Runtime.PreRequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeMarshallingEvent
	PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 * ___mBeforeMarshallingEvent_7;
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeRequestEvent
	RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * ___mBeforeRequestEvent_8;
	// Amazon.Runtime.ResponseEventHandler Amazon.Runtime.AmazonServiceClient::mAfterResponseEvent
	ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E * ___mAfterResponseEvent_9;
	// Amazon.Runtime.ExceptionEventHandler Amazon.Runtime.AmazonServiceClient::mExceptionEvent
	ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB * ___mExceptionEvent_10;
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::<Signer>k__BackingField
	AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * ___U3CSignerU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ____logger_1)); }
	inline Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 * get__logger_1() const { return ____logger_1; }
	inline Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t3B8E37F562928FBA868DBCF9B447AB1BECB8E466 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____logger_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEndpointDiscoveryResolverU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CEndpointDiscoveryResolverU3Ek__BackingField_2)); }
	inline EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC * get_U3CEndpointDiscoveryResolverU3Ek__BackingField_2() const { return ___U3CEndpointDiscoveryResolverU3Ek__BackingField_2; }
	inline EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC ** get_address_of_U3CEndpointDiscoveryResolverU3Ek__BackingField_2() { return &___U3CEndpointDiscoveryResolverU3Ek__BackingField_2; }
	inline void set_U3CEndpointDiscoveryResolverU3Ek__BackingField_2(EndpointDiscoveryResolverBase_t3EC031A990937A8CDA7AFDC082398FE0F00969FC * value)
	{
		___U3CEndpointDiscoveryResolverU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEndpointDiscoveryResolverU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRuntimePipelineU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CRuntimePipelineU3Ek__BackingField_3)); }
	inline RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 * get_U3CRuntimePipelineU3Ek__BackingField_3() const { return ___U3CRuntimePipelineU3Ek__BackingField_3; }
	inline RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 ** get_address_of_U3CRuntimePipelineU3Ek__BackingField_3() { return &___U3CRuntimePipelineU3Ek__BackingField_3; }
	inline void set_U3CRuntimePipelineU3Ek__BackingField_3(RuntimePipeline_tAD43C7650B5841E2938B5F6B1362FCF3DE411673 * value)
	{
		___U3CRuntimePipelineU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRuntimePipelineU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CCredentialsU3Ek__BackingField_4)); }
	inline AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * get_U3CCredentialsU3Ek__BackingField_4() const { return ___U3CCredentialsU3Ek__BackingField_4; }
	inline AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 ** get_address_of_U3CCredentialsU3Ek__BackingField_4() { return &___U3CCredentialsU3Ek__BackingField_4; }
	inline void set_U3CCredentialsU3Ek__BackingField_4(AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * value)
	{
		___U3CCredentialsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCredentialsU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CConfigU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CConfigU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CConfigU3Ek__BackingField_5() const { return ___U3CConfigU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CConfigU3Ek__BackingField_5() { return &___U3CConfigU3Ek__BackingField_5; }
	inline void set_U3CConfigU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CConfigU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConfigU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServiceMetadataU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CServiceMetadataU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CServiceMetadataU3Ek__BackingField_6() const { return ___U3CServiceMetadataU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CServiceMetadataU3Ek__BackingField_6() { return &___U3CServiceMetadataU3Ek__BackingField_6; }
	inline void set_U3CServiceMetadataU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CServiceMetadataU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CServiceMetadataU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_mBeforeMarshallingEvent_7() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mBeforeMarshallingEvent_7)); }
	inline PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 * get_mBeforeMarshallingEvent_7() const { return ___mBeforeMarshallingEvent_7; }
	inline PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 ** get_address_of_mBeforeMarshallingEvent_7() { return &___mBeforeMarshallingEvent_7; }
	inline void set_mBeforeMarshallingEvent_7(PreRequestEventHandler_t31FB458F43C1725C9CAD6DC38834DDE676E84B12 * value)
	{
		___mBeforeMarshallingEvent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBeforeMarshallingEvent_7), (void*)value);
	}

	inline static int32_t get_offset_of_mBeforeRequestEvent_8() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mBeforeRequestEvent_8)); }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * get_mBeforeRequestEvent_8() const { return ___mBeforeRequestEvent_8; }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 ** get_address_of_mBeforeRequestEvent_8() { return &___mBeforeRequestEvent_8; }
	inline void set_mBeforeRequestEvent_8(RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * value)
	{
		___mBeforeRequestEvent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBeforeRequestEvent_8), (void*)value);
	}

	inline static int32_t get_offset_of_mAfterResponseEvent_9() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mAfterResponseEvent_9)); }
	inline ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E * get_mAfterResponseEvent_9() const { return ___mAfterResponseEvent_9; }
	inline ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E ** get_address_of_mAfterResponseEvent_9() { return &___mAfterResponseEvent_9; }
	inline void set_mAfterResponseEvent_9(ResponseEventHandler_t3A00E62DF2988A7C0E9E488FCF698B2B9B7C4F3E * value)
	{
		___mAfterResponseEvent_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mAfterResponseEvent_9), (void*)value);
	}

	inline static int32_t get_offset_of_mExceptionEvent_10() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___mExceptionEvent_10)); }
	inline ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB * get_mExceptionEvent_10() const { return ___mExceptionEvent_10; }
	inline ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB ** get_address_of_mExceptionEvent_10() { return &___mExceptionEvent_10; }
	inline void set_mExceptionEvent_10(ExceptionEventHandler_tC2F21646EB034EDADD73325B45D4EE112065BABB * value)
	{
		___mExceptionEvent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mExceptionEvent_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSignerU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB, ___U3CSignerU3Ek__BackingField_11)); }
	inline AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * get_U3CSignerU3Ek__BackingField_11() const { return ___U3CSignerU3Ek__BackingField_11; }
	inline AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE ** get_address_of_U3CSignerU3Ek__BackingField_11() { return &___U3CSignerU3Ek__BackingField_11; }
	inline void set_U3CSignerU3Ek__BackingField_11(AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * value)
	{
		___U3CSignerU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSignerU3Ek__BackingField_11), (void*)value);
	}
};


// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55  : public RuntimeObject
{
public:
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonWebServiceRequest::mBeforeRequestEvent
	RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * ___mBeforeRequestEvent_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.StreamUploadProgressCallback>k__BackingField
	EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A * ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1;
	// System.Boolean Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.UseSigV4>k__BackingField
	bool ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_mBeforeRequestEvent_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55, ___mBeforeRequestEvent_0)); }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * get_mBeforeRequestEvent_0() const { return ___mBeforeRequestEvent_0; }
	inline RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 ** get_address_of_mBeforeRequestEvent_0() { return &___mBeforeRequestEvent_0; }
	inline void set_mBeforeRequestEvent_0(RequestEventHandler_t4118E32A9F22CF6693B5BC0E3A8AC06B3370C1F7 * value)
	{
		___mBeforeRequestEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBeforeRequestEvent_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1)); }
	inline EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A * get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A ** get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(EventHandler_1_tBFA73E6BA675FB411A2DE8E5F2FB4E68C628BB1A * value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2)); }
	inline bool get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2(bool value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_2 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// Amazon.Runtime.ConstantClass
struct ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ConstantClass::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6, ___U3CValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueU3Ek__BackingField_2), (void*)value);
	}
};

struct ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_StaticFields
{
public:
	// System.Object Amazon.Runtime.ConstantClass::staticFieldsLock
	RuntimeObject * ___staticFieldsLock_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>> Amazon.Runtime.ConstantClass::staticFields
	Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB * ___staticFields_1;

public:
	inline static int32_t get_offset_of_staticFieldsLock_0() { return static_cast<int32_t>(offsetof(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_StaticFields, ___staticFieldsLock_0)); }
	inline RuntimeObject * get_staticFieldsLock_0() const { return ___staticFieldsLock_0; }
	inline RuntimeObject ** get_address_of_staticFieldsLock_0() { return &___staticFieldsLock_0; }
	inline void set_staticFieldsLock_0(RuntimeObject * value)
	{
		___staticFieldsLock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticFieldsLock_0), (void*)value);
	}

	inline static int32_t get_offset_of_staticFields_1() { return static_cast<int32_t>(offsetof(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_StaticFields, ___staticFields_1)); }
	inline Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB * get_staticFields_1() const { return ___staticFields_1; }
	inline Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB ** get_address_of_staticFields_1() { return &___staticFields_1; }
	inline void set_staticFields_1(Dictionary_2_t71D5D170FB3A03B5DBAAE13089A29754034656FB * value)
	{
		___staticFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticFields_1), (void*)value);
	}
};


// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___numInfo_10)); }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numInfo_10), (void*)value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dateTimeInfo_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textInfo_12)); }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_13), (void*)value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___englishname_14), (void*)value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativename_15), (void*)value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso3lang_16), (void*)value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso2lang_17), (void*)value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___win3lang_18), (void*)value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___territory_19), (void*)value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___native_calendar_names_20)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_calendar_names_20), (void*)value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___compareInfo_21)); }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compareInfo_21), (void*)value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___calendar_24)); }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calendar_24), (void*)value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_culture_25)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_culture_25), (void*)value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cached_serialized_form_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_cultureData_28)); }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cultureData_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariant_culture_info_0), (void*)value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_table_lock_1), (void*)value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_current_culture_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentUICulture_33), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentCulture_34), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_number_35), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_name_36), (void*)value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// Amazon.Runtime.Internal.DefaultRequest
struct DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.Internal.DefaultRequest::parametersCollection
	ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 * ___parametersCollection_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::parametersFacade
	RuntimeObject* ___parametersFacade_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::headers
	RuntimeObject* ___headers_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::subResources
	RuntimeObject* ___subResources_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::pathResources
	RuntimeObject* ___pathResources_4;
	// System.Uri Amazon.Runtime.Internal.DefaultRequest::endpoint
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___endpoint_5;
	// System.String Amazon.Runtime.Internal.DefaultRequest::resourcePath
	String_t* ___resourcePath_6;
	// System.String Amazon.Runtime.Internal.DefaultRequest::serviceName
	String_t* ___serviceName_7;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.DefaultRequest::originalRequest
	AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * ___originalRequest_8;
	// System.Byte[] Amazon.Runtime.Internal.DefaultRequest::content
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___content_9;
	// System.IO.Stream Amazon.Runtime.Internal.DefaultRequest::contentStream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___contentStream_10;
	// System.String Amazon.Runtime.Internal.DefaultRequest::contentStreamHash
	String_t* ___contentStreamHash_11;
	// System.String Amazon.Runtime.Internal.DefaultRequest::httpMethod
	String_t* ___httpMethod_12;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::useQueryString
	bool ___useQueryString_13;
	// System.String Amazon.Runtime.Internal.DefaultRequest::requestName
	String_t* ___requestName_14;
	// Amazon.RegionEndpoint Amazon.Runtime.Internal.DefaultRequest::alternateRegion
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___alternateRegion_15;
	// System.Int64 Amazon.Runtime.Internal.DefaultRequest::originalStreamLength
	int64_t ___originalStreamLength_16;
	// System.Int32 Amazon.Runtime.Internal.DefaultRequest::marshallerVersion
	int32_t ___marshallerVersion_17;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<SetContentFromParameters>k__BackingField
	bool ___U3CSetContentFromParametersU3Ek__BackingField_18;
	// System.String Amazon.Runtime.Internal.DefaultRequest::<HostPrefix>k__BackingField
	String_t* ___U3CHostPrefixU3Ek__BackingField_19;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<Suppress404Exceptions>k__BackingField
	bool ___U3CSuppress404ExceptionsU3Ek__BackingField_20;
	// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.DefaultRequest::<AWS4SignerResult>k__BackingField
	AWS4SigningResult_t7CF5A4B7035B9EF17A794BE62E7F3F0BC88B45B0 * ___U3CAWS4SignerResultU3Ek__BackingField_21;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<UseChunkEncoding>k__BackingField
	bool ___U3CUseChunkEncodingU3Ek__BackingField_22;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<UseSigV4>k__BackingField
	bool ___U3CUseSigV4U3Ek__BackingField_23;
	// System.String Amazon.Runtime.Internal.DefaultRequest::<AuthenticationRegion>k__BackingField
	String_t* ___U3CAuthenticationRegionU3Ek__BackingField_24;
	// System.String Amazon.Runtime.Internal.DefaultRequest::<DeterminedSigningRegion>k__BackingField
	String_t* ___U3CDeterminedSigningRegionU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_parametersCollection_0() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___parametersCollection_0)); }
	inline ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 * get_parametersCollection_0() const { return ___parametersCollection_0; }
	inline ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 ** get_address_of_parametersCollection_0() { return &___parametersCollection_0; }
	inline void set_parametersCollection_0(ParameterCollection_tC3413882E625355B627426F854690BBF79B9A0E3 * value)
	{
		___parametersCollection_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parametersCollection_0), (void*)value);
	}

	inline static int32_t get_offset_of_parametersFacade_1() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___parametersFacade_1)); }
	inline RuntimeObject* get_parametersFacade_1() const { return ___parametersFacade_1; }
	inline RuntimeObject** get_address_of_parametersFacade_1() { return &___parametersFacade_1; }
	inline void set_parametersFacade_1(RuntimeObject* value)
	{
		___parametersFacade_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parametersFacade_1), (void*)value);
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___headers_2)); }
	inline RuntimeObject* get_headers_2() const { return ___headers_2; }
	inline RuntimeObject** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(RuntimeObject* value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headers_2), (void*)value);
	}

	inline static int32_t get_offset_of_subResources_3() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___subResources_3)); }
	inline RuntimeObject* get_subResources_3() const { return ___subResources_3; }
	inline RuntimeObject** get_address_of_subResources_3() { return &___subResources_3; }
	inline void set_subResources_3(RuntimeObject* value)
	{
		___subResources_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___subResources_3), (void*)value);
	}

	inline static int32_t get_offset_of_pathResources_4() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___pathResources_4)); }
	inline RuntimeObject* get_pathResources_4() const { return ___pathResources_4; }
	inline RuntimeObject** get_address_of_pathResources_4() { return &___pathResources_4; }
	inline void set_pathResources_4(RuntimeObject* value)
	{
		___pathResources_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathResources_4), (void*)value);
	}

	inline static int32_t get_offset_of_endpoint_5() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___endpoint_5)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_endpoint_5() const { return ___endpoint_5; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_endpoint_5() { return &___endpoint_5; }
	inline void set_endpoint_5(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___endpoint_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endpoint_5), (void*)value);
	}

	inline static int32_t get_offset_of_resourcePath_6() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___resourcePath_6)); }
	inline String_t* get_resourcePath_6() const { return ___resourcePath_6; }
	inline String_t** get_address_of_resourcePath_6() { return &___resourcePath_6; }
	inline void set_resourcePath_6(String_t* value)
	{
		___resourcePath_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resourcePath_6), (void*)value);
	}

	inline static int32_t get_offset_of_serviceName_7() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___serviceName_7)); }
	inline String_t* get_serviceName_7() const { return ___serviceName_7; }
	inline String_t** get_address_of_serviceName_7() { return &___serviceName_7; }
	inline void set_serviceName_7(String_t* value)
	{
		___serviceName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serviceName_7), (void*)value);
	}

	inline static int32_t get_offset_of_originalRequest_8() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___originalRequest_8)); }
	inline AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * get_originalRequest_8() const { return ___originalRequest_8; }
	inline AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 ** get_address_of_originalRequest_8() { return &___originalRequest_8; }
	inline void set_originalRequest_8(AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * value)
	{
		___originalRequest_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___originalRequest_8), (void*)value);
	}

	inline static int32_t get_offset_of_content_9() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___content_9)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_content_9() const { return ___content_9; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_content_9() { return &___content_9; }
	inline void set_content_9(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___content_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___content_9), (void*)value);
	}

	inline static int32_t get_offset_of_contentStream_10() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___contentStream_10)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_contentStream_10() const { return ___contentStream_10; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_contentStream_10() { return &___contentStream_10; }
	inline void set_contentStream_10(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___contentStream_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentStream_10), (void*)value);
	}

	inline static int32_t get_offset_of_contentStreamHash_11() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___contentStreamHash_11)); }
	inline String_t* get_contentStreamHash_11() const { return ___contentStreamHash_11; }
	inline String_t** get_address_of_contentStreamHash_11() { return &___contentStreamHash_11; }
	inline void set_contentStreamHash_11(String_t* value)
	{
		___contentStreamHash_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentStreamHash_11), (void*)value);
	}

	inline static int32_t get_offset_of_httpMethod_12() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___httpMethod_12)); }
	inline String_t* get_httpMethod_12() const { return ___httpMethod_12; }
	inline String_t** get_address_of_httpMethod_12() { return &___httpMethod_12; }
	inline void set_httpMethod_12(String_t* value)
	{
		___httpMethod_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___httpMethod_12), (void*)value);
	}

	inline static int32_t get_offset_of_useQueryString_13() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___useQueryString_13)); }
	inline bool get_useQueryString_13() const { return ___useQueryString_13; }
	inline bool* get_address_of_useQueryString_13() { return &___useQueryString_13; }
	inline void set_useQueryString_13(bool value)
	{
		___useQueryString_13 = value;
	}

	inline static int32_t get_offset_of_requestName_14() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___requestName_14)); }
	inline String_t* get_requestName_14() const { return ___requestName_14; }
	inline String_t** get_address_of_requestName_14() { return &___requestName_14; }
	inline void set_requestName_14(String_t* value)
	{
		___requestName_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___requestName_14), (void*)value);
	}

	inline static int32_t get_offset_of_alternateRegion_15() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___alternateRegion_15)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_alternateRegion_15() const { return ___alternateRegion_15; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_alternateRegion_15() { return &___alternateRegion_15; }
	inline void set_alternateRegion_15(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___alternateRegion_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alternateRegion_15), (void*)value);
	}

	inline static int32_t get_offset_of_originalStreamLength_16() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___originalStreamLength_16)); }
	inline int64_t get_originalStreamLength_16() const { return ___originalStreamLength_16; }
	inline int64_t* get_address_of_originalStreamLength_16() { return &___originalStreamLength_16; }
	inline void set_originalStreamLength_16(int64_t value)
	{
		___originalStreamLength_16 = value;
	}

	inline static int32_t get_offset_of_marshallerVersion_17() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___marshallerVersion_17)); }
	inline int32_t get_marshallerVersion_17() const { return ___marshallerVersion_17; }
	inline int32_t* get_address_of_marshallerVersion_17() { return &___marshallerVersion_17; }
	inline void set_marshallerVersion_17(int32_t value)
	{
		___marshallerVersion_17 = value;
	}

	inline static int32_t get_offset_of_U3CSetContentFromParametersU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CSetContentFromParametersU3Ek__BackingField_18)); }
	inline bool get_U3CSetContentFromParametersU3Ek__BackingField_18() const { return ___U3CSetContentFromParametersU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CSetContentFromParametersU3Ek__BackingField_18() { return &___U3CSetContentFromParametersU3Ek__BackingField_18; }
	inline void set_U3CSetContentFromParametersU3Ek__BackingField_18(bool value)
	{
		___U3CSetContentFromParametersU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CHostPrefixU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CHostPrefixU3Ek__BackingField_19)); }
	inline String_t* get_U3CHostPrefixU3Ek__BackingField_19() const { return ___U3CHostPrefixU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CHostPrefixU3Ek__BackingField_19() { return &___U3CHostPrefixU3Ek__BackingField_19; }
	inline void set_U3CHostPrefixU3Ek__BackingField_19(String_t* value)
	{
		___U3CHostPrefixU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHostPrefixU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSuppress404ExceptionsU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CSuppress404ExceptionsU3Ek__BackingField_20)); }
	inline bool get_U3CSuppress404ExceptionsU3Ek__BackingField_20() const { return ___U3CSuppress404ExceptionsU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CSuppress404ExceptionsU3Ek__BackingField_20() { return &___U3CSuppress404ExceptionsU3Ek__BackingField_20; }
	inline void set_U3CSuppress404ExceptionsU3Ek__BackingField_20(bool value)
	{
		___U3CSuppress404ExceptionsU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CAWS4SignerResultU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CAWS4SignerResultU3Ek__BackingField_21)); }
	inline AWS4SigningResult_t7CF5A4B7035B9EF17A794BE62E7F3F0BC88B45B0 * get_U3CAWS4SignerResultU3Ek__BackingField_21() const { return ___U3CAWS4SignerResultU3Ek__BackingField_21; }
	inline AWS4SigningResult_t7CF5A4B7035B9EF17A794BE62E7F3F0BC88B45B0 ** get_address_of_U3CAWS4SignerResultU3Ek__BackingField_21() { return &___U3CAWS4SignerResultU3Ek__BackingField_21; }
	inline void set_U3CAWS4SignerResultU3Ek__BackingField_21(AWS4SigningResult_t7CF5A4B7035B9EF17A794BE62E7F3F0BC88B45B0 * value)
	{
		___U3CAWS4SignerResultU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAWS4SignerResultU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUseChunkEncodingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CUseChunkEncodingU3Ek__BackingField_22)); }
	inline bool get_U3CUseChunkEncodingU3Ek__BackingField_22() const { return ___U3CUseChunkEncodingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CUseChunkEncodingU3Ek__BackingField_22() { return &___U3CUseChunkEncodingU3Ek__BackingField_22; }
	inline void set_U3CUseChunkEncodingU3Ek__BackingField_22(bool value)
	{
		___U3CUseChunkEncodingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CUseSigV4U3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CUseSigV4U3Ek__BackingField_23)); }
	inline bool get_U3CUseSigV4U3Ek__BackingField_23() const { return ___U3CUseSigV4U3Ek__BackingField_23; }
	inline bool* get_address_of_U3CUseSigV4U3Ek__BackingField_23() { return &___U3CUseSigV4U3Ek__BackingField_23; }
	inline void set_U3CUseSigV4U3Ek__BackingField_23(bool value)
	{
		___U3CUseSigV4U3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CAuthenticationRegionU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CAuthenticationRegionU3Ek__BackingField_24)); }
	inline String_t* get_U3CAuthenticationRegionU3Ek__BackingField_24() const { return ___U3CAuthenticationRegionU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CAuthenticationRegionU3Ek__BackingField_24() { return &___U3CAuthenticationRegionU3Ek__BackingField_24; }
	inline void set_U3CAuthenticationRegionU3Ek__BackingField_24(String_t* value)
	{
		___U3CAuthenticationRegionU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAuthenticationRegionU3Ek__BackingField_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDeterminedSigningRegionU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4, ___U3CDeterminedSigningRegionU3Ek__BackingField_25)); }
	inline String_t* get_U3CDeterminedSigningRegionU3Ek__BackingField_25() const { return ___U3CDeterminedSigningRegionU3Ek__BackingField_25; }
	inline String_t** get_address_of_U3CDeterminedSigningRegionU3Ek__BackingField_25() { return &___U3CDeterminedSigningRegionU3Ek__BackingField_25; }
	inline void set_U3CDeterminedSigningRegionU3Ek__BackingField_25(String_t* value)
	{
		___U3CDeterminedSigningRegionU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDeterminedSigningRegionU3Ek__BackingField_25), (void*)value);
	}
};


// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_55;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * ___dataItem_56;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_57;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_58;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * ___encoderFallback_59;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * ___decoderFallback_60;

public:
	inline static int32_t get_offset_of_m_codePage_55() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_codePage_55)); }
	inline int32_t get_m_codePage_55() const { return ___m_codePage_55; }
	inline int32_t* get_address_of_m_codePage_55() { return &___m_codePage_55; }
	inline void set_m_codePage_55(int32_t value)
	{
		___m_codePage_55 = value;
	}

	inline static int32_t get_offset_of_dataItem_56() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___dataItem_56)); }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * get_dataItem_56() const { return ___dataItem_56; }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E ** get_address_of_dataItem_56() { return &___dataItem_56; }
	inline void set_dataItem_56(CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * value)
	{
		___dataItem_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_56), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_57() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_deserializedFromEverett_57)); }
	inline bool get_m_deserializedFromEverett_57() const { return ___m_deserializedFromEverett_57; }
	inline bool* get_address_of_m_deserializedFromEverett_57() { return &___m_deserializedFromEverett_57; }
	inline void set_m_deserializedFromEverett_57(bool value)
	{
		___m_deserializedFromEverett_57 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_58() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_isReadOnly_58)); }
	inline bool get_m_isReadOnly_58() const { return ___m_isReadOnly_58; }
	inline bool* get_address_of_m_isReadOnly_58() { return &___m_isReadOnly_58; }
	inline void set_m_isReadOnly_58(bool value)
	{
		___m_isReadOnly_58 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_59() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___encoderFallback_59)); }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * get_encoderFallback_59() const { return ___encoderFallback_59; }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 ** get_address_of_encoderFallback_59() { return &___encoderFallback_59; }
	inline void set_encoderFallback_59(EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * value)
	{
		___encoderFallback_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_59), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_60() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___decoderFallback_60)); }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * get_decoderFallback_60() const { return ___decoderFallback_60; }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D ** get_address_of_decoderFallback_60() { return &___decoderFallback_60; }
	inline void set_decoderFallback_60(DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * value)
	{
		___decoderFallback_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_60), (void*)value);
	}
};

struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_61;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___encodings_8)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_61() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___s_InternalSyncObject_61)); }
	inline RuntimeObject * get_s_InternalSyncObject_61() const { return ___s_InternalSyncObject_61; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_61() { return &___s_InternalSyncObject_61; }
	inline void set_s_InternalSyncObject_61(RuntimeObject * value)
	{
		___s_InternalSyncObject_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_61), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.IntUnmarshaller
struct IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4  : public RuntimeObject
{
public:

public:
};

struct IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.IntUnmarshaller Amazon.Runtime.Internal.Transform.IntUnmarshaller::_instance
	IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_StaticFields, ____instance_0)); }
	inline IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * get__instance_0() const { return ____instance_0; }
	inline IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller
struct InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4  : public RuntimeObject
{
public:

public:
};

struct InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::_instance
	InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_StaticFields, ____instance_0)); }
	inline InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * get__instance_0() const { return ____instance_0; }
	inline InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Runtime.Internal.InvokeOptionsBase
struct InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.InvokeOptionsBase::_requestMarshaller
	RuntimeObject* ____requestMarshaller_0;
	// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.Internal.InvokeOptionsBase::_responseUnmarshaller
	ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21 * ____responseUnmarshaller_1;
	// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.EndpointDiscoveryDataBase,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.InvokeOptionsBase::_endpointDiscoveryMarshaller
	RuntimeObject* ____endpointDiscoveryMarshaller_2;
	// Amazon.Runtime.Internal.EndpointOperationDelegate Amazon.Runtime.Internal.InvokeOptionsBase::_endpointOperation
	EndpointOperationDelegate_t7188E39FDC66C6C6AF51AC9FF3E40758EAA4D1B1 * ____endpointOperation_3;

public:
	inline static int32_t get_offset_of__requestMarshaller_0() { return static_cast<int32_t>(offsetof(InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4, ____requestMarshaller_0)); }
	inline RuntimeObject* get__requestMarshaller_0() const { return ____requestMarshaller_0; }
	inline RuntimeObject** get_address_of__requestMarshaller_0() { return &____requestMarshaller_0; }
	inline void set__requestMarshaller_0(RuntimeObject* value)
	{
		____requestMarshaller_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____requestMarshaller_0), (void*)value);
	}

	inline static int32_t get_offset_of__responseUnmarshaller_1() { return static_cast<int32_t>(offsetof(InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4, ____responseUnmarshaller_1)); }
	inline ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21 * get__responseUnmarshaller_1() const { return ____responseUnmarshaller_1; }
	inline ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21 ** get_address_of__responseUnmarshaller_1() { return &____responseUnmarshaller_1; }
	inline void set__responseUnmarshaller_1(ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21 * value)
	{
		____responseUnmarshaller_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____responseUnmarshaller_1), (void*)value);
	}

	inline static int32_t get_offset_of__endpointDiscoveryMarshaller_2() { return static_cast<int32_t>(offsetof(InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4, ____endpointDiscoveryMarshaller_2)); }
	inline RuntimeObject* get__endpointDiscoveryMarshaller_2() const { return ____endpointDiscoveryMarshaller_2; }
	inline RuntimeObject** get_address_of__endpointDiscoveryMarshaller_2() { return &____endpointDiscoveryMarshaller_2; }
	inline void set__endpointDiscoveryMarshaller_2(RuntimeObject* value)
	{
		____endpointDiscoveryMarshaller_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____endpointDiscoveryMarshaller_2), (void*)value);
	}

	inline static int32_t get_offset_of__endpointOperation_3() { return static_cast<int32_t>(offsetof(InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4, ____endpointOperation_3)); }
	inline EndpointOperationDelegate_t7188E39FDC66C6C6AF51AC9FF3E40758EAA4D1B1 * get__endpointOperation_3() const { return ____endpointOperation_3; }
	inline EndpointOperationDelegate_t7188E39FDC66C6C6AF51AC9FF3E40758EAA4D1B1 ** get_address_of__endpointOperation_3() { return &____endpointOperation_3; }
	inline void set__endpointOperation_3(EndpointOperationDelegate_t7188E39FDC66C6C6AF51AC9FF3E40758EAA4D1B1 * value)
	{
		____endpointOperation_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____endpointOperation_3), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller
struct JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB  : public RuntimeObject
{
public:

public:
};

struct JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::instance
	JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB_StaticFields, ___instance_0)); }
	inline JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB * get_instance_0() const { return ___instance_0; }
	inline JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_0), (void*)value);
	}
};


// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B  : public RuntimeObject
{
public:
	// ThirdParty.Json.LitJson.WriterContext ThirdParty.Json.LitJson.JsonWriter::context
	WriterContext_t78CC992BA30308B8C47F7D1F7DFC20E2CF7E86DA * ___context_1;
	// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext> ThirdParty.Json.LitJson.JsonWriter::ctx_stack
	Stack_1_t913945E3D311A059DC7DFC94BA47E5B1DC5A89B9 * ___ctx_stack_2;
	// System.Boolean ThirdParty.Json.LitJson.JsonWriter::has_reached_end
	bool ___has_reached_end_3;
	// System.Char[] ThirdParty.Json.LitJson.JsonWriter::hex_seq
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___hex_seq_4;
	// System.Int32 ThirdParty.Json.LitJson.JsonWriter::indentation
	int32_t ___indentation_5;
	// System.Int32 ThirdParty.Json.LitJson.JsonWriter::indent_value
	int32_t ___indent_value_6;
	// System.Text.StringBuilder ThirdParty.Json.LitJson.JsonWriter::inst_string_builder
	StringBuilder_t * ___inst_string_builder_7;
	// System.Boolean ThirdParty.Json.LitJson.JsonWriter::pretty_print
	bool ___pretty_print_8;
	// System.Boolean ThirdParty.Json.LitJson.JsonWriter::validate
	bool ___validate_9;
	// System.IO.TextWriter ThirdParty.Json.LitJson.JsonWriter::writer
	TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___writer_10;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___context_1)); }
	inline WriterContext_t78CC992BA30308B8C47F7D1F7DFC20E2CF7E86DA * get_context_1() const { return ___context_1; }
	inline WriterContext_t78CC992BA30308B8C47F7D1F7DFC20E2CF7E86DA ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(WriterContext_t78CC992BA30308B8C47F7D1F7DFC20E2CF7E86DA * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___context_1), (void*)value);
	}

	inline static int32_t get_offset_of_ctx_stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___ctx_stack_2)); }
	inline Stack_1_t913945E3D311A059DC7DFC94BA47E5B1DC5A89B9 * get_ctx_stack_2() const { return ___ctx_stack_2; }
	inline Stack_1_t913945E3D311A059DC7DFC94BA47E5B1DC5A89B9 ** get_address_of_ctx_stack_2() { return &___ctx_stack_2; }
	inline void set_ctx_stack_2(Stack_1_t913945E3D311A059DC7DFC94BA47E5B1DC5A89B9 * value)
	{
		___ctx_stack_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ctx_stack_2), (void*)value);
	}

	inline static int32_t get_offset_of_has_reached_end_3() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___has_reached_end_3)); }
	inline bool get_has_reached_end_3() const { return ___has_reached_end_3; }
	inline bool* get_address_of_has_reached_end_3() { return &___has_reached_end_3; }
	inline void set_has_reached_end_3(bool value)
	{
		___has_reached_end_3 = value;
	}

	inline static int32_t get_offset_of_hex_seq_4() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___hex_seq_4)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_hex_seq_4() const { return ___hex_seq_4; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_hex_seq_4() { return &___hex_seq_4; }
	inline void set_hex_seq_4(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___hex_seq_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hex_seq_4), (void*)value);
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indent_value_6() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___indent_value_6)); }
	inline int32_t get_indent_value_6() const { return ___indent_value_6; }
	inline int32_t* get_address_of_indent_value_6() { return &___indent_value_6; }
	inline void set_indent_value_6(int32_t value)
	{
		___indent_value_6 = value;
	}

	inline static int32_t get_offset_of_inst_string_builder_7() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___inst_string_builder_7)); }
	inline StringBuilder_t * get_inst_string_builder_7() const { return ___inst_string_builder_7; }
	inline StringBuilder_t ** get_address_of_inst_string_builder_7() { return &___inst_string_builder_7; }
	inline void set_inst_string_builder_7(StringBuilder_t * value)
	{
		___inst_string_builder_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inst_string_builder_7), (void*)value);
	}

	inline static int32_t get_offset_of_pretty_print_8() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___pretty_print_8)); }
	inline bool get_pretty_print_8() const { return ___pretty_print_8; }
	inline bool* get_address_of_pretty_print_8() { return &___pretty_print_8; }
	inline void set_pretty_print_8(bool value)
	{
		___pretty_print_8 = value;
	}

	inline static int32_t get_offset_of_validate_9() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___validate_9)); }
	inline bool get_validate_9() const { return ___validate_9; }
	inline bool* get_address_of_validate_9() { return &___validate_9; }
	inline void set_validate_9(bool value)
	{
		___validate_9 = value;
	}

	inline static int32_t get_offset_of_writer_10() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B, ___writer_10)); }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * get_writer_10() const { return ___writer_10; }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 ** get_address_of_writer_10() { return &___writer_10; }
	inline void set_writer_10(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * value)
	{
		___writer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___writer_10), (void*)value);
	}
};

struct JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo ThirdParty.Json.LitJson.JsonWriter::number_format
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___number_format_0;

public:
	inline static int32_t get_offset_of_number_format_0() { return static_cast<int32_t>(offsetof(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_StaticFields, ___number_format_0)); }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * get_number_format_0() const { return ___number_format_0; }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D ** get_address_of_number_format_0() { return &___number_format_0; }
	inline void set_number_format_0(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * value)
	{
		___number_format_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___number_format_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller
struct KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B  : public RuntimeObject
{
public:

public:
};

struct KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::_instance
	KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_StaticFields, ____instance_0)); }
	inline KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * get__instance_0() const { return ____instance_0; }
	inline KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller
struct KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF  : public RuntimeObject
{
public:

public:
};

struct KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::_instance
	KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_StaticFields, ____instance_0)); }
	inline KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * get__instance_0() const { return ____instance_0; }
	inline KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller
struct KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728  : public RuntimeObject
{
public:

public:
};

struct KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::_instance
	KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_StaticFields, ____instance_0)); }
	inline KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * get__instance_0() const { return ____instance_0; }
	inline KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller
struct KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1  : public RuntimeObject
{
public:

public:
};

struct KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::_instance
	KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_StaticFields, ____instance_0)); }
	inline KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * get__instance_0() const { return ____instance_0; }
	inline KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller
struct KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5  : public RuntimeObject
{
public:

public:
};

struct KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::_instance
	KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_StaticFields, ____instance_0)); }
	inline KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * get__instance_0() const { return ____instance_0; }
	inline KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller
struct KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37  : public RuntimeObject
{
public:

public:
};

struct KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::_instance
	KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_StaticFields, ____instance_0)); }
	inline KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * get__instance_0() const { return ____instance_0; }
	inline KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// Amazon.Runtime.Internal.Transform.MarshallerContext
struct MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.IRequest Amazon.Runtime.Internal.Transform.MarshallerContext::<Request>k__BackingField
	RuntimeObject* ___U3CRequestU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952, ___U3CRequestU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CRequestU3Ek__BackingField_0() const { return ___U3CRequestU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CRequestU3Ek__BackingField_0() { return &___U3CRequestU3Ek__BackingField_0; }
	inline void set_U3CRequestU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CRequestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRequestU3Ek__BackingField_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller
struct ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7  : public RuntimeObject
{
public:

public:
};

struct ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::_instance
	ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_StaticFields, ____instance_0)); }
	inline ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * get__instance_0() const { return ____instance_0; }
	inline ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.PutRecordsRequestEntry
struct PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF  : public RuntimeObject
{
public:
	// System.IO.MemoryStream Amazon.Kinesis.Model.PutRecordsRequestEntry::_data
	MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * ____data_0;
	// System.String Amazon.Kinesis.Model.PutRecordsRequestEntry::_explicitHashKey
	String_t* ____explicitHashKey_1;
	// System.String Amazon.Kinesis.Model.PutRecordsRequestEntry::_partitionKey
	String_t* ____partitionKey_2;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF, ____data_0)); }
	inline MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * get__data_0() const { return ____data_0; }
	inline MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C ** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_0), (void*)value);
	}

	inline static int32_t get_offset_of__explicitHashKey_1() { return static_cast<int32_t>(offsetof(PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF, ____explicitHashKey_1)); }
	inline String_t* get__explicitHashKey_1() const { return ____explicitHashKey_1; }
	inline String_t** get_address_of__explicitHashKey_1() { return &____explicitHashKey_1; }
	inline void set__explicitHashKey_1(String_t* value)
	{
		____explicitHashKey_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____explicitHashKey_1), (void*)value);
	}

	inline static int32_t get_offset_of__partitionKey_2() { return static_cast<int32_t>(offsetof(PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF, ____partitionKey_2)); }
	inline String_t* get__partitionKey_2() const { return ____partitionKey_2; }
	inline String_t** get_address_of__partitionKey_2() { return &____partitionKey_2; }
	inline void set__partitionKey_2(String_t* value)
	{
		____partitionKey_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____partitionKey_2), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller
struct PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69  : public RuntimeObject
{
public:

public:
};

struct PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller::Instance
	PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_StaticFields, ___Instance_0)); }
	inline PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * get_Instance_0() const { return ___Instance_0; }
	inline PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller
struct PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15  : public RuntimeObject
{
public:

public:
};

struct PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::_instance
	PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_StaticFields, ____instance_0)); }
	inline PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * get__instance_0() const { return ____instance_0; }
	inline PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Kinesis.Model.PutRecordsResultEntry
struct PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232  : public RuntimeObject
{
public:
	// System.String Amazon.Kinesis.Model.PutRecordsResultEntry::_errorCode
	String_t* ____errorCode_0;
	// System.String Amazon.Kinesis.Model.PutRecordsResultEntry::_errorMessage
	String_t* ____errorMessage_1;
	// System.String Amazon.Kinesis.Model.PutRecordsResultEntry::_sequenceNumber
	String_t* ____sequenceNumber_2;
	// System.String Amazon.Kinesis.Model.PutRecordsResultEntry::_shardId
	String_t* ____shardId_3;

public:
	inline static int32_t get_offset_of__errorCode_0() { return static_cast<int32_t>(offsetof(PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232, ____errorCode_0)); }
	inline String_t* get__errorCode_0() const { return ____errorCode_0; }
	inline String_t** get_address_of__errorCode_0() { return &____errorCode_0; }
	inline void set__errorCode_0(String_t* value)
	{
		____errorCode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____errorCode_0), (void*)value);
	}

	inline static int32_t get_offset_of__errorMessage_1() { return static_cast<int32_t>(offsetof(PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232, ____errorMessage_1)); }
	inline String_t* get__errorMessage_1() const { return ____errorMessage_1; }
	inline String_t** get_address_of__errorMessage_1() { return &____errorMessage_1; }
	inline void set__errorMessage_1(String_t* value)
	{
		____errorMessage_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____errorMessage_1), (void*)value);
	}

	inline static int32_t get_offset_of__sequenceNumber_2() { return static_cast<int32_t>(offsetof(PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232, ____sequenceNumber_2)); }
	inline String_t* get__sequenceNumber_2() const { return ____sequenceNumber_2; }
	inline String_t** get_address_of__sequenceNumber_2() { return &____sequenceNumber_2; }
	inline void set__sequenceNumber_2(String_t* value)
	{
		____sequenceNumber_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sequenceNumber_2), (void*)value);
	}

	inline static int32_t get_offset_of__shardId_3() { return static_cast<int32_t>(offsetof(PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232, ____shardId_3)); }
	inline String_t* get__shardId_3() const { return ____shardId_3; }
	inline String_t** get_address_of__shardId_3() { return &____shardId_3; }
	inline void set__shardId_3(String_t* value)
	{
		____shardId_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____shardId_3), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller
struct PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA  : public RuntimeObject
{
public:

public:
};

struct PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::_instance
	PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_StaticFields, ____instance_0)); }
	inline PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * get__instance_0() const { return ____instance_0; }
	inline PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.RegionEndpoint
struct RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A  : public RuntimeObject
{
public:
	// System.String Amazon.RegionEndpoint::<SystemName>k__BackingField
	String_t* ___U3CSystemNameU3Ek__BackingField_29;
	// System.String Amazon.RegionEndpoint::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_30;

public:
	inline static int32_t get_offset_of_U3CSystemNameU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A, ___U3CSystemNameU3Ek__BackingField_29)); }
	inline String_t* get_U3CSystemNameU3Ek__BackingField_29() const { return ___U3CSystemNameU3Ek__BackingField_29; }
	inline String_t** get_address_of_U3CSystemNameU3Ek__BackingField_29() { return &___U3CSystemNameU3Ek__BackingField_29; }
	inline void set_U3CSystemNameU3Ek__BackingField_29(String_t* value)
	{
		___U3CSystemNameU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSystemNameU3Ek__BackingField_29), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A, ___U3CDisplayNameU3Ek__BackingField_30)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_30() const { return ___U3CDisplayNameU3Ek__BackingField_30; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_30() { return &___U3CDisplayNameU3Ek__BackingField_30; }
	inline void set_U3CDisplayNameU3Ek__BackingField_30(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDisplayNameU3Ek__BackingField_30), (void*)value);
	}
};

struct RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint> Amazon.RegionEndpoint::_hashBySystemName
	Dictionary_2_t4BA118A7A726F5C0B3CF2922C17F6E49A7B4BA0D * ____hashBySystemName_0;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USEast1_1;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast1Regional
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USEast1Regional_2;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast2
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USEast2_3;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USWest1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USWest1_4;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USWest2
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USWest2_5;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUNorth1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___EUNorth1_6;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___EUWest1_7;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest2
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___EUWest2_8;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest3
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___EUWest3_9;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUCentral1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___EUCentral1_10;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUSouth1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___EUSouth1_11;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APEast1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APEast1_12;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APNortheast1_13;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast2
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APNortheast2_14;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast3
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APNortheast3_15;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSouth1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APSouth1_16;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSoutheast1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APSoutheast1_17;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSoutheast2
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___APSoutheast2_18;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::SAEast1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___SAEast1_19;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USGovCloudEast1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USGovCloudEast1_20;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USGovCloudWest1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___USGovCloudWest1_21;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CNNorth1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___CNNorth1_22;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CNNorthWest1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___CNNorthWest1_23;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CACentral1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___CACentral1_24;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::MESouth1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___MESouth1_25;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::AFSouth1
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___AFSouth1_26;
	// System.Collections.Generic.Dictionary`2<Amazon.RegionEndpoint,Amazon.RegionEndpoint> Amazon.RegionEndpoint::_hashRegionEndpointOverride
	Dictionary_2_t17E91A0A878220377BD1CC373D5F93529B46F9A2 * ____hashRegionEndpointOverride_27;
	// Amazon.Internal.IRegionEndpointProvider Amazon.RegionEndpoint::_regionEndpointProvider
	RuntimeObject* ____regionEndpointProvider_28;

public:
	inline static int32_t get_offset_of__hashBySystemName_0() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ____hashBySystemName_0)); }
	inline Dictionary_2_t4BA118A7A726F5C0B3CF2922C17F6E49A7B4BA0D * get__hashBySystemName_0() const { return ____hashBySystemName_0; }
	inline Dictionary_2_t4BA118A7A726F5C0B3CF2922C17F6E49A7B4BA0D ** get_address_of__hashBySystemName_0() { return &____hashBySystemName_0; }
	inline void set__hashBySystemName_0(Dictionary_2_t4BA118A7A726F5C0B3CF2922C17F6E49A7B4BA0D * value)
	{
		____hashBySystemName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hashBySystemName_0), (void*)value);
	}

	inline static int32_t get_offset_of_USEast1_1() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USEast1_1)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USEast1_1() const { return ___USEast1_1; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USEast1_1() { return &___USEast1_1; }
	inline void set_USEast1_1(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USEast1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USEast1_1), (void*)value);
	}

	inline static int32_t get_offset_of_USEast1Regional_2() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USEast1Regional_2)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USEast1Regional_2() const { return ___USEast1Regional_2; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USEast1Regional_2() { return &___USEast1Regional_2; }
	inline void set_USEast1Regional_2(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USEast1Regional_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USEast1Regional_2), (void*)value);
	}

	inline static int32_t get_offset_of_USEast2_3() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USEast2_3)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USEast2_3() const { return ___USEast2_3; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USEast2_3() { return &___USEast2_3; }
	inline void set_USEast2_3(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USEast2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USEast2_3), (void*)value);
	}

	inline static int32_t get_offset_of_USWest1_4() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USWest1_4)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USWest1_4() const { return ___USWest1_4; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USWest1_4() { return &___USWest1_4; }
	inline void set_USWest1_4(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USWest1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USWest1_4), (void*)value);
	}

	inline static int32_t get_offset_of_USWest2_5() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USWest2_5)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USWest2_5() const { return ___USWest2_5; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USWest2_5() { return &___USWest2_5; }
	inline void set_USWest2_5(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USWest2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USWest2_5), (void*)value);
	}

	inline static int32_t get_offset_of_EUNorth1_6() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___EUNorth1_6)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_EUNorth1_6() const { return ___EUNorth1_6; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_EUNorth1_6() { return &___EUNorth1_6; }
	inline void set_EUNorth1_6(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___EUNorth1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EUNorth1_6), (void*)value);
	}

	inline static int32_t get_offset_of_EUWest1_7() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___EUWest1_7)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_EUWest1_7() const { return ___EUWest1_7; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_EUWest1_7() { return &___EUWest1_7; }
	inline void set_EUWest1_7(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___EUWest1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EUWest1_7), (void*)value);
	}

	inline static int32_t get_offset_of_EUWest2_8() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___EUWest2_8)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_EUWest2_8() const { return ___EUWest2_8; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_EUWest2_8() { return &___EUWest2_8; }
	inline void set_EUWest2_8(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___EUWest2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EUWest2_8), (void*)value);
	}

	inline static int32_t get_offset_of_EUWest3_9() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___EUWest3_9)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_EUWest3_9() const { return ___EUWest3_9; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_EUWest3_9() { return &___EUWest3_9; }
	inline void set_EUWest3_9(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___EUWest3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EUWest3_9), (void*)value);
	}

	inline static int32_t get_offset_of_EUCentral1_10() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___EUCentral1_10)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_EUCentral1_10() const { return ___EUCentral1_10; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_EUCentral1_10() { return &___EUCentral1_10; }
	inline void set_EUCentral1_10(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___EUCentral1_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EUCentral1_10), (void*)value);
	}

	inline static int32_t get_offset_of_EUSouth1_11() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___EUSouth1_11)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_EUSouth1_11() const { return ___EUSouth1_11; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_EUSouth1_11() { return &___EUSouth1_11; }
	inline void set_EUSouth1_11(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___EUSouth1_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EUSouth1_11), (void*)value);
	}

	inline static int32_t get_offset_of_APEast1_12() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APEast1_12)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APEast1_12() const { return ___APEast1_12; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APEast1_12() { return &___APEast1_12; }
	inline void set_APEast1_12(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APEast1_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APEast1_12), (void*)value);
	}

	inline static int32_t get_offset_of_APNortheast1_13() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APNortheast1_13)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APNortheast1_13() const { return ___APNortheast1_13; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APNortheast1_13() { return &___APNortheast1_13; }
	inline void set_APNortheast1_13(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APNortheast1_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APNortheast1_13), (void*)value);
	}

	inline static int32_t get_offset_of_APNortheast2_14() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APNortheast2_14)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APNortheast2_14() const { return ___APNortheast2_14; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APNortheast2_14() { return &___APNortheast2_14; }
	inline void set_APNortheast2_14(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APNortheast2_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APNortheast2_14), (void*)value);
	}

	inline static int32_t get_offset_of_APNortheast3_15() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APNortheast3_15)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APNortheast3_15() const { return ___APNortheast3_15; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APNortheast3_15() { return &___APNortheast3_15; }
	inline void set_APNortheast3_15(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APNortheast3_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APNortheast3_15), (void*)value);
	}

	inline static int32_t get_offset_of_APSouth1_16() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APSouth1_16)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APSouth1_16() const { return ___APSouth1_16; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APSouth1_16() { return &___APSouth1_16; }
	inline void set_APSouth1_16(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APSouth1_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APSouth1_16), (void*)value);
	}

	inline static int32_t get_offset_of_APSoutheast1_17() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APSoutheast1_17)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APSoutheast1_17() const { return ___APSoutheast1_17; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APSoutheast1_17() { return &___APSoutheast1_17; }
	inline void set_APSoutheast1_17(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APSoutheast1_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APSoutheast1_17), (void*)value);
	}

	inline static int32_t get_offset_of_APSoutheast2_18() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___APSoutheast2_18)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_APSoutheast2_18() const { return ___APSoutheast2_18; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_APSoutheast2_18() { return &___APSoutheast2_18; }
	inline void set_APSoutheast2_18(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___APSoutheast2_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___APSoutheast2_18), (void*)value);
	}

	inline static int32_t get_offset_of_SAEast1_19() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___SAEast1_19)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_SAEast1_19() const { return ___SAEast1_19; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_SAEast1_19() { return &___SAEast1_19; }
	inline void set_SAEast1_19(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___SAEast1_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SAEast1_19), (void*)value);
	}

	inline static int32_t get_offset_of_USGovCloudEast1_20() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USGovCloudEast1_20)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USGovCloudEast1_20() const { return ___USGovCloudEast1_20; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USGovCloudEast1_20() { return &___USGovCloudEast1_20; }
	inline void set_USGovCloudEast1_20(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USGovCloudEast1_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USGovCloudEast1_20), (void*)value);
	}

	inline static int32_t get_offset_of_USGovCloudWest1_21() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___USGovCloudWest1_21)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_USGovCloudWest1_21() const { return ___USGovCloudWest1_21; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_USGovCloudWest1_21() { return &___USGovCloudWest1_21; }
	inline void set_USGovCloudWest1_21(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___USGovCloudWest1_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___USGovCloudWest1_21), (void*)value);
	}

	inline static int32_t get_offset_of_CNNorth1_22() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___CNNorth1_22)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_CNNorth1_22() const { return ___CNNorth1_22; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_CNNorth1_22() { return &___CNNorth1_22; }
	inline void set_CNNorth1_22(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___CNNorth1_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CNNorth1_22), (void*)value);
	}

	inline static int32_t get_offset_of_CNNorthWest1_23() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___CNNorthWest1_23)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_CNNorthWest1_23() const { return ___CNNorthWest1_23; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_CNNorthWest1_23() { return &___CNNorthWest1_23; }
	inline void set_CNNorthWest1_23(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___CNNorthWest1_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CNNorthWest1_23), (void*)value);
	}

	inline static int32_t get_offset_of_CACentral1_24() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___CACentral1_24)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_CACentral1_24() const { return ___CACentral1_24; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_CACentral1_24() { return &___CACentral1_24; }
	inline void set_CACentral1_24(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___CACentral1_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CACentral1_24), (void*)value);
	}

	inline static int32_t get_offset_of_MESouth1_25() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___MESouth1_25)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_MESouth1_25() const { return ___MESouth1_25; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_MESouth1_25() { return &___MESouth1_25; }
	inline void set_MESouth1_25(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___MESouth1_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MESouth1_25), (void*)value);
	}

	inline static int32_t get_offset_of_AFSouth1_26() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ___AFSouth1_26)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_AFSouth1_26() const { return ___AFSouth1_26; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_AFSouth1_26() { return &___AFSouth1_26; }
	inline void set_AFSouth1_26(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___AFSouth1_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AFSouth1_26), (void*)value);
	}

	inline static int32_t get_offset_of__hashRegionEndpointOverride_27() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ____hashRegionEndpointOverride_27)); }
	inline Dictionary_2_t17E91A0A878220377BD1CC373D5F93529B46F9A2 * get__hashRegionEndpointOverride_27() const { return ____hashRegionEndpointOverride_27; }
	inline Dictionary_2_t17E91A0A878220377BD1CC373D5F93529B46F9A2 ** get_address_of__hashRegionEndpointOverride_27() { return &____hashRegionEndpointOverride_27; }
	inline void set__hashRegionEndpointOverride_27(Dictionary_2_t17E91A0A878220377BD1CC373D5F93529B46F9A2 * value)
	{
		____hashRegionEndpointOverride_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hashRegionEndpointOverride_27), (void*)value);
	}

	inline static int32_t get_offset_of__regionEndpointProvider_28() { return static_cast<int32_t>(offsetof(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A_StaticFields, ____regionEndpointProvider_28)); }
	inline RuntimeObject* get__regionEndpointProvider_28() const { return ____regionEndpointProvider_28; }
	inline RuntimeObject** get_address_of__regionEndpointProvider_28() { return &____regionEndpointProvider_28; }
	inline void set__regionEndpointProvider_28(RuntimeObject* value)
	{
		____regionEndpointProvider_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____regionEndpointProvider_28), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller
struct ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01  : public RuntimeObject
{
public:

public:
};

struct ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::_instance
	ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_StaticFields, ____instance_0)); }
	inline ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * get__instance_0() const { return ____instance_0; }
	inline ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.StringUnmarshaller
struct StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7  : public RuntimeObject
{
public:

public:
};

struct StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.StringUnmarshaller Amazon.Runtime.Internal.Transform.StringUnmarshaller::_instance
	StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_StaticFields, ____instance_0)); }
	inline StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * get__instance_0() const { return ____instance_0; }
	inline StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::disposed
	bool ___disposed_0;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::<MaintainResponseBody>k__BackingField
	bool ___U3CMaintainResponseBodyU3Ek__BackingField_1;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::<IsException>k__BackingField
	bool ___U3CIsExceptionU3Ek__BackingField_2;
	// ThirdParty.Ionic.Zlib.CrcCalculatorStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<CrcStream>k__BackingField
	CrcCalculatorStream_t3B289C1840AE2D3C4EDDF8A7D59061A1B39AFB55 * ___U3CCrcStreamU3Ek__BackingField_3;
	// System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::<Crc32Result>k__BackingField
	int32_t ___U3CCrc32ResultU3Ek__BackingField_4;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WebResponseData>k__BackingField
	RuntimeObject* ___U3CWebResponseDataU3Ek__BackingField_5;
	// Amazon.Runtime.Internal.Util.CachingWrapperStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WrappingStream>k__BackingField
	CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D * ___U3CWrappingStreamU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___U3CMaintainResponseBodyU3Ek__BackingField_1)); }
	inline bool get_U3CMaintainResponseBodyU3Ek__BackingField_1() const { return ___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return &___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline void set_U3CMaintainResponseBodyU3Ek__BackingField_1(bool value)
	{
		___U3CMaintainResponseBodyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsExceptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___U3CIsExceptionU3Ek__BackingField_2)); }
	inline bool get_U3CIsExceptionU3Ek__BackingField_2() const { return ___U3CIsExceptionU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsExceptionU3Ek__BackingField_2() { return &___U3CIsExceptionU3Ek__BackingField_2; }
	inline void set_U3CIsExceptionU3Ek__BackingField_2(bool value)
	{
		___U3CIsExceptionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCrcStreamU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___U3CCrcStreamU3Ek__BackingField_3)); }
	inline CrcCalculatorStream_t3B289C1840AE2D3C4EDDF8A7D59061A1B39AFB55 * get_U3CCrcStreamU3Ek__BackingField_3() const { return ___U3CCrcStreamU3Ek__BackingField_3; }
	inline CrcCalculatorStream_t3B289C1840AE2D3C4EDDF8A7D59061A1B39AFB55 ** get_address_of_U3CCrcStreamU3Ek__BackingField_3() { return &___U3CCrcStreamU3Ek__BackingField_3; }
	inline void set_U3CCrcStreamU3Ek__BackingField_3(CrcCalculatorStream_t3B289C1840AE2D3C4EDDF8A7D59061A1B39AFB55 * value)
	{
		___U3CCrcStreamU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCrcStreamU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCrc32ResultU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___U3CCrc32ResultU3Ek__BackingField_4)); }
	inline int32_t get_U3CCrc32ResultU3Ek__BackingField_4() const { return ___U3CCrc32ResultU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CCrc32ResultU3Ek__BackingField_4() { return &___U3CCrc32ResultU3Ek__BackingField_4; }
	inline void set_U3CCrc32ResultU3Ek__BackingField_4(int32_t value)
	{
		___U3CCrc32ResultU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CWebResponseDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___U3CWebResponseDataU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CWebResponseDataU3Ek__BackingField_5() const { return ___U3CWebResponseDataU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CWebResponseDataU3Ek__BackingField_5() { return &___U3CWebResponseDataU3Ek__BackingField_5; }
	inline void set_U3CWebResponseDataU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CWebResponseDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CWebResponseDataU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CWrappingStreamU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B, ___U3CWrappingStreamU3Ek__BackingField_6)); }
	inline CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D * get_U3CWrappingStreamU3Ek__BackingField_6() const { return ___U3CWrappingStreamU3Ek__BackingField_6; }
	inline CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D ** get_address_of_U3CWrappingStreamU3Ek__BackingField_6() { return &___U3CWrappingStreamU3Ek__BackingField_6; }
	inline void set_U3CWrappingStreamU3Ek__BackingField_6(CachingWrapperStream_t3FA5D01579E65F788E88AA8A4D886E8E4EC32E5D * value)
	{
		___U3CWrappingStreamU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CWrappingStreamU3Ek__BackingField_6), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<Amazon.Kinesis.Model.PutRecordsRequestEntry>
struct Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B, ___list_0)); }
	inline List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * get_list_0() const { return ___list_0; }
	inline List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B, ___current_3)); }
	inline PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * get_current_3() const { return ___current_3; }
	inline PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Nullable`1<System.Int32>
struct Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// Amazon.Kinesis.AmazonKinesisClient
struct AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0  : public AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB
{
public:

public:
};

struct AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_StaticFields
{
public:
	// Amazon.Runtime.Internal.IServiceMetadata Amazon.Kinesis.AmazonKinesisClient::serviceMetadata
	RuntimeObject* ___serviceMetadata_12;

public:
	inline static int32_t get_offset_of_serviceMetadata_12() { return static_cast<int32_t>(offsetof(AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_StaticFields, ___serviceMetadata_12)); }
	inline RuntimeObject* get_serviceMetadata_12() const { return ___serviceMetadata_12; }
	inline RuntimeObject** get_address_of_serviceMetadata_12() { return &___serviceMetadata_12; }
	inline void set_serviceMetadata_12(RuntimeObject* value)
	{
		___serviceMetadata_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serviceMetadata_12), (void*)value);
	}
};


// Amazon.Kinesis.AmazonKinesisRequest
struct AmazonKinesisRequest_t6FADAEC3B6BFB9A305B4B0D75615BCC7DD5B0635  : public AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD, ___m_source_0)); }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_source_0), (void*)value);
	}
};

struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActionToActionObjShunt_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_pinvoke
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD_marshaled_com
{
	CancellationTokenSource_t78B989179DE23EDD36F870FFEE20A15D6D3C65B3 * ___m_source_0;
};

// Amazon.Kinesis.EncryptionType
struct EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F  : public ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6
{
public:

public:
};

struct EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_StaticFields
{
public:
	// Amazon.Kinesis.EncryptionType Amazon.Kinesis.EncryptionType::KMS
	EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ___KMS_3;
	// Amazon.Kinesis.EncryptionType Amazon.Kinesis.EncryptionType::NONE
	EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ___NONE_4;

public:
	inline static int32_t get_offset_of_KMS_3() { return static_cast<int32_t>(offsetof(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_StaticFields, ___KMS_3)); }
	inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * get_KMS_3() const { return ___KMS_3; }
	inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F ** get_address_of_KMS_3() { return &___KMS_3; }
	inline void set_KMS_3(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * value)
	{
		___KMS_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___KMS_3), (void*)value);
	}

	inline static int32_t get_offset_of_NONE_4() { return static_cast<int32_t>(offsetof(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_StaticFields, ___NONE_4)); }
	inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * get_NONE_4() const { return ___NONE_4; }
	inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F ** get_address_of_NONE_4() { return &___NONE_4; }
	inline void set_NONE_4(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * value)
	{
		___NONE_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NONE_4), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// Amazon.Runtime.Internal.InvokeOptions
struct InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55  : public InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4
{
public:

public:
};


// Amazon.Runtime.Internal.Transform.JsonMarshallerContext
struct JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315  : public MarshallerContext_t6FACAC575C73317718CE9CEF8102E7FF6C35D952
{
public:
	// ThirdParty.Json.LitJson.JsonWriter Amazon.Runtime.Internal.Transform.JsonMarshallerContext::<Writer>k__BackingField
	JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * ___U3CWriterU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWriterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315, ___U3CWriterU3Ek__BackingField_1)); }
	inline JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * get_U3CWriterU3Ek__BackingField_1() const { return ___U3CWriterU3Ek__BackingField_1; }
	inline JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B ** get_address_of_U3CWriterU3Ek__BackingField_1() { return &___U3CWriterU3Ek__BackingField_1; }
	inline void set_U3CWriterU3Ek__BackingField_1(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * value)
	{
		___U3CWriterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CWriterU3Ek__BackingField_1), (void*)value);
	}
};


// Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller
struct JsonResponseUnmarshaller_t6A1B8EBE5327D6B9A39E51DB1A23C2593CFA1AFF  : public ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21
{
public:

public:
};


// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeReadWriteTask_2), (void*)value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asyncActiveSemaphore_3), (void*)value);
	}
};

struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields, ___Null_1)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_Null_1() const { return ___Null_1; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}
};


// System.IO.TextWriter
struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___CoreNewLine_9;
	// System.IFormatProvider System.IO.TextWriter::InternalFormatProvider
	RuntimeObject* ___InternalFormatProvider_10;

public:
	inline static int32_t get_offset_of_CoreNewLine_9() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643, ___CoreNewLine_9)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_CoreNewLine_9() const { return ___CoreNewLine_9; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_CoreNewLine_9() { return &___CoreNewLine_9; }
	inline void set_CoreNewLine_9(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___CoreNewLine_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CoreNewLine_9), (void*)value);
	}

	inline static int32_t get_offset_of_InternalFormatProvider_10() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643, ___InternalFormatProvider_10)); }
	inline RuntimeObject* get_InternalFormatProvider_10() const { return ___InternalFormatProvider_10; }
	inline RuntimeObject** get_address_of_InternalFormatProvider_10() { return &___InternalFormatProvider_10; }
	inline void set_InternalFormatProvider_10(RuntimeObject* value)
	{
		___InternalFormatProvider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InternalFormatProvider_10), (void*)value);
	}
};

struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___Null_1;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteCharDelegate_2;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteStringDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteStringDelegate_3;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharArrayRangeDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteCharArrayRangeDelegate_4;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineCharDelegate_5;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineStringDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineStringDelegate_6;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharArrayRangeDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineCharArrayRangeDelegate_7;
	// System.Action`1<System.Object> System.IO.TextWriter::_FlushDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____FlushDelegate_8;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ___Null_1)); }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharDelegate_2() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteCharDelegate_2)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteCharDelegate_2() const { return ____WriteCharDelegate_2; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteCharDelegate_2() { return &____WriteCharDelegate_2; }
	inline void set__WriteCharDelegate_2(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteCharDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharDelegate_2), (void*)value);
	}

	inline static int32_t get_offset_of__WriteStringDelegate_3() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteStringDelegate_3)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteStringDelegate_3() const { return ____WriteStringDelegate_3; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteStringDelegate_3() { return &____WriteStringDelegate_3; }
	inline void set__WriteStringDelegate_3(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteStringDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteStringDelegate_3), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharArrayRangeDelegate_4() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteCharArrayRangeDelegate_4)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteCharArrayRangeDelegate_4() const { return ____WriteCharArrayRangeDelegate_4; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteCharArrayRangeDelegate_4() { return &____WriteCharArrayRangeDelegate_4; }
	inline void set__WriteCharArrayRangeDelegate_4(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteCharArrayRangeDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharArrayRangeDelegate_4), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharDelegate_5() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineCharDelegate_5)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineCharDelegate_5() const { return ____WriteLineCharDelegate_5; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineCharDelegate_5() { return &____WriteLineCharDelegate_5; }
	inline void set__WriteLineCharDelegate_5(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineCharDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharDelegate_5), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineStringDelegate_6() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineStringDelegate_6)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineStringDelegate_6() const { return ____WriteLineStringDelegate_6; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineStringDelegate_6() { return &____WriteLineStringDelegate_6; }
	inline void set__WriteLineStringDelegate_6(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineStringDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineStringDelegate_6), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharArrayRangeDelegate_7() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineCharArrayRangeDelegate_7)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineCharArrayRangeDelegate_7() const { return ____WriteLineCharArrayRangeDelegate_7; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineCharArrayRangeDelegate_7() { return &____WriteLineCharArrayRangeDelegate_7; }
	inline void set__WriteLineCharArrayRangeDelegate_7(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineCharArrayRangeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharArrayRangeDelegate_7), (void*)value);
	}

	inline static int32_t get_offset_of__FlushDelegate_8() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____FlushDelegate_8)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__FlushDelegate_8() const { return ____FlushDelegate_8; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__FlushDelegate_8() { return &____FlushDelegate_8; }
	inline void set__FlushDelegate_8(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____FlushDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____FlushDelegate_8), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Amazon.Runtime.ErrorType
struct ErrorType_t971F24890E07794830AD8190527B9517D96FDA25 
{
public:
	// System.Int32 Amazon.Runtime.ErrorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ErrorType_t971F24890E07794830AD8190527B9517D96FDA25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Net.HttpStatusCode
struct HttpStatusCode_tFCB1BA96A101857DA7C390345DE43B77F9567D98 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpStatusCode_tFCB1BA96A101857DA7C390345DE43B77F9567D98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ThirdParty.Json.LitJson.JsonToken
struct JsonToken_tEB3578626FD835DEC938370B8BFC460C299023E4 
{
public:
	// System.Int32 ThirdParty.Json.LitJson.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_tEB3578626FD835DEC938370B8BFC460C299023E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.MemoryStream
struct MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C  : public Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____buffer_4;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_5;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_6;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_7;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_8;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_9;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_10;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_11;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 * ____lastReadTask_13;

public:
	inline static int32_t get_offset_of__buffer_4() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____buffer_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__buffer_4() const { return ____buffer_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__buffer_4() { return &____buffer_4; }
	inline void set__buffer_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____buffer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buffer_4), (void*)value);
	}

	inline static int32_t get_offset_of__origin_5() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____origin_5)); }
	inline int32_t get__origin_5() const { return ____origin_5; }
	inline int32_t* get_address_of__origin_5() { return &____origin_5; }
	inline void set__origin_5(int32_t value)
	{
		____origin_5 = value;
	}

	inline static int32_t get_offset_of__position_6() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____position_6)); }
	inline int32_t get__position_6() const { return ____position_6; }
	inline int32_t* get_address_of__position_6() { return &____position_6; }
	inline void set__position_6(int32_t value)
	{
		____position_6 = value;
	}

	inline static int32_t get_offset_of__length_7() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____length_7)); }
	inline int32_t get__length_7() const { return ____length_7; }
	inline int32_t* get_address_of__length_7() { return &____length_7; }
	inline void set__length_7(int32_t value)
	{
		____length_7 = value;
	}

	inline static int32_t get_offset_of__capacity_8() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____capacity_8)); }
	inline int32_t get__capacity_8() const { return ____capacity_8; }
	inline int32_t* get_address_of__capacity_8() { return &____capacity_8; }
	inline void set__capacity_8(int32_t value)
	{
		____capacity_8 = value;
	}

	inline static int32_t get_offset_of__expandable_9() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____expandable_9)); }
	inline bool get__expandable_9() const { return ____expandable_9; }
	inline bool* get_address_of__expandable_9() { return &____expandable_9; }
	inline void set__expandable_9(bool value)
	{
		____expandable_9 = value;
	}

	inline static int32_t get_offset_of__writable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____writable_10)); }
	inline bool get__writable_10() const { return ____writable_10; }
	inline bool* get_address_of__writable_10() { return &____writable_10; }
	inline void set__writable_10(bool value)
	{
		____writable_10 = value;
	}

	inline static int32_t get_offset_of__exposable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____exposable_11)); }
	inline bool get__exposable_11() const { return ____exposable_11; }
	inline bool* get_address_of__exposable_11() { return &____exposable_11; }
	inline void set__exposable_11(bool value)
	{
		____exposable_11 = value;
	}

	inline static int32_t get_offset_of__isOpen_12() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____isOpen_12)); }
	inline bool get__isOpen_12() const { return ____isOpen_12; }
	inline bool* get_address_of__isOpen_12() { return &____isOpen_12; }
	inline void set__isOpen_12(bool value)
	{
		____isOpen_12 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_13() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____lastReadTask_13)); }
	inline Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 * get__lastReadTask_13() const { return ____lastReadTask_13; }
	inline Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 ** get_address_of__lastReadTask_13() { return &____lastReadTask_13; }
	inline void set__lastReadTask_13(Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 * value)
	{
		____lastReadTask_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastReadTask_13), (void*)value);
	}
};


// Amazon.Kinesis.Model.PutRecordsRequest
struct PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD  : public AmazonKinesisRequest_t6FADAEC3B6BFB9A305B4B0D75615BCC7DD5B0635
{
public:
	// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry> Amazon.Kinesis.Model.PutRecordsRequest::_records
	List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * ____records_3;
	// System.String Amazon.Kinesis.Model.PutRecordsRequest::_streamName
	String_t* ____streamName_4;

public:
	inline static int32_t get_offset_of__records_3() { return static_cast<int32_t>(offsetof(PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD, ____records_3)); }
	inline List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * get__records_3() const { return ____records_3; }
	inline List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 ** get_address_of__records_3() { return &____records_3; }
	inline void set__records_3(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * value)
	{
		____records_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____records_3), (void*)value);
	}

	inline static int32_t get_offset_of__streamName_4() { return static_cast<int32_t>(offsetof(PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD, ____streamName_4)); }
	inline String_t* get__streamName_4() const { return ____streamName_4; }
	inline String_t** get_address_of__streamName_4() { return &____streamName_4; }
	inline void set__streamName_4(String_t* value)
	{
		____streamName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____streamName_4), (void*)value);
	}
};


// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller
struct PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C  : public JsonResponseUnmarshaller_t6A1B8EBE5327D6B9A39E51DB1A23C2593CFA1AFF
{
public:

public:
};

struct PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_StaticFields
{
public:
	// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::_instance
	PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_StaticFields, ____instance_0)); }
	inline PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * get__instance_0() const { return ____instance_0; }
	inline PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// Amazon.Runtime.RequestRetryMode
struct RequestRetryMode_t52F6CA0E36D24CEEC417A3A3AD8F61B13A2177BC 
{
public:
	// System.Int32 Amazon.Runtime.RequestRetryMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestRetryMode_t52F6CA0E36D24CEEC417A3A3AD8F61B13A2177BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Amazon.Runtime.SigningAlgorithm
struct SigningAlgorithm_t60B037561C24B69281488E75C41F970916CEA363 
{
public:
	// System.Int32 Amazon.Runtime.SigningAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SigningAlgorithm_t60B037561C24B69281488E75C41F970916CEA363, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.StringWriter
struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839  : public TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643
{
public:
	// System.Text.StringBuilder System.IO.StringWriter::_sb
	StringBuilder_t * ____sb_12;
	// System.Boolean System.IO.StringWriter::_isOpen
	bool ____isOpen_13;

public:
	inline static int32_t get_offset_of__sb_12() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839, ____sb_12)); }
	inline StringBuilder_t * get__sb_12() const { return ____sb_12; }
	inline StringBuilder_t ** get_address_of__sb_12() { return &____sb_12; }
	inline void set__sb_12(StringBuilder_t * value)
	{
		____sb_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sb_12), (void*)value);
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}
};

struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_StaticFields
{
public:
	// System.Text.UnicodeEncoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StringWriter::m_encoding
	UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * ___m_encoding_11;

public:
	inline static int32_t get_offset_of_m_encoding_11() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_StaticFields, ___m_encoding_11)); }
	inline UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * get_m_encoding_11() const { return ___m_encoding_11; }
	inline UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 ** get_address_of_m_encoding_11() { return &___m_encoding_11; }
	inline void set_m_encoding_11(UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * value)
	{
		___m_encoding_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_encoding_11), (void*)value);
	}
};


// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_28;
	// System.Threading.Tasks.Task/ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * ___m_contingentProperties_33;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskScheduler_7)); }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_parent_8)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_28() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_continuationObject_28)); }
	inline RuntimeObject * get_m_continuationObject_28() const { return ___m_continuationObject_28; }
	inline RuntimeObject ** get_address_of_m_continuationObject_28() { return &___m_continuationObject_28; }
	inline void set_m_continuationObject_28(RuntimeObject * value)
	{
		___m_continuationObject_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_33() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_contingentProperties_33)); }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * get_m_contingentProperties_33() const { return ___m_contingentProperties_33; }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 ** get_address_of_m_contingentProperties_33() { return &___m_contingentProperties_33; }
	inline void set_m_contingentProperties_33(ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * value)
	{
		___m_contingentProperties_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_33), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_29;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_30;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * ___s_currentActiveTasks_31;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_32;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_taskCancelCallback_34;
	// System.Func`1<System.Threading.Tasks.Task/ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * ___s_createContingentProperties_35;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___s_completedTask_36;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * ___s_IsExceptionObservedByParentPredicate_37;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_ecCallback_38;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___s_IsTaskContinuationNullPredicate_39;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_29() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCompletionSentinel_29)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_29() const { return ___s_taskCompletionSentinel_29; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_29() { return &___s_taskCompletionSentinel_29; }
	inline void set_s_taskCompletionSentinel_29(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_29), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_30() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_asyncDebuggingEnabled_30)); }
	inline bool get_s_asyncDebuggingEnabled_30() const { return ___s_asyncDebuggingEnabled_30; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_30() { return &___s_asyncDebuggingEnabled_30; }
	inline void set_s_asyncDebuggingEnabled_30(bool value)
	{
		___s_asyncDebuggingEnabled_30 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_31() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_currentActiveTasks_31)); }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * get_s_currentActiveTasks_31() const { return ___s_currentActiveTasks_31; }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 ** get_address_of_s_currentActiveTasks_31() { return &___s_currentActiveTasks_31; }
	inline void set_s_currentActiveTasks_31(Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * value)
	{
		___s_currentActiveTasks_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_31), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_32() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_activeTasksLock_32)); }
	inline RuntimeObject * get_s_activeTasksLock_32() const { return ___s_activeTasksLock_32; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_32() { return &___s_activeTasksLock_32; }
	inline void set_s_activeTasksLock_32(RuntimeObject * value)
	{
		___s_activeTasksLock_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_32), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_34() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCancelCallback_34)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_taskCancelCallback_34() const { return ___s_taskCancelCallback_34; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_taskCancelCallback_34() { return &___s_taskCancelCallback_34; }
	inline void set_s_taskCancelCallback_34(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_taskCancelCallback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_34), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_35() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_createContingentProperties_35)); }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * get_s_createContingentProperties_35() const { return ___s_createContingentProperties_35; }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B ** get_address_of_s_createContingentProperties_35() { return &___s_createContingentProperties_35; }
	inline void set_s_createContingentProperties_35(Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * value)
	{
		___s_createContingentProperties_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_35), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_36() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_completedTask_36)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_s_completedTask_36() const { return ___s_completedTask_36; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_s_completedTask_36() { return &___s_completedTask_36; }
	inline void set_s_completedTask_36(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___s_completedTask_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_37() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsExceptionObservedByParentPredicate_37)); }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * get_s_IsExceptionObservedByParentPredicate_37() const { return ___s_IsExceptionObservedByParentPredicate_37; }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD ** get_address_of_s_IsExceptionObservedByParentPredicate_37() { return &___s_IsExceptionObservedByParentPredicate_37; }
	inline void set_s_IsExceptionObservedByParentPredicate_37(Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * value)
	{
		___s_IsExceptionObservedByParentPredicate_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_37), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_38() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_ecCallback_38)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_ecCallback_38() const { return ___s_ecCallback_38; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_ecCallback_38() { return &___s_ecCallback_38; }
	inline void set_s_ecCallback_38(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_ecCallback_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_38), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_39() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsTaskContinuationNullPredicate_39)); }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * get_s_IsTaskContinuationNullPredicate_39() const { return ___s_IsTaskContinuationNullPredicate_39; }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB ** get_address_of_s_IsTaskContinuationNullPredicate_39() { return &___s_IsTaskContinuationNullPredicate_39; }
	inline void set_s_IsTaskContinuationNullPredicate_39(Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * value)
	{
		___s_IsTaskContinuationNullPredicate_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_39), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_19)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};


// System.Xml.XmlNodeType
struct XmlNodeType_t6202952ADDE08339EF2AAC42CE97C84E99AC5D81 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeType_t6202952ADDE08339EF2AAC42CE97C84E99AC5D81, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>
struct Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<Amazon.Runtime.RequestRetryMode>
struct Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<System.TimeSpan>
struct Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F 
{
public:
	// T System.Nullable`1::value
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F, ___value_0)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_value_0() const { return ___value_0; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Threading.Tasks.Task`1<Amazon.Kinesis.Model.PutRecordsResponse>
struct Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685  : public Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * ___m_result_40;

public:
	inline static int32_t get_offset_of_m_result_40() { return static_cast<int32_t>(offsetof(Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685, ___m_result_40)); }
	inline PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * get_m_result_40() const { return ___m_result_40; }
	inline PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 ** get_address_of_m_result_40() { return &___m_result_40; }
	inline void set_m_result_40(PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * value)
	{
		___m_result_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_40), (void*)value);
	}
};

struct Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t2AA05B7A6F4895470B4B35E5E8DD7D5EBEDFCFD3 * ___s_Factory_41;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t2C541A67AA77D0D282702143303CAFD1FEE63B13 * ___TaskWhenAnyCast_42;

public:
	inline static int32_t get_offset_of_s_Factory_41() { return static_cast<int32_t>(offsetof(Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685_StaticFields, ___s_Factory_41)); }
	inline TaskFactory_1_t2AA05B7A6F4895470B4B35E5E8DD7D5EBEDFCFD3 * get_s_Factory_41() const { return ___s_Factory_41; }
	inline TaskFactory_1_t2AA05B7A6F4895470B4B35E5E8DD7D5EBEDFCFD3 ** get_address_of_s_Factory_41() { return &___s_Factory_41; }
	inline void set_s_Factory_41(TaskFactory_1_t2AA05B7A6F4895470B4B35E5E8DD7D5EBEDFCFD3 * value)
	{
		___s_Factory_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_41), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_42() { return static_cast<int32_t>(offsetof(Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685_StaticFields, ___TaskWhenAnyCast_42)); }
	inline Func_2_t2C541A67AA77D0D282702143303CAFD1FEE63B13 * get_TaskWhenAnyCast_42() const { return ___TaskWhenAnyCast_42; }
	inline Func_2_t2C541A67AA77D0D282702143303CAFD1FEE63B13 ** get_address_of_TaskWhenAnyCast_42() { return &___TaskWhenAnyCast_42; }
	inline void set_TaskWhenAnyCast_42(Func_2_t2C541A67AA77D0D282702143303CAFD1FEE63B13 * value)
	{
		___TaskWhenAnyCast_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_42), (void*)value);
	}
};


// Amazon.Runtime.Internal.Auth.AWS4Signer
struct AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D  : public AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE
{
public:
	// System.Boolean Amazon.Runtime.Internal.Auth.AWS4Signer::<SignPayload>k__BackingField
	bool ___U3CSignPayloadU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CSignPayloadU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D, ___U3CSignPayloadU3Ek__BackingField_15)); }
	inline bool get_U3CSignPayloadU3Ek__BackingField_15() const { return ___U3CSignPayloadU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CSignPayloadU3Ek__BackingField_15() { return &___U3CSignPayloadU3Ek__BackingField_15; }
	inline void set_U3CSignPayloadU3Ek__BackingField_15(bool value)
	{
		___U3CSignPayloadU3Ek__BackingField_15 = value;
	}
};

struct AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_StaticFields
{
public:
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::TerminatorBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___TerminatorBytes_5;
	// System.Collections.Generic.IEnumerable`1<System.String> Amazon.Runtime.Internal.Auth.AWS4Signer::_headersToIgnoreWhenSigning
	RuntimeObject* ____headersToIgnoreWhenSigning_14;

public:
	inline static int32_t get_offset_of_TerminatorBytes_5() { return static_cast<int32_t>(offsetof(AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_StaticFields, ___TerminatorBytes_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_TerminatorBytes_5() const { return ___TerminatorBytes_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_TerminatorBytes_5() { return &___TerminatorBytes_5; }
	inline void set_TerminatorBytes_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___TerminatorBytes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TerminatorBytes_5), (void*)value);
	}

	inline static int32_t get_offset_of__headersToIgnoreWhenSigning_14() { return static_cast<int32_t>(offsetof(AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_StaticFields, ____headersToIgnoreWhenSigning_14)); }
	inline RuntimeObject* get__headersToIgnoreWhenSigning_14() const { return ____headersToIgnoreWhenSigning_14; }
	inline RuntimeObject** get_address_of__headersToIgnoreWhenSigning_14() { return &____headersToIgnoreWhenSigning_14; }
	inline void set__headersToIgnoreWhenSigning_14(RuntimeObject* value)
	{
		____headersToIgnoreWhenSigning_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____headersToIgnoreWhenSigning_14), (void*)value);
	}
};


// Amazon.Runtime.AmazonServiceException
struct AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF  : public Exception_t
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.AmazonServiceException::errorType
	int32_t ___errorType_17;
	// System.String Amazon.Runtime.AmazonServiceException::errorCode
	String_t* ___errorCode_18;
	// System.String Amazon.Runtime.AmazonServiceException::requestId
	String_t* ___requestId_19;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonServiceException::statusCode
	int32_t ___statusCode_20;

public:
	inline static int32_t get_offset_of_errorType_17() { return static_cast<int32_t>(offsetof(AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF, ___errorType_17)); }
	inline int32_t get_errorType_17() const { return ___errorType_17; }
	inline int32_t* get_address_of_errorType_17() { return &___errorType_17; }
	inline void set_errorType_17(int32_t value)
	{
		___errorType_17 = value;
	}

	inline static int32_t get_offset_of_errorCode_18() { return static_cast<int32_t>(offsetof(AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF, ___errorCode_18)); }
	inline String_t* get_errorCode_18() const { return ___errorCode_18; }
	inline String_t** get_address_of_errorCode_18() { return &___errorCode_18; }
	inline void set_errorCode_18(String_t* value)
	{
		___errorCode_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorCode_18), (void*)value);
	}

	inline static int32_t get_offset_of_requestId_19() { return static_cast<int32_t>(offsetof(AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF, ___requestId_19)); }
	inline String_t* get_requestId_19() const { return ___requestId_19; }
	inline String_t** get_address_of_requestId_19() { return &___requestId_19; }
	inline void set_requestId_19(String_t* value)
	{
		___requestId_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___requestId_19), (void*)value);
	}

	inline static int32_t get_offset_of_statusCode_20() { return static_cast<int32_t>(offsetof(AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF, ___statusCode_20)); }
	inline int32_t get_statusCode_20() const { return ___statusCode_20; }
	inline int32_t* get_address_of_statusCode_20() { return &___statusCode_20; }
	inline void set_statusCode_20(int32_t value)
	{
		___statusCode_20 = value;
	}
};


// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A  : public RuntimeObject
{
public:
	// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::responseMetadataField
	ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * ___responseMetadataField_0;
	// System.Int64 Amazon.Runtime.AmazonWebServiceResponse::contentLength
	int64_t ___contentLength_1;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::httpStatusCode
	int32_t ___httpStatusCode_2;

public:
	inline static int32_t get_offset_of_responseMetadataField_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A, ___responseMetadataField_0)); }
	inline ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * get_responseMetadataField_0() const { return ___responseMetadataField_0; }
	inline ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 ** get_address_of_responseMetadataField_0() { return &___responseMetadataField_0; }
	inline void set_responseMetadataField_0(ResponseMetadata_t719F9F781EBA93800B024F975DFC5F58FAD4FFD4 * value)
	{
		___responseMetadataField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___responseMetadataField_0), (void*)value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_httpStatusCode_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A, ___httpStatusCode_2)); }
	inline int32_t get_httpStatusCode_2() const { return ___httpStatusCode_2; }
	inline int32_t* get_address_of_httpStatusCode_2() { return &___httpStatusCode_2; }
	inline void set_httpStatusCode_2(int32_t value)
	{
		___httpStatusCode_2 = value;
	}
};


// Amazon.Runtime.Internal.ErrorResponse
struct ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0  : public RuntimeObject
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String Amazon.Runtime.Internal.ErrorResponse::<Code>k__BackingField
	String_t* ___U3CCodeU3Ek__BackingField_1;
	// System.String Amazon.Runtime.Internal.ErrorResponse::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_2;
	// System.String Amazon.Runtime.Internal.ErrorResponse::<RequestId>k__BackingField
	String_t* ___U3CRequestIdU3Ek__BackingField_3;
	// System.Exception Amazon.Runtime.Internal.ErrorResponse::<InnerException>k__BackingField
	Exception_t * ___U3CInnerExceptionU3Ek__BackingField_4;
	// System.Net.HttpStatusCode Amazon.Runtime.Internal.ErrorResponse::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0, ___U3CTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0, ___U3CCodeU3Ek__BackingField_1)); }
	inline String_t* get_U3CCodeU3Ek__BackingField_1() const { return ___U3CCodeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCodeU3Ek__BackingField_1() { return &___U3CCodeU3Ek__BackingField_1; }
	inline void set_U3CCodeU3Ek__BackingField_1(String_t* value)
	{
		___U3CCodeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCodeU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0, ___U3CMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_2() const { return ___U3CMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_2() { return &___U3CMessageU3Ek__BackingField_2; }
	inline void set_U3CMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMessageU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRequestIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0, ___U3CRequestIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CRequestIdU3Ek__BackingField_3() const { return ___U3CRequestIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CRequestIdU3Ek__BackingField_3() { return &___U3CRequestIdU3Ek__BackingField_3; }
	inline void set_U3CRequestIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CRequestIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRequestIdU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CInnerExceptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0, ___U3CInnerExceptionU3Ek__BackingField_4)); }
	inline Exception_t * get_U3CInnerExceptionU3Ek__BackingField_4() const { return ___U3CInnerExceptionU3Ek__BackingField_4; }
	inline Exception_t ** get_address_of_U3CInnerExceptionU3Ek__BackingField_4() { return &___U3CInnerExceptionU3Ek__BackingField_4; }
	inline void set_U3CInnerExceptionU3Ek__BackingField_4(Exception_t * value)
	{
		___U3CInnerExceptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInnerExceptionU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0, ___U3CStatusCodeU3Ek__BackingField_5)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_5() const { return ___U3CStatusCodeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_5() { return &___U3CStatusCodeU3Ek__BackingField_5; }
	inline void set_U3CStatusCodeU3Ek__BackingField_5(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_5 = value;
	}
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943  : public UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::streamReader
	StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * ___streamReader_9;
	// System.Xml.XmlReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::_xmlReader
	XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138 * ____xmlReader_10;
	// System.Collections.Generic.Stack`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stack
	Stack_1_tF2F8B5476F614882C00CEDDE027482B818D7FF1D * ___stack_11;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stackString
	String_t* ___stackString_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeValues
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___attributeValues_13;
	// System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeNames
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___attributeNames_14;
	// System.Collections.Generic.IEnumerator`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeEnumerator
	RuntimeObject* ___attributeEnumerator_15;
	// System.Xml.XmlNodeType Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeType
	int32_t ___nodeType_16;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeContent
	String_t* ___nodeContent_17;
	// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::disposed
	bool ___disposed_18;

public:
	inline static int32_t get_offset_of_streamReader_9() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___streamReader_9)); }
	inline StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * get_streamReader_9() const { return ___streamReader_9; }
	inline StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 ** get_address_of_streamReader_9() { return &___streamReader_9; }
	inline void set_streamReader_9(StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * value)
	{
		___streamReader_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___streamReader_9), (void*)value);
	}

	inline static int32_t get_offset_of__xmlReader_10() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ____xmlReader_10)); }
	inline XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138 * get__xmlReader_10() const { return ____xmlReader_10; }
	inline XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138 ** get_address_of__xmlReader_10() { return &____xmlReader_10; }
	inline void set__xmlReader_10(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138 * value)
	{
		____xmlReader_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____xmlReader_10), (void*)value);
	}

	inline static int32_t get_offset_of_stack_11() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___stack_11)); }
	inline Stack_1_tF2F8B5476F614882C00CEDDE027482B818D7FF1D * get_stack_11() const { return ___stack_11; }
	inline Stack_1_tF2F8B5476F614882C00CEDDE027482B818D7FF1D ** get_address_of_stack_11() { return &___stack_11; }
	inline void set_stack_11(Stack_1_tF2F8B5476F614882C00CEDDE027482B818D7FF1D * value)
	{
		___stack_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stack_11), (void*)value);
	}

	inline static int32_t get_offset_of_stackString_12() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___stackString_12)); }
	inline String_t* get_stackString_12() const { return ___stackString_12; }
	inline String_t** get_address_of_stackString_12() { return &___stackString_12; }
	inline void set_stackString_12(String_t* value)
	{
		___stackString_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stackString_12), (void*)value);
	}

	inline static int32_t get_offset_of_attributeValues_13() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___attributeValues_13)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_attributeValues_13() const { return ___attributeValues_13; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_attributeValues_13() { return &___attributeValues_13; }
	inline void set_attributeValues_13(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___attributeValues_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributeValues_13), (void*)value);
	}

	inline static int32_t get_offset_of_attributeNames_14() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___attributeNames_14)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_attributeNames_14() const { return ___attributeNames_14; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_attributeNames_14() { return &___attributeNames_14; }
	inline void set_attributeNames_14(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___attributeNames_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributeNames_14), (void*)value);
	}

	inline static int32_t get_offset_of_attributeEnumerator_15() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___attributeEnumerator_15)); }
	inline RuntimeObject* get_attributeEnumerator_15() const { return ___attributeEnumerator_15; }
	inline RuntimeObject** get_address_of_attributeEnumerator_15() { return &___attributeEnumerator_15; }
	inline void set_attributeEnumerator_15(RuntimeObject* value)
	{
		___attributeEnumerator_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributeEnumerator_15), (void*)value);
	}

	inline static int32_t get_offset_of_nodeType_16() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___nodeType_16)); }
	inline int32_t get_nodeType_16() const { return ___nodeType_16; }
	inline int32_t* get_address_of_nodeType_16() { return &___nodeType_16; }
	inline void set_nodeType_16(int32_t value)
	{
		___nodeType_16 = value;
	}

	inline static int32_t get_offset_of_nodeContent_17() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___nodeContent_17)); }
	inline String_t* get_nodeContent_17() const { return ___nodeContent_17; }
	inline String_t** get_address_of_nodeContent_17() { return &___nodeContent_17; }
	inline void set_nodeContent_17(String_t* value)
	{
		___nodeContent_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodeContent_17), (void*)value);
	}

	inline static int32_t get_offset_of_disposed_18() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943, ___disposed_18)); }
	inline bool get_disposed_18() const { return ___disposed_18; }
	inline bool* get_address_of_disposed_18() { return &___disposed_18; }
	inline void set_disposed_18(bool value)
	{
		___disposed_18 = value;
	}
};

struct XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943_StaticFields
{
public:
	// System.Xml.XmlReaderSettings Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::READER_SETTINGS
	XmlReaderSettings_tD1257418720567D67A7C49A7498914FE937F1520 * ___READER_SETTINGS_7;
	// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodesToSkip
	HashSet_1_t5B8880CAAF26A02176440210489F689BE557D267 * ___nodesToSkip_8;

public:
	inline static int32_t get_offset_of_READER_SETTINGS_7() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943_StaticFields, ___READER_SETTINGS_7)); }
	inline XmlReaderSettings_tD1257418720567D67A7C49A7498914FE937F1520 * get_READER_SETTINGS_7() const { return ___READER_SETTINGS_7; }
	inline XmlReaderSettings_tD1257418720567D67A7C49A7498914FE937F1520 ** get_address_of_READER_SETTINGS_7() { return &___READER_SETTINGS_7; }
	inline void set_READER_SETTINGS_7(XmlReaderSettings_tD1257418720567D67A7C49A7498914FE937F1520 * value)
	{
		___READER_SETTINGS_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___READER_SETTINGS_7), (void*)value);
	}

	inline static int32_t get_offset_of_nodesToSkip_8() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943_StaticFields, ___nodesToSkip_8)); }
	inline HashSet_1_t5B8880CAAF26A02176440210489F689BE557D267 * get_nodesToSkip_8() const { return ___nodesToSkip_8; }
	inline HashSet_1_t5B8880CAAF26A02176440210489F689BE557D267 ** get_address_of_nodesToSkip_8() { return &___nodesToSkip_8; }
	inline void set_nodesToSkip_8(HashSet_1_t5B8880CAAF26A02176440210489F689BE557D267 * value)
	{
		___nodesToSkip_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodesToSkip_8), (void*)value);
	}
};


// Amazon.Kinesis.AmazonKinesisException
struct AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B  : public AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF
{
public:

public:
};


// Amazon.Runtime.ClientConfig
struct ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099  : public RuntimeObject
{
public:
	// Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::regionEndpoint
	RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___regionEndpoint_2;
	// System.Boolean Amazon.Runtime.ClientConfig::probeForRegionEndpoint
	bool ___probeForRegionEndpoint_3;
	// System.Boolean Amazon.Runtime.ClientConfig::throttleRetries
	bool ___throttleRetries_4;
	// System.Boolean Amazon.Runtime.ClientConfig::useHttp
	bool ___useHttp_5;
	// System.Boolean Amazon.Runtime.ClientConfig::useAlternateUserAgentHeader
	bool ___useAlternateUserAgentHeader_6;
	// System.String Amazon.Runtime.ClientConfig::serviceURL
	String_t* ___serviceURL_7;
	// System.String Amazon.Runtime.ClientConfig::authRegion
	String_t* ___authRegion_8;
	// System.String Amazon.Runtime.ClientConfig::authServiceName
	String_t* ___authServiceName_9;
	// System.String Amazon.Runtime.ClientConfig::signatureVersion
	String_t* ___signatureVersion_10;
	// Amazon.Runtime.SigningAlgorithm Amazon.Runtime.ClientConfig::signatureMethod
	int32_t ___signatureMethod_11;
	// System.Boolean Amazon.Runtime.ClientConfig::readEntireResponse
	bool ___readEntireResponse_12;
	// System.Boolean Amazon.Runtime.ClientConfig::logResponse
	bool ___logResponse_13;
	// System.Int32 Amazon.Runtime.ClientConfig::bufferSize
	int32_t ___bufferSize_14;
	// System.Int64 Amazon.Runtime.ClientConfig::progressUpdateInterval
	int64_t ___progressUpdateInterval_15;
	// System.Boolean Amazon.Runtime.ClientConfig::resignRetries
	bool ___resignRetries_16;
	// System.Net.ICredentials Amazon.Runtime.ClientConfig::proxyCredentials
	RuntimeObject* ___proxyCredentials_17;
	// System.Boolean Amazon.Runtime.ClientConfig::logMetrics
	bool ___logMetrics_18;
	// System.Boolean Amazon.Runtime.ClientConfig::disableLogging
	bool ___disableLogging_19;
	// System.Nullable`1<System.TimeSpan> Amazon.Runtime.ClientConfig::timeout
	Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F  ___timeout_20;
	// System.Boolean Amazon.Runtime.ClientConfig::allowAutoRedirect
	bool ___allowAutoRedirect_21;
	// System.Boolean Amazon.Runtime.ClientConfig::useDualstackEndpoint
	bool ___useDualstackEndpoint_22;
	// System.Boolean Amazon.Runtime.ClientConfig::disableHostPrefixInjection
	bool ___disableHostPrefixInjection_23;
	// System.Int32 Amazon.Runtime.ClientConfig::endpointDiscoveryCacheLimit
	int32_t ___endpointDiscoveryCacheLimit_24;
	// System.Nullable`1<Amazon.Runtime.RequestRetryMode> Amazon.Runtime.ClientConfig::retryMode
	Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E  ___retryMode_25;
	// System.Nullable`1<System.Int32> Amazon.Runtime.ClientConfig::maxRetries
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ___maxRetries_26;
	// System.Boolean Amazon.Runtime.ClientConfig::<FastFailRequests>k__BackingField
	bool ___U3CFastFailRequestsU3Ek__BackingField_27;
	// System.Boolean Amazon.Runtime.ClientConfig::cacheHttpClient
	bool ___cacheHttpClient_28;
	// System.Nullable`1<System.Int32> Amazon.Runtime.ClientConfig::_httpClientCacheSize
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ____httpClientCacheSize_29;
	// System.Net.IWebProxy Amazon.Runtime.ClientConfig::proxy
	RuntimeObject* ___proxy_30;
	// System.Int32 Amazon.Runtime.ClientConfig::proxyPort
	int32_t ___proxyPort_31;
	// System.Nullable`1<System.Int32> Amazon.Runtime.ClientConfig::<MaxConnectionsPerServer>k__BackingField
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ___U3CMaxConnectionsPerServerU3Ek__BackingField_32;
	// Amazon.Runtime.HttpClientFactory Amazon.Runtime.ClientConfig::<HttpClientFactory>k__BackingField
	HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24 * ___U3CHttpClientFactoryU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_regionEndpoint_2() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___regionEndpoint_2)); }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * get_regionEndpoint_2() const { return ___regionEndpoint_2; }
	inline RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A ** get_address_of_regionEndpoint_2() { return &___regionEndpoint_2; }
	inline void set_regionEndpoint_2(RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * value)
	{
		___regionEndpoint_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___regionEndpoint_2), (void*)value);
	}

	inline static int32_t get_offset_of_probeForRegionEndpoint_3() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___probeForRegionEndpoint_3)); }
	inline bool get_probeForRegionEndpoint_3() const { return ___probeForRegionEndpoint_3; }
	inline bool* get_address_of_probeForRegionEndpoint_3() { return &___probeForRegionEndpoint_3; }
	inline void set_probeForRegionEndpoint_3(bool value)
	{
		___probeForRegionEndpoint_3 = value;
	}

	inline static int32_t get_offset_of_throttleRetries_4() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___throttleRetries_4)); }
	inline bool get_throttleRetries_4() const { return ___throttleRetries_4; }
	inline bool* get_address_of_throttleRetries_4() { return &___throttleRetries_4; }
	inline void set_throttleRetries_4(bool value)
	{
		___throttleRetries_4 = value;
	}

	inline static int32_t get_offset_of_useHttp_5() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___useHttp_5)); }
	inline bool get_useHttp_5() const { return ___useHttp_5; }
	inline bool* get_address_of_useHttp_5() { return &___useHttp_5; }
	inline void set_useHttp_5(bool value)
	{
		___useHttp_5 = value;
	}

	inline static int32_t get_offset_of_useAlternateUserAgentHeader_6() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___useAlternateUserAgentHeader_6)); }
	inline bool get_useAlternateUserAgentHeader_6() const { return ___useAlternateUserAgentHeader_6; }
	inline bool* get_address_of_useAlternateUserAgentHeader_6() { return &___useAlternateUserAgentHeader_6; }
	inline void set_useAlternateUserAgentHeader_6(bool value)
	{
		___useAlternateUserAgentHeader_6 = value;
	}

	inline static int32_t get_offset_of_serviceURL_7() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___serviceURL_7)); }
	inline String_t* get_serviceURL_7() const { return ___serviceURL_7; }
	inline String_t** get_address_of_serviceURL_7() { return &___serviceURL_7; }
	inline void set_serviceURL_7(String_t* value)
	{
		___serviceURL_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serviceURL_7), (void*)value);
	}

	inline static int32_t get_offset_of_authRegion_8() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___authRegion_8)); }
	inline String_t* get_authRegion_8() const { return ___authRegion_8; }
	inline String_t** get_address_of_authRegion_8() { return &___authRegion_8; }
	inline void set_authRegion_8(String_t* value)
	{
		___authRegion_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authRegion_8), (void*)value);
	}

	inline static int32_t get_offset_of_authServiceName_9() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___authServiceName_9)); }
	inline String_t* get_authServiceName_9() const { return ___authServiceName_9; }
	inline String_t** get_address_of_authServiceName_9() { return &___authServiceName_9; }
	inline void set_authServiceName_9(String_t* value)
	{
		___authServiceName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authServiceName_9), (void*)value);
	}

	inline static int32_t get_offset_of_signatureVersion_10() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___signatureVersion_10)); }
	inline String_t* get_signatureVersion_10() const { return ___signatureVersion_10; }
	inline String_t** get_address_of_signatureVersion_10() { return &___signatureVersion_10; }
	inline void set_signatureVersion_10(String_t* value)
	{
		___signatureVersion_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___signatureVersion_10), (void*)value);
	}

	inline static int32_t get_offset_of_signatureMethod_11() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___signatureMethod_11)); }
	inline int32_t get_signatureMethod_11() const { return ___signatureMethod_11; }
	inline int32_t* get_address_of_signatureMethod_11() { return &___signatureMethod_11; }
	inline void set_signatureMethod_11(int32_t value)
	{
		___signatureMethod_11 = value;
	}

	inline static int32_t get_offset_of_readEntireResponse_12() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___readEntireResponse_12)); }
	inline bool get_readEntireResponse_12() const { return ___readEntireResponse_12; }
	inline bool* get_address_of_readEntireResponse_12() { return &___readEntireResponse_12; }
	inline void set_readEntireResponse_12(bool value)
	{
		___readEntireResponse_12 = value;
	}

	inline static int32_t get_offset_of_logResponse_13() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___logResponse_13)); }
	inline bool get_logResponse_13() const { return ___logResponse_13; }
	inline bool* get_address_of_logResponse_13() { return &___logResponse_13; }
	inline void set_logResponse_13(bool value)
	{
		___logResponse_13 = value;
	}

	inline static int32_t get_offset_of_bufferSize_14() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___bufferSize_14)); }
	inline int32_t get_bufferSize_14() const { return ___bufferSize_14; }
	inline int32_t* get_address_of_bufferSize_14() { return &___bufferSize_14; }
	inline void set_bufferSize_14(int32_t value)
	{
		___bufferSize_14 = value;
	}

	inline static int32_t get_offset_of_progressUpdateInterval_15() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___progressUpdateInterval_15)); }
	inline int64_t get_progressUpdateInterval_15() const { return ___progressUpdateInterval_15; }
	inline int64_t* get_address_of_progressUpdateInterval_15() { return &___progressUpdateInterval_15; }
	inline void set_progressUpdateInterval_15(int64_t value)
	{
		___progressUpdateInterval_15 = value;
	}

	inline static int32_t get_offset_of_resignRetries_16() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___resignRetries_16)); }
	inline bool get_resignRetries_16() const { return ___resignRetries_16; }
	inline bool* get_address_of_resignRetries_16() { return &___resignRetries_16; }
	inline void set_resignRetries_16(bool value)
	{
		___resignRetries_16 = value;
	}

	inline static int32_t get_offset_of_proxyCredentials_17() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___proxyCredentials_17)); }
	inline RuntimeObject* get_proxyCredentials_17() const { return ___proxyCredentials_17; }
	inline RuntimeObject** get_address_of_proxyCredentials_17() { return &___proxyCredentials_17; }
	inline void set_proxyCredentials_17(RuntimeObject* value)
	{
		___proxyCredentials_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___proxyCredentials_17), (void*)value);
	}

	inline static int32_t get_offset_of_logMetrics_18() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___logMetrics_18)); }
	inline bool get_logMetrics_18() const { return ___logMetrics_18; }
	inline bool* get_address_of_logMetrics_18() { return &___logMetrics_18; }
	inline void set_logMetrics_18(bool value)
	{
		___logMetrics_18 = value;
	}

	inline static int32_t get_offset_of_disableLogging_19() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___disableLogging_19)); }
	inline bool get_disableLogging_19() const { return ___disableLogging_19; }
	inline bool* get_address_of_disableLogging_19() { return &___disableLogging_19; }
	inline void set_disableLogging_19(bool value)
	{
		___disableLogging_19 = value;
	}

	inline static int32_t get_offset_of_timeout_20() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___timeout_20)); }
	inline Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F  get_timeout_20() const { return ___timeout_20; }
	inline Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F * get_address_of_timeout_20() { return &___timeout_20; }
	inline void set_timeout_20(Nullable_1_tCFBE5FE1DB8A702D620FFC81D556C8E8AD5B871F  value)
	{
		___timeout_20 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_21() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___allowAutoRedirect_21)); }
	inline bool get_allowAutoRedirect_21() const { return ___allowAutoRedirect_21; }
	inline bool* get_address_of_allowAutoRedirect_21() { return &___allowAutoRedirect_21; }
	inline void set_allowAutoRedirect_21(bool value)
	{
		___allowAutoRedirect_21 = value;
	}

	inline static int32_t get_offset_of_useDualstackEndpoint_22() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___useDualstackEndpoint_22)); }
	inline bool get_useDualstackEndpoint_22() const { return ___useDualstackEndpoint_22; }
	inline bool* get_address_of_useDualstackEndpoint_22() { return &___useDualstackEndpoint_22; }
	inline void set_useDualstackEndpoint_22(bool value)
	{
		___useDualstackEndpoint_22 = value;
	}

	inline static int32_t get_offset_of_disableHostPrefixInjection_23() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___disableHostPrefixInjection_23)); }
	inline bool get_disableHostPrefixInjection_23() const { return ___disableHostPrefixInjection_23; }
	inline bool* get_address_of_disableHostPrefixInjection_23() { return &___disableHostPrefixInjection_23; }
	inline void set_disableHostPrefixInjection_23(bool value)
	{
		___disableHostPrefixInjection_23 = value;
	}

	inline static int32_t get_offset_of_endpointDiscoveryCacheLimit_24() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___endpointDiscoveryCacheLimit_24)); }
	inline int32_t get_endpointDiscoveryCacheLimit_24() const { return ___endpointDiscoveryCacheLimit_24; }
	inline int32_t* get_address_of_endpointDiscoveryCacheLimit_24() { return &___endpointDiscoveryCacheLimit_24; }
	inline void set_endpointDiscoveryCacheLimit_24(int32_t value)
	{
		___endpointDiscoveryCacheLimit_24 = value;
	}

	inline static int32_t get_offset_of_retryMode_25() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___retryMode_25)); }
	inline Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E  get_retryMode_25() const { return ___retryMode_25; }
	inline Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E * get_address_of_retryMode_25() { return &___retryMode_25; }
	inline void set_retryMode_25(Nullable_1_t25C79BDCB81BC2D6E273B0A415A2802910F3B61E  value)
	{
		___retryMode_25 = value;
	}

	inline static int32_t get_offset_of_maxRetries_26() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___maxRetries_26)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get_maxRetries_26() const { return ___maxRetries_26; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of_maxRetries_26() { return &___maxRetries_26; }
	inline void set_maxRetries_26(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		___maxRetries_26 = value;
	}

	inline static int32_t get_offset_of_U3CFastFailRequestsU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___U3CFastFailRequestsU3Ek__BackingField_27)); }
	inline bool get_U3CFastFailRequestsU3Ek__BackingField_27() const { return ___U3CFastFailRequestsU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CFastFailRequestsU3Ek__BackingField_27() { return &___U3CFastFailRequestsU3Ek__BackingField_27; }
	inline void set_U3CFastFailRequestsU3Ek__BackingField_27(bool value)
	{
		___U3CFastFailRequestsU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_cacheHttpClient_28() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___cacheHttpClient_28)); }
	inline bool get_cacheHttpClient_28() const { return ___cacheHttpClient_28; }
	inline bool* get_address_of_cacheHttpClient_28() { return &___cacheHttpClient_28; }
	inline void set_cacheHttpClient_28(bool value)
	{
		___cacheHttpClient_28 = value;
	}

	inline static int32_t get_offset_of__httpClientCacheSize_29() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ____httpClientCacheSize_29)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get__httpClientCacheSize_29() const { return ____httpClientCacheSize_29; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of__httpClientCacheSize_29() { return &____httpClientCacheSize_29; }
	inline void set__httpClientCacheSize_29(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		____httpClientCacheSize_29 = value;
	}

	inline static int32_t get_offset_of_proxy_30() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___proxy_30)); }
	inline RuntimeObject* get_proxy_30() const { return ___proxy_30; }
	inline RuntimeObject** get_address_of_proxy_30() { return &___proxy_30; }
	inline void set_proxy_30(RuntimeObject* value)
	{
		___proxy_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___proxy_30), (void*)value);
	}

	inline static int32_t get_offset_of_proxyPort_31() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___proxyPort_31)); }
	inline int32_t get_proxyPort_31() const { return ___proxyPort_31; }
	inline int32_t* get_address_of_proxyPort_31() { return &___proxyPort_31; }
	inline void set_proxyPort_31(int32_t value)
	{
		___proxyPort_31 = value;
	}

	inline static int32_t get_offset_of_U3CMaxConnectionsPerServerU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___U3CMaxConnectionsPerServerU3Ek__BackingField_32)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get_U3CMaxConnectionsPerServerU3Ek__BackingField_32() const { return ___U3CMaxConnectionsPerServerU3Ek__BackingField_32; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of_U3CMaxConnectionsPerServerU3Ek__BackingField_32() { return &___U3CMaxConnectionsPerServerU3Ek__BackingField_32; }
	inline void set_U3CMaxConnectionsPerServerU3Ek__BackingField_32(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		___U3CMaxConnectionsPerServerU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CHttpClientFactoryU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099, ___U3CHttpClientFactoryU3Ek__BackingField_33)); }
	inline HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24 * get_U3CHttpClientFactoryU3Ek__BackingField_33() const { return ___U3CHttpClientFactoryU3Ek__BackingField_33; }
	inline HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24 ** get_address_of_U3CHttpClientFactoryU3Ek__BackingField_33() { return &___U3CHttpClientFactoryU3Ek__BackingField_33; }
	inline void set_U3CHttpClientFactoryU3Ek__BackingField_33(HttpClientFactory_t9379BB749836724C5AE08F0CDAEDE59CBAA05F24 * value)
	{
		___U3CHttpClientFactoryU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHttpClientFactoryU3Ek__BackingField_33), (void*)value);
	}
};

struct ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_StaticFields
{
public:
	// System.TimeSpan Amazon.Runtime.ClientConfig::InfiniteTimeout
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___InfiniteTimeout_0;
	// System.TimeSpan Amazon.Runtime.ClientConfig::MaxTimeout
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxTimeout_1;

public:
	inline static int32_t get_offset_of_InfiniteTimeout_0() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_StaticFields, ___InfiniteTimeout_0)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_InfiniteTimeout_0() const { return ___InfiniteTimeout_0; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_InfiniteTimeout_0() { return &___InfiniteTimeout_0; }
	inline void set_InfiniteTimeout_0(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___InfiniteTimeout_0 = value;
	}

	inline static int32_t get_offset_of_MaxTimeout_1() { return static_cast<int32_t>(offsetof(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_StaticFields, ___MaxTimeout_1)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxTimeout_1() const { return ___MaxTimeout_1; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxTimeout_1() { return &___MaxTimeout_1; }
	inline void set_MaxTimeout_1(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxTimeout_1 = value;
	}
};


// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686  : public UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::streamReader
	StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * ___streamReader_7;
	// ThirdParty.Json.LitJson.JsonReader Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::jsonReader
	JsonReader_tCCF81BC14AE9F7B6A52B16914C5918E260CDF515 * ___jsonReader_8;
	// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::stack
	JsonPathStack_tA5E036E6D34C4F9396F396BDD6701446556647F2 * ___stack_9;
	// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::currentField
	String_t* ___currentField_10;
	// System.Nullable`1<ThirdParty.Json.LitJson.JsonToken> Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::currentToken
	Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA  ___currentToken_11;
	// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::disposed
	bool ___disposed_12;
	// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::wasPeeked
	bool ___wasPeeked_13;

public:
	inline static int32_t get_offset_of_streamReader_7() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___streamReader_7)); }
	inline StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * get_streamReader_7() const { return ___streamReader_7; }
	inline StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 ** get_address_of_streamReader_7() { return &___streamReader_7; }
	inline void set_streamReader_7(StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * value)
	{
		___streamReader_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___streamReader_7), (void*)value);
	}

	inline static int32_t get_offset_of_jsonReader_8() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___jsonReader_8)); }
	inline JsonReader_tCCF81BC14AE9F7B6A52B16914C5918E260CDF515 * get_jsonReader_8() const { return ___jsonReader_8; }
	inline JsonReader_tCCF81BC14AE9F7B6A52B16914C5918E260CDF515 ** get_address_of_jsonReader_8() { return &___jsonReader_8; }
	inline void set_jsonReader_8(JsonReader_tCCF81BC14AE9F7B6A52B16914C5918E260CDF515 * value)
	{
		___jsonReader_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jsonReader_8), (void*)value);
	}

	inline static int32_t get_offset_of_stack_9() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___stack_9)); }
	inline JsonPathStack_tA5E036E6D34C4F9396F396BDD6701446556647F2 * get_stack_9() const { return ___stack_9; }
	inline JsonPathStack_tA5E036E6D34C4F9396F396BDD6701446556647F2 ** get_address_of_stack_9() { return &___stack_9; }
	inline void set_stack_9(JsonPathStack_tA5E036E6D34C4F9396F396BDD6701446556647F2 * value)
	{
		___stack_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stack_9), (void*)value);
	}

	inline static int32_t get_offset_of_currentField_10() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___currentField_10)); }
	inline String_t* get_currentField_10() const { return ___currentField_10; }
	inline String_t** get_address_of_currentField_10() { return &___currentField_10; }
	inline void set_currentField_10(String_t* value)
	{
		___currentField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentField_10), (void*)value);
	}

	inline static int32_t get_offset_of_currentToken_11() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___currentToken_11)); }
	inline Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA  get_currentToken_11() const { return ___currentToken_11; }
	inline Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA * get_address_of_currentToken_11() { return &___currentToken_11; }
	inline void set_currentToken_11(Nullable_1_tFE0671AA73CF5DF5F957B3D2D75155421F7C3AAA  value)
	{
		___currentToken_11 = value;
	}

	inline static int32_t get_offset_of_disposed_12() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___disposed_12)); }
	inline bool get_disposed_12() const { return ___disposed_12; }
	inline bool* get_address_of_disposed_12() { return &___disposed_12; }
	inline void set_disposed_12(bool value)
	{
		___disposed_12 = value;
	}

	inline static int32_t get_offset_of_wasPeeked_13() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686, ___wasPeeked_13)); }
	inline bool get_wasPeeked_13() const { return ___wasPeeked_13; }
	inline bool* get_address_of_wasPeeked_13() { return &___wasPeeked_13; }
	inline void set_wasPeeked_13(bool value)
	{
		___wasPeeked_13 = value;
	}
};


// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// Amazon.Kinesis.Model.PutRecordsResponse
struct PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045  : public AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A
{
public:
	// Amazon.Kinesis.EncryptionType Amazon.Kinesis.Model.PutRecordsResponse::_encryptionType
	EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ____encryptionType_3;
	// System.Nullable`1<System.Int32> Amazon.Kinesis.Model.PutRecordsResponse::_failedRecordCount
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ____failedRecordCount_4;
	// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry> Amazon.Kinesis.Model.PutRecordsResponse::_records
	List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * ____records_5;

public:
	inline static int32_t get_offset_of__encryptionType_3() { return static_cast<int32_t>(offsetof(PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045, ____encryptionType_3)); }
	inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * get__encryptionType_3() const { return ____encryptionType_3; }
	inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F ** get_address_of__encryptionType_3() { return &____encryptionType_3; }
	inline void set__encryptionType_3(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * value)
	{
		____encryptionType_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____encryptionType_3), (void*)value);
	}

	inline static int32_t get_offset_of__failedRecordCount_4() { return static_cast<int32_t>(offsetof(PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045, ____failedRecordCount_4)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get__failedRecordCount_4() const { return ____failedRecordCount_4; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of__failedRecordCount_4() { return &____failedRecordCount_4; }
	inline void set__failedRecordCount_4(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		____failedRecordCount_4 = value;
	}

	inline static int32_t get_offset_of__records_5() { return static_cast<int32_t>(offsetof(PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045, ____records_5)); }
	inline List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * get__records_5() const { return ____records_5; }
	inline List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 ** get_address_of__records_5() { return &____records_5; }
	inline void set__records_5(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * value)
	{
		____records_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____records_5), (void*)value);
	}
};


// Amazon.Kinesis.AmazonKinesisConfig
struct AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D  : public ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099
{
public:
	// System.String Amazon.Kinesis.AmazonKinesisConfig::_userAgent
	String_t* ____userAgent_35;

public:
	inline static int32_t get_offset_of__userAgent_35() { return static_cast<int32_t>(offsetof(AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D, ____userAgent_35)); }
	inline String_t* get__userAgent_35() const { return ____userAgent_35; }
	inline String_t** get_address_of__userAgent_35() { return &____userAgent_35; }
	inline void set__userAgent_35(String_t* value)
	{
		____userAgent_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____userAgent_35), (void*)value);
	}
};

struct AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_StaticFields
{
public:
	// System.String Amazon.Kinesis.AmazonKinesisConfig::UserAgentString
	String_t* ___UserAgentString_34;

public:
	inline static int32_t get_offset_of_UserAgentString_34() { return static_cast<int32_t>(offsetof(AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_StaticFields, ___UserAgentString_34)); }
	inline String_t* get_UserAgentString_34() const { return ___UserAgentString_34; }
	inline String_t** get_address_of_UserAgentString_34() { return &___UserAgentString_34; }
	inline void set_UserAgentString_34(String_t* value)
	{
		___UserAgentString_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UserAgentString_34), (void*)value);
	}
};


// Amazon.Kinesis.Model.InvalidArgumentException
struct InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.KMSAccessDeniedException
struct KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.KMSDisabledException
struct KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.KMSInvalidStateException
struct KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.KMSNotFoundException
struct KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.KMSOptInRequiredException
struct KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.KMSThrottlingException
struct KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.ProvisionedThroughputExceededException
struct ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};


// Amazon.Kinesis.Model.ResourceNotFoundException
struct ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B  : public AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Threading.Tasks.Task`1<!!0> Amazon.Runtime.AmazonServiceClient::InvokeAsync<System.Object>(Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.Internal.InvokeOptionsBase,System.Threading.CancellationToken)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_tC1805497876E88B78A2B0CB81C6409E0B381AC17 * AmazonServiceClient_InvokeAsync_TisRuntimeObject_m4CDAE3685A4BEAD4ACCD4ED74162CF99D5A97A7C_gshared (AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB * __this, AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * ___request0, InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4 * ___options1, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  ___cancellationToken2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mBD7199657787018123B7B8F2B048B503D484C097_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, int32_t ___capacity0, const RuntimeMethod* method);
// !!0 Amazon.Runtime.ConstantClass::FindValue<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ConstantClass_FindValue_TisRuntimeObject_mFBD8EE05ECCABACE6129ADEA6C74E114AD019D2F_gshared (String_t* ___value0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32>::GetValueOrDefault()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_gshared_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_gshared (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>::.ctor(!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListUnmarshaller_2__ctor_mD70243E8F411BC9C3BD3A55FA1C8DF836D746B97_gshared (ListUnmarshaller_2_t8BDE52D0EFF3CA7CD00D31DF6349AA636C0D959A * __this, RuntimeObject * ___iUnmarshaller0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!0> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<System.Object,System.Object>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ListUnmarshaller_2_Unmarshall_mF0CBC3D0365ACF97BF01B927B26AE17FE112B1D5_gshared (ListUnmarshaller_2_t8BDE52D0EFF3CA7CD00D31DF6349AA636C0D959A * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method);

// System.Void Amazon.Kinesis.AmazonKinesisConfig::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisConfig__ctor_m61B8B11CE83581F976E212D24DDCE8C60EF1D98A (AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.ClientConfig::set_RegionEndpoint(Amazon.RegionEndpoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ClientConfig_set_RegionEndpoint_mC0C36203BCE0F2423D15D593A156B100A41A003E (ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099 * __this, RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.AmazonKinesisClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.Kinesis.AmazonKinesisConfig)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisClient__ctor_mA422F58AF0BFF082F71B466CEA6D02F630516F57 (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * ___credentials0, AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * ___clientConfig1, const RuntimeMethod* method);
// System.Void Amazon.Runtime.AmazonServiceClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.Runtime.ClientConfig)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonServiceClient__ctor_mBF0730EE760A3209048451CB7B95CC3F796985AD (AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB * __this, AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * ___credentials0, ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099 * ___config1, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.Auth.AWS4Signer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AWS4Signer__ctor_m0C5E9E6AFF7BD86B722135DDE17D770894463306 (AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.AmazonServiceClient::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonServiceClient_Dispose_mF1BDBFD1F83C742FA192AFFA3896B74DCEFDC4F7 (AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB * __this, bool ___disposing0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.InvokeOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvokeOptions__ctor_mC8DD478F8404260C05FD2E6FF4D26C944E216AB4 (InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * PutRecordsRequestMarshaller_get_Instance_m863F7927ABAC02B5A1083E29BDDFF08AFB5479AA_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * PutRecordsResponseUnmarshaller_get_Instance_mCAE356524F66AE7AE050AA8CCE6CE1327AC1EF3E_inline (const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<!!0> Amazon.Runtime.AmazonServiceClient::InvokeAsync<Amazon.Kinesis.Model.PutRecordsResponse>(Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.Internal.InvokeOptionsBase,System.Threading.CancellationToken)
inline Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685 * AmazonServiceClient_InvokeAsync_TisPutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_m0418AEF71BCD7A4F1376054D52A3DB4A35638A56 (AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB * __this, AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * ___request0, InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4 * ___options1, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  ___cancellationToken2, const RuntimeMethod* method)
{
	return ((  Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685 * (*) (AmazonServiceClient_tF5F587A9E74189E18471E0704805C2E168BFBFDB *, AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 *, InvokeOptionsBase_t8431E3ED5D69265BD053A1CFADF3D52309E16CF4 *, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD , const RuntimeMethod*))AmazonServiceClient_InvokeAsync_TisRuntimeObject_m4CDAE3685A4BEAD4ACCD4ED74162CF99D5A97A7C_gshared)(__this, ___request0, ___options1, ___cancellationToken2, method);
}
// System.Void Amazon.Kinesis.Internal.AmazonKinesisMetadata::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisMetadata__ctor_m9D0AC128854D2B470A460E8D331ECEAF982B45F3 (AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.ClientConfig::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ClientConfig__ctor_mF7458A55808D9FE37F6F49A9E7E1BBE807B3181D (ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099 * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.ClientConfig::set_AuthenticationServiceName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ClientConfig_set_AuthenticationServiceName_mA6A0E4F14EEA4C9A42061D4C18AEF69785CF83E1_inline (ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String Amazon.Util.Internal.InternalSDKUtils::BuildUserAgentString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InternalSDKUtils_BuildUserAgentString_m2C1A262CB88B697FA16980D3485B3DF3BD7C93D4 (String_t* ___serviceSdkVersion0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.AmazonServiceException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonServiceException__ctor_m6A5B1D6DD41F926B3921A96F5259093DDA64E487 (AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Int32)
inline void Dictionary_2__ctor_m374496B3B8F3532FC52B465384E6C950DDA79F8B (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, int32_t, const RuntimeMethod*))Dictionary_2__ctor_mBD7199657787018123B7B8F2B048B503D484C097_gshared)(__this, ___capacity0, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.AmazonWebServiceRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonWebServiceRequest__ctor_m2F70AA8C6602940F5E5A6AEAE90FBED8247D9FB9 (AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.ConstantClass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConstantClass__ctor_m2E56E06C64185F9EAEC5D3630DF2C35E6E8E2E9B (ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6 * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 Amazon.Runtime.ConstantClass::FindValue<Amazon.Kinesis.EncryptionType>(System.String)
inline EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ConstantClass_FindValue_TisEncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_m45BFB226A317DFED373E14B26E0D3493151FC468 (String_t* ___value0, const RuntimeMethod* method)
{
	return ((  EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * (*) (String_t*, const RuntimeMethod*))ConstantClass_FindValue_TisRuntimeObject_mFBD8EE05ECCABACE6129ADEA6C74E114AD019D2F_gshared)(___value0, method);
}
// Amazon.Kinesis.EncryptionType Amazon.Kinesis.EncryptionType::FindValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * EncryptionType_FindValue_mAE3F6719EAC3829296885A4F2ECD138077E0AC2B (String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.EncryptionType::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EncryptionType__ctor_m5A44FFAFB9729B6C8785FC6890C3D9874C446175 (EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.AmazonKinesisException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9 (AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.ErrorResponse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.InvalidArgumentException Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * InvalidArgumentExceptionUnmarshaller_Unmarshall_m9498CC9012F65405D1309ECDB0DFCF4494703EF7 (InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.String Amazon.Runtime.Internal.ErrorResponse::get_Message()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// System.Exception Amazon.Runtime.Internal.ErrorResponse::get_InnerException()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Exception_t * ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::get_Type()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// System.String Amazon.Runtime.Internal.ErrorResponse::get_Code()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// System.String Amazon.Runtime.Internal.ErrorResponse::get_RequestId()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// System.Net.HttpStatusCode Amazon.Runtime.Internal.ErrorResponse::get_StatusCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.InvalidArgumentException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidArgumentException__ctor_mCC2E1D2514B57E6EC1C03CEC7E691F7D558E7B37 (InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::ReadAtDepth(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27 (UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B * __this, int32_t ___targetDepth0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidArgumentExceptionUnmarshaller__ctor_m0B06463E543528B539EDE25D58F57A5C8349EDF5 (InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.KMSAccessDeniedException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * KMSAccessDeniedExceptionUnmarshaller_Unmarshall_m7512E68CFE3157D6371AA838CA213140DE413CF5 (KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.KMSAccessDeniedException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSAccessDeniedException__ctor_mA695FF0824E4ED92482CAB96A0DF12B4AD8A8273 (KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSAccessDeniedExceptionUnmarshaller__ctor_m89BD64F04C19D7D45F0DA7A8640EA73DF713D2BD (KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.KMSDisabledException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * KMSDisabledExceptionUnmarshaller_Unmarshall_mB574723FC32FCB75EF56889945657C4EC48C7CDF (KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.KMSDisabledException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSDisabledException__ctor_mF66CC7234E73C1A7160E803A2344F5B8BC5698B7 (KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSDisabledExceptionUnmarshaller__ctor_m64BB9F8EAADAE1DDFAD756544CF3A8A2F7AFCCCB (KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.KMSInvalidStateException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * KMSInvalidStateExceptionUnmarshaller_Unmarshall_m8AECCA10430208CD1BA8DAFA3EED99F28E9C93F9 (KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.KMSInvalidStateException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSInvalidStateException__ctor_mA54A1F35E8B2FCC2BAA11393256CAFAED9A4B30F (KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSInvalidStateExceptionUnmarshaller__ctor_mDB045E97FC64C6D60CC5B26ED7A962CCCB761B65 (KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.KMSNotFoundException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * KMSNotFoundExceptionUnmarshaller_Unmarshall_mA9C221F55F082CA98EA09CC7C4D08E6FA92015AB (KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.KMSNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSNotFoundException__ctor_mFEA7EB9F5DC486A026019DFFA043394578BEA10C (KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSNotFoundExceptionUnmarshaller__ctor_m85DFC6A1DBE9DD34D7B751E79CCDDAC7B3D953DD (KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.KMSOptInRequiredException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * KMSOptInRequiredExceptionUnmarshaller_Unmarshall_m971A2AD42993EB0E84F40524B6367132EE6B2F90 (KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.KMSOptInRequiredException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSOptInRequiredException__ctor_mA62F89061DA9CCF9EAE9395CE146B676D298C51E (KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSOptInRequiredExceptionUnmarshaller__ctor_m2ABACB57C9808F3BD5A9FF6C60430CD1839087B1 (KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.KMSThrottlingException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * KMSThrottlingExceptionUnmarshaller_Unmarshall_m45AC671D3AB3E89505ECE3A0A47116DBA7156A8B (KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.KMSThrottlingException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSThrottlingException__ctor_m351089A433E7E63591E94F7034436952CE74E24A (KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSThrottlingExceptionUnmarshaller__ctor_mB2D2053DECA3E7A09E117FDCBDC07D5854A09495 (KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * __this, const RuntimeMethod* method);
// Amazon.Kinesis.Model.ProvisionedThroughputExceededException Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * ProvisionedThroughputExceededExceptionUnmarshaller_Unmarshall_mC8C5292AAD0B3F329DC9220FFCAE60DCC5A0857E (ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.ProvisionedThroughputExceededException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProvisionedThroughputExceededException__ctor_m4E80B47B6B7B890EDF63C7F023EEAD53E0FFD417 (ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProvisionedThroughputExceededExceptionUnmarshaller__ctor_m6CFB55B10800C6A26438F0B82386C9CAC62AD3C7 (ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry>::get_Count()
inline int32_t List_1_get_Count_mC4281CDDAF1829194CAF38FB27521B5859B00B23_inline (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry>::.ctor()
inline void List_1__ctor_m67E3D76494E3E22BBF70C37077DD5978721472D5 (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Amazon.Kinesis.AmazonKinesisRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisRequest__ctor_m9B5FCB0CBD93CB1FD51497E65C9BF2B274009B92 (AmazonKinesisRequest_t6FADAEC3B6BFB9A305B4B0D75615BCC7DD5B0635 * __this, const RuntimeMethod* method);
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequestEntry::IsSetData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequestEntry_IsSetData_mDCB90648700A106D315A6BF5CB2349908A62FD68 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method);
// ThirdParty.Json.LitJson.JsonWriter Amazon.Runtime.Internal.Transform.JsonMarshallerContext::get_Writer()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline (JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * __this, const RuntimeMethod* method);
// System.Void ThirdParty.Json.LitJson.JsonWriter::WritePropertyName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, String_t* ___property_name0, const RuntimeMethod* method);
// System.IO.MemoryStream Amazon.Kinesis.Model.PutRecordsRequestEntry::get_Data()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * PutRecordsRequestEntry_get_Data_m6838F4D073A2FBC74A6DC77F8110DBF794D64D96_inline (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method);
// System.String Amazon.Runtime.Internal.Util.StringUtils::FromMemoryStream(System.IO.MemoryStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StringUtils_FromMemoryStream_m8390D1D01AD7AC326C5B09E6E0D3116A28930BED (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * ___value0, const RuntimeMethod* method);
// System.Void ThirdParty.Json.LitJson.JsonWriter::Write(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, String_t* ___str0, const RuntimeMethod* method);
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequestEntry::IsSetExplicitHashKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequestEntry_IsSetExplicitHashKey_mE7285ACD56560FA1FA00C220028E7375B1017398 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method);
// System.String Amazon.Kinesis.Model.PutRecordsRequestEntry::get_ExplicitHashKey()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PutRecordsRequestEntry_get_ExplicitHashKey_m0CBD1B2DFC1D20BB67A0ED020694A62A646E044A_inline (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method);
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequestEntry::IsSetPartitionKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequestEntry_IsSetPartitionKey_m2435D5D5A4EE1A7A08F7666F605B153E73D59DAE (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method);
// System.String Amazon.Kinesis.Model.PutRecordsRequestEntry::get_PartitionKey()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PutRecordsRequestEntry_get_PartitionKey_m5393CA895759E34EB672953C0FB276748F25C075_inline (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntryMarshaller__ctor_m26A34C3F0CD48D9B17E2E7F1D24CE6BC12E7B7B3 (PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * __this, const RuntimeMethod* method);
// Amazon.Runtime.Internal.IRequest Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::Marshall(Amazon.Kinesis.Model.PutRecordsRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PutRecordsRequestMarshaller_Marshall_mCEDD148E1A3453293C757B4636EB4C1F4F829FA0 (PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * __this, PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * ___publicRequest0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.DefaultRequest::.ctor(Amazon.Runtime.AmazonWebServiceRequest,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultRequest__ctor_mCC8FA797C2692B14AAF4BB563BB011A03642DAE3 (DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4 * __this, AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * ___request0, String_t* ___serviceName1, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164 (const RuntimeMethod* method);
// System.Void System.IO.StringWriter::.ctor(System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringWriter__ctor_mDF4AB6FD46E8B9824F2F7A9C26EA086A2C1AE5CF (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * __this, RuntimeObject* ___formatProvider0, const RuntimeMethod* method);
// System.Void ThirdParty.Json.LitJson.JsonWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter__ctor_m5ED4B5D93FAD7484093D38FFF40DB5EAD705F0A5 (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___writer0, const RuntimeMethod* method);
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectStart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter_WriteObjectStart_m385A8272F17BCB16C9F994381B6BADCE1CFD6A6A (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.Transform.JsonMarshallerContext::.ctor(Amazon.Runtime.Internal.IRequest,ThirdParty.Json.LitJson.JsonWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonMarshallerContext__ctor_m72EC1A523D8129B418D93DAA8CB9D73FF5DC8E76 (JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * __this, RuntimeObject* ___request0, JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * ___writer1, const RuntimeMethod* method);
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequest::IsSetRecords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequest_IsSetRecords_m3B5885C7C7269428EB9F4E2473822C278D0F9C55 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method);
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayStart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter_WriteArrayStart_mB1FE97CA88553F8E58669D8438BABEBD586F98C9 (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry> Amazon.Kinesis.Model.PutRecordsRequest::get_Records()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * PutRecordsRequest_get_Records_m0B82025129D059C456710C59395D3880206A8C53_inline (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry>::GetEnumerator()
inline Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B  List_1_GetEnumerator_m9D2B1DB4562661F72F9B89936F8868F603AE171C (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B  (*) (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Amazon.Kinesis.Model.PutRecordsRequestEntry>::get_Current()
inline PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * Enumerator_get_Current_m2C4D23C823D545395FDBC79BF090573AEA5260C9_inline (Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B * __this, const RuntimeMethod* method)
{
	return ((  PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * (*) (Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller::Marshall(Amazon.Kinesis.Model.PutRecordsRequestEntry,Amazon.Runtime.Internal.Transform.JsonMarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntryMarshaller_Marshall_m135B7606A8596643EAC3E28D85A93152FBC36F9C (PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * __this, PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * ___requestObject0, JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * ___context1, const RuntimeMethod* method);
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteObjectEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter_WriteObjectEnd_m127A9318E08F34226D40487B6D833F540B86A679 (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Amazon.Kinesis.Model.PutRecordsRequestEntry>::MoveNext()
inline bool Enumerator_MoveNext_m48595B6CB451BC53D2617A05AC16BEE5AFE23788 (Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Amazon.Kinesis.Model.PutRecordsRequestEntry>::Dispose()
inline void Enumerator_Dispose_m28D1709F9D0841D7B183C81F0C0ADF48E0514269 (Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void ThirdParty.Json.LitJson.JsonWriter::WriteArrayEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonWriter_WriteArrayEnd_m5B07BFF16123F3B2D07899B10B056F8DAD9758C6 (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * __this, const RuntimeMethod* method);
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequest::IsSetStreamName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequest_IsSetStreamName_m01C05A7199CF74108A6FB89B0D3D611BB8E63837 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method);
// System.String Amazon.Kinesis.Model.PutRecordsRequest::get_StreamName()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PutRecordsRequest_get_StreamName_m45845D00DC2F171F10E982D2C435C8322A6C1438_inline (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E (const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestMarshaller__ctor_m82B5BDAD41CAE70F37B843DFC4F1FA6634CC0B5F (PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32>::GetValueOrDefault()
inline int32_t Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_gshared_inline)(__this, method);
}
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
inline void Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184 (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_gshared)(__this, ___value0, method);
}
// System.Void System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry>::.ctor()
inline void List_1__ctor_m6D7DA5C1785CDE161C1059E3ED6AD426AE0D697E (List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Amazon.Runtime.AmazonWebServiceResponse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonWebServiceResponse__ctor_mBD52EF4DA0C1AC9176DC586E5BFC88F85F511788 (AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponse__ctor_m60FD6F750C159EB03777EB7535CD190E50E1345B (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, const RuntimeMethod* method);
// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::TestExpression(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30 (UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B * __this, String_t* ___expression0, int32_t ___startingStackDepth1, const RuntimeMethod* method);
// Amazon.Runtime.Internal.Transform.StringUnmarshaller Amazon.Runtime.Internal.Transform.StringUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline (const RuntimeMethod* method);
// System.String Amazon.Runtime.Internal.Transform.StringUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415 (StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method);
// Amazon.Kinesis.EncryptionType Amazon.Kinesis.EncryptionType::op_Implicit(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * EncryptionType_op_Implicit_m3DFC47783601FD569AB21E0CE01B0BBFA1AA02FC (String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::set_EncryptionType(Amazon.Kinesis.EncryptionType)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResponse_set_EncryptionType_m656BB1986ED3E88A812EF202E6BAB9C0805D6651_inline (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ___value0, const RuntimeMethod* method);
// Amazon.Runtime.Internal.Transform.IntUnmarshaller Amazon.Runtime.Internal.Transform.IntUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * IntUnmarshaller_get_Instance_m746BA04951EF77262464E3E09FDC12BFDAA4E321_inline (const RuntimeMethod* method);
// System.Int32 Amazon.Runtime.Internal.Transform.IntUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IntUnmarshaller_Unmarshall_m6DC0F5DE60C245CE4314BB8D8A7671441EDC46E7 (IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::set_FailedRecordCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponse_set_FailedRecordCount_m9A84BA582D19D6830CABFBC4706797830C4F9C6F (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, int32_t ___value0, const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * PutRecordsResultEntryUnmarshaller_get_Instance_m603433B97D4171B1A877D86684D33CE0E61E054D_inline (const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.Kinesis.Model.PutRecordsResultEntry,Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller>::.ctor(!1)
inline void ListUnmarshaller_2__ctor_m89D5E1C842B667C35641BB71F900B26FADB8E27A (ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 * __this, PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * ___iUnmarshaller0, const RuntimeMethod* method)
{
	((  void (*) (ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 *, PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA *, const RuntimeMethod*))ListUnmarshaller_2__ctor_mD70243E8F411BC9C3BD3A55FA1C8DF836D746B97_gshared)(__this, ___iUnmarshaller0, method);
}
// System.Collections.Generic.List`1<!0> Amazon.Runtime.Internal.Transform.ListUnmarshaller`2<Amazon.Kinesis.Model.PutRecordsResultEntry,Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller>::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
inline List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * ListUnmarshaller_2_Unmarshall_mDFDC8DA37111C8507A123F81BDABC8FB3735857B (ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	return ((  List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * (*) (ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 *, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 *, const RuntimeMethod*))ListUnmarshaller_2_Unmarshall_mF0CBC3D0365ACF97BF01B927B26AE17FE112B1D5_gshared)(__this, ___context0, method);
}
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::set_Records(System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResponse_set_Records_mA1E535E86B2B709A6941EEEC5A96D87C91F6335A_inline (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * ___value0, const RuntimeMethod* method);
// Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::GetInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB * JsonErrorResponseUnmarshaller_GetInstance_mF38659F6157C8EDF253734672FFD34FCD188AAA3 (const RuntimeMethod* method);
// Amazon.Runtime.Internal.ErrorResponse Amazon.Runtime.Internal.Transform.JsonErrorResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * JsonErrorResponseUnmarshaller_Unmarshall_m057826DF6640BD156507A76F4BDA8CEB1C43EE9B (JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.ErrorResponse::set_InnerException(System.Exception)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, Exception_t * ___value0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.ErrorResponse::set_StatusCode(System.Net.HttpStatusCode)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Byte[] Amazon.Runtime.Internal.Transform.UnmarshallerContext::GetResponseBodyBytes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* UnmarshallerContext_GetResponseBodyBytes_m7AA3163269840F761D8DEC6A78CEA74C46C95DAE (UnmarshallerContext_t19CB82A3165D313386450AF9D51D9CCC3DCADB7B * __this, const RuntimeMethod* method);
// System.Void System.IO.MemoryStream::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MemoryStream__ctor_m3E041ADD3DB7EA42E7DB56FE862097819C02B9C2 (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::.ctor(System.IO.Stream,System.Boolean,Amazon.Runtime.Internal.Transform.IWebResponseData,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonUnmarshallerContext__ctor_mF3B6DEDDF1A70D283EFD799EDAAE8A952AC87D7D (JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___responseStream0, bool ___maintainResponseBody1, RuntimeObject* ___responseData2, bool ___isException3, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * InvalidArgumentExceptionUnmarshaller_get_Instance_m5A3C388F04C21D2FD79D9937BF227DCAAB5E6E5E_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * KMSAccessDeniedExceptionUnmarshaller_get_Instance_m7CFCC5F6EE51556E83FE82FFFAF8420240B3A58F_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * KMSDisabledExceptionUnmarshaller_get_Instance_m9B0A090129654FD01B9FF75448776DE135F62360_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * KMSInvalidStateExceptionUnmarshaller_get_Instance_mB7E189F9F3EED17B1738DEAC65C14CF08A31366F_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * KMSNotFoundExceptionUnmarshaller_get_Instance_mF5D5B0BAB7DDB7ED4A1ECD86EEF0497B3BF2DCAE_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * KMSOptInRequiredExceptionUnmarshaller_get_Instance_m0F9A55C20A1FC41CBA5F3F26833A38D1D126666D_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * KMSThrottlingExceptionUnmarshaller_get_Instance_m66206A46CF3E7C5A7A9020DCE942FA3FBAFC6BDC_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * ProvisionedThroughputExceededExceptionUnmarshaller_get_Instance_mC05CFB60AD500DA5874EE0BF8B74263AED866DBB_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * ResourceNotFoundExceptionUnmarshaller_get_Instance_mE30554BC8F95AF3250FC3972F929F1D37C638C33_inline (const RuntimeMethod* method);
// Amazon.Kinesis.Model.ResourceNotFoundException Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * ResourceNotFoundExceptionUnmarshaller_Unmarshall_m080557324DF57827FAF33DC7E9C3A4ABFFB375C1 (ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method);
// System.Void Amazon.Runtime.Internal.Transform.JsonResponseUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonResponseUnmarshaller__ctor_m7A471A9D41EBFE83FEA37BAAD40BCF2D485F9ADE (JsonResponseUnmarshaller_t6A1B8EBE5327D6B9A39E51DB1A23C2593CFA1AFF * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponseUnmarshaller__ctor_mA565C6583D8EE26409E5FF4AD10573F4B0C81EC8 (PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * __this, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83 (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * __this, const RuntimeMethod* method);
// ThirdParty.Json.LitJson.JsonToken Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::get_CurrentTokenType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t JsonUnmarshallerContext_get_CurrentTokenType_mA60F9E7148B1EF1BCD664D3AB068A81910999E19 (JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntry__ctor_mC05AB10DE255D87608774275EF30A8DA8C2F5368 (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_ErrorCode(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ErrorCode_mBD158DC0CC885F5B518CE4F48D5D1F7A07C8C050_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_ErrorMessage(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ErrorMessage_m431BBF0E595CDEB59DA750B3BE3099F232D849DF_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_SequenceNumber(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_SequenceNumber_mE137FD3C0A7B5BB0D21BE05B927D4C526D5098EE_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_ShardId(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ShardId_mFD4ADEE87AF692E117D37647479BD359F56D6C83_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntryUnmarshaller__ctor_m846B750DDEB9CCBFB74F923A44B957A573924CC1 (PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * __this, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.ResourceNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResourceNotFoundException__ctor_mF9051383698D65CF7D915BF2230CE0F698D73715 (ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method);
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResourceNotFoundExceptionUnmarshaller__ctor_m3032315C83DB7FF387C99236E17485DCB10E1ADB (ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.AmazonKinesisClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.RegionEndpoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisClient__ctor_m7A86375542E321EB814A02A01DE2E9716583909D (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * ___credentials0, RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * ___region1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * L_0 = ___credentials0;
		AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * L_1 = (AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D *)il2cpp_codegen_object_new(AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var);
		AmazonKinesisConfig__ctor_m61B8B11CE83581F976E212D24DDCE8C60EF1D98A(L_1, /*hidden argument*/NULL);
		AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * L_2 = L_1;
		RegionEndpoint_t0EBE9B741F8A9076657BD0F0D2A20DF721C1CE9A * L_3 = ___region1;
		NullCheck(L_2);
		ClientConfig_set_RegionEndpoint_mC0C36203BCE0F2423D15D593A156B100A41A003E(L_2, L_3, /*hidden argument*/NULL);
		AmazonKinesisClient__ctor_mA422F58AF0BFF082F71B466CEA6D02F630516F57(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.AmazonKinesisClient::.ctor(Amazon.Runtime.AWSCredentials,Amazon.Kinesis.AmazonKinesisConfig)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisClient__ctor_mA422F58AF0BFF082F71B466CEA6D02F630516F57 (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * ___credentials0, AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * ___clientConfig1, const RuntimeMethod* method)
{
	{
		AWSCredentials_t0280B3022E1EBBB54CD2E3A7DF65CBA3421DED06 * L_0 = ___credentials0;
		AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * L_1 = ___clientConfig1;
		AmazonServiceClient__ctor_mBF0730EE760A3209048451CB7B95CC3F796985AD(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Kinesis.AmazonKinesisClient::CreateSigner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AbstractAWSSigner_t4B1FD30F433047677EC7DEDEE4EB23CC1F4131EE * AmazonKinesisClient_CreateSigner_m3B72C7627D4B8D4CAC21A3875271EE7B30D4FE49 (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D * L_0 = (AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D *)il2cpp_codegen_object_new(AWS4Signer_tE3D64418923132213313546C10C0C83B424D218D_il2cpp_TypeInfo_var);
		AWS4Signer__ctor_m0C5E9E6AFF7BD86B722135DDE17D770894463306(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// Amazon.Runtime.Internal.IServiceMetadata Amazon.Kinesis.AmazonKinesisClient::get_ServiceMetadata()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AmazonKinesisClient_get_ServiceMetadata_mC8C2B92F3684B05554BE45B75D9BD8067931A719 (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_StaticFields*)il2cpp_codegen_static_fields_for(AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_il2cpp_TypeInfo_var))->get_serviceMetadata_12();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.AmazonKinesisClient::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisClient_Dispose_m26E41203C6D39C74187F6EDDEA8A289B73E422A5 (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___disposing0;
		AmazonServiceClient_Dispose_mF1BDBFD1F83C742FA192AFFA3896B74DCEFDC4F7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Threading.Tasks.Task`1<Amazon.Kinesis.Model.PutRecordsResponse> Amazon.Kinesis.AmazonKinesisClient::PutRecordsAsync(Amazon.Kinesis.Model.PutRecordsRequest,System.Threading.CancellationToken)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685 * AmazonKinesisClient_PutRecordsAsync_mD007837E101D060AB7EC376E883198474C2E614B (AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0 * __this, PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * ___request0, CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  ___cancellationToken1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonServiceClient_InvokeAsync_TisPutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_m0418AEF71BCD7A4F1376054D52A3DB4A35638A56_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 * V_0 = NULL;
	{
		InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 * L_0 = (InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 *)il2cpp_codegen_object_new(InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55_il2cpp_TypeInfo_var);
		InvokeOptions__ctor_mC8DD478F8404260C05FD2E6FF4D26C944E216AB4(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * L_2;
		L_2 = PutRecordsRequestMarshaller_get_Instance_m863F7927ABAC02B5A1083E29BDDFF08AFB5479AA_inline(/*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< RuntimeObject* >::Invoke(5 /* System.Void Amazon.Runtime.Internal.InvokeOptionsBase::set_RequestMarshaller(Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>) */, L_1, L_2);
		InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * L_4;
		L_4 = PutRecordsResponseUnmarshaller_get_Instance_mCAE356524F66AE7AE050AA8CCE6CE1327AC1EF3E_inline(/*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< ResponseUnmarshaller_t237F2F84B907308309E0A65B6686E566B1E0FC21 * >::Invoke(7 /* System.Void Amazon.Runtime.Internal.InvokeOptionsBase::set_ResponseUnmarshaller(Amazon.Runtime.Internal.Transform.ResponseUnmarshaller) */, L_3, L_4);
		PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * L_5 = ___request0;
		InvokeOptions_t0F2B78FB9B8BC3105C9BEAC02D483CE9FB137F55 * L_6 = V_0;
		CancellationToken_tC9D68381C9164A4BA10397257E87ADC832AF5FFD  L_7 = ___cancellationToken1;
		Task_1_t00B2976B3ACF1BAB0CDF56C8B6A663DE58D08685 * L_8;
		L_8 = AmazonServiceClient_InvokeAsync_TisPutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_m0418AEF71BCD7A4F1376054D52A3DB4A35638A56(__this, L_5, L_6, L_7, /*hidden argument*/AmazonServiceClient_InvokeAsync_TisPutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_m0418AEF71BCD7A4F1376054D52A3DB4A35638A56_RuntimeMethod_var);
		return L_8;
	}
}
// System.Void Amazon.Kinesis.AmazonKinesisClient::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisClient__cctor_m76D0287460B005888D4DA197AF57CB73139BEF9B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD * L_0 = (AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD *)il2cpp_codegen_object_new(AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD_il2cpp_TypeInfo_var);
		AmazonKinesisMetadata__ctor_m9D0AC128854D2B470A460E8D331ECEAF982B45F3(L_0, /*hidden argument*/NULL);
		((AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_StaticFields*)il2cpp_codegen_static_fields_for(AmazonKinesisClient_tDDDCBF190A156252AFA0479F5226A082EC2A77C0_il2cpp_TypeInfo_var))->set_serviceMetadata_12(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.AmazonKinesisConfig::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisConfig__ctor_m61B8B11CE83581F976E212D24DDCE8C60EF1D98A (AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3321C3E441B4DD2FCC481DE41D1977249A6A5B81);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var);
		String_t* L_0 = ((AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_StaticFields*)il2cpp_codegen_static_fields_for(AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var))->get_UserAgentString_34();
		__this->set__userAgent_35(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099_il2cpp_TypeInfo_var);
		ClientConfig__ctor_mF7458A55808D9FE37F6F49A9E7E1BBE807B3181D(__this, /*hidden argument*/NULL);
		ClientConfig_set_AuthenticationServiceName_mA6A0E4F14EEA4C9A42061D4C18AEF69785CF83E1_inline(__this, _stringLiteral3321C3E441B4DD2FCC481DE41D1977249A6A5B81, /*hidden argument*/NULL);
		return;
	}
}
// System.String Amazon.Kinesis.AmazonKinesisConfig::get_RegionEndpointServiceName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AmazonKinesisConfig_get_RegionEndpointServiceName_mBF79258F1AB1F6BE30C428B0ABCA46C28ED84892 (AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3321C3E441B4DD2FCC481DE41D1977249A6A5B81);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3321C3E441B4DD2FCC481DE41D1977249A6A5B81;
	}
}
// System.String Amazon.Kinesis.AmazonKinesisConfig::get_UserAgent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AmazonKinesisConfig_get_UserAgent_m920C51ED5BF6A3B78C9235C4C7C92C834F6AAED2 (AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__userAgent_35();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.AmazonKinesisConfig::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisConfig__cctor_m75F0C315A506AC201D6A9C09BB5DD869B596D16A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InternalSDKUtils_t279A42889919AF1D76C646A48A51BC4025B7FE13_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC1D7C6FF863D0BF9263EEE11BFD63292BD62B7F2);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalSDKUtils_t279A42889919AF1D76C646A48A51BC4025B7FE13_il2cpp_TypeInfo_var);
		String_t* L_0;
		L_0 = InternalSDKUtils_BuildUserAgentString_m2C1A262CB88B697FA16980D3485B3DF3BD7C93D4(_stringLiteralC1D7C6FF863D0BF9263EEE11BFD63292BD62B7F2, /*hidden argument*/NULL);
		((AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_StaticFields*)il2cpp_codegen_static_fields_for(AmazonKinesisConfig_tD5EB3F95F54D50A5D7D1E07A36DE76E521A6438D_il2cpp_TypeInfo_var))->set_UserAgentString_34(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.AmazonKinesisException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9 (AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonServiceException__ctor_m6A5B1D6DD41F926B3921A96F5259093DDA64E487(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Amazon.Kinesis.Internal.AmazonKinesisMetadata::get_ServiceId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AmazonKinesisMetadata_get_ServiceId_m6BD29E7D6411C2C9F9C9940DC8C3319E81B6B078 (AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8E504213A7C2697E5C226BAE5003D6F866C3523);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteralC8E504213A7C2697E5C226BAE5003D6F866C3523;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Kinesis.Internal.AmazonKinesisMetadata::get_OperationNameMapping()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AmazonKinesisMetadata_get_OperationNameMapping_m121DA76D159D70759DBB32B1DC88CFDF7DE75252 (AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m374496B3B8F3532FC52B465384E6C950DDA79F8B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_0 = (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)il2cpp_codegen_object_new(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m374496B3B8F3532FC52B465384E6C950DDA79F8B(L_0, 0, /*hidden argument*/Dictionary_2__ctor_m374496B3B8F3532FC52B465384E6C950DDA79F8B_RuntimeMethod_var);
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Internal.AmazonKinesisMetadata::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisMetadata__ctor_m9D0AC128854D2B470A460E8D331ECEAF982B45F3 (AmazonKinesisMetadata_t42CE96593E0CA7684FD9C28EF3DAE1E46203FCBD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.AmazonKinesisRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmazonKinesisRequest__ctor_m9B5FCB0CBD93CB1FD51497E65C9BF2B274009B92 (AmazonKinesisRequest_t6FADAEC3B6BFB9A305B4B0D75615BCC7DD5B0635 * __this, const RuntimeMethod* method)
{
	{
		AmazonWebServiceRequest__ctor_m2F70AA8C6602940F5E5A6AEAE90FBED8247D9FB9(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.EncryptionType::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EncryptionType__ctor_m5A44FFAFB9729B6C8785FC6890C3D9874C446175 (EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var);
		ConstantClass__ctor_m2E56E06C64185F9EAEC5D3630DF2C35E6E8E2E9B(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Amazon.Kinesis.EncryptionType Amazon.Kinesis.EncryptionType::FindValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * EncryptionType_FindValue_mAE3F6719EAC3829296885A4F2ECD138077E0AC2B (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConstantClass_FindValue_TisEncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_m45BFB226A317DFED373E14B26E0D3493151FC468_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ConstantClass_t57E778846C9666151BF0C3C6A5596A0AB18752A6_il2cpp_TypeInfo_var);
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_1;
		L_1 = ConstantClass_FindValue_TisEncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_m45BFB226A317DFED373E14B26E0D3493151FC468(L_0, /*hidden argument*/ConstantClass_FindValue_TisEncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_m45BFB226A317DFED373E14B26E0D3493151FC468_RuntimeMethod_var);
		return L_1;
	}
}
// Amazon.Kinesis.EncryptionType Amazon.Kinesis.EncryptionType::op_Implicit(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * EncryptionType_op_Implicit_m3DFC47783601FD569AB21E0CE01B0BBFA1AA02FC (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_1;
		L_1 = EncryptionType_FindValue_mAE3F6719EAC3829296885A4F2ECD138077E0AC2B(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Amazon.Kinesis.EncryptionType::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EncryptionType__cctor_m30C8D43A57D6960683FC191458E7564324B79346 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD4FF15F0C367172440AA7A961D856E09E93C8108);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDCFD6604DC9979C11967D249B507578379EDBF28);
		s_Il2CppMethodInitialized = true;
	}
	{
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_0 = (EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F *)il2cpp_codegen_object_new(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		EncryptionType__ctor_m5A44FFAFB9729B6C8785FC6890C3D9874C446175(L_0, _stringLiteralDCFD6604DC9979C11967D249B507578379EDBF28, /*hidden argument*/NULL);
		((EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_StaticFields*)il2cpp_codegen_static_fields_for(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var))->set_KMS_3(L_0);
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_1 = (EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F *)il2cpp_codegen_object_new(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		EncryptionType__ctor_m5A44FFAFB9729B6C8785FC6890C3D9874C446175(L_1, _stringLiteralD4FF15F0C367172440AA7A961D856E09E93C8108, /*hidden argument*/NULL);
		((EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_StaticFields*)il2cpp_codegen_static_fields_for(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var))->set_NONE_4(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.InvalidArgumentException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidArgumentException__ctor_mCC2E1D2514B57E6EC1C03CEC7E691F7D558E7B37 (InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.InvalidArgumentException Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * InvalidArgumentExceptionUnmarshaller_Unmarshall_mCABC0FAE591F0D9B3C405EF287948B2D07EC5A27 (InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * L_2;
		L_2 = InvalidArgumentExceptionUnmarshaller_Unmarshall_m9498CC9012F65405D1309ECDB0DFCF4494703EF7(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.InvalidArgumentException Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * InvalidArgumentExceptionUnmarshaller_Unmarshall_m9498CC9012F65405D1309ECDB0DFCF4494703EF7 (InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * L_14 = (InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF *)il2cpp_codegen_object_new(InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF_il2cpp_TypeInfo_var);
		InvalidArgumentException__ctor_mCC2E1D2514B57E6EC1C03CEC7E691F7D558E7B37(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * InvalidArgumentExceptionUnmarshaller_get_Instance_m5A3C388F04C21D2FD79D9937BF227DCAAB5E6E5E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * L_0 = ((InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_StaticFields*)il2cpp_codegen_static_fields_for(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidArgumentExceptionUnmarshaller__ctor_m0B06463E543528B539EDE25D58F57A5C8349EDF5 (InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.InvalidArgumentExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidArgumentExceptionUnmarshaller__cctor_m4375C856C931D969239D544D07AA4B59F8CF55C8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * L_0 = (InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 *)il2cpp_codegen_object_new(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		InvalidArgumentExceptionUnmarshaller__ctor_m0B06463E543528B539EDE25D58F57A5C8349EDF5(L_0, /*hidden argument*/NULL);
		((InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_StaticFields*)il2cpp_codegen_static_fields_for(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.KMSAccessDeniedException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSAccessDeniedException__ctor_mA695FF0824E4ED92482CAB96A0DF12B4AD8A8273 (KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.KMSAccessDeniedException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * KMSAccessDeniedExceptionUnmarshaller_Unmarshall_m9D627DE5FF8BB4F90C3B9A969835EC9BB4B8C558 (KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * L_2;
		L_2 = KMSAccessDeniedExceptionUnmarshaller_Unmarshall_m7512E68CFE3157D6371AA838CA213140DE413CF5(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.KMSAccessDeniedException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * KMSAccessDeniedExceptionUnmarshaller_Unmarshall_m7512E68CFE3157D6371AA838CA213140DE413CF5 (KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * L_14 = (KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 *)il2cpp_codegen_object_new(KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44_il2cpp_TypeInfo_var);
		KMSAccessDeniedException__ctor_mA695FF0824E4ED92482CAB96A0DF12B4AD8A8273(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * KMSAccessDeniedExceptionUnmarshaller_get_Instance_m7CFCC5F6EE51556E83FE82FFFAF8420240B3A58F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * L_0 = ((KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_StaticFields*)il2cpp_codegen_static_fields_for(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSAccessDeniedExceptionUnmarshaller__ctor_m89BD64F04C19D7D45F0DA7A8640EA73DF713D2BD (KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSAccessDeniedExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSAccessDeniedExceptionUnmarshaller__cctor_mFE0F3644CBF22E2678DE1D85488776F87E8C7547 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * L_0 = (KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B *)il2cpp_codegen_object_new(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		KMSAccessDeniedExceptionUnmarshaller__ctor_m89BD64F04C19D7D45F0DA7A8640EA73DF713D2BD(L_0, /*hidden argument*/NULL);
		((KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_StaticFields*)il2cpp_codegen_static_fields_for(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.KMSDisabledException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSDisabledException__ctor_mF66CC7234E73C1A7160E803A2344F5B8BC5698B7 (KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.KMSDisabledException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * KMSDisabledExceptionUnmarshaller_Unmarshall_mB575128B35978F8137E5F08BF59CCC22CF3FF8EA (KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * L_2;
		L_2 = KMSDisabledExceptionUnmarshaller_Unmarshall_mB574723FC32FCB75EF56889945657C4EC48C7CDF(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.KMSDisabledException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * KMSDisabledExceptionUnmarshaller_Unmarshall_mB574723FC32FCB75EF56889945657C4EC48C7CDF (KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * L_14 = (KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A *)il2cpp_codegen_object_new(KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A_il2cpp_TypeInfo_var);
		KMSDisabledException__ctor_mF66CC7234E73C1A7160E803A2344F5B8BC5698B7(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * KMSDisabledExceptionUnmarshaller_get_Instance_m9B0A090129654FD01B9FF75448776DE135F62360 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * L_0 = ((KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_StaticFields*)il2cpp_codegen_static_fields_for(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSDisabledExceptionUnmarshaller__ctor_m64BB9F8EAADAE1DDFAD756544CF3A8A2F7AFCCCB (KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSDisabledExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSDisabledExceptionUnmarshaller__cctor_m68A91E17E477EB2E0113324729DB731FA8883616 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * L_0 = (KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF *)il2cpp_codegen_object_new(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		KMSDisabledExceptionUnmarshaller__ctor_m64BB9F8EAADAE1DDFAD756544CF3A8A2F7AFCCCB(L_0, /*hidden argument*/NULL);
		((KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_StaticFields*)il2cpp_codegen_static_fields_for(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.KMSInvalidStateException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSInvalidStateException__ctor_mA54A1F35E8B2FCC2BAA11393256CAFAED9A4B30F (KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.KMSInvalidStateException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * KMSInvalidStateExceptionUnmarshaller_Unmarshall_m16ACCE7A952BAD4AC9A2F632D6C5340CDD860D58 (KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * L_2;
		L_2 = KMSInvalidStateExceptionUnmarshaller_Unmarshall_m8AECCA10430208CD1BA8DAFA3EED99F28E9C93F9(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.KMSInvalidStateException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * KMSInvalidStateExceptionUnmarshaller_Unmarshall_m8AECCA10430208CD1BA8DAFA3EED99F28E9C93F9 (KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * L_14 = (KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 *)il2cpp_codegen_object_new(KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068_il2cpp_TypeInfo_var);
		KMSInvalidStateException__ctor_mA54A1F35E8B2FCC2BAA11393256CAFAED9A4B30F(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * KMSInvalidStateExceptionUnmarshaller_get_Instance_mB7E189F9F3EED17B1738DEAC65C14CF08A31366F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * L_0 = ((KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_StaticFields*)il2cpp_codegen_static_fields_for(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSInvalidStateExceptionUnmarshaller__ctor_mDB045E97FC64C6D60CC5B26ED7A962CCCB761B65 (KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSInvalidStateExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSInvalidStateExceptionUnmarshaller__cctor_m10323617CD9490DF702217EA132E18F8BDB6A072 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * L_0 = (KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 *)il2cpp_codegen_object_new(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		KMSInvalidStateExceptionUnmarshaller__ctor_mDB045E97FC64C6D60CC5B26ED7A962CCCB761B65(L_0, /*hidden argument*/NULL);
		((KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_StaticFields*)il2cpp_codegen_static_fields_for(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.KMSNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSNotFoundException__ctor_mFEA7EB9F5DC486A026019DFFA043394578BEA10C (KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.KMSNotFoundException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * KMSNotFoundExceptionUnmarshaller_Unmarshall_m26F7B8AFD92EECAF8BAE3FF01654C699011C9CDF (KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * L_2;
		L_2 = KMSNotFoundExceptionUnmarshaller_Unmarshall_mA9C221F55F082CA98EA09CC7C4D08E6FA92015AB(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.KMSNotFoundException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * KMSNotFoundExceptionUnmarshaller_Unmarshall_mA9C221F55F082CA98EA09CC7C4D08E6FA92015AB (KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * L_14 = (KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 *)il2cpp_codegen_object_new(KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1_il2cpp_TypeInfo_var);
		KMSNotFoundException__ctor_mFEA7EB9F5DC486A026019DFFA043394578BEA10C(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * KMSNotFoundExceptionUnmarshaller_get_Instance_mF5D5B0BAB7DDB7ED4A1ECD86EEF0497B3BF2DCAE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * L_0 = ((KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_StaticFields*)il2cpp_codegen_static_fields_for(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSNotFoundExceptionUnmarshaller__ctor_m85DFC6A1DBE9DD34D7B751E79CCDDAC7B3D953DD (KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSNotFoundExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSNotFoundExceptionUnmarshaller__cctor_mE88F539B04CC2D1DCB8005E08E4A8A9A6C867F25 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * L_0 = (KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 *)il2cpp_codegen_object_new(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		KMSNotFoundExceptionUnmarshaller__ctor_m85DFC6A1DBE9DD34D7B751E79CCDDAC7B3D953DD(L_0, /*hidden argument*/NULL);
		((KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_StaticFields*)il2cpp_codegen_static_fields_for(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.KMSOptInRequiredException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSOptInRequiredException__ctor_mA62F89061DA9CCF9EAE9395CE146B676D298C51E (KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.KMSOptInRequiredException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * KMSOptInRequiredExceptionUnmarshaller_Unmarshall_m33A6781EBB548707FD0D83845C6DE77C5B4B5193 (KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * L_2;
		L_2 = KMSOptInRequiredExceptionUnmarshaller_Unmarshall_m971A2AD42993EB0E84F40524B6367132EE6B2F90(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.KMSOptInRequiredException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * KMSOptInRequiredExceptionUnmarshaller_Unmarshall_m971A2AD42993EB0E84F40524B6367132EE6B2F90 (KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * L_14 = (KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 *)il2cpp_codegen_object_new(KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785_il2cpp_TypeInfo_var);
		KMSOptInRequiredException__ctor_mA62F89061DA9CCF9EAE9395CE146B676D298C51E(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * KMSOptInRequiredExceptionUnmarshaller_get_Instance_m0F9A55C20A1FC41CBA5F3F26833A38D1D126666D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * L_0 = ((KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_StaticFields*)il2cpp_codegen_static_fields_for(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSOptInRequiredExceptionUnmarshaller__ctor_m2ABACB57C9808F3BD5A9FF6C60430CD1839087B1 (KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSOptInRequiredExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSOptInRequiredExceptionUnmarshaller__cctor_m54F323BC2297492BFE077DE303F9ABF26341C1D7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * L_0 = (KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 *)il2cpp_codegen_object_new(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		KMSOptInRequiredExceptionUnmarshaller__ctor_m2ABACB57C9808F3BD5A9FF6C60430CD1839087B1(L_0, /*hidden argument*/NULL);
		((KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_StaticFields*)il2cpp_codegen_static_fields_for(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.KMSThrottlingException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSThrottlingException__ctor_m351089A433E7E63591E94F7034436952CE74E24A (KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.KMSThrottlingException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * KMSThrottlingExceptionUnmarshaller_Unmarshall_m7B42C829742B0B5101A0A7462474682F3FE51697 (KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * L_2;
		L_2 = KMSThrottlingExceptionUnmarshaller_Unmarshall_m45AC671D3AB3E89505ECE3A0A47116DBA7156A8B(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.KMSThrottlingException Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * KMSThrottlingExceptionUnmarshaller_Unmarshall_m45AC671D3AB3E89505ECE3A0A47116DBA7156A8B (KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * L_14 = (KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E *)il2cpp_codegen_object_new(KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E_il2cpp_TypeInfo_var);
		KMSThrottlingException__ctor_m351089A433E7E63591E94F7034436952CE74E24A(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * KMSThrottlingExceptionUnmarshaller_get_Instance_m66206A46CF3E7C5A7A9020DCE942FA3FBAFC6BDC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * L_0 = ((KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_StaticFields*)il2cpp_codegen_static_fields_for(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSThrottlingExceptionUnmarshaller__ctor_mB2D2053DECA3E7A09E117FDCBDC07D5854A09495 (KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.KMSThrottlingExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KMSThrottlingExceptionUnmarshaller__cctor_m5682AF2C25EF28F647CDCEEF6405766D7ED9EDD5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * L_0 = (KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 *)il2cpp_codegen_object_new(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		KMSThrottlingExceptionUnmarshaller__ctor_mB2D2053DECA3E7A09E117FDCBDC07D5854A09495(L_0, /*hidden argument*/NULL);
		((KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_StaticFields*)il2cpp_codegen_static_fields_for(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.ProvisionedThroughputExceededException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProvisionedThroughputExceededException__ctor_m4E80B47B6B7B890EDF63C7F023EEAD53E0FFD417 (ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.ProvisionedThroughputExceededException Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * ProvisionedThroughputExceededExceptionUnmarshaller_Unmarshall_m6162C894818D74F67272684B1D62974CE2894AFF (ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * L_2;
		L_2 = ProvisionedThroughputExceededExceptionUnmarshaller_Unmarshall_mC8C5292AAD0B3F329DC9220FFCAE60DCC5A0857E(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.ProvisionedThroughputExceededException Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * ProvisionedThroughputExceededExceptionUnmarshaller_Unmarshall_mC8C5292AAD0B3F329DC9220FFCAE60DCC5A0857E (ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * L_14 = (ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 *)il2cpp_codegen_object_new(ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87_il2cpp_TypeInfo_var);
		ProvisionedThroughputExceededException__ctor_m4E80B47B6B7B890EDF63C7F023EEAD53E0FFD417(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * ProvisionedThroughputExceededExceptionUnmarshaller_get_Instance_mC05CFB60AD500DA5874EE0BF8B74263AED866DBB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * L_0 = ((ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_StaticFields*)il2cpp_codegen_static_fields_for(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProvisionedThroughputExceededExceptionUnmarshaller__ctor_m6CFB55B10800C6A26438F0B82386C9CAC62AD3C7 (ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.ProvisionedThroughputExceededExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProvisionedThroughputExceededExceptionUnmarshaller__cctor_mB84C11E75B0A5BC64DF45FEF0F680DBD1E92A9D1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * L_0 = (ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 *)il2cpp_codegen_object_new(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		ProvisionedThroughputExceededExceptionUnmarshaller__ctor_m6CFB55B10800C6A26438F0B82386C9CAC62AD3C7(L_0, /*hidden argument*/NULL);
		((ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_StaticFields*)il2cpp_codegen_static_fields_for(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry> Amazon.Kinesis.Model.PutRecordsRequest::get_Records()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * PutRecordsRequest_get_Records_m0B82025129D059C456710C59395D3880206A8C53 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	{
		List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_0 = __this->get__records_3();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsRequest::set_Records(System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsRequestEntry>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequest_set_Records_mC8E9D4DACAA0D0E98226EE31E0B52E90C4EC7FB1 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_0 = ___value0;
		__this->set__records_3(L_0);
		return;
	}
}
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequest::IsSetRecords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequest_IsSetRecords_m3B5885C7C7269428EB9F4E2473822C278D0F9C55 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC4281CDDAF1829194CAF38FB27521B5859B00B23_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_0 = __this->get__records_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_1 = __this->get__records_3();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_mC4281CDDAF1829194CAF38FB27521B5859B00B23_inline(L_1, /*hidden argument*/List_1_get_Count_mC4281CDDAF1829194CAF38FB27521B5859B00B23_RuntimeMethod_var);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.String Amazon.Kinesis.Model.PutRecordsRequest::get_StreamName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PutRecordsRequest_get_StreamName_m45845D00DC2F171F10E982D2C435C8322A6C1438 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__streamName_4();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsRequest::set_StreamName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequest_set_StreamName_m57B417AB5E4A1E295F968967249575188B0CB78C (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__streamName_4(L_0);
		return;
	}
}
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequest::IsSetStreamName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequest_IsSetStreamName_m01C05A7199CF74108A6FB89B0D3D611BB8E63837 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__streamName_4();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequest__ctor_mCB6AA01DFA7C5760FEBD844A25E8FDD1A1B2D9A1 (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m67E3D76494E3E22BBF70C37077DD5978721472D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_0 = (List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 *)il2cpp_codegen_object_new(List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8_il2cpp_TypeInfo_var);
		List_1__ctor_m67E3D76494E3E22BBF70C37077DD5978721472D5(L_0, /*hidden argument*/List_1__ctor_m67E3D76494E3E22BBF70C37077DD5978721472D5_RuntimeMethod_var);
		__this->set__records_3(L_0);
		AmazonKinesisRequest__ctor_m9B5FCB0CBD93CB1FD51497E65C9BF2B274009B92(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IO.MemoryStream Amazon.Kinesis.Model.PutRecordsRequestEntry::get_Data()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * PutRecordsRequestEntry_get_Data_m6838F4D073A2FBC74A6DC77F8110DBF794D64D96 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_0 = __this->get__data_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsRequestEntry::set_Data(System.IO.MemoryStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntry_set_Data_mC880F499968CAF08B9893BCF12E89F1827F3E370 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * ___value0, const RuntimeMethod* method)
{
	{
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_0 = ___value0;
		__this->set__data_0(L_0);
		return;
	}
}
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequestEntry::IsSetData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequestEntry_IsSetData_mDCB90648700A106D315A6BF5CB2349908A62FD68 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_0 = __this->get__data_0();
		return (bool)((!(((RuntimeObject*)(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.Kinesis.Model.PutRecordsRequestEntry::get_ExplicitHashKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PutRecordsRequestEntry_get_ExplicitHashKey_m0CBD1B2DFC1D20BB67A0ED020694A62A646E044A (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__explicitHashKey_1();
		return L_0;
	}
}
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequestEntry::IsSetExplicitHashKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequestEntry_IsSetExplicitHashKey_mE7285ACD56560FA1FA00C220028E7375B1017398 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__explicitHashKey_1();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.String Amazon.Kinesis.Model.PutRecordsRequestEntry::get_PartitionKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PutRecordsRequestEntry_get_PartitionKey_m5393CA895759E34EB672953C0FB276748F25C075 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__partitionKey_2();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsRequestEntry::set_PartitionKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntry_set_PartitionKey_m2C32E634DFAC2E86C2385DE708A831FF4C022774 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__partitionKey_2(L_0);
		return;
	}
}
// System.Boolean Amazon.Kinesis.Model.PutRecordsRequestEntry::IsSetPartitionKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PutRecordsRequestEntry_IsSetPartitionKey_m2435D5D5A4EE1A7A08F7666F605B153E73D59DAE (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__partitionKey_2();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsRequestEntry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntry__ctor_mAD126BE0542BEF0BF3CBDB34071F34BEDBBF7153 (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller::Marshall(Amazon.Kinesis.Model.PutRecordsRequestEntry,Amazon.Runtime.Internal.Transform.JsonMarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntryMarshaller_Marshall_m135B7606A8596643EAC3E28D85A93152FBC36F9C (PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * __this, PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * ___requestObject0, JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringUtils_tFB5F525A8B6AA6C5D7F0478BEE0B948D6C43EC55_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral33D4E43B349E3E198FD7BC4359029E3D92563948);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4552726700ED073D423A03C5215DC945DA4BF29D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB4D8750C59CE123025990AD22F8E27C3459AF91F);
		s_Il2CppMethodInitialized = true;
	}
	{
		PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_0 = ___requestObject0;
		NullCheck(L_0);
		bool L_1;
		L_1 = PutRecordsRequestEntry_IsSetData_mDCB90648700A106D315A6BF5CB2349908A62FD68(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_2 = ___context1;
		NullCheck(L_2);
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_3;
		L_3 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F(L_3, _stringLiteralB4D8750C59CE123025990AD22F8E27C3459AF91F, /*hidden argument*/NULL);
		JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_4 = ___context1;
		NullCheck(L_4);
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_5;
		L_5 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_4, /*hidden argument*/NULL);
		PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_6 = ___requestObject0;
		NullCheck(L_6);
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_7;
		L_7 = PutRecordsRequestEntry_get_Data_m6838F4D073A2FBC74A6DC77F8110DBF794D64D96_inline(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringUtils_tFB5F525A8B6AA6C5D7F0478BEE0B948D6C43EC55_il2cpp_TypeInfo_var);
		String_t* L_8;
		L_8 = StringUtils_FromMemoryStream_m8390D1D01AD7AC326C5B09E6E0D3116A28930BED(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B(L_5, L_8, /*hidden argument*/NULL);
	}

IL_002e:
	{
		PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_9 = ___requestObject0;
		NullCheck(L_9);
		bool L_10;
		L_10 = PutRecordsRequestEntry_IsSetExplicitHashKey_mE7285ACD56560FA1FA00C220028E7375B1017398(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0057;
		}
	}
	{
		JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_11 = ___context1;
		NullCheck(L_11);
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_12;
		L_12 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F(L_12, _stringLiteral33D4E43B349E3E198FD7BC4359029E3D92563948, /*hidden argument*/NULL);
		JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_13 = ___context1;
		NullCheck(L_13);
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_14;
		L_14 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_13, /*hidden argument*/NULL);
		PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_15 = ___requestObject0;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = PutRecordsRequestEntry_get_ExplicitHashKey_m0CBD1B2DFC1D20BB67A0ED020694A62A646E044A_inline(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B(L_14, L_16, /*hidden argument*/NULL);
	}

IL_0057:
	{
		PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_17 = ___requestObject0;
		NullCheck(L_17);
		bool L_18;
		L_18 = PutRecordsRequestEntry_IsSetPartitionKey_m2435D5D5A4EE1A7A08F7666F605B153E73D59DAE(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0080;
		}
	}
	{
		JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_19 = ___context1;
		NullCheck(L_19);
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_20;
		L_20 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F(L_20, _stringLiteral4552726700ED073D423A03C5215DC945DA4BF29D, /*hidden argument*/NULL);
		JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_21 = ___context1;
		NullCheck(L_21);
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_22;
		L_22 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_21, /*hidden argument*/NULL);
		PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_23 = ___requestObject0;
		NullCheck(L_23);
		String_t* L_24;
		L_24 = PutRecordsRequestEntry_get_PartitionKey_m5393CA895759E34EB672953C0FB276748F25C075_inline(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B(L_22, L_24, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntryMarshaller__ctor_m26A34C3F0CD48D9B17E2E7F1D24CE6BC12E7B7B3 (PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestEntryMarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestEntryMarshaller__cctor_mA85A9CC40D33ACE1BA8AEF9E3C60111B078E0BAC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * L_0 = (PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 *)il2cpp_codegen_object_new(PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var);
		PutRecordsRequestEntryMarshaller__ctor_m26A34C3F0CD48D9B17E2E7F1D24CE6BC12E7B7B3(L_0, /*hidden argument*/NULL);
		((PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var))->set_Instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Runtime.Internal.IRequest Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::Marshall(Amazon.Runtime.AmazonWebServiceRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PutRecordsRequestMarshaller_Marshall_mADFFC1795DC820F4C87364B467E2EBED24E58FB0 (PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * __this, AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AmazonWebServiceRequest_tE2C5128502E604789672ED084834ACADB5CC3D55 * L_0 = ___input0;
		RuntimeObject* L_1;
		L_1 = PutRecordsRequestMarshaller_Marshall_mCEDD148E1A3453293C757B4636EB4C1F4F829FA0(__this, ((PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD *)CastclassClass((RuntimeObject*)L_0, PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// Amazon.Runtime.Internal.IRequest Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::Marshall(Amazon.Kinesis.Model.PutRecordsRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PutRecordsRequestMarshaller_Marshall_mCEDD148E1A3453293C757B4636EB4C1F4F829FA0 (PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * __this, PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * ___publicRequest0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m28D1709F9D0841D7B183C81F0C0ADF48E0514269_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m48595B6CB451BC53D2617A05AC16BEE5AFE23788_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m2C4D23C823D545395FDBC79BF090573AEA5260C9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m9D2B1DB4562661F72F9B89936F8868F603AE171C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1F77D28E6879315E73D66F867E162905627E1407);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B8F1DE7E8271919EB50CE5D116B0F803ACEFB60);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94CC7D9BC870205BF912A085D58A7FB72033D488);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA128CFBDC22969D81529BF1140C8705F47CB988C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralADC42BB0C23D6440C9A28C5EE4DA00970F75D59A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBBB8F4B2D76068F57D1096B3199EACDDB534F144);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF9490EED751287724BE78361B0E48EA1B8E24C62);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFE605B72EB9FCBE11100F2C6C4D7EF8582CB6FD0);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	String_t* V_1 = NULL;
	StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * V_2 = NULL;
	JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * V_3 = NULL;
	JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * V_4 = NULL;
	String_t* V_5 = NULL;
	Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B  V_6;
	memset((&V_6), 0, sizeof(V_6));
	PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * L_0 = ___publicRequest0;
		DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4 * L_1 = (DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4 *)il2cpp_codegen_object_new(DefaultRequest_t6C0677B0F393C48280493A2110BB55DCB514A0E4_il2cpp_TypeInfo_var);
		DefaultRequest__ctor_mCC8FA797C2692B14AAF4BB563BB011A03642DAE3(L_1, L_0, _stringLiteralADC42BB0C23D6440C9A28C5EE4DA00970F75D59A, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = _stringLiteral5B8F1DE7E8271919EB50CE5D116B0F803ACEFB60;
		RuntimeObject* L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3;
		L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_2);
		String_t* L_4 = V_1;
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(1 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_3, _stringLiteralA128CFBDC22969D81529BF1140C8705F47CB988C, L_4);
		RuntimeObject* L_5 = V_0;
		NullCheck(L_5);
		RuntimeObject* L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_6);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(1 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_6, _stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18, _stringLiteral94CC7D9BC870205BF912A085D58A7FB72033D488);
		RuntimeObject* L_7 = V_0;
		NullCheck(L_7);
		RuntimeObject* L_8;
		L_8 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.IRequest::get_Headers() */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_7);
		NullCheck(L_8);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(1 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.String>::set_Item(!0,!1) */, IDictionary_2_tA1998BD309D36A531FCABB9A6BE08237AEB22560_il2cpp_TypeInfo_var, L_8, _stringLiteral1F77D28E6879315E73D66F867E162905627E1407, _stringLiteralBBB8F4B2D76068F57D1096B3199EACDDB534F144);
		RuntimeObject* L_9 = V_0;
		NullCheck(L_9);
		InterfaceActionInvoker1< String_t* >::Invoke(7 /* System.Void Amazon.Runtime.Internal.IRequest::set_HttpMethod(System.String) */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_9, _stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D);
		RuntimeObject* L_10 = V_0;
		NullCheck(L_10);
		InterfaceActionInvoker1< String_t* >::Invoke(11 /* System.Void Amazon.Runtime.Internal.IRequest::set_ResourcePath(System.String) */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_10, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		RuntimeObject* L_11 = V_0;
		NullCheck(L_11);
		InterfaceActionInvoker1< int32_t >::Invoke(14 /* System.Void Amazon.Runtime.Internal.IRequest::set_MarshallerVersion(System.Int32) */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_11, 2);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_12;
		L_12 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_13 = (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 *)il2cpp_codegen_object_new(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var);
		StringWriter__ctor_mDF4AB6FD46E8B9824F2F7A9C26EA086A2C1AE5CF(L_13, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
	}

IL_0075:
	try
	{ // begin try (depth: 1)
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_14 = V_2;
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_15 = (JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B *)il2cpp_codegen_object_new(JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B_il2cpp_TypeInfo_var);
			JsonWriter__ctor_m5ED4B5D93FAD7484093D38FFF40DB5EAD705F0A5(L_15, L_14, /*hidden argument*/NULL);
			V_3 = L_15;
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_16 = V_3;
			NullCheck(L_16);
			JsonWriter_WriteObjectStart_m385A8272F17BCB16C9F994381B6BADCE1CFD6A6A(L_16, /*hidden argument*/NULL);
			RuntimeObject* L_17 = V_0;
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_18 = V_3;
			JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_19 = (JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 *)il2cpp_codegen_object_new(JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315_il2cpp_TypeInfo_var);
			JsonMarshallerContext__ctor_m72EC1A523D8129B418D93DAA8CB9D73FF5DC8E76(L_19, L_17, L_18, /*hidden argument*/NULL);
			V_4 = L_19;
			PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * L_20 = ___publicRequest0;
			NullCheck(L_20);
			bool L_21;
			L_21 = PutRecordsRequest_IsSetRecords_m3B5885C7C7269428EB9F4E2473822C278D0F9C55(L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_0116;
			}
		}

IL_0096:
		{
			JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_22 = V_4;
			NullCheck(L_22);
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_23;
			L_23 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_22, /*hidden argument*/NULL);
			NullCheck(L_23);
			JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F(L_23, _stringLiteralF9490EED751287724BE78361B0E48EA1B8E24C62, /*hidden argument*/NULL);
			JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_24 = V_4;
			NullCheck(L_24);
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_25;
			L_25 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_24, /*hidden argument*/NULL);
			NullCheck(L_25);
			JsonWriter_WriteArrayStart_mB1FE97CA88553F8E58669D8438BABEBD586F98C9(L_25, /*hidden argument*/NULL);
			PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * L_26 = ___publicRequest0;
			NullCheck(L_26);
			List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_27;
			L_27 = PutRecordsRequest_get_Records_m0B82025129D059C456710C59395D3880206A8C53_inline(L_26, /*hidden argument*/NULL);
			NullCheck(L_27);
			Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B  L_28;
			L_28 = List_1_GetEnumerator_m9D2B1DB4562661F72F9B89936F8868F603AE171C(L_27, /*hidden argument*/List_1_GetEnumerator_m9D2B1DB4562661F72F9B89936F8868F603AE171C_RuntimeMethod_var);
			V_6 = L_28;
		}

IL_00c0:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00f1;
			}

IL_00c2:
			{
				PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_29;
				L_29 = Enumerator_get_Current_m2C4D23C823D545395FDBC79BF090573AEA5260C9_inline((Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B *)(&V_6), /*hidden argument*/Enumerator_get_Current_m2C4D23C823D545395FDBC79BF090573AEA5260C9_RuntimeMethod_var);
				V_7 = L_29;
				JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_30 = V_4;
				NullCheck(L_30);
				JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_31;
				L_31 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_30, /*hidden argument*/NULL);
				NullCheck(L_31);
				JsonWriter_WriteObjectStart_m385A8272F17BCB16C9F994381B6BADCE1CFD6A6A(L_31, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var);
				PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69 * L_32 = ((PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsRequestEntryMarshaller_t2B839DD391412859CBE3303DDCA74AC408A26C69_il2cpp_TypeInfo_var))->get_Instance_0();
				PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * L_33 = V_7;
				JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_34 = V_4;
				NullCheck(L_32);
				PutRecordsRequestEntryMarshaller_Marshall_m135B7606A8596643EAC3E28D85A93152FBC36F9C(L_32, L_33, L_34, /*hidden argument*/NULL);
				JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_35 = V_4;
				NullCheck(L_35);
				JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_36;
				L_36 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_35, /*hidden argument*/NULL);
				NullCheck(L_36);
				JsonWriter_WriteObjectEnd_m127A9318E08F34226D40487B6D833F540B86A679(L_36, /*hidden argument*/NULL);
			}

IL_00f1:
			{
				bool L_37;
				L_37 = Enumerator_MoveNext_m48595B6CB451BC53D2617A05AC16BEE5AFE23788((Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B *)(&V_6), /*hidden argument*/Enumerator_MoveNext_m48595B6CB451BC53D2617A05AC16BEE5AFE23788_RuntimeMethod_var);
				if (L_37)
				{
					goto IL_00c2;
				}
			}

IL_00fa:
			{
				IL2CPP_LEAVE(0x10A, FINALLY_00fc);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_00fc;
		}

FINALLY_00fc:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m28D1709F9D0841D7B183C81F0C0ADF48E0514269((Enumerator_tE91635399324159E6FFC02DD799E18E7FCC5236B *)(&V_6), /*hidden argument*/Enumerator_Dispose_m28D1709F9D0841D7B183C81F0C0ADF48E0514269_RuntimeMethod_var);
			IL2CPP_END_FINALLY(252)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(252)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_JUMP_TBL(0x10A, IL_010a)
		}

IL_010a:
		{
			JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_38 = V_4;
			NullCheck(L_38);
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_39;
			L_39 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_38, /*hidden argument*/NULL);
			NullCheck(L_39);
			JsonWriter_WriteArrayEnd_m5B07BFF16123F3B2D07899B10B056F8DAD9758C6(L_39, /*hidden argument*/NULL);
		}

IL_0116:
		{
			PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * L_40 = ___publicRequest0;
			NullCheck(L_40);
			bool L_41;
			L_41 = PutRecordsRequest_IsSetStreamName_m01C05A7199CF74108A6FB89B0D3D611BB8E63837(L_40, /*hidden argument*/NULL);
			if (!L_41)
			{
				goto IL_0141;
			}
		}

IL_011e:
		{
			JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_42 = V_4;
			NullCheck(L_42);
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_43;
			L_43 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_42, /*hidden argument*/NULL);
			NullCheck(L_43);
			JsonWriter_WritePropertyName_m98D57C70F6528B0646568D6B0688A045B74E8A8F(L_43, _stringLiteralFE605B72EB9FCBE11100F2C6C4D7EF8582CB6FD0, /*hidden argument*/NULL);
			JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * L_44 = V_4;
			NullCheck(L_44);
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_45;
			L_45 = JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline(L_44, /*hidden argument*/NULL);
			PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * L_46 = ___publicRequest0;
			NullCheck(L_46);
			String_t* L_47;
			L_47 = PutRecordsRequest_get_StreamName_m45845D00DC2F171F10E982D2C435C8322A6C1438_inline(L_46, /*hidden argument*/NULL);
			NullCheck(L_45);
			JsonWriter_Write_m28C2B1F010DAC0E49F8518D184C8C6FD7F83969B(L_45, L_47, /*hidden argument*/NULL);
		}

IL_0141:
		{
			JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_48 = V_3;
			NullCheck(L_48);
			JsonWriter_WriteObjectEnd_m127A9318E08F34226D40487B6D833F540B86A679(L_48, /*hidden argument*/NULL);
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_49 = V_2;
			NullCheck(L_49);
			String_t* L_50;
			L_50 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
			V_5 = L_50;
			RuntimeObject* L_51 = V_0;
			Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_52;
			L_52 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
			String_t* L_53 = V_5;
			NullCheck(L_52);
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_54;
			L_54 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_52, L_53);
			NullCheck(L_51);
			InterfaceActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(16 /* System.Void Amazon.Runtime.Internal.IRequest::set_Content(System.Byte[]) */, IRequest_t3F46EC30F3D73DD6CD00A0851D42CEB73C2FEBEE_il2cpp_TypeInfo_var, L_51, L_54);
			IL2CPP_LEAVE(0x16D, FINALLY_0163);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0163;
	}

FINALLY_0163:
	{ // begin finally (depth: 1)
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_55 = V_2;
			if (!L_55)
			{
				goto IL_016c;
			}
		}

IL_0166:
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_56 = V_2;
			NullCheck(L_56);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_56);
		}

IL_016c:
		{
			IL2CPP_END_FINALLY(355)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(355)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x16D, IL_016d)
	}

IL_016d:
	{
		RuntimeObject* L_57 = V_0;
		return L_57;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * PutRecordsRequestMarshaller_get_Instance_m863F7927ABAC02B5A1083E29BDDFF08AFB5479AA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * L_0 = ((PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestMarshaller__ctor_m82B5BDAD41CAE70F37B843DFC4F1FA6634CC0B5F (PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsRequestMarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsRequestMarshaller__cctor_mF767EEEACE1136D63F71E2CA223C02CEACA07682 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * L_0 = (PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 *)il2cpp_codegen_object_new(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		PutRecordsRequestMarshaller__ctor_m82B5BDAD41CAE70F37B843DFC4F1FA6634CC0B5F(L_0, /*hidden argument*/NULL);
		((PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::set_EncryptionType(Amazon.Kinesis.EncryptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponse_set_EncryptionType_m656BB1986ED3E88A812EF202E6BAB9C0805D6651 (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ___value0, const RuntimeMethod* method)
{
	{
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_0 = ___value0;
		__this->set__encryptionType_3(L_0);
		return;
	}
}
// System.Int32 Amazon.Kinesis.Model.PutRecordsResponse::get_FailedRecordCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PutRecordsResponse_get_FailedRecordCount_m19ABF9954F3E329D2D2E55DA69CD32B520EC35C7 (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * L_0 = __this->get_address_of__failedRecordCount_4();
		int32_t L_1;
		L_1 = Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_inline((Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *)L_0, /*hidden argument*/Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var);
		return L_1;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::set_FailedRecordCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponse_set_FailedRecordCount_m9A84BA582D19D6830CABFBC4706797830C4F9C6F (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184((&L_1), L_0, /*hidden argument*/Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var);
		__this->set__failedRecordCount_4(L_1);
		return;
	}
}
// System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry> Amazon.Kinesis.Model.PutRecordsResponse::get_Records()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * PutRecordsResponse_get_Records_m5A3E1C9489357E2EA3579116899441DCE553037D (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, const RuntimeMethod* method)
{
	{
		List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * L_0 = __this->get__records_5();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::set_Records(System.Collections.Generic.List`1<Amazon.Kinesis.Model.PutRecordsResultEntry>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponse_set_Records_mA1E535E86B2B709A6941EEEC5A96D87C91F6335A (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * L_0 = ___value0;
		__this->set__records_5(L_0);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResponse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponse__ctor_m60FD6F750C159EB03777EB7535CD190E50E1345B (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m6D7DA5C1785CDE161C1059E3ED6AD426AE0D697E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * L_0 = (List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 *)il2cpp_codegen_object_new(List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4_il2cpp_TypeInfo_var);
		List_1__ctor_m6D7DA5C1785CDE161C1059E3ED6AD426AE0D697E(L_0, /*hidden argument*/List_1__ctor_m6D7DA5C1785CDE161C1059E3ED6AD426AE0D697E_RuntimeMethod_var);
		__this->set__records_5(L_0);
		AmazonWebServiceResponse__ctor_mBD52EF4DA0C1AC9176DC586E5BFC88F85F511788(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Runtime.AmazonWebServiceResponse Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AmazonWebServiceResponse_t07514F568FB4853AF7C79F9332DE78DD15EA760A * PutRecordsResponseUnmarshaller_Unmarshall_m9B97ABA762243C2A172D2C597A4A305BFC95CB25 (PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUnmarshaller_2_Unmarshall_mDFDC8DA37111C8507A123F81BDABC8FB3735857B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUnmarshaller_2__ctor_m89D5E1C842B667C35641BB71F900B26FADB8E27A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral375CB1C404C0385792B7E52009524648F40DCCD9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCDD783E351A9472256912F82543292477274E595);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF9490EED751287724BE78361B0E48EA1B8E24C62);
		s_Il2CppMethodInitialized = true;
	}
	PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * V_0 = NULL;
	int32_t V_1 = 0;
	StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * V_2 = NULL;
	IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * V_3 = NULL;
	ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 * V_4 = NULL;
	{
		PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * L_0 = (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 *)il2cpp_codegen_object_new(PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045_il2cpp_TypeInfo_var);
		PutRecordsResponse__ctor_m60FD6F750C159EB03777EB7535CD190E50E1345B(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_1 = ___context0;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_1);
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_3 = ___context0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_3);
		V_1 = L_4;
		goto IL_0089;
	}

IL_0016:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_5 = ___context0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		bool L_7;
		L_7 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_5, _stringLiteral375CB1C404C0385792B7E52009524648F40DCCD9, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_8;
		L_8 = StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline(/*hidden argument*/NULL);
		V_2 = L_8;
		PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * L_9 = V_0;
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_10 = V_2;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_11 = ___context0;
		NullCheck(L_10);
		String_t* L_12;
		L_12 = StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415(L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F_il2cpp_TypeInfo_var);
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_13;
		L_13 = EncryptionType_op_Implicit_m3DFC47783601FD569AB21E0CE01B0BBFA1AA02FC(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		PutRecordsResponse_set_EncryptionType_m656BB1986ED3E88A812EF202E6BAB9C0805D6651_inline(L_9, L_13, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_003e:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_14 = ___context0;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		bool L_16;
		L_16 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_14, _stringLiteralCDD783E351A9472256912F82543292477274E595, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_il2cpp_TypeInfo_var);
		IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * L_17;
		L_17 = IntUnmarshaller_get_Instance_m746BA04951EF77262464E3E09FDC12BFDAA4E321_inline(/*hidden argument*/NULL);
		V_3 = L_17;
		PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * L_18 = V_0;
		IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * L_19 = V_3;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_20 = ___context0;
		NullCheck(L_19);
		int32_t L_21;
		L_21 = IntUnmarshaller_Unmarshall_m6DC0F5DE60C245CE4314BB8D8A7671441EDC46E7(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		PutRecordsResponse_set_FailedRecordCount_m9A84BA582D19D6830CABFBC4706797830C4F9C6F(L_18, L_21, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0061:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_22 = ___context0;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		bool L_24;
		L_24 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_22, _stringLiteralF9490EED751287724BE78361B0E48EA1B8E24C62, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0089;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * L_25;
		L_25 = PutRecordsResultEntryUnmarshaller_get_Instance_m603433B97D4171B1A877D86684D33CE0E61E054D_inline(/*hidden argument*/NULL);
		ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 * L_26 = (ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 *)il2cpp_codegen_object_new(ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8_il2cpp_TypeInfo_var);
		ListUnmarshaller_2__ctor_m89D5E1C842B667C35641BB71F900B26FADB8E27A(L_26, L_25, /*hidden argument*/ListUnmarshaller_2__ctor_m89D5E1C842B667C35641BB71F900B26FADB8E27A_RuntimeMethod_var);
		V_4 = L_26;
		PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * L_27 = V_0;
		ListUnmarshaller_2_tB7553559C8FD5C7ACB06D2831B06425642613FF8 * L_28 = V_4;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_29 = ___context0;
		NullCheck(L_28);
		List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * L_30;
		L_30 = ListUnmarshaller_2_Unmarshall_mDFDC8DA37111C8507A123F81BDABC8FB3735857B(L_28, L_29, /*hidden argument*/ListUnmarshaller_2_Unmarshall_mDFDC8DA37111C8507A123F81BDABC8FB3735857B_RuntimeMethod_var);
		NullCheck(L_27);
		PutRecordsResponse_set_Records_mA1E535E86B2B709A6941EEEC5A96D87C91F6335A_inline(L_27, L_30, /*hidden argument*/NULL);
	}

IL_0089:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_31 = ___context0;
		int32_t L_32 = V_1;
		NullCheck(L_31);
		bool L_33;
		L_33 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_31, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_0016;
		}
	}
	{
		PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * L_34 = V_0;
		return L_34;
	}
}
// Amazon.Runtime.AmazonServiceException Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::UnmarshallException(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,System.Exception,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF * PutRecordsResponseUnmarshaller_UnmarshallException_m0CCE415C24295DB03FA762FB907B5B2C6EF515C9 (PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, Exception_t * ___innerException1, int32_t ___statusCode2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2E0AA02ACB9B6C72DBE03DAF6927F1E835035F10);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4F34DBAAEC28C6F84BF9626DF38FA6484F34C45E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral605A1381100CA3BAE8A2FC12B6AA3B519873344D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBEB5E070F7FC787CED4C5F275ACCA71B0365907A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE401658E1A4AA158FAA8FD47AEF1BA4DD7AEB4A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE4288194BDB19413685247524F3FBE96A06954F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE754822751080A8DD149E86BE95EC91307E3EF3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE8A3A0730E87DF5520C478865697FDBE4DB24387);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF5DFCDED6F5B9810386DA4E3C49944FC9A2BDBCB);
		s_Il2CppMethodInitialized = true;
	}
	ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * V_0 = NULL;
	MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * V_1 = NULL;
	JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * V_2 = NULL;
	AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 10> __leave_targets;
	{
		JsonErrorResponseUnmarshaller_tBB152028F7CF8B3DA016EDA8657E5EB518EA67CB * L_0;
		L_0 = JsonErrorResponseUnmarshaller_GetInstance_mF38659F6157C8EDF253734672FFD34FCD188AAA3(/*hidden argument*/NULL);
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_1 = ___context0;
		NullCheck(L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2;
		L_2 = JsonErrorResponseUnmarshaller_Unmarshall_m057826DF6640BD156507A76F4BDA8CEB1C43EE9B(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_3 = V_0;
		Exception_t * L_4 = ___innerException1;
		NullCheck(L_3);
		ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF_inline(L_3, L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_5 = V_0;
		int32_t L_6 = ___statusCode2;
		NullCheck(L_5);
		ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156_inline(L_5, L_6, /*hidden argument*/NULL);
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_7 = ___context0;
		NullCheck(L_7);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
		L_8 = UnmarshallerContext_GetResponseBodyBytes_m7AA3163269840F761D8DEC6A78CEA74C46C95DAE(L_7, /*hidden argument*/NULL);
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_9 = (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C *)il2cpp_codegen_object_new(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m3E041ADD3DB7EA42E7DB56FE862097819C02B9C2(L_9, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_10 = V_1;
			JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_11 = (JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 *)il2cpp_codegen_object_new(JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686_il2cpp_TypeInfo_var);
			JsonUnmarshallerContext__ctor_mF3B6DEDDF1A70D283EFD799EDAAE8A952AC87D7D(L_11, L_10, (bool)0, (RuntimeObject*)NULL, (bool)0, /*hidden argument*/NULL);
			V_2 = L_11;
		}

IL_0030:
		try
		{ // begin try (depth: 2)
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = V_0;
				NullCheck(L_12);
				String_t* L_13;
				L_13 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_12, /*hidden argument*/NULL);
				if (!L_13)
				{
					goto IL_005c;
				}
			}

IL_0038:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_14 = V_0;
				NullCheck(L_14);
				String_t* L_15;
				L_15 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_14, /*hidden argument*/NULL);
				NullCheck(L_15);
				bool L_16;
				L_16 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_15, _stringLiteralDE754822751080A8DD149E86BE95EC91307E3EF3, /*hidden argument*/NULL);
				if (!L_16)
				{
					goto IL_005c;
				}
			}

IL_004a:
			{
				IL2CPP_RUNTIME_CLASS_INIT(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
				InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * L_17;
				L_17 = InvalidArgumentExceptionUnmarshaller_get_Instance_m5A3C388F04C21D2FD79D9937BF227DCAAB5E6E5E_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_18 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_19 = V_0;
				NullCheck(L_17);
				InvalidArgumentException_t8CA97209CC2B44C96421ED512F5C3B8AD66660AF * L_20;
				L_20 = InvalidArgumentExceptionUnmarshaller_Unmarshall_m9498CC9012F65405D1309ECDB0DFCF4494703EF7(L_17, L_18, L_19, /*hidden argument*/NULL);
				V_3 = L_20;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_005c:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_21 = V_0;
				NullCheck(L_21);
				String_t* L_22;
				L_22 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_21, /*hidden argument*/NULL);
				if (!L_22)
				{
					goto IL_0088;
				}
			}

IL_0064:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_23 = V_0;
				NullCheck(L_23);
				String_t* L_24;
				L_24 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_23, /*hidden argument*/NULL);
				NullCheck(L_24);
				bool L_25;
				L_25 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_24, _stringLiteral2E0AA02ACB9B6C72DBE03DAF6927F1E835035F10, /*hidden argument*/NULL);
				if (!L_25)
				{
					goto IL_0088;
				}
			}

IL_0076:
			{
				IL2CPP_RUNTIME_CLASS_INIT(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
				KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * L_26;
				L_26 = KMSAccessDeniedExceptionUnmarshaller_get_Instance_m7CFCC5F6EE51556E83FE82FFFAF8420240B3A58F_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_27 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_28 = V_0;
				NullCheck(L_26);
				KMSAccessDeniedException_t91580472C8BC4D6604EFFA3C0CEF70DC1F6F0B44 * L_29;
				L_29 = KMSAccessDeniedExceptionUnmarshaller_Unmarshall_m7512E68CFE3157D6371AA838CA213140DE413CF5(L_26, L_27, L_28, /*hidden argument*/NULL);
				V_3 = L_29;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_0088:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_30 = V_0;
				NullCheck(L_30);
				String_t* L_31;
				L_31 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_30, /*hidden argument*/NULL);
				if (!L_31)
				{
					goto IL_00b4;
				}
			}

IL_0090:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_32 = V_0;
				NullCheck(L_32);
				String_t* L_33;
				L_33 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_32, /*hidden argument*/NULL);
				NullCheck(L_33);
				bool L_34;
				L_34 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_33, _stringLiteralDE401658E1A4AA158FAA8FD47AEF1BA4DD7AEB4A, /*hidden argument*/NULL);
				if (!L_34)
				{
					goto IL_00b4;
				}
			}

IL_00a2:
			{
				IL2CPP_RUNTIME_CLASS_INIT(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
				KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * L_35;
				L_35 = KMSDisabledExceptionUnmarshaller_get_Instance_m9B0A090129654FD01B9FF75448776DE135F62360_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_36 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_37 = V_0;
				NullCheck(L_35);
				KMSDisabledException_t524AC9D240E7FEA2F9035A3D67BAACEEA8F2337A * L_38;
				L_38 = KMSDisabledExceptionUnmarshaller_Unmarshall_mB574723FC32FCB75EF56889945657C4EC48C7CDF(L_35, L_36, L_37, /*hidden argument*/NULL);
				V_3 = L_38;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_00b4:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_39 = V_0;
				NullCheck(L_39);
				String_t* L_40;
				L_40 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_39, /*hidden argument*/NULL);
				if (!L_40)
				{
					goto IL_00e0;
				}
			}

IL_00bc:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_41 = V_0;
				NullCheck(L_41);
				String_t* L_42;
				L_42 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_41, /*hidden argument*/NULL);
				NullCheck(L_42);
				bool L_43;
				L_43 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_42, _stringLiteral4F34DBAAEC28C6F84BF9626DF38FA6484F34C45E, /*hidden argument*/NULL);
				if (!L_43)
				{
					goto IL_00e0;
				}
			}

IL_00ce:
			{
				IL2CPP_RUNTIME_CLASS_INIT(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
				KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * L_44;
				L_44 = KMSInvalidStateExceptionUnmarshaller_get_Instance_mB7E189F9F3EED17B1738DEAC65C14CF08A31366F_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_45 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_46 = V_0;
				NullCheck(L_44);
				KMSInvalidStateException_t621DB773477A461BBF9A42893A68DBEBE70D6068 * L_47;
				L_47 = KMSInvalidStateExceptionUnmarshaller_Unmarshall_m8AECCA10430208CD1BA8DAFA3EED99F28E9C93F9(L_44, L_45, L_46, /*hidden argument*/NULL);
				V_3 = L_47;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_00e0:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_48 = V_0;
				NullCheck(L_48);
				String_t* L_49;
				L_49 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_48, /*hidden argument*/NULL);
				if (!L_49)
				{
					goto IL_010c;
				}
			}

IL_00e8:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_50 = V_0;
				NullCheck(L_50);
				String_t* L_51;
				L_51 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_50, /*hidden argument*/NULL);
				NullCheck(L_51);
				bool L_52;
				L_52 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_51, _stringLiteralDE4288194BDB19413685247524F3FBE96A06954F, /*hidden argument*/NULL);
				if (!L_52)
				{
					goto IL_010c;
				}
			}

IL_00fa:
			{
				IL2CPP_RUNTIME_CLASS_INIT(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
				KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * L_53;
				L_53 = KMSNotFoundExceptionUnmarshaller_get_Instance_mF5D5B0BAB7DDB7ED4A1ECD86EEF0497B3BF2DCAE_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_54 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_55 = V_0;
				NullCheck(L_53);
				KMSNotFoundException_t780BA01E3E1E2C2968110CBBB52609608500F4A1 * L_56;
				L_56 = KMSNotFoundExceptionUnmarshaller_Unmarshall_mA9C221F55F082CA98EA09CC7C4D08E6FA92015AB(L_53, L_54, L_55, /*hidden argument*/NULL);
				V_3 = L_56;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_010c:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_57 = V_0;
				NullCheck(L_57);
				String_t* L_58;
				L_58 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_57, /*hidden argument*/NULL);
				if (!L_58)
				{
					goto IL_0138;
				}
			}

IL_0114:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_59 = V_0;
				NullCheck(L_59);
				String_t* L_60;
				L_60 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_59, /*hidden argument*/NULL);
				NullCheck(L_60);
				bool L_61;
				L_61 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_60, _stringLiteralE8A3A0730E87DF5520C478865697FDBE4DB24387, /*hidden argument*/NULL);
				if (!L_61)
				{
					goto IL_0138;
				}
			}

IL_0126:
			{
				IL2CPP_RUNTIME_CLASS_INIT(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
				KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * L_62;
				L_62 = KMSOptInRequiredExceptionUnmarshaller_get_Instance_m0F9A55C20A1FC41CBA5F3F26833A38D1D126666D_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_63 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_64 = V_0;
				NullCheck(L_62);
				KMSOptInRequiredException_t62AE646D74D799EA260BC7D740EB6AAC199AF785 * L_65;
				L_65 = KMSOptInRequiredExceptionUnmarshaller_Unmarshall_m971A2AD42993EB0E84F40524B6367132EE6B2F90(L_62, L_63, L_64, /*hidden argument*/NULL);
				V_3 = L_65;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_0138:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_66 = V_0;
				NullCheck(L_66);
				String_t* L_67;
				L_67 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_66, /*hidden argument*/NULL);
				if (!L_67)
				{
					goto IL_0164;
				}
			}

IL_0140:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_68 = V_0;
				NullCheck(L_68);
				String_t* L_69;
				L_69 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_68, /*hidden argument*/NULL);
				NullCheck(L_69);
				bool L_70;
				L_70 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_69, _stringLiteral605A1381100CA3BAE8A2FC12B6AA3B519873344D, /*hidden argument*/NULL);
				if (!L_70)
				{
					goto IL_0164;
				}
			}

IL_0152:
			{
				IL2CPP_RUNTIME_CLASS_INIT(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
				KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * L_71;
				L_71 = KMSThrottlingExceptionUnmarshaller_get_Instance_m66206A46CF3E7C5A7A9020DCE942FA3FBAFC6BDC_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_72 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_73 = V_0;
				NullCheck(L_71);
				KMSThrottlingException_t17E9F326BE276786715638BBB7D5F49873B46C3E * L_74;
				L_74 = KMSThrottlingExceptionUnmarshaller_Unmarshall_m45AC671D3AB3E89505ECE3A0A47116DBA7156A8B(L_71, L_72, L_73, /*hidden argument*/NULL);
				V_3 = L_74;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_0164:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_75 = V_0;
				NullCheck(L_75);
				String_t* L_76;
				L_76 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_75, /*hidden argument*/NULL);
				if (!L_76)
				{
					goto IL_018d;
				}
			}

IL_016c:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_77 = V_0;
				NullCheck(L_77);
				String_t* L_78;
				L_78 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_77, /*hidden argument*/NULL);
				NullCheck(L_78);
				bool L_79;
				L_79 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_78, _stringLiteralF5DFCDED6F5B9810386DA4E3C49944FC9A2BDBCB, /*hidden argument*/NULL);
				if (!L_79)
				{
					goto IL_018d;
				}
			}

IL_017e:
			{
				IL2CPP_RUNTIME_CLASS_INIT(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
				ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * L_80;
				L_80 = ProvisionedThroughputExceededExceptionUnmarshaller_get_Instance_mC05CFB60AD500DA5874EE0BF8B74263AED866DBB_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_81 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_82 = V_0;
				NullCheck(L_80);
				ProvisionedThroughputExceededException_t436EDABDD986FBB5C72F830D620CBF271792BA87 * L_83;
				L_83 = ProvisionedThroughputExceededExceptionUnmarshaller_Unmarshall_mC8C5292AAD0B3F329DC9220FFCAE60DCC5A0857E(L_80, L_81, L_82, /*hidden argument*/NULL);
				V_3 = L_83;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_018d:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_84 = V_0;
				NullCheck(L_84);
				String_t* L_85;
				L_85 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_84, /*hidden argument*/NULL);
				if (!L_85)
				{
					goto IL_01b6;
				}
			}

IL_0195:
			{
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_86 = V_0;
				NullCheck(L_86);
				String_t* L_87;
				L_87 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_86, /*hidden argument*/NULL);
				NullCheck(L_87);
				bool L_88;
				L_88 = String_Equals_m8A062B96B61A7D652E7D73C9B3E904F6B0E5F41D(L_87, _stringLiteralBEB5E070F7FC787CED4C5F275ACCA71B0365907A, /*hidden argument*/NULL);
				if (!L_88)
				{
					goto IL_01b6;
				}
			}

IL_01a7:
			{
				IL2CPP_RUNTIME_CLASS_INIT(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
				ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * L_89;
				L_89 = ResourceNotFoundExceptionUnmarshaller_get_Instance_mE30554BC8F95AF3250FC3972F929F1D37C638C33_inline(/*hidden argument*/NULL);
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_90 = V_2;
				ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_91 = V_0;
				NullCheck(L_89);
				ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * L_92;
				L_92 = ResourceNotFoundExceptionUnmarshaller_Unmarshall_m080557324DF57827FAF33DC7E9C3A4ABFFB375C1(L_89, L_90, L_91, /*hidden argument*/NULL);
				V_3 = L_92;
				IL2CPP_LEAVE(0x1F6, FINALLY_01b8);
			}

IL_01b6:
			{
				IL2CPP_LEAVE(0x1CC, FINALLY_01b8);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_01b8;
		}

FINALLY_01b8:
		{ // begin finally (depth: 2)
			{
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_93 = V_2;
				if (!L_93)
				{
					goto IL_01c1;
				}
			}

IL_01bb:
			{
				JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_94 = V_2;
				NullCheck(L_94);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_94);
			}

IL_01c1:
			{
				IL2CPP_END_FINALLY(440)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(440)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x1F6, FINALLY_01c2);
			IL2CPP_END_CLEANUP(0x1CC, FINALLY_01c2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01c2;
	}

FINALLY_01c2:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_95 = V_1;
			if (!L_95)
			{
				goto IL_01cb;
			}
		}

IL_01c5:
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_96 = V_1;
			NullCheck(L_96);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_96);
		}

IL_01cb:
		{
			IL2CPP_END_FINALLY(450)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(450)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1F6, IL_01f6)
		IL2CPP_JUMP_TBL(0x1CC, IL_01cc)
	}

IL_01cc:
	{
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_97 = V_0;
		NullCheck(L_97);
		String_t* L_98;
		L_98 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_97, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_99 = V_0;
		NullCheck(L_99);
		Exception_t * L_100;
		L_100 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_99, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_101 = V_0;
		NullCheck(L_101);
		int32_t L_102;
		L_102 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_101, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_103 = V_0;
		NullCheck(L_103);
		String_t* L_104;
		L_104 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_103, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_105 = V_0;
		NullCheck(L_105);
		String_t* L_106;
		L_106 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_105, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_107 = V_0;
		NullCheck(L_107);
		int32_t L_108;
		L_108 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_107, /*hidden argument*/NULL);
		AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B * L_109 = (AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B *)il2cpp_codegen_object_new(AmazonKinesisException_t9AADCAD1E09C798A1C007F46DDD16EFA589F4D8B_il2cpp_TypeInfo_var);
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(L_109, L_98, L_100, L_102, L_104, L_106, L_108, /*hidden argument*/NULL);
		return L_109;
	}

IL_01f6:
	{
		AmazonServiceException_t69E9529AE45BFC6620F7887448B25FC2F97B14FF * L_110 = V_3;
		return L_110;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * PutRecordsResponseUnmarshaller_get_Instance_mCAE356524F66AE7AE050AA8CCE6CE1327AC1EF3E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * L_0 = ((PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponseUnmarshaller__ctor_mA565C6583D8EE26409E5FF4AD10573F4B0C81EC8 (PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * __this, const RuntimeMethod* method)
{
	{
		JsonResponseUnmarshaller__ctor_m7A471A9D41EBFE83FEA37BAAD40BCF2D485F9ADE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResponseUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResponseUnmarshaller__cctor_mC345C3E49E7CAF26AE8E7C49C8005A4A645DAE9E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * L_0 = (PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C *)il2cpp_codegen_object_new(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		PutRecordsResponseUnmarshaller__ctor_mA565C6583D8EE26409E5FF4AD10573F4B0C81EC8(L_0, /*hidden argument*/NULL);
		((PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_ErrorCode(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ErrorCode_mBD158DC0CC885F5B518CE4F48D5D1F7A07C8C050 (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__errorCode_0(L_0);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_ErrorMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ErrorMessage_m431BBF0E595CDEB59DA750B3BE3099F232D849DF (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__errorMessage_1(L_0);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_SequenceNumber(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_SequenceNumber_mE137FD3C0A7B5BB0D21BE05B927D4C526D5098EE (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__sequenceNumber_2(L_0);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::set_ShardId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ShardId_mFD4ADEE87AF692E117D37647479BD359F56D6C83 (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__shardId_3(L_0);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.PutRecordsResultEntry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntry__ctor_mC05AB10DE255D87608774275EF30A8DA8C2F5368 (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.PutRecordsResultEntry Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::Amazon.Runtime.Internal.Transform.IUnmarshaller<Amazon.Kinesis.Model.PutRecordsResultEntry,Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext>.Unmarshall(Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * PutRecordsResultEntryUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_Kinesis_Model_PutRecordsResultEntryU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mC56E04FBA392907DB6AD240FF3EA1156F56ED5C4 (PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * __this, XmlUnmarshallerContext_t300C2CD19116463FF210FF5A2F196D9910AE7943 * ___context0, const RuntimeMethod* method)
{
	{
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_0 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PutRecordsResultEntryUnmarshaller_Amazon_Runtime_Internal_Transform_IUnmarshallerU3CAmazon_Kinesis_Model_PutRecordsResultEntryU2CAmazon_Runtime_Internal_Transform_XmlUnmarshallerContextU3E_Unmarshall_mC56E04FBA392907DB6AD240FF3EA1156F56ED5C4_RuntimeMethod_var)));
	}
}
// Amazon.Kinesis.Model.PutRecordsResultEntry Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * PutRecordsResultEntryUnmarshaller_Unmarshall_mEE08D1E048F4F076300EB10B6FC93D1CBBABE1E9 (PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1021DBAB771621B55019EBDCA7C9CA5DB3FDB1B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CD2848583A4EAD77430A317E3B6DB2BCCDB1003);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8BDEA0B4D2FABE3E1DFEE0187E022E1AD5F29CED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC1C97CAE5706ED016D99773A34B00C678F4C2A6C);
		s_Il2CppMethodInitialized = true;
	}
	PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * V_0 = NULL;
	int32_t V_1 = 0;
	StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * V_2 = NULL;
	StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * V_3 = NULL;
	StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * V_4 = NULL;
	StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * V_5 = NULL;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_2 = ___context0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = JsonUnmarshallerContext_get_CurrentTokenType_mA60F9E7148B1EF1BCD664D3AB068A81910999E19(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0013;
		}
	}
	{
		return (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 *)NULL;
	}

IL_0013:
	{
		PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * L_4 = (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 *)il2cpp_codegen_object_new(PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232_il2cpp_TypeInfo_var);
		PutRecordsResultEntry__ctor_mC05AB10DE255D87608774275EF30A8DA8C2F5368(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_5 = ___context0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_5);
		V_1 = L_6;
		goto IL_00b3;
	}

IL_0025:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_7 = ___context0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		bool L_9;
		L_9 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_7, _stringLiteral1021DBAB771621B55019EBDCA7C9CA5DB3FDB1B6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_10;
		L_10 = StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline(/*hidden argument*/NULL);
		V_2 = L_10;
		PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * L_11 = V_0;
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_12 = V_2;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_13 = ___context0;
		NullCheck(L_12);
		String_t* L_14;
		L_14 = StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		PutRecordsResultEntry_set_ErrorCode_mBD158DC0CC885F5B518CE4F48D5D1F7A07C8C050_inline(L_11, L_14, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0048:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		bool L_17;
		L_17 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_15, _stringLiteral6CD2848583A4EAD77430A317E3B6DB2BCCDB1003, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_18;
		L_18 = StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline(/*hidden argument*/NULL);
		V_3 = L_18;
		PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * L_19 = V_0;
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_20 = V_3;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_21 = ___context0;
		NullCheck(L_20);
		String_t* L_22;
		L_22 = StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		PutRecordsResultEntry_set_ErrorMessage_m431BBF0E595CDEB59DA750B3BE3099F232D849DF_inline(L_19, L_22, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_006b:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_23 = ___context0;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		bool L_25;
		L_25 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_23, _stringLiteral8BDEA0B4D2FABE3E1DFEE0187E022E1AD5F29CED, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_26;
		L_26 = StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline(/*hidden argument*/NULL);
		V_4 = L_26;
		PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * L_27 = V_0;
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_28 = V_4;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_29 = ___context0;
		NullCheck(L_28);
		String_t* L_30;
		L_30 = StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		PutRecordsResultEntry_set_SequenceNumber_mE137FD3C0A7B5BB0D21BE05B927D4C526D5098EE_inline(L_27, L_30, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0090:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_31 = ___context0;
		int32_t L_32 = V_1;
		NullCheck(L_31);
		bool L_33;
		L_33 = UnmarshallerContext_TestExpression_m0AA7AF592F5B931209B33A6A25394339D0D94A30(L_31, _stringLiteralC1C97CAE5706ED016D99773A34B00C678F4C2A6C, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_34;
		L_34 = StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline(/*hidden argument*/NULL);
		V_5 = L_34;
		PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * L_35 = V_0;
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_36 = V_5;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_37 = ___context0;
		NullCheck(L_36);
		String_t* L_38;
		L_38 = StringUnmarshaller_Unmarshall_m0B13BA9687EC89F1B05A3F3582CC7F7767FFF415(L_36, L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		PutRecordsResultEntry_set_ShardId_mFD4ADEE87AF692E117D37647479BD359F56D6C83_inline(L_35, L_38, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_39 = ___context0;
		int32_t L_40 = V_1;
		NullCheck(L_39);
		bool L_41;
		L_41 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_39, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_0025;
		}
	}
	{
		PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * L_42 = V_0;
		return L_42;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * PutRecordsResultEntryUnmarshaller_get_Instance_m603433B97D4171B1A877D86684D33CE0E61E054D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * L_0 = ((PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntryUnmarshaller__ctor_m846B750DDEB9CCBFB74F923A44B957A573924CC1 (PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.PutRecordsResultEntryUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PutRecordsResultEntryUnmarshaller__cctor_mA550D8F2D4D40A099DB498BC96626ED67897AE1E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * L_0 = (PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA *)il2cpp_codegen_object_new(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		PutRecordsResultEntryUnmarshaller__ctor_m846B750DDEB9CCBFB74F923A44B957A573924CC1(L_0, /*hidden argument*/NULL);
		((PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Amazon.Kinesis.Model.ResourceNotFoundException::.ctor(System.String,System.Exception,Amazon.Runtime.ErrorType,System.String,System.String,System.Net.HttpStatusCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResourceNotFoundException__ctor_mF9051383698D65CF7D915BF2230CE0F698D73715 (ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___errorType2, String_t* ___errorCode3, String_t* ___requestId4, int32_t ___statusCode5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___errorType2;
		String_t* L_3 = ___errorCode3;
		String_t* L_4 = ___requestId4;
		int32_t L_5 = ___statusCode5;
		AmazonKinesisException__ctor_m6B84E12C9527185540152EB45331C0E4D10C2FC9(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Amazon.Kinesis.Model.ResourceNotFoundException Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * ResourceNotFoundExceptionUnmarshaller_Unmarshall_m23272689D690DD15D71B9A106B1FE601BE214FC1 (ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_1 = (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 *)il2cpp_codegen_object_new(ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0_il2cpp_TypeInfo_var);
		ErrorResponse__ctor_mD491F5F069A4B6A11457D6DA19ABA4D71C4EF55C(L_1, /*hidden argument*/NULL);
		ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * L_2;
		L_2 = ResourceNotFoundExceptionUnmarshaller_Unmarshall_m080557324DF57827FAF33DC7E9C3A4ABFFB375C1(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Amazon.Kinesis.Model.ResourceNotFoundException Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::Unmarshall(Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext,Amazon.Runtime.Internal.ErrorResponse)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * ResourceNotFoundExceptionUnmarshaller_Unmarshall_m080557324DF57827FAF33DC7E9C3A4ABFFB375C1 (ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * __this, JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * ___context0, ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * ___errorResponse1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * V_0 = NULL;
	int32_t V_1 = 0;
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_0 = ___context0;
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::Read() */, L_0);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_2 = ___errorResponse1;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline(L_2, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_4 = ___errorResponse1;
		NullCheck(L_4);
		Exception_t * L_5;
		L_5 = ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline(L_4, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_6 = ___errorResponse1;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline(L_6, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_8 = ___errorResponse1;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline(L_8, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_10 = ___errorResponse1;
		NullCheck(L_10);
		String_t* L_11;
		L_11 = ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline(L_10, /*hidden argument*/NULL);
		ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * L_12 = ___errorResponse1;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline(L_12, /*hidden argument*/NULL);
		ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * L_14 = (ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B *)il2cpp_codegen_object_new(ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B_il2cpp_TypeInfo_var);
		ResourceNotFoundException__ctor_mF9051383698D65CF7D915BF2230CE0F698D73715(L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_15 = ___context0;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::get_CurrentDepth() */, L_15);
		V_1 = L_16;
	}

IL_0038:
	{
		JsonUnmarshallerContext_t5BB2FCE3B8950BAA5EB8C79B322826C624643686 * L_17 = ___context0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = UnmarshallerContext_ReadAtDepth_mB056A3B0303BFBD5DBBE5F93E10F24F1FD5C9C27(L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0038;
		}
	}
	{
		ResourceNotFoundException_t676E21FD37622BE3E8A5862517BCCC1D8DEEFB2B * L_20 = V_0;
		return L_20;
	}
}
// Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * ResourceNotFoundExceptionUnmarshaller_get_Instance_mE30554BC8F95AF3250FC3972F929F1D37C638C33 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * L_0 = ((ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_StaticFields*)il2cpp_codegen_static_fields_for(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResourceNotFoundExceptionUnmarshaller__ctor_m3032315C83DB7FF387C99236E17485DCB10E1ADB (ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Amazon.Kinesis.Model.Internal.MarshallTransformations.ResourceNotFoundExceptionUnmarshaller::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResourceNotFoundExceptionUnmarshaller__cctor_m95EF141F8CE6EAB7E88458887C3435E43EB70D5D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * L_0 = (ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 *)il2cpp_codegen_object_new(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		ResourceNotFoundExceptionUnmarshaller__ctor_m3032315C83DB7FF387C99236E17485DCB10E1ADB(L_0, /*hidden argument*/NULL);
		((ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_StaticFields*)il2cpp_codegen_static_fields_for(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var))->set__instance_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * PutRecordsRequestMarshaller_get_Instance_m863F7927ABAC02B5A1083E29BDDFF08AFB5479AA_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var);
		PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15 * L_0 = ((PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsRequestMarshaller_tE8B80013E81E511C8A9BB92780076B838DC7CA15_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * PutRecordsResponseUnmarshaller_get_Instance_mCAE356524F66AE7AE050AA8CCE6CE1327AC1EF3E_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var);
		PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C * L_0 = ((PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsResponseUnmarshaller_tCD4909E741B2539DFF785ED202BBB01E35A8843C_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ClientConfig_set_AuthenticationServiceName_mA6A0E4F14EEA4C9A42061D4C18AEF69785CF83E1_inline (ClientConfig_t718F7F15C79C7CDF53B4BA93C331A0ADEBB0B099 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_authServiceName_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ErrorResponse_get_Message_m422005D49E9E6DB3211C8D60B2D7F515CD2B5188_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CMessageU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Exception_t * ErrorResponse_get_InnerException_mFE68A2767329BDD2C23FDE3479CD9E2CB47447D6_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method)
{
	{
		Exception_t * L_0 = __this->get_U3CInnerExceptionU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ErrorResponse_get_Type_m8A3D25C3C5C442B4817C54B16536152B7A165B06_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTypeU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ErrorResponse_get_Code_mDDED34BAD0F16E75B2E0DCBCE84DE6C4C25F3CCD_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCodeU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ErrorResponse_get_RequestId_m47CBA9EF25DD2D2DC2A5F25DE9EE04F6A6ADAE34_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CRequestIdU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ErrorResponse_get_StatusCode_mFCF1DB376A96C427D83F389B1E05FC8BB5A95461_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CStatusCodeU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * JsonMarshallerContext_get_Writer_mE40C79BDDA648538056C762EB7F257ECEB9AE77B_inline (JsonMarshallerContext_t2F0059B63727A2751A315BF9F8C5701D2DA3E315 * __this, const RuntimeMethod* method)
{
	{
		JsonWriter_tAEF2B40EE9CAB09A043EC9FCD3FD97CDF887AC8B * L_0 = __this->get_U3CWriterU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * PutRecordsRequestEntry_get_Data_m6838F4D073A2FBC74A6DC77F8110DBF794D64D96_inline (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_0 = __this->get__data_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PutRecordsRequestEntry_get_ExplicitHashKey_m0CBD1B2DFC1D20BB67A0ED020694A62A646E044A_inline (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__explicitHashKey_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PutRecordsRequestEntry_get_PartitionKey_m5393CA895759E34EB672953C0FB276748F25C075_inline (PutRecordsRequestEntry_tE1F98C25A128EA53B6D83DBEE20A3827A6F266BF * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__partitionKey_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * PutRecordsRequest_get_Records_m0B82025129D059C456710C59395D3880206A8C53_inline (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	{
		List_1_t6FB1A19583E10766B69FB8C2B6F271BE301748B8 * L_0 = __this->get__records_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PutRecordsRequest_get_StreamName_m45845D00DC2F171F10E982D2C435C8322A6C1438_inline (PutRecordsRequest_tED0EAA1E46748BD1AE175851BAB61709B82BD8FD * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__streamName_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * StringUnmarshaller_get_Instance_m38168CCCA5DD39F255F64D42246137C302C96436_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var);
		StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7 * L_0 = ((StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_StaticFields*)il2cpp_codegen_static_fields_for(StringUnmarshaller_t4871B77EB6379C524C14F11B8C74AAC50A3CB2A7_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResponse_set_EncryptionType_m656BB1986ED3E88A812EF202E6BAB9C0805D6651_inline (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * ___value0, const RuntimeMethod* method)
{
	{
		EncryptionType_tDDEA6D425BBA152CEEECCBC1BA3D002F27A6106F * L_0 = ___value0;
		__this->set__encryptionType_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * IntUnmarshaller_get_Instance_m746BA04951EF77262464E3E09FDC12BFDAA4E321_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_il2cpp_TypeInfo_var);
		IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4 * L_0 = ((IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_StaticFields*)il2cpp_codegen_static_fields_for(IntUnmarshaller_t0662AC84D59803239AA654230B1C9C10958D02F4_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * PutRecordsResultEntryUnmarshaller_get_Instance_m603433B97D4171B1A877D86684D33CE0E61E054D_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var);
		PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA * L_0 = ((PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_StaticFields*)il2cpp_codegen_static_fields_for(PutRecordsResultEntryUnmarshaller_t689B93A4E323005063EC947B1A279737C80A14CA_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResponse_set_Records_mA1E535E86B2B709A6941EEEC5A96D87C91F6335A_inline (PutRecordsResponse_tF8806E8B0A10DEFEA047D303CE604D7841BBC045 * __this, List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t90D282FCDC6B3B8BD3DD0DE0DBA17C31CCF396C4 * L_0 = ___value0;
		__this->set__records_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ErrorResponse_set_InnerException_m40B49F99563E2A7B1228E0ACAC6E93F6EA7EACDF_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, Exception_t * ___value0, const RuntimeMethod* method)
{
	{
		Exception_t * L_0 = ___value0;
		__this->set_U3CInnerExceptionU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ErrorResponse_set_StatusCode_mABA88F73D9A3B8B238F10ACD71B3850DED488156_inline (ErrorResponse_t28C8DA36F705F0156D18A0F496544330788C71A0 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStatusCodeU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * InvalidArgumentExceptionUnmarshaller_get_Instance_m5A3C388F04C21D2FD79D9937BF227DCAAB5E6E5E_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var);
		InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4 * L_0 = ((InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_StaticFields*)il2cpp_codegen_static_fields_for(InvalidArgumentExceptionUnmarshaller_tC7C7566554E8CDA1D614B537CD147096E80D53D4_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * KMSAccessDeniedExceptionUnmarshaller_get_Instance_m7CFCC5F6EE51556E83FE82FFFAF8420240B3A58F_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var);
		KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B * L_0 = ((KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_StaticFields*)il2cpp_codegen_static_fields_for(KMSAccessDeniedExceptionUnmarshaller_tA8584EC36F14F5F1ED19624CD9E2E6E75A22F27B_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * KMSDisabledExceptionUnmarshaller_get_Instance_m9B0A090129654FD01B9FF75448776DE135F62360_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var);
		KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF * L_0 = ((KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_StaticFields*)il2cpp_codegen_static_fields_for(KMSDisabledExceptionUnmarshaller_tF841F98F56F8CD003368256C17AE07A6C811AEDF_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * KMSInvalidStateExceptionUnmarshaller_get_Instance_mB7E189F9F3EED17B1738DEAC65C14CF08A31366F_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var);
		KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728 * L_0 = ((KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_StaticFields*)il2cpp_codegen_static_fields_for(KMSInvalidStateExceptionUnmarshaller_tC3E3C2621E02C8F1A7987C4CD5C437DC3CAF2728_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * KMSNotFoundExceptionUnmarshaller_get_Instance_mF5D5B0BAB7DDB7ED4A1ECD86EEF0497B3BF2DCAE_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var);
		KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1 * L_0 = ((KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_StaticFields*)il2cpp_codegen_static_fields_for(KMSNotFoundExceptionUnmarshaller_t0457341BF66B83D6132BFDDA70CF3721DCE8D4C1_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * KMSOptInRequiredExceptionUnmarshaller_get_Instance_m0F9A55C20A1FC41CBA5F3F26833A38D1D126666D_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var);
		KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5 * L_0 = ((KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_StaticFields*)il2cpp_codegen_static_fields_for(KMSOptInRequiredExceptionUnmarshaller_tEF140A7EA26B2C3AF719C00C882F8C1E8AA1BAF5_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * KMSThrottlingExceptionUnmarshaller_get_Instance_m66206A46CF3E7C5A7A9020DCE942FA3FBAFC6BDC_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var);
		KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37 * L_0 = ((KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_StaticFields*)il2cpp_codegen_static_fields_for(KMSThrottlingExceptionUnmarshaller_tCBF678710ABD5397DCEA4D4A1659B554059DEA37_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * ProvisionedThroughputExceededExceptionUnmarshaller_get_Instance_mC05CFB60AD500DA5874EE0BF8B74263AED866DBB_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var);
		ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7 * L_0 = ((ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_StaticFields*)il2cpp_codegen_static_fields_for(ProvisionedThroughputExceededExceptionUnmarshaller_tAE4BA0422D8D0376A645D9DE045EA6245B1830A7_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * ResourceNotFoundExceptionUnmarshaller_get_Instance_mE30554BC8F95AF3250FC3972F929F1D37C638C33_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var);
		ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01 * L_0 = ((ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_StaticFields*)il2cpp_codegen_static_fields_for(ResourceNotFoundExceptionUnmarshaller_t4EC8589A7EF188D188F9FD2E0BDF59090C2A5D01_il2cpp_TypeInfo_var))->get__instance_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ErrorCode_mBD158DC0CC885F5B518CE4F48D5D1F7A07C8C050_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__errorCode_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ErrorMessage_m431BBF0E595CDEB59DA750B3BE3099F232D849DF_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__errorMessage_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_SequenceNumber_mE137FD3C0A7B5BB0D21BE05B927D4C526D5098EE_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__sequenceNumber_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PutRecordsResultEntry_set_ShardId_mFD4ADEE87AF692E117D37647479BD359F56D6C83_inline (PutRecordsResultEntry_t3C7AF1BEEF8654F36E8AC6F0FFE87BBD622E2232 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__shardId_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_gshared_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_0();
		return (int32_t)L_0;
	}
}
